//
//  UnitGroupDrawer.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/12/20.
//

#ifndef UnitGroupDrawer_h
#define UnitGroupDrawer_h

#include <stdio.h>
#include "cocos2d.h"
#include "UnitGroup.h"
USING_NS_CC;


class Team;
class PrimativeDrawer;
class InteractiveObjectHighlighter;

/*
 Draws a rectangle around a command defined selection area, and uses an InteractiveObjectHighlighter to highlight entities that would be part of a UnitGroup
 */
class UnitGroupDrawer : public DrawNode
{
private:
    Vector<Unit *> _unitsInGroup;
    Vec2 startPosition;
    UnitGroup * _unitGroup;
    
    InteractiveObjectHighlighter * _highlighter;
    
    UnitGroupDrawer();
    ~UnitGroupDrawer();
    bool init();

public:
    static UnitGroupDrawer * create();
    
    UnitGroup * getUnitGroup() {return _unitGroup;}

    virtual void dragStart(Vec2 point);
    virtual void dragMove(Vec2 point);
    virtual void dragEnd(Vec2 point);
    
};

#endif /* UnitGroupDrawer_h */
