#include "AppDelegate.h"
#include "MainMenuScene.h"
#include "TrenchWarsManager.h"

// #define USE_AUDIO_ENGINE 1
// #define USE_SIMPLE_AUDIO_ENGINE 1

#if USE_AUDIO_ENGINE && USE_SIMPLE_AUDIO_ENGINE
#error "Don't use AudioEngine and SimpleAudioEngine at the same time. Please just select one in your game!"
#endif

#if USE_AUDIO_ENGINE
#include "audio/include/AudioEngine.h"
using namespace cocos2d::experimental;
#elif USE_SIMPLE_AUDIO_ENGINE
#include "audio/include/SimpleAudioEngine.h"
using namespace CocosDenshion;
#endif

USING_NS_CC;

static cocos2d::Size designResolutionSize = cocos2d::Size(480, 320);
static cocos2d::Size smallResolutionSize = cocos2d::Size(480, 320);
static cocos2d::Size mediumResolutionSize = cocos2d::Size(1024, 768);
static cocos2d::Size largeResolutionSize = cocos2d::Size(2048, 1536);

AppDelegate::AppDelegate()
{
}

AppDelegate::~AppDelegate() 
{
#if USE_AUDIO_ENGINE
    AudioEngine::end();
#elif USE_SIMPLE_AUDIO_ENGINE
    SimpleAudioEngine::end();
#endif
}

// if you want a different context, modify the value of glContextAttrs
// it will affect all platforms
void AppDelegate::initGLContextAttrs()
{
    // set OpenGL context attributes: red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

// if you want to use the package manager to install more packages,  
// don't modify or remove this function
static int register_all_packages()
{
    return 0; //flag for packages manager
}

Size AppDelegate::getDeviceScreenSize() {

    Size screenSize = Size::ZERO;

    GLFWmonitor *monitor = glfwGetPrimaryMonitor();
    if (nullptr != monitor) {
        const GLFWvidmode* videoMode = glfwGetVideoMode(monitor);
        screenSize = Size(videoMode->width, videoMode->height);
    }
    else {
        auto glview = Director::getInstance()->getOpenGLView();
        screenSize = reinterpret_cast<GLViewImpl*>(glview)->getMonitorSize();
    }
    
    return screenSize;
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
        //glview = GLViewImpl::createWithRect("TrenchWars", cocos2d::Rect(0, 0, mediumResolutionSize.width, mediumResolutionSize.height));
        glview = GLViewImpl::create("TrenchWars");
#else
        glview = GLViewImpl::create("TrenchWars");
#endif
        director->setOpenGLView(glview);
    }

    Size frameSize = getDeviceScreenSize();
    frameSize = frameSize * .8; //FIXME: lol
    // turn on display FPS
    director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0f / 120);
    director->setDepthTest(true);
        
    int retina = glview->getRetinaFactor();
    int scale = glview->getScaleX();
    Rect viewRect = glview->getViewPortRect();

    // Set the design resolution
    glview->setDesignResolutionSize(frameSize.width, frameSize.height, ResolutionPolicy::EXACT_FIT);
    glview->setFrameSize(frameSize.width, frameSize.height);
    register_all_packages();

    auto sharedFileUtils = FileUtils::getInstance();
    std::string ret;
    sharedFileUtils->purgeCachedEntries();
    std::vector<std::string> searchPaths = sharedFileUtils->getOriginalSearchPaths();
    searchPaths.push_back("/animations");
    searchPaths.push_back("/definitions");
    searchPaths.push_back("/fonts");
    searchPaths.push_back("/images");
    searchPaths.push_back("/other");
    searchPaths.push_back("/particles");
    searchPaths.push_back("/sounds");
    searchPaths.push_back("/tiles");
    sharedFileUtils->setSearchPaths(searchPaths);
    
    
    TrenchWarsManager * twManager = new TrenchWarsManager(true);

    auto scene = MainMenuScene::createScene();

    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. Note, when receiving a phone call it is invoked.
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

#if USE_AUDIO_ENGINE
    AudioEngine::pauseAll();
#elif USE_SIMPLE_AUDIO_ENGINE
    SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    SimpleAudioEngine::getInstance()->pauseAllEffects();
#endif
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

#if USE_AUDIO_ENGINE
    AudioEngine::resumeAll();
#elif USE_SIMPLE_AUDIO_ENGINE
    SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    SimpleAudioEngine::getInstance()->resumeAllEffects();
#endif
}
