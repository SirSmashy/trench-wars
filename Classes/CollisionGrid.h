#ifndef __COLLISION_GRID_H__
#define __COLLISION_GRID_H__
//
//  TWCollisionGrid.h
//  TrenchWars
//
//  Created by Paul Reed on 2/25/12.
//  Copyright (c) 2012 . All rights reserved.
//
#include "cocos2d.h"
#include "CollisionCell.h"
#include "NavigationGrid.h"
#include "World.h"
#include "VectorMath.h"

USING_NS_CC;

#define WORLD_TO_GRAPHICS_SIZE       16
#define ONE_OVER_WORLD_TO_GRAPHICS_SIZE 0.0625 // 1 / WORLD_TO_GRAPHICS_SIZE


static unsigned short gridIds = 0;

enum COVER_DIRECTIONS
{
    COVER_NONE = 0,
    COVER_RIGHT_CANT_SEE = 1,
    COVER_RIGHT_CAN_SEE = 2,
    COVER_UP_CANT_SEE = 4,
    COVER_UP_CAN_SEE = 8,
    COVER_LEFT_CANT_SEE = 16,
    COVER_LEFT_CAN_SEE = 32,
    COVER_DOWN_CANT_SEE = 64,
    COVER_DOWN_CAN_SEE = 128,
};

class MovingPhysicalEntity;
class Entity;
class Team;

class CollisionGrid : public Ref
{
private:
    CollisionCell *** _collisionGrid;
    Size _gridSize;
    Rect _worldBounds;
    int _gridOffset;
    unsigned short _gridId;
    
    bool _shouldDrawGrid;
    
    Vec2 translateToGridReferenceFrame(Vec2 vec);
    
public:
    CollisionGrid(Size size, int positionOffset);
    
    Size getGridSize() {return _gridSize;}
    NavigationGrid * getNavigationGrid();  
    unsigned short getGridId() {return _gridId;}

    bool iterateCellsInLine(Vec2 start, Vec2 end, const std::function<bool (CollisionCell * cell, float distanceTraveled, bool minorCell)> & iteratorFunction);
    
    bool iterateCellsInCircle(Vec2 center, float radius, const std::function<bool (CollisionCell * cell)> & iteratorFunction);

    void cellsInLine(Vec2 start, Vec2 end, std::vector<CollisionCell *> * cellArray);
    void cellsInCircle(Vec2 origin, float radius, std::vector<CollisionCell *> * cellArray);
    void cellsInRect(Rect rect, std::vector<CollisionCell *> * cellArray);
    void cellsInRect(Vec2 start, Vec2 end, std::vector<CollisionCell *> * cellArray);
    void cellsInPolygon(std::vector<Vec2> & polygon, std::vector<CollisionCell *> * cellArray);
    
    double getCoverAndHeightValueForCell(CollisionCell * cell, CollisionCell * center);
    double findCoverValueInRect(Rect area, CollisionCell * center, Team * team);
    bool canSeeAroundCoverAtPosition(CollisionCell * cell, DIRECTION directionToLook);

    int constrainXIndex(int index);
    int constrainYIndex(int index);
    Vec2 constrainPosition(Vec2 position);
    bool constrainLine(Vec2 & start, Vec2 & end);
        
    inline bool cellIndexValid(int x, int y) {
        return x >= 0 && x < _gridSize.width && y >= 0 && y < _gridSize.height;
    }
    
    inline CollisionCell * getCell(int x, int y) {
        if(cellIndexValid(x, y))
        {
            return _collisionGrid[x][y];
        }
        return theInvalidCell;
    }
    
    inline CollisionCell * getCellUnsafe(int x, int y) {
        return _collisionGrid[x][y];
    }
    
    
    CollisionCell * getCellForCoordinates(Vec2 coordinates);
    Rect getWorldBounds() {return _worldBounds;}
    Vec2 getValidMoveCellAroundPosition(Vec2 position, int searchRadius = 10);
        
    void shutdown();
};




#endif //__COLLISION_GRID_H__
