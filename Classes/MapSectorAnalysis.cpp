//
//  MapSectorAnalysis.m
//  Trench Wars
//
//  Created by Paul Reed on 6/2/14.
//  Copyright (c) 2014 Paul Reed. All rights reserved.
//

#include "MapSectorAnalysis.h"
#include "EngagementManager.h"
#include "MapSector.h"
#include "SectorGrid.h"
#include "AICommand.h"
#include "PrimativeDrawer.h"
#include "GameVariableStore.h"
#include "MultithreadedAutoReleasePool.h"
#include "TeamManager.h"


MapSectorAnalysis::MapSectorAnalysis()
{
    
}

bool MapSectorAnalysis::init(MapSector * sector, AICommand * command)
{
    _sector = sector;
    _owningCommand = command;
    
    _friendlyStrength = 0;
    _friendlyFirePower = 0;
    _friendlyArtilleryPower = 0;
    _friendlyProjectedStrength = 0; //units that are moving through or to this sector
    _friendlyCommandPostPresent = false;
    _inControlRadiusStatus = 1; //outside control radius
    _inCommandPostBuildRadius = false;
    
    
    _enemyStrength = 0;
    _enemyFirePower = 0;
    _enemyArtilleryPower = 0;
    _enemyProjectedStrength = 0; //units that are moving through or to this sector
    _enemyCommandPostPresent = false;
    
    
    _defendGoalPotential = 0;
    _attackGoalPotential = 0;
    _buildCommandPostGoalPotential = 0;
    _bombardGoalPotential = 0;
    _fortifyGoalPotential = 0;
    _retreatFromGoalPotential = 0;
    _danger = 0;
    
//    _primativeDrawer = PrimativeDrawer::create();
    
    globalEngagementManager->requestMainThreadFunction([this,sector] () {
        distanceFromPrimaryCommandPost = _owningCommand->getPrimaryCommandPost()->getPosition().distance(sector->getSectorOrigin());
        //we always know where the enemy primary command posts are
        distanceFromNearestEnemyCommandPost = 9999999;
        for(auto team : globalTeamManager->getTeams())
        {
            for(auto command : team->getCommands())
            {
                if(command == _owningCommand)
                    continue;
                
                double distance = command->getPrimaryCommandPost()->getPosition().distance(sector->getSectorOrigin());
                if(distance < distanceFromNearestEnemyCommandPost)
                    distanceFromNearestEnemyCommandPost = distance;
            }
            
        }
    });
    

    return true;
}

MapSectorAnalysis * MapSectorAnalysis::create(MapSector * sector, AICommand * command)
{
    MapSectorAnalysis * analysis = new MapSectorAnalysis();
    if(analysis->init(sector, command))
    {
        globalAutoReleasePool->addObject(analysis);
        return analysis;
    }
    CC_SAFE_DELETE(analysis);
    return nullptr;
}



void MapSectorAnalysis::determineTraversability()
{
    bool blockedIndexes [globalSectorGrid->getCellsInSector()] [globalSectorGrid->getCellsInSector()];
    bool coverIndexes [globalSectorGrid->getCellsInSector()] [globalSectorGrid->getCellsInSector()];
    
    CollisionCell * cell;
    //vertical
    for(int y = 0; y < globalSectorGrid->getCellsInSector(); y++)
    {
        for(int x = 0; x < globalSectorGrid->getCellsInSector(); x++)
        {
            blockedIndexes[x][y] = false;
            coverIndexes[x][y] = false;
            
            cell = _sector->getCell(x, y);
            
            
            if(!cell->isTraversable())
            {
                //if this cell is along the top edge of the sector then it automatically blocks
                if(y == 0)
                {
                    blockedIndexes[x][y] = true;
                }
                else
                {//if its not along the top edge, then check to see if the cell above this, or the cell left of this is blocked and if so, then this is blocking too
                    if(blockedIndexes[x][y-1])
                    {
                        blockedIndexes[x][y] = true;
                    }
                    else if(x > 0 && blockedIndexes[x-1][y])
                    {
                        blockedIndexes[x][y] = true;
                    }
                }
                if(blockedIndexes[x][y] && y == globalSectorGrid->getCellsInSector()-1)
                {
                    //_verticalFlow = 0;
                }
            }
        }
    }
}

void MapSectorAnalysis::setEnemyCommandPostPresent(bool commandPostPresent)
{
    _enemyCommandPostPresent = commandPostPresent;
}

void MapSectorAnalysis::setEnemyStrength(int enemyStrength)
{
    //FIXME: magic numbers
    _danger -= (_enemyStrength * 10); //remove the old value
    _enemyStrength = enemyStrength;
    _danger += enemyStrength * 10; //add the new value
}

void MapSectorAnalysis::setEnemyFirePower(int enemyFirePower)
{
    _danger -= (_enemyFirePower * 10); //remove the old value
    _enemyFirePower = enemyFirePower;
    _danger += enemyFirePower * 10; //add the new value

}

void MapSectorAnalysis::setInControlRadiusStatus(int inControlRadiusStatus)
{
    _inControlRadiusStatus = inControlRadiusStatus;
    if(_inControlRadiusStatus != 1)
    {
        distanceFromNearestCommandPost = 9999999;
        double distance;
        for(CommandPost * commandsPost : _owningCommand->getCommandPosts())
        {
            distance = commandsPost->getPosition().distance(_sector->getSectorOrigin());
            if(distance < distanceFromNearestCommandPost)
            {
                distanceFromNearestCommandPost = distance;
            }
        }
        
        distanceFromNearestEnemyCommandPost = 9999999;
        for(auto team : globalTeamManager->getTeams())
        {
            for(auto command : team->getCommands())
            {
                if(command == _owningCommand)
                    continue;
                for(CommandPost * commandsPost : command->getCommandPosts())
                {
                    distance = commandsPost->getPosition().distance(_sector->getSectorOrigin());
                    
                    if(distance < distanceFromNearestEnemyCommandPost)
                    {
                        distanceFromNearestEnemyCommandPost = distance;
                    }
                }
            }
        }
    }
}


void MapSectorAnalysis::setInCommandPostBuildRadius(bool inCommandPostBuildRadius)
{
    _inCommandPostBuildRadius = inCommandPostBuildRadius;
}



void MapSectorAnalysis::performAnalysis()
{
    float previousOccupy = _defendGoalPotential;
    float previousAttack = _attackGoalPotential;
    float previousBuild = _buildCommandPostGoalPotential;
    float previousFority = _fortifyGoalPotential;
    
    float mapSize = (world->getCollisionGrid()->getGridSize().width);
    
    //// Percent distance of the total map size
    float enemyCommandPostValue = (distanceFromNearestEnemyCommandPost / mapSize);
    if(enemyCommandPostValue < 0)
        enemyCommandPostValue = 0;
    
    _primativeDrawer->clear();
    
    //BUILD COMMAND POST
    if(_inCommandPostBuildRadius && _inControlRadiusStatus != -1 && distanceFromNearestEnemyCommandPost > 1500)
    {
        _buildCommandPostGoalPotential = (1 - enemyCommandPostValue) * 100;
    }
    else
    {
        _buildCommandPostGoalPotential = 0;
    }
    
    if(_inControlRadiusStatus == 1)
    {
        _defendGoalPotential = 0;
        _attackGoalPotential = 0;
        _fortifyGoalPotential = 0;
    }
    else
    {
        //percent of the command post radius distance from nearest friendly command post
        float nearestCommandPostPercentDistance =  1 - ((distanceFromNearestCommandPost) / globalVariableStore->getVariable(CommandPostDefaultRadius));
        if(nearestCommandPostPercentDistance < 0)
        {
            nearestCommandPostPercentDistance = 0;
        }
        nearestCommandPostPercentDistance = powf(nearestCommandPostPercentDistance,2.5);
        
        //distance from primary command post
        float primaryCommandPostPercentDistance =  1 - ((distanceFromPrimaryCommandPost) / mapSize);
        primaryCommandPostPercentDistance = powf(primaryCommandPostPercentDistance,2.5);

        //DEFEND Potential calculate
        //This calculation involves how close this point is to the primary command post, fortifications present, and the presence of enemy power
        if(_enemyFirePower <= 0)
            _defendGoalPotential = 0;
        else
        {
            _defendGoalPotential = (5 * _enemyFirePower) - (_enemyFirePower * _enemyFirePower); //As enemy firepower increases toward (5/2) the defend potential increases, once past (5/2) the potential decreases (no point in defeneding vs overwhelming force?)
            _defendGoalPotential *= (1 + _sector->getTotalSecurity()) / globalSectorGrid->getCellsInSector(); // less cover results in lower potential
            _defendGoalPotential *= ((1 - primaryCommandPostPercentDistance) * 100); // closer to the primariry results in higher potential
        }
        
        //ATTACK
        if(_enemyCommandPostPresent)
        {
            _attackGoalPotential = 100;
        }
        else if(_enemyStrength > 0)
        {
            _attackGoalPotential = (1 - nearestCommandPostPercentDistance) * 100;
        }
        else
        {
            _attackGoalPotential = 0;
        }
        //FORTIFY
        float distance = ( (1-enemyCommandPostValue) - nearestCommandPostPercentDistance);
        _fortifyGoalPotential = (distance) * 100;
    }
    if(previousFority != _fortifyGoalPotential)
        _owningCommand->changeToSectorAnalysis(this, FORITY);
    if(previousOccupy != _defendGoalPotential)
        _owningCommand->changeToSectorAnalysis(this, OCCUPY);
    if(previousAttack != _attackGoalPotential)
        _owningCommand->changeToSectorAnalysis(this, ATTACK);
    if(previousBuild != _buildCommandPostGoalPotential)
        _owningCommand->changeToSectorAnalysis(this, BUILD_COMMAND_POST);

    Color4F color;
    if(_buildCommandPostGoalPotential > 0)
        color = Color4F::GREEN;
    else
        color = Color4F::RED;

    _primativeDrawer->clear();
    _primativeDrawer->drawSegment(_sector->getSectorOrigin(), Vec2(_sector->getSectorOrigin().x + _sector->getSectorBounds().size.width, _sector->getSectorOrigin().y), 2, color);
    
    _primativeDrawer->drawSegment(_sector->getSectorOrigin(), Vec2(_sector->getSectorOrigin().x, _sector->getSectorOrigin().y + _sector->getSectorBounds().size.height), 2, color);
}
