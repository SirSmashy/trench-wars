//
//  EntityManager.cpp
//  TrenchWars
//
//  Created by Paul Reed on 12/28/22.
//

#include "EntityManager.h"
#include "EngagementManager.h"
#include "BackgroundTaskHandler.h"
#include "InteractiveObjectUtils.h"
#include "FixedStreamBuf.h"


EntityManager * globalEntityManager;

EntityManager::EntityManager()
{
    globalEntityManager = this;
    

    _entityTypes.try_emplace(INFANTRY);
    _entityTypes.try_emplace(COURIER);
    _entityTypes.try_emplace(CREW_WEAPON);
    _entityTypes.try_emplace(COMMANDPOST);
    _entityTypes.try_emplace(BUNKER);
    _entityTypes.try_emplace(PROJECTILE);
    _entityTypes.try_emplace(UNIT);
    _entityTypes.try_emplace(CREW_WEAPON_UNIT);
    _entityTypes.try_emplace(STATIC_ENTITY);
    _entityTypes.try_emplace(PROPOSED_TRENCH);
    _entityTypes.try_emplace(PROPOSED_COMMAND_POST);
    _entityTypes.try_emplace(PROPOSED_BUNKER);
    _entityTypes.try_emplace(OBJECTIVE_ENTITY);
    _entityTypes.try_emplace(DEBUG_ENTITY);
    _entityTypes.try_emplace(OTHER);
    _queryANext = false;
}

void EntityManager::query(float deltaTime)
{
    _queryVector.clear();
    if(_queryANext)
    {
        _queryVector.reserve(_queryingEntitiesA.size());
        for(auto & entEntry : _queryingEntitiesA)
        {
            _queryVector.push_back(entEntry.second);
        }
    }
    else
    {
        _queryVector.reserve(_queryingEntitiesB.size());
        for(auto & entEntry : _queryingEntitiesB)
        {
            _queryVector.push_back(entEntry.second);
        }
    }
    _queryANext = !_queryANext;


    int entsPerThread = ceil( (float) _queryVector.size() / globalBackgroundTaskHandler->getThreadCount());
    int threads = globalBackgroundTaskHandler->getThreadCount();
    if(entsPerThread == 0)
    {
        threads = 1;
        entsPerThread = _queryVector.size();
    }
    globalEngagementManager->_tasksRunning.store(threads);
    
    for(int i = 0; i < threads; i++)
    {
        globalBackgroundTaskHandler->addBackgroundTask( [this, i, deltaTime, entsPerThread] () {
            int start = i * entsPerThread;
            int end = start + entsPerThread;
            if(end > _queryVector.size())
            {
                end = _queryVector.size();
            }
            for(int j = start; j < end; j++)
            {
                _queryVector[j]->query(deltaTime);
            }
            globalEngagementManager->_tasksRunning--;
            return false;
        });
    }
}


void EntityManager::act(float deltaTime)
{
    /////////////  ACT  //////////////
    //////////////////////////////////////////
    _actVector.clear();
    _actVector.reserve(_actingEntities.size());
    for(auto & entEntry : _actingEntities)
    {
        _actVector.push_back(entEntry.second);
    }
    int entsPerThread = ceil( (float) _actVector.size() / globalBackgroundTaskHandler->getThreadCount());
    int threads = globalBackgroundTaskHandler->getThreadCount();
    if(entsPerThread == 0)
    {
        threads = 1;
        entsPerThread = _actVector.size();
    }
    globalEngagementManager->_tasksRunning.store(threads);
    
    for(int i = 0; i < threads; i++)
    {
        globalBackgroundTaskHandler->addBackgroundTask( [this, i, deltaTime, entsPerThread] () {
            int start = i * entsPerThread;
            int end = start + entsPerThread;
            if(end > _actVector.size())
            {
                end = _actVector.size();
            }
            for(int j = start; j < end; j++)
            {
                _actVector[j]->act(deltaTime);
            }
            globalEngagementManager->_tasksRunning--;
            return false;
        });
    }
}

void EntityManager::update(float deltaTime)
{
    _entitiesJustAdded.clear();
    _entitiesJustRemoved.clear();
    for(Entity * ent : _entitiesToAdd)
    {
        if(ent->shouldQuery())
        {
            if(_queryingEntitiesA.size() <= _queryingEntitiesB.size())
            {
                _queryingEntitiesA.emplace(ent->uID(), ent);
            }
            else
            {
                _queryingEntitiesB.emplace(ent->uID(), ent);
            }
        }
        if(ent->shouldAct())
        {
            _actingEntities.emplace(ent->uID(), ent);
        }
        ent->postLoad();
        _entities.insert(ent->uID(), ent);
        if(ent->uID() == -1)
        {
            LOG_DEBUG_ERROR("WTF NO!!!! \n");
        }
        if(ent->isNetworkEntity())
        {
            _entitiesJustAdded.insert(ent->uID());
        }
        _entityTypes[ent->getEntityType()].insert(ent);
    }
    _entitiesToAdd.clear();
    
    for(Entity * ent : _entitiesToRemove)
    {
        if(ent->isNetworkEntity())
        {
            _entitiesJustRemoved.insert(ent->uID());
        }
        _queryingEntitiesA.erase(ent->uID());
        _queryingEntitiesB.erase(ent->uID());
        _actingEntities.erase(ent->uID());
        _entityTypes[ent->getEntityType()].erase(ent);
        _entities.erase(ent->uID());
        
    }
    _entitiesToRemove.clear();
}

Entity * EntityManager::getEntity(ENTITY_ID entId)
{
    return _entities.at(entId);
}

void EntityManager::registryEntity(Entity * ent)
{
    _entitiesMutex.lock();
    _entitiesToAdd.pushBack(ent);
    _entitiesMutex.unlock();
}

void  EntityManager::removeEntity(Entity * ent)
{
    _entitiesMutex.lock();
    _entitiesToRemove.pushBack(ent);
    _entitiesMutex.unlock();
}


void EntityManager::serializeEntitiesOfType(cereal::BinaryOutputArchive & archive, ENTITY_TYPE type)
{
    size_t totalCount = 0;
    
    auto & typeSet = _entityTypes[type];
    
    std::list<Entity *> entsToSave;
    for(auto ent : typeSet)
    {
        entsToSave.push_back(ent);
        totalCount++;
    }
    
    archive(totalCount);
    
    
    for(auto ent : entsToSave)
    {
        ENTITY_TYPE type = ent->getEntityType();
        archive(ent->uID());
        archive(ent->getEntityType());
    }
    
    for(auto ent : entsToSave)
    {
        ent->saveToArchive(archive);
    }
}



void EntityManager::serializeEntities(cereal::BinaryOutputArchive & archive, bool includeProjectiles)
{
    size_t totalCount = 0;
    std::list<Entity *> entsToSave;
    for(auto entPair : _entities)
    {
        if(includeProjectiles || entPair.second->getEntityType() != PROJECTILE)
        {
            entsToSave.push_back(entPair.second);
            totalCount++;
        }
    }
    
    archive(totalCount);
    for(auto ent : entsToSave)
    {
        archive(ent->uID());
        archive(ent->getEntityType());
    }
    
    for(auto ent : entsToSave)
    {
        ent->saveToArchive(archive);
    }
}


void EntityManager::serializeEntityQueryChanges(cereal::BinaryOutputArchive & archive)
{
    size_t totalEntsUpdated = 0;
    
    std::vector<Entity *> entsToUpdate;
    std::unordered_map<ENTITY_ID, Entity *> * queryMap;
    if(!_queryANext)
    {
        queryMap = &_queryingEntitiesA;
    }
    else
    {
        queryMap = &_queryingEntitiesB;
    }
    
    entsToUpdate.reserve(queryMap->size());

    for(auto & entEntry : *queryMap)
    {
        if(entEntry.second->getEntityType() != PROJECTILE)
        {
            int count = entEntry.second->hasUpdateToSendOverNetwork();
            if(count > 0)
            {
                entsToUpdate.push_back(entEntry.second);
                totalEntsUpdated ++;
            }
        }
    }
    
    archive(totalEntsUpdated);
    int i = 0;
    for(auto ent : entsToUpdate)
    {
        archive(ent->uID());
        archive(i);
        archive(ent->getEntityType());
        ent->saveNetworkUpdate(archive);
        i++;
    }
}

void EntityManager::serializeEntityAdditionsAndRemovals(cereal::BinaryOutputArchive & archive)
{
    long long frameCount = globalEngagementManager->getFrameCount();
    archive(frameCount);
    archive(_entitiesJustAdded.size());

    for(auto entId : _entitiesJustAdded)
    {
        Entity * ent = _entities.at(entId);
        archive(entId);
        archive(ent->getEntityType());
    }
    for(auto entId : _entitiesJustAdded)
    {
        Entity * ent = _entities.at(entId);
        ent->saveToArchive(archive);
    }
    archive(_entitiesJustRemoved.size());
    for(auto entId : _entitiesJustRemoved)
    {
        archive(entId);
    }
}


void EntityManager::deserializeEntityQueryChanges(cereal::BinaryInputArchive & archive)
{
    size_t entsUpdated;
    archive(entsUpdated);
    int test;
    ENTITY_TYPE type;
    for(int i = 0; i < entsUpdated; i++)
    {
        ENTITY_ID entId;
        archive(entId);
        archive(test);
        archive(type);
        
        Entity * ent = _entities.at(entId);
        if(ent == nullptr || type != ent->getEntityType())
        {
            printf("SHIT! %lld \n", entId);
        }
        ent->updateFromNetwork(archive);
    }
}

void EntityManager::deserializeEntityAdditionsAndRemovals(cereal::BinaryInputArchive & archive)
{
    // New ents added this frame
    for(int i = 0; i < (QUERY_EVERY_X_FRAME); i++)
    {
        std::list<Entity *> allEnts;
        long long frameCount;
        size_t count;
        archive(frameCount);
        archive(count);
        ENTITY_ID entId;
        ENTITY_TYPE type;
        
        for(int j = 0; j < count; j++)
        {
            archive(entId);
            archive(type);
            
            Entity * newEnt = _entities.at(entId); // This had better be null
            if(newEnt == nullptr)
            {
                newEnt = InteractiveObjectUtils::createEntityOfType(type);
                newEnt->setID(entId);
                _entities.insert(newEnt->uID(), newEnt);
            }
            allEnts.push_back(newEnt);
        }
        
        for(auto ent : allEnts)
        {
            ent->loadFromArchive(archive);
        }
        archive(count);
        for(int j = 0; j < count; j++)
        {
            archive(entId);
            Entity * removalEnt = _entities.at(entId);
            removalEnt->removeFromGame();
        }
    }
}



void EntityManager::deserializeEntities(cereal::BinaryInputArchive & archive)
{
    size_t size;
    archive(size);
    ENTITY_ID entId;
    ENTITY_TYPE type;
    std::list<Entity *> allEnts;
    
    for(int i = 0; i < size; i++)
    {
        archive(entId);
        archive(type);
        Entity * newEnt = _entities.at(entId);
        if(newEnt == nullptr)
        {
            newEnt = InteractiveObjectUtils::createEntityOfType(type);
            newEnt->setID(entId);
            _entities.insert(newEnt->uID(), newEnt);
        }
        allEnts.push_back(newEnt);
    }
    
    for(auto ent : allEnts)
    {
        ent->loadFromArchive(archive);
    }
}

void EntityManager::shutdown()
{
    for(const auto & entEntry : _entities)
    {
        entEntry.second->removeFromGame();
    }
    _entities.clear();
    _entitiesToAdd.clear();
    _entitiesToRemove.clear();
}
