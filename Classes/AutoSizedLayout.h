//
//  AutoSizedLayout.h
//  TrenchWars
//
//  Created by Paul Reed on 9/27/20.
//

#ifndef AutoSizedLayout_h
#define AutoSizedLayout_h

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::ui;
//forward declations


class AutoSizedLayout : public Layout
{
protected:
    virtual bool init() override;
    void updateContentSize();

public:
    AutoSizedLayout();
    static AutoSizedLayout * create();
    virtual void doLayout() override;
};


#endif /* AutoSizedLayout_h */
