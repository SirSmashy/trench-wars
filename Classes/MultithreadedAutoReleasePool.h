//
//  MultithreadedAutoReleasePool.hpp
//  TrenchWars
//
//  Created by Paul Reed on 6/28/22.
//

#ifndef MultithreadedAutoReleasePool_h
#define MultithreadedAutoReleasePool_h

#include "concurrentqueue.h"
#include "Refs.h"
#include <mutex>

class MultithreadedAutoReleasePool
{
    moodycamel::ConcurrentQueue<Ref *> _objectsPool;    
public:
    
    MultithreadedAutoReleasePool();
    void addObject(Ref * object);
    void clearPool();
};

extern MultithreadedAutoReleasePool * globalAutoReleasePool;

#endif /* MultithreadedAutoReleasePool_h */
