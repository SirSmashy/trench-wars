//
//  AutoSizedLayout.cpp
//  TrenchWars
//
//  Created by Paul Reed on 9/27/20.
//

#include "AutoSizedLayout.h"


AutoSizedLayout::AutoSizedLayout() : Layout()
{
    
}

bool AutoSizedLayout::init()
{
    Layout::init();

    return true;
}

AutoSizedLayout * AutoSizedLayout::create()
{
    AutoSizedLayout * newMenu = new AutoSizedLayout();
    if(newMenu->init())
    {
        newMenu->autorelease();
        return newMenu;
    }
    
    CC_SAFE_DELETE(newMenu);
    return nullptr;
}


void AutoSizedLayout::doLayout()
{
    bool layoutWasDirty = _doLayoutDirty;
    Layout::doLayout();
    updateContentSize();

//    if(layoutWasDirty)
//    {
//        _doLayoutDirty = false;
//    }
}


void AutoSizedLayout::updateContentSize()
{
    Rect contentBounds;
    Rect childBounds;
    
    
    int minY = 10000000000;
    int minX = 10000000000;
    
    if(getChildren().size() == 0)
    {
        setContentSize(Size(0,0));
        return;
    }
    
    for(Node * child : getChildren())
    {
        childBounds = child->getBoundingBox();
        
        Widget * widget = dynamic_cast<Widget*>(child);
        if(widget)
        {
            LayoutParameter * params = widget->getLayoutParameter();
            if(params != nullptr)
            {
                Margin margin = params->getMargin();
                childBounds.origin.x -= margin.left;
                childBounds.origin.y -= margin.bottom;
                childBounds.size.width += margin.left + margin.right;
                childBounds.size.height += margin.bottom + margin.top;
            }

        }
        
        if(childBounds.origin.x < minX)
        {
            minX = childBounds.origin.x;
        }
        if(childBounds.origin.y < minY)
        {
            minY = childBounds.origin.y;
        }
        
        if(contentBounds.size.width == 0 && contentBounds.size.height == 0)
        {
            contentBounds = childBounds;
        }
        else
        {
            contentBounds.merge(childBounds);
        }
    }
    
    
    setContentSize(contentBounds.size);
    
    if(_colorRender != nullptr)
    {
        _colorRender->setPosition(Vec2(minX,minY));
    }
    if(_gradientRender != nullptr)
    {
        _gradientRender->setPosition(Vec2(minX,minY));
    }
    if(_backGroundImage != nullptr)
    {
        _backGroundImage->setPosition(Vec2(minX,minY));
    }
}
