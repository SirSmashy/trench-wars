//
//  HistoricalViewSprite.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/25/24.
//

#include "HistoricalViewSprite.h"
#include "CollisionGrid.h"
#include "EngagementManager.h"
#include "Timing.h"

#define MAX_TEXTURE_SIZE 8192

HistoricalViewSprite * HistoricalViewSprite::create(Size worldSize, Size captureSize, int xCaptures, int yCaptures, Node * nodeToCapture)
{
    HistoricalViewSprite *sprite = new (std::nothrow) HistoricalViewSprite();
    if (sprite && sprite->init(worldSize,captureSize, xCaptures, yCaptures, nodeToCapture))
    {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}



bool HistoricalViewSprite::init(Size worldSize, Size captureSize, int xCaptures, int yCaptures, Node * nodeToCapture)
{
    _nodeToCapture = nodeToCapture;
    _updating = false;
    _capturingEntireWorld = false;
    _worldSize = worldSize * WORLD_TO_GRAPHICS_SIZE;
    _sectorSize = captureSize * WORLD_TO_GRAPHICS_SIZE;
    
    // Force texture to be power of 2
    int w = ccNextPOT(_worldSize.width * CC_CONTENT_SCALE_FACTOR());
    int h = ccNextPOT(_worldSize.height * CC_CONTENT_SCALE_FACTOR());
    
    if(w > MAX_TEXTURE_SIZE && w > h)
    {
        _textureToWorldScale = MAX_TEXTURE_SIZE / ((double) w);
    }
    else if(h > MAX_TEXTURE_SIZE)
    {
        _textureToWorldScale = MAX_TEXTURE_SIZE / ((double) h);
    }
    else
    {
        _textureToWorldScale = 1.0;
    }
    
    _textureToWorldScale = 1.0;
    _textureSize.width = w * _textureToWorldScale;
    _textureSize.height = h * _textureToWorldScale;

    
    auto dataLen = _textureSize.width * _textureSize.height * 4;
    void *data = malloc(dataLen);
    
    memset(data, 0, dataLen);
    
    _texture = new (std::nothrow) Texture2D();
    _texture->initWithData(data, dataLen, Texture2D::PixelFormat::RGBA8888, _textureSize.width, _textureSize.height, Size((float)w, (float)h));
    _texture->retain();
    
    _paperTexture = new (std::nothrow) Texture2D();
    Image * paper = new Image();
    paper->initWithImageFile("PaperTexture.jpg");
    _paperTexture->initWithImage(paper);
    _paperTexture->retain();
    _texture->setAlphaTexture(_paperTexture);

    
    Rect world;
    world.size = _worldSize;
    initWithTexture(_texture, world);

    setFlippedY(true);
    setBlendFunc( BlendFunc::ALPHA_PREMULTIPLIED );
    setOpacityModifyRGB(true);
    setGLProgramState(GLProgramState::getOrCreateWithGLProgramName(GLProgram::SHADER_NAME_POSITION_TEXTURE_COLOR_FADE_TO_GRAY));
    setZOrder(10);
    setContentSize(_worldSize);
    setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    setPositionZ(Z_POSITION_HISTORICAL_LANDSCAPE);
    
    // ////////////////////////////////////////////////////////////
    // Finally setup the texture used to capture a portion of the world
    // ////////////////////////////////////////////////////////////
    w = (int)(_sectorSize.width * CC_CONTENT_SCALE_FACTOR());
    h = (int)(_sectorSize.height * CC_CONTENT_SCALE_FACTOR());
    _scaledSectorSize = Size(w * _textureToWorldScale, h * _textureToWorldScale);

    GLint oldRBO;
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &_oldFBO);
    glGetIntegerv(GL_RENDERBUFFER_BINDING, &oldRBO);

    // generate FBO
    glGenFramebuffers(1, &_captureFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, _captureFBO);

        
    // associate texture with FBO
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _texture->getName(), 0);
    
    
    //create and attach depth buffer
    glGenRenderbuffers(1, &_captureDepthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _captureDepthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, (GLsizei)_textureSize.width, (GLsizei)_textureSize.height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _captureDepthRenderBuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, _captureDepthRenderBuffer);

    auto result = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    CCASSERT(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE, "Could not attach texture to framebuffer");

    
    glBindRenderbuffer(GL_RENDERBUFFER, oldRBO);
    glBindFramebuffer(GL_FRAMEBUFFER, _oldFBO);

    CC_SAFE_FREE(data);
    
    setupPolygon(xCaptures, yCaptures);

    
    return true;
}

void HistoricalViewSprite::setupPolygon(int captureAreasWidth, int captureAreasHeight)
{
    // we won't know the size of verts and indices until we process all of the triangles!
    std::vector<V3F_C4B_T2F> verts;
    std::vector<unsigned short> indices;
    
    int indexStart = 0;
    
    double xSize = captureAreasWidth;
    double ySize = captureAreasHeight;
    
    for(int y = 0; y <= captureAreasHeight; y++)
    {
        for(int x = 0; x <= captureAreasWidth; x++)
        {
            if(x == 0 || x == captureAreasWidth)
            {
                if(y == 0 || y == captureAreasHeight)
                {
                    _captureAreasPerVertex.emplace_back(1);
                }
                else
                {
                    _captureAreasPerVertex.emplace_back(2);
                }
            }
            else if(y == 0 || y == captureAreasHeight)
            {
                _captureAreasPerVertex.emplace_back(2);
            }
            else
            {
                _captureAreasPerVertex.emplace_back(4);
            }
            auto v3 = Vec3(x * _scaledSectorSize.width, y * _scaledSectorSize.height, 0);
            //vert does not exist yet, so we need to create a new one,
            auto c4b = Color4B::WHITE;
            c4b.a = 125;
            auto t2f = Tex2F(x / xSize, y / ySize); // don't worry about tex coords now, we calculate that later
            V3F_C4B_T2F vert = {v3,c4b,t2f};
            verts.push_back(vert);
        }
    }
    captureAreasWidth += 1;
    for(int y = 1; y <= captureAreasHeight; y++)
    {
        for(int x = 1; x <= captureAreasWidth-1; x++)
        {
            Vec2 position((x-1) * _scaledSectorSize.width * ONE_OVER_WORLD_TO_GRAPHICS_SIZE, (y-1) * _scaledSectorSize.height * ONE_OVER_WORLD_TO_GRAPHICS_SIZE);
            int index1 = ((y-1) * captureAreasWidth) + (x-1);
            int index2 = ((y-1) * captureAreasWidth) + (x);
            int index3 = ((y) * captureAreasWidth) + (x-1);
            int index4 = ((y) * captureAreasWidth) + (x);
            // 1st triangle for capture area
            indices.push_back(index1);
            indices.push_back(index2);
            indices.push_back(index3);

            // 2nd triangle for capture area
            indices.push_back(index2);
            indices.push_back(index4);
            indices.push_back(index3);
            
            _captureAreaToVertexIndicesMap.try_emplace(position, std::make_tuple(index1, index2, index3, index4));
        }
    }

    // now that we know the size of verts and indices we can create the buffers
    V3F_C4B_T2F* vertsBuf = new (std::nothrow) V3F_C4B_T2F[verts.size()];
    memcpy(vertsBuf, verts.data(), verts.size() * sizeof(V3F_C4B_T2F));
    
    unsigned short* indicesBuf = new (std::nothrow) unsigned short[indices.size()];
    memcpy(indicesBuf, indices.data(), indices.size() * sizeof(short));
    
    // Triangles should really use std::vector and not arrays for verts and indices.
    // Then the above memcpy would not be necessary
    TrianglesCommand::Triangles triangles = { vertsBuf, indicesBuf, static_cast< int>(verts.size()), static_cast< int>(indices.size()) };
    
    PolygonInfo polygon;
    polygon.setTriangles(triangles);
    setPolygonInfo(polygon);
}

void HistoricalViewSprite::updateVisibility(std::unordered_map<Vec2, unsigned char, Vec2Hash, Vec2Compare> & captureAreaVisibilityMap)
{
    std::vector<int> historyPercent;
    for(int i = 0; i < _polyInfo.triangles.vertCount; i++)
    {
        historyPercent.push_back(0);
    }
    
    for(auto area : captureAreaVisibilityMap)
    {
        auto captureAreaVerts = _captureAreaToVertexIndicesMap.find(area.first);
        if(captureAreaVerts != _captureAreaToVertexIndicesMap.end())
        {
            historyPercent[std::get<0>(captureAreaVerts->second)] += area.second;
            historyPercent[std::get<1>(captureAreaVerts->second)] += area.second;
            historyPercent[std::get<2>(captureAreaVerts->second)] += area.second;
            historyPercent[std::get<3>(captureAreaVerts->second)] += area.second;
        }
    }
    for(int i = 0; i < _polyInfo.triangles.vertCount; i++)
    {
        historyPercent[i] /= _captureAreasPerVertex[i];
    }
    for(int i = 0; i < _polyInfo.triangles.vertCount; i++)
    {
        _polyInfo.triangles.verts[i].colors.a = historyPercent[i];
    }
}

void HistoricalViewSprite::captureEntireWorld()
{
//    MicroSecondSpeedTimer timer( [=](long long int time) {
//        LOG_DEBUG("  Total time (ms): %f \n", (double)time/1000.0);
//    });
    if(_textureSize.width >= _textureSize.height)
    {
        _captureStartPosition.x = 0;
        _captureStartPosition.y = -(_textureSize.width - _textureSize.height) / 2;
    }
    else
    {
        _captureStartPosition.x = -(_textureSize.height - _textureSize.width) / 2;
        _captureStartPosition.y = 0;
    }
    
    _capturingEntireWorld = true;
    _updating = true;
    
    Camera::setVisitingCamera(nullptr);
    Vec2 oldPosition = _nodeToCapture->getPosition();
    float oldScale = _nodeToCapture->getScale();
    
    _nodeToCapture->setPosition(Vec2::ZERO);
    _nodeToCapture->setScale(1.0);
    
    
    Size halfSize = _worldSize / 2;
    double w = halfSize.width > halfSize.height ? halfSize.width : halfSize.height;
    Mat4 imageProjection;
    imageProjection.set(1, 0, 0, -halfSize.width,
                        0, 1, 0, -halfSize.height,
                        0, 0, -1, .1,
                        0, 0, .05, w + 1);
    
    Vec4 test1(0,0,1,1);
    Vec4 test2(_worldSize.width, _worldSize.height,1,1);
    
    imageProjection.transformVector(&test1);
    imageProjection.transformVector(&test2);
    
    test1 = test1 / test1.w;
    test2 = test2 / test2.w;
    
    
    auto director = Director::getInstance();
    director->pushProjectionMatrix(0);
    director->loadProjectionMatrix(imageProjection, 0);
    
    
    begin();
    _nodeToCapture->visit();
    end();
    
    director->getRenderer()->render();
    director->popProjectionMatrix(0);
    
    
    _nodeToCapture->setPosition(oldPosition);
    _nodeToCapture->setScale(oldScale);
    _updating = false;
}

void HistoricalViewSprite::captureWorldAtPoint(Vec2 point)
{
    _captureQueue.push(point);
}


void HistoricalViewSprite::update()
{
    if(_captureQueue.size() > 0)
    {
        doCaptureWorldAtPoint(_captureQueue.front());
        _captureQueue.pop();
    }
}


void HistoricalViewSprite::doCaptureWorldAtPoint(Vec2 point)
{
//    LOG_DEBUG("Capture: \n");
//    MicroSecondSpeedTimer timer( [=](long long int time) {
//        LOG_DEBUG("  Total time (ms): %f \n", (double)time/1000.0);
//    });
    _updating = true;
    _capturingEntireWorld = false;
    Size halfSize = _scaledSectorSize / 2;
    _captureStartPosition.x = point.x * (WORLD_TO_GRAPHICS_SIZE * _textureToWorldScale);
    _captureStartPosition.y = point.y * (WORLD_TO_GRAPHICS_SIZE * _textureToWorldScale);
    
    auto director = Director::getInstance();
    
        
    Camera::setVisitingCamera(nullptr);
    Vec2 oldPosition = _nodeToCapture->getPosition();
    float oldScale = _nodeToCapture->getScale();
    
    _nodeToCapture->setPosition(-point * WORLD_TO_GRAPHICS_SIZE);
    _nodeToCapture->setScale(1.0);
    
    Mat4 imageProjection;
    imageProjection.set(1, 0, 0, -halfSize.width,
                        0, 1, 0, -halfSize.height,
                        0, 0, -1, .1,
                        0, 0, .05, halfSize.width + 1);
    
    
    director->pushProjectionMatrix(0);
    director->loadProjectionMatrix(imageProjection, 0);

    
    begin();
    _nodeToCapture->visit();
    end();
    
    director->getRenderer()->render();
    director->popProjectionMatrix(0);

    
    _nodeToCapture->setPosition(oldPosition);
    _nodeToCapture->setScale(oldScale);
    _updating = false;
}

void HistoricalViewSprite::begin()
{
    Director* director = Director::getInstance();
    CCASSERT(nullptr != director, "Director is null when setting matrix stack");
    
    director->pushMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_PROJECTION);
    _projectionMatrix = director->getMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_PROJECTION);
    
    director->pushMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
    _transformMatrix = director->getMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
    
    _groupCommand.init(_globalZOrder);
    
    Renderer *renderer =  Director::getInstance()->getRenderer();
    renderer->addCommand(&_groupCommand);
    renderer->pushGroup(_groupCommand.getRenderQueueID());
    
    _beginCommand.init(_globalZOrder);
    _beginCommand.func = CC_CALLBACK_0(HistoricalViewSprite::onBegin, this);
    
    Director::getInstance()->getRenderer()->addCommand(&_beginCommand);
}

void HistoricalViewSprite::end()
{
    _endCommand.init(_globalZOrder);
    _endCommand.func = CC_CALLBACK_0(HistoricalViewSprite::onEnd, this);
    
    Director* director = Director::getInstance();
    CCASSERT(nullptr != director, "Director is null when setting matrix stack");
    
    Renderer *renderer = director->getRenderer();
    renderer->addCommand(&_endCommand);
    renderer->popGroup();
    
    director->popMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_PROJECTION);
    director->popMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
}



void HistoricalViewSprite::onBegin()
{
    //
    Director *director = Director::getInstance();
    
    _oldProjMatrix = director->getMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_PROJECTION);
    director->loadMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_PROJECTION, _projectionMatrix);
    
    _oldTransMatrix = director->getMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
    director->loadMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW, _transformMatrix);
    
    //calculate viewport
    // Viewport is where we draw the output rendering. 0,0 to 1024,1024 will draw the normalized device coordinates (-1 to 1) to 0,0 to 1024, 1024.
    Size viewPortSize;
    if(_capturingEntireWorld)
    {
        viewPortSize = _textureSize.width > _textureSize.height ? Size(_textureSize.width,_textureSize.width) : Size(_textureSize.height,_textureSize.height);
    }
    else
    {
        viewPortSize = _scaledSectorSize;
    }
    glViewport((GLsizei) _captureStartPosition.x, (GLsizei) _captureStartPosition.y, (GLsizei)viewPortSize.width, (GLsizei)viewPortSize.height);

    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &_oldFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, _captureFBO);
    
    if(!_capturingEntireWorld)
    {
        glEnable(GL_SCISSOR_TEST);
        glScissor(_captureStartPosition.x, _captureStartPosition.y, (GLsizei)viewPortSize.width, (GLsizei)viewPortSize.height);
        glClearColor(0, 0, 0, 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        glDisable(GL_SCISSOR_TEST);
    }

}

void HistoricalViewSprite::onEnd()
{
    Director *director = Director::getInstance();
    
    glBindFramebuffer(GL_FRAMEBUFFER, _oldFBO);
    
    // restore viewport
    director->setViewport();
    const auto& vp = Camera::getDefaultViewport();
    glViewport(vp._left, vp._bottom, vp._width, vp._height);
    //
    director->loadMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_PROJECTION, _oldProjMatrix);
    director->loadMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW, _oldTransMatrix);
}

void HistoricalViewSprite::visit(Renderer* renderer, const Mat4 &parentTransform, uint32_t parentFlags)
{
    if(_updating)
    {
        return; // Don't capture ourselves
    }
    Node::visit(renderer, parentTransform, parentFlags);
}
