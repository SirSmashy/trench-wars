//
//  CellHighlighter.cpp
//  TrenchWars
//
//  Created by Paul Reed on 6/27/21.
//

#include "CellHighlighter.h"
#include "PlayerLayer.h"
#include "CollisionCell.h"
#include "CollisionGrid.h"
#include "World.h"

CellHighlighter::CellHighlighter()  : DrawNode()
{
    _highlightType = CELL_HIGHLIGHT_NONE;
    _highlightColor = Color4F::RED;
}

CellHighlighter::~CellHighlighter()
{
    
}

bool CellHighlighter::init()
{
    if(DrawNode::init()) {
        globalPlayerLayer->addChild(this, 0); //1000
//        Director * director = Director::getInstance();
//        Scheduler * scheduler = director->getScheduler();
//        scheduler->schedule([=] (float dt) { update(dt); }, this, 0.0333333333, false, "highlight");
        return true;
    }
    return false;
}

CellHighlighter * CellHighlighter::create()
{
    CellHighlighter * highlighter = new CellHighlighter();
    if(highlighter->init())
    {
        highlighter->autorelease();
        return  highlighter;
    }
    
    CC_SAFE_DELETE(highlighter);
    return nullptr;
}

void CellHighlighter::setHightlightType(CELL_HIGHLIGHT_DATA_TYPE type)
{
    _highlightType = type;
}

void CellHighlighter::setHighlightColor(Color4F highlightColor)
{
    _highlightColor = highlightColor;
}

void CellHighlighter::highlightCells(const std::vector<CollisionCell *> & cells)
{
    clear();
    for(CollisionCell * cell : cells)
    {
        highlightCell(cell);
    }
}

void CellHighlighter::hightlightCellsNearPoint(Vec2 location, float radius)
{
    std::vector<CollisionCell *> cells;
    world->getCollisionGrid(location)->cellsInCircle(location, radius, &cells);
    highlightCells(cells);
}


void CellHighlighter::highlightCell(CollisionCell * cell)
{
    if(_highlightType != CELL_HIGHLIGHT_NONE)
    {
        Color4F color = _highlightColor;
        switch (_highlightType)
        {
            case CELL_HIGHLIGHT_CONCEALMENT:
            {
                double value = cell->getTerrainCharacteristics().getConcealmentValue();
                color.a = value / 255;
                break;
            }
            case CELL_HIGHLIGHT_TOTAL_SECURITY:
            {
                double value = cell->getTerrainCharacteristics().getTotalSecurity();
                color.a = value / 2000;
                break;
            }
            case CELL_HIGHLIGHT_SECURITY_NO_HEIGHT:
            {
                double value = cell->getTerrainCharacteristics().getSecurityWithoutHeight();
                color.a = value / 1100;
                break;
            }
            case CELL_HIGHLIGHT_HEIGHT:
            {
                double value = cell->getTerrainCharacteristics().getHeight();
                color.a = fabs(value / 2);
                break;
            }
            case CELL_HIGHLIGHT_IMPASSABLE:
            {
                double value = cell->getTerrainCharacteristics().getImpassable();
                color.a = value;
                break;
            }
        }
        
        drawSolidRect(cell->getCellPosition() * WORLD_TO_GRAPHICS_SIZE, Vec2(cell->getCellPosition().x + 1,cell->getCellPosition().y + 1) * WORLD_TO_GRAPHICS_SIZE, color);
    }

    Color4F color = _highlightColor;
    if(_highlightType != CELL_HIGHLIGHT_NONE)
    {
        color.a = .5;
    }
    drawRect(cell->getCellPosition() * WORLD_TO_GRAPHICS_SIZE, Vec2(cell->getCellPosition().x + 1,cell->getCellPosition().y + 1) * WORLD_TO_GRAPHICS_SIZE, color);

}

void CellHighlighter::clearCells()
{
    clear();
}

void CellHighlighter::remove()
{
    clearCells();
    globalPlayerLayer->removeChild(this);
//    Director * director = Director::getInstance();
//    Scheduler * scheduler = director->getScheduler();
//    scheduler->unschedule("highlight", this);
}
