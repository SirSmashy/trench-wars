//
//  PolygonLandscapeObjectParser.cpp
//  TrenchWars
//
//  Created by Paul Reed on 8/16/22.
//

#include "PolygonLandscapeDefinitionXMLParser.h"
#include "StringUtils.h"
#include <stdexcept>


PolygonLandscapeObjectXMLParser::PolygonLandscapeObjectXMLParser(EntityFactory * factory)
{
    _currentDefinition = nullptr;
    _entityFactory = factory;
    
}

void PolygonLandscapeObjectXMLParser::parseFile(std::string const & filename)
{
    std::string fileContents = FileUtils::getInstance()->getStringFromFile(filename);
    if(!fileContents.empty())
    {
        xml_document doc;
        pugi::xml_parse_result result = doc.load(fileContents.c_str());
        if(result.status == xml_parse_status::status_ok)
        {
            bool parsedSuccessfully = doc.traverse(*this);
            if(!parsedSuccessfully)
            {
                CCLOG("parse error!");
            }
        }
        else
        {
            CCLOG("parse error %s ",result.description());
        }
    }
    else
    {
        CCLOG("PolygonLandscapeObjectXMLParser: Could not Open File %s",filename.c_str());
    }
}

bool PolygonLandscapeObjectXMLParser::for_each(xml_node& node)
{
    ci_string name = node.name();
    std::string value = node.first_child().value();
    
    try
    {
        if(name.compare("PolygonLandscapeObject") == 0)
        {
            _currentDefinition = new PolygonLandscapeObjectDefiniton();
            auto attribute = node.attribute("id");
            if(attribute != nullptr)
            {
                _currentDefinition->_entityName = attribute.value();
                _currentDefinition->_hashedName = std::hash<std::string>{}(_currentDefinition->_entityName);
                _entityFactory->addPolygonLandscapeDefinition(_currentDefinition);
            }
        }
        else if(name.compare("textureName") == 0)
        {
            _currentDefinition->_textureName= value;
        }
        else if(name.compare("textureXSize") == 0)
        {
            _currentDefinition->_textureXSize = std::stof(value);
        }
        else if(name.compare("textureYSize") == 0)
        {
            _currentDefinition->_textureYSize =std::stof(value);
        }
        else if(name.compare("boundaryTextureName") == 0)
        {
            _currentDefinition->_boundaryTextureName = value;
        }
        else if(name.compare("boundaryTextureSize") == 0)
        {
            _currentDefinition->_boundaryTextureSize = std::stof(value);
        }
        else if(name.compare("Cover") == 0)
        {
            _currentDefinition->_cover =std::stod(value);
        }
        else if(name.compare("Hardness") == 0)
        {
            _currentDefinition->_hardness =std::stod(value);
        }
        else if(name.compare("Height") == 0)
        {
            _currentDefinition->_height =std::stoi(value);
        }
        else if(name.compare("Impassable") == 0)
        {
            _currentDefinition->_impassable =std::stoi(value);
        }
        else if(name.compare("MapColor") == 0)
        {
            _currentDefinition->_mapColor = parseColor4B(value);
        }
    }
    catch (const std::invalid_argument& ia)
    {
        CCLOG("Invalid argument: %s for node %s with value %s ", ia.what(), name.c_str(), value.c_str());
        return false;
    }
    
    return true;
}

