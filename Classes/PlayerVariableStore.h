//
//  PlayerVariableStore.hpp
//  TrenchWars
//
//  Created by Paul Reed on 12/15/22.
//

#ifndef PlayerVariableStore_h
#define PlayerVariableStore_h


#include "cocos2d.h"
#include "pugixml/pugixml.hpp"
#include <stdio.h>

using namespace pugi;
USING_NS_CC;
// These are variables that are used
enum PlayerVariables
{
    EmblemColorR,
    EmblemColorG,
    EmblemColorB,
    TeamEmblemColorR,
    TeamEmblemColorG,
    TeamEmblemColorB,
    ObjectiveColorR,
    ObjectiveColorG,
    ObjectiveColorB,
    EmblemSize
} ;

class PlayerVariableStore : public Ref, xml_tree_walker
{
private:
    std::unordered_map<PlayerVariables,double> playerVariables;
        
public:
    PlayerVariableStore();
    
    void parsePlayerVariableFile(const std::string & filename);
    virtual bool for_each(xml_node& node);
    inline double  getVariable(PlayerVariables variable) {
        return playerVariables[variable];
    };
};

extern PlayerVariableStore * globalPlayerVariableStore;

#endif /* PlayerVariableStore_h */
