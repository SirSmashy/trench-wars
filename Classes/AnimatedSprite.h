//
//  AnimatedSprite.hpp
//  TrenchWars
//
//  Created by Paul Reed on 9/19/20.
//

#ifndef AnimatedSprite_h
#define AnimatedSprite_h

#include "cocos2d.h"
#include "EntityDefinitions.h"
#include "VectorMath.h"

USING_NS_CC;

class SpriteBatchGroup;


class EntityAnimation: public Ref
{
public:
    Animation * _animation;
    std::string _tag;
    
    bool _createParticle;
    std::string _particleName;
    Vec2 _particleOffset;
    float _particleHeight = 0.6;
    int _particleCreationFrame;
};

class EntityAnimationSet: public Ref
{
public:
    std::unordered_map<ANIMATION_TYPE, Map<DIRECTION,EntityAnimation *> *> _animationDictionary;
    SpriteBatchGroup * _batchGroup;
    Vec2 _positionOffset = Vec2::ZERO;

    bool _uiLayer;
    
    EntityAnimation * getAnimation(ANIMATION_TYPE type, DIRECTION direction);
    EntityAnimation * getFirstValidAnimation();

};

class AnimatedSprite : public Sprite
{
private:
    EntityAnimationSet * _spriteBatch;
    Size _spriteSize;
    Vec2 _positionOffset;
        
protected:

    ANIMATION_TYPE currentAnimationState;
    DIRECTION currentAnimationDirection;
    bool currentAnimationRepeat;

CC_CONSTRUCTOR_ACCESS :

    virtual bool init(EntityAnimationSet * spriteBatch, Vec2 position);

public:
    static AnimatedSprite * create(EntityAnimationSet * spriteBatch, Vec2 position);

    void setAnimation(ANIMATION_TYPE animation, bool repeat, DIRECTION direction = NONE);
    
    virtual void setPosition(Vec2 position);
    virtual void setScale(float scaleX, float scaleY) override;
    void setSpriteSize(Size size);
    ANIMATION_TYPE  getCurrentAnimationState() {return currentAnimationState;}
    DIRECTION       getCurrentAnimationDirection() {return currentAnimationDirection;}
};

#endif /* AnimatedSprite_h */
