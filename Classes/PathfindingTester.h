//
//  PathfindingTester.hpp
//  TrenchWars
//
//  Created by Paul Reed on 11/26/20.
//

#ifndef PathfindingTester_h
#define PathfindingTester_h

#include "Tester.h"
#include "Entity.h"
#include "Emblem.h"
#include "PrimativeDrawer.h"
#include "List.h"
#include "PathBuildingNode.h"
#include "Path.h"
#include "DebugEntity.h"
#include "LineDrawer.h"

struct PathTest : public Ref
{
    Vec2 offset;
    Path * path;
    Label * costLabel;
    Vector<RouteNode *> routeNodes;
    Vector<PathNode *> pathNodes;
    DebugEntity * debug;
};

class PathFindingTester : public Tester
{
protected:
    MOVEMENT_PREFERENCE _movementPreference;

    PrimativeDrawer * _primativeDrawer;
    LineDrawer * _lineDrawer;
    
    Vector<DebugEntity *> pathDebugEnts;
    Vector<PathTest *> _pathTests;
    
    Emblem * _startEmblem;
    Emblem * _endEmblem;
    
    Color4F _testColors[7];

    PathFindingTester();
    void init(Vec2 position);
    
    void updatePathDebug(Color4F color);
    void updateSinglePath(PathTest * pathTest, Color4F color);
    void draw();
    
public:
    static PathFindingTester * create(Vec2 position);
    

    int setPathsCount(int pathsCount);
    virtual Vec2 getPosition() {return Vec2();}

    bool testCollisionWithPoint(Vec2 point);    
    virtual void update();
    
    Emblem * getEmblemAtPoint(Vec2 location);
    
    void showPathingNodeInfoAtPoint(Vec2 location);
    
    virtual void removeFromGame();
};

#endif /* PathfindingTester_hpp */
