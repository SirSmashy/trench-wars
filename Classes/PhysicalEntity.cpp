//
//  Entity.cpp
//  TrenchWars
//
//  Created by Paul Reed on 1/29/12.
//  Copyright (c). All rights reserved.
//

#include "PhysicalEntity.h"
#include "VectorMath.h"
#include "PrimativeDrawer.h"
#include "Unit.h"
#include "Command.h"
#include "PlayerLayer.h"
#include "EntityManager.h"
#include "EntityFactory.h"
#include "VectorMath.h"
#include "EngagementManager.h"
#include "World.h"
#include "CollisionGrid.h"
#include "GameStatistics.h"
#include "CocosObjectManager.h"
#include "TeamManager.h"
#include "Team.h"

//PhysicalEntityState::PhysicalEntityState()
//{
//    
//}

PhysicalEntity::PhysicalEntity()
{

}

PhysicalEntity::~PhysicalEntity()
{
    _sprite->remove();
    _sprite->release();
}

void PhysicalEntity::loadFromDefinition(PhysicalEntityDefinition * definition)
{
    _sprite = AnimatedSpriteProxy::create(definition->getLowerCaseSpriteName(), _position, _alwaysUpdateSprite);
    _sprite->retain();
    
    _debugging = false;
    testText = nullptr;
    
    CollisionGrid * grid = world->getCollisionGrid(_position);
    if(!grid->getWorldBounds().containsPoint(_position))
    {
        Rect worldBounds = grid->getWorldBounds();
        if(_position.x <= worldBounds.origin.x)
        {
            _position.x = worldBounds.origin.x;
        }
        else if(_position.x >= worldBounds.getMaxX())
        {
            _position.x = worldBounds.getMaxX();
        }
        if(_position.y <= worldBounds.origin.y)
        {
            _position.y = worldBounds.origin.y;
        }
        else if(_position.y >= worldBounds.getMaxY())
        {
            _position.y = worldBounds.getMaxY();
        }
    }
    
    if(definition->workPositions.size() != 0)
    {
        for(Vec2 vec : definition->workPositions)
        {
            _workPositions.push_back(new WorkPosition(vec, nullptr));
        }
    }
    else
    {
        _workPositions.push_back(new WorkPosition(Vec2(-0.5,0.5), nullptr));
        _workPositions.push_back(new WorkPosition(Vec2(0,0.5), nullptr));
        _workPositions.push_back(new WorkPosition(Vec2(0.5,0.5), nullptr));
        
        _workPositions.push_back(new WorkPosition(Vec2(-0.5,0), nullptr));
        _workPositions.push_back(new WorkPosition(Vec2(0,0), nullptr));
        _workPositions.push_back(new WorkPosition(Vec2(0.5,0), nullptr));
        
        _workPositions.push_back(new WorkPosition(Vec2(-0.5,-0.5), nullptr));
        _workPositions.push_back(new WorkPosition(Vec2(0,-0.5), nullptr));
        _workPositions.push_back(new WorkPosition(Vec2(0.5,-0.5), nullptr));
    }

    _hashedEntityName = definition->_hashedName;
    _collisionBounds.origin = _position;
    _collisionBounds.size.width = definition->_collisionSize.x;
    _collisionBounds.size.height = definition->_collisionSize.y;
    
    if(definition->_spriteSize.x != -1)
    {
        _sprite->setSpriteSize(Size(definition->_spriteSize.x, definition->_spriteSize.y));
    }
    
    world->addPhysicalEntity(this);
}


bool PhysicalEntity::init(PhysicalEntityDefinition * definition, Vec2 position, bool alwaysUpdateSprite)
{
    Entity::init();
    _position = position;
    _alwaysUpdateSprite = alwaysUpdateSprite;

    _facing = RIGHT;
    
    loadFromDefinition(definition);
    
    return true;
}

void PhysicalEntity::postLoad()
{

}

int PhysicalEntity::getFreeWorkPositions()
{
    _workPositionsMutex.lock();
    int freePositions = 0;
    for(WorkPosition * workPosition : _workPositions)
    {
        if(workPosition->owner == nullptr) {
            freePositions++;
        }
    }
    _workPositionsMutex.unlock();
    return freePositions;
}

Vec2 PhysicalEntity::claimWorkPosition(PhysicalEntity * ent)
{
    _workPositionsMutex.lock();
    Vec2 nearest;
    double bestDistance = 99999999;
    double distance;
    WorkPosition * best = nullptr;
    for(WorkPosition * workPosition : _workPositions)
    {
        //already occupied
        if(workPosition->owner != nullptr)
        {
            //already occupied by the ent trying to claim it, just return the position
            if(workPosition->owner == ent)
            {
                _workPositionsMutex.unlock();
                return workPosition->position;
            }
            continue;
        }

        distance = ent->getPosition().distanceSquared(getPosition() + workPosition->position);
        if(distance < bestDistance) {
            best = workPosition;
            bestDistance = distance;
        }
    }

    if(best != nullptr)
    {
        best->owner = ent;
        _workPositionsMutex.unlock();
        return best->position;
    }
    _workPositionsMutex.unlock();
    return Vec2();
}

void PhysicalEntity::releaseWorkPosition(PhysicalEntity * ent)
{
    _workPositionsMutex.lock();
    for(WorkPosition * workPosition : _workPositions)
    {
        if(workPosition->owner == ent) {
            workPosition->owner = nullptr;
        }
    }
    _workPositionsMutex.unlock();
}

void PhysicalEntity::setPosition(Vec2 position)
{
    _position = position;
    _collisionBounds.origin = position;
    
    
    _sprite->setPosition(_position);
}


///////// collision handling
bool PhysicalEntity::testCollisionWithBounds(Rect otherBounds)
{
    return _collisionBounds.intersectsRect(otherBounds);
}

bool PhysicalEntity::testCollisionWithLine(Vec2 start, Vec2 end)
{
    if(_collisionBounds.containsPoint(start))
    {
        return true;
    }
    
    ////bottom left to bottom right check
    Vec2 boundEdgeStart = _collisionBounds.origin;
    Vec2 boundEdgeEnd = _collisionBounds.origin;
    boundEdgeEnd.x += _collisionBounds.size.width;
    
    if(Vec2::isSegmentIntersect(start, end, boundEdgeStart, boundEdgeEnd))
    {
        return true;
    }
    
    //bottom right to top right
    boundEdgeStart = boundEdgeEnd;
    boundEdgeEnd.y += _collisionBounds.size.height;
    
    if(Vec2::isSegmentIntersect(start, end, boundEdgeStart, boundEdgeEnd))
    {
        return true;
    }
    
    //top right to top left
    boundEdgeStart = boundEdgeEnd;
    boundEdgeEnd.x = _collisionBounds.origin.x;
    
    if(Vec2::isSegmentIntersect(start, end, boundEdgeStart, boundEdgeEnd))
    {
        return true;
    }
    //top left to bottom left
    boundEdgeStart = boundEdgeEnd;
    boundEdgeEnd.y = _collisionBounds.origin.y;
    
    if(Vec2::isSegmentIntersect(start, end, boundEdgeStart, boundEdgeEnd))
    {
        return true;
    }
    return false;
}

bool PhysicalEntity::testCollisionWithPoint(Vec2 point)
{
    return _collisionBounds.containsPoint(point);
}

bool PhysicalEntity::testCollisionWithCircle(Vec2 center, float radius)
{
    if(_collisionBounds.containsPoint(center))
    {
        return true;
    }
    
    float xClosest = std::clamp(center.x,_collisionBounds.origin.x,_collisionBounds.origin.x + _collisionBounds.size.width);
    float yClosest = std::clamp(center.y,_collisionBounds.origin.y,_collisionBounds.origin.y + _collisionBounds.size.height);
    
    float distX = center.x - xClosest;
    float distY = center.y - yClosest;
    
    if( ((distX * distX) + (distY * distY)) < (radius * radius))
        return true;
    
    return false;
}
void PhysicalEntity::addWork(float deltaTime)
{
}

bool PhysicalEntity::isWorkComplete()
{
    return true;
}
CollisionGrid * PhysicalEntity::getCurrentCollisionGrid()
{
    return world->getCollisionGrid(getPosition());
}

void PhysicalEntity::displayTestInfo()
{
    if(testText == nullptr)
    {
        testText = Label::createWithSystemFont("", "verdana", 12);
        testText->setDimensions(200, 100);
        
        testText->setColor(Color3B(255, 50, 50));
        testText->setPosition(getPosition());
        //world->addChild(testText, 100000);
    }
}

void PhysicalEntity::hideTestInfo()
{
    globalPlayerLayer->removeChild(testText, true);
    testText = nullptr;
}

void PhysicalEntity::updateTestInfo()
{

}

void PhysicalEntity::removeFromGame()
{
    world->removePhysicalEntity(this);
    Entity::removeFromGame();
}

void PhysicalEntity::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Entity::saveToArchive(archive);
    archive(_position);
    archive((int ) _facing);
    archive(_inTerrain);

    archive(_hashedEntityName);
    archive(_alwaysUpdateSprite);
}

void PhysicalEntity::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Entity::loadFromArchive(archive);
    archive(_position);
    int facing;
    archive(facing);
    _facing = (DIRECTION) facing;
    archive(_inTerrain);

    archive(_hashedEntityName);
    archive(_alwaysUpdateSprite);

    _primaryCollisionCell = getCurrentCollisionGrid()->getCellForCoordinates(_position);
    PhysicalEntityDefinition * def = (PhysicalEntityDefinition *) globalEntityFactory->getPhysicalEntityDefinitionWithHashedName(_hashedEntityName);
    loadFromDefinition(def);
}


void PhysicalEntity::populateDebugPanel(GameMenu * menu, Vec2 location)
{
    std::ostringstream outStream;
    outStream << std::fixed;
    outStream.precision(0);
    
    PhysicalEntityDefinition * def = globalEntityFactory->getPhysicalEntityDefinitionWithHashedName(_hashedEntityName);
    if(def != nullptr)
    {
        outStream << "Name: " << def->_entityName;
        menu->addText(outStream.str());
    }
    
    outStream.str("");
    outStream << "Id: " << uID();
    menu->addText(outStream.str());
    
    outStream.str("");
    outStream << "Pos: " << getPosition().x << " " << getPosition().y;
    menu->addText(outStream.str());
}
