//
//  CircleMenu.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/18/20.
//

#include <cmath>
#include "CircularMenu.h"
#include "MenuLayer.h"
#include "VectorMath.h"
#include "Emblem.h"
#include "GameVariableStore.h"
#include "LandscapeObjectiveBuilder.h"
#include "Order.h"
#include "EmblemManager.h"
#include "CommandPost.h"
#include "PlayerLayer.h"
#include "World.h"
#include "CollisionGrid.h"

#define VERTS_IN_CIRCLE 100
#define BACKGROUND_COLOR_STANDARD Color4F(.6,.6,.6, .35)
#define BACKGROUND_COLOR_HIGHLIGHT Color4F(.7,.7,.7, .35)
#define BACKGROUND_COLOR_DISABLED Color4F(.4,.4,.4, .35)

#define BORDER_COLOR_STANDARD Color4F(1.0, 1.0, 1.0, 0.5)
#define BORDER_COLOR_HIGHLIGHT Color4F(1.0, 0.88, 0.05, 0.755)
#define BORDER_COLOR_DISABLED Color4F(0.8, 0.8, 0.8, 0.4)

#define BACKGROUND_POLY_MAX_ANGLE M_PI_4

CircularMenu::CircularMenu() : Layout()
{
    
}

bool CircularMenu::init()
{
    Layout::init();
    _gamePosition = Vec2();
    _radius = -1;
    _selectedIndex = -1;
    _holeRadius = 0;
    _startAngle = 0;
    _endAngle = M_PI * 2;
    _overallOpacity = 1.0;
    
    _primativeDrawer = PrimativeDrawer::create(false);
    _primativeDrawer->setLineWidth(2);
    addChild(_primativeDrawer);
    globalMenuLayer->addChild(this);
    _primativeDrawer->retain();
    
    Director * director = Director::getInstance();
    Scheduler * scheduler = director->getScheduler();
    scheduler->schedule([=] (float dt) { update(dt); }, this, 0.0333333333, false, "GameMenu");

    
    setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    return true;
}

CircularMenu * CircularMenu::create()
{
    CircularMenu * newMenu = new CircularMenu();
    if(newMenu->init())
    {
        newMenu->autorelease();
        return newMenu;
    }
       
    CC_SAFE_DELETE(newMenu);
    return nullptr;
}

void CircularMenu::update(float deltaTime)
{
    Vec2 screenLocation = globalPlayerLayer->convertToWorldSpace(world->getAboveGroundPosition(_gamePosition) * WORLD_TO_GRAPHICS_SIZE );
    setPosition(screenLocation);
    doLayout();
}

void CircularMenu::addOption(std::string name, const std::function<void(Vec2)> & callback, bool disabled)
{
    double fontSize = 20;
    MenuOption * option = new MenuOption();
    option->label = Label::createWithTTF(name, "arial.ttf", fontSize);
    option->label->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    option->label->setAlignment(TextHAlignment::CENTER, TextVAlignment::CENTER);
    if(disabled)
    {
        option->label->setColor(Color3B(200,200,200));
    }
    addChild(option->label);
    option->callback = callback;
    option->disabled = disabled;

    _options.pushBack(option);
}

bool CircularMenu::pointInMenu(Vec2 point)
{
    if(!isVisible())
    {
        return false;
    }
    float distance = point.distance(getPosition());
    if(distance > _radius)
    {
        return false;
    }
    if(distance < _holeRadius)
    {
        clearHighlightedOption();
        return false;
    }
    float angle = getAngleFromStart(point);
    if(angle > (_endAngle - _startAngle))
    {
        return false;
    }
    highlightOptionAtPoint(point);
    return true;
}

void CircularMenu::highlightOptionAtPoint(Vec2 point)
{
    if(_options.size() == 0)
    {
        return;
    }
    float totalAngle = _endAngle - _startAngle;
    double anglePerOption = (totalAngle) / _options.size();
    
    Vec2 vecFromCenter = point - getPosition();
    float angle = getAngleFromStart(point);
        
    clearHighlightedOption();
    _selectedIndex = angle / anglePerOption;
    if(_selectedIndex >= _options.size() || _selectedIndex < 0)
    {
        _selectedIndex = -1;
    }
    else
    {
        _options.at(_selectedIndex)->label->enableBold();
    }
    doLayout();
}

float CircularMenu::getAngleFromStart(Vec2 point)
{
    Vec2 vecFromCenter = point - getPosition();
    double angle = atan2(vecFromCenter.y, vecFromCenter.x);
    double rawAngle = angle;
    angle -= (M_PI_2 - _startAngle);
    angle = -angle;
    if(angle < 0)
    {
        angle += M_PI * 2;
    }
    else if(angle > M_PI * 2)
    {
        angle -= M_PI * 2;
    }
    return angle;
}

void CircularMenu::clearHighlightedOption()
{
    if(_selectedIndex != -1)
    {
        _options.at(_selectedIndex)->label->disableEffect();
    }
    _selectedIndex = -1;
    doLayout();
}

void CircularMenu::drawOptionBorder(float startAngle, float endAngle, bool disabled, bool highlight)
{
    std::vector<Vec2> sliceVerts;
    
    int circleVertCount = ((endAngle - startAngle) / (M_PI * 2)) * VERTS_IN_CIRCLE;
    float radsPerVert = (endAngle - startAngle) / circleVertCount;
    
    Vec2 vert = Vec2::ZERO;
    vert.y += _radius;
    vert.rotate(Vec2::ZERO, -endAngle);
    sliceVerts.push_back(vert);
    
    for(int i = 0; i < circleVertCount; i++)
    {
        vert.rotate(Vec2::ZERO, radsPerVert);
        sliceVerts.push_back(vert);
    }
    
    if (_holeRadius > 0) {
        vert = Vec2::ZERO;
        vert.y += _holeRadius * 2; // Multiply the radius by 2 to make it feel bigger
        vert.rotate(Vec2::ZERO, -startAngle);
        sliceVerts.push_back(vert);
        
        for(int i = 0; i < circleVertCount; i++)
        {
            vert.rotate(Vec2::ZERO, -radsPerVert);
            sliceVerts.push_back(vert);
        }
    }
    else
    {
        sliceVerts.push_back(Vec2::ZERO);
    }
    
    Color4F borderColor;
    if(disabled)
    {
        borderColor = BORDER_COLOR_DISABLED;
    }
    else if(highlight)
    {
        borderColor = BORDER_COLOR_HIGHLIGHT;
    }
    else
    {
        borderColor = BORDER_COLOR_STANDARD;
        borderColor.a *= _overallOpacity; //This feels weird
    }
    
    for(int i = 1; i < sliceVerts.size(); i++)
    {
        _primativeDrawer->drawLine(sliceVerts.at(i-1), sliceVerts.at(i), borderColor);
    }
    _primativeDrawer->drawLine(sliceVerts.at(sliceVerts.size()-1), sliceVerts.at(0), borderColor);

}

void CircularMenu::drawOptionBackground(float startAngle, float endAngle, bool disabled, bool highlight)
{
    Color4F fillColor;
    if(disabled)
    {
        fillColor = BACKGROUND_COLOR_DISABLED;
    }
    else if(highlight)
    {
        fillColor = BACKGROUND_COLOR_HIGHLIGHT;
    }
    else
    {
        fillColor = BACKGROUND_COLOR_STANDARD;
        fillColor.a *= _overallOpacity; //This feels weird
    }
    
    int polyCount = ceil((endAngle - startAngle) / BACKGROUND_POLY_MAX_ANGLE);
    if(polyCount < 1)
    {
        polyCount = 1;
    }
    for(int i = 0; i < polyCount; i++)
    {
        float start = startAngle + (BACKGROUND_POLY_MAX_ANGLE * i);
        float end = endAngle;
        if(end - start > BACKGROUND_POLY_MAX_ANGLE)
        {
            end = start + BACKGROUND_POLY_MAX_ANGLE;
        }
        std::vector<Vec2> sliceVerts;
        int circleVertCount = ((end - start) / (M_PI * 2)) * VERTS_IN_CIRCLE;
        float radsPerVert = (end - start) / circleVertCount;
        
        Vec2 vert = Vec2::ZERO;
        vert.y += _radius;
        vert.rotate(Vec2::ZERO, -end);
        sliceVerts.push_back(vert);
        
        for(int i = 0; i < circleVertCount; i++)
        {
            vert.rotate(Vec2::ZERO, radsPerVert);
            sliceVerts.push_back(vert);
        }
        
        if (_holeRadius > 0) {
            vert = Vec2::ZERO;
            vert.y += _holeRadius * 2; // Multiply the radius by 2 to make it feel bigger
            vert.rotate(Vec2::ZERO, -start);
            sliceVerts.push_back(vert);
            
            for(int i = 0; i < circleVertCount; i++)
            {
                vert.rotate(Vec2::ZERO, -radsPerVert);
                sliceVerts.push_back(vert);
            }
        }
        else
        {
            sliceVerts.push_back(Vec2::ZERO);
        }
        _primativeDrawer->drawPolygon(sliceVerts.data(), sliceVerts.size(), fillColor, 0, BORDER_COLOR_STANDARD);
    }
    
}

void CircularMenu::selectOptionAtPoint(Vec2 point)
{
    Vec2 gameLocation = world->getWorldPosition(globalPlayerLayer->convertToNodeSpace(point) * ONE_OVER_WORLD_TO_GRAPHICS_SIZE);

    if(_options.size() == 0 || _selectedIndex == -1 || _options.at(_selectedIndex)->disabled)
    {
        return;
    }
    
    _options.at(_selectedIndex)->callback(gameLocation);
}

void CircularMenu::doLayout()
{
    _primativeDrawer->clear();
    if(_options.size() == 0)
    {
        return;
    }
    
    int divisions = _options.size();
    float totalAngle = _endAngle - _startAngle;
    double anglePerOption = (totalAngle) / divisions;

    double maxOptionWidth = 0;
    
    for(MenuOption * option : _options)
    {
        float startAngle = _startAngle;
        float endAngle = startAngle + anglePerOption;
        float width = option->label->getContentSize().width + 6; //Add a little padding
        if(width > maxOptionWidth)
        {
            maxOptionWidth = width;
        }
    }
    
    _radius = ((M_PI * 2) / anglePerOption) * maxOptionWidth;
    _radius /= M_PI;
    if(_radius < globalVariableStore->getVariable(MinimumMenuSize))
    {
        _radius = globalVariableStore->getVariable(MinimumMenuSize);
    }
    
    //Position the option labels
    for(int i = 0; i < divisions; i++)
    {
        float centerOfOptionSlice = _startAngle + (anglePerOption * i);
        Vec2 sliceCenter;
        sliceCenter.y += _radius * .6;
        centerOfOptionSlice += anglePerOption / 2;
        sliceCenter.rotate(Vec2::ZERO, -centerOfOptionSlice);
        _options.at(i)->label->setPosition(sliceCenter);
        _options.at(i)->label->setOpacity(_overallOpacity * 255);
    }
    for(int i = 0; i < divisions; i++)
    {
        float startAngle = _startAngle + (anglePerOption * i);
        float endAngle = startAngle + anglePerOption;
        drawOptionBackground(startAngle, endAngle,_options.at(i)->disabled, _selectedIndex == i);
        drawOptionBorder(startAngle, endAngle, _options.at(i)->disabled, _selectedIndex == i);
    }
}

void CircularMenu::setGamePosition(Vec2 position)
{
    _gamePosition = position;
    Vec2 screenLocation = globalPlayerLayer->convertToWorldSpace(world->getAboveGroundPosition(_gamePosition) * WORLD_TO_GRAPHICS_SIZE);
    setPosition(screenLocation);

}

void CircularMenu::setVisible(bool visible)
{
    Layout::setVisible(visible);
    if(!visible)
    {
        clearHighlightedOption();
    }
}

void CircularMenu::setOpacity(GLubyte opacity)
{
    _overallOpacity = opacity;
    _overallOpacity /= 255;
}

void CircularMenu::setHoleRadius(float radius)
{
    _holeRadius = radius;
}

void CircularMenu::setCircleExtent(float startAngle, float endAngle)
{
    _startAngle = startAngle;
    _endAngle = endAngle;
}


void CircularMenu::clear()
{
    _radius = -1;
    _selectedIndex = -1;
    _primativeDrawer->clear();
    for(MenuOption * option : _options)
    {
        removeChild(option->label);
    }
    _options.clear();
}

void CircularMenu::remove()
{
    removeChild(_primativeDrawer);
    _primativeDrawer->remove();
    _primativeDrawer->release();

    clear();
}
