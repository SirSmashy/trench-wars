//
//  World.h
//  TrenchWars
//
//  Created by Paul Reed on 11/14/21.
//

#ifndef World_h
#define World_h

#include <unordered_set>
#include "cocos2d.h"
#include "OverlayDrawer.h"
#include "Refs.h"
#include "PermanentParticleSystem.h"
#include "StaticEntity.h"
#include "MovingPhysicalEntity.h"
#include "Cloud.h"
#include "ParticleBatchGroup.h"
#include "WorldLayer.h"
#include <cereal/archives/binary.hpp>


class CollisionGrid;
class NavigationGrid;
class EngagementScene;
class Bunker;
class PolygonLandscapeObject;

class NavigationPatch;
class NavigationPatchBorder;


#define UNDERGROUND_Z_OFFSET 6000000


// The transition coordinates between two world spaces: basically an entrance/exit to an underground area
struct Transition
{
    Vec2 _start;
    Vec2 _end;
    
    Transition(Vec2 start, Vec2 end)
    {
        this->_start = start;
        this->_end = end;
    }
};

/* A WorldSection is a portion of the world.
 * Only a single area will be above ground, the rest will be underground
 * Because Cocos2d does not support 3D spaces, all underground areas are actually placed outside the above ground area (well north of it)
 * The underground areas are offset for rendering to appear at their origin point in the above ground world.
 */
class WorldSection : public ComparableRef
{
public:
    CollisionGrid * _collisionGrid;
    NavigationGrid * _navGrid;
    Map<std::string, ParticleBatchGroup *> _particleBatches;
    Map<std::string, ParticleBatchGroup *> _permanentParticleBatches;

    std::unordered_map<ENTITY_ID, PhysicalEntity *> _physicalEntities;
    Map<ENTITY_ID, PolygonLandscapeObject *> _landscapeEntities;
    std::unordered_map<ENTITY_ID, Cloud *> _atmosphericEntities;
    
public:

    Rect _areaBounds; //This is the actual coordinate bounds of world area
    Vec2 _offset; // This is the offset used to visually position the area
    std::map<ENTITY_ID, Transition> _transitionsToOtherGrids;
    bool _underground;
    bool _areaDestroyed;
    
    PolygonLandscapeObject * getLandscapeObjectAtPosition();
    
    void loadFromArchive(cereal::BinaryInputArchive & archive);
    void saveToArchive(cereal::BinaryOutputArchive & archive);
};


struct WorldSectionRequest
{
    Vec2 sectionOrigin;
    Size sectionSize;
    Vec2 entranceToArea;
                    
    WorldSectionRequest(Vec2 origin, Size size, Vec2 entrance)
    {
        sectionOrigin = origin;
        sectionSize = size;
        entranceToArea = entrance;
    }
};

class World : public Ref
{
private:
    float _belowgroundAreaOffset;
    
    friend class WorldSection;

    // Cocos2d and rendering related members
    WorldLayer * _worldLayer;
    OverlayDrawer * _backgroundMask; //Used when showing the below ground areas. Masks out the above ground node;
    bool _undergroundVisible;
    
    Size _worldSize;
    std::mutex entitieseMapMutex;
    Vector<WorldSection *> _worldSections;
    
    std::vector<WorldSectionRequest> _worldSectionRequests;
    std::vector<Vec2> _worldSectionsRemoved;
    
    Map<ENTITY_ID, PolygonLandscapeObject *> _landscapeEntities;
    
    World();
    bool init(Image * map, EngagementScene * scene, bool createStaticEnts);
    bool init(cereal::BinaryInputArchive & inputArchive, EngagementScene * scene);
    
    WorldSection * createWorldSection(Vec2 origin, Size areaSize);
    void addWorldSection(WorldSection * section);

        
public:
    static World * create(Image * map, EngagementScene * scene, bool createStaticEnts = true);
    static World * create(cereal::BinaryInputArchive & inputArchive, EngagementScene * scene);

    void createUndergroundArea(Vec2 origin, Size areaSize, Vec2 entranceToArea, Bunker * entrance);

    void destroyUndergroundArea(Vec2 positionInArea);
    int getNumberOfAreas() {return _worldSections.size();}
    
    
    CollisionGrid * getCollisionGrid(Vec2 position = Vec2());
    NavigationGrid * getNavGrid(Vec2 position = Vec2());
    NavigationGrid * getNavGridForIndex(int sectionIndex = 0);
    
    Size getWorldSize() {return _worldSize;}
    Node * getWorldCocosNode() {return _worldLayer;}
    
    bool isUndergroundVisible() {return _undergroundVisible;}
    void toggleShowUnderground();
    
    void addPhysicalEntity(PhysicalEntity * entity);
    void removePhysicalEntity(PhysicalEntity * entity);
    
    void addLandscapeObject(PolygonLandscapeObject * entity);
    PolygonLandscapeObject * getLandscapeObject(ENTITY_ID objectId);
    void removeLandscapeObject(PolygonLandscapeObject * entity);
    
    void addAtmosphericObject(Cloud * entity);
    PolygonLandscapeObject * getAtmosphericObject(ENTITY_ID objectId);
    void removeAtmosphericObject(Cloud * entity);
        
    WorldSection * getWorldSectionForPosition(Vec2 position);
    const Vector<WorldSection *> getWorldSections() {return _worldSections;}
    
    Vec2 getWorldPosition(Vec2 visualPosition); //Potententially gets underground areas if their position lies below the visualPosition and underground areas are being displayed
    Vec2 getAboveGroundPosition(Vec2 worldPosition);
    bool isPositionUnderground(Vec2 position);
    bool arePositionsInSameArea(Vec2 firstPosition, Vec2 secondPosition);
    bool isBunkerEntranceNearPosition(Vec2 position, float distance);
    
    void transitionEntityToNewArea(Vec2 fromPosition, Vec2 toPosition, MovingPhysicalEntity * ent);

    void addChild(Node * node, int zOrder = 0);
    void removeChild(Node * node);
    
    void saveToArchive(cereal::BinaryOutputArchive & archive);
    
    void serializeChanges(cereal::BinaryOutputArchive & archive);
    void deserializeChanges(cereal::BinaryInputArchive & archive);
    void clearChanges();
};

extern World * world;

#endif /* World_h */
