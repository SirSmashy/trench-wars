//
//  ServerEventController.cpp
//  TrenchWars
//
//  Created by Paul Reed on 1/17/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#include "ServerEventController.h"
#include "Order.h"
#include "Command.h"
#include "Team.h"
#include "Unit.h"
#include "CommandPost.h"
#include "Objective.h"
#include "UIController.h"
#include "TrenchWarsManager.h"
#include "TeamManager.h"
#include <cereal/types/string.hpp>
#include "EntityManager.h"
#include "EngagementManager.h"

ServerEventController::ServerEventController()
{
    
}

void ServerEventController::init()
{
    GameEventController::init();
}

ServerEventController * ServerEventController::create()
{
    ServerEventController * controller = new ServerEventController();
    controller->init();
    return controller;
}

void ServerEventController::postLoad()
{
    if(globalUIController != nullptr)
    {
        globalUIController->postLoad();
        Player * localPlayer = globalTrenchWarsManager->getLocalPlayer();
        Command * command = globalTeamManager->getCommand(localPlayer->commandId);
        globalUIController->getCamera()->setScreenPosition(command->getSpawnPosition());
        
        for(auto team : globalTeamManager->getTeams())
        {
            _teamChanges.try_emplace(team->getTeamId());
        }
        
    }
}


void ServerEventController::buildClientUpdate(cereal::BinaryOutputArchive & archive, int teamId)
{
    Team * team = globalTeamManager->getTeam(teamId);
    auto & teamChange = _teamChanges.at(teamId);
    archive(teamId);
    
    archive(teamChange.planChanges.size());
    for(auto & planPair : teamChange.planChanges)
    {
        archive(planPair.first);
        archive(planPair.second);
        
        if(planPair.second == PLAN_ACTION_CREATE)
        {
            Plan * plan = team->getPlanWithId(planPair.first);
            plan->saveToArchive(archive);
        }
        else if(planPair.second == PLAN_ACTION_RENAME)
        {
            Plan * plan = team->getPlanWithId(planPair.first);
            archive(plan->getName());
        }
    }
    
    archive(teamChange.newIssuedOrders.size());
    for(auto & newOrder : teamChange.newIssuedOrders)
    {
        archive(newOrder.issuedOrder->getOrderType());
        newOrder.issuedOrder->saveToArchive(archive);
        archive(newOrder.unitIssuedOrder);
        archive(newOrder.planOrderIsPartOf);
        newOrder.issuedOrder->release();
    }
    
    archive(teamChange.orderCompleted.size());
    for(auto & completeOrder : teamChange.orderCompleted)
    {
        archive(completeOrder.completedOrder);
        archive(completeOrder.unitCompleting);
        archive(completeOrder.unitFailedOrder);
    }
    
    teamChange.planChanges.clear();
    teamChange.plansRemoved.clear();
    teamChange.newIssuedOrders.clear();
    teamChange.orderCompleted.clear();
}

void ServerEventController::parseUpdateFromClient(cereal::BinaryInputArchive & archive, int teamId, int playerId)
{
    size_t size;
    ENTITY_ID unitId;
    ENTITY_ID planId;
    ORDER_TYPE orderType;
    Team * team = globalTeamManager->getTeam(teamId);
    
    Player * player = globalTrenchWarsManager->getPlayerForPlayerId(playerId);
    
    
    archive(size);
    for(int i = 0; i < size; i++)
    {
        archive(unitId);
        archive(planId);
        archive(orderType);
        
        Unit * unit = (Unit *) globalEntityManager->getEntity(unitId);
        Plan * plan = team->getPlanWithId(planId);
        
        if(unit == nullptr)
        {
            LOG_DEBUG_ERROR("OH NO! \n");
            return;
        }
        
        if(player->commandId != unit->getOwningCommand()->getCommandID())
        {
            LOG_DEBUG_ERROR("Illegal order! \n");
            return;
        }
        
        switch (orderType) {
            case MOVE_ORDER:
            {
                Vec2 position;
                archive(position);
                LOG_DEBUG("Received move from client %.1f %.1f \n", position.x, position.y);
                issueMoveOrder(position, unit, plan);
                break;
            }
            case OCCUPY_ORDER:
            case ATTACK_POSITION_ORDER:
            case OBJECTIVE_WORK_ORDER:
            case ENTITY_WORK_ORDER:
            {
                ENTITY_ID objectiveId;
                OBJECTIVE_ORDER_ACTION action;
                
                archive(objectiveId);
                archive(action);
                Objective * objective = (Objective *) globalEntityManager->getEntity(objectiveId);
                if(objective == nullptr)
                {
                    continue;
                }
                
                switch (action) {
                    case OBJECTIVE_ORDER_OCCUPY:
                    {
                        issueOccupyOrder(objective, unit, plan);
                        break;
                    }
                    case OBJECTIVE_ORDER_DIG_TRENCHES:
                    {
                        issueDigTrenchesOrder(objective, unit, plan);
                        break;
                    }
                    case OBJECTIVE_ORDER_DIG_BUNKERS:
                    {
                        issueDigBunkersOrder(objective, unit, plan);
                        break;
                    }
                    case OBJECTIVE_ORDER_BUILD_COMMAND_POST:
                    {
                        issueBuildCommandPostOrder(objective, unit, plan);
                        break;
                    }
                    case OBJECTIVE_ORDER_CLEAR_AREA:
                    {
                        issueClearAreaOrder(objective, unit, plan);
                        break;
                    }
                    case OBJECTIVE_ORDER_ATTACK:
                    {
                        issueAttackPositionOrder(objective, unit, plan);
                        break;
                    }
                        
                    default:
                        break;
                }
                break;
            }
            case STOP_ORDER:
            {
                issueStopOrder(unit, plan);
                break;
            }
                
            default:
                break;
        }
    }
    
    OBJECTIVE_ACTION objectiveAction;
    ENTITY_ID objectiveId;
    
    archive(size);
    for(int i = 0; i < size; i++)
    {
        archive(objectiveAction);
        archive(objectiveId);
        Objective * objective = (Objective *) globalEntityManager->getEntity(objectiveId);

        switch(objectiveAction)
        {
            case OBJECTIVE_ACTION_CREATE:
            {
                std::vector<Vec2> points;
                Vec2 point;
                bool enclosed;
                size_t pointsSize;
                archive(pointsSize);
                for(int j = 0 ; j < pointsSize; j++)
                {
                    archive(point);
                    points.push_back(point);
                }
                archive(enclosed);
                ENTITY_ID unitId;
                archive(unitId);
                Unit * unit = nullptr;
                if(unitId != -1)
                {
                    unit = (Unit *) globalEntityManager->getEntity(unitId);
                }
                createNewObjective(points, enclosed, unit);
                break;
            }
            case OBJECTIVE_ACTION_REMOVE:
            {
                if(objective == nullptr)
                {
                    LOG_ERROR("Error: OBJECTIVE_ACTION_REMOVE objective is null! \n");
                }
//                if(player->commandId != objective->getOwningTeam()->getCommandID())
//                {
//                    LOG_DEBUG("Illegal objective action! \n");
//                    return;
//                }

                removeObjective(objective);
                break;
            }
            case OBJECTIVE_ACTION_CREATE_COMMAND_POST:
            {
                if(objective == nullptr)
                {
                    LOG_ERROR("Error: OBJECTIVE_ACTION_CREATE_COMMAND_POST objective is null! \n");
                }
                Vec2 position;
                archive(position);
                createCommandPostInObjective(objective, position);
                break;
            }
            case OBJECTIVE_ACTION_CREATE_TRENCH:
            {
                if(objective == nullptr)
                {
                    LOG_ERROR("Error: OBJECTIVE_ACTION_CREATE_TRENCH objective is null! \n");
                }
                createTrenchLineInObjective(objective);
                break;
            }
            case OBJECTIVE_ACTION_CREATE_BUNKERS:
            {
                if(objective == nullptr)
                {
                    LOG_ERROR("Error: OBJECTIVE_ACTION_CREATE_BUNKERS objective is null! \n");
                }
                createBunkersInObjective(objective);
                break;
            }
            case OBJECTIVE_ACTION_REMOVE_TRENCH:
            {
                if(objective == nullptr)
                {
                    LOG_ERROR("Error: OBJECTIVE_ACTION_REMOVE_TRENCH objective is null! \n");
                }
                removeTrenchLineInObjective(objective);
                break;
            }
            case OBJECTIVE_ACTION_REMOVE_BUNKERS:
            {
                if(objective == nullptr)
                {
                    LOG_ERROR("Error: OBJECTIVE_ACTION_REMOVE_TRENCH objective is null! \n");
                }
                removeBunkersInObjective(objective);
                break;
            }
        }
    }
    
    PLAN_ACTION planAction;
    
    archive(size);
    for(int i = 0; i < size; i++)
    {
        archive(planAction);
        archive(planId);
        Plan * plan = team->getPlanWithId(planId);
        switch(planAction)
        {
            case PLAN_ACTION_CREATE:
            {
                int playerId;
                archive(playerId);
                createNewPlan(playerId);
                break;
            }
            case PLAN_ACTION_REMOVE:
            {
                if(plan == nullptr)
                {
                    LOG_ERROR("Error: PLAN_ACTION_REMOVE plan is null! \n");
                }
                removePlan(plan);
                break;
            }
            case PLAN_ACTION_EXECUTE:
            {
                if(plan == nullptr)
                {
                    LOG_ERROR("Error: PLAN_ACTION_EXECUTE plan is null! \n");
                }
                executePlan(plan);
                break;
            }
            case PLAN_ACTION_STOP:
            {
                if(plan == nullptr)
                {
                    LOG_ERROR("Error: PLAN_ACTION_STOP plan is null! \n");
                }
                stopPlan(plan);
                break;
            }
            case PLAN_ACTION_RENAME:
            {
                if(plan == nullptr)
                {
                    LOG_ERROR("Error: PLAN_ACTION_RENAME plan is null! \n");
                }
                std::string name;
                archive(name);
                renamePlan(plan, name);
                break;
            }
        }
    }

}

void ServerEventController::query(float deltaTime)
{
    
}


////////////// low level event functions //////////////////
///



void ServerEventController::issueMoveOrder(Vec2 position, Unit * commandable, Plan * addToPlan)
{
    MoveOrder * move = MoveOrder::create(position);
    handleEventOrderIssued(move, commandable, addToPlan);
}

void ServerEventController::issueOccupyOrder(Objective * objective, Unit * commandable, Plan * addToPlan)
{
    OccupyOrder * occupy = OccupyOrder::create(objective);
    handleEventOrderIssued(occupy, commandable, addToPlan);
}

void ServerEventController::issueDigTrenchesOrder(Objective * objective, Unit * commandable, Plan * addToPlan)
{
    if(!objective->hasTrenchLine())
    {
        objective->createTrenchLine();
    }
    ObjectiveWorkOrder * work = ObjectiveWorkOrder::create(objective->getTrenchLine());
    handleEventOrderIssued(work, commandable, addToPlan);
}

void ServerEventController::issueDigBunkersOrder(Objective * objective, Unit * commandable, Plan * addToPlan)
{
    ObjectiveWorkOrder * work = ObjectiveWorkOrder::create(objective->getBunkers());
    handleEventOrderIssued(work, commandable, addToPlan);
}

void ServerEventController::issueBuildCommandPostOrder(Objective * objective, Unit * commandable, Plan * addToPlan)
{
    EntityWorkOrder * work = EntityWorkOrder::create(objective->getCommandPost());
    handleEventOrderIssued(work, commandable, addToPlan);
}

void ServerEventController::issueClearAreaOrder(Objective * objective, Unit * commandable, Plan * addToPlan)
{
}

void ServerEventController::issueAttackPositionOrder(Objective * objective, Unit * commandable, Plan * addToPlan)
{
    AttackPositionOrder * attackOrder = AttackPositionOrder::create(objective);
    handleEventOrderIssued(attackOrder, commandable, addToPlan);
}

void ServerEventController::issueStopOrder(Unit * unit, Plan * addToPlan)
{
    LOG_DEBUG("Stop! \n");
    if(addToPlan == nullptr)
    {
        if(unit->hasOrder())
        {
            StopOrder * stop = StopOrder::create();
            handleEventOrderIssued(stop, unit, addToPlan);
        }
        else
        {
            handleEventOrderCancelled(unit);
        }
    }
    else
    {
        if(!addToPlan->isExecuting())
        {
            handleEventOrderRemovedFromPlan(unit, addToPlan);
        }
        else
        {
            StopOrder * stop = StopOrder::create();
            handleEventOrderIssued(stop, unit, addToPlan);
        }
    }
}


void ServerEventController::createNewObjective(std::vector<Vec2> & points, bool enclosed, Unit * unitCreating)
{
    Objective * objective = Objective::create(globalTeamManager->getLocalPlayersTeam(), points, enclosed);
    handleEventObjectiveCreated(objective);
    if(unitCreating != nullptr)
    {
        objective->createTrenchLine();
        ObjectiveWorkOrder * work = ObjectiveWorkOrder::create(objective->getTrenchLine());
        handleEventOrderIssued(work, unitCreating, nullptr);
    }
}

void ServerEventController::removeObjective(Objective * objective)
{
    handleEventObjectiveRemoved(objective);
    objective->removeFromGame();
}


// Command Pots
void ServerEventController::createCommandPostInObjective(Objective * objective, Vec2 location)
{
    objective->createCommandPost(location);
}


// Trenches
void ServerEventController::createTrenchLineInObjective(Objective * objective)
{
    objective->createTrenchLine();
}

void ServerEventController::removeTrenchLineInObjective(Objective * objective)
{
    objective->removeTrenchLine();
}

// Bunkers
void ServerEventController::createBunkersInObjective(Objective * objective)
{
    objective->createBunkers();
}


void ServerEventController::removeBunkersInObjective(Objective * objective)
{
    objective->removeBunkers();
}


// Plans
void ServerEventController::createNewPlan(int playerId)
{
    Plan * newPlan = Plan::create("New Plan", playerId);
    Team * team =  globalTeamManager->getTeamForPlayer(playerId);
    team->addPlan(newPlan);
    handleEventNewPlan(newPlan);
    _teamChanges.at(team->getTeamId()).planChanges.emplace_back(newPlan->uID(), PLAN_ACTION_CREATE);
}

void ServerEventController::removePlan(Plan * plan)
{
    _teamChanges.at(plan->getOwningTeam()->getTeamId()).planChanges.emplace_back(plan->uID(), PLAN_ACTION_REMOVE);
    handleEventPlanRemoved(plan);
    globalTeamManager->getLocalPlayersTeam()->removePlan(plan);
}

void ServerEventController::executePlan(Plan * plan)
{
    if(plan->isExecuting())
    {
        LOG_DEBUG_ERROR("Plan is already executing! \n");

        return;
    }
    for(auto orderPair : plan->getOrders())
    {
        Unit * ent = (Unit *) globalEntityManager->getEntity(orderPair.first);
        auto order = ent->getOwningCommand()->getOrderWithId(orderPair.second);
        if(ent != nullptr && order != nullptr)
        {
           handleEventOrderIssued(order, ent, nullptr);
        }
    }
    plan->setExecuting(true);
    handleEventPlanExecuting(plan);
    _teamChanges.at(plan->getOwningTeam()->getTeamId()).planChanges.emplace_back(plan->uID(), PLAN_ACTION_EXECUTE);
}


void ServerEventController::stopPlan(Plan * plan)
{
    if(!plan->isExecuting())
    {
        LOG_DEBUG_ERROR("Plan is not executing! \n");
        return;
    }
    for(auto order : plan->getOrders())
    {
        Unit * ent = (Unit *) globalEntityManager->getEntity(order.first);
        if(ent != nullptr)
        {
            handleEventOrderIssued(StopOrder::create(), ent, nullptr);
        }
    }
    plan->setExecuting(false);
    handleEventPlanStopped(plan);
    _teamChanges.at(plan->getOwningTeam()->getTeamId()).planChanges.emplace_back(plan->uID(), PLAN_ACTION_STOP);
}


void ServerEventController::renamePlan(Plan * plan, const std::string & newName)
{
    plan->setName(newName);
    _teamChanges.at(plan->getOwningTeam()->getTeamId()).planChanges.emplace_back(plan->uID(), PLAN_ACTION_RENAME);
    handleEventPlanRenamed(plan, newName);
}

// Team status
void ServerEventController::updateTeamVisibility(Team * team, 
                                                 std::unordered_map<ENTITY_ID, double> & sectorVisibleTime,
                                                 std::unordered_set<ENTITY_ID> & sectorsNewlyHidden,
                                                 std::unordered_set<ENTITY_ID> & sectorsNewlyVisible)
{
    
    // Network stuff here
    handleEventTeamVisibilityUpdate(team, sectorVisibleTime);
}

// ////////// EVENT HANDLING ////////
void ServerEventController::handleEventOrderIssued(Order * order, Unit * unit, Plan * addToPlan)
{
    unit->getOwningCommand()->addOrder(order);
    if(globalTrenchWarsManager->getNetworkMode() == NETWORK_MODE_SERVER)
    {
        ENTITY_ID unitId = unit->uID();
        ENTITY_ID planId = addToPlan != nullptr ? addToPlan->uID()  : -1;
        LOG_DEBUG("Record order issued %lld %d \n", order->uID(), order->getOrderType());
        _teamChanges.at(unit->getOwningTeam()->getTeamId()).newIssuedOrders.emplace_back(order, unitId, planId);
        order->retain();
    }
    
    if(addToPlan == nullptr)
    {
        if(unit->getOwningTeam()->isPositionInCommandArea(unit->getAveragePosition()))
        {
            unit->setOrder(order);
            if(globalUIController != nullptr)
            {
                globalUIController->reportUnitIssuedOrder(order, unit);
            }
        }
        else
        {
            CommandPost * post = unit->getOwningCommand()->getNearestCommandPost(unit->getAveragePosition());
            post->sendOrderToUnit(order, unit);
            if(globalUIController != nullptr)
            {
                globalUIController->reportCourierTransportingOrder(order, unit);
            }
        }
    }
    else
    {
        addToPlan->addOrder(unit, order);
        if(globalUIController != nullptr)
        {
            globalUIController->reportOrderAddedToPlan(order, unit, addToPlan);
        }
    }
}

void ServerEventController::handleEventObjectiveCreated(Objective * objective)
{
    if(globalUIController != nullptr)
    {
        globalUIController->reportNewObjective(objective);
    }
}

void ServerEventController::handleEventObjectiveRemoved(Objective * objective)
{
    if(globalUIController != nullptr)
    {
        globalUIController->reportObjectiveRemoved(objective);
    }
}


void ServerEventController::handleEventSectorVisible(MapSector * sector)
{
    
}

void ServerEventController::handleEventTeamVisibilityUpdate(Team * team, std::unordered_map<ENTITY_ID, double> & sectorVisibleTime)
{
    if(globalUIController != nullptr && globalTeamManager->getLocalPlayersTeam() == team)
    {
        globalUIController->reportTeamVisibilityUpdate(sectorVisibleTime);
    }
}


//command post related
void ServerEventController::handleEventNewCommandPost(CommandPost * commandPost)
{
    if(globalUIController != nullptr)
    {
        globalUIController->reportNewCommandPost(commandPost);
    }
}

void ServerEventController::handleEventCommandPostDestroyed(CommandPost * commandPost)
{
    
}


//Unit related
void ServerEventController::handleEventNewUnit(Unit * unit)
{
    if(globalUIController != nullptr && unit->getOwningTeam() == globalTeamManager->getLocalPlayersTeam())
    {
        globalUIController->reportNewUnit(unit);
    }
}


void ServerEventController::handleEventUnitDestroyed(Unit * unit)
{
    if(globalUIController != nullptr)
    {
        globalUIController->reportUnitDestroyed(unit);
    }
}

// ORDER related
void ServerEventController::handleEventOrderComplete(Unit * entCompleting, Order * order, bool failed)
{
    Team * team = nullptr;
    ENTITY_ID completingId = -1;
    if(entCompleting != nullptr)
    {
        team = entCompleting->getOwningCommand()->getOwningTeam();
        completingId = entCompleting->uID();
    }
    
    if(globalUIController != nullptr)
    {
        globalUIController->reportOrderComplete(entCompleting, order, failed);
    }
    auto plan = team->getPlanWithId(order->getOwningPlanId());
    if(plan != nullptr)
    {
        plan->orderComplete(entCompleting);
    }
    
    LOG_DEBUG("Record order complete %lld %d \n", order->uID(), order->getOrderType());
    _teamChanges.at(team->getTeamId()).orderCompleted.emplace_back(order->uID(), completingId, failed);
}

void ServerEventController::handleEventOrderCancelled(Unit * commandable)
{
    if(globalUIController != nullptr)
    {
        globalUIController->reportOrderCancelled(commandable);
    }
}


// PLAN related
void ServerEventController::handleEventNewPlan(Plan * plan)
{
    if(globalUIController != nullptr)
    {
        globalUIController->reportNewPlan(plan);
    }
}

void ServerEventController::handleEventPlanExecuting(Plan * plan)
{
    if(globalUIController != nullptr)
    {
        globalUIController->reportPlanExecuting(plan);
    }
}


void ServerEventController::handleEventPlanStopped(Plan * plan)
{
    if(globalUIController != nullptr)
    {
        globalUIController->reportPlanStopped(plan);
    }
}

void ServerEventController::handleEventPlanRemoved(Plan * plan)
{
    if(globalUIController != nullptr)
    {
        globalUIController->reportPlanRemoved(plan);
    }
}

void ServerEventController::handleEventOrderRemovedFromPlan(Unit * unit, Plan * plan)
{
    plan->removeOrderForUnit(unit);
    if(globalUIController != nullptr)
    {
        globalUIController->reportOrderRemovedFromPlan(unit, plan);
    }
}

void ServerEventController::handleEventPlanRenamed(Plan * plan, const std::string & newName)
{
    if(globalUIController != nullptr)
    {
        globalUIController->reportPlanRenamed(plan, newName);
    }
}


//friendly unit presence
void ServerEventController::handleEventUnitInSector(MapSector * sector, Unit * unit)
{
    
}

//enemy unit presence
void ServerEventController::handleEventEnemyUnitSpotted(Unit * enemy)
{
    
}

void ServerEventController::handleEventEnemyCommandPostSpotted(CommandPost *enemy)
{
    
}

void ServerEventController::handleEventEnemyCanEngageSector(MapSector * sector, Human * enemy)
{
    
}

void ServerEventController::handleEventEnemyArtilleryInSector(MapSector * sector)
{
    
}
