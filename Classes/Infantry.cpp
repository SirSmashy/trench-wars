//
//  Infantry.m
//  TrenchWars
//
//  Created by Paul Reed on 4/17/12.
//  Copyright (c) 2012 . All rights reserved.
//

#include "Infantry.h"
#include "CollisionCell.h"
#include "EntityFactory.h"
#include "PrimativeDrawer.h"
#include "Order.h"
#include "ObjectiveObjectCollection.h"
#include "VectorMath.h"
#include "GameStatistics.h"
#include "GameVariableStore.h"
#include "EntityManager.h"
#include "MultithreadedAutoReleasePool.h"
#include "CollisionGrid.h"
#include "Command.h"
#include "GameMenu.h"
#include "Team.h"
#include "EngagementManager.h"
#include "TrenchWarsManager.h"
#include "InteractiveObjectUtils.h"
#include "TestBed.h"

#define CHECKVISIONCOUNTERMAX 8
#define CHECKCHARGECOUNTERMAX 4


Infantry::Infantry() :
Human(),
lookForTargetCounter(CHECKVISIONCOUNTERMAX, true),
checkChargeCounter(CHECKCHARGECOUNTERMAX, true)
{
}

Infantry::~Infantry()
{
}


bool Infantry::init(InfantryDefinition * definition, Vec2 position, Command * command, Unit * unit)
{
    if(Human::init(definition, position, command))
    {
        _currentRecoil = 0;
        _firing = false;
        _targetHeight = 0;
        _targetPosition = Vec2::ZERO;
        _targetFireAngle = 0;
        
        
        if(definition != nullptr)
        {
            _maxAmmoReserve = definition->_ammoReserve;
            _currentAmmoReserve = definition->_ammoReserve;
            _owningUnit = unit;
            
            _targetAquireSpeed = definition->_targetAquireSpeed;
            _acquireTargetTimeModifier = 1.0;
            _chargeRadius = definition->_chargeRadius;

            _currentWeapon = nullptr;
            _crewWeapon = nullptr;
            for(auto weaponDefIterator = definition->_weapons.begin(); weaponDefIterator != definition->_weapons.end(); weaponDefIterator++)
            {
                WeaponDefinition * weaponDef = *weaponDefIterator;
                Weapon * newWeapon;
                
                newWeapon = Weapon::create(weaponDef, this);
                _weaponList.pushBack(newWeapon);

                if(_currentWeapon == nullptr || _currentWeapon->getPreferenceOrder() < newWeapon->getPreferenceOrder())
                {
                    _currentWeapon = newWeapon;
                }
            }
            for(Weapon * weapon : _weaponList)
            {
                if(weapon != _currentWeapon)
                {
                    weapon->setStored(true);
                }
            }
            _fireOffsets[RIGHT] = definition->_muzzlePositionOffset;
            _fireOffsets[UP_RIGHT] = definition->_muzzlePositionOffset;
            _fireOffsets[UP] = Vec2(definition->_muzzlePositionOffset.x /2, definition->_muzzlePositionOffset.y);
            _fireOffsets[UP_LEFT] = Vec2(0, definition->_muzzlePositionOffset.y);
            _fireOffsets[LEFT] = Vec2(0, definition->_muzzlePositionOffset.y);
            _fireOffsets[DOWN_LEFT] = Vec2(0, 0);
            _fireOffsets[DOWN] = Vec2(definition->_muzzlePositionOffset.x /2, 0);
            _fireOffsets[DOWN_RIGHT] = Vec2(definition->_muzzlePositionOffset.x, 0);
        }
    }
    else
    {
        CCLOG("oh no!!! ");
    }
    return true;
}

Infantry * Infantry::create(InfantryDefinition * definition, Vec2 position, Command * command, Unit * unit)
{
    Infantry * newInfantry = new Infantry();
    if(newInfantry && newInfantry->init(definition, position, command, unit))
    {
        globalAutoReleasePool->addObject(newInfantry);
        return newInfantry;
    }
    CC_SAFE_DELETE(newInfantry);
    return nullptr;
}

Infantry * Infantry::create(const std::string & definitionName, Vec2 position, Command * command, Unit * unit)
{
    InfantryDefinition * definition = (InfantryDefinition *) globalEntityFactory->getPhysicalEntityDefinitionWithName(definitionName);
    if(definition != nullptr)
    {
        return Infantry::create(definition, position, command, unit);
    }
    return nullptr;
}

Infantry * Infantry::create()
{
    Infantry * newInfantry = new Infantry();
    globalAutoReleasePool->addObject(newInfantry);
    return newInfantry;
}


void Infantry::query(float deltaTime)
{
    Human::query(deltaTime);
//    MicroSecondSpeedTimer timer( [=](long long int time) {
//        globalGameStatistics->setTestValue("I Query: ", (double)time/1000.0);
//    });
    
    if(_lifeState == DEAD)
    {
        return;
    }

    if(_newCrewWeapon.isChanged())
    {
        _crewWeapon = _newCrewWeapon.get();
        _newCrewWeapon.clearChange();
    }
    
    if(lookForTargetCounter.count())
    {
        if(!(globalTestBed->behaviorsToSkip & LOOK_FOR_TARGET_BEHAVIOR))
        {
            lookForTarget();
        }
    }
    
    if(!(globalTestBed->behaviorsToSkip & UPDATE_WEAPON_BEHAVIOR))
    {
        updateCurrentWeapon();
    }
}

void Infantry::act(float deltaTime)
{
    Human::act(deltaTime);
    
    if(_lifeState == DEAD)
    {
        return;
    }
    
    if(_firing)
    {
        fireWeapon();
        if(_currentRecoil > 0)
        {
            _currentRecoil = _currentRecoil <= deltaTime ? 0: _currentRecoil - deltaTime;
        }
    }
}

void Infantry::updateReloadGoal(ReloadWeaponGoal * goal, float deltaTime)
{
    if(_workingOnGoal)
    {
        return;
    }
    
    if(goal->getWeapon()->getWeapaonState() == WEAPON_READY || goal->getWeapon()->getWeapaonState() == WEAPON_RECHAMBERING)
    {
        handleGoalComplete(goal);
    }
    else
    {
        if(goal->getWeapon()->isCrewWeapon())
        {
            if(_movingTowardGoal)
            {
                return;
            }
            _movingTowardGoal = true;
            CrewWeapon * weapon = dynamic_cast<CrewWeapon *>(goal->getWeapon());
            bool inWorkRange = moveTowardWorkEnt(weapon);
            // If we are able to work, and we are at the work postion, then work
            if(!_shouldLieProne && !_workingOnGoal && inWorkRange) //TODO fix magic number
            {
                _workingOnGoal = true;
                if(_velocity.isZero())
                {
                    setFacing(VectorMath::facingFromVector(weapon->getPosition() - getPosition()));
                }
                setPerformingAction(ACTION_HELP);
                weapon->addWorkToWeapon(deltaTime);
            }
        }
        else
        {
            _workingOnGoal = true;
            setPerformingAction(ACTION_RELOADING);
            goal->getWeapon()->addWorkToWeapon(deltaTime);
        }
    }
}

void Infantry::updateAttackTargetGoal(AttackTargetGoal * goal, float deltaTime)
{
    if((globalTestBed->behaviorsToSkip & ATTACK_TARGET_GOAL_BEHAVIOR))
    {
        return;
    }
    Human * target = goal->getTarget();
    // Check to see if the target is dead and we should stop firing
    if(target->getActorState() == DEAD)
    {
        stopFiring();
        _goals.erase(GOAL_ORIGIN_TARGET);
        return;
    }
    
    double distance = _position.distance(target->getPosition());
    
    // Check to see if the target should be charged
    if(!_movingTowardGoal)
    {
        //only charge if they are in the charging radius, AND they are in the same state of cover (or better) than this ent
        // (i.e. don't charge OUT of cover)
        if(goal->shouldCharge() ||
           (distance < _chargeRadius && target->getTotalSecurity() >= getTotalSecurity()))
        {
            _movingTowardGoal = true;
            goal->setCharging();
            if(target->getPosition().distanceSquared(_goalPosition.position) > 5) //FIXME: magic number
            {
                setGoalPosition(target->getPosition(), true);
            }
        }
    }
    
    if(goal->shouldSelectWeapon())
    {
        chooseWeaponForTarget(target->getPosition());
    }
    
    if(_currentWeapon == nullptr)
    {
        return;
    }
    
    // If the choosen weapon is a crew weapon, move to it
    if(_currentWeapon->isCrewWeapon())
    {
        if(_movingTowardGoal)
        {
            return;
        }
        _movingTowardGoal = true;
        Vec2 weaponWork = _currentWeapon->getWeaponWorkPosition();
        if(weaponWork.distanceSquared(_goalPosition.position) > POSITIONS_SAME_DISTANCE)
        {
            setGoalPosition(_currentWeapon->getWeaponWorkPosition(), false);
        }
        if(weaponWork.distanceSquared(_position) > POSITIONS_SAME_DISTANCE_SQ)
        {
            return;
        }
    }
    

    // already working on another goal, return
    if(_workingOnGoal)
    {
        return;
    }
    
    _workingOnGoal = true;
    // already shooting, return
    if(_firing)
    {
        return;
    }
    
    // Acquire the target if we haven't yet, or start shooting at it if we have. Either way, are attention and arms are now occupied
    if(_currentWeapon->getWeapaonState() != WEAPON_READY)
    {
        // Weapon isn't ready to fire (or there is no weapon) so give up
        return;
    }
    
    // wait until the target has been aquired (bring the target to the weapon's sight)
    if(goal->getTargetAcquireTime() > 0)
    {
        goal->decrementTargetAcquireTime(deltaTime);
        if(goal->getTargetAcquireTime() == 0)
        {
            _acquireTargetTimeModifier = 1.0;
        }
        return;
    }
        
    // See if the target is to close or far to actually attack
    if(_currentWeapon->getMaximumRange() < distance || _currentWeapon->getMinimumRange() > distance)
        return;
    
    if(_currentWeapon->getProjectileType() == MELEE)
    {
        setPerformingAction(ACTION_ATTACK);
        _currentWeapon->meleeAttackWithTarget(target, 0);
    }
    else
    {
        if(goal->shouldCheckTarget())
        {
            int height;
            // Check to see if something (or someone) is in the way todo FIX THIS IS TOO EXPENSIVE TO DO FREQUENTLY
            if(_currentWeapon->getProjectileType() == DIRECT && !canSeeOtherHuman(target, height))
            {
                // Something in the way, pick another target
                lookForTargetCounter.setToMaximum();
                _goals.erase(GOAL_ORIGIN_TARGET);
                _acquireTargetTimeModifier -= 0.5; //faster to reacquire
                return;
            }
            goal->setTargetHeight(height);
        }
        
        Vec2 targetPosition = target->getPosition();
        targetPosition.x += target->getCollisionBounds().size.width / 2; //shoot at the middle of the target
        targetPosition.y += target->getCollisionBounds().size.height / 2;
        
        
        Vec2 predictedPosition = getPredictedTargetPosition(targetPosition, target->getAverageVelocity());
        double firingAngle = getFiringAngle(predictedPosition);
        startFiringCurrentWeapon(firingAngle, predictedPosition, goal->getTargetHeight());
    }
}

void Infantry::updateAttackPositionGoal(AttackPositionGoal * goal, float deltaTime)
{
    if(_movingTowardGoal) // cant move and shoot at a position at the same time
    {
        return;
    }
    
    // If this infantry has a crew weapon, then always try to use it to attack a position
    if(_crewWeapon != nullptr)
    {
        _movingTowardGoal = true;
        Vec2 workPosition = _crewWeapon->getWeaponWorkPosition();
        if(workPosition.distanceSquared(_goalPosition.position) > POSITIONS_SAME_DISTANCE)
        {
            setGoalPosition(_crewWeapon->getWeaponWorkPosition());
            return;
        }
        else if(workPosition.distanceSquared(_position) > POSITIONS_SAME_DISTANCE_SQ)
        {
            return;
        }
    }
    
    if(_workingOnGoal)  //working on another goal, return
    {
        return;
    }

    _workingOnGoal = true;
    if(_firing)  //already firing, just return
    {
        return;
    }
    
    Vec2 position = goal->getRandomTargetPoint();
    chooseWeaponForTarget(position);
    if(_currentWeapon == nullptr)
    {
        return;
    }
    
    if(_currentWeapon->getWeapaonState() == WEAPON_READY)
    {
        double angle = getFiringAngle(position);
        startFiringCurrentWeapon(angle, position, 0);
    }
}

void Infantry::updateCurrentWeapon()
{
    if(_currentWeapon == nullptr)
    {
        // Select a default?
        chooseWeaponForTarget(getPosition() + Vec2(50,0));
        if(_currentWeapon == nullptr)
        {
            return;
        }
    }
    
    _currentWeapon->updateState();
    _currentWeapon->updateRandomValues();
    WEAPONSTATE weaponState = _currentWeapon->getWeapaonState();
    if(weaponState == WEAPON_NEEEDS_AMMO)
    {
        addAmmoToWeapon();
        weaponState = _currentWeapon->getWeapaonState();
    }
    
    if(weaponState == WEAPON_RELOADING || weaponState == WEAPON_UNSTORING)
    {
        if(_currentWeapon->isCrewWeapon())
        {
            if(_goals.at(GOAL_ORIGIN_RELOAD_CREW) == nullptr)
            {
                _owningUnit->crewWeaponNeedsWork(true);
            }
        }
        else if(_goals.at(GOAL_ORIGIN_RELOAD_PERSONAL) == nullptr)
        {
            ReloadWeaponGoal * reload = ReloadWeaponGoal::create(_currentWeapon);
            reload->setImportance(10);
            addGoal(GOAL_ORIGIN_RELOAD_PERSONAL, reload);
        }
    }
}

Human * Infantry::getCurrentTarget()
{
    AttackTargetGoal * goal = (AttackTargetGoal *) _goals.at(GOAL_ORIGIN_TARGET);
    if(goal != nullptr)
    {
        return goal->getTarget();
    }
    return nullptr;
}

void Infantry::addAmmoToWeapon()
{
    if(_currentWeapon == nullptr)
    {
        return;
    }
    int ammoToAdd = 0;
    int cost = _currentWeapon->getCostToReload();
    if(_currentWeapon->isCrewWeapon())
    {
        ammoToAdd = _owningUnit->getUnitAmmo(cost);
    }
    else
    {
        // Don't have enough ammo on hand, try to get some from supply
        if(_currentAmmoReserve < cost)
        {
            // Try to get ammo
            _currentAmmoReserve += getOwningCommand()->withdrawAmmo(this, _maxAmmoReserve - _currentAmmoReserve);
        }
        if(_currentAmmoReserve >= cost)
        {
            ammoToAdd = cost;
            _currentAmmoReserve -= ammoToAdd;
        }
    }
    
    if(ammoToAdd > 0)
    {
        _currentWeapon->addAmmo(_currentWeapon->getMagazineSize());
    }
}

void Infantry::handleGoalComplete(Goal * goal)
{
    if(goal->hasAssociatedOrder())
    {
        _owningUnit->memberCompletedGoal(goal->getAssociatedOrderId(), false);
        // tell the unit about the goal
    }
    Human::handleGoalComplete(goal);
}

Vec2 Infantry::getPredictedTargetPosition(Vec2 initialTargetPosition, Vec2 targetVelocity)
{
    Vec2 startPosition = getPosition();
    Vec2 startToEnd = initialTargetPosition - startPosition;
    Vec2 predicted = startPosition + VectorMath::computePredictedTargetPosition(_currentWeapon->getProjectileSpeed(), startToEnd, targetVelocity, _currentWeapon->getProjectileType());
    return predicted;
}
/**
 *
 *
 */
double Infantry::getFiringAngle(Vec2 targetPosition)
{
    double fireAngle = 0;
    Vec2 startPosition = getWeaponFirePosition();
    Vec2 startToEnd = targetPosition - startPosition;
    
    bool fireBackwards = startToEnd.x < 0;
    
    if(_currentWeapon->getProjectileType() == INDIRECT_LOW || _currentWeapon->getProjectileType() == INDIRECT_HIGH)
    {
        // stupid trick to make the gravity vector work properly
        if(fireBackwards)
        {
            startToEnd.x *= -1;
        }
        
        fireAngle = VectorMath::computeFiringAngleWithVelocity (_currentWeapon->getProjectileSpeed(), startToEnd.length(), _currentWeapon->getProjectileType());
        if(fireBackwards) {
            fireAngle = -fireAngle;
        }
    }
    
    return fireAngle;
}

void Infantry::aimAtTarget(AttackTargetGoal * targetGoal)
{
    // FIX wtf is this shit, NO, just wrong
    targetGoal->setTargetAcquireTime(_targetAquireSpeed * ( 1 - getPercentOfMaximumExperience()) * _acquireTargetTimeModifier); //reset the time to acquire a target
    Vec2 diff = targetGoal->getTarget()->getPosition() - getPosition();
    setFacing(VectorMath::facingFromVector(diff));
}

void Infantry::lookForTarget()
{
    Human * newTarget = getOwningUnit()->findTargetForActor(this);
    AttackTargetGoal * attack = (AttackTargetGoal *) _goals.at(GOAL_ORIGIN_TARGET);
    Human * currentTarget = attack != nullptr ? attack->getTarget() : nullptr;
    bool changeTarget = false;
    if(newTarget != nullptr && newTarget != currentTarget)
    {
        //if the current target is valid, check to see if the current target should be changed
        //to the new target
        if(currentTarget != nullptr && currentTarget->getActorState() != DEAD)
        {
            // compare distances to the current target and the new target, if the new target is closer then switch, bias slightly in favor of the current target
            double distance = currentTarget->getPosition().distanceSquared(getPosition());
            double newDistance = newTarget->getPosition().distanceSquared(getPosition()) * 1.1; //FIXME: FIX magic number
            if(newDistance < distance)
            {
                changeTarget = true;
            }
        }
        else
        {
            changeTarget = true;
        }
        
        
        if(changeTarget)
        {
            currentTarget = newTarget;
            if(attack != nullptr)
            {
                attack->setTarget(currentTarget);
            }
            else
            {
                attack = AttackTargetGoal::create(currentTarget);
                _goals.insert(GOAL_ORIGIN_TARGET, attack);
            }
            checkChargeCounter.setToMaximum();
            aimAtTarget(attack);
        }
    }
   
    if(attack != nullptr)
    {
        double targetDistance = currentTarget->getPosition().distance(getPosition());
        double stress = 100 / pow(globalVariableStore->getVariable(EnemyDistanceToStressExponent), targetDistance); // TODO fix magic formula???
        if(changeTarget)
        {
            addStress(stress);
        }
        attack->setImportance(1 + (stress / 12.5)); // This should range from 1 to 9
    }
}

float Infantry::getMinWeaponRange()
{
    float min = 999999999;
    for(Weapon * weapon : _weaponList)
    {
        if(weapon->getMinimumRange() < min)
        {
            min = weapon->getMinimumRange();
        }
    }
    if(_crewWeapon != nullptr)
    {
        if(_crewWeapon->getMinimumRange() < min)
        {
            min = _crewWeapon->getMinimumRange();
        }
    }
    return min;
}


float Infantry::getMaxWeaponRange()
{
    float max = 0;
    for(Weapon * weapon : _weaponList)
    {
        if(weapon->getMaximumRange() > max)
        {
            max = weapon->getMaximumRange();
        }
    }
    if(_crewWeapon != nullptr)
    {
        if(_crewWeapon->getMaximumRange() < max)
        {
            max = _crewWeapon->getMaximumRange();
        }
    }
    return max;
}

void Infantry::addCrewWeapon(CrewWeapon * weapon)
{
    _newCrewWeapon = weapon;
}

bool Infantry::isFriendNearLocation(Vec2 targetLocation)
{
    std::list<Entity *> friendlies;
    //make sure there are no friendlies nearby
    bool nearbyFriend = false;
    std::vector<CollisionCell *> cells;
    getCurrentCollisionGrid()->cellsInCircle(targetLocation, 5, &cells);
    for(auto cell : cells)
    {
        for(auto ent : cell->getMovingEntsInCell())
        {
            if(ent.second->getTeamID() == getTeamID())
            {
                nearbyFriend = true;
                break;
            }
        }
        if(nearbyFriend)
        {
            break;
        }
    }
    
    //                getCurrentCollisionGrid()->entitiesInCirlce(target->getPosition(), 32, getTeamID(), COLLIDE_OTHER_TEAM_HUMAN, &nearbyTargets);
    //                //if the enemy is not in cover, then there should be multiple enemies near each other (otherwise a waste of ammo)
    //                // FIX seems stupid to have this check here
    //                if(actorTarg->getConcealmentValue()
    //                {
    //                    currentPreference -=5;
    //                }

    return nearbyFriend;
}

void Infantry::chooseWeaponForTarget(Vec2 targetLocation)
{
    double distance = targetLocation.distance(getPosition());
    bool friendNear = isFriendNearLocation(targetLocation);
    
    Weapon * best = nullptr;
    int currentPreference;
    
    for(Weapon * weapon : _weaponList)
    {
        currentPreference = weapon->getPreferenceOrder();
        //check if the weapon is in range
        if(weapon->getMinimumRange() > distance || weapon->getMaximumRange() < distance)
        {
            currentPreference = -1;
        }
        
        //Make sure its appropriate to use the weapon (won't hit friendly and not overkill for the target)
        
        if(weapon->isExplosiveProjectile() && friendNear)
        {
            continue;
        }
        
        if(best == nullptr || currentPreference > best->getPreferenceOrder())
        {
            best = weapon;
        }
    }
    
    if(_crewWeapon != nullptr)
    {
        //check if the weapon is in range
        if(_crewWeapon->getMinimumRange() <= distance &&
           _crewWeapon->getMaximumRange() >= distance &&
           !(_crewWeapon->isExplosiveProjectile() && friendNear))
        {
            best = _crewWeapon;
        }
    }
    
    if(best != nullptr)
    {
        if(_currentWeapon != best)
        {
            _currentWeapon->setStored(true);
            best->setStored(false);
            _currentWeapon = best;
            if(_goals.at(GOAL_ORIGIN_RELOAD_PERSONAL) != nullptr)
            {
                _goals.erase(GOAL_ORIGIN_RELOAD_PERSONAL);
            }
        }
    }
}

Vec2 Infantry::getWeaponFirePosition()
{
    if(_currentWeapon->isCrewWeapon())
    {
        return _currentWeapon->getWeaponFirePosition();
    }
    else
    {
        return _position + _fireOffsets[_facing];
    }
}

void Infantry::startFiringCurrentWeapon(double angle, Vec2 position, int height)
{
    _targetPosition = position;
    _targetFireAngle = angle;
    _targetHeight = height;
    _firing = true;
    
    if(_takingCoverBehindEnt != -1)
    {
        PhysicalEntity * ent = dynamic_cast<PhysicalEntity *>( globalEntityManager->getEntity(_takingCoverBehindEnt));
        if(ent != nullptr)
        {
            _firePosition = VectorMath::positionOnLineBeyondRect(getWeaponFirePosition(), _targetPosition, ent->getCollisionBounds());
        }
    }
    
    setPerformingAction(ACTION_ATTACK);
}

void Infantry::fireWeapon()
{
    _currentWeapon->updateWeaponChamberState();
    WEAPONSTATE state = _currentWeapon->getWeapaonState();
    if(state != WEAPON_READY)
    {
        // Weapon isn't ready so don't actually fire
        if(state != WEAPON_RECHAMBERING)
        {
            // Weapon isn't just rechambering, meaning it has run out of ammo and the infantry must reload it
            _firing = false;
        }
        return;
    }
    
    _timeSinceLastVisibleAction = 0;
    if(_currentWeapon->getProjectileType() == MELEE)
    {
        return;
    }
    else
    {
        Vec2 firePosition = getWeaponFirePosition();
        if(_takingCoverBehindEnt != -1 && _firePosition != Vec2::ZERO)
        {
            firePosition = _firePosition;
        }
        
        _currentWeapon->fire(_targetFireAngle, firePosition, _targetPosition, _targetHeight);
        _firing = _currentWeapon->getWeapaonState() != WEAPON_RELOADING;
        
        if(!_firing)
        {
            _currentRecoil = 0;
        }
        else
        {
            _currentRecoil += _currentWeapon->getRecoil();
            if(_currentRecoil > 1)
            {
                _currentRecoil = 0;
                _firing = false;
                lookForTargetCounter.setToMaximum();
            }
        }
    }
}

void Infantry::startedMoving()
{
    _takingCoverBehindEnt = -1;
    _firePosition.setZero();
}

void Infantry::goalPositionReached()
{
    if(_lifeState == DEAD || _owningUnit == nullptr)
    {
        return;
    }
    DIRECTION enemyDirection = _owningUnit->getNearestEnemeyDirection();
    
    Vec2 direction = VectorMath::unitVectorFromFacing(enemyDirection);
    direction.x = ceil(direction.x);
    direction.y = ceil(direction.y);
    
    CollisionCell * cell = getCurrentCollisionGrid()->getCell(_primaryCollisionCell->getXIndex() + direction.x, _primaryCollisionCell->getYIndex() + direction.y);
    if(cell->getStaticEntity() != nullptr && getCurrentCollisionGrid()->canSeeAroundCoverAtPosition(cell, enemyDirection))
    {
        _takingCoverBehindEnt = cell->getStaticEntity()->uID();
    }
    
    if(enemyDirection != NONE)
    {
        setFacing(enemyDirection);
    }
}

void Infantry::enemySpotted(DIRECTION direction, float distance)
{
    double stress = 100 / pow(globalVariableStore->getVariable(EnemyDistanceToStressExponent), distance); // TODO fix magic formula???
    addStress(stress);
}

Vec2 Infantry::getUnitPosition()
{
    return _owningUnit->getAveragePosition();
}

CollisionCell * Infantry::getOptimumCellAroundCell(CollisionCell * center)
{
    if(_lifeState == DEAD)
    {
        return center;
    }
    
    DIRECTION enemyDirection = _owningUnit->getNearestEnemeyDirection();
    Vec2 enemeyDirectionVector = VectorMath::unitVectorFromFacing(enemyDirection);
    enemeyDirectionVector.x = ceil(enemeyDirectionVector.x);
    enemeyDirectionVector.y = ceil(enemeyDirectionVector.y);
    
    
    CollisionGrid * grid = world->getCollisionGrid(center->getCellPosition());
    CollisionCell * best = nullptr;
    short bestValue = -1;
    int searchRadius = 10;
            
    grid->iterateCellsInCircle(center->getCellPosition(), searchRadius, [this, &best, &bestValue, &grid, enemyDirection, enemeyDirectionVector] (CollisionCell * cell)
    {
        if(cell->isTraversable() && _owningTeam->tryToClaimPosition(cell, uID()))
        {
            short totalCover = cell->getTerrainCharacteristics().getTotalSecurity();
            if(enemyDirection != NONE)
            {
                CollisionCell * neighbor = grid->getCell(cell->getXIndex() + enemeyDirectionVector.x, cell->getYIndex() + enemeyDirectionVector.y);
                if(neighbor != theInvalidCell)
                {
                    totalCover += neighbor->getTerrainCharacteristics().getSecurityWithoutHeight();
                    //Prefer cover that you can shoot or see around
                    if(grid->canSeeAroundCoverAtPosition(neighbor, enemyDirection))
                    {
                        totalCover += 1; //FIXME: is this really a large enough benefit? How much should we prefer cover you can shoot around
                    }
                }
                
                // Add some of the benefit of the cover even if we aren't directly behind it, its better than nothing
                neighbor = grid->getCell(cell->getXIndex() + (enemeyDirectionVector.x * 2), cell->getYIndex() + (enemeyDirectionVector.y * 2));
                if(neighbor != theInvalidCell)
                {
                    totalCover += (neighbor->getTerrainCharacteristics().getSecurityWithoutHeight() / 2);
                }
                
            }
 
            if(totalCover > bestValue)
            {
                bestValue = totalCover;
                if(best != nullptr)
                {
                    _owningTeam->releasePositionClaim(best);
                }
                best = cell;
            }
            else
            {
                _owningTeam->releasePositionClaim(cell);
            }
        }
        return true;
    });
    
    if(best == nullptr)
    {
        return center;
    }

    return best;
}


int Infantry::hasUpdateToSendOverNetwork()
{
    int variables = Human::hasUpdateToSendOverNetwork();

    if(_infantryNetworkUpdate.firing != _firing)
    {
        variables++;
    }
    
    ENTITY_ID weaponId = _crewWeapon != nullptr ? _crewWeapon->uID() : 0;
    if(weaponId != _infantryNetworkUpdate.crewWeapon)
    {
        variables++;
    }
    
    weaponId = _currentWeapon != nullptr ? _currentWeapon->uID() : 0;
    if(weaponId != _infantryNetworkUpdate.currentWeapon)
    {
        variables++;
    }
    
    if(_infantryNetworkUpdate.targetPosition != _targetPosition)
    {
        variables++;
    }
    if(_infantryNetworkUpdate.targetFireAngle != _targetFireAngle)
    {
        variables++;
    }
    if(_infantryNetworkUpdate.targetHeight != _targetHeight)
    {
        variables++;
    }
    
    if(_infantryNetworkUpdate.currentAmmoReserve != _currentAmmoReserve)
    {
        variables++;
    }
    
    if(_currentWeapon != nullptr)
    {
        variables += _currentWeapon->checkForNetworkUpdate();
    }


    return variables;
}

void Infantry::saveNetworkUpdate(cereal::BinaryOutputArchive & archive)
{
    Human::saveNetworkUpdate(archive);
    unsigned short modifiedVars = 0;
    if(_infantryNetworkUpdate.firing != _firing)
    {
        modifiedVars += 1;
    }
    
    ENTITY_ID weaponId = _crewWeapon != nullptr ? _crewWeapon->uID() : 0;
    if(weaponId != _infantryNetworkUpdate.crewWeapon)
    {
        modifiedVars += 2;
    }
    
    weaponId = _currentWeapon != nullptr ? _currentWeapon->uID() : 0;
    if(weaponId != _infantryNetworkUpdate.currentWeapon)
    {
        modifiedVars += 4;
    }
    

    if(_infantryNetworkUpdate.targetPosition != _targetPosition)
    {
        modifiedVars += 8;
    }
    if(_infantryNetworkUpdate.targetFireAngle != _targetFireAngle)
    {
        modifiedVars += 16;
    }
    if(_infantryNetworkUpdate.targetHeight != _targetHeight)
    {
        modifiedVars += 32;
    }
    
    if(_infantryNetworkUpdate.currentAmmoReserve != _currentAmmoReserve)
    {
        modifiedVars += 64;
    }
    
    archive(modifiedVars);
    if(_infantryNetworkUpdate.firing != _firing)
    {
        _infantryNetworkUpdate.firing = _firing;
        archive(_firing);
    }
    
    weaponId = _crewWeapon != nullptr ? _crewWeapon->uID() : 0;
    if(weaponId != _infantryNetworkUpdate.crewWeapon)
    {
        _infantryNetworkUpdate.crewWeapon = weaponId;
        archive(weaponId);
    }
    
    weaponId = _currentWeapon != nullptr ? _currentWeapon->uID() : 0;
    if(weaponId != _infantryNetworkUpdate.currentWeapon)    
    {
        _infantryNetworkUpdate.currentWeapon = weaponId;
        archive(weaponId);
    }

    if(_infantryNetworkUpdate.targetPosition != _targetPosition)
    {
        _infantryNetworkUpdate.targetPosition = _targetPosition;
        archive(_targetPosition);
    }
    if(_infantryNetworkUpdate.targetFireAngle != _targetFireAngle)
    {
        _infantryNetworkUpdate.targetFireAngle = _targetFireAngle;
        archive(_targetFireAngle);
    }
    if(_infantryNetworkUpdate.targetHeight != _targetHeight)
    {
        _infantryNetworkUpdate.targetHeight = _targetHeight;
        archive(_targetHeight);
    }
    
    if(_infantryNetworkUpdate.currentAmmoReserve != _currentAmmoReserve)
    {
        _infantryNetworkUpdate.currentAmmoReserve = _currentAmmoReserve;
        archive(_currentAmmoReserve);
    }
    
    if(_currentWeapon != nullptr)
    {
        _currentWeapon->saveNetworkDate(archive);
    }

}

void Infantry::updateFromNetwork(cereal::BinaryInputArchive & archive)
{
    Human::updateFromNetwork(archive);
    unsigned short modifiedVars;
    archive(modifiedVars);
    
    if(modifiedVars & 1)
    {
        archive(_firing);
    }
    if(modifiedVars & 2)
    {
        ENTITY_ID crewWeapon;
        archive(crewWeapon);
        _crewWeapon = dynamic_cast<CrewWeapon *>(globalEntityManager->getEntity(crewWeapon));
    }
    if(modifiedVars & 4)
    {
        ENTITY_ID currentWeapon;
        archive(currentWeapon);
        if(_crewWeapon != nullptr && _crewWeapon->uID() == currentWeapon)
        {
            _currentWeapon = _crewWeapon;
        }
        for(auto weapon : _weaponList)
        {
            if(weapon->uID() == currentWeapon)
            {
                _currentWeapon = weapon;
                break;
            }
        }
    }

    if(modifiedVars & 8)
    {
        archive(_targetPosition);
    }
    if(modifiedVars & 16)
    {
        archive(_targetFireAngle);
    }
    if(modifiedVars & 32)
    {
        archive(_targetHeight);
    }
    if(modifiedVars & 64)
    {
        archive(_currentAmmoReserve);
    }
    
    if(_currentWeapon != nullptr)
    {
        _currentWeapon->updateFromNetworkData(archive);
    }
}

void Infantry::die()
{
    for(auto goalPair : _goals)
    {
        if(goalPair.second->hasAssociatedOrder())
        {
            _owningUnit->memberCompletedGoal(goalPair.second->getAssociatedOrderId(), true);
        }
    }
    _weaponList.clear();
    _newCrewWeapon.clearChange();
    _owningUnit->removeEntity(this);
    _owningUnit = nullptr;
    Human::die();
}

void Infantry::removeFromGame()
{
    if(_lifeState != DEAD)
    {
        _owningUnit->removeEntity(this);
        _owningUnit = nullptr;
        Human::die();
    }
    Human::removeFromGame();
}


void Infantry::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Human::saveToArchive(archive);
    archive(_targetPosition);
    archive(_targetFireAngle);
    archive(_targetHeight);
    archive(_firing);
    archive(_firePosition);
    archive(_currentRecoil);
    archive(_acquireTargetTimeModifier);
    archive(_maxAmmoReserve);
    archive(_currentAmmoReserve);
    archive(_targetAquireSpeed);
    archive(_chargeRadius);
    
    archive(_weaponList.size());
    for(auto weapon : _weaponList)
    {
        weapon->saveToArchive(archive);
    }
    if(_crewWeapon != nullptr)
    {
        archive(_crewWeapon->uID());
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
    
    if(_currentWeapon != nullptr)
    {
        if(_currentWeapon == _crewWeapon)
        {
            archive(9999);
        }
        else
        {
            for(int i = 0; i < _weaponList.size(); i++)
            {
                if(_weaponList.at(i) == _currentWeapon)
                {
                    archive(i);
                    break;
                }
            }
        }
    }
    else
    {
        archive(-1);
    }
    
    if(_newCrewWeapon.isChanged())
    {
        archive(_newCrewWeapon.get()->uID());
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
    archive(checkChargeCounter);
    archive(lookForTargetCounter);
    archive(_experience);
    if(_owningUnit != nullptr)
    {
        archive(_owningUnit->uID());
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
}

void Infantry::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Human::loadFromArchive(archive);
    archive(_targetPosition);
    archive(_targetFireAngle);
    archive(_targetHeight);
    archive(_firing);
    archive(_firePosition);
    archive(_currentRecoil);
    archive(_acquireTargetTimeModifier);
    archive(_maxAmmoReserve);
    archive(_currentAmmoReserve);
    archive(_targetAquireSpeed);
    archive(_chargeRadius);
    ENTITY_ID thingId;

    
    size_t weaponSize;
    archive(weaponSize);
    for(int i = 0; i < weaponSize; i++)
    {
        Weapon * weapon = Weapon::create();
        weapon->loadFromArchive(archive);
        _weaponList.pushBack(weapon);
    }
    
    archive(thingId);
    if(thingId != -1)
    {
        _crewWeapon = dynamic_cast<CrewWeapon *>(globalEntityManager->getEntity(thingId));
    }
    else
    {
        _crewWeapon = nullptr;
    }
    int weaponIndex;
    archive(weaponIndex);
    if(weaponIndex == 9999)
    {
        _currentWeapon = _crewWeapon;
    }
    else if(weaponIndex != -1)
    {
        _currentWeapon = _weaponList.at(weaponIndex);
    }
    else
    {
        _currentWeapon = nullptr;
    }
    
    archive(thingId);
    if(thingId != -1)
    {
        CrewWeapon * weapon = dynamic_cast<CrewWeapon *>(globalEntityManager->getEntity(thingId));
        _newCrewWeapon = weapon;
    }
    
    archive(checkChargeCounter);
    archive(lookForTargetCounter);
    archive(_experience);

    archive(thingId);
    if(thingId != -1)
    {
        _owningUnit = dynamic_cast<Unit *>(globalEntityManager->getEntity(thingId));
    }
    else
    {
        _owningUnit = nullptr;
    }
    
    InfantryDefinition * definition = (InfantryDefinition *) globalEntityFactory->getPhysicalEntityDefinitionWithHashedName(_hashedEntityName);
    _fireOffsets[RIGHT] = definition->_muzzlePositionOffset;
    _fireOffsets[UP_RIGHT] = definition->_muzzlePositionOffset;
    _fireOffsets[UP] = Vec2(definition->_muzzlePositionOffset.x /2, definition->_muzzlePositionOffset.y);
    _fireOffsets[UP_LEFT] = Vec2(0, definition->_muzzlePositionOffset.y);
    _fireOffsets[LEFT] = Vec2(0, definition->_muzzlePositionOffset.y);
    _fireOffsets[DOWN_LEFT] = Vec2(0, 0);
    _fireOffsets[DOWN] = Vec2(definition->_muzzlePositionOffset.x /2, 0);
    _fireOffsets[DOWN_RIGHT] = Vec2(definition->_muzzlePositionOffset.x, 0);
}


void Infantry::populateDebugPanel(GameMenu * menu, Vec2 location)
{
    Human::populateDebugPanel(menu, location);
    std::ostringstream stream;
    stream << std::fixed;
    stream.precision(0);
    
    if(_currentWeapon != nullptr)
    {
        _currentWeapon->populateDebugPanelForWeapon(menu, location);
    }
    else
    {
        stream << "No Weapon";
        menu->addText(stream.str());
    }
    
    stream.str("");
    stream << "Reserve Ammo: " << _currentAmmoReserve;
    menu->addText(stream.str());
    
    std::string targetText = "Target: ";
    AttackTargetGoal * attack = (AttackTargetGoal * ) _goals.at(GOAL_ORIGIN_TARGET);
    Human * target = attack != nullptr ? attack->getTarget() : nullptr;
    if(target != nullptr)
    {
        targetText += std::to_string(target->uID());
        menu->addText(targetText);
        std::string status;
        if(attack->getTargetAcquireTime() > 0)
        {
            status += "Acquiring: ";
            status += std::to_string(attack->getTargetAcquireTime());
        }
        menu->addText(status);
    }
    else
    {
        targetText += "none";
        menu->addText(targetText);
    }
    
    stream.precision(5);
    stream.str("");
    stream << " Z Pos: " << _sprite->getSprite()->getPositionZ();
    menu->addText(stream.str());
    
    stream.str("");
    stream << " Z Ind: " << _sprite->getSprite()->getZOrder();
    menu->addText(stream.str());
}
