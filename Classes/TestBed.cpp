//
//  TestBed.cpp
//  TrenchWars
//
//  Created by Paul Reed on 7/31/22.
//

#include "TestBed.h"
#include "cocos2d.h"
#include "EngagementManager.h"
#include "EntityDebugDrawer.h"
#include <cereal/archives/binary.hpp>
#include <sstream>
#include "EntityFactory.h"
#include "CollisionGrid.h"
#include "TeamManager.h"
#include "Team.h"
#include "NavigationSystem.h"
#include "GameCamera.h"
#include "Cloud.h"
#include "MapSector.h"
#include "SectorGrid.h"

TestBed * globalTestBed;

void TestBed::init()
{
    pathTesting = false;
    debugDrawer = EntityDebugDrawer::create();
    globalTestBed  = this;
    _clickAction = CLICK_ACTION_NONE;
    
    _debugCellHighlighter = CellHighlighter::create();
    _debugCellHighlighter->retain();
    updateGrids =  false;
    _drawCells = false;
    _debugCloud = nullptr;
    looksPerFrame = 5;
    visionType = 0;
    
    behaviorsToSkip = NULL_BEHAVIOR;
}


TestBed * TestBed::create()
{
    TestBed * testBed = new TestBed();
    testBed->init();
    testBed->autorelease();
    return testBed;
}



static void DebugOutput(ESteamNetworkingSocketsDebugOutputType eType, const char *pszMsg )
{
    SteamNetworkingMicroseconds time = SteamNetworkingUtils()->GetLocalTimestamp();
    LOG_DEBUG( "%10.6f %s\n", time*1e-6, pszMsg );
    fflush(stdout);
    if ( eType == k_ESteamNetworkingSocketsDebugOutputType_Bug )
    {
        fflush(stdout);
        fflush(stderr);
        //        NukeProcess(1);
    }
}


void TestBed::update(float deltaTime)
{
    if(updateGrids)
    {
        Vec2 cameraPosition = globalCamera->getCameraPositionWorld() * -1;

        globalNavigationSystem->updateNavigationGridDrawing(cameraPosition);
        
        if(_drawCells)
        {
            _debugCellHighlighter->hightlightCellsNearPoint(cameraPosition, 50);
        }
        
        updateGrids = false;
    }
}

void TestBed::toggleDrawCells()
{
    _drawCells = !_drawCells;
    if(_drawCells)
    {
        updateGrids = true;
    }
    else
    {
        _debugCellHighlighter->clearCells();
    }
}
/////////////////// Test Functions///////////////
///


void TestBed::toggleDebugDrawing()
{
    debugDrawer->toggleDrawTargets();
}

void TestBed::setCellHighlight(CELL_HIGHLIGHT_DATA_TYPE type)
{
    _debugCellHighlighter->setHightlightType(type);
    updateGrids = true;
}


DebugEntity * TestBed::createDebugEntityAtPosition(Vec2 position, const std::string & labelText, Vec2 labelOffset, bool hideOnNotVisible)
{
    CollisionCell * cell = world->getCollisionGrid()->getCellForCoordinates(position);
    DebugEntity * debug = DebugEntity::create(position, labelText, labelOffset, hideOnNotVisible);
    debug->retain();
    
    if(_collisionCellIdToDebugEntityMap.find(cell->uID()) == _collisionCellIdToDebugEntityMap.end())
    {
        _collisionCellIdToDebugEntityMap[cell->uID()] = std::list<DebugEntity *>();
    }
    _collisionCellIdToDebugEntityMap[cell->uID()].push_back(debug);
    return debug;
}


void TestBed::updateDebugEntityAtPosition(DebugEntity * debug, Vec2 oldPosition)
{
    CollisionCell * oldCell = world->getCollisionGrid()->getCellForCoordinates(oldPosition);
    for(auto it = _collisionCellIdToDebugEntityMap[oldCell->uID()].begin(); it != _collisionCellIdToDebugEntityMap[oldCell->uID()].end(); it++)
    {
        if(*it == debug)
        {
            _collisionCellIdToDebugEntityMap[oldCell->uID()].erase(it);
            break;
        }
    }
    
    CollisionCell * cell = world->getCollisionGrid()->getCellForCoordinates(debug->getPosition());
    _collisionCellIdToDebugEntityMap[cell->uID()].push_back(debug);
}

const std::list<DebugEntity *> & TestBed::getDebugEntitiesAtPosition(Vec2 position)
{
    CollisionCell * cell = world->getCollisionGrid()->getCellForCoordinates(position);
    return _collisionCellIdToDebugEntityMap[cell->uID()];
}

DebugEntity * TestBed::removeDebugEntity(DebugEntity * debug)
{
    CollisionCell * cell = world->getCollisionGrid()->getCellForCoordinates(debug->getPosition());
    for(auto it = _collisionCellIdToDebugEntityMap[cell->uID()].begin(); it != _collisionCellIdToDebugEntityMap[cell->uID()].end(); it++)
    {
        if((*it)->uID() == debug->uID())
        {
            _collisionCellIdToDebugEntityMap[cell->uID()].erase(it);
            break;
        }
    }
    debug->removeFromGame();
    debug->release();
}

void TestBed::checkDebugEntityHover(Vec2 position)
{
    for(auto debug :_hoveredDebugEnts)
    {
        debug->setVisible(false);
    }
    _hoveredDebugEnts.clear();
    DebugEntity * newHover = getDebugEntityNearPosition(position);
    if(newHover != nullptr)
    {
        newHover->setVisible(true);
        _hoveredDebugEnts.push_back(newHover);
    }
}

DebugEntity * TestBed::getDebugEntityNearPosition(Vec2 position)
{
    double bestDistance = 99999999999;
    DebugEntity * nearest = nullptr;
    std::vector<CollisionCell *> cells;
    world->getCollisionGrid()->cellsInCircle(position, 16, &cells);
    for(auto cell : cells)
    {
        if(_collisionCellIdToDebugEntityMap.find(cell->uID()) != _collisionCellIdToDebugEntityMap.end())
        {
            for(auto debug : _collisionCellIdToDebugEntityMap[cell->uID()])
            {
                if(debug->getPosition().distance(position) < bestDistance)
                {
                    bestDistance = debug->getPosition().distance(position);
                    nearest = debug;
                }
            }
        }
    }
    return nearest;
}


void TestBed::setMovingEntBehavior(MOVING_ENTITY_BEHAVIORS behavior, bool enabled)
{
    if(enabled)
    {
        behaviorsToSkip = behaviorsToSkip | behavior;
    }
    else
    {
        behaviorsToSkip = behaviorsToSkip & ~behavior;
    }
}


void TestBed::createTestCombatWave(Vec2 location)
{
    Command * player1 = globalTeamManager->getCommand(0);
    Command * player2 = globalTeamManager->getCommand(1);
    
    //    player1->createCommandPost(location);
    //    player2->createCommandPost(location);
    
    auto rect = world->getWorldSectionForPosition(Vec2())->_areaBounds;
    
    std::string unitName = "Bolt Action Squad";
    int count = 50;
    int count2 = 2;
    float teeam1Position = location.x - 1200;
    float teeam2Position = location.x + 1200;
    float yPosition = location.y - ((count / 2.0) * 200);
    
    for(int j = 0; j < count; j++)
    {
        for(int i = 0; i < count2; i++)
        {
            int xOffset = i * 50;
            MoveOrder * move1 = MoveOrder::create(Vec2(teeam2Position, yPosition));
            player1->addOrder(move1);
            MoveOrder * move2 = MoveOrder::create(Vec2(teeam1Position, yPosition));
            player2->addOrder(move2);

            
            Vec2 position = Vec2(teeam1Position - xOffset, yPosition);
            position = world->getCollisionGrid()->getValidMoveCellAroundPosition(position);
            Unit * unit = Unit::create(unitName, position, player1);

            unit->setOrder(move1);
            
            position = Vec2(teeam2Position + xOffset, yPosition);
            position = world->getCollisionGrid()->getValidMoveCellAroundPosition(position);
            unit = Unit::create(unitName, position, player2);
            unit->setOrder(move2);
        }
        yPosition += 200;
    }
}

void TestBed::createAttackWave(Vec2 location)
{
    Command * player = globalTeamManager->getCommand(1);

    FormationDefinition * formation = globalEntityFactory->getFormationDefinition("infantryAttack");
    if(formation != nullptr)
    {
        for(auto unitSpawn : formation->units)
        {
            Vec2 position = location + unitSpawn->location;
            position = world->getCollisionGrid()->getValidMoveCellAroundPosition(position);
            Unit * unit = Unit::create(unitSpawn->unitDefinitionName, position, player);

            
            Vec2 enemyLocation = globalTeamManager->getCommand(0)->getSpawnPosition();
            enemyLocation.x += globalRandom->randomDouble(-125.0, 125.0);
            enemyLocation.y += globalRandom->randomDouble(-300.0, 300.0);
            MoveOrder * moveOrder = MoveOrder::create(enemyLocation, false);
            player->addOrder(moveOrder);

            unit->setOrder(moveOrder);
        }
    }
}

void TestBed::createDefenseWave(Vec2 location)
{
    Command * player = globalTeamManager->getCommand(1);

    FormationDefinition * formation = globalEntityFactory->getFormationDefinition("infantryAttack");
    if(formation != nullptr)
    {
        for(auto unitSpawn : formation->units)
        {
            Vec2 position = location + unitSpawn->location;
            position = world->getCollisionGrid()->getValidMoveCellAroundPosition(position);
            Unit * unit = Unit::create(unitSpawn->unitDefinitionName, position, player);
            // TODO fix have the units dig in
        }
    }
}


void TestBed::createTestRandomUnits(Vec2 location)
{
    Command * player1 = globalTeamManager->getCommand(0);
    Command * player2 = globalTeamManager->getCommand(1);
    
    auto rect = world->getWorldSectionForPosition(Vec2())->_areaBounds;
    
    std::string unitName = "Bolt Action Squad";
    int count = 100;
    
    for(int j = 0; j < count; j++)
    {
        MoveOrder * move1 = MoveOrder::create(location, player1);
        player1->addOrder(move1);
        MoveOrder * move2 = MoveOrder::create(location, player2);
        player2->addOrder(move2);

        Vec2 spawn = Vec2( globalRandom->randomDouble(0.0f, rect.size.width * .6f), globalRandom->randomDouble(0.0f, rect.size.height));
        spawn = world->getCollisionGrid()->getValidMoveCellAroundPosition(spawn);
        Unit * unit = Unit::create(unitName, spawn, player1);
        unit->setOrder(move1);
        
        spawn = Vec2( globalRandom->randomDouble(rect.size.width * .4f, rect.size.width), globalRandom->randomDouble(0.0f, rect.size.height));
        spawn = world->getCollisionGrid()->getValidMoveCellAroundPosition(spawn);
        unit = Unit::create(unitName, spawn, player2);
        unit->setOrder(move2);
        
    }
}

void TestBed::createTestCloud(Vec2 location)
{
    _debugCloud = Cloud::create("smoke", location,3);
}

Cloud * TestBed::getTestCloudNearPosition(Vec2 position)
{
    if(_debugCloud != nullptr && _debugCloud->testCollisionWithPoint(position))
    {
        return _debugCloud;
    }
    return nullptr;
}

void TestBed::createHistorical()
{
}


void TestBed::renderSectorToImage(Rect rect)
{

}


void TestBed::doClickAction(InteractiveObject * objClicked, Vec2 location)
{
    switch (_clickAction)
    {
        case CLICK_ACTION_DAMAGE:
        {
            if(objClicked->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
            {
                Entity * ent = dynamic_cast<Entity *>(objClicked);
                if(ent->getEntityType() == INFANTRY ||
                   ent->getEntityType() == STATIC_ENTITY ||
                   ent->getEntityType() == COURIER ||
                   ent->getEntityType() == BUNKER ||
                   ent->getEntityType() == COMMANDPOST)
                {
                    PhysicalEntity * physEnt = (PhysicalEntity * ) ent;
                    LOG_DEBUG("Applying damage to %lld \n", physEnt->uID());
                    physEnt->receiveDamage(10000000);
                }
            }
        }
        case CLICK_ACTION_SPLIT:
        {
            CollisionCell * cell = world->getCollisionGrid()->getCellForCoordinates(location);
            TerrainCharacteristics terrain;
            terrain.set(0, .5, true, false);
//            cell->setTerrainCharacteristics(terrain);
            break;
        }
        case CLICK_ACTION_BOOM_300:
        {
            ProjectileDefinition * def = new ProjectileDefinition();
            def->_detonateParticleName = "Explosion.plist";
            def->_explosive = true;
            def->_spriteName = "bullet";
            Projectile::create(def, location, location, 0, 0, INDIRECT_LOW, 0, nullptr, 1000, 300);

            break;
        }
        case CLICK_ACTION_BOOM_800: 
        {
            ProjectileDefinition * def = new ProjectileDefinition();
            def->_detonateParticleName = "Explosion.plist";
            def->_explosive = true;
            def->_spriteName = "bullet";
            Projectile::create(def, location, location, 0, 0, INDIRECT_LOW, 0, nullptr, 1000, 800);
            break;
        }
        case CLICK_ACTION_BOOM_RANDOM:
        {
            location += Vec2(globalRandom->randomDouble(-50, 50), globalRandom->randomDouble(-50, 50));
            ProjectileDefinition * def = new ProjectileDefinition();
            def->_detonateParticleName = "Explosion.plist";
            def->_explosive = true;
            def->_spriteName = "bullet";
            Projectile::create(def, location, location, 0, 0, INDIRECT_LOW, 0, nullptr, 1000, globalRandom->randomInt(200, 1200));
            break;
        }
        case CLICK_ACTION_TREE: 
        {
            StaticEntity::create("tree", location);
            break;
        }
        case CLICK_ACTION_CLOUD:
        {
            Cloud::create("smoke", location,3);
            break;
        }
        case CLICK_ACTION_BUSH: {
            StaticEntity::create("bush", location);
            break;
        }
        case CLICK_ACTION_TRENCH: {
            StaticEntity::create("trench", location);
            break;
        }        
        case CLICK_ACTION_BUNKER: {
            Size testSize(5,5);
            Bunker * bunker = Bunker::create(location, testSize, RIGHT);
            break;
        }
        case CLICK_ACTION_FOREST: {
            Vec2 finalLocation;
            for(int i = 0; i < 30; i++)
            {
                finalLocation = location + Vec2(globalRandom->randomDouble(-50, 50), globalRandom->randomDouble(-50, 50));
                std::string name = "tree";
                StaticEntity * tree = StaticEntity::create(name, finalLocation);
            }
            break;
        }
        case CLICK_ACTION_TREE_LINE: {
            Vec2 finalLocation;
            for(int i = 0; i < 30; i++)
            {
                finalLocation.x = location.x;
                finalLocation.y = location.y + i;
                std::string name = "tree";
                StaticEntity * tree = StaticEntity::create(name, finalLocation);
            }
            break;
        }
        case CLICK_ACTION_TRENCH_LINE: {
            Vec2 finalLocation;
            for(int i = 0; i < 30; i++)
            {
                finalLocation.x = location.x;
                finalLocation.y = location.y + i;
                std::string name = "trench";
                StaticEntity * tree = StaticEntity::create(name, finalLocation);
            }
            break;
        }
    }
}

std::string TestBed::clickActionToName(CLICK_TEST_ACTION action)
{
    switch (action)
    {
        case CLICK_ACTION_NONE:
            return "None";
        case CLICK_ACTION_SPLIT:
            return "Split";
        case CLICK_ACTION_DAMAGE:
            return "Damage";
        case CLICK_ACTION_BOOM_800:
            return "Boom 800";
        case CLICK_ACTION_BOOM_300:
            return "Boom 300";     
        case CLICK_ACTION_BOOM_RANDOM:
            return "Boom Random";
        case CLICK_ACTION_TREE:
            return "Tree";
        case CLICK_ACTION_BUSH:
            return "Bush";
        case CLICK_ACTION_TRENCH:
            return "Trench";       
        case CLICK_ACTION_BUNKER:
            return "Bunker";
        case CLICK_ACTION_FOREST:
            return "Forest";
        case CLICK_ACTION_TREE_LINE:
            return "Tree Line";
        case CLICK_ACTION_TRENCH_LINE:
            return "Trench Line";
        case CLICK_ACTION_CLOUD:
            return "Cloud";
        default:
            return "Unknown";
    }

}


void TestBed::selectTestInfoUnitAtLocation(Vec2 location)
{
    
}


void TestBed::printPatchNumbers()
{
    if(addPatch > 0 || removePatch > 0 ||
       addBorder > 0  || removeBorder ||
       addNode > 0 || removeNode > 0 ||
       addEdge > 0 || removeEdge >  0)
    {
        LOG_DEBUG("NAV GRID STATS \n");
        LOG_DEBUG("Add Patch %d \n", addPatch.load());
        LOG_DEBUG("Remove Patch %d \n", removePatch.load());
        
        LOG_DEBUG("Add Border %d \n", addBorder.load());
        LOG_DEBUG("Remove Border %d \n", removeBorder.load());
        
        LOG_DEBUG("Add Node %d \n", addNode.load());
        LOG_DEBUG("Remove Node %d \n", removeNode.load());
        
        LOG_DEBUG("Add Edge %d \n", addEdge.load());
        LOG_DEBUG("Remove Edge %d \n", removeEdge.load());
        resetPatchNumbers();
    }
}


void TestBed::resetPatchNumbers()
{
    addPatch = 0;
    removePatch = 0;
    
    addBorder = 0;
    removeBorder = 0;
    
    addNode = 0;
    removeNode =0;
    
    addEdge = 0;
    removeEdge = 0;
}
