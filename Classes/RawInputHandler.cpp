//
//  RawInputHandler.cpp
//  TrenchWars
//
//  Created by Paul Reed on 8/16/20.
//

#include "RawInputHandler.h"
#include "EngagementManager.h"
#include "PlayerLayer.h"
#include "GameCamera.h"
#include "EmblemManager.h"
#include "UserInputParser.h"
#include "InputActionHandler.h"
#include "TestBed.h"
#include "LandscapeObjectiveBuilder.h"
#include "CollisionGrid.h"
#include "NavigationPatch.h"
#include "PolygonLandscapeObject.h"
#include "TeamManager.h"
#include "InteractiveObjectUtils.h"
#include "MapSector.h"
#include "UIController.h"

RawInputHandler::RawInputHandler(InputActionHandler * actionHandler, GameCamera * camera)
{
    _actionHandler = actionHandler;
    _gestureObject = nullptr;
    actionInProgress = nullptr;
    lastMouseScroll = 0;

    _gestureStarted = false;
    _drawKeyPressed = false;
    _camera = camera;
    _camera->retain();
    _hoveredHighlighter = InteractiveObjectHighlighter::create();
    _hoveredHighlighter->retain();

    
    auto keyboardListener = EventListenerKeyboard::create();
    keyboardListener->onKeyPressed = CC_CALLBACK_2(RawInputHandler::onKeyPressed, this);
    keyboardListener->onKeyReleased = CC_CALLBACK_2(RawInputHandler::onKeyReleased, this);
    
    mouseListener = EventListenerMouse::create();
    mouseListener->onMouseDown = CC_CALLBACK_1(RawInputHandler::onPointerDown, this);
    mouseListener->onMouseMove = CC_CALLBACK_1(RawInputHandler::onPointerMove, this);
    mouseListener->onMouseUp = CC_CALLBACK_1(RawInputHandler::onPointerUp, this);
    mouseListener->onMouseScroll = CC_CALLBACK_1(RawInputHandler::onPointerScroll, this);
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, globalPlayerLayer);
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mouseListener, globalPlayerLayer);
    
    currentChord.mouseDrag = false;
    currentChord.mouseInput = MOUSE_NONE;
    
    setupUserInput();
}

void RawInputHandler::setupUserInput()
{
    std::set<GAME_ACTION> undefinedActions {
       PAN_LEFT,
       PAN_RIGHT,
       PAN_UP,
       PAN_DOWN,
       PAN_MOUSE,
       ZOOM_IN,
       ZOOM_OUT,
       ZOOM_MOUSE,
       SELECT_OBJECT,
       ADD_REMOVE_SELECTION, //Add
       DRAG_OBJECT,
       SHOW_CONTEXT_MENU,
       DRAW_OBJECTIVE,
       MARQUEE_SELECT,
       DEBUG_MENU,
       TOGGLE_INFO,
       TOGGLE_GAME_MENU,
       TOGGLE_PAUSE,
       TEST_CLICK
   };
    
   UserInputParser * inputParser = new UserInputParser(this);
   inputParser->parseUserInputFile("UserInput.xml");
    
   for(InputAction * inputAction : _inputActions)
   {
       if(inputAction->chordToActivate.keys.size() == 0 && inputAction->chordToActivate.mouseInput == MOUSE_NONE)
       {
           LOG_DEBUG_ERROR("Invalid action definition! %d ", inputAction->action);
       }
       else
       {
           undefinedActions.erase(inputAction->action);
       }
   }
    
   for(GAME_ACTION action : undefinedActions)
   {
       InputAction * inputAction = new InputAction();
       inputAction->action = action;
       
       switch(action)
       {
           case PAN_LEFT:
               inputAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_LEFT_ARROW);
               break;
           case PAN_RIGHT:
                inputAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_RIGHT_ARROW);
               break;
           case PAN_UP:
               inputAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_UP_ARROW);
               break;
           case PAN_DOWN:
               inputAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_DOWN_ARROW);
               break;
           case PAN_MOUSE:
               inputAction->chordToActivate.mouseInput = MOUSE_RIGHT;
               inputAction->chordToActivate.mouseDrag = true;
               break;
           case ZOOM_IN:
               inputAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_X);
               break;
           case ZOOM_OUT:
               inputAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_C);
               break;
           case ZOOM_MOUSE:
               inputAction->chordToActivate.mouseInput = MOUSE_SCROLL;
               break;
           case SELECT_OBJECT:
               inputAction->chordToActivate.mouseInput = MOUSE_LEFT;
               break;
           case ADD_REMOVE_SELECTION:
               inputAction->chordToActivate.mouseInput = MOUSE_LEFT;
               inputAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_SHIFT);
               break;
           case DRAG_OBJECT:
               inputAction->chordToActivate.mouseInput = MOUSE_LEFT;
               inputAction->chordToActivate.mouseDrag = true;
               break;
           case SHOW_CONTEXT_MENU:
               inputAction->chordToActivate.mouseInput = MOUSE_RIGHT;
               break;
           case DRAW_OBJECTIVE:
               inputAction->chordToActivate.mouseInput = MOUSE_LEFT;
               inputAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_SPACE);
               inputAction->chordToActivate.mouseDrag = true;
               break;
           case MARQUEE_SELECT:
               inputAction->chordToActivate.mouseInput = MOUSE_LEFT;
               inputAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_SHIFT);
               inputAction->chordToActivate.mouseDrag = true;
               break;
           case CAMERA_RESET:
               inputAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_V);
               break;
           case DEBUG_MENU:
               inputAction->chordToActivate.mouseInput = MOUSE_RIGHT;
               inputAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_Q);
               break;
           case TOGGLE_INFO:
               inputAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_I);
               break;
           case TOGGLE_GAME_MENU:
               inputAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_ESCAPE);
               break;
           case TOGGLE_PAUSE:
               inputAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_P);
               break;
           case TEST_CLICK:
               inputAction->chordToActivate.mouseInput = MOUSE_LEFT;
               inputAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_F);
               break;

       }
       addInputAction(inputAction);
   }
}

void RawInputHandler::onPointerDown(EventMouse * event)
{
    // If the user is in the menu then don't bother
    auto location =  event->getLocationInView();
    if(globalMenuLayer->pointInMenu(location, false))
    {
        return;
    }
    globalMenuLayer->getPrimaryCircularMenu()->setVisible(false);
    _gestureStarted = true;
    gestureStartLocationScreen = location;
    gestureStartLocationGame = world->getWorldPosition(globalPlayerLayer->convertToNodeSpace(location)) * ONE_OVER_WORLD_TO_GRAPHICS_SIZE;
    lastGestureLocationScreen = gestureStartLocationScreen;
    gestureButton = event->getMouseButton();
    
    _gestureObject = getObjectAtPoint(gestureStartLocationGame);
    _hoveredHighlighter->clearObjects();
    
    //TODO FIX make this work for multiple layers
    CollisionCell * cell = world->getCollisionGrid(gestureStartLocationGame)->getCellForCoordinates(gestureStartLocationGame);
    NavigationPatch * patch = cell->getNavigationPatch();
    float moveCost = patch != nullptr ? patch->getMovementCost(MOVE_STRONGLY_PREFER_COVER) : -1;
    double congestion = patch != nullptr ? patch->getCongestionForTeam(9) : -1;
    double borderCon = -1;
    if(patch != nullptr)
    {
        for(auto neighbor : patch->getNeighbors())
        {
            if(neighbor.second->getCongestionForTeam(9) > borderCon)
            {
                borderCon = neighbor.second->getCongestionForTeam(9);
            }
        }
    }
    
    auto landscape = cell->getLandscapeObject();
    size_t defName = 0;
    if(landscape != nullptr)
    {
        defName = landscape->getHashedDefinitionName();
    }
    
    if(cell != nullptr)
    {
        int totalSecurity = cell->getMapSector() != nullptr ? cell->getMapSector()->getTotalSecurity() : -1;
        int teamSecurity = cell->getMapSector() != nullptr ? cell->getMapSector()->getAvailableTotalSecurityForTeam(0) : -1;
        LOG_DEBUG("Location: %.1f %.1f \n    Cell: %f %f \n  Landscape: %ld \n  Patch: %lld \n    Cover: %d Hard: %d Height %d Trav: %d Secur: %d \n    Move: %.1f Con %.2f BorCon %.1f \n    Sector Security: %f Available: %f \n",
               gestureStartLocationGame.x * WORLD_TO_GRAPHICS_SIZE, gestureStartLocationGame.y * WORLD_TO_GRAPHICS_SIZE,
               gestureStartLocationGame.x, gestureStartLocationGame.y,
               cell->getLandscapeObject() != nullptr ? cell->getLandscapeObject()->uID(): -1,
               patch != nullptr ?  patch->uID() : -1,
               cell->getTerrainCharacteristics().getCoverValue(),
               cell->getTerrainCharacteristics().getHardness(),
               cell->getTerrainCharacteristics().getHeight(),
               cell->isTraversable(),
               cell->getTerrainCharacteristics().getTotalSecurity(),
               moveCost, congestion, borderCon,
               totalSecurity, teamSecurity);
        if(cell->getStaticEntity() != nullptr)
        {
            LOG_DEBUG(" Ent %lld T: %d Z: %d \n", cell->getStaticEntity()->uID(), cell->getStaticEntity()->Entity::getEntityType(), cell->getStaticEntity()->getSprite()->getSprite()->getPositionZ());
        }
    }
    else
    {
        LOG_DEBUG("Location: %.1f %.1f \n", gestureStartLocationGame.x, gestureStartLocationGame.y);

    }
}

void RawInputHandler::onPointerMove(EventMouse * event)
{
    auto location =  event->getLocationInView();
    Vec2 gameLocation = world->getWorldPosition(globalPlayerLayer->convertToNodeSpace(location) * ONE_OVER_WORLD_TO_GRAPHICS_SIZE);
    lastMouseLocation = event->getLocationInView();

    if(globalMenuLayer->pointInMenu(location, false))
    {
        return;
    }
    if(_gestureStarted)
    {
        globalLandscapeObjectiveBuilder->update(gameLocation);
        if( gestureStartLocationScreen.distanceSquared(location) > DRAG_THRESHOLD)
        {
            currentChord.mouseInput = event->getMouseButton() == EventMouse::MouseButton::BUTTON_LEFT ? MOUSE_LEFT : MOUSE_RIGHT;
            currentChord.mouseDrag = true;
        }
        recognizeAction();
    }
    else
    {
        updateObjectHovering(gameLocation);
    }
    
    globalTestBed->checkDebugEntityHover(gameLocation);
    lastGestureLocationScreen = location;
}

void RawInputHandler::onPointerUp(EventMouse * event)
{
    auto location =  event->getLocationInView();
    Vec2 gameLocation = world->getWorldPosition(globalPlayerLayer->convertToNodeSpace(location) * ONE_OVER_WORLD_TO_GRAPHICS_SIZE);
    if(globalMenuLayer->pointInMenu(location, true) || !_gestureStarted)
    {
        return;
    }
    
    currentChord.mouseInput = event->getMouseButton() == EventMouse::MouseButton::BUTTON_LEFT ? MOUSE_LEFT : MOUSE_RIGHT;
    lastMouseLocation = event->getLocationInView();
    recognizeAction();

    currentChord.mouseInput = MOUSE_NONE;
    currentChord.mouseDrag = false;
    recognizeAction();

    lastGestureLocationScreen = location;
    _gestureObject = nullptr;
    _gestureStarted = false;
    updateObjectHovering(gameLocation);
}


void RawInputHandler::updateObjectHovering(Vec2 point)
{
    std::unordered_map<ENTITY_ID, InteractiveObject *> newHoveredObjects;
    std::list<InteractiveObject *> orderedEnts;

    // FIX this won't work if we aren't updating a collision grid (which we shouldn't for remote clients??? maybe?)
    CollisionCell * cell = world->getCollisionGrid()->getCellForCoordinates(point);

    //check for landscape ent
    if(cell->getLandscapeObject() != nullptr)
    {
        newHoveredObjects.emplace(cell->getLandscapeObject()->uID(), cell->getLandscapeObject());
        orderedEnts.push_back(cell->getLandscapeObject());
    }
    
    // Check to see if objectives and any proposed entities are being hovered
    Tester * tester = globalUIController->getTesterAtPoint(point);
    if(tester != nullptr)
    {
        newHoveredObjects.emplace(tester->uID(),tester);
        orderedEnts.push_back(tester);
    }
    for(auto team : globalTeamManager->getTeams())
    {
        Objective * obj = team->getObjectiveAtPoint(point);
        if(obj != nullptr)
        {
            newHoveredObjects.emplace(obj->uID(),obj);
            // TODO fix why are we doing this????
//            if(obj->hasTrenchLine())
//            {
//                for(auto trench : *obj->getTrenchLine())
//                {
//                    if(trench->getEntityType() == PROPOSED_TRENCH && trench->testCollisionWithPoint(point))
//                    {
//                        newHoveredObjects.emplace(trench->uID(),trench);
//                        orderedEnts.push_back(trench);
//                    }
//                }
//            }
//            if(obj->hasBunkers())
//            {
//                for(auto bunker : *obj->getBunkers())
//                {
//                    if(bunker->getEntityType() == PROPOSED_BUNKER && bunker->testCollisionWithPoint(point))
//                    {
//                        newHoveredObjects.emplace(bunker->uID(),bunker);
//                        orderedEnts.push_back(bunker);
//                    }
//                }
//            }
        }
    }
    
    // Check for static entity
    if(cell->getStaticEntity() != nullptr)
    {
        newHoveredObjects.emplace(cell->getStaticEntity()->uID(),cell->getStaticEntity());
        orderedEnts.push_back(cell->getStaticEntity());
    }

    for(auto ent : cell->getMovingEntsInCell())
    {
        if(ent.second->testCollisionWithCircle(point, 5))
        {
            newHoveredObjects.emplace(ent.second->uID(),ent.second);
            orderedEnts.push_back(ent.second);
        }
    }
    
    Emblem * emblem = globalEmblemManager->getEmblemAtPoint(point);
    if(emblem != nullptr)
    {
        newHoveredObjects.emplace(emblem->uID(), emblem);
        orderedEnts.push_back(emblem);
    }
        
    for(auto newEnt : orderedEnts)
    {
        if(_hoveredObjects.find(newEnt->uID()) == _hoveredObjects.end())
        {
            handleObjectHoverEnter(newEnt, point);
            _hoveredObjects.emplace(newEnt->uID(), newEnt->getInteractiveObjectType());
        }
    }
    
    std::vector<ENTITY_ID> removals;
    for(auto oldPair : _hoveredObjects)
    {
        if(newHoveredObjects.find(oldPair.first) == newHoveredObjects.end())
        {
            InteractiveObject * object = InteractiveObjectUtils::getInteractiveObject(oldPair.first, oldPair.second);
            if(object != nullptr)
            {
                handleObjectHoverLeave(object, point);
            }
            removals.push_back(oldPair.first);
        }
    }
    
    for(auto entId : removals)
    {
        _hoveredObjects.erase(entId);
    }
}

void RawInputHandler::handleObjectHoverEnter(InteractiveObject * ent, Vec2 gameLocation)
{
    _hoveredHighlighter->addObject(ent);
    _actionHandler->hoverEnter(ent, gameLocation);
    if(ent->getInteractiveObjectType() == EMBLEM_INTERACTIVE_OBJECT)
    {
        Emblem * emblem = dynamic_cast<Emblem *>(ent);
        emblem->emblemHoverEnter(gameLocation);
    }
}

void RawInputHandler::handleObjectHoverLeave(InteractiveObject * ent, Vec2 gameLocation)
{
    _actionHandler->hoverLeave(ent, gameLocation);
    _hoveredHighlighter->removeObject(ent);
    

    if(ent->getInteractiveObjectType() == EMBLEM_INTERACTIVE_OBJECT)
    {
        Emblem * emblem = dynamic_cast<Emblem *>(ent);
        emblem->emblemHoverLeave(gameLocation);
    }
}

void RawInputHandler::onPointerScroll(EventMouse * event)
{
    lastMouseScroll = event->getScrollY();
    currentChord.mouseInput = MOUSE_SCROLL;
    recognizeAction();
    currentChord.mouseInput = MOUSE_NONE;
}


InteractiveObject * RawInputHandler::getObjectAtPoint(Vec2 point)
{
    // Check the local command first
    Emblem * emblem = globalEmblemManager->getEmblemAtPoint(point);
    if(emblem != nullptr)
    {
        return emblem;
    }
    
    // FIX this won't work if we aren't updating a collision grid (which we shouldn't for remote clients??? maybe?)
    CollisionCell * cell = world->getCollisionGrid()->getCellForCoordinates(point);
    for(auto ent : cell->getMovingEntsInCell())
    {
        if(ent.second->testCollisionWithCircle(point, 5))
        {
            return ent.second;
        }
    }
    
    Tester * test = globalUIController->getTesterAtPoint(point);
    if(test != nullptr)
    {
        return test;
    }
    
    Cloud * cloud = globalTestBed->getTestCloudNearPosition(point);
    if(cloud != nullptr)
    {
        return cloud;
    }

    
    for(auto team : globalTeamManager->getTeams())
    {
        Objective * obj = team->getObjectiveAtPoint(point);
        if(obj != nullptr)
        {
            return obj;
        }
    }
    
    DebugEntity * debug = globalTestBed->getDebugEntityNearPosition(point);
    if(debug != nullptr)
    {
        return debug;
    }
    
    if(cell->getStaticEntity() != nullptr)
    {
        return cell->getStaticEntity();
    }
    return nullptr;
//    return cell->getLandscapeObject();
}


// Implementation of the keyboard event callback function prototype
void RawInputHandler::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
    currentChord.keys.insert(keyCode);
    recognizeAction();
}

void RawInputHandler::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
    currentChord.keys.erase(keyCode);
}

void RawInputHandler::addInputAction(InputAction * action)
{
    _inputActions.pushBack(action);
}

void RawInputHandler::recognizeAction()
{
    for(InputAction * action : _inputActions)
    {
        if(action->inputMatches(currentChord))
        {
            doAction(action, false);
            return;
        }
    }
    
    //We didn't match an action, see if there is an action in progress
    // If so, do the action and then set actionInProgress to null
    if(actionInProgress != nullptr)
    {
        doAction(actionInProgress, true);
        actionInProgress = nullptr;
    }
}


void RawInputHandler::doAction(InputAction * action, bool endAction)
{
    Director * gameDirector = cocos2d::Director::getInstance();
    Size viewSize = gameDirector->getVisibleSize();

    Vec2 screenChange;
    screenChange.x = viewSize.width /2;
    screenChange.y = viewSize.height /2;
    
    Vec2 gameLocation = world->getWorldPosition(globalPlayerLayer->convertToNodeSpace(lastMouseLocation) * ONE_OVER_WORLD_TO_GRAPHICS_SIZE);
    
    // If the action depends upon a mouse drag then set it as the action in progress
    if(action->chordToActivate.mouseDrag)
    {
        actionInProgress = action;
    }
    switch(action->action)
    {
        case PAN_LEFT:
            _camera->panScreen(Vec2(-100,0));
            break;
        case PAN_RIGHT:
            _camera->panScreen(Vec2(100,0));
            break;
        case PAN_UP:
            _camera->panScreen(Vec2(0,100));
            break;
        case PAN_DOWN:
            _camera->panScreen(Vec2(0,-100));
            break;
        case PAN_MOUSE:
            _camera->panScreen(lastGestureLocationScreen - lastMouseLocation);
            break;
        case ZOOM_IN:
            _camera->zoom(1.5, screenChange);
            break;
        case ZOOM_OUT:
            _camera->zoom(.5, screenChange);
            break;
        case ZOOM_MOUSE:
            _camera->zoom(1 - (lastMouseScroll /6), lastMouseLocation);
            break;
        case CAMERA_RESET:
            _camera->reset();
            break;
        case SELECT_OBJECT:
        {
            _actionHandler->selectObject(_gestureObject, gameLocation);
            break;
        }
        case ADD_REMOVE_SELECTION:
        {
            _actionHandler->modifySelection(_gestureObject);
            break;
        }
        case DRAG_OBJECT:
        {
            if(_gestureObject != nullptr && _gestureObject->getInteractiveObjectType() == EMBLEM_INTERACTIVE_OBJECT)
            {
                Emblem * emblem = dynamic_cast<Emblem *>(_gestureObject);
                emblem->emblemHoverLeave(gameLocation);
            }
            _actionHandler->dragObject(_gestureObject, gameLocation, endAction);
            _hoveredHighlighter->setObject(_gestureObject);

            break;
        }
        case SHOW_CONTEXT_MENU:
        {
            _actionHandler->orderObject(gameLocation);
            break;
        }
        case DRAW_OBJECTIVE:
        {
            _actionHandler->drawObjective(gameLocation, endAction);
            break;
        }
        case MARQUEE_SELECT:
        {
            _actionHandler->marqueeSelect(gameLocation, endAction);
            break;
        }
        case DEBUG_MENU:
        {
            _actionHandler->showTestMenu(gameLocation);
            break;
        }
        case TOGGLE_INFO:
        {
            _actionHandler->toggleInfoScreen();
            break;
        }
        case TOGGLE_GAME_MENU:
        {
            _actionHandler->toggleGameMenu();
            break;
        }
        case TOGGLE_PAUSE:
        {
            _actionHandler->togglePause();
            break;
        }
        case TEST_CLICK:
        {
            globalTestBed->doClickAction(_gestureObject, gameLocation);
        }
    }
}


void RawInputHandler::remove()
{
    _hoveredHighlighter->release();
}
