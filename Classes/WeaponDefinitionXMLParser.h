#ifndef __WEAPON_DEFINITION_XML_PARSER_H__
#define __WEAPON_DEFINITION_XML_PARSER_H__

//
//  WeaponDefinitionXMLParser.h
//  TrenchWars
//
//  Created by Paul Reed on 10/26/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#include "pugixml/pugixml.hpp"
#include "EntityFactory.h"
#include "cocos2d.h"

using namespace pugi;
USING_NS_CC;


class WeaponDefinitionXMLParser :public xml_tree_walker
{
    WeaponDefinition * _currentWeaponDefinition;
    EntityFactory *  _entityFactory;
    
public:
    WeaponDefinitionXMLParser(EntityFactory * factory);
    
    void parseFile(std::string const & filename);
    virtual bool for_each(xml_node& node);
};


#endif //__WEAPON_DEFINITION_XML_PARSER_H__
