//
//  PlanExecution.m
//  TrenchWars
//
//  Created by Paul Reed on 5/3/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//

#include "PlanExecution.h"
#include "Unit.h"
#include "AIStrategy.h"
#include "Command.h"
#include "EngagementManager.h"
#include "MapSectorAnalysis.h"
#include "Order.h"
#include "VectorMath.h"
#include "StaticEntity.h"
#include "GameVariableStore.h"
#include "MultithreadedAutoReleasePool.h"
#include "SectorGrid.h"
#include "MapSector.h"
#include "AICommand.h"

ExecutionTask::ExecutionTask()
{
    
}

bool ExecutionTask::init(void * object, TASK_TYPE type)
{
    taskObject = object; //FIXME: uggg
    taskState = TASK_NOT_STARTED;
    taskType = type;
    return true;
}

ExecutionTask * ExecutionTask::create(void * object, TASK_TYPE type)
{
    ExecutionTask * task = new ExecutionTask();
    
    if(task->init(object, type))
    {
        globalAutoReleasePool->addObject(task);
        return task;
    }
    CC_SAFE_DELETE(task);
    return nullptr;
}

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

ExecutionStage::ExecutionStage()
{
    
}

bool ExecutionStage::init(EXECUTION_STAGE_TYPE type)
{
    stageType = type;
    percentComplete = 0;
    nextStage = nullptr;
    stageState = NOT_STARTED;
    waitForAllUnitsToFinish = false;
    return true;
}

ExecutionStage * create(EXECUTION_STAGE_TYPE type)
{
    ExecutionStage * stage = new ExecutionStage();
    if(stage->init(type))
    {
        globalAutoReleasePool->addObject(stage);
        return stage;
    }
    
    CC_SAFE_DELETE(stage);
    return nullptr;
}

bool ExecutionStage::unitFinishedTask(Unit * unit)
{
    ExecutionTask * taskOfUnit = nullptr;
    for(ExecutionTask * task : tasks)
    {
        if(task->unitsWorkingOnTask.contains(unit))
        {
            task->taskState = TASK_COMPLETE;
            task->unitsWorkingOnTask.eraseObject(unit);
            taskOfUnit = task;
            break;
        }
    }
    if(taskOfUnit != nullptr && taskOfUnit->unitsWorkingOnTask.size() == 0 && taskOfUnit->taskState == TASK_COMPLETE)
    {
        tasks.eraseObject(taskOfUnit);
    }
    
    if(tasksNotAssigned == 0)
    {
        if(tasks.size() == 0)
        {
            stageState = COMPLETED_PLAN;
        }
        return true;
    }
    return tryAddingUnit(unit);
}

bool ExecutionStage::tryAddingUnit(Unit * unit)
{
    return false;
}


void ExecutionStage::removeUnit(Unit * unit)
{
    for(ExecutionTask * task : tasks)
    {
        if(task->unitsWorkingOnTask.contains(unit))
        {
            task->unitsWorkingOnTask.eraseObject(unit);
            if(task->unitsWorkingOnTask.size() == 0)
            {
                tasksNotAssigned++;
            }
        }
    }
}

void ExecutionStage::addtaskWithObject(void * object, TASK_TYPE type)
{
    ExecutionTask * newTask = ExecutionTask::create(object, type);
    tasks.pushBack(newTask);
    tasksNotAssigned++;
}

ExecutionTask * ExecutionStage::getTaskNearestUnit(Unit * unit, bool onlyUnassignedTasks)
{
    ExecutionTask * goalTask = nullptr;
    double distance = 99999999;
    double checkDistance;
    //Find the map sector nearest to this unit
    for(ExecutionTask * task : tasks)
    {
        if(!onlyUnassignedTasks || task->unitsWorkingOnTask.size() == 0)
        {
            MapSectorAnalysis * sector = (MapSectorAnalysis *) task->taskObject;
            checkDistance = sector->_sector->getSectorOrigin().distanceSquared(unit->getBoundingRect().origin);
            
            if(checkDistance < distance)
            {
                goalTask = task;
                distance = checkDistance;
            }
        }
    }
    return goalTask;
}

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

MoveStage::MoveStage()
{
    
}



bool MoveStage::init(bool waitForAllUnits)
{
    if(ExecutionStage::init(MOVE_STAGE))
    {
        waitForAllUnitsToFinish = waitForAllUnits;
        return true;
    }
    return false;
}

MoveStage * MoveStage::create(bool waitForAllUnits)
{
    MoveStage * stage = new MoveStage();
    if(stage->init(waitForAllUnits))
    {
        globalAutoReleasePool->addObject(stage);
        return stage;
    }
    CC_SAFE_DELETE(stage);
    return nullptr;
}

bool MoveStage::tryAddingUnit(Unit * unit)
{
    if(tasksNotAssigned == 0)
        return true;

    Vec2 position;
    MapSectorAnalysis * movePosition = nullptr;
    ExecutionTask * selectedTask = nullptr;
    for(ExecutionTask * task : tasks)
    {
        if(task->unitsWorkingOnTask.size() == 0)
        {
            movePosition = (MapSectorAnalysis *) task->taskObject;
            selectedTask = task;
            break;
        }
    }
    
    if(movePosition == nullptr)
    {
        return true;
    }
    
    position.x = movePosition->_sector->getSectorOrigin().x + (globalSectorGrid->getSectorGridSize().width / 2);
    position.y = movePosition->_sector->getSectorOrigin().y + (globalSectorGrid->getSectorGridSize().height / 2);
    
    MoveOrder * moveOrder = MoveOrder::create(position);
    unit->setOrder(moveOrder);
    
    selectedTask->unitsWorkingOnTask.pushBack(unit);
    selectedTask->taskState = TASK_IN_PROGRESS;
    
    tasksNotAssigned--;
    stageState = EXECUTING;
    return false;
}

bool MoveStage::unitFinishedTask(Unit * unit)
{
    ExecutionTask * selectedTask;
    for(ExecutionTask * task : tasks)
    {
        if(task->unitsWorkingOnTask.contains(unit))
        {
            task->unitsWorkingOnTask.eraseObject(unit);
            task->taskState = TASK_COMPLETE;
            selectedTask = task;
            break;
        }
    }
    if(selectedTask->unitsWorkingOnTask.size() == 0)
    {
        tasks.eraseObject(selectedTask);
    }
    
    if(tasksNotAssigned == 0 && tasks.size() == 0)
    {
        stageState = COMPLETED_PLAN;
    }

    return true;
}


///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
FortifyStage::FortifyStage()
{
    
}

bool FortifyStage::init()
{
    if(ExecutionStage::init(FORTIFY_STAGE))
    {
        return true;
    }
    return false;
}

FortifyStage * FortifyStage::create()
{
    FortifyStage * newStage = new FortifyStage();
    if(newStage->init())
    {
        globalAutoReleasePool->addObject(newStage);
        return newStage;
    }
    CC_SAFE_DELETE(newStage);
    return nullptr;
}

bool FortifyStage::tryAddingUnit(Unit * unit)
{
    if(tasksNotAssigned == 0)
        return true;
    
    ExecutionTask * goalTask = getTaskNearestUnit(unit, true);

    if(goalTask == nullptr)
    {
        //SHIT! This shouldn't happen
        CCLOG("I DIDNT FIND ANYTHING FOR %d TO DO!!!!!! ", unit->uID());
        return true;
    }
    
    MapSectorAnalysis * goalSector = (MapSectorAnalysis *)  goalTask->taskObject;
    //infantry units should dig a line, MG or Arty a box.... for now
    bool digAll = false;
    if(unit->getUnitType() > STANDARD_UNIT )
        digAll = true;
    
    Rect trenchRect;
    
    trenchRect.origin.x = goalSector->_sector->getSectorOrigin().x + (globalSectorGrid->getSectorGridSize().width / 2) - (unit->getDefaultUnitSize().width/2);
    trenchRect.origin.y = goalSector->_sector->getSectorOrigin().y + (globalSectorGrid->getSectorGridSize().height / 2) - (unit->getDefaultUnitSize().height/2);
    
    Vec2 trenchPositionEnd;
    trenchRect.size.width = (unit->getDefaultUnitSize().width);
    trenchRect.size.height = (unit->getDefaultUnitSize().height);
    
    //FIXME: TODO fix create an objective and dig trenches
    
    goalTask->unitsWorkingOnTask.pushBack(unit);
    goalTask->taskState = TASK_IN_PROGRESS;
    tasksNotAssigned--;
    stageState = EXECUTING;
    return false;
}

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

BuildCommandStage::BuildCommandStage()
{
    
}

bool BuildCommandStage::init()
{
    if(ExecutionStage::init(BUILD_COMMAND_STAGE))
    {
        return true;
    }
    return false;
}

BuildCommandStage * BuildCommandStage::create()
{
    BuildCommandStage * newStage = new BuildCommandStage();
    if(newStage->init())
    {
        globalAutoReleasePool->addObject(newStage);
        return newStage;
    }
    CC_SAFE_DELETE(newStage);
    return nullptr;
}

bool BuildCommandStage::tryAddingUnit(Unit * unit)
{
    if(tasksNotAssigned == 0)
        return true;
    
    ExecutionTask * goalTask = getTaskNearestUnit(unit, true);
    if(goalTask == nullptr)
    {
        //SHIT! This shouldn't happen
        CCLOG("I DIDNT FIND ANYTHING FOR %d TO DO!!!!!! ",unit->uID());
        return true;
    }
    

    //convert the task object from a MapSectorAnalysis to a CommandPost.... god this is awful
    if(goalTask->taskState == TASK_NOT_STARTED)
    {
        MapSectorAnalysis * analysis = (MapSectorAnalysis *) goalTask->taskObject;
        Vec2 position = analysis->_sector->getSectorOrigin();
        position.x += globalSectorGrid->getSectorGridSize().width/2;
        position.y += globalSectorGrid->getSectorGridSize().height/2;
        
        CommandPost * newPost = CommandPost::create(position, analysis->_owningCommand);
        goalTask->taskObject = newPost;
    }
    
    CommandPost * commandPost = (CommandPost *) goalTask->taskObject;

    // FIX update
    // UnitWorkOrder * workOrder = new UnitWorkOrder(0 , commandPost);
    //unit->setOrder(workOrder);
    
    goalTask->unitsWorkingOnTask.pushBack(unit);
    
    goalTask->taskState = TASK_IN_PROGRESS;
    
    tasksNotAssigned--;
    stageState = EXECUTING;
    return false;
}



///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
HoldStage::HoldStage()
{
    
}

bool HoldStage::init()
{
    if(ExecutionStage::init(HOLD_STAGE))
    {
        return true;
    }
    return false;
}


HoldStage * HoldStage::create()
{
    HoldStage * newStage = new HoldStage();
    if(newStage->init())
    {
        globalAutoReleasePool->addObject(newStage);
        return newStage;
    }
    CC_SAFE_DELETE(newStage);
    return nullptr;
}

bool HoldStage::tryAddingUnit(Unit * unit)
{
    if(tasksNotAssigned == 0)
        return true;
    
    ExecutionTask * goalTask = getTaskNearestUnit(unit, true);
    if(goalTask == nullptr)
    {
        //SHIT! This shouldn't happen
        CCLOG("I DIDNT FIND ANYTHING FOR %ld TO DO!!!!!! ",unit->uID());
        return true;
    }
    
    goalTask->unitsWorkingOnTask.pushBack(unit);
    goalTask->taskState = TASK_IN_PROGRESS;
    tasksNotAssigned--;
    stageState = EXECUTING;
    return false;
}

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

BombardStage::BombardStage()
{
    
}

bool BombardStage::init()
{
    if(ExecutionStage::init(BOMBARDMENT_STAGE))
    {
        return true;
    }
    return false;
}


BombardStage * BombardStage::create()
{
    BombardStage * newStage = new BombardStage();
    if(newStage->init())
    {
        globalAutoReleasePool->addObject(newStage);
        return newStage;
    }
    CC_SAFE_DELETE(newStage);
    return nullptr;
}

bool BombardStage::tryAddingUnit(Unit * unit)
{
    stageState = EXECUTING;
    return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


PlanExecution::PlanExecution()
{
    
}

bool PlanExecution::init(AIStrategy * strategy)
{
    _strategyToExecute = strategy;
    executionState = NOT_STARTED;
    _unitsInHoldingStage = false;
    
    numberOfUnitsNeeded = _strategyToExecute->requiredUnitStrenth;
    
    if(_strategyToExecute->_goalType == BOMBARD)
    {
        createStage(BOMBARDMENT_STAGE, false);
    }
    else if(_strategyToExecute->_goalType == BUILD_COMMAND_POST)
    {
        createStage(MOVE_STAGE , false);
        createStage(BUILD_COMMAND_STAGE , false);
    }
    else if(_strategyToExecute->_goalType == FORITY)
    {
        createStage(FORTIFY_STAGE , false);
    }
    else if(_strategyToExecute->_goalType == OCCUPY)
    {
        //put a stage in here for moving to the jump off point
        createStage(MOVE_STAGE , false);
        createStage(HOLD_STAGE , false);
    }
    return true;
}

PlanExecution * PlanExecution::create(AIStrategy * plan)
{
    PlanExecution * newPlan = new PlanExecution();
    if(newPlan->init(plan))
    {
        globalAutoReleasePool->addObject(newPlan);
        return newPlan;
    }
    CC_SAFE_DELETE(newPlan);
    return nullptr;
}


void PlanExecution::updateNumberOfUnitsNeeded(int requiredStregthChange)
{
    numberOfUnitsNeeded += requiredStregthChange;
}

void PlanExecution::addUnit(Unit * unit)
{
    numberOfUnitsNeeded--;
    ExecutionStage * stage = executionStages.front();
    addUnitToStage(unit, stage);
}

void PlanExecution::removeUnit(Unit * unit)
{
    unitFailedTask(unit);
}

void PlanExecution::stop()
{
    
}

void PlanExecution::addUnitToStage(Unit * unit, ExecutionStage * stage)
{
    std::string tabs;
    while(stage != nullptr)
    {
        if(! stage->tryAddingUnit(unit) || stage->waitForAllUnitsToFinish)
        {
            _unitWorkingOnStageDictionary.insert(unit->uID(), stage);
            executionState = stage->stageState;
            participatingUnits.pushBack(unit);
            break;
        }
        tabs.append("   "); // WTF is this for??
        
        stage = stage->nextStage;
    }
    if(stage == nullptr)
    {
        numberOfUnitsNeeded = 0;
        _strategyToExecute->_owningCommand->removeUnitFromStrategy(unit);
        participatingUnits.eraseObject(unit);
    }
}

void PlanExecution::unitFinishedTask(Unit * unit)
{
    ExecutionStage * stage = _unitWorkingOnStageDictionary.at(unit->uID());
    if(stage != nullptr)
    {
        bool unitReadyForNextStage = stage->unitFinishedTask(unit);
        if(unitReadyForNextStage)
        {
            ExecutionStage * nextStage = stage->nextStage;
            if(nextStage != nullptr)
            {
                if(stage->waitForAllUnitsToFinish == false)
                {//go tot he next stage
                    addUnitToStage(unit, nextStage);
                }
                else if(stage->stageState == COMPLETED_PLAN)
                {
                    for(Unit * otherUnit : participatingUnits)
                    {
                        addUnitToStage(otherUnit, nextStage);
                    }
                }
                executionState = nextStage->stageState;
                _unitsInHoldingStage = nextStage->stageType == HOLD_STAGE? true : false;
            }
            else
            {
                _strategyToExecute->_owningCommand->removeUnitFromStrategy(unit);
                participatingUnits.eraseObject(unit);

                _unitWorkingOnStageDictionary.erase(unit->uID());
                if(stage->stageState == COMPLETED_PLAN)
                {
                    executionState = COMPLETED_PLAN;
                }
            }
        }
    }
    else
    {
        //this should never happen
        CCLOG("Shit! ");
    }
}

void PlanExecution::unitFailedTask(Unit * unit)
{
    participatingUnits.eraseObject(unit);

    ExecutionStage * stage = _unitWorkingOnStageDictionary.at(unit->uID());
    
    if(stage != nullptr)
    {
        stage->removeUnit(unit);
    }
    _strategyToExecute->_owningCommand->removeUnitFromStrategy(unit);
    
    numberOfUnitsNeeded++;
}


void PlanExecution::prepareBombardmentStage(ExecutionStage * stage)
{
    Vec2 averagePosition;
    Rect area;
    // FIX gross
    area.size.width = -9999999;
    area.size.height = -9999999;
    area.origin.x = 9999999;
    area.origin.y = 9999999;
    
    sectorsInStragegyMutex.lock();
    
    for(MapSectorAnalysis * sector : _strategyToExecute->sectorsInStrategy)
    {
        
        averagePosition.x += sector->_sector->getSectorOrigin().x + (globalSectorGrid->getSectorGridSize().width / 2);
        averagePosition.y += sector->_sector->getSectorOrigin().y + (globalSectorGrid->getSectorGridSize().height / 2);
        
        if(area.origin.x > sector->_sector->getSectorOrigin().x)
            area.origin.x = sector->_sector->getSectorOrigin().x;
        if(area.origin.y > sector->_sector->getSectorOrigin().y)
            area.origin.y = sector->_sector->getSectorOrigin().y;
        if(area.size.width < sector->_sector->getSectorOrigin().x)
            area.size.width = sector->_sector->getSectorOrigin().x;
        if(area.size.height < sector->_sector->getSectorOrigin().y)
            area.size.height = sector->_sector->getSectorOrigin().y;
    }
    averagePosition.x /= _strategyToExecute->sectorsInStrategy.size();
    averagePosition.y /= _strategyToExecute->sectorsInStrategy.size();
    sectorsInStragegyMutex.unlock();
    
    area.size.width = area.size.width - area.origin.x + globalSectorGrid->getSectorGridSize().width;
    area.size.height = area.size.height - area.origin.y + globalSectorGrid->getSectorGridSize().height;
        
    ExecutionStage * previousStage = executionStages.back();
    previousStage->nextStage = stage;
    executionStages.pushBack(stage);
}


void PlanExecution::createStage(EXECUTION_STAGE_TYPE stageType, bool waitForAllUnits)
{
    ExecutionStage * stage = nullptr;
    if(stageType == MOVE_STAGE)
    {
        stage  = MoveStage::create(waitForAllUnits);
    }
    else if(stageType == FORTIFY_STAGE)
    {
        stage  = FortifyStage::create();
    }
    else if(stageType == BUILD_COMMAND_STAGE)
    {
        stage  = BuildCommandStage::create();
    }
    else if(stageType == HOLD_STAGE)
    {
        stage  = HoldStage::create();
    }
    else if(stageType == BOMBARDMENT_STAGE)
    {
        stage  = BombardStage::create();
    }
    ExecutionStage * previousStage = executionStages.back();
    previousStage->nextStage = stage;
    executionStages.pushBack(stage);
    
    sectorsInStragegyMutex.lock();

    for(MapSectorAnalysis * analysis : _strategyToExecute->sectorsInStrategy)
    {
        stage->addtaskWithObject(analysis,MAP_SECTOR_ANALYSIS_TASK);
    }
    sectorsInStragegyMutex.unlock();
}


void PlanExecution::deleteExecution()
{
    StopOrder * stop = StopOrder::create();
    for(Unit * unit : participatingUnits)
    {
        unit->setOrder(stop);
        _strategyToExecute->_owningCommand->removeUnitFromStrategy(unit);
    }
    participatingUnits.clear();
    for(ExecutionStage * stage : executionStages)
    {
        if(stage->stageState != NOT_STARTED)
        {
            for(ExecutionTask * task : stage->tasks)
            {
                if(task->taskType == ENTITY_TASK)
                {
                    PhysicalEntity * ent = (PhysicalEntity *) task->taskObject;
                    if(! ent->isWorkComplete()) //ent->_entityState < DEAD
                    {
                        ent->removeFromGame();
                    }
                }
            }
        }
    }
}

std::string PlanExecution::stringForExecutionState()
{
    if(executionState == NOT_STARTED)
    {
        return "NOT STARTED";

    }
    if(executionState == EXECUTING)
    {
        return "EXECUTING";
    }
    if(executionState == ALL_TASKS_ASSIGNED)
    {
        return "ALL_TASKS_ASSIGNED";
    }
    if(executionState == COMPLETED_PLAN)
    {
        return "NOT STARTED";
    }
    if(executionState == FAILED)
    {
        return "FAILED";
    }
    return "UNKNOWN";
}

