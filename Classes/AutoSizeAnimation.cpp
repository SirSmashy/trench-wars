//
//  AutoSizeAnimation.cpp
//  TrenchWars
//
//  Created by Paul Reed on 12/29/22.
//

#include "AutoSizeAnimation.h"

bool AutoSizeAnimation::init(EntityAnimation *animation, Size size)
{
    if(Animate::initWithAnimation(animation->_animation))
    {
        _entityAnimation = animation;
        
        // If a size is specified, determine the scale by grabbing the first frame and dividing the size by the frame's rect
        if(size.width != -1 && _entityAnimation->_animation->getFrames().size() != 0)
        {
            SpriteFrame * frame = _entityAnimation->_animation->getFrames().at(0)->getSpriteFrame();
            _spriteScale.x = size.width / frame->getRect().size.width;
            _spriteScale.y = size.height / frame->getRect().size.height;
        }
        else
        {
            _spriteScale = Vec2(-1,-1);
        }
        
        return true;
    }
    return false;
}

AutoSizeAnimation * AutoSizeAnimation::create(EntityAnimation *animation, Size size)
{
    AutoSizeAnimation * newAnimation = new AutoSizeAnimation();
    if(newAnimation->init(animation, size))
    {
        newAnimation->autorelease();
        return newAnimation;
    }
    CC_SAFE_DELETE(newAnimation);
    return nullptr;
}

void AutoSizeAnimation::startWithTarget(Node *target)
{
    Animate::startWithTarget(target);
    if(_spriteScale.x != -1)
    {
        target->setScale(_spriteScale.x, _spriteScale.y);
    }
}
