//
//  InputAction.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/31/20.
//

#ifndef InputAction_h
#define InputAction_h

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;
using namespace cocos2d::ui;

enum MOUSE_INPUT
{
    MOUSE_NONE,
    MOUSE_LEFT,
    MOUSE_RIGHT,
    MOUSE_MIDDLE,
    MOUSE_SCROLL
};

enum GAME_ACTION
{
    PAN_LEFT,
    PAN_RIGHT,
    PAN_UP,
    PAN_DOWN,
    PAN_MOUSE,
    ZOOM_IN,
    ZOOM_OUT,
    ZOOM_MOUSE,
    CAMERA_RESET,
    SELECT_OBJECT,
    ADD_REMOVE_SELECTION, //Add
    DRAG_OBJECT,
    SHOW_CONTEXT_MENU,
    DRAW_OBJECTIVE,
    MARQUEE_SELECT,
    DEBUG_MENU,
    TOGGLE_INFO,
    TOGGLE_GAME_MENU,
    TOGGLE_PAUSE,
    TEST_CLICK
};

struct InputChord
{
    std::set<EventKeyboard::KeyCode> keys;
    MOUSE_INPUT mouseInput;
    bool mouseDrag;
    bool operator==(InputChord & other);
};

class InputAction: public Ref
{
public:
    InputChord chordToActivate;
    GAME_ACTION action;
public:
    bool inputMatches(InputChord chord);
    InputAction();
    
};
#endif /* InputAction_h */
