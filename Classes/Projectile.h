#ifndef __PROJECTILE_H__
#define __PROJECTILE_H__

//
//  Projectile.h
//  TrenchWars
//
//  Created by Paul Reed on 3/17/12.
//  Copyright (c) 2012 . All rights reserved.
//
#include "PhysicalEntity.h"
#include "Weapon.h"
#include "cocos2d.h"


class Human;
class CollisionCell;
class MapSector;
class ProjectileDefinition;

class Projectile : public PhysicalEntity
{
protected:
    
    ProjectileDefinition * _projectileDefinition;

    Human * _owner;
    WEAPON_PROJECTILE_TYPE _projectileType;
    
    Vec2 _projectileStart;
    Vec2 _collisionCheckStart;
    
    int _startHeight;
    double  _startTime;
    
    Vec2 _projectileVelocity;
    double  _verticalVelocity;
    double  _maxDistance;
    double  _projectileHeight;
    double  _distanceTravelled;
    double  _speed;
    
    float   _penetrationPower;
    int     _attackDamage;
    float   _damageChance;
    float   _damageChanceChange;
    
    bool _collided;
        
    CollisionCell * secondToLastCellTravelledThrough;
    std::unordered_map<int, MapSector *> sectorsEncounted;
    
    bool tryDamagingEntity(PhysicalEntity * ent, double rangeModifier);

    void moveStraight(float deltaTime);
    void moveArc(float deltaTime);
    
CC_CONSTRUCTOR_ACCESS :
    
    Projectile();
    virtual bool init(ProjectileDefinition * definition, Vec2 position, Vec2 goalPosition, double fireAngle, int height, WEAPON_PROJECTILE_TYPE type, double fizzleDistance, Human * owner, float speedOverride, int damageOverride);
    
    
    void checkForCollisions();
    virtual void createExplosion();
    void createCratorAtLocation(Vec2 location, float diameter);
public:

    ~Projectile();
public:
    static Projectile * create(ProjectileDefinition * definition, Vec2 position, Vec2 goalPosition, double fireAngle, int height, WEAPON_PROJECTILE_TYPE type, double fizzleDistance, Human * owner, float speedOverride = -1, int damageOverride = -1 );
    
    static Projectile * create();
    
    virtual ENTITY_TYPE getEntityType() const {return PROJECTILE;}
    
    virtual bool shouldQuery() {return true;}
    virtual bool shouldAct() {return true;}

    virtual void setPosition(Vec2 position);
    virtual void query(float deltaTime);
    virtual void act(float deltaTime);

    virtual void removeFromGame();
        
    virtual bool isNetworkEntity() {return false;}

    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};


#endif // __PROJECTILE_H__
