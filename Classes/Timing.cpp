//
//  Timing.cpp
//  TrenchWars
//
//  Created by Paul Reed on 5/9/20.
//

#include "Timing.h"


MicroSecondSpeedTimer::MicroSecondSpeedTimer(long long int startTime, const std::function<void(long long int)> & timerCompleteCallBack)
{
    this->startTime = startTime;
    callBackFunction = timerCompleteCallBack;
    timerRunning = true;
}


MicroSecondSpeedTimer::MicroSecondSpeedTimer(const std::function<void(long long int)> & timerCompleteCallBack)
{
    startTime = CURRENT_TIME_MICRO;
    callBackFunction = timerCompleteCallBack;
    timerRunning = true;
}


MicroSecondSpeedTimer::~MicroSecondSpeedTimer()
{
    if(timerRunning)
    {
        callBackFunction(CURRENT_TIME_MICRO - startTime);
    }
}

void MicroSecondSpeedTimer::stop()
{
    timerRunning = false;
    callBackFunction(CURRENT_TIME_MICRO - startTime);
}
