#ifndef __ACTOR_H__
#define __ACTOR_H__

//  Human.h
//  TrenchWars
//
//  Created by Paul Reed on 2/20/12.
//  Copyright (c) 2012 . All rights reserved.
//

#include "MovingPhysicalEntity.h"
#include "Weapon.h"
#include "PrimativeDrawer.h"
#include "TerrainCharacteristics.h"
#include "ChangeTrackingVariable.h"
#include "Goal.h"
#include "Order.h"
#include "DoubleBuffer.h"

#define SELECTGOALMAX 10
#define HIDDEN_IN_CONCEALMENT_RANGE 300
#define HIDDEN_IN_HEAVY_COVER_RANGE 400
#define HIDDEN_IN_LIGHT_COVER_RANGE 500
#define TIME_BEFORE_REMOVING_CORPSE 60000

#define MAXIMUM_EXPERIENCE 10000

class HumanDefinition;
class MapSector;
class Unit;


enum LIFE_STATE
{
    ALIVE,
    DEAD,
};

enum STRESS_STATE
{
    STRESS_CALM,
    STRESS_WORRIED,
    STRESS_SEEK_COVER,
    STRESS_PANIC
};

struct HumanNetworkUpdate
{
    bool shouldDie =  false;
    ENTITY_ID takingCoverBehindEnt = 0;
    float stress = 0;
    bool prone = false;
};

enum GOAL_ORIGIN
{
    GOAL_ORIGIN_ORDER,
    GOAL_ORIGIN_COVER,
    GOAL_ORIGIN_PRONE,
    GOAL_ORIGIN_RELOAD_PERSONAL,
    GOAL_ORIGIN_RELOAD_CREW,
    GOAL_ORIGIN_HELP,
    GOAL_ORIGIN_TARGET
};


// A physical entity that is either a human or a crew-served weapon. Actors can be killed, move about the game world, and pursue goals.
class Human : public MovingPhysicalEntity
{
protected:
    LIFE_STATE _lifeState;
    Map<GOAL_ORIGIN, Goal *> _goals;
    RefMapChangeTrackingVariable<GOAL_ORIGIN, Goal> _pendingGoals;

    Goal * _currentMoveGoal;
    Goal * _currentActionGoal;
    
    int     _health;
    int     _maxHealth;
    AtomicChangeTrackingVariable<int> _pendingHealthChange;
    
    float   _sightRadius; //how far this actor can see
    float   _experience;
    AtomicChangeTrackingVariable<float> _pendingExperience;

    float   _stress;
    float   _stressReductionRate;
    STRESS_STATE _currentStressState;
    AtomicChangeTrackingVariable<float> _pendingStress;
    
    bool _movingTowardGoal;
    bool _workingOnGoal;
    bool _shouldLieProne;
    
    Vec2 _unitOffset; //How this actor is positioned relative to the unit
    Vec2  _workPosition;
    Vec2  _workPositionOffset;
    ENTITY_ID _currentWorkEntity;
    ENTITY_ID _takingCoverBehindEnt;
    
    double  _timeSinceLastVisibleAction;
    double   lastUpdateTime;
    bool    _shouldDie;
    
    double _lastLookAngleRadians;

    std::unordered_set<ENTITY_ID> _sectorsVisible;
    std::mutex _sectorsVisibleMutex;

    HumanNetworkUpdate _humanNetworkUpdate;
    std::unordered_set<ENTITY_ID> _enemiesVisible;
    
    Command * _owningCommand;
    
    int _visionBudget;
    
    // more later
CC_CONSTRUCTOR_ACCESS :
    Human();
    ~Human();
    virtual bool init(HumanDefinition * definition, Vec2 position, Command * command);
    
protected:
    
    virtual void updateStopGoal(StopGoal * stop, float deltaTime);
    virtual void updateMoveGoal(MoveGoal * move, float deltaTime);
    virtual void updateLieProneGoal(LieProneGoal * move, float deltaTime);
    virtual void updateWorkGoal(WorkGoal * goal, float deltaTime);
    virtual void updateReloadGoal(ReloadWeaponGoal * goal, float deltaTime) {};
    virtual void updateAttackPositionGoal(AttackPositionGoal * goal, float deltaTime) {};
    virtual void updateAttackTargetGoal(AttackTargetGoal * goal, float deltaTime) {};
    
    bool moveTowardWorkEnt(PhysicalEntity * workEnt);
    void addGoal(GOAL_ORIGIN origin, Goal * goal);
    virtual void updateStress();
    void updateGoals(float deltaTime);
    virtual void handleGoalComplete(Goal * goal);
        
    bool doLineVisibilityTestWithCells(Vec2 start, Vec2 endPosition, float &accumulatedCover, float maximumCover);
public:
    static Human * create(HumanDefinition * definition, Vec2 position, Command * command);
    static Human * create();

    
    virtual void query(float deltaTime);
    virtual void act(float deltaTime);

    LIFE_STATE getActorState() {return _lifeState;}

    virtual Command * getOwningCommand() {return _owningCommand;}

    void addGoalFromOrder(Order * order);
    void addHelpGoal(Goal * goal, bool helpReload);

    bool isPerformingAction();
    virtual bool isPerformingVisibleAction();
    bool canSeeOtherHuman(Human * other, int & maximumHeightToTarget);
    //
    
    float getPercentOfMaximumExperience();
    void addExperience(float experience);
    float getExperience() {return _experience;}
    
    void addStress(float stress);
    float getStress() {return _stress;}
        
    // Looking and Shooting
    float getSightRadius() {return _sightRadius;}
    
    void setUnitOffset(Vec2 offset) {_unitOffset = offset;}
    Vec2 getUnitOffset() {return _unitOffset;}
    void clearUnitGoal();
    
    void getLastVisibleSectors(std::unordered_set<ENTITY_ID> & sectorsVisible);

    virtual Vec2 getUnitPosition() {return _position;}
    virtual int hasUpdateToSendOverNetwork();
    virtual void saveNetworkUpdate(cereal::BinaryOutputArchive & archive);
    virtual void updateFromNetwork(cereal::BinaryInputArchive & archive);
    
    virtual void receiveDamage(int damage);
    virtual void die();
    virtual void removeFromGame();
    
    void updateTestInfo();
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
    virtual void populateDebugPanel(GameMenu * menu, Vec2 location);
};

#endif // __ACTOR_H__
