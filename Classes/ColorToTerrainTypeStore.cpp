//
//  ColorToTerrainTypeStore.cpp
//  TrenchWars
//
//  Created by Paul Reed on 11/7/22.
//

#include "ColorToTerrainTypeStore.h"
#include "StringUtils.h"


ColorToTerrainTypeStore * globalColorToTerrainTypeStore;

ColorToTerrainTypeStore::ColorToTerrainTypeStore()
{
    globalColorToTerrainTypeStore = this;
}

void ColorToTerrainTypeStore::addTerrainType(std::string name, Color4B color, size_t hashedName, bool staticEntity)
{
    _terrainTypes.emplace(hashedName, TerrainType());
    _terrainTypes[hashedName].name = name;
    _terrainTypes[hashedName].color = color;
    _terrainTypes[hashedName].hashedName = hashedName;
    _terrainTypes[hashedName].staticEntity = staticEntity;
    _colorToTerrainIdMap.emplace(color, hashedName);
}

size_t ColorToTerrainTypeStore::colorToTerrainType(unsigned char r, unsigned char g, unsigned char b, unsigned char a)
{
    Color4B color(r,g,b,a);
    auto it = _colorToTerrainIdMap.find(color);
    if(it != _colorToTerrainIdMap.end())
    {
        return it->second;
    }
    return _terrainTypes.begin()->second.hashedName;
}

bool ColorToTerrainTypeStore::isTerrainTypeStaticEnt(size_t terrainType)
{
    return _terrainTypes[terrainType].staticEntity;
}


std::string ColorToTerrainTypeStore::terrainTypeToName(size_t terrainType)
{
    return _terrainTypes[terrainType].name;
}
