//
//  LobbyScene.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/15/23.
//

#include "LobbyScene.h"
#include "LobbyManager.h"
#include "ScenarioFactory.h"
#include "TestingScene.h"
#include "TrenchWarsManager.h"


LobbyScene * LobbyScene::create(LobbyManager * lobbyManager)
{
    LobbyScene * lobby = new LobbyScene();
    lobby->init(lobbyManager);
    return lobby;
}

bool LobbyScene::init(LobbyManager * lobbyManager)
{
    if ( !Scene::init() )
    {
        return false;
    }
    
    _localPlayer = globalTrenchWarsManager->getLocalPlayer();
    
    _lobbyManager = lobbyManager;
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    // Create a colored background (Dark Grey)
    LayerColor * backgroundColor = LayerColor::create(Color4B::GRAY, visibleSize.width, visibleSize.height);
    addChild(backgroundColor);
    
    _selectedScenarioButton = nullptr;
    
    _commandsMenu = AutoSizedListView::create();
    _commandsMenu->setPadding(5, 2, 5, 2);
    _commandsMenu->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    _commandsMenu->setPositionNormalized(Vec2(0.6,0.9));
    _commandsMenu->setLayoutType(Layout::Type::VERTICAL);
    _commandsMenu->setTag(9999);
    addChild(_commandsMenu);
    
    _scenarioMenu = AutoSizedListView::create();
    _scenarioMenu->setPadding(5, 2, 5, 2);
    _scenarioMenu->setPositionNormalized(Vec2(0.2,0.9));
    _scenarioMenu->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    _scenarioMenu->setLayoutType(Layout::Type::VERTICAL);
    _scenarioMenu->setGravity(ListView::Gravity::LEFT);
    
    LinearLayoutParameter * params = LinearLayoutParameter::create();
    params->setMargin(Margin(10, 10, 10, 10));
    params->setGravity(LINEAR_GRAVITY_LEFT);
    _scenarioMenu->setLayoutParameter(params);
    
    addChild(_scenarioMenu);


    auto backButton = Button::create();
    backButton->setTitleText("Back");
    backButton->setTitleFontName("arial.ttf");
    backButton->setTitleFontSize(20);
    backButton->setPositionNormalized(Vec2(.2,.1));
    backButton->addClickEventListener([backButton, this] (Ref * sender) {
        globalTrenchWarsManager->exitToMainMenu();
    });
    addChild(backButton);
    
    _startButton = Button::create();
    _startButton->setTitleText("Start Game");
    _startButton->setTitleFontName("arial.ttf");
    _startButton->setTitleFontSize(25);
    _startButton->setPositionNormalized(Vec2(.8,.1));
    _startButton->addClickEventListener([this] (Ref * sender) {
        _lobbyManager->playerSelectedStartGame(_localPlayer->playerId);
        if(_lobbyManager->isPlayerReady(_localPlayer->playerId))
        {
            _startButton->setColor(Color3B::GREEN);
        }
        else
        {
            _startButton->setColor(Color3B::GRAY);
        }
    });
//    _startButton->setEnabled(false);
    addChild(_startButton);
    
    return true;
}


void LobbyScene::showScenarioMenu()
{
    auto label = Text::create("Scenarios", "arial.ttf", 32);
    _scenarioMenu->addChild(label);
    _scenarioMenu->addSeperater();
    
    auto scenarioList = globalScenarioFactory->getScenarioNames();
    for(auto scenarioName : scenarioList)
    {
        auto scenarioButton = Button::create();
        scenarioButton->setPressedActionEnabled(true);
        
        scenarioButton->setTitleText(scenarioName);
        scenarioButton->setTitleFontName("arial.ttf");
        scenarioButton->setTag(1);
        scenarioButton->setColor(Color3B(20,20,20));
        scenarioButton->setTitleAlignment(TextHAlignment::LEFT);
        scenarioButton->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);

        scenarioButton->setTitleFontSize(20); //FIXME: make adjustable
        scenarioButton->addClickEventListener( [this, scenarioName] (Ref * sender) {
            if(_selectedScenarioButton != nullptr)
            {
                _selectedScenarioButton->getTitleLabel()->disableEffect();
            }
            _lobbyManager->playerSetScenario(scenarioName);
            _selectedScenarioButton = (Button *) sender;
            _selectedScenarioButton->getTitleLabel()->enableBold();
        });
        
        _scenarioMenu->addChild(scenarioButton);
    }
}

void LobbyScene::updateCommandsMenu()
{
    _commandsMenu->removeAllChildren();
    
    auto teamDetailsMap = _lobbyManager->getTeamDetailsMap();
        
    Text * scenario;
    if(!_scenarioName.empty())
    {
        scenario = Text::create(_scenarioName + ":", "arial.ttf", 32);
    }
    else
    {
        scenario = Text::create("No Scenario Selected", "arial.ttf", 32);
    }

    _commandsMenu->addChild(scenario);
    _commandsMenu->addSeperater();

    for(auto teamPair : teamDetailsMap)
    {
        std::ostringstream stream;
        stream << "Team " << teamPair.first << ": ";
        auto teamText = Text::create(stream.str(), "arial.ttf", 26);
        _commandsMenu->addChild(teamText);

        for(auto commandPair : teamPair.second.commandIdToCommandDetailsMap)
        {
            bool isAIPlayer = commandPair.second.playerId == -1;
            stream.str("");
            stream << "Command " << commandPair.first << ": ";
            if(isAIPlayer)
            {
                stream << "AI";
            }
            else
            {
                Player * player = globalTrenchWarsManager->getPlayerForPlayerId(commandPair.second.playerId);
                stream << "Player " << player->playerName;
            }
            
            int commandId = commandPair.first;
            int teamId = teamPair.first;
            std::string name = stream.str();
            auto commandButton = Button::create();
            commandButton->setPressedActionEnabled(true);
            commandButton->setEnabled(isAIPlayer);
            commandButton->setTitleText(name);
            commandButton->setTitleFontName("arial.ttf");
            commandButton->setTitleFontSize(20); //FIXME: make adjustable
            if(!isAIPlayer)
            {
                if(_lobbyManager->isPlayerReady(commandPair.second.playerId))
                {
                    commandButton->setColor(Color3B::GREEN);
                }
                else
                {
                    commandButton->setColor(Color3B::YELLOW);
                }
            }
            else
            {
                commandButton->setColor(Color3B::BLACK);
            }
            commandButton->addClickEventListener( [this, commandId, teamId] (Ref * sender) {
                commandSelected(teamId, commandId);
            });
            _commandsMenu->addChild(commandButton);
        }
    }

    
}

void LobbyScene::scenarioSelected(const std::string & scenarioName)
{
    _scenarioName = scenarioName;
}

void LobbyScene::setScenarioImage(const unsigned char * data, size_t size)
{
    _scenarioImageView = RawDataImageView::createWithData(data, size);
    _scenarioImageView->setPositionNormalized(Vec2(.2,.3));
    _scenarioImageView->setScale(0.3);
    addChild(_scenarioImageView);
}


void LobbyScene::commandSelected(int teamId, int commandId)
{
    if(_localPlayer != nullptr)
    {
        _lobbyManager->playerSelectedCommand(teamId, _localPlayer->playerId, commandId);
    }
}

void LobbyScene::editBoxReturn(EditBox* editBox)
{
//    field->setInsertText(false);
//    field->setCursorEnabled(false);
//    field->setEnabled(false);
    _lobbyManager->joinServer(editBox->getText());

}
