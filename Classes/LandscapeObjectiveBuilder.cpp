//
//  LandscapeObjectiveBuilder.cpp
//  TrenchWars
//
//  Created by Paul Reed on 7/2/21.
//

#include "LandscapeObjectiveBuilder.h"
#include "CollisionCell.h"
#include "EngagementManager.h"
#include "Order.h"
#include "CircularMenu.h"
#include "CollisionGrid.h"
#include "MultithreadedAutoReleasePool.h"
#include "TeamManager.h"

LandscapeObjectiveBuilder * globalLandscapeObjectiveBuilder = nullptr;

bool LandscapeObjectiveBuilder::init()
{
    _cellHighlighter = CellHighlighter::create();
    _cellHighlighter->retain();
    _locked = false;
    globalLandscapeObjectiveBuilder = this;
    return true;
}

LandscapeObjectiveBuilder * LandscapeObjectiveBuilder::create()
{
    LandscapeObjectiveBuilder * tracker = new LandscapeObjectiveBuilder();
    if(tracker->init())
    {
        globalAutoReleasePool->addObject(tracker);
        return tracker;
    }
    
    CC_SAFE_DELETE(tracker);
    return nullptr;
}


void LandscapeObjectiveBuilder::addCellRecursive(CollisionCell * cell)
{
    if(_cellsMap.find(cell->uID()) != _cellsMap.end())
    {
        return;
    }
    
    _cells.push_back(cell);
    _cellsMap.emplace(cell->uID(), cell);
    CollisionCell * down = world->getCollisionGrid()->getCell(cell->getXIndex(), cell->getYIndex() -1);
    if(down != theInvalidCell && down->hasTrench())
    {
        addCellRecursive(down);
    }
    CollisionCell * right = world->getCollisionGrid()->getCell(cell->getXIndex() +1, cell->getYIndex());
    if(right != theInvalidCell && right->hasTrench())
    {
        addCellRecursive(right);
    }
    CollisionCell * up = world->getCollisionGrid()->getCell(cell->getXIndex(), cell->getYIndex() +1);
    if(up != theInvalidCell && up->hasTrench())
    {
        addCellRecursive(up);
    }
    CollisionCell * left = world->getCollisionGrid()->getCell(cell->getXIndex() -1, cell->getYIndex());
    if(left != theInvalidCell && left->hasTrench())
    {
        addCellRecursive(left);
    }
    
}

void LandscapeObjectiveBuilder::update(Vec2 mousePosition)
{
    if(_locked) {
        return;
    }
    _cells.clear();
    _cellsMap.clear();
    
    CollisionCell * cell = world->getCollisionGrid()->getCellForCoordinates(mousePosition);
    if(cell != theInvalidCell && cell->hasTrench())
    {
        addCellRecursive(cell);
    }
    _cellHighlighter->highlightCells(_cells);
}


/**
 *
 */
bool LandscapeObjectiveBuilder::pointInObjective(Vec2 point)
{
    CollisionCell * cell = world->getCollisionGrid()->getCellForCoordinates(point);
    if(_cellsMap.find(cell->uID()) != _cellsMap.end()) {
        return true;
    }
    return false;
}

Objective * LandscapeObjectiveBuilder::buildLandscapeObjective()
{
    std::vector<Vec2> points;
    for(auto cell : _cells)
    {
        points.push_back(cell->getCellPosition());
    }
    return Objective::create(globalTeamManager->getLocalPlayersTeam(), points, false);
}

void LandscapeObjectiveBuilder::clear()
{
    
}
