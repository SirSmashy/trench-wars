#ifndef __UNIT_H__
#define __UNIT_H__
//
//  Unit.h
//  TrenchWars
//
//  Created by Paul Reed on 2/5/12.
//  Copyright (c) 2012. All rights reserved.
//

#include "cocos2d.h"
#include "Order.h"
#include "RedBlackTree.h"
#include "Weapon.h"
#include "Refs.h"
#include "PrimativeDrawer.h"
#include "ChangeTrackingVariable.h"
#include "DoubleBuffer.h"

#define SIGHTCOUNTERUNITMAX 10 // one a secondish
#define UPDATEBOUNDSCOUNTERMAX 10  // once a secondish

class Team;
class Command;
class Infantry;
class UnitDefinition;
class HumanDefinition;
class StaticEntity;
class UnitGroup;
class MapSector;

enum UNIT_TYPE
{
    STANDARD_UNIT,
    WEAPON_UNIT
};

enum UNIT_MEMBER_STATE
{
    MEMBER_HOLDING_POSITION,
    MEMBER_WORKING,
    MEMBER_MOVING
};


// A Unit represents a group of humans. Units are analygous to squads, platoons, or other military formations. Units may be given orders, which are carried out by the humans within the unit.
class Unit : public PlanningObject
{
protected:
    std::vector<Infantry *> _unitMembers;
    CountTo checkSightCounter;
    CountTo updateBoundsCounter;
    
    UnitDefinition * _unitDefinition;
    size_t _hashedEntityName;
    Vector<Human *> _enemies;
    std::unordered_map<Infantry *, bool> _tryRandomEnemiesForInfantry;
    std::mutex _enemiesVisibleMutex;
    DIRECTION _nearestEnemyDirection;
    
    VectorChangeTrackingVariable<Infantry *> _unitMembersToRemove;

    Rect  _sightBounds;
    Rect  _boundingRect;
    Size  _defaultUnitSize; //This represents the size of the unit
    Vec2  _unitPosition; //Position from which the unit's members are offset. This is essentially the unit's center. Differs from unit.position because unit.position is offset behind unitPosition
    Vec2 _averageUnitPosition;
    Vec2 _averageUnitVelocity;
    
    float _averageUnitStress;
    float _averageUnitAmmoPercent;
    
    Command * _owningCommand;
    int _maxStrength;
    int _currentStrength;
    
    BlockingRefChangeTrackingVariable<Order> _pendingOrder;
    Order * _currentOrder;
    int _membersWorkingOnOrder;
    std::mutex _currentOrderMutex;
    
    std::unordered_set<ENTITY_ID> _sectorsVisible;


    virtual void doWorkOrder(WorkOrder * work);

    virtual void doMoveOrder(MoveOrder * move);
    virtual void doOccupyOrder(OccupyOrder * occupy);
    virtual void doAttackPositionOrder(AttackPositionOrder * attack);
    
    void startOrder();
    
    void createActors();
    void updateBounds();
    void updateVision();

    void createWorkOrderForMember(Infantry * member, WorkOrder * workOrder);
    void updateUnitStatusDisplay();

    virtual void clearOrder();
    void digIn();
    
    virtual void handleMemberRemoval(Infantry * member);

CC_CONSTRUCTOR_ACCESS :
    
    Unit();
    virtual bool init(UnitDefinition * definition, Vec2 position, Command * command);
public:
    static Unit * create(UnitDefinition * definition, Vec2 position, Command * command);
    static Unit * create(const std::string & definitionName, Vec2 position, Command * command);
    static Unit * create();
    
    virtual ENTITY_TYPE getEntityType() const {return UNIT;}

    virtual Vec2 getAveragePosition() {return _averageUnitPosition;}
    virtual void addEntity(PhysicalEntity * ent);
    virtual void removeEntity(PhysicalEntity * ent);
    virtual bool containsEntity(PhysicalEntity * ent);
    virtual bool testCollisionWithPoint(Vec2 point);

    virtual std::vector<Infantry *>::iterator begin() { return _unitMembers.begin(); }
    virtual std::vector<Infantry *>::const_iterator begin() const { return _unitMembers.cbegin();  }
    virtual std::vector<Infantry *>::iterator end() { return _unitMembers.end();  }
    virtual std::vector<Infantry *>::const_iterator end() const { return _unitMembers.cend();  }

    size_t size() {return _unitMembers.size();}
    int getMaxStrength() {return _maxStrength;}
    
    //update related functions
    virtual bool shouldQuery() {return true;}
    virtual bool shouldAct() {return true;}
    
    virtual void query(float deltaTime);
    virtual void act(float deltaTime);

    
    virtual void postLoad();
    virtual UNIT_TYPE getUnitType() {return STANDARD_UNIT;}
    virtual Command * getOwningCommand() {return _owningCommand;}
    virtual bool canBombard() {return false;}
    virtual bool positionValidForBombardment(Vec2 position) {return false;}
    virtual int getUnitAmmo(int requestedAmount) {return 0;}
    virtual void crewWeaponNeedsWork(bool helpReloading) {};

    Human * findTargetForActor(Infantry * actor);
    DIRECTION getNearestEnemeyDirection() {return _nearestEnemyDirection;}

    //Unit Status and Info
    Rect getBoundingRect() {return _boundingRect;}
    Size  getDefaultUnitSize() {return _defaultUnitSize;}
    bool isMoving();
    bool hasOrder();
    
    float getAverageUnitStress() {return _averageUnitStress;}
    float getAverageUnitAmmoPercent() {return _averageUnitAmmoPercent;}
        
    //Command -> Unit Interface Functions    
    virtual void setOrder(Order * order);
    virtual void memberCompletedGoal(ENTITY_ID associatedOrderId, bool failed);

    virtual void populateDebugPanel(GameMenu * menu, Vec2 location);
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
    
    virtual void removeFromGame();
};

#endif //__UNIT_H__
