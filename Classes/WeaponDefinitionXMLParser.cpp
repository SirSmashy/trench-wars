//
//  WeaponDefinitionXMLParser.m
//  TrenchWars
//
//  Created by Paul Reed on 10/26/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#include "WeaponDefinitionXMLParser.h"
#include "StringUtils.h"
#include "CollisionGrid.h"
#include <stdexcept>

WeaponDefinitionXMLParser::WeaponDefinitionXMLParser(EntityFactory * factory)
{
    _entityFactory = factory;
    _currentWeaponDefinition = nullptr;
}

void WeaponDefinitionXMLParser::parseFile(std::string const& filename)
{
    std::string fileContents = FileUtils::getInstance()->getStringFromFile(filename);
    if(!fileContents.empty())
    {
        xml_document doc;
        pugi::xml_parse_result result = doc.load(fileContents.c_str());
        if(result.status == xml_parse_status::status_ok)
        {
            bool parsedSuccessfully = doc.traverse(*this);
            if(!parsedSuccessfully)
            {
                CCLOG("parse error!");
            }
        }
        else
        {
            CCLOG("parse error %s ",result.description());
        }
    }
    else
    {
        CCLOG("WeaponDefinitionXMLParser: Could not Open File %s",filename.c_str());
    }
}

bool WeaponDefinitionXMLParser::for_each(xml_node& node)
{
    ci_string name = node.name();
    std::string value = node.first_child().value();
    
    try
    {
        if(name.compare("Weapon") == 0)
        {
            _currentWeaponDefinition = new WeaponDefinition();
            auto attribute = node.attribute("id");
            if(attribute != nullptr)
            {
                _currentWeaponDefinition->_entityName = attribute.value();
                _currentWeaponDefinition->_hashedName = std::hash<std::string>{}(_currentWeaponDefinition->_entityName);

                _entityFactory->addWeaponDefinition(_currentWeaponDefinition);
            }
        }
        else if(name.compare("type") == 0)
        {
            _currentWeaponDefinition->_type = (WEAPON_PROJECTILE_TYPE)std::stoi(value);
        }
        else if(name.compare("rechamberRate") == 0)
        {
            _currentWeaponDefinition->_rechamberRate =std::stof(value);
        }
        else if(name.compare("reloadRate") == 0)
        {
            _currentWeaponDefinition->_reloadRate =std::stof(value);
        }
        else if(name.compare("readyRate") == 0)
        {
            _currentWeaponDefinition->_readyRate =std::stof(value);
        }
        else if(name.compare("magazineSize") == 0)
        {
            _currentWeaponDefinition->_magazineSize =std::stoi(value);
        }
        else if(name.compare("maximumRange") == 0)
        {
            _currentWeaponDefinition->_maximumRange =std::stof(value);
        }
        else if(name.compare("minimumRange") == 0)
        {
            _currentWeaponDefinition->_minimumRange =std::stof(value);
        }
        else if(name.compare("muzzleParticleName") == 0)
        {
            _currentWeaponDefinition->_muzzleParticleName = value;
        }
        else if(name.compare("PreferenceOrder") == 0)
        {
            _currentWeaponDefinition->_preferenceOrder=std::stoi(value);
        }
        else if(name.compare("imprecisionMOA") == 0)
        {
            _currentWeaponDefinition->_imprecisionMOA=std::stoi(value);
        }
        else if(name.compare("meleeDamage") == 0)
        {
            _currentWeaponDefinition->_meleeDamage=std::stoi(value);
        }
        else if(name.compare("projectile") == 0)
        {
            ProjectileDefinition * projectile = (ProjectileDefinition *) _entityFactory->getPhysicalEntityDefinitionWithName(value);
            _currentWeaponDefinition->projectile = projectile;
        }
        else if(name.compare("recoil") == 0)
        {
            _currentWeaponDefinition->_recoilPerShot =std::stof(value);

        }
    }
    catch (const std::invalid_argument& ia)
    {
        CCLOG("Invalid argument: %s for node %s with value %s ", ia.what(), name.c_str(), value.c_str());
    }
    
    return true;
}
