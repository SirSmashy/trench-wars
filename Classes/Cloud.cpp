//
//  Cloud.cpp
//  TrenchWars
//
//  Created by Paul Reed on 7/30/24.
//

#include "Cloud.h"
#include "ParticleManager.h"
#include "MultithreadedAutoReleasePool.h"
#include "CocosObjectManager.h"
#include "EntityFactory.h"
#include "World.h"
#include "CollisionGrid.h"
#include "SectorGrid.h"
#include "Random.h"
#include "World.h"

Cloud::Cloud()
{
}

bool Cloud::init(CloudDefinition * definition, Vec2 position, float initialRadius )
{
    Entity::init();
    _position = position;
    // clearly we need something here to specify the initial size and shape of the cloud
    _particleProxy = ParticleSystemProxy::create(definition->particleName, position);
    _radius = .1;
    _radiusSquared = _radius * _radius;
    _initialBurstTimeLeft = definition->burstTime;
    _burstRadiusChangePerSecond = definition->burstRadiusChangePerSecond;
    _enduringRadiusChangePerSecond = definition->enduringRadiusChangePerSecond;
    _density = definition->initialDensity;
    _densityChangePerSecond = definition->densityChangePerSecond;
    _densityToAlphaMultiple = definition->densityToAlphaMultiple;
    _lifeSpan = definition->lifeSpan;
    
    _velocity = Vec2(globalRandom->randomDouble(-1, 1), globalRandom->randomDouble(-1, 1));

    ParticleCloudProperties cloudProps;
    cloudProps.particleCount = definition->particleCount;
    cloudProps.cloudRadiusToMaxParticleSizePercent = definition->particleSizeToRadiusRatio;
    cloudProps.cloudRadiusToMinParticleSizePercent = .25 ;
    _particleProxy->setupParticles(cloudProps);
    _particleProxy->retain();
    _particleProxy->setPosition(_position);
    _particleProxy->setRadius(_radius);
    _particleProxy->setSize(_radius);
    
    world->addAtmosphericObject(this);
    return true;
}

Cloud * Cloud::create(CloudDefinition * definition, Vec2 position, float initialRadius)
{
    Cloud * newCloud = new Cloud();
    if(newCloud && newCloud->init(definition, position, initialRadius))
    {
        globalAutoReleasePool->addObject(newCloud);
        return newCloud;
    }
    CC_SAFE_DELETE(newCloud);
    return nullptr;
}


Cloud * Cloud::create(const std::string & definitionName, Vec2 position, float initialRadius)
{
    CloudDefinition * definition = globalEntityFactory->getCloudDefinition(definitionName);
    if(definition != nullptr)
    {
        return Cloud::create(definition, position, initialRadius);
    }
    return nullptr;
}

Cloud * Cloud::create()
{
    Cloud * newCloud = new Cloud();
    if(newCloud)
    {
        globalAutoReleasePool->addObject(newCloud);
        return newCloud;
    }
    CC_SAFE_DELETE(newCloud);
    return nullptr;
}

Cloud::~Cloud()
{
    _particleProxy->remove();
    _particleProxy->release();
}

void Cloud::postLoad()
{
    _particleProxy->setRadius(_radius);
}


void Cloud::query(float deltaTime)
{
    // clouds probably don't query. What do they have to think about?
}

void Cloud::act(float deltaTime)
{
    _lifeSpan -= deltaTime;
    if(_lifeSpan <= 0)
    {
        removeFromGame();
        return;
    }
    if(_initialBurstTimeLeft > 0)
    {
        _initialBurstTimeLeft -= deltaTime;
        _radius += _burstRadiusChangePerSecond * deltaTime;
    }
    else
    {
        _radius += _enduringRadiusChangePerSecond * deltaTime;
    }
    _radiusSquared = _radius * _radius;
    _position += _velocity * deltaTime;
    

    _density -= _densityChangePerSecond * deltaTime;
    if(_density <= 0)
    {
        _lifeSpan = 0;
        removeFromGame();
        return;
    }

    std::vector<MapSector *> sectorsFound;
    std::unordered_set<MapSector *> sectorsSet;
    globalSectorGrid->sectorsInCircleWithCenter(_position, _radius, &sectorsFound);
    for(auto sector : sectorsFound)
    {
        sectorsSet.insert(sector);
        if(!_sectorsInCloud.contains(sector))
        {
            sector->addCloudToSector(this);
            _sectorsInCloud.insert(sector);
        }
    }
    std::vector<MapSector *> sectorsToRemove;
    for(auto sector : _sectorsInCloud)
    {
        if(!sectorsSet.contains(sector))
        {
            sector->removeCloudFromSector(this);
            sectorsToRemove.push_back(sector);
        }
    }
    for(auto sector : sectorsToRemove)
    {
        _sectorsInCloud.erase(sector);
    }
    
    _particleProxy->setAlpha((_density/100) * _densityToAlphaMultiple);
    _particleProxy->setPosition(_position);
    _particleProxy->setRadius(_radius);
    _particleProxy->setSize(_radius * 2);

}

void Cloud::removeFromGame()
{
    for(auto sector : _sectorsInCloud)
    {
        sector->removeCloudFromSector(this);
    }
    Entity::removeFromGame();
}

void Cloud::setPosition(Vec2 position)
{
    _position = position;
    _particleProxy->setPosition(position);
}

void Cloud::changeRadius(float amount)
{
    _particleProxy->setRadius(amount);
}

void Cloud::changeAlpha(float amount)
{
    _particleProxy->setAlpha(amount);
}

bool Cloud::testCollisionWithPoint(Vec2 point)
{
    if(_position.distanceSquared(point) < _radiusSquared)
    {
        return true;
    }
    return false;
}

void Cloud::debugClick()
{
}

void Cloud::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Entity::loadFromArchive(archive);
    archive(_position);
    archive(_velocity);
    archive(_initialBurstTimeLeft);
    archive(_burstRadiusChangePerSecond);
    archive(_radius);
    archive(_radiusSquared);
    archive(_density);
    archive(_enduringRadiusChangePerSecond);
    archive(_densityChangePerSecond);
    archive(_densityToAlphaMultiple);
    archive(_lifeSpan);
}

void Cloud::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Entity::saveToArchive(archive);
    archive(_position);
    archive(_velocity);
    archive(_initialBurstTimeLeft);
    archive(_burstRadiusChangePerSecond);
    archive(_radius);
    archive(_radiusSquared);
    archive(_density);
    archive(_enduringRadiusChangePerSecond);
    archive(_densityChangePerSecond);
    archive(_densityToAlphaMultiple);
    archive(_lifeSpan);
}


void Cloud::setDebugMe()
{
}

