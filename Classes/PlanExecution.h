#ifndef __PLAN_EXECUTION_H__
#define __PLAN_EXECUTION_H__
//
//  PlanExecution.h
//  TrenchWars
//
//  Created by Paul Reed on 5/3/15.
//  Copyright (c) 2015 Apportable. All rights reserved.
//
#include <mutex>
#include "cocos2d.h"

class AIStrategy;
class Unit;
class MapSector;
class PhysicalEntity;

USING_NS_CC;

enum PLAN_EXECUTION_STATE
{
    NOT_STARTED,
    EXECUTING,
    ALL_TASKS_ASSIGNED,
    COMPLETED_PLAN,
    FAILED
};


enum EXECUTION_STAGE_TYPE
{
    MOVE_STAGE,
    FORTIFY_STAGE,
    BUILD_COMMAND_STAGE,
    HOLD_STAGE,
    BOMBARDMENT_STAGE,
};

enum TASK_STATE
{
    TASK_NOT_STARTED,
    TASK_IN_PROGRESS,
    TASK_COMPLETE
};

enum TASK_TYPE
{
    ENTITY_TASK,
    MAP_SECTOR_ANALYSIS_TASK,
    COMMAND_POST_TASK
};

class ExecutionTask : public Ref
{
public:
    //FIXME: make these private later
    TASK_STATE taskState;
    TASK_TYPE taskType;
    void * taskObject; //FIXME: TODO figure out what this is for
    Vector<Unit *> unitsWorkingOnTask;
    
public:
    
    ExecutionTask();
    virtual bool init(void * object, TASK_TYPE type);
    static ExecutionTask * create(void * object, TASK_TYPE type);
};


//////////////////////////
class ExecutionStage : public Ref
{
public:
    //FIXME: make these private later
    Vector<ExecutionTask *> tasks;
    int tasksNotAssigned;
    int percentComplete;
    EXECUTION_STAGE_TYPE stageType;
    ExecutionStage * nextStage;
    PLAN_EXECUTION_STATE stageState;
    bool waitForAllUnitsToFinish;
    
public:
    ExecutionStage();
    bool init(EXECUTION_STAGE_TYPE type);
    static ExecutionStage * create(EXECUTION_STAGE_TYPE type);
    
    virtual bool unitFinishedTask(Unit * unit);
    virtual bool tryAddingUnit(Unit * unit);
    virtual void removeUnit(Unit * unit);
    virtual void addtaskWithObject(void * object, TASK_TYPE type);
    
    ExecutionTask * getTaskNearestUnit(Unit * unit, bool onlyUnassignedTasks);

};


class MoveStage : public ExecutionStage
{
public:
    MoveStage();
    bool init(bool waitForAllUnits);
    static MoveStage * create(bool waitForAllUnits);

    virtual bool tryAddingUnit(Unit * unit);
    virtual bool unitFinishedTask(Unit * unit);
};

class FortifyStage : public ExecutionStage
{
public:
    FortifyStage();
    bool init();
    static FortifyStage * create();
    
    virtual bool tryAddingUnit(Unit * unit);
};

class BuildCommandStage : public ExecutionStage
{
public:
    BuildCommandStage();
    bool init();
    static BuildCommandStage * create();
    
    
    virtual bool tryAddingUnit(Unit * unit);
};

class HoldStage : public ExecutionStage
{
public:
    HoldStage();
    bool init();
    static HoldStage * create();
    
    
    virtual bool tryAddingUnit(Unit * unit);
};

class BombardStage : public ExecutionStage
{
public:
    BombardStage();
    bool init();
    static BombardStage * create();
    
    virtual bool tryAddingUnit(Unit * unit);
};


///////////////////////
////////////////////
class PlanExecution : public Ref
{
public:
    //FIXME: make these private later
    AIStrategy * _strategyToExecute;
    Vector<ExecutionStage *> executionStages;
    Map<int, ExecutionStage *> _unitWorkingOnStageDictionary;
    bool _unitsInHoldingStage;
    int numberOfUnitsNeeded;
    Vector<Unit *> participatingUnits;
    PLAN_EXECUTION_STATE executionState;
    std::mutex sectorsInStragegyMutex;

private:
    void createStage(EXECUTION_STAGE_TYPE stageType, bool waitForAllUnits);
    void addUnitToStage(Unit * unit, ExecutionStage * stage);

    void prepareBombardmentStage(ExecutionStage * stage);


public:
    PlanExecution();
    virtual bool init(AIStrategy * plan);
    static PlanExecution * create(AIStrategy * plan);
    
    
    void updateNumberOfUnitsNeeded(int requiredStregthChange);
    void unitFinishedTask(Unit * unit);
    void unitFailedTask(Unit * unit);
    void addUnit(Unit * unit);
    void removeUnit(Unit * unit);
    void stop();
    void deleteExecution();
    std::string stringForExecutionState();
};


#endif //__PLAN_EXECUTION_H__
