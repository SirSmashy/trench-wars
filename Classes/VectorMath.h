#ifndef __VECTOR_MATH_H__
#define __VECTOR_MATH_H__

//
//  VectorMath.h
//  Trench Wars
//
//  Created by Paul Reed on 1/8/14.
//  Copyright (c) 2014 . All rights reserved.
//

#include "Refs.h"
#include "cocos2d.h"
#include "EntityDefinitions.h"
#include <utility>

#define M_PI_8 0.3926990816987241395
#define M_PI_3_8 1.1780972450961724185
#define M_PI_5_8 1.9634954084936206975
#define M_PI_7_8 2.7488935718910689765

#define COS_45 0.707106781186548
#define SIN_45 0.707106781186548
#define D2R 0.0174532925199432954744
#define R2D 57.2957795130823228646

#define VEC_TO_INT(vec) ((vec.y * 30000) + vec.x)
#define ANGLE_FOR_GRID_SIZE 2000


enum DIRECTION
{
    // Single Direction
    RIGHT = 0,
    UP_RIGHT = 1,
    UP = 2,
    UP_LEFT = 3,
    LEFT = 4,
    DOWN_LEFT = 5,
    DOWN = 6,
    DOWN_RIGHT = 7,
    MAX_SINGLE_DIRECTION = 8,
    // Multiple Directions
    RIGHT_LEFT = 8,
    UP_DOWN = 9,
    RIGHT_DOWN_LEFT = 10,
    DOWN_LEFT_UP = 11,
    LEFT_UP_RIGHT = 12,
    UP_RIGHT_DOWN = 13,
    ALL = 14,
    NONE = 15
};

static DIRECTION getOppositeDirection(DIRECTION direction)
{
    if(direction == RIGHT)
    {
        return LEFT;
    }
    else if(direction == LEFT)
    {
        return RIGHT;
    }
    else if(direction == UP)
    {
        return DOWN;
    }
    else if(direction == DOWN)
    {
        return UP;
    }
    return NONE;
}

// stolen directly from BOOST
template <class T>
inline void hash_combine(std::size_t& seed, const T& v)
{
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

struct Vec2FullHash
{
public:
    std::size_t operator() (Vec2 const & vec) const {
        std::hash<float> hashFunc;
        size_t hash = hashFunc(vec.x);
        hash_combine<float>(hash, vec.y);
        return hash;
    }
};

struct Vec2Hash
{
public:
    std::size_t operator() (Vec2 const & vec) const {
        return VEC_TO_INT(vec);
    }
};

struct Vec2Compare
{
public:
    bool operator() (Vec2 const &vecA, Vec2 const &vecB) const {
        return vecA == vecB;
    }
};

class VectorMath
{
public:
    
    static double computeFiringAngleWithVelocity(double velocity, double range ,WEAPON_PROJECTILE_TYPE highArc);
    
    static double computeTimeOfFlightWithVelocity(double velocity, double angle, double heightDifference);
    
    static float distanceFromLineSegment(Vec2 start, Vec2 end, Vec2 point);
    
    static void quadraticSolver(double a, double b, double c, double * root1, double * root2);
    
    static Vec2 computePredictedTargetPosition(double projectileVelocity, Vec2 vectorToTarget, Vec2 targetVelocity, WEAPON_PROJECTILE_TYPE type);
    
    static double computeMaximumWeaponRangeWithSpeed(double speed);
    
    static Vec2 unitVectorFromFacing(int facing);
    static Vec2 vectorFromFacing(int facing);
    static DIRECTION facingFromVector(Vec2 vector);
    static DIRECTION cardinalDireectionFromFacing(int facing);
    static DIRECTION rotateDirectionBySteps(int direction, int steps);
    
    static DIRECTION facingFromRadians(double radians);
    
    static int facingDifference(DIRECTION a, DIRECTION b);
    
    static int degreesFromFacing(DIRECTION facing);
    static double radiansFromFacing(DIRECTION facing);
    
    static double signedAngleBetweenVecs(Vec2 from, Vec2 to);
    static int encodedAngleBetweenVecs(Vec2 from, Vec2 to);
    static Vec2 encodedAngleToVec(int encodedAng);

    static float fastSqrt(float x);
    
    static bool rectOnCircumfrenceWithCenter(Vec2 center, double radius, Rect rect);
    static Vec2 positionOnLineBeyondRect(Vec2 lineStart, Vec2 lineEnd, Rect rect);
    
    static bool lineIntersectsRect(Vec2 lineStart, Vec2 lineEnd, Rect & rect);
    static std::list<Vec2> getLineIntersectionsWithRect(Vec2 lineStart, Vec2 lineEnd, Rect & rect);
    
    
    static Vec2 getIntersectionBetweenLineSegments(const Vec2& A, const Vec2& B, const Vec2& C, const Vec2& D);
    
    static std::pair<bool,bool> getIntersectionBetweenLineSegmentAndCircle(const Vec2& lineStart, const Vec2& lineEnd, const Vec2& center, float radius, Vec2& intersection1, Vec2& intersection2);
    
    static Rect getRectIntersection(Rect & a, Rect & b);
};

#endif // __VECTOR_MATH_H__
