//
//  OrderDrawer.h
//  TrenchWars
//
//  Created by Paul Reed on 4/8/21.
//

#ifndef OrderDrawer_h
#define OrderDrawer_h

#include "Plan.h"
#include "cocos2d.h"
#include "LineDrawer.h"

class GameEventController;

class OrderDrawer : public Ref
{
    bool init();
    Plan * _plan;
    Command * _command;
    Map<ENTITY_ID, LineDrawer *> _unitToLineMap;
    Map<ENTITY_ID, LineDrawer *> _unitGroupToLineMap;

    OrderDrawer();
    void clearLines();
    
    void drawUnitGroupOrders(bool drawConnectionLines);
    
public:
    static OrderDrawer * create();
    virtual void clear();
    virtual void update();
    virtual void drawOrders(const std::unordered_map<ENTITY_ID, ENTITY_ID> & orders, bool drawConnectionLines);
    void remove();
};

#endif /* OrderDrawer_h */
