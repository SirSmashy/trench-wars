//
//  UnitEmblem.cpp
//  TrenchWars
//
//  Created by Paul Reed on 12/8/22.
//

#include "UnitEmblem.h"
#include "MultithreadedAutoReleasePool.h"
#include "EmblemManager.h"
#include "MenuLayer.h"
#include "CircularMenuBuilder.h"
#include "CrewWeaponUnit.h"
#include "EngagementManager.h"
#include "World.h"

#define UNITICONOFFSET -2


UnitEmblem::UnitEmblem() : Emblem(), _circleMenuCountdown(10)
{
    
}

bool UnitEmblem::init(Unit * ent, EMBLEM_TYPE type, Vec2 position)
{
    if(Emblem::init(ent,type, position))
    {
        _unit = ent;
        _showCircleMenuAfterCount = false;
        _objectiveDraggedOver = nullptr;
        globalEngagementManager->requestMainThreadFunction([this,type] () {
            if(type == POSITION_EMBLEM)
            {
                Entity * ent = dynamic_cast<Entity *>(_ent);
                if(_unit->getUnitType() == STANDARD_UNIT)
                {
                    setAnimation(EMBLEM_INFANTRY, false);
                }
                else if(_unit->getUnitType() == WEAPON_UNIT)
                {
                    CrewWeaponUnit * crew = (CrewWeaponUnit *) ent;
                    if(crew->isIndirectFireWeapon())
                    {
                        setAnimation(EMBLEM_ARTY, false);
                    }
                    else
                    {
                        setAnimation(EMBLEM_MG, false);
                    }
                }
            }
        });
        return true;
    }
    return false;
}

UnitEmblem * UnitEmblem::create(Unit * ent, EMBLEM_TYPE type, Vec2 position)
{
    UnitEmblem * newEmblem = new UnitEmblem();
    if(newEmblem->init(ent, type, position)) {
        globalAutoReleasePool->addObject(newEmblem);
        
        return newEmblem;
    }
    CC_SAFE_DELETE(newEmblem);
    return nullptr;
}

void UnitEmblem::update(float deltaTime)
{
    Emblem::update(deltaTime);
    if(!_visible)
    {
        return;
    }
    
    if(_showCircleMenuAfterCount && _circleMenuCountdown.count())
    {
        globalEngagementManager->requestMainThreadFunction([this] () {
            CircularMenu * menu = globalMenuLayer->getPrimaryCircularMenu();
            CircularMenuBuilder::populateForEmblem(menu, this, getPosition());
        });
        _showCircleMenuAfterCount = false;
        _circleMenuCountdown.reset();
    }

    if(_emblemType == POSITION_EMBLEM)
    {
        Vec2 position = world->getAboveGroundPosition(_unit->getAveragePosition());
        position.x += UNITICONOFFSET;
        setPosition(position);
        if(_unit->getAverageUnitStress() > 30)
        {
            addStatusIcon(EMBLEM_STATUS_STRESSED);
        }
        else
        {
            removeStatusIcon(EMBLEM_STATUS_STRESSED);
        }
        
        if(_unit->getAverageUnitAmmoPercent() <= 0.5)
        {
            addStatusIcon(EMBLEM_STATUS_LOW_AMMO);
        }
        else
        {
            removeStatusIcon(EMBLEM_STATUS_LOW_AMMO);
        }
        
        float casualties = _unit->size();
        casualties /= _unit->getMaxStrength();
        if(casualties <= .66) //FIXME: magic number
        {
            addStatusIcon(EMBLEM_STATUS_CASUALTY_25);
        }
        else
        {
            removeStatusIcon(EMBLEM_STATUS_CASUALTY_25);
        }
        
        if(!_unit->getOwningCommand()->isPositionInCommandArea(_unit->getAveragePosition()))
        {
            addStatusIcon(EMBLEM_STATUS_NO_COMMAND);
        }
        else
        {
            removeStatusIcon(EMBLEM_STATUS_NO_COMMAND);
        }
    }
//

}

void UnitEmblem::setEmblemType(EMBLEM_TYPE type)
{
    _emblemType = type;
    if(_emblemType == ORDER_EMBLEM)
    {
        _background->setAnimation(EMBLEM_BACKGROUND_CIRCLE, false);
    }
    else
    {
        _background->setAnimation(DEFAULT, false);
    }
}


void UnitEmblem::emblemDragged(Vec2 position)
{
    if(!_canDrag)
    {
        return;
    }
    Emblem::emblemDragged(position);
    
    setFlashing(false);
    CircularMenu * menu = globalMenuLayer->getPrimaryCircularMenu();
    menu->clear(); //
    
    if(_emblemType == POSITION_EMBLEM)
    {
        Emblem * emb = globalEmblemManager->createEmblemForUnit(_unit, POSITION_EMBLEM);
        setEmblemType(ORDER_EMBLEM);
        removeAllStatusIcons();
        _icon->setAnimation(EMBLEM_ORDER, false);
        std::vector<Emblem *> emblems = globalEmblemManager->getEmblemsForCommandableObject(_ent->uID());
        for(auto emblem : emblems)
        {
            if(emblem != this && (emblem->getEmblemType() == ORDER_EMBLEM))
            {
                globalEmblemManager->removeEmblemForCommandableObject(_ent->uID(), emblem);
            }
        }
    }

    // Highlight objectives and what not
}

void UnitEmblem::emblemDragEnded(Vec2 position)
{
    Emblem::emblemDragEnded(position);

    CircularMenu * menu = globalMenuLayer->getPrimaryCircularMenu();
    CircularMenuBuilder::populateForEmblem(menu, this, position);
    setFlashing(true);
}

void UnitEmblem::emblemHoverEnter(Vec2 position)
{
    if(_isLocalPlayersEmblem)
    {
        _showCircleMenuAfterCount = true;
        _circleMenuCountdown.reset();
    }
}

void UnitEmblem::emblemHoverLeave(Vec2 position)
{
    _circleMenuCountdown.reset();
    _showCircleMenuAfterCount = false;

    CircularMenu * menu = globalMenuLayer->getPrimaryCircularMenu();
    menu->clear(); //
}
