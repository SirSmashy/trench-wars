//
//  EmblemManager.cpp
//  TrenchWars
//
//  Created by Paul Reed on 7/13/21.
//

#include "GameEventController.h"
#include "EmblemManager.h"
#include "EntityFactory.h"
#include "Order.h"
#include "World.h"
#include "CollisionGrid.h"


EmblemManager * globalEmblemManager;

EmblemManager::EmblemManager()
{
    globalEmblemManager = this;
    Director * director = Director::getInstance();
    Scheduler * scheduler = director->getScheduler();
    scheduler->schedule([=] (float dt) { update(dt); }, this, 0.0333333333, false, "emblemManager");
}

void EmblemManager::update(float deltaTime)
{
    // Nothing for now, do a scan line soon
    for(auto emblem : _emblemsMap)
    {
        emblem.second->update(deltaTime);
    }
}

Emblem * EmblemManager::createEmblemForUnitGroup(UnitGroup * group, EMBLEM_TYPE type)
{
    Vec2 position = group->getPosition();
    if(_entityToEmblemsMap.find(group->uID()) == _entityToEmblemsMap.end())
    {
        _entityToEmblemsMap.emplace(group->uID(), std::unordered_set<ENTITY_ID>{});
    }
    
    Emblem * newEmblem = UnitGroupEmblem::create(group,type, position);
    _emblemsMap.insert(newEmblem->uID(), newEmblem);
    _entityToEmblemsMap[group->uID()].insert(newEmblem->uID());
    newEmblem->setScale(1.25);
    return newEmblem;
}

Emblem * EmblemManager::createEmblemForUnit(Unit * unit, EMBLEM_TYPE type)
{
    Vec2 position = unit->getAveragePosition();
    if(_entityToEmblemsMap.find(unit->uID()) == _entityToEmblemsMap.end())
    {
        _entityToEmblemsMap.emplace(unit->uID(), std::unordered_set<ENTITY_ID>{});
    }
    
    Emblem * newEmblem = UnitEmblem::create(unit,type, position);
    _emblemsMap.insert(newEmblem->uID(), newEmblem);
    _entityToEmblemsMap[unit->uID()].insert(newEmblem->uID());
    return newEmblem;
}

TesterEmblem * EmblemManager::createEmblemForTester(Tester * tester, bool updateOnDragStart)
{
    Vec2 position = tester->getPosition();
    
    if(_entityToEmblemsMap.find(tester->uID()) == _entityToEmblemsMap.end())
    {
        _entityToEmblemsMap.emplace(tester->uID(), std::unordered_set<ENTITY_ID>{});
    }
    TesterEmblem * newEmblem = TesterEmblem::create(tester, position, updateOnDragStart);
    _emblemsMap.insert(newEmblem->uID(), newEmblem);
    _entityToEmblemsMap[tester->uID()].insert(newEmblem->uID());

    newEmblem->setAnimation(EMBLEM_DEFAULT, false);
    return newEmblem;
}

void EmblemManager::removeAllEmblemsForCommandableObject(ENTITY_ID objectId)
{
    if(_entityToEmblemsMap.find(objectId) == _entityToEmblemsMap.end())
    {
        return;
    }
    for(auto emblemId : _entityToEmblemsMap.at(objectId))
    {
        _emblemsMap.at(emblemId)->removeFromGame();
        _emblemsMap.erase(emblemId);
    }
    _entityToEmblemsMap.erase(objectId);
}

void EmblemManager::removeEmblemForCommandableObject(ENTITY_ID objectId, Emblem * emblem)
{
    emblem->removeFromGame();
    if(_entityToEmblemsMap.find(objectId) == _entityToEmblemsMap.end())
    {
        LOG_DEBUG_ERROR("Oh dear! This shouldn't happen \n");
        return;
    }
    _entityToEmblemsMap[objectId].erase(emblem->uID());
    _emblemsMap.erase(emblem->uID());
}

void EmblemManager::removeAllEmblemsWithType(EMBLEM_TYPE type)
{
    std::vector<ENTITY_ID> allRemovals;
    for(auto & entEmblems : _entityToEmblemsMap)
    {
        std::vector<ENTITY_ID> entRemove;
        for(auto & emblem : entEmblems.second)
        {
            if(_emblemsMap.at(emblem)->getEmblemType() == type)
            {
                entRemove.push_back(emblem);
                allRemovals.push_back(emblem);
            }
        }
        for(auto emblemId : entRemove)
        {
            entEmblems.second.erase(emblemId);
        }
    }
    for(auto emblemId : allRemovals)
    {
        _emblemsMap.at(emblemId)->removeFromGame();
        _emblemsMap.erase(emblemId);
    }
    
}

void EmblemManager::moveEmblemToOrderPosition(Order * order, Emblem * emblem)
{
    Vec2 position;

    if(order->getOrderType() == MOVE_ORDER )
    {
        MoveOrder * moveOrder = (MoveOrder *) order;
        position = moveOrder->getPosition();
        emblem->setPosition(position);
        emblem->setAnimation(EMBLEM_MOVE, false);
    }
    else if(order->getOrderType() == OCCUPY_ORDER)
    {
        OccupyOrder * occupy = (OccupyOrder *) order;
        position = occupy->getObjective()->getCenterPoint();
        emblem->setPosition(position);
        emblem->setAnimation(EMBLEM_MOVE, false);

    }
    else if(order->getOrderType() == OBJECTIVE_WORK_ORDER || order->getOrderType() == ENTITY_WORK_ORDER)
    {
        WorkOrder * workOrder = (WorkOrder *)order;
        position = workOrder->getAverageWorkPosition();
        emblem->setPosition(position);
        emblem->setAnimation(EMBLEM_DIG, false);
    }
    else if(order->getOrderType() == ATTACK_POSITION_ORDER)
    {
        emblem->setAnimation(EMBLEM_ATTACK, false);
        AttackPositionOrder * attack = (AttackPositionOrder *) order;
        emblem->setPosition(attack->getRandomTargetPoint());
    }
    else if( order->getOrderType() == STOP_ORDER)
    {
        
    }
}

Emblem * EmblemManager::getEmblem(ENTITY_ID emblemId)
{
    return _emblemsMap.at(emblemId);
}

Emblem * EmblemManager::getEmblemForCommandableObjectWithType(ENTITY_ID objectId, EMBLEM_TYPE type)
{
    if(_entityToEmblemsMap.find(objectId) == _entityToEmblemsMap.end())
    {
        return nullptr;
    }
    for(auto emblemId : _entityToEmblemsMap[objectId])
    {
        if(_emblemsMap.at(emblemId)->getEmblemType() == type)
        {
            return _emblemsMap.at(emblemId);
        }
    }
    return nullptr;
}

std::vector<Emblem *> EmblemManager::getEmblemsForCommandableObject(ENTITY_ID objectId)
{
    std::vector<Emblem *> emblems;
    if(_entityToEmblemsMap.find(objectId) == _entityToEmblemsMap.end())
    {
        return emblems;
    }
    for(auto emblemId : _entityToEmblemsMap[objectId])
    {
        emblems.push_back(_emblemsMap.at(emblemId));
    }
    return emblems;
}

Emblem * EmblemManager::getEmblemAtPoint(Vec2 point)
{
    point = world->getAboveGroundPosition(point) * WORLD_TO_GRAPHICS_SIZE;
    for(auto pair : _emblemsMap)
    {
        if(pair.second->testCollisionWithCircle(point, WORLD_TO_GRAPHICS_SIZE))
        {
            return pair.second;
        }
    }
    return nullptr;
}
