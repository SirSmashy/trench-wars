//
//  JoinServerScene.hpp
//  TrenchWars
//
//  Created by Paul Reed on 11/20/23.
//

#ifndef JoinServerScene_h
#define JoinServerScene_h


#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "ScenarioFactory.h"
#include "AutoSizedListView.h"
#include "AutoSizedLayout.h"
#include "Modal.h"

using namespace cocos2d::ui;
USING_NS_CC;

class ClientLobbyManager;
class Player;

class JoinServerScene : public cocos2d::Scene, public cocos2d::ui::EditBoxDelegate
{
    ClientLobbyManager * _lobbyManager;
    
    Text * _statusLabel;
    Text * _addressLabel;
    TextField * _addressField;
    Player * _localPlayer;
    
    //    ScrollView * chatScreen;
    bool init(ClientLobbyManager * lobbyManager);
public:
    
    static JoinServerScene* create(ClientLobbyManager * lobbyManager);
    void update();
    
    void displayError(const std::string & error);
    
    virtual void editBoxReturn(EditBox* editBox);
};


#endif /* JoinServerScene_h */
