//
//  MapLoader.cpp
//  TrenchWars
//
//  Created by Paul Reed on 8/24/22.
//

#include "MapLoader.h"
#include "MultithreadedAutoReleasePool.h"
#include "Timing.h"
#include "BackgroundTaskHandler.h"
#include "EntityFactory.h"
#include "ColorToTerrainTypeStore.h"
#include "World.h"
#include "CollisionGrid.h"


MapArea::MapArea()
{
    
}

MapArea::~MapArea()
{
    
}

void MapArea::addPosition(Vec2 position)
{
    if(position.x < minX)
    {
        minimumCellIndex = position;
        minX = position.x;
    }
    if(position.x > maxX)
    {
        maxX = position.x;
    }
    
    if(position.y < minY)
    {
        minY = position.y ;
    }
    if(position.y  > maxY)
    {
        maxY = position.y ;
    }
    cellIndicesInArea.insert(position);
}

void MapArea::printArea()
{
    LOG_DEBUG("Area: %d C: %7d B: %4d %4d to %4d %4d \n", terrainType, cellIndicesInArea.size(),
           minX, 2055 - maxY,
           maxX, 2055 - minY);
}

MapLoader::MapLoader(Image * map, bool createStaticEntities) :
_pixelToAreaMap(map->getWidth(), std::vector<MapArea*>(map->getHeight()))
{
    _createStaticEntities = createStaticEntities;
    _mapWorldOrigin.setZero();
    _mapVisualPositionOffset.setZero();

    _mapWidth = map->getWidth();
    _mapHeight = map->getHeight();
    
    _mapData.resize(_mapWidth);
    for(int i = 0; i < _mapData.size(); i++)
    {
        _mapData[i].resize(_mapHeight);
    }
    
    std::atomic_int tasksRunning;
    int threads = globalBackgroundTaskHandler->getThreadCount();
    int colsPerThread = ceil( (float) _mapWidth / threads);
    if(colsPerThread == 0)
    {
        threads = 1;
        colsPerThread = _mapWidth;
    }
    tasksRunning.store(threads);
    int bytesPerPixel = map->getBitPerPixel() / 8;

    // There are a LOT of pixels to process, break the map up into seperate sets of rows to multi-thread loading
    // Each thread will process a set of pixel rows
    for(int i = 0; i < threads; i++)
    {
        globalBackgroundTaskHandler->addBackgroundTask([this, i, colsPerThread, &tasksRunning, bytesPerPixel, map] () {
            int start = i * colsPerThread;
            int end = start + colsPerThread;
            if(end >= _mapWidth)
            {
                end = _mapWidth;
            }

            unsigned char r,g,b,a;
            int pixel;
            for(int x = start; x < end; x++)
            {
                for(int y = 0; y < _mapHeight; y++)
                {
                    pixel = (((_mapHeight - 1 - y) * _mapWidth) + x ) * bytesPerPixel;
                    r = *(map->getData() + pixel);
                    g = *(map->getData() + pixel + 1);
                    b = *(map->getData() + pixel + 2);
                    a = *(map->getData() + pixel + 3);
                    _mapData[x][y] = globalColorToTerrainTypeStore->colorToTerrainType(r,g,b,a);
                }
            }
            tasksRunning--;
            return false;
        });
    }
    
    while(globalBackgroundTaskHandler->runNextBackgroundTask()) {}
    while(tasksRunning.load() > 0) {}
}

MapLoader::MapLoader(std::vector<std::vector<size_t>> & mapData, Vec2 mapWorldOrigin, Vec2 mapVisualPositionOffset) :
_pixelToAreaMap(mapData[0].size(), std::vector<MapArea*>(mapData.size()))
{
    _createStaticEntities = false;
    _mapWorldOrigin = mapWorldOrigin;
    _mapVisualPositionOffset = mapVisualPositionOffset;
    _mapData = mapData;
    _mapHeight = mapData.size();
    _mapWidth = mapData[0].size();
}


/**
  * Walk through the map cells and create polygonal areas from cells with the same terrainType
 */
void MapLoader::createMapAreas()
{
    MicroSecondSpeedTimer timer( [=](long long int time) {
        LOG_INFO("Map Parse  : %.2f \n", time / 1000000.0);
    });
    std::atomic_int tasksRunning;
    int threads = globalBackgroundTaskHandler->getThreadCount();
    int rowsPerThread = ceil( (float) _mapHeight / threads);
//    rowsPerThread = 0; //TEST
    if(rowsPerThread == 0)
    {
        threads = 1;
        rowsPerThread = _mapHeight;
    }
    tasksRunning.store(threads);
    
    // There are a LOT of cells to process, break the map up into seperate sets of rows to multi-thread loading
    // Each thread will process a set of pixel rows
    for(int i = 0; i < threads; i++)
    {
        globalBackgroundTaskHandler->addBackgroundTask([this, i, rowsPerThread, &tasksRunning] () {
            int start = i * rowsPerThread;
            int end = start + rowsPerThread;
            if(end > _mapHeight)
            {
                end = _mapHeight;
            }
            createMapAreasForSection(start, end);
            tasksRunning--;
            return false;
        });
    }
    
    while(globalBackgroundTaskHandler->runNextBackgroundTask()) {}
    while(tasksRunning.load() > 0) {}
    
    //initial set of areas created
    // The initial areas now need to be combined along the rows that seperated each thread's set of rows
    Map<ENTITY_ID, CombinationArea *> areasToCombineMap;
    std::unordered_set<CombinationArea *> combinations;
    
    for(int i = 1; i < threads; i++)
    {
        // A seem is where one thread's set of rows end, and another thread's begin
        int ySeamStart = (rowsPerThread * i) - 1;
        int ySeamEnd = ySeamStart+1;
        
        for(int x = 0; x < _mapWidth; x++)
        {
            auto name = globalColorToTerrainTypeStore->terrainTypeToName(_pixelToAreaMap[x][ySeamStart]->terrainType);
            
            // Walk across the width of the seam looking for MapAreas that should be merged
            if(_pixelToAreaMap[x][ySeamStart]->terrainType == _pixelToAreaMap[x][ySeamEnd]->terrainType &&
               _pixelToAreaMap[x][ySeamStart] != _pixelToAreaMap[x][ySeamEnd])
            {
                //These pixels have the same terrain type but different MapAreas, they should be merged
                CombinationArea * combinationStart = areasToCombineMap.at(_pixelToAreaMap[x][ySeamStart]->uID());
                CombinationArea * combinationEnd = areasToCombineMap.at(_pixelToAreaMap[x][ySeamEnd]->uID());
                
                if(combinationStart == nullptr)
                {
                    if(combinationEnd == nullptr)
                    {
                        //No CombinationArea exists for either the top or bottom of the seam, create one
                        combinationStart = new CombinationArea();
                        areasToCombineMap.insert(_pixelToAreaMap[x][ySeamStart]->uID(), combinationStart);
                        combinationStart->release();
                        combinations.insert(combinationStart);
                    }
                    else
                    {
                        // The bottom of the seam is already part of a CombinationArea, add the top of the seam to this combination
                        combinationStart = combinationEnd;
                        areasToCombineMap.insert(_pixelToAreaMap[x][ySeamStart]->uID(), combinationStart);
                    }
                    
                    combinationStart->areasToCombine.push_back(_pixelToAreaMap[x][ySeamStart]);
                }
                if(combinationEnd == nullptr)
                {
                    // The bottom of the seam is not part of a CombinationArea, add it to the CombinationArea for the top of the seam
                    combinationStart->areasToCombine.push_back(_pixelToAreaMap[x][ySeamEnd]);
                    areasToCombineMap.insert(_pixelToAreaMap[x][ySeamEnd]->uID(), combinationStart);
                }
                if(combinationStart != nullptr && combinationEnd != nullptr && combinationStart != combinationEnd) //both are already part of a combined area. Combine the combined areas?
                {
                    for(auto endArea : combinationEnd->areasToCombine)
                    {
                        combinationStart->areasToCombine.push_back(endArea);
                        areasToCombineMap.insert(endArea->uID(), combinationStart);
                    }
                    combinations.erase(combinationEnd);
                }
            }
        }
    }
    
    tasksRunning = combinations.size();
    //Now that we know which MapAreas should be merged, perform the actual merger
    // This could be slow so multithread it
    for(auto combo : combinations)
    {
        globalBackgroundTaskHandler->addBackgroundTask( [this, combo, &tasksRunning] () {
            MapArea * result = combo->areasToCombine[0];
            for(int i = 1; i < combo->areasToCombine.size(); i++)
            {
                result = mergeMapAreas(result, combo->areasToCombine[i], 0, 0, nullptr);
            }
            tasksRunning--;
            return false;
        });
    }
    
    while(globalBackgroundTaskHandler->runNextBackgroundTask()) {}
    while(tasksRunning.load() > 0) {}
    
    LOG_DEBUG("  Total Areas post-merge: %d \n", _mapAreas.size());
}


/**
 * Walk through each cell in the passed in section (vertically split ) of the map and generate or update MapAreas for cells that have the same terrain type
 */
void MapLoader::createMapAreasForSection(int yStart, int yEnd)
{
    std::vector<MapArea *>  previousRow;
    previousRow.assign(_mapWidth, nullptr);
    
    for(int y = yStart; y < yEnd; y++)
    {
        size_t currentAreaType = -1;
        int currentAreaStartIndex = 0;
        MapArea * currentMapArea = nullptr;
        
        for(int x = 0; x < _mapWidth; x++)
        {
            size_t terrain = _mapData[x][y];
            if(currentAreaType == -1)
            {
                currentAreaType = terrain;
            }
            
            // Check to see if this pixel has the same terrain type as the previous pixel(s) in this row
            if(currentAreaType != terrain)
            {
                createOrUpdateMapArea(y, currentAreaStartIndex, x, currentAreaType, currentMapArea, previousRow);
                currentMapArea = nullptr;
                currentAreaStartIndex = x;
                currentAreaType = terrain;
            }
            
            // Check to see if the pixel above this one has the same terrain type
            if(previousRow[x] != nullptr && previousRow[x]->terrainType == terrain)
            {
                //Same type! Now check to see if a MapArea has already been assigned for this area of terrain
                if(currentMapArea == nullptr)
                {
                    //Not assigned, so assign it to the previous row
                    currentMapArea = previousRow.at(x);
                }
                else if(currentMapArea != previousRow.at(x))
                {
                    //A MapArea has already been assigned that is not the same as the MapArea above this pixel, merge them into a single MapArea
                    currentMapArea = mergeMapAreas(currentMapArea, previousRow.at(x), currentAreaStartIndex, x, &previousRow);
                }
            }
        }
        
        createOrUpdateMapArea(y, currentAreaStartIndex, _mapWidth, currentAreaType, currentMapArea, previousRow);
    }
}

/** Combine two adjacent map areas into a single new map area
  * This combination is performed by moving all the cells in the smaller area into the larger area, and then deleting the smaller area
 */
MapArea * MapLoader::mergeMapAreas(MapArea * areaOne, MapArea * areaTwo, int currentAreaStartIndex, int currentX, std::vector<MapArea *> * previousRow)
{
    MapArea * mergeInto;
    MapArea * mergeFrom;
    
    // If the areaOne has more cells than areaTwo, then merge areaTwo into areaOne
    if(areaOne->cellIndicesInArea.size() > areaTwo->cellIndicesInArea.size())
    {
        mergeInto = areaOne;
        mergeFrom = areaTwo;
    }
    else
    {
        mergeInto = areaTwo;
        mergeFrom = areaOne;
    }
    
    for(auto position : mergeFrom->cellIndicesInArea)
    {
        _pixelToAreaMap[position.x][position.y] = mergeInto;
        mergeInto->addPosition(position);
    }
    
    if(previousRow != nullptr)
    {
        //The previous row may have additional references to the areaTwo, switch them out for areaOne
        for(int i = mergeFrom->minX; i <= mergeFrom->maxX; i++)
        {
            if(previousRow->at(i)!= nullptr && previousRow->at(i) == mergeFrom)
            {
                (*previousRow)[i] = mergeInto;
            }
        }
    }
    
    _candidatesMutex.lock();
    _mapAreas.erase(mergeFrom);
    _candidatesMutex.unlock();
    return mergeInto;
}

/** Adds the passed in cell range to a map area
  * If the passed in mapArea is null, then a new mapArea will be created before adding the cell range
 */
void MapLoader::createOrUpdateMapArea(int yIndex, int xStart, int xEnd, size_t terrain, MapArea * currentMapArea, std::vector<MapArea *> & previousRow)
{
    bool created = false;
    if(currentMapArea == nullptr)
    {
        created = true;
        currentMapArea = new MapArea();
        currentMapArea->terrainType = terrain;
        _candidatesMutex.lock();
        _mapAreas.insert(currentMapArea);
        _candidatesMutex.unlock();
        
        globalAutoReleasePool->addObject(currentMapArea);
    }
        
    // Add all the cells in the current area to the map area
    for(int x = xStart; x < xEnd; x++)
    {
        _pixelToAreaMap[x][yIndex] = currentMapArea;
        currentMapArea->addPosition(Vec2(x, yIndex));
        previousRow[x] = currentMapArea;
    }
    if(currentMapArea->cellIndicesInArea.size() == 0)
    {
        LOG_DEBUG_ERROR("ummmm area \n");
    }
}

/*
 *
 */
void MapLoader::createPolygonLandscapeObjects()
{
    LOG_INFO("  Creating landscape objects \n");
    
    int polysCount = 0;
    std::vector<MapArea *> areasList;
    areasList.reserve(_mapAreas.size());
    for(auto area : _mapAreas)
    {
        if(!_createStaticEntities && globalColorToTerrainTypeStore->isTerrainTypeStaticEnt(area->terrainType))
        {
            continue;
        }
        areasList.push_back(area);
        if(!globalColorToTerrainTypeStore->isTerrainTypeStaticEnt(area->terrainType))
        {
            polysCount++;
        }
    }
    
    LOG_INFO("    Creating %d polygon areas \n", polysCount);
    
    int areasPerThread = ceil( (float) areasList.size() / globalBackgroundTaskHandler->getThreadCount());
    int threads = globalBackgroundTaskHandler->getThreadCount();
    if(areasPerThread == 0)
    {
        threads = 1;
        areasPerThread = areasList.size();
    }
    
    std::atomic_int tasksRunning;
    tasksRunning.store(threads);
    
    
    for(int i = 0; i < threads; i++)
    {
        globalBackgroundTaskHandler->addBackgroundTask( [this, i, areasPerThread, &areasList, &tasksRunning] () {
            int start = i * areasPerThread;
            int end = start + areasPerThread;
            if(end > areasList.size())
            {
                end = areasList.size();
            }
            for(int j = start; j < end; j++)
            {
                MapArea * area = areasList[j];
                std::string name = globalColorToTerrainTypeStore->terrainTypeToName(area->terrainType);
                if(globalColorToTerrainTypeStore->isTerrainTypeStaticEnt(area->terrainType))
                {
                    std::unordered_set<Vec2, Vec2Hash, Vec2Compare> objectPositions;
                    for(auto position : area->cellIndicesInArea)
                    {
                        objectPositions.insert(position);
                    }
                    while(objectPositions.size() > 0)
                    {
                        Vec2 position = *objectPositions.begin();
                        objectPositions.erase(position);
                        
                        StaticEntity * ent = StaticEntity::create(name, (position) + _mapWorldOrigin);
                        Rect collisionBounds = ent->getCollisionBounds();
                        
                        for(int x = 0; x < collisionBounds.size.width; x++)
                        {
                            for(int y = 0; y < collisionBounds.size.height; y++)
                            {
                                objectPositions.erase(collisionBounds.origin + Vec2(x,y));
                            }
                        }
                    }
                }
                else
                {
                    Rect cellIndexBounds(area->minX, area->minY, area->maxX - area->minX, area->maxY - area->minY );
                    PolygonLandscapeObject::create(name, _mapWorldOrigin, _mapVisualPositionOffset, area->minimumCellIndex, cellIndexBounds, area->cellIndicesInArea);

                }
            }
            tasksRunning--;
            return false;
        });
    }
    
    while(globalBackgroundTaskHandler->runNextBackgroundTask()) {}
    while(tasksRunning.load() > 0) {}
}

/**
 *
 */
bool MapLoader::patchValid(int minX, int maxX, int minY, int maxY)
{
    float x = maxX - minX + 1;
    float y = maxY - minY + 1;
    float size = x * y;
    float aspect = x / y;
    return size <= 100000 && aspect < 4 && aspect > 0.35; // 3 to one
}

/**
  *
 */
void MapLoader::buildNavigationPatch(Vec2 & start, MapArea * area, TerrainCharacteristics & terrain, bool print)
{
    int minX = start.x;
    int maxX = minX;
    int minY = start.y;
    int maxY = minY;
    
    bool upValid = true;
    bool downValid = true;
    bool rightValid = true;
    bool leftValid = true;
    DIRECTION currentDirection = UP;
    
    // Iterively build a navigation patch (a RECTANGLE of cell indices)
    // With each pass, attempt to expand the rectangle in one of 4 directions by doing the following:
    //      See if each cell that is one past the current patch's edge (in the current direction) is in the area
    //      e.g., if the current directio is UP then check if each cell along the top border (+1) is in the area
    // If each cell is in the area, then expand the patch in that direction, and then change direction
    // If one or more cells is NOT in the area, then don't expand the patch, and mark that direction as no longer "valid"
    // When there are no more valid directions to try expanding, quit
    while(patchValid(minX,maxX,minY,maxY) && (upValid || downValid || rightValid || leftValid))
    {
        if(currentDirection ==  UP)
        {
            if(upValid)
            {
                bool canIncrease = true;
                for(int x = minX; x <= maxX; x++)
                {
                    if(!area->cellIndicesInArea.contains( Vec2(x, maxY + 1)))
                    {
                        canIncrease = false;
                        break;
                    }
                }
                if(canIncrease)
                {
                    for(int x = minX; x <= maxX; x++)
                    {
                        area->cellIndicesInArea.erase(Vec2(x, maxY + 1));
                    }
                    maxY++;
                    if(maxY == _mapHeight -1)
                    {
                        upValid = false;
                        continue;
                    }
                }
                else
                {
                    upValid = false;
                }
            }
            currentDirection = RIGHT;
        }
        if(currentDirection ==  RIGHT)
        {
            if(rightValid)
            {
                bool canIncrease = true;
                for(int y = minY; y <= maxY; y++)
                {
                    if(!area->cellIndicesInArea.contains( Vec2(maxX + 1, y)))
                    {
                        canIncrease = false;
                        break;
                    }
                }
                if(canIncrease)
                {
                    for(int y = minY; y <= maxY; y++)
                    {
                        area->cellIndicesInArea.erase( Vec2(maxX + 1, y));
                    }
                    maxX++;
                    if(maxX == _mapWidth -1)
                    {
                        rightValid = false;
                        continue;
                    }
                }
                else
                {
                    rightValid = false;
                }
            }
            
            currentDirection = DOWN;
        }
        if(currentDirection ==  DOWN)
        {
            if(downValid)
            {
                bool canIncrease = true;
                for(int x = minX; x <= maxX; x++)
                {
                    if(!area->cellIndicesInArea.contains( Vec2(x, minY - 1)))
                    {
                        canIncrease = false;
                        break;
                    }
                }
                if(canIncrease)
                {
                    for(int x = minX; x <= maxX; x++)
                    {
                        area->cellIndicesInArea.erase(Vec2(x, minY - 1));
                    }
                    minY--;
                    if(minY == 0)
                    {
                        downValid = false;
                    }
                }
                else
                {
                    downValid = false;
                }
            }
            
            currentDirection = LEFT;
        }
        if(currentDirection ==  LEFT)
        {
            if(leftValid)
            {
                bool canIncrease = true;
                for(int y = minY; y <= maxY; y++)
                {
                    if(!area->cellIndicesInArea.contains( Vec2(minX - 1, y)))
                    {
                        canIncrease = false;
                        break;
                    }
                }
                if(canIncrease)
                {
                    for(int y = minY; y <= maxY; y++)
                    {
                        area->cellIndicesInArea.erase( Vec2(minX - 1, y));
                    }
                    minX--;
                    if(minX == 0)
                    {
                        leftValid = false;
                        continue;
                    }
                }
                else
                {
                    leftValid = false;
                }
            }
            currentDirection = UP;
        }
    }

    _navPatchesMutex.lock();
    _navigationRects.push_back(NavigationPatchRect(minX, maxX, minY, maxY, terrain));
    _navPatchesMutex.unlock();
}

/**
 *
 */
void MapLoader::createNavigationPatches()
{
    LOG_INFO("  Creating Navigation Patches \n");
    std::atomic_int tasksRunning;
    tasksRunning.store(_mapAreas.size());
    
    for(auto area : _mapAreas)
    {
        globalBackgroundTaskHandler->addBackgroundTask( [this, &tasksRunning, area] () {
            int patchesCreated = 0;
            // Setup the terrain type for this navigation patch
            std::string name = globalColorToTerrainTypeStore->terrainTypeToName(area->terrainType);
            TerrainCharacteristics  terrain;
            if(globalColorToTerrainTypeStore->isTerrainTypeStaticEnt(area->terrainType))
            {
                StaticEntityDefinition * definition = (StaticEntityDefinition *) globalEntityFactory->getPhysicalEntityDefinitionWithName(name);
                terrain.set(definition->_height, definition->_cover,definition->_hardness, definition->_impassable);
            }
            else
            {
                PolygonLandscapeObjectDefiniton * definition = globalEntityFactory->getPolygonLandscapeObjectDefinitionWithName(name);
                terrain.set(definition->_height, definition->_cover, definition->_hardness, definition->_impassable);
            }
            
            // Step one, create a map for each cell in the area:
            //      Key: Cell index
            //      value: The number of neighboring cells (cardinal direction only) that are also in this area
            // Also create a vector that has the same contents as the cellIndicesInArea set
            std::map<int,std::list<Vec2>> cellAdjacencyCountMaps;
            for(int i = 8; i >= 0; i--)
            {
                cellAdjacencyCountMaps.try_emplace(i);
            }
            for(auto vec : area->cellIndicesInArea)
            {
                int count = 0;
                if(area->cellIndicesInArea.contains( Vec2(vec.x, vec.y +1))) {count++;} //UP
                if(area->cellIndicesInArea.contains( Vec2(vec.x + 1, vec.y +1))) {count++;} //UP-RIGHT
                if(area->cellIndicesInArea.contains( Vec2(vec.x + 1, vec.y))) {count++;} //RIGHT
                if(area->cellIndicesInArea.contains( Vec2(vec.x +1, vec.y - 1))) {count++;} //RIGHT-DOWN
                if(area->cellIndicesInArea.contains( Vec2(vec.x, vec.y - 1))) {count++;} //DOWN
                if(area->cellIndicesInArea.contains( Vec2(vec.x - 1, vec.y-1))) {count++;} //DOWN-LEFT
                if(area->cellIndicesInArea.contains( Vec2(vec.x - 1, vec.y))) {count++;} //LEFT
                if(area->cellIndicesInArea.contains( Vec2(vec.x - 1, vec.y +1))) {count++;} //LEFT-UP
                
                cellAdjacencyCountMaps[8 - count].push_back(vec);
            }
            
            bool print = false;
//            if(area->maxX - area->minX > 0 && area->maxY - area->minY > 0)
//            {
//                print = true;
//                
//                LOG_DEBUG("Area %d \n", area->cellIndicesInArea.size());
//                for(int i = 0; i <= 4; i++)
//                {
//                    LOG_DEBUG("  %d: %d \n",i, cellAdjacencyCountMaps[i].size());
//                }
//                LOG_DEBUG("done \n");
//
//            }
            
            for(auto & adjacencyList : cellAdjacencyCountMaps)
            {
                for(auto & position : adjacencyList.second)
                {
                    if(area->cellIndicesInArea.contains(position))
                    {
                        area->cellIndicesInArea.erase(position);
                        buildNavigationPatch(position, area, terrain, print);
                        patchesCreated++;
                    }
                }
            }
            tasksRunning--;
            return false;
        });
    }
    
    while(globalBackgroundTaskHandler->runNextBackgroundTask()) {}
    while(tasksRunning.load() > 0) {}
    LOG_INFO("  Total patches is %d \n", _navigationRects.size());
}
