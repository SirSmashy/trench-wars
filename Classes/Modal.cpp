//
//  Modal.cpp
//  TrenchWars
//
//  Created by Paul Reed on 11/10/23.
//

#include "Modal.h"


Modal::Modal() : Layout()
{
    
}

bool Modal::init(Layout::Type layoutDirection)
{
    Layout::init();

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    

    
    // Create a colored background (Dark Grey)
    setBackGroundColor(Color3B(0,0,0));
    setBackGroundColorOpacity(125);
    setBackGroundColorType(LayoutBackGroundColorType::SOLID);
    setGlobalZOrder(1000);
    setContentSize(visibleSize);
    setSize(visibleSize);
    setTouchEnabled(true);
    setSwallowTouches(true);


    _contentWidget = ListView::create();
    _contentWidget->setPositionNormalized(Vec2(.5,.5));
    _contentWidget->setBackGroundColor(Color3B(75,75,75));
    _contentWidget->setBackGroundColorType(LayoutBackGroundColorType::SOLID);
    _contentWidget->setContentSize(visibleSize/3);
    _contentWidget->setSize(visibleSize/3);
    _contentWidget->setAnchorPoint(Vec2::ANCHOR_MIDDLE);

    _contentWidget->setLayoutType(layoutDirection);
    _contentWidget->setLayoutComponentEnabled(true);
    if(layoutDirection == Layout::Type::HORIZONTAL)
    {
        _contentWidget->setDirection(ScrollView::Direction::HORIZONTAL);
        _contentWidget->setGravity(ListView::Gravity::CENTER_VERTICAL);
    }
    else
    {
        _contentWidget->setGravity(ListView::Gravity::CENTER_HORIZONTAL);
    }
    
    LinearLayoutParameter * params = LinearLayoutParameter::create();
    params->setMargin(Margin(10, 10, 10, 10));
//    params->setGravity(LinearGravity::CENTER_VERTICAL);
    _contentWidget->setLayoutParameter(params);

    addChild(_contentWidget);
    return true;
}


Modal * Modal::create(Layout::Type layoutDirection)
{
    Modal * newMenu = new Modal();
    if(newMenu->init(layoutDirection))
    {
        newMenu->autorelease();
        return newMenu;
    }
    
    CC_SAFE_DELETE(newMenu);
    return nullptr;
}


void Modal::addContent(Widget * content)
{
    _contentWidget->addChild(content);
}

void Modal::clearContent()
{
    _contentWidget->removeAllChildren();
}

