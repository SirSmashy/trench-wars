//
//  Crater.hpp
//  TrenchWars
//
//  Created by Paul Reed on 2/3/25.
//

#ifndef Crater_h
#define Crater_h

#include "StaticEntity.h"

class Crater : public StaticEntity
{
    Crater();
    virtual bool init(StaticEntityDefinition * definition, Vec2 position, float size);
    virtual bool init(const std::string & definitionName, Vec2 position, float size);

    
public:
    static Crater * create(StaticEntityDefinition * definition, Vec2 position, float size = -1);
    static Crater * create(const std::string & definitionName, Vec2 position, float size = -1);
    static Crater * create();

};

#endif /* Crater_h */
