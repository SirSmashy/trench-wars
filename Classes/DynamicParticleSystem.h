//
//  DynamicParticleSystem.hpp
//  TrenchWars
//
//  Created by Paul Reed on 7/27/24.
//

#ifndef DynamicParticleSystem_h
#define DynamicParticleSystem_h

#include "cocos2d.h"
#include "ChangeTrackingVariable.h"

USING_NS_CC;

struct ParticleCloudProperties
{
    int particleCount;
    float cloudRadiusToMinParticleSizePercent;
    float cloudRadiusToMaxParticleSizePercent;
};

class DynamicParticleSystem : public ParticleSystemQuad
{
    bool _created;
    
    SimpleChangeTrackingVariable<float> _pendingAlpha;
    SimpleChangeTrackingVariable<float> _pendingRadius;
    SimpleChangeTrackingVariable<float> _pendingSize;

    DynamicParticleSystem();
    ~DynamicParticleSystem();

public:
    
    static DynamicParticleSystem * createWithTotalParticles(int numberOfParticles);
    static DynamicParticleSystem * create(const std::string& filename);
    
    void setupParticles(ParticleCloudProperties & cloudProperties);
    void setAlpha(float alpha);
    void setRadius(float radius);
    void setSize(float size);
    virtual void stop();
    virtual void update(float deltaTime);
};

#endif /* DynamicParticleSystem_h */
