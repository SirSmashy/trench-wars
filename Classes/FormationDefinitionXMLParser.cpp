//
//  FormationDefinitionXMLParser.cpp
//  TrenchWars
//
//  Created by Paul Reed on 11/20/22.
//

#include "FormationDefinitionXMLParser.h"
#include "CollisionGrid.h"
#include "StringUtils.h"
#include <stdexcept>


FormationDefinitionXMLParser::FormationDefinitionXMLParser(EntityFactory * factory)
{
    _currentFormationDefinition = nullptr;
    currentUnitSpawn = nullptr;
    _entityFactory = factory;
    
}

void FormationDefinitionXMLParser::parseFile(std::string const & filename)
{
    std::string fileContents = FileUtils::getInstance()->getStringFromFile(filename);
    if(!fileContents.empty())
    {
        xml_document doc;
        pugi::xml_parse_result result = doc.load(fileContents.c_str());
        if(result.status == xml_parse_status::status_ok)
        {
            bool parsedSuccessfully = doc.traverse(*this);
            if(!parsedSuccessfully)
            {
                CCLOG("parse error!");
            }
        }
        else
        {
            CCLOG("parse error %s ",result.description());
        }
    }
    else
    {
        CCLOG("FormationDefinitionXMLParser: Could not Open File %s",filename.c_str());
    }
}

bool FormationDefinitionXMLParser::for_each(xml_node& node)
{
    ci_string name = node.name();
    std::string value = node.first_child().value();
    
    try
    {
        if(name.compare("Formation") == 0)
        {
            _currentFormationDefinition = new FormationDefinition();
            auto attribute = node.attribute("name");
            if(attribute != nullptr)
            {
                _currentFormationDefinition->_entityName = attribute.value();
                _currentFormationDefinition->_hashedName = std::hash<std::string>{}(_currentFormationDefinition->_entityName);

                _entityFactory->addFormationDefinition(_currentFormationDefinition);
            }
        }
        
        else if(name.compare("Unit") == 0)
        {
            currentUnitSpawn = new UnitSpawn();
            auto attribute = node.attribute("definitionName");
            if(attribute != nullptr)
            {
                currentUnitSpawn->unitDefinitionName = attribute.value();
            }
            else
            {
                CCLOG("Failure to Parse Unit Spawn. Element is missing definitionName attribute");
            }
            attribute = node.attribute("offset");
            if(attribute != nullptr)
            {
                currentUnitSpawn->location = parseVector(attribute.value());
            }
            else
            {
                CCLOG("Failure to Parse Unit Spawn. Element is missing offset attribute");
            }
            _currentFormationDefinition->units.pushBack(currentUnitSpawn);
        }
    }
    catch (const std::invalid_argument& ia)
    {
        CCLOG("Invalid argument: %s for node %s with value %s ", ia.what(), name.c_str(), value.c_str());
        return false;
    }
    
    return true;
}
