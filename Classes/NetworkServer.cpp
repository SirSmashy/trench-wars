//
//  NetworkServer.cpp
//  TrenchWars
//
//  Created by Paul Reed on 3/11/24.
//

#include "NetworkServer.h"
#include "SerializationHelpers.h"
#include "TrenchWarsManager.h"

const uint16 DEFAULT_SERVER_PORT = 27020;


// ////////////////

NetworkServer::NetworkServer()
{
}

void NetworkServer::startListening()
{
    // Disable authentication when running with Steam, for this
    // example, since we're not a real app.
    //
    // Authentication is disabled automatically in the open-source
    // version since we don't have a trusted third party to issue
    // certs.
    SteamNetworkingMicroseconds g_logTimeZero;
    
    g_logTimeZero = SteamNetworkingUtils()->GetLocalTimestamp();
    
    SteamNetworkingUtils()->SetDebugOutputFunction( k_ESteamNetworkingSocketsDebugOutputType_Msg, DebugOutput );
    
    SteamNetworkingIPAddr serverLocalAddr;
    serverLocalAddr.Clear(); //Listen to every incoming IP
    
    serverLocalAddr.m_port = DEFAULT_SERVER_PORT;
    SteamNetworkingConfigValue_t opt;
    
    //This callback handles clients joining or leaving
    opt.SetPtr( k_ESteamNetworkingConfig_Callback_ConnectionStatusChanged, (void*)NetworkInterface::SteamNetConnectionStatusChangedCallback );
    
    //Starting listening
    _listenSocket = _networkInterface->CreateListenSocketIP( serverLocalAddr, 1, &opt );
    
    if ( _listenSocket == k_HSteamListenSocket_Invalid )
        LOG_ERROR( "Failed to listen on port %d", DEFAULT_SERVER_PORT );
    _pollGroup = _networkInterface->CreatePollGroup();
    if ( _pollGroup == k_HSteamNetPollGroup_Invalid )
        LOG_ERROR( "Failed to listen on port %d", DEFAULT_SERVER_PORT );
    LOG_ERROR( "Server listening on port %d\n", DEFAULT_SERVER_PORT );
    
    _networkStatus = NETWORK_STATUS_SERVER_LISTENING;
    
}

void NetworkServer::OnSteamNetConnectionStatusChanged( SteamNetConnectionStatusChangedCallback_t  * connectionInfo )
{
    // What's the state of the connection?
    switch ( connectionInfo->m_info.m_eState )
    {
        case k_ESteamNetworkingConnectionState_None:
            // NOTE: We will get callbacks here when we destroy connections.  You can ignore these.
            break;
            
        case k_ESteamNetworkingConnectionState_ClosedByPeer:
        case k_ESteamNetworkingConnectionState_ProblemDetectedLocally:
        {
            // Ignore if they were not previously connected.  (If they disconnected
            // before we accepted the connection.)
            if ( connectionInfo->m_eOldState == k_ESteamNetworkingConnectionState_Connected )
            {
                
                // Locate the client.  Note that it should have been found, because this
                // is the only codepath where we remove clients (except on shutdown),
                // and connection change callbacks are dispatched in queue order.
                auto itClient = _connectionToClientMap.find( connectionInfo->m_hConn );
                assert( itClient != _connectionToClientMap.end() );
                
                // Select appropriate log messages
                const char *pszDebugLogAction;
                if ( connectionInfo->m_info.m_eState == k_ESteamNetworkingConnectionState_ProblemDetectedLocally )
                {
                    LOG_INFO("Connection to client detected locally \n");
                }
                else
                {
                    // Note that here we could check the reason code to see if
                    // it was a "usual" connection or an "unusual" one.
                    LOG_INFO("Connection closed by client \n");
                }
                
                // Spew something to our own log.  Note that because we put their nick
                // as the connection description, it will show up, along with their
                // transport-specific data (e.g. their IP address)
                LOG_INFO( "Connection %s %s, reason %d: %s\n",
                       connectionInfo->m_info.m_szConnectionDescription,
                       pszDebugLogAction,
                       connectionInfo->m_info.m_eEndReason,
                       connectionInfo->m_info.m_szEndDebug
                       );
                
                _networkFailures.push_back(NetworkFailure(NETWORK_FAILURE_CLIENT_DROPPED, connectionInfo->m_hConn, itClient->second->_playerId));
                _playerToConnectionMap.erase(itClient->second->_playerId);
                globalTrenchWarsManager->playerLeftGame(itClient->second->_playerId);
                _connectionToClientMap.erase( connectionInfo->m_hConn );
            }
            else
            {
                assert( connectionInfo->m_eOldState == k_ESteamNetworkingConnectionState_Connecting );
            }
            
            // Clean up the connection.  This is important!
            // The connection is "closed" in the network sense, but
            // it has not been destroyed.  We must close it on our end, too
            // to finish up.  The reason information do not matter in this case,
            // and we cannot linger because it's already closed on the other end,
            // so we just pass 0's.
            _networkInterface->CloseConnection( connectionInfo->m_hConn, 0, nullptr, false );
            break;
        }
            
        case k_ESteamNetworkingConnectionState_Connecting:
        {
            // This must be a new connection
            LOG_INFO( "Connection request from %s \n", connectionInfo->m_info.m_szConnectionDescription );
            
            // A client is attempting to connect
            // Try to accept the connection.
            if ( _networkInterface->AcceptConnection( connectionInfo->m_hConn ) != k_EResultOK )
            {
                // This could fail.  If the remote host tried to connect, but then
                // disconnected, the connection may already be half closed.  Just
                // destroy whatever we have on our side.
                _networkInterface->CloseConnection( connectionInfo->m_hConn, 0, nullptr, false );
                LOG_ERROR( "Can't accept connection.  (It was already closed?) \n" );
                break;
            }
            
            // Assign the poll group
            if ( !_networkInterface->SetConnectionPollGroup( connectionInfo->m_hConn, _pollGroup ) )
            {
                _networkInterface->CloseConnection( connectionInfo->m_hConn, 0, nullptr, false );
                LOG_ERROR( "Failed to set poll group! \n" );
                break;
            }
            
            if(_connectionToClientMap.find( connectionInfo->m_hConn ) == _connectionToClientMap.end())
            {
                NetworkClient_t * client = new NetworkClient_t();
                client->_clientState = CLIENT_IN_LOBBY; //bleh, this depends upon game state
                client->_networkIdentity = connectionInfo->m_info.m_identityRemote; //Might not actually need this
                client->_connectionId = connectionInfo->m_hConn;
                _connectionToClientMap[ connectionInfo->m_hConn ] = client;
                
                int playerId = globalTrenchWarsManager->remotePlayerJoinedGame();
                client->_playerId = playerId;
                _playerToConnectionMap.emplace(playerId, client->_connectionId);
                
                LOG_INFO("Hello new player!!!! %d \n", playerId);
                // Immediatly send this player their server-based playerId
                MessageBuffer * buffer = prepareNetworkPackage(MESSAGE_REMOTE_PLAYER_ID);
                auto & archive = buffer->getArchive();
                archive(playerId);
                sendPreparedDataToPlayer(buffer, playerId);
            }
            else
            {
                //Not sure if this needs to be handled or if steamnetworkingsockets automatically handles repeat connections
            }
            
            break;
        }
            
        case k_ESteamNetworkingConnectionState_Connected:
            // We will get a callback immediately after accepting the connection.
            // Since we are the server, we can ignore this, it's not news to us.
            break;
        default:
            // Silences -Wswitch
            break;
    }
}

void NetworkServer::pollIncomingMessages()
{
    ISteamNetworkingMessage ** messages = new ISteamNetworkingMessage*[16];
    int numMsgs = _networkInterface->ReceiveMessagesOnPollGroup( _pollGroup, messages, 16 );
    if ( numMsgs == 0 )
        return;
    if ( numMsgs < 0 )
        LOG_ERROR("Error checking messages! \n");
    
    for(int i = 0; i < numMsgs; i++)
    {
        auto itClient = _connectionToClientMap.find( messages[i]->m_conn );
        assert( itClient != _connectionToClientMap.end() );
        // //////
        //
        
        char * data = (char *) messages[i]->m_pData;
        MessageHeaderUnion header;
        header.byteValue = data;
        if(header.header->messageCount > 1)
        {
            parseChunkedMessage(data, messages[i]->m_cbSize, itClient->second->_playerId);
        }
        else
        {
            parseStandardMessage(data, messages[i]->m_cbSize, itClient->second->_playerId);
        }
        messages[i]->Release();
    }
}

void NetworkServer::sendPreparedDataToPlayer(MessageBuffer * buffer, int playerId)
{
    std::list players = {playerId};
    sendPreparedDataToPlayers(buffer, players);
}

void NetworkServer::sendPreparedDataToPlayers(MessageBuffer * buffer, std::list<int> & players)
{
    auto it = _inUseBuffers.find(buffer->_bufferId);
    if(it == _inUseBuffers.end())
    {
        return;
    }
    
    buffer->connectionsToSendTo.clear();
    for(auto player : players)
    {
        if(_playerToConnectionMap.find(player) == _playerToConnectionMap.end())
        {
            LOG_INFO("Cannot send data to command %d: Player not registered \n", player);
        }
        else
        {
            buffer->connectionsToSendTo.push_back(_playerToConnectionMap.at(player));
        }
    }
    _bufferMutex.lock();
    _buffersWaitingToBeSent.push_back(buffer);
    _inUseBuffers.erase(buffer->_bufferId);
    _bufferMutex.unlock();
}
