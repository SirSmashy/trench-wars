//
//  SectorGrid.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/11/12.
//  Copyright (c) 2012 . All rights reserved.
//

#include "SectorGrid.h"
#include "Command.h"
#include "World.h"
#include "BackgroundTaskHandler.h"

SectorGrid * globalSectorGrid;

SectorGrid::SectorGrid(Size cellGridSize, int cellsInSector)
{
    _sectorWidthInCells = cellsInSector;
    _sectorGridSize = cellGridSize;
    _sectorGridSize.width /= _sectorWidthInCells;
    _sectorGridSize.height /= _sectorWidthInCells;
    
    _sectorGrid = new MapSector **[_sectorGridSize.width];
    _drawingSectors = false;
    _redrawSectors = false;
    int xCellCount = _sectorWidthInCells;
    int yCellCount = _sectorWidthInCells;
    for(int x = 0; x < _sectorGridSize.width; x++)
    {
        if( ( (x+1) * _sectorWidthInCells) > cellGridSize.width)
        {
            xCellCount = cellGridSize.width - (x * _sectorWidthInCells);
        }
        else
        {
            xCellCount = _sectorWidthInCells;
        }
        _sectorGrid[x] = new MapSector *[_sectorGridSize.height];
        for(int y = 0; y < _sectorGridSize.height; y++)
        {
            if( ((y +1) * _sectorWidthInCells) > cellGridSize.height)
            {
                yCellCount = cellGridSize.height - (y * _sectorWidthInCells);
            }
            else
            {
                yCellCount = _sectorWidthInCells;
            }
            _sectorGrid[x][y] = MapSector::create(x,y, xCellCount, yCellCount, _sectorWidthInCells);
            _sectorIdToSectorMap.insert(_sectorGrid[x][y]->uID(), _sectorGrid[x][y]);
        }
    }
    globalSectorGrid = this;
}

void SectorGrid::addTeam()
{
    for(int x = 0; x < _sectorGridSize.width; x++)
    {
        for(int y = 0; y < _sectorGridSize.height; y++)
        {
            _sectorGrid[x][y]->addTeam();
        }
    }
}


MapSector * SectorGrid::sectorForCell(CollisionCell * cell)
{
    if(cell == nullptr || cell == theInvalidCell)
        return nullptr;
    int x = cell->getXIndex() / _sectorWidthInCells;
    int y = cell->getYIndex() / _sectorWidthInCells;
    
    return _sectorGrid[x][y];
}


MapSector * SectorGrid::sectorAtIndex(int x, int y)
{
    if(x < 0 || y < 0 || x >= _sectorGridSize.width || y >= _sectorGridSize.height)
        return nullptr;
    return _sectorGrid[x][y];
}

MapSector * SectorGrid::sectorForCellIndex(int x, int y)
{
    return sectorAtIndex(x / _sectorWidthInCells, y / _sectorWidthInCells);
}


MapSector * SectorGrid::sectorForPosition(Vec2 position)
{
    int x = (int) position.x / (_sectorWidthInCells);
    int y = (int) position.y / (_sectorWidthInCells);
    
    if(x < 0 || y < 0 || x >= _sectorGridSize.width || y >= _sectorGridSize.height)
        return nullptr;
    return _sectorGrid[x][y];
}

MapSector * SectorGrid::sectorForUID(ENTITY_ID entId)
{
    return _sectorIdToSectorMap.at(entId);
}

void SectorGrid::sectorsInBounds(Rect bounds, std::vector<MapSector *> * array)
{
    int xMin = (int) bounds.origin.x / (_sectorWidthInCells);
    int xMax = (int) (bounds.origin.x + bounds.size.width) / (_sectorWidthInCells);
    int yMin = (int) bounds.origin.y / (_sectorWidthInCells);
    int yMax = (int) (bounds.origin.y + bounds.size.height) / (_sectorWidthInCells);
    
    for(int x = xMin; x <= xMax; x++)
    {
        for(int y = yMin; y <= yMax; y++)
        {
            if(x < 0 || y < 0 || x >= _sectorGridSize.width || y >= _sectorGridSize.height)
                continue;
            array->push_back(_sectorGrid[x][y]);
        }
    }
}

void SectorGrid::sectorsInCircleWithCenter(Vec2 center, double radius, std::vector<MapSector *> * array)
{
    int xCenter = (int) center.x / (_sectorWidthInCells);
    int yCenter = (int) center.y / (_sectorWidthInCells);
    
    //add a bit of a fudge factor since the origin of a sector might be outside the radius even though most of the sector is within it
   // radius += (_sectorWidthInCells) /2;
    
    int sectorRadius = ceil(radius / (_sectorWidthInCells));
    if(sectorRadius == 0)
    {
        array->push_back(_sectorGrid[xCenter][yCenter]);
        return;
    }
        
    for(int x = -sectorRadius; x <= sectorRadius; x++)
    {
        if((xCenter + x) < 0)
        {
            continue;
        }
        if((xCenter + x) >= _sectorGridSize.width)
        {
            break;
        }
        for(int y = -sectorRadius; y <= sectorRadius; y++)
        {
            if((yCenter + y) < 0)
                continue;
            else if((yCenter + y) >= _sectorGridSize.height)
            {
                break;
            }
            
            MapSector * sector = _sectorGrid[xCenter+x][yCenter +y];
            if(sector->getSectorBounds().intersectsCircle(center, radius))
            {
                array->push_back(_sectorGrid[xCenter +x][yCenter + y]);
            }
        }
    }
    if(array->size() == 0)
    {
        LOG_DEBUG_ERROR("WHHHY???? \n");
    }
}

bool SectorGrid::iterateSectorsInLine(Vec2 start, Vec2 end, const std::function<bool (MapSector * sector)> & iteratorFunction)
{
    MapSector * startSector = sectorForPosition(start);
    
    end = constrainPosition(end);
    MapSector * endSector = sectorForPosition(end);
    
    if(startSector == nullptr)
    {
        return false;
    }
    
    Vec2 scaledStart = start / _sectorWidthInCells;
    Vec2 scaledEnd = end / _sectorWidthInCells;
        
    // get the magnitude of x and y
    int xDiff = endSector->getXIndex() - startSector->getXIndex();
    int yDiff = endSector->getYIndex() - startSector->getYIndex();
    
    Vec2 stepChange;
    
    int steps;
    float inverseStepChange;
    float toBorder;

    float * majorAxisStep;
    float * minorAxisStep;
    float * minorAxis;
    float * majorAxis;
    float lastMinorAxis;
    float lastMajorAxis;
    
    Vec2 currentPosition = scaledStart;
    Vec2 previousPosition = scaledStart;
    
    Vec2 majorAxisDir;
    Vec2 minorAxisDir;

    CollisionGrid * grid = world->getCollisionGrid();
    
    // Determine which axis of the line is larger.
    // Walk exactly one value in the direction of the larger (MAJOR) axis
    // For the other axis (minor), walk the ratio between the major and minor axis
    if(abs(scaledEnd.x - scaledStart.x) >= abs(scaledEnd.y - scaledStart.y))
    {
        steps = abs(xDiff);
        stepChange.y = steps == 0 ? 0 : ((scaledEnd.y - scaledStart.y)) / abs(scaledEnd.x - scaledStart.x);
        stepChange.x = xDiff > 0 ? 1: -1;
        
        majorAxisDir = Vec2(stepChange.x, 0);
        minorAxisDir = Vec2(0, stepChange.y > 0 ? 1 : -1);

        majorAxisStep = &stepChange.x;
        minorAxisStep = &stepChange.y;
        
        minorAxis = &currentPosition.y;
        majorAxis = &currentPosition.x;
    }
    else
    {
        steps = abs(yDiff);
        stepChange.x = steps == 0 ? 0 : (scaledEnd.x - scaledStart.x) / abs(scaledEnd.y - scaledStart.y);
        stepChange.y = yDiff > 0 ? 1 : -1;
        
        majorAxisDir = Vec2(0, stepChange.y);
        minorAxisDir = Vec2(stepChange.x > 0 ? 1 : -1, 0);

        majorAxisStep = &stepChange.y;
        minorAxisStep = &stepChange.x;

        minorAxis = &currentPosition.x;
        majorAxis = &currentPosition.y;
    }
    
    toBorder = (*majorAxis - ((int) *majorAxis));
    if(*majorAxisStep > 0)
    {
        toBorder = 1 - toBorder;
    }

    if(*minorAxisStep == 0)
    {
        inverseStepChange = 1000;
    }
    else
    {
        inverseStepChange = fabs(1 / *minorAxisStep);
    }
    
    lastMinorAxis = *minorAxis;
    lastMajorAxis = *majorAxis;
            
    MapSector * sector;

    // Walk along the line iterating the sectors
    for(int i = 0; i < steps; i++)
    {
        if(( (int)lastMinorAxis) != ( (int)*minorAxis))
        {
            float toMinorAxisBorder = lastMinorAxis - ((int)lastMinorAxis);
            if(*minorAxisStep > 0)
            {
                toMinorAxisBorder = 1 - toMinorAxisBorder;
            }
            toMinorAxisBorder *= inverseStepChange;
    
            Vec2 adjacent = previousPosition;
            if(toMinorAxisBorder < toBorder)
            {
                adjacent += minorAxisDir;
            }
            else
            {
                adjacent += majorAxisDir;
            }

            sector = sectorAtIndex(adjacent.x, adjacent.y);
            if(sector == nullptr || iteratorFunction(sector) == false)
            {
                return false;
            }
        }

        sector = sectorAtIndex(currentPosition.x, currentPosition.y);
        if(sector == nullptr || iteratorFunction(sector) == false)
        {
            return false;
        }
        
        lastMinorAxis = *minorAxis;
        lastMajorAxis = *majorAxis;
        previousPosition = currentPosition;
        currentPosition += stepChange;
    }
    
    // One last check for the last sector
    if(( (int)lastMinorAxis) != ( (int)*minorAxis))
    {
        float toMinorAxisBorder = lastMinorAxis - ((int)lastMinorAxis);
        if(*minorAxisStep > 0)
        {
            toMinorAxisBorder = 1 - toMinorAxisBorder;
        }
        toMinorAxisBorder *= inverseStepChange;
        
        Vec2 adjacent = previousPosition;
        if(toMinorAxisBorder < toBorder)
        {
            adjacent += minorAxisDir;
        }
        else
        {
            adjacent += majorAxisDir;
        }
        
        sector = sectorAtIndex(adjacent.x, adjacent.y);
        if(sector == nullptr || iteratorFunction(sector) == false)
        {
            return false;
        }
    }
    if(iteratorFunction(endSector) == false)
    {
        return false;
    }
    
    return true;
}

Vec2 SectorGrid::constrainPosition(Vec2 position)
{
    Vec2 scaledSize = position / _sectorWidthInCells;
    if(position.x < 0 )
    {
        position.x = 0;
    }
    else if(scaledSize.x >= _sectorGridSize.width)
    {
        position.x = (_sectorGridSize.width * _sectorWidthInCells) -1;
    }
    
    if(position.y < 0)
    {
        position.y = 0;
    }
    else if(scaledSize.y >= _sectorGridSize.height)
    {
        position.y = (_sectorGridSize.height * _sectorWidthInCells) -1;
    }
    return position;
}

void SectorGrid::generateSectorGridData()
{
//    MicroSecondSpeedTimer timer( [=](long long int time) {
//        LOG_DEBUG("  Concealment map: %.2f \n", time / 1000000.0);
//    });
//    // Build concealment data
//    for(int y = 0; y < _sectorGridSize.height; y++)
//    {
//        for(int x = 0; x < _sectorGridSize.width; x++)
//        {
//            globalBackgroundTaskHandler->addBackgroundTask([this, y, x] () {
//                _sectorGrid[x][y]->buildConcealmentMap();
//                return false;
//            });
//        }
//    }
    
//    while(globalBackgroundTaskHandler->runNextBackgroundTask()){}

//    timer.stop();
    MicroSecondSpeedTimer timer2( [=](long long int time) {
        LOG_DEBUG("Paths to neighbors: %.2f \n", time / 1000000.0);
    });
    
    // Build paths to neighboring sectors
    // Right/left
    for(int y = 0; y < _sectorGridSize.height; y++)
    {
        globalBackgroundTaskHandler->addBackgroundTask([this, y] () {
            for(int x = 0; x < _sectorGridSize.width -1; x++)
            {
                _sectorGrid[x][y]->buildPathToNeighbor(_sectorGrid[x+1][y]);
            }
            return false;
        });
    }
    //Down/Up
    for(int x = 0; x < _sectorGridSize.width; x++)
    {
        globalBackgroundTaskHandler->addBackgroundTask([this, x] () {
            for(int y = 0; y < _sectorGridSize.height -1; y++)
            {
                _sectorGrid[x][y]->buildPathToNeighbor(_sectorGrid[x][y+1]);
            }
            return false;
        });
    }
    
    //up-right/down-left
    for(int y = 0; y < _sectorGridSize.height-1; y++)
    {
        for(int x = 0; x < _sectorGridSize.width-1; x++)
        {
            _sectorGrid[x][y]->buildPathToNeighbor(_sectorGrid[x+1][y+1]);
        }
    }
    
    //down-right/up-left
    for(int y = 0; y < _sectorGridSize.height-1; y++)
    {
        for(int x = 1; x < _sectorGridSize.width; x++)
        {
            _sectorGrid[x][y]->buildPathToNeighbor(_sectorGrid[x-1][y+1]);
        }
    }
    while(globalBackgroundTaskHandler->runNextBackgroundTask()){}
    timer2.stop();
}


void SectorGrid::toggleDrawSectors()
{
    _drawingSectors = !_drawingSectors;
    drawSectors();
}

void SectorGrid::drawSectors()
{
    _redrawSectors = false;
    if(_drawingSectors)
    {
        if(_sectorLines.size() == 0)
        {
            float maxX = _sectorGrid[(int)_sectorGridSize.width-1][(int)_sectorGridSize.height-1]->getSectorBounds().origin.x + _sectorGrid[(int)_sectorGridSize.width-1][(int)_sectorGridSize.height-1]->getSectorBounds().size.width;
            float maxY = _sectorGrid[(int)_sectorGridSize.width-1][(int)_sectorGridSize.height-1]->getSectorBounds().origin.y +_sectorGrid[(int)_sectorGridSize.width-1][(int)_sectorGridSize.height-1]->getSectorBounds().size.height;

            LineDrawer * line;
            for(int x = 0; x < _sectorGridSize.width; x++)
            {
                Vec2 start(_sectorGrid[x][0]->getSectorBounds().origin.x, 0);
                Vec2 end(_sectorGrid[x][0]->getSectorBounds().origin.x,  maxY);
                
                line = LineDrawer::create(start * WORLD_TO_GRAPHICS_SIZE, end * WORLD_TO_GRAPHICS_SIZE, 5);
                line->setColor(Color3B::BLACK);
                _sectorLines.pushBack(line);
            }
            
            line = LineDrawer::create(Vec2(maxX, 0) * WORLD_TO_GRAPHICS_SIZE, Vec2(maxX, maxY) * WORLD_TO_GRAPHICS_SIZE, 5);
            line->setColor(Color3B::BLACK);
            _sectorLines.pushBack(line);

            
            for(int y = 0; y < _sectorGridSize.height; y++)
            {
                Vec2 start(0, _sectorGrid[0][y]->getSectorBounds().origin.y);
                Vec2 end(maxX, _sectorGrid[0][y]->getSectorBounds().origin.y);
                
                line = LineDrawer::create(start * WORLD_TO_GRAPHICS_SIZE, end * WORLD_TO_GRAPHICS_SIZE, 5);
                line->setColor(Color3B::BLACK);
                _sectorLines.pushBack(line);
            }
            
            line = LineDrawer::create(Vec2(0, maxY) * WORLD_TO_GRAPHICS_SIZE, Vec2(maxX, maxY) * WORLD_TO_GRAPHICS_SIZE, 5);
            line->setColor(Color3B::BLACK);
            _sectorLines.pushBack(line);
        }
        else
        {
            for(auto line : _sectorLines)
            {
                line->setVisible(true);
            }
        }
        
        for(auto line : _sectorConnectionLines)
        {
            line->remove();
        }
        
        _sectorConnectionLines.clear();
        LineDrawer * line;
        for(int x = 0; x < _sectorGridSize.width; x++)
        {
            for(int y = 0; y < _sectorGridSize.height; y++)
            {
                if(_sectorGrid[x][y]->getPathingCell() == nullptr)
                {
                    continue;
                }
                Vec2 start = _sectorGrid[x][y]->getPathingCell()->getCellPosition() * WORLD_TO_GRAPHICS_SIZE;
                Vec2 end = start + Vec2(10,10);
                line = LineDrawer::create(start , end , 5);
                line->setColor(Color3B::RED);
                line->setOpacity(125);
                _sectorConnectionLines.pushBack(line);
                
                for(auto path : _sectorGrid[x][y]->getPathsToNeighbors())
                {
                    Vec2 start = _sectorGrid[x][y]->getPathingCell()->getCellPosition()  * WORLD_TO_GRAPHICS_SIZE;
                    Vec2 end = path.first->getPathingCell()->getCellPosition()  * WORLD_TO_GRAPHICS_SIZE;

                    line = LineDrawer::create(start, end, 1);
                    if(start.x < end.x || start.y < end.y)
                    {
                        line->setColor(Color3B::BLUE);
                        line->setOpacity(125);
                        line->setWidth(1);
                        start += Vec2(1,1);
                        end += Vec2(1,1);
                        line->setLinePosition(start, end);
                    }
                    else
                    {
                        line->setColor(Color3B::GREEN);
                    }
                    line->makeDashed();
                    line->setFlowing(true);
                    _sectorConnectionLines.pushBack(line);
                }
            }
        }
    }
    else
    {
        for(auto line : _sectorLines)
        {
            line->setVisible(false);
        }
        for(auto line : _sectorConnectionLines)
        {
            line->remove();
        }
        _sectorConnectionLines.clear();
    }
}

void SectorGrid::shutdown()
{
    for(auto line : _sectorLines)
    {
        line->remove();
    }
    _sectorLines.clear();
    for(int x = 0; x < _sectorGridSize.width; x++)
    {
        for(int y = 0; y < _sectorGridSize.height; y++)
        {
            _sectorGrid[x][y]->shutdown();
            _sectorGrid[x][y]->release();
        }
        delete [] _sectorGrid[x];
    }
    delete [] _sectorGrid;
}
