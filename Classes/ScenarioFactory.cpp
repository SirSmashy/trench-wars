//
//  Scenario.cpp
//  TrenchWars
//
//  Created by Paul Reed on 5/2/20.
//

#include "ScenarioFactory.h"
#include "CollisionGrid.h"

#include "StringUtils.h"
#include <stdexcept>

ScenarioFactory * globalScenarioFactory;

ScenarioFactory::ScenarioFactory()
{
    globalScenarioFactory = this;
}

void ScenarioFactory::parseScenarioFile(const std::string & filename)
{
    nextCommandId = 0;
    std::string fileContents = FileUtils::getInstance()->getStringFromFile(filename);
    if(!fileContents.empty())
    {
        xml_document doc;
        pugi::xml_parse_result result = doc.load(fileContents.c_str());
        if(result.status == xml_parse_status::status_ok)
        {
            bool parsedSuccessfully = doc.traverse(*this);
            if(!parsedSuccessfully)
            {
                CCLOG("parse error!");
            }
        }
        else
        {
            CCLOG("parse error %s ",result.description());
        }
    }
    else
    {
        CCLOG("ScenarioFactory: Could not Open File %s",filename.c_str());
    }
}

bool ScenarioFactory::for_each(xml_node & node)
{
    ci_string name = node.name();
    std::string value = node.first_child().value();
    
    try
    {
        if(name.compare("Scenario") == 0)
        {
            currentScenarioDefinition = new ScenarioDefinition();
            nextCommandId = 0;
            auto attribute = node.attribute("name");
            if(attribute != nullptr)
            {
                currentScenarioDefinition->scenarioName = attribute.value();
            }
            else
            {
                CCLOG("Failure to Parse Scenario. Scenarior element is missing name attribute");
            }
            
            attribute = node.attribute("terrain");
            if(attribute != nullptr)
            {
                currentScenarioDefinition->terrainName = attribute.value();
            }
            else
            {
                CCLOG("Failure to Parse Scenario. Scenarior element is missing terrain attribute");
            }
            
            scenarios.insert(currentScenarioDefinition->scenarioName, currentScenarioDefinition);
            
        }
        else if(name.compare("Team") == 0)
        {
            currentTeamDefinition = new TeamDefinition;
            currentTeamDefinition->teamId = currentScenarioDefinition->teams.size();
            currentScenarioDefinition->teams.pushBack(currentTeamDefinition);
        }
        else if(name.compare("Command") == 0)
        {
            currentCommandDefinition = new CommandDefinition();
            auto attribute = node.attribute("spawnPoint");
            if(attribute != nullptr)
            {
                currentCommandDefinition->spawnPoint = parseVector(attribute.value());
            }
            else
            {
                CCLOG("Failure to Parse Command. Command element is missing spawnPoint attribute");
            }
            attribute = node.attribute("supplyRate");
            if(attribute != nullptr)
            {
                currentCommandDefinition->supplyRate = std::stoi(attribute.value());
            }
            else
            {
                CCLOG("Failure to Parse Command. Command element is missing supplyRate attribute");
            }
            attribute = node.attribute("initialSupply");
            if(attribute != nullptr)
            {
                currentCommandDefinition->initialSupply = std::stoi(attribute.value());
            }
            else
            {
                CCLOG("Failure to Parse Command. Command element is missing initialSupply attribute");
            }
            attribute = node.attribute("spawnAction");
            if(attribute != nullptr)
            {
                currentCommandDefinition->spawnAction = stringToSpawnAction(attribute.value());
            }
            
            currentCommandDefinition->commandId = nextCommandId++;
            currentTeamDefinition->commands.pushBack(currentCommandDefinition);

        }
        else if(name.compare("CommandPost") == 0)
        {
            currentCommandSpawn = new CommandPointSpawn();
            auto attribute = node.attribute("definitionName");
            if(attribute != nullptr)
            {
                currentCommandSpawn->commandPostDefinitionName = attribute.value();
            }
            else
            {
                CCLOG("Failure to Parse Command Post Spawn. Element is missing definitionName attribute");
            }
            attribute = node.attribute("location");
            if(attribute != nullptr)
            {
                currentCommandSpawn->location = parseVector(attribute.value());
            }
            else
            {
                CCLOG("Failure to Parse Command Post Spawn. Element is missing location attribute");
            }
            currentCommandDefinition->commandPoints.pushBack(currentCommandSpawn);
        }
        else if(name.compare("Formation") == 0)
        {
            currentFormationSpawn = new FormationSpawn();
            auto attribute = node.attribute("definitionName");
            if(attribute != nullptr)
            {
                currentFormationSpawn->formationDefinitionName = attribute.value();
            }
            else
            {
                CCLOG("Failure to Parse Formation Spawn. Element is missing definitionName attribute");
            }
            attribute = node.attribute("location");
            if(attribute != nullptr)
            {
                currentFormationSpawn->location = parseVector(attribute.value());
            }
            else
            {
                CCLOG("Failure to Parse Formation Spawn. Element is missing location attribute");
            }
            currentCommandDefinition->formations.pushBack(currentFormationSpawn);
        }
    }
    catch (const std::invalid_argument& ia)
    {
        CCLOG("Failure to Parse Scenario. Invalid argument: %s for node %s with value %s ", ia.what(), name.c_str(), value.c_str());
        return false;
    }
    
    return true;
}

ScenarioDefinition * ScenarioFactory::getScenario(const std::string & scenarioName)
{
    return scenarios.at(scenarioName);
}

std::vector<std::string> ScenarioFactory::getScenarioNames()
{
    return scenarios.keys();
}

DEBUG_SPAWN_MODE ScenarioFactory::stringToSpawnAction(const std::string & spawnString)
{
    if(spawnString.compare("attack") == 0)
    {
        return SPAWN_ATTACK_CP;
    }
    if(spawnString.compare("left") == 0)
    {
        return SPAWN_GO_LEFT;
    }
    if(spawnString.compare("right") == 0)
    {
        return SPAWN_GO_RIGHT;
    }
    if(spawnString.compare("digIn") == 0)
    {
        return SPAWN_DIG_IN;
    }
    return SPAWN_DO_NOTHING;
}

