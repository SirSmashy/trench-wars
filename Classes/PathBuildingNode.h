//
//  PathBuildingNode.hpp
//  TrenchWars
//
//  Created by Paul Reed on 9/16/22.
//

#ifndef PathBuildingNode_h
#define PathBuildingNode_h

#include "Refs.h"
#include "CollisionCell.h"

class MapSector;
class NavigationPatch;
class NavigationPatchBorder;
class PatchPathEdge;

enum PATH_TYPE
{
    MINIMUM_PATH,
    TOWARD_GOAL_PATH,
    GOAL_PATH
};

class PathBuildingNode : public ComparableRef
{
protected:
    double _totalEstimatedCost;
    double _costToTravelHere;
    double _remainingTravelCostHeuristic;
    double _distanceFromParent;
    PathBuildingNode * _parent;
    bool _examined;
    int _team;
    
public:
    
    PathBuildingNode();
    
    double getCostToTravelHere() {return _costToTravelHere;}
    double getEstimatedTotalCost() {return _totalEstimatedCost;}
    double getRemainingCostHeuristic() {return _remainingTravelCostHeuristic;}
    double getDistanceFromParent() {return _distanceFromParent;}
    PathBuildingNode * getParent() {return _parent;}
    void setParent(PathBuildingNode * newParent) { _parent = newParent;}
    
    inline bool isExamined() {return _examined;}
    inline void setExamined(bool examined) {_examined = examined;}
    
    virtual CollisionCell * getPosition() {return nullptr;}
    virtual CollisionCell * getGoalCell() {return nullptr;}

    
    virtual double getCongestionMultiplier() {return 1.0;}
    virtual int getPathTypeInt() {return 0;}
    
    //debug variables
    double totalDistance;
    int updates;
    double congestion;
    bool usedInPath;
};

class SectorPathBuildingNode : public PathBuildingNode
{
    MapSector * _sector;

public:
    
    ~SectorPathBuildingNode();
    SectorPathBuildingNode(MapSector * sector, MapSector * goalSector, int team = 9);
    SectorPathBuildingNode(SectorPathBuildingNode * parent, MapSector * sector, MapSector * goalSector, int team = 9);
    MapSector * getMapSector() {return _sector;}
    
    bool update(SectorPathBuildingNode * potentialNewParent, MOVEMENT_PREFERENCE movePreference);
    
    double getCongestionMultiplier();

    virtual CollisionCell * getPosition();

};


class PatchPathBuildingNode : public PathBuildingNode
{
protected:
    PATH_TYPE _pathType;
    int _pathLength;
    NavigationPatch * _patch;
    CollisionCell * _goalCell;
    
    void calculateHeuristic(MOVEMENT_PREFERENCE movePreference);
    
    
public:
    ~PatchPathBuildingNode();
    PatchPathBuildingNode();
    PatchPathBuildingNode(CollisionCell * start, NavigationPatch * patch, CollisionCell * goalCell, int team = 9);
    PatchPathBuildingNode(PatchPathBuildingNode * parent, NavigationPatch * patch, CollisionCell * goalCell, PATH_TYPE pathType, int team = 9);
    
    NavigationPatch * getNavigationPatch() {return _patch;}
    virtual bool update(PatchPathBuildingNode * potentialNewParent, MOVEMENT_PREFERENCE movePreference);
    virtual CollisionCell * getPosition();

    int getPathLength() {return _pathLength;}
    PATH_TYPE getPathType() {return _pathType;}
    double getCongestionMultiplier();
    
    CollisionCell * getGoalCell() {return _goalCell;}

    virtual int getPathTypeInt() {return (int) _pathType;}

    // Path smoothing variables
    bool useExitCellInPath;
    bool pivot;
    NavigationPatchBorder * _crossedBorder;
    CollisionCell * exitFromPreviousPatch;
    CollisionCell * entranceToThisPatch;
};

class GoalCellPathBuildingNode : public PatchPathBuildingNode
{
public:
    GoalCellPathBuildingNode(PatchPathBuildingNode * parent, NavigationPatch * patch, CollisionCell * goalCell, int team = 9);
    virtual bool update(PatchPathBuildingNode * potentialNewParent, MOVEMENT_PREFERENCE movePreference);
    PATH_TYPE getPathType() {return GOAL_PATH;}
};

#endif /* PathBuildingNode_h */
