//
//  ClientEventController.h
//  TrenchWars
//
//  Created by Paul Reed on 12/12/23.
//

#ifndef ClientEventController_h
#define ClientEventController_h


#include "cocos2d.h"
#include "GameEventController.h"
#include "List.h"
#include <cereal/types/string.hpp>


class InputActionHandler;
class PlanningMenu;

// /////////////////////
// /////////////////////
// Orders the client will send to the server
struct PendingOrder : public Ref
{
    ENTITY_ID unit;
    ENTITY_ID plan;
    ORDER_TYPE type;
    
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const
    {
        archive(unit);
        archive(plan);
        archive(type);
    }
};

struct PendingMoveOrder : public PendingOrder
{
    Vec2 position;
    
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const
    {
        PendingOrder::saveToArchive(archive);
        archive(position);
    }
    
};

struct PendingObjectiveOrder : public PendingOrder
{
    ENTITY_ID objectiveId;
    OBJECTIVE_ORDER_ACTION objectiveAction;
    
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const
    {
        PendingOrder::saveToArchive(archive);
        archive(objectiveId);
        archive(objectiveAction);
    }
};

// /////////////////////
// /////////////////////
// Actiosn taken by the client to modify an objective. Will be sent to server
struct PendingObjectiveAction : public Ref
{
    OBJECTIVE_ACTION action;
    ENTITY_ID objectiveId;
    
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const
    {
        archive(action);
        archive(objectiveId);
    }
};

struct PendingObjectiveCreate : public PendingObjectiveAction
{
    std::vector<Vec2> points;
    bool enclosed;
    ENTITY_ID unitCreatingId;
    
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const
    {
        PendingObjectiveAction::saveToArchive(archive);
        archive(points.size());
        for(auto point : points)
        {
            archive(point);
        }
        archive(enclosed);
        archive(unitCreatingId);
    }
};

struct PendingObjectiveCreateCommandPost : public PendingObjectiveAction
{
    Vec2 position;
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const
    {
        PendingObjectiveAction::saveToArchive(archive);
        archive(position);
    }
};

// /////////////////////
// /////////////////////
// Actions taken by the client to modify a plan. WIll be sent to server
struct PendingPlanAction : public Ref
{
    PLAN_ACTION action;
    ENTITY_ID planId;
    
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const
    {
        archive(action);
        archive(planId);
    }
};

struct PendingPlanRename : public PendingPlanAction
{
    std::string name;
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const
    {
        PendingPlanAction::saveToArchive(archive);
        archive(name);
    }
};

struct PendingNewPlan : public PendingPlanAction
{
    int playerId;
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const
    {
        PendingPlanAction::saveToArchive(archive);
        archive(playerId);
    }
};
// /////////////////////
// /////////////////////


class ClientEventController : public GameEventController
{
protected:
    
    List<PendingOrder *> _pendingOrders;
    List<PendingObjectiveAction *> _pendingObjectiveActions;
    List<PendingPlanAction *> _pendingPlanActions;

    std::mutex _pendingMutex;

public:
    
    ClientEventController();
    virtual void init();
    virtual void postLoad();
        
    static ClientEventController * create();
    virtual void query(float deltaTime);
    
    void parseServerUpdate(cereal::BinaryInputArchive & archive);
    
    void buildClientUpdatePacket(cereal::BinaryOutputArchive & archive);
        

    // ////////  New Event Requests ////////
    // Orders
    virtual void issueMoveOrder(Vec2 position, Unit * commandable, Plan * addToPlan);
    virtual void issueOccupyOrder(Objective * objective, Unit * commandable, Plan * addToPlan);
    virtual void issueDigTrenchesOrder(Objective * objective, Unit * commandable, Plan * addToPlan);
    virtual void issueDigBunkersOrder(Objective * objective, Unit * commandable, Plan * addToPlan);
    virtual void issueBuildCommandPostOrder(Objective * objective, Unit * commandable, Plan * addToPlan);
    virtual void issueClearAreaOrder(Objective * objective, Unit * commandable, Plan * addToPlan);
    virtual void issueAttackPositionOrder(Objective * objective, Unit * commandable, Plan * addToPlan);
    virtual void issueStopOrder(Unit * commandable, Plan * addToPlan);
    
    //Objective
    virtual void createNewObjective(std::vector<Vec2> & points, bool enclosed, Unit * unitCreating);
    virtual void removeObjective(Objective * objective);
    
    // Command Posts
    virtual void createCommandPostInObjective(Objective * objective, Vec2 location);
    
    // Trench Lines
    virtual void createTrenchLineInObjective(Objective * objective);
    virtual void removeTrenchLineInObjective(Objective * objective);
    
    // Bunkers
    virtual void createBunkersInObjective(Objective * objective);
    virtual void removeBunkersInObjective(Objective * objective);
    
    // Plans
    virtual void createNewPlan(int playerId);
    virtual void removePlan(Plan * plan);
    virtual void executePlan(Plan * plan);
    virtual void stopPlan(Plan * plan);
    virtual void renamePlan(Plan * plan, const std::string & newName);
    
    // Team status
    virtual void updateTeamVisibility(Team * team, 
                                      std::unordered_map<ENTITY_ID, double> & sectorVisibleTime,
                                      std::unordered_set<ENTITY_ID> & sectorsNewlyHidden,
                                      std::unordered_set<ENTITY_ID> & sectorsNewlyVisible);

    
    // ///////// Handling of newly created events ///////
    // Orders
    virtual void handleEventOrderIssued(Order * order, Unit * unit, Plan * addToPlan);
    virtual void handleEventOrderComplete(Unit * unitCompleting, Order * order, bool failed = false);
    virtual void handleEventOrderCancelled(Unit * commandable);

    
    // Objectives
    virtual void handleEventObjectiveCreated(Objective * objective);
    virtual void handleEventObjectiveRemoved(Objective * objective);
    
    // visible
    virtual void handleEventSectorVisible(MapSector * sector);
    virtual void handleEventTeamVisibilityUpdate(Team * team, std::unordered_map<ENTITY_ID, double> & sectorVisibleTime);

    // command post related
    virtual void handleEventNewCommandPost(CommandPost * commandPost);
    virtual void handleEventCommandPostDestroyed(CommandPost * commandPost);
    
    // Unit related
    virtual void handleEventNewUnit(Unit * unit);
    virtual void handleEventUnitDestroyed(Unit * unit);
        
    // Plan related
    virtual void handleEventNewPlan(Plan * plan);
    virtual void handleEventPlanRemoved(Plan * plan);
    virtual void handleEventPlanExecuting(Plan * plan);
    virtual void handleEventPlanStopped(Plan * plan);
    virtual void handleEventPlanRenamed(Plan * plan, const std::string & newName);
    virtual void handleEventOrderRemovedFromPlan(Unit * unit, Plan * plan);
    
    //friendly unit presence
    virtual void handleEventUnitInSector(MapSector * sector, Unit * unit);
    
    
    //enemy unit presence
    virtual void handleEventEnemyUnitSpotted(Unit * enemy);
    virtual void handleEventEnemyCommandPostSpotted(CommandPost *enemy);
    virtual void handleEventEnemyCanEngageSector(MapSector * sector, Human * enemy);
    virtual void handleEventEnemyArtilleryInSector(MapSector * sector);
    //////////// end low level event functions
};

#endif /* ClientEventController_h */
