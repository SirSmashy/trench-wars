//
//  LineDrawer.hpp
//  TrenchWars
//
//  Created by Paul Reed on 12/5/22.
//

#ifndef LineDrawer_h
#define LineDrawer_h

#include "cocos2d.h"
#include "AnimatedSprite.h"
USING_NS_CC;

class LineDrawer : public AnimatedSprite
{
private:
    Vec2 _endPosition;
    float _lineWidth;
    bool _flowing;
    bool _scaleWidth;
    
    Rect _previousTextureCoords;
    CC_CONSTRUCTOR_ACCESS :
    virtual bool init(Vec2 startPosition, Vec2 endPosition, float width);
    ~LineDrawer();
public:
    static LineDrawer * create(Vec2 startPosition, Vec2 endPosition, float width = 2.0);
    void setLinePosition(Vec2 startPosition, Vec2 endPosition);
    void setEndPosition(Vec2 endPosition);
    void setStartPosition(Vec2 startPosition);
    void setAnimation(ANIMATION_TYPE animation, bool repeat);
    float getWidth() {return _lineWidth;}
    void setWidth(float width);
    void setScaleWidthWithParentScale(bool scaleWidth);
    virtual void update(float delta);

    void makeSolid();
    void makeDashed();
    void makeDotted();
    void setFlowing(bool flowing);
    void remove();
};

#endif /* LineDrawer_hpp */
