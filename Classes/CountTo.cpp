//
//  Counter.cpp
//  TrenchWars
//
//  Created by Paul Reed on 5/18/20.
//

#include "CountTo.h"


CountTo::CountTo(int countTo, bool randomStart)
{
    _countTo = countTo;
    _currentCount = 0;
    if(randomStart) {
        _currentCount = globalRandom->randomInt(0, _countTo - 1);
    }
}


bool CountTo::count() {
    _currentCount++;
    if(_currentCount >= _countTo) {
        _currentCount = 0;
        return true;
    }
    return false;
}

void CountTo::setToMaximum()
{
    _currentCount = _countTo;
}

void CountTo::reset() {
    _currentCount = 0;
}
