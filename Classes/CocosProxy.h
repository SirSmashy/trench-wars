//
//  CocosProxy.h
//  TrenchWars
//
//  Created by Paul Reed on 6/23/22.
//

#ifndef SpriteProxy_h
#define SpriteProxy_h

#include "cocos2d.h"
#include "Refs.h"


USING_NS_CC;


enum ProxyType
{
    ANIMATED_SPRITE_PROXY,
    POLYGON_SPRITE_PROXY,
    PARTICLE_SPRITE_PROXY
};

class CocosProxy : public ComparableRef
{
public:
    
    virtual void createCocosObject() = 0;
    virtual bool updateCocosObject() = 0;
    virtual Sprite * getSprite() = 0;
    virtual ProxyType getProxyType() = 0;
};

#endif /* SpriteProxy_h */
