//
//  Order.hpp
//  TrenchWars
//
//  Created by Paul Reed on 9/7/20.
//

#ifndef Order_h
#define Order_h

#include "cocos2d.h"
#include "Refs.h"
#include "PhysicalEntity.h"
#include "ObjectiveObjectCollection.h"

USING_NS_CC;

enum ORDER_TYPE
{
    MOVE_ORDER,
    OCCUPY_ORDER,
    OBJECTIVE_WORK_ORDER,
    ENTITY_WORK_ORDER,
    ATTACK_POSITION_ORDER,
    ATTACK_TARGET_ORDER,
    STOP_ORDER,
    WAIT_ORDER
};

class Objective;
class CollisionCell;
class Human;
class Plan;
class Unit;
class Order;


class Order : public ComparableRef
{
protected:

    ENTITY_ID _owningPlanId;

public:
    Order();
    virtual ~Order();
    virtual ORDER_TYPE getOrderType() = 0;
    
    virtual Vec2 getPosition();
        
    virtual void setOwningPlanId(ENTITY_ID plan) {_owningPlanId = plan;}
    virtual ENTITY_ID getOwningPlanId() {return _owningPlanId;}
        
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
    
    std::string getOrderTypeAsString();
    
    static Order * createOrderOfType(ORDER_TYPE type);
};


class MoveOrder : public Order
{
protected:
    Vec2  _position;
    bool  _ignoreCover;
    
    MoveOrder();
    MoveOrder(Vec2 location, bool ignoreCover);
    ~MoveOrder();
        
public:
    static MoveOrder * create(Vec2 location, bool ignoreCover = false);
    static MoveOrder * create();
    
    virtual ORDER_TYPE getOrderType() {return MOVE_ORDER;}
    
    virtual Vec2 getPosition() {return _position;}
    void setPosition(Vec2 position) {_position = position;}

    bool shouldIgnoreCover() {return _ignoreCover;}
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

class OccupyOrder : public MoveOrder
{
private:
    Objective * _objective;
    ENTITY_ID _objectiveId;
    
    OccupyOrder();
    OccupyOrder(Objective * objective, bool ignoreCover);
    ~OccupyOrder();
    
public:
    static OccupyOrder * create(Objective * objective, bool ignoreCover = false);
    static OccupyOrder * create();
    
    virtual ORDER_TYPE getOrderType() {return OCCUPY_ORDER;}

        
    Objective * getObjective() {return _objective;}
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};


class WorkOrder : public Order
{
protected:
    
    WorkOrder();
public:
    ~WorkOrder();
        
    virtual Vec2 getPosition() {return getAverageWorkPosition();}
    virtual Vec2 getAverageWorkPosition() = 0;
};

class EntityWorkOrder : public WorkOrder
{
    PhysicalEntity * _workObject;
    EntityWorkOrder(PhysicalEntity * workObject);

public:
    
    static EntityWorkOrder * create(PhysicalEntity * workObject = nullptr);
    virtual ORDER_TYPE getOrderType() {return ENTITY_WORK_ORDER;}


    virtual Vec2 getAverageWorkPosition();
    PhysicalEntity * getWorkObject() {return _workObject;}

    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;

};

class ObjectiveWorkOrder : public WorkOrder
{
    ObjectiveObjectCollection * _workObjects;
    ObjectiveWorkOrder(ObjectiveObjectCollection * workObjects);

public:
    
    static ObjectiveWorkOrder * create(ObjectiveObjectCollection * workObjects = nullptr);
    virtual ORDER_TYPE getOrderType() {return OBJECTIVE_WORK_ORDER;}

    virtual Vec2 getAverageWorkPosition();
    ObjectiveObjectCollection * getWorkObjects() {return _workObjects;}

    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;

};

class AttackPositionOrder : public Order
{
private:
    Objective * _objective;
    AttackPositionOrder(Objective * objective);
    AttackPositionOrder();

public:
    ~AttackPositionOrder();
    static AttackPositionOrder * create(Objective * objective);
    static AttackPositionOrder * create();
    
    virtual ORDER_TYPE getOrderType() {return ATTACK_POSITION_ORDER;}
    virtual Vec2 getPosition() {return getRandomTargetPoint();}

    Vec2 getRandomTargetPoint();
    Objective * getTargetObjective() {return _objective;}
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

class StopOrder : public Order
{
private:
    StopOrder();
    ~StopOrder();
public:
    static StopOrder * create();
    virtual Vec2 getPosition() {return Vec2();}
    virtual ORDER_TYPE getOrderType() {return STOP_ORDER;}
};


enum WAIT_TYPE
{
    WAIT_STRESS,
};

class WaitUntilOrder : public Order
{
private:
    
    WAIT_TYPE _waitType;
    double _waitTheshold;
    Human * _waitingEnt;
    ENTITY_ID _waitEntId;
    
    WaitUntilOrder();
    WaitUntilOrder(WAIT_TYPE type, Human * waitingEnt, double threshold);
    
public:
    static WaitUntilOrder * create(WAIT_TYPE type,Human * waitingEnt, double threshold);
    static WaitUntilOrder * create();
    virtual ORDER_TYPE getOrderType() {return WAIT_ORDER;}

    bool isWaitComplete();
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

#endif /* Order_h */
