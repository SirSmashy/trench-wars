//
//  ClientProjectile.cpp
//  TrenchWars
//
//  Created by Paul Reed on 4/8/24.
//

#include "ClientProjectile.h"
#include "CollisionGrid.h"
#include "ParticleManager.h"
#include "GameVariableStore.h"
#include "EntityFactory.h"

void ClientProjectile::act(float deltaTime)
{
    Projectile::act(deltaTime);
    if(_collided)
    {
        if(_projectileType != DIRECT && _projectileType != SHRAPNEL)
        {
            createExplosion();
        }
        removeFromGame();
        return;
    }
}


/**
 *
 *
 */
void ClientProjectile::createExplosion()
{
    Vec2 location = getPosition();
    CollisionCell * center = getCurrentCollisionGrid()->getCellForCoordinates(location);
    if(center == nullptr)
    {
        return;
    }
    
    double speed;
    double projectileCount = _attackDamage / globalVariableStore->getVariable(ProjectileDamageToProjectileCountDivisor);
    double halfPower = _attackDamage / 2;
    double quarterPower = _attackDamage / 4;
    Vec2 goalPosition;
    double projectileDirection = VectorMath::signedAngleBetweenVecs(Vec2(1,0), _projectileVelocity);
    
    ProjectileDefinition * shrapnal = (ProjectileDefinition *) globalEntityFactory->getPhysicalEntityDefinitionWithName("shrapnel");
    
    float radsPerProjectile = _projectileDefinition->_burstAngle * D2R / projectileCount;
    float projectileAngle = projectileDirection - ((_projectileDefinition->_burstAngle * D2R) /2);
    
    goalPosition = Vec2(1000,0);
    goalPosition.rotate(Vec2(), projectileAngle);
    goalPosition += location;
    
    for(int i = 0; i < projectileCount; i++)
    {
        speed = globalRandom->randomInt(halfPower, _attackDamage) * 2;
        Projectile::create(shrapnal, location, goalPosition, 0, center->getTerrainCharacteristics().getHeight(), SHRAPNEL, 0, _owner, speed, 0);
        goalPosition.rotate(location, radsPerProjectile);
    }
    
    if(!_projectileDefinition->_detonateParticleName.empty())
    {
        globalParticleManager->requestParticle(_projectileDefinition->_detonateParticleName, getPosition());
    }
}
