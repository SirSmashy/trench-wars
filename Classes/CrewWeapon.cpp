//
//  CrewWeapon.cpp
//  TrenchWars
//
//  Created by Paul Reed on 3/15/13.
//  Copyright (c) 2013 . All rights reserved.
//

#include "CrewWeapon.h"
#include "EngagementManager.h"
#include "CollisionCell.h"
#include "VectorMath.h"
#include "CrewWeaponUnit.h"
#include "GameStatistics.h"
#include "MultithreadedAutoReleasePool.h"
#include "CollisionGrid.h"
#include "Infantry.h"
#include "Command.h"
#include "Team.h"
#include "EntityFactory.h"
#include "EntityManager.h"
#include "World.h"
#include "BackgroundTaskHandler.h"

CrewWeapon::CrewWeapon() : MovingPhysicalEntity(),
_pendingWork(0),
_pendingRemoval(false)
{
}

CrewWeapon::~CrewWeapon()
{
}

bool CrewWeapon::init(CrewWeaponDefinition * definition, Vec2 position, Command * command, CrewWeaponUnit * unit)
{
    if(MovingPhysicalEntity::init(definition, position, command->getOwningTeam()) && Weapon::init(definition->_weapon, nullptr))
    {
        _owningUnit = unit;
        _crewSize = unit->getMaxStrength();
        _goal = nullptr;
        _fireOffsets[RIGHT] = definition->_muzzlePositionOffset;
        _fireOffsets[UP_RIGHT] = definition->_muzzlePositionOffset;
        _fireOffsets[UP] = Vec2(definition->_muzzlePositionOffset.x /2, definition->_muzzlePositionOffset.y);
        _fireOffsets[UP_LEFT] = Vec2(0, definition->_muzzlePositionOffset.y);
        _fireOffsets[LEFT] = Vec2(0, definition->_muzzlePositionOffset.y);
        _fireOffsets[DOWN_LEFT] = Vec2(0, 0);
        _fireOffsets[DOWN] = Vec2(definition->_muzzlePositionOffset.x /2, 0);
        _fireOffsets[DOWN_RIGHT] = Vec2(definition->_muzzlePositionOffset.x, 0);

    }
    else
    {
        CCLOG("oh no!!! ");
    }
    return true;
}

CrewWeapon * CrewWeapon::create(CrewWeaponDefinition * definition, Vec2 position, Command * command, CrewWeaponUnit * unit)
{
    CrewWeapon * newWeapon = new CrewWeapon();
    if(newWeapon->init(definition, position, command, unit))
    {
        globalAutoReleasePool->addObject((MovingPhysicalEntity *) newWeapon);
        return newWeapon;
    }
    CC_SAFE_DELETE(newWeapon);
    return nullptr;
}

CrewWeapon * CrewWeapon::create(const std::string & definitionName, Vec2 position, Command * command, CrewWeaponUnit * unit)
{
    CrewWeaponDefinition * definition = (CrewWeaponDefinition *) globalEntityFactory->getPhysicalEntityDefinitionWithName(definitionName);
    if(definition != nullptr)
    {
        return CrewWeapon::create(definition, position, command, unit);
    }
    return nullptr;
}

CrewWeapon * CrewWeapon::create()
{
    CrewWeapon * newWeapon = new CrewWeapon();
    globalAutoReleasePool->addObject((MovingPhysicalEntity *) newWeapon);
    return newWeapon;
}

void CrewWeapon::query(float deltaTime)
{
    _moveRate = 0;

    if(_pendingWork.isChanged())
    {
        float work = _pendingWork;
        _pendingWork.clearChange();
        if (!atGoalPosition())
        {
            _moveRate += work * (_maximumMoveRate / ( _crewSize * deltaTime));
            if (_moveRate > _maximumMoveRate)
            {
                _moveRate = _maximumMoveRate;
            }
        }
    }
    if(_pendingRemoval.isChanged())
    {
        removeFromGame();
        return;
    }
    if(_pendingGoal.isChanged())
    {
        Goal * goal = _pendingGoal.get();
        if(_goal != nullptr)
        {
            _goal->release();
            _goal = nullptr;
        }
        
        if(goal != nullptr)
        {
            _goal = goal;
            _goal->retain();
        }
        else
        {
            setGoalPosition(getPosition());
        }
        
        _pendingGoal.clearChange();
    }
    if(_goal != nullptr)
    {
        if(_goal->getGoalType() == MOVE_ORDER)
        {
            MoveGoal * move = (MoveGoal *) _goal;
           // ///////
            if(move->getNeedsOptimization())
            {
                if(!move->getWaitingForOptimize())
                {
                    move->setWaitingForOptimize(true);
                    globalBackgroundTaskHandler->addPathfindingTask([move, this]
                                                                    {
                        Vec2 position = move->getPosition();
                        position = world->getCollisionGrid(position)->getValidMoveCellAroundPosition(position);
                        CollisionCell * goalCell = world->getCollisionGrid(position)->getCellForCoordinates(position);
                        CollisionCell * optimum = getOptimumCellAroundCell(goalCell);
                        if(optimum != nullptr)
                        {
                            goalCell = optimum;
                        }
                        move->setPosition(goalCell->getCellPosition());
                        move->setNeedsOptimization(false);
                        move->setWaitingForOptimize(false);
                        return false;
                    });
                }
            }
            else
            {
                if(_goalPosition.position != move->getPosition())
                {
                    _owningUnit->crewWeaponNeedsWork(false);
                    setGoalPosition( move->getPosition(), move->shouldIgnoreCover());
                }
                else if(atGoalPosition())
                {
                    if(_goal->hasAssociatedOrder())
                    {
                        _owningUnit->memberCompletedGoal(_goal->getAssociatedOrderId(), false);
                    }
                    _goal->release();
                    _goal = nullptr;
                }
            }
        }
    }
    MovingPhysicalEntity::query(deltaTime);
}

void CrewWeapon::act(float deltaTime)
{
    MovingPhysicalEntity::act(deltaTime);
}

void CrewWeapon::addWork(float deltaTime)
{
    _pendingWork += deltaTime;
}

Vec2 CrewWeapon::getWeaponWorkPosition()
{
    return PhysicalEntity::getPosition() + _workPositions[0]->position;
}

Vec2 CrewWeapon::getWeaponFirePosition()
{
    return _position + _fireOffsets[_facing];
}

void CrewWeapon::setOperator(Infantry * owner)
{
    _workPositionsMutex.lock();
    _owningInfantry = owner;
    _workPositions[0]->owner = owner;
    _workPositionsMutex.unlock();

}

void CrewWeapon::setGoal(Goal * goal)
{
    _pendingGoal = goal;
}

void CrewWeapon::addWorkToWeapon(double deltaTime)
{
    deltaTime /= _crewSize;
    Weapon::addWorkToWeapon(deltaTime);
}

bool CrewWeapon::isWorkComplete()
{
    if(!atGoalPosition())
    {
        return false;
    }
    return true;
}


CollisionCell * CrewWeapon::getOptimumCellAroundCell(CollisionCell * center)
{    
    DIRECTION enemyDirection = _owningUnit->getNearestEnemeyDirection();
    Vec2 enemeyDirectionVector = VectorMath::unitVectorFromFacing(enemyDirection);
    enemeyDirectionVector.x = ceil(enemeyDirectionVector.x);
    enemeyDirectionVector.y = ceil(enemeyDirectionVector.y);
    
    
    CollisionGrid * grid = world->getCollisionGrid(center->getCellPosition());
    CollisionCell * best = nullptr;
    short bestValue = -1;
    int searchRadius = 10;
    
    grid->iterateCellsInCircle(center->getCellPosition(), searchRadius, [this, &best, &bestValue, &grid, enemyDirection, enemeyDirectionVector] (CollisionCell * cell)
                               {
        if(cell->isTraversable() && _owningTeam->tryToClaimPosition(cell, uID()))
        {
            short totalCover = cell->getTerrainCharacteristics().getTotalSecurity();
            if(enemyDirection != NONE)
            {
                CollisionCell * neighbor = grid->getCell(cell->getXIndex() + enemeyDirectionVector.x, cell->getYIndex() + enemeyDirectionVector.y);
                if(neighbor != theInvalidCell)
                {
                    totalCover += neighbor->getTerrainCharacteristics().getSecurityWithoutHeight();
                    //Prefer cover that you can shoot or see around
                    if(grid->canSeeAroundCoverAtPosition(neighbor, enemyDirection))
                    {
                        totalCover += 1; //FIXME: is this really a large enough benefit? How much should we prefer cover you can shoot around
                    }
                }
            }
            
            if(totalCover > bestValue)
            {
                bestValue = totalCover;
                if(best != nullptr)
                {
                    _owningTeam->releasePositionClaim(best);
                }
                best = cell;
            }
            else
            {
                _owningTeam->releasePositionClaim(cell);
            }
        }
        return true;
    });
    
    return best;
}

void CrewWeapon::markForRemoval()
{
    _pendingRemoval = true;
}

void CrewWeapon::removeFromGame()
{
    if(_goal != nullptr)
    {
        _goal->release();
        _goal = nullptr;
    }

    MovingPhysicalEntity::removeFromGame();
}

int CrewWeapon::hasUpdateToSendOverNetwork()
{
    int updates = 0;
    updates += MovingPhysicalEntity::hasUpdateToSendOverNetwork();
    updates += Weapon::checkForNetworkUpdate();
}
void CrewWeapon::saveNetworkUpdate(cereal::BinaryOutputArchive & archive)
{
    MovingPhysicalEntity::saveNetworkUpdate(archive);
    Weapon::saveNetworkDate(archive);
}

void CrewWeapon::updateFromNetwork(cereal::BinaryInputArchive & archive)
{
    MovingPhysicalEntity::updateFromNetwork(archive);
    Weapon::updateFromNetworkData(archive);
}


void CrewWeapon::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Weapon::saveToArchive(archive);
    MovingPhysicalEntity::saveToArchive(archive);
    archive(_crewSize);

    
    if(_goal != nullptr)
    {
        archive(_goal->uID());
        archive(_goal->getGoalType());
        _goal->saveToArchive(archive);
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
    archive(_owningUnit->uID());
    
    if(_pendingGoal.isChanged())
    {
        auto goal = _pendingGoal.get();
        if(goal != nullptr)
        {
            archive(goal->uID());
            archive(goal->getGoalType());
            goal->saveToArchive(archive);
        }
        else
        {
            archive((ENTITY_ID) -2);
        }
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
    
    if(_pendingRemoval.isChanged())
    {
        bool remove = _pendingRemoval;
        archive(remove);
    }
    else
    {
        archive(false);
    }
    
    archive(_pendingWork);
}
 
void CrewWeapon::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Weapon::loadFromArchive(archive);
    MovingPhysicalEntity::loadFromArchive(archive);
    archive(_crewSize);

    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        GOAL_TYPE type;
        archive(type);
        _goal = Goal::createGoalOfType(type);
        _goal->retain();
    }
    else
    {
        _goal = nullptr;
    }
    
    archive(thingId);
    if(thingId != -1)
    {
        _owningUnit = dynamic_cast<CrewWeaponUnit *>(globalEntityManager->getEntity(thingId));
    }
    
    archive(thingId);
    if(thingId == -2)
    {
        _pendingGoal = nullptr;
    }
    else if(thingId != -1)
    {
        GOAL_TYPE type;
        archive(type);
        _pendingGoal = Goal::createGoalOfType(type);
    }

    
    bool pendingRemove;
    archive(pendingRemove);
    if(pendingRemove)
    {
        _pendingRemoval = true;
    }
    archive(_pendingWork);
    
    CrewWeaponDefinition * definition = (CrewWeaponDefinition *) globalEntityFactory->getPhysicalEntityDefinitionWithHashedName(_hashedEntityName);

    _fireOffsets[RIGHT] = definition->_muzzlePositionOffset;
    _fireOffsets[UP_RIGHT] = definition->_muzzlePositionOffset;
    _fireOffsets[UP] = Vec2(definition->_muzzlePositionOffset.x /2, definition->_muzzlePositionOffset.y);
    _fireOffsets[UP_LEFT] = Vec2(0, definition->_muzzlePositionOffset.y);
    _fireOffsets[LEFT] = Vec2(0, definition->_muzzlePositionOffset.y);
    _fireOffsets[DOWN_LEFT] = Vec2(0, 0);
    _fireOffsets[DOWN] = Vec2(definition->_muzzlePositionOffset.x /2, 0);
    _fireOffsets[DOWN_RIGHT] = Vec2(definition->_muzzlePositionOffset.x, 0);

}


void CrewWeapon::populateDebugPanel(GameMenu * menu, Vec2 location)
{
    MovingPhysicalEntity::populateDebugPanel(menu, location);
    std::ostringstream stream;
    std::string text;
    stream << std::fixed;
    stream.precision(0);
        
    text.clear();
    
    if(_goal == nullptr)
    {
        text = "No Goal";
    }
    else
    {
        text = "Goal: ";
        switch(_goal->getGoalType())
        {
            case MOVE_GOAL:
            {
                text += "Move";
                break;
            }
            case ATTACK_POSITION_GOAL: {
                text += "Attack Position";
                break;
            }
        }
    }
    menu->addText(text);
    text.clear();
    
    stream.precision(5);
    stream.str("");
    stream << " Z Pos: " << _sprite->getSprite()->getPositionZ();
    menu->addText(stream.str());
    
    stream.str("");
    stream << " Z Ind: " << _sprite->getSprite()->getZOrder();
    menu->addText(stream.str());
    
    populateDebugPanelForWeapon(menu, location);
}
