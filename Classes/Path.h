//
//  Path.h
//  TrenchWars
//
//  Created by Paul Reed on 1/19/23.
//

#ifndef Path_h
#define Path_h

#include "Refs.h"
#include "List.h"
#include "TerrainCharacteristics.h"
#include "World.h"

USING_NS_CC;


class MapSector;
class Transition;
class NavigationPatch;

enum RouteType
{
    ROUTE_TYPE_POSITION,
    ROUTE_TYPE_SECTOR,
    ROUTE_TYPE_TRANSITION
};

enum PathStatus
{
    PATH_STATUS_READY,
    PATH_STATUS_DONE,
    PATH_STATUS_NEEDS_NEXT_SEGMENT
};

class PathNode : public ComparableRef
{
    ENTITY_ID _neighboringPatchId;
    NavigationPatch * _navigationPatch;
    CollisionCell * _cell;
    bool _addCongestion;
    ENTITY_ID _patchId;
    Vec2  _position;
    int _team;

public:
    
    ~PathNode();
    PathNode(Vec2 position, CollisionCell * cell, int team, bool addCongestion, ENTITY_ID neighborPatchId = -1);
    ENTITY_ID getPatchId() {return _patchId;}
    virtual Vec2 getPosition() {return _position;}
    void overridePosition(Vec2 position) {_position = position;}

    bool pivot;
};

class RouteNode : public ComparableRef
{
protected:
    Vec2  _position;
public:
    
    RouteNode()
    {
    }
    
    RouteNode(Vec2 position)
    {
        _position = position;
    }
    
    virtual RouteType getRouteType() {return ROUTE_TYPE_POSITION;}
    virtual Vec2 getPosition() {return _position;}
};

class SectorRouteNode : public RouteNode
{
    MapSector * _sector;
    int _team;
public:
    ~SectorRouteNode();
    SectorRouteNode(MapSector * sector, int team);
    MapSector * getSector() {return _sector;}
    RouteType getRouteType() {return ROUTE_TYPE_SECTOR;}
};

class TransitionRouteNode : public RouteNode
{
    Transition _transition;
    bool _toUnderground;
public:
    TransitionRouteNode(Transition transition, bool toUnderground);
    RouteType getRouteType() {return ROUTE_TYPE_TRANSITION;}
    Vec2 getTransitionStart();
    Vec2 getTransitionEnd();
};


class Path : public ComparableRef
{
    List<PathNode *> _pathNodes;
    List<RouteNode *> _routeNodes;
    
    RouteNode * _currentRouteNode;

    Vec2 _routeOffset;
    Vec2 _goalPosition;
    int _team;
    bool _debug;
    MOVEMENT_PREFERENCE _movePreference;
    
    bool getPathToSectorRouteNode(Vec2 from, SectorRouteNode * routeNode, Vec2 * modifiedPosition);
    bool getPathToTransitionNode(Vec2 from, TransitionRouteNode * transitionNode);
    
public:
    Path(int team, bool debug, Vec2 routeOffset);
    
    void buildPath(Vec2 start, Vec2 goalPosition, MOVEMENT_PREFERENCE movePreference);
    void setDebug(bool debug) {_debug = debug;}
    
    const List<PathNode *> & getPathNodes() {return _pathNodes;}
    const List<RouteNode *> & getRouteNodes() {return _routeNodes;}
    
    PathNode * getNextPathNode();
    
    PathStatus getPathStatus();
    void buildNextPathSegment(Vec2 currentPosition, Vec2 * modifiedPosition = nullptr);
    Vec2 getNextPathPosition();
    
    void clear();
};

#endif /* Path_h */
