//
//  ParticlesManager.hpp
//  TrenchWars
//
//  Created by Paul Reed on 12/28/22.
//

#ifndef ParticleManager_h
#define ParticleManager_h

#include "cocos2d.h"
#include "PermanentParticleSystem.h"
#include "DynamicParticleSystem.h"
#include "Refs.h"
#include <mutex>

#define Z_POSITION_PARTICLES 2.1


class StaticEntity;

USING_NS_CC;

struct ParticleCreationRequest
{
    std::string filename;
    Vec2 particlePosition;
    ENTITY_ID requestingEntity;
    bool permanentParticle;
    float particleHeight;
    
    ParticleCreationRequest()
    {
    }
    
    ParticleCreationRequest(const std::string & file, Vec2 position, ENTITY_ID requestor,  float particleHeight, bool permanent)
    {
        requestingEntity = requestor;
        filename = file;
        particlePosition = position;
        permanentParticle = permanent;
        this->particleHeight = particleHeight;
    }
};

class ParticleManager
{
    Map<ENTITY_ID ,ParticleSystem *> _particleOwnerToParticleMap;
    std::vector<ParticleCreationRequest> _particleCreationRequests;
    std::vector<ENTITY_ID> _particleRemovalRequests;
    std::mutex _requestsMutex;
    bool _permanentParticlesVisible;
    void removeParticle(ENTITY_ID owner);

public:
    
    ParticleManager();
    
    void update(float deltaTime);
    void setPermanentParticleVisibility(bool visible);
    bool arePermanentParticlesVisible() {return _permanentParticlesVisible;}
    
    void requestParticle(const std::string & filename, Vec2 position, ENTITY_ID requestor = -1, float height = Z_POSITION_PARTICLES, bool permanent = false);
    void requestParticleRemoval(ENTITY_ID ownerId);
    
    ParticleSystem * createParticle(const std::string & filename, Vec2 position, ENTITY_ID requestor, float height, bool permanent, bool dynamic);
};

extern ParticleManager * globalParticleManager;

#endif /* ParticleManager_h */
