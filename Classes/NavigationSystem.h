//
//  NavigationSystem.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/5/22.
//

#ifndef NavigationSystem_h
#define NavigationSystem_h

#include "cocos2d.h"
#include <unordered_set>
#include <mutex>

USING_NS_CC;

class NavigationGrid;
class NavigationPatch;
class NavigationPatchBorder;

class NavigationSystem : public Ref
{
    std::unordered_set<NavigationPatch *> _patchesToInit;
    std::unordered_set<NavigationPatch *> _patchesToGenerateBorders;
    std::unordered_set<NavigationPatch *> _patchesToRemove;
    
    std::mutex _requestsMutex;
    
    
    void findNeighbors();
    void releaseOldPatches();

public:
    NavigationSystem();

    void splitNavigationPatches();
    bool updateNavigationGrids();
    
    void addPatchToInit(NavigationPatch *  patch);
    void addPatchToRemove(NavigationPatch *  patch);
    void addPatchToGenerateBorders(NavigationPatch *  patch);
    
    void removePatchToInit(NavigationPatch *  patch);
    void removePatchToGenerateBorders(NavigationPatch *  patch);
    
    bool getNeedsUpdating();
    void updateNavigationGridDrawing(Vec2 drawNearLocation);
};

extern NavigationSystem * globalNavigationSystem;

#endif /* NavigationSystem_h */
