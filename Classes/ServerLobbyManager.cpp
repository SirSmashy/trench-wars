//
//  ServerLobbyManager.cpp
//  TrenchWars
//
//  Created by Paul Reed on 11/19/23.
//

#include "ServerLobbyManager.h"
#include "SerializationHelpers.h"
#include "LobbyScene.h"
#include "TrenchWarsManager.h"
#include <cereal/types/string.hpp>


void ServerLobbyManager::init()
{
    // Create the lobby scene here
    _lobbyNetworkMode = globalTrenchWarsManager->getNetworkMode();
    
    if(globalTrenchWarsManager->hasLocalPlayer())
    {
        _lobbyScene = LobbyScene::create(this);
        _lobbyScene->showScenarioMenu();
        _lobbyScene->retain();
        Director::getInstance()->replaceScene(TransitionFade::create(0.5, _lobbyScene, Color3B(0,255,255)));
    }
    else
    {
        _lobbyScene = nullptr;
    }

    auto players = globalTrenchWarsManager->getPlayerMap();
    for(auto playerPair : players)
    {
        _playerLobbyStatusMap[playerPair.second->playerId].playerIsReady = false;
        _playerLobbyStatusMap[playerPair.second->playerId].playerNeedsScenario = playerPair.second->remotePlayer;
        _playerLobbyStatusMap[playerPair.second->playerId].remotePlayer = playerPair.second->remotePlayer;
    }
    _dataChanged = true;
}

ServerLobbyManager * ServerLobbyManager::create()
{
    ServerLobbyManager * lobby = new ServerLobbyManager();
    lobby->init();
    lobby->autorelease();
    return lobby;
}

void ServerLobbyManager::update(float deltaTime)
{
    if(_lobbyNetworkMode == NETWORK_MODE_SERVER)
    {
        globalNetworkInterface->update();
        globalNetworkInterface->drainReceivedQueue();
    }
   
    if(!_dataChanged)
    {
        return;
    }
    
    if(_lobbyNetworkMode == NETWORK_MODE_SERVER)
    {
        sendLobbyStatusToClients();
    }

    if(globalTrenchWarsManager->hasLocalPlayer())
    {
        _lobbyScene->updateCommandsMenu();
    }
    
    bool canStart = true;
    for(auto & playerPair : _playerLobbyStatusMap)
    {
        if(playerPair.second.playerIsReady == false || playerPair.second.playerNeedsScenario == true)
        {
            canStart = false;
        }
    }
    
    if(canStart)
    {
        startGame();
    }
    
    _dataChanged = false;
}


void ServerLobbyManager::sendLobbyStatusToClients()
{
    // Generate lobby status message
    MessageBuffer * buffer = globalNetworkInterface->prepareNetworkPackage(MESSAGE_LOBBY_INFO);
    cereal::BinaryOutputArchive & archive = buffer->getArchive();
    auto playerMap = globalTrenchWarsManager->getPlayerMap();
    archive(playerMap.size());
    for(auto & playerPair : playerMap)
    {
        archive(playerPair.second->playerId);
        archive(playerPair.second->playerName);
        archive(playerPair.second->commandId);
        archive(playerPair.second->teamId);
        archive((int)_playerLobbyStatusMap[playerPair.first].playerIsReady);
    }
    
    archive(_teamIdToTeamDetailsMap.size());
    for(auto & teamPair : _teamIdToTeamDetailsMap)
    {
        archive(teamPair.first);
        archive(teamPair.second.commandIdToCommandDetailsMap.size());
        for(auto & commandPair : teamPair.second.commandIdToCommandDetailsMap)
        {
            archive(commandPair.first);
            archive(commandPair.second.playerId);
            archive(commandPair.second.spawnPoint);
        }
    }
    
    if(_selectedScenario != nullptr)
    {
        archive(_selectedScenario->scenarioName);
    }
    else
    {
        archive(std::string("No Scenario Selected"));
    }
    
    bool needToSendPlayerScenarioMap = false;
    
    std::list<int> playersToSend;
    // Send each player the lobby status and check to see if at least one player needs the scenario map
    for(auto playerPair : _playerLobbyStatusMap)
    {
        if(playerPair.second.remotePlayer)
        {
            playersToSend.push_back(playerPair.first);
            if(playerPair.second.playerNeedsScenario)
            {
                needToSendPlayerScenarioMap = true;
            }
        }
    }
    globalNetworkInterface->sendPreparedDataToPlayers(buffer, playersToSend);

    
    
    ////// Now send the scenario map if needed
    // at least one player needs the scenario map, generate the message and send it to everyone who needs it
    if(needToSendPlayerScenarioMap && _selectedScenario != nullptr)
    {
        MessageBuffer * mapBuffer = globalNetworkInterface->prepareNetworkPackage(MESSAGE_SCENARIO_MAP);
        cereal::BinaryOutputArchive & archive = mapBuffer->getArchive();
        
        auto scenarioFilePath = FileUtils::getInstance()->fullPathForFilename(_selectedScenario->terrainName);
        Data data = FileUtils::getInstance()->getDataFromFile(scenarioFilePath);
        
        // Archive image file and size
        archive(data.getSize());
        for(int i = 0; i < data.getSize(); i++)
        {
            archive(data.getBytes()[i]);
        }
        
        playersToSend.clear();
        for(auto & playerPair : _playerLobbyStatusMap)
        {
            if(playerPair.second.remotePlayer &&  playerPair.second.playerNeedsScenario)
            {
                playersToSend.push_back(playerPair.first);
                playerPair.second.playerNeedsScenario = false;
            }
        }
        globalNetworkInterface->sendPreparedDataToPlayers(mapBuffer, playersToSend);

    }
}


void ServerLobbyManager::startGame()
{
    if(_lobbyNetworkMode == NETWORK_MODE_SERVER)
    {
        MessageBuffer * buffer = globalNetworkInterface->prepareNetworkPackage(MESSAGE_GAME_START);
        cereal::BinaryOutputArchive & archive = buffer->getArchive();
        archive(_selectedScenario->teams.size());
        for(auto team : _selectedScenario->teams)
        {
            archive(team->teamId);
            archive(team->commands.size());
            auto & teamDetails = _teamIdToTeamDetailsMap.at(team->teamId);
            
            for(auto command : team->commands)
            {
                command->controllingPlayerId = teamDetails.commandIdToCommandDetailsMap.at(command->commandId).playerId;
                command->aiCommand = teamDetails.commandIdToCommandDetailsMap.at(command->commandId).playerId == -1;
                
                archive(command->commandId);
                archive(command->controllingPlayerId);
                archive(command->spawnPoint);
            }
        }
        
        std::list<int> playersToSend;
        for(auto playerPair : _playerLobbyStatusMap)
        {
            if(playerPair.second.remotePlayer)
            {
                playersToSend.push_back(playerPair.first);
            }
        }
        globalNetworkInterface->sendPreparedDataToPlayers(buffer, playersToSend);
    }
    if(globalNetworkInterface != nullptr)
    {
        // Make sure we send any queued messages before we go and load the scenario
        globalNetworkInterface->update();
    }

    globalTrenchWarsManager->startEngagementFromScenario(_selectedScenario);
}


void ServerLobbyManager::parseNetworkMessage(GameMessage * message)
{
    unsigned int updateBits;
    auto & archive = message->getArchive();

    
    archive(updateBits);
    
    if( (updateBits & PLAYER_MESSAGE_COMMAND_SELECT) )
    {
        int commandId, teamId;
        archive(teamId);
        archive(commandId);
        playerSelectedCommand(teamId, message->getPlayerId(), commandId);
    }
    
    if( (updateBits & PLAYER_MESSAGE_NAME))
    {
        std::string name;
        archive(name);
        globalTrenchWarsManager->playerSetName(message->getPlayerId(), name);
    }
    
    if( (updateBits & PLAYER_MESSAGE_READY_TO_START))
    {
        int ready;
        archive(ready);
        _playerLobbyStatusMap[message->getPlayerId()].playerIsReady = ready;
        _dataChanged = true;
    }
    
    // Shouldn't ever get these
    if( (updateBits & PLAYER_MESSAGE_ORDER))
    {
    }
    if( (updateBits & PLAYER_MESSAGE_PLAN))
    {
    }
    if( (updateBits & PLAYER_MESSAGE_OBJECTIVE))
    {
    }
}

void ServerLobbyManager::playerSetName(int playerId, const std::string & name)
{
    globalTrenchWarsManager->playerSetName(playerId, name);
    _dataChanged = true;
}

void ServerLobbyManager::playerSelectedCommand(int teamId, int playerId, int commandId)
{
    Player * player = globalTrenchWarsManager->getPlayerForPlayerId(playerId);
    if(player == nullptr)
    {
        LOG_ERROR("Error! Player %d does not exist \n", playerId);
    }
    
    if(_teamIdToTeamDetailsMap.find(teamId) == _teamIdToTeamDetailsMap.end())
    {
        LOG_ERROR("Error! Team Id %d does not exist \n", teamId);
        return;
    }
    auto & newTeam = _teamIdToTeamDetailsMap.at(teamId);
    if(newTeam.commandIdToCommandDetailsMap.find(commandId) == newTeam.commandIdToCommandDetailsMap.end())
    {
        LOG_ERROR("Error! Command Id %d does not exist \n", commandId);
        return;
    }
    else if(newTeam.commandIdToCommandDetailsMap.at(commandId).playerId != -1)
    {
        LOG_ERROR("Error! Command %d already selected by plyaer %d \n", commandId, newTeam.commandIdToCommandDetailsMap.at(commandId).playerId);
        return;
    }
    
    // Set the player's old command to NO player
    if(_teamIdToTeamDetailsMap.find(player->teamId) != _teamIdToTeamDetailsMap.end())
    {
        auto & oldTeam = _teamIdToTeamDetailsMap.at(player->teamId);
        if(oldTeam.commandIdToCommandDetailsMap.find(player->commandId) != oldTeam.commandIdToCommandDetailsMap.end())
        {
            oldTeam.commandIdToCommandDetailsMap[player->commandId].playerId = -1; //NO Player
        }
    }
    
    newTeam.commandIdToCommandDetailsMap[commandId].playerId = playerId;
    newTeam.commandIdToCommandDetailsMap[commandId].teamId = teamId;

    player->commandId = commandId;
    player->teamId = teamId;
    
//    if(_lobbyNetworkMode == NETWORK_MODE_SINGLE)
//    {
//        _playerLobbyStatusMap[playerId].playerIsReady = true;
//    }
    _dataChanged = true;
}


void ServerLobbyManager::playerSetScenario(const std::string & scenarioName)
{
    _selectedScenario = globalScenarioFactory->getScenario(scenarioName);
    _teamIdToTeamDetailsMap.clear();
    for(auto team : _selectedScenario->teams)
    {
        _teamIdToTeamDetailsMap.try_emplace(team->teamId);
        auto & teamDetails = _teamIdToTeamDetailsMap.at(team->teamId);
        for(auto commandDef : team->commands)
        {
            teamDetails.commandIdToCommandDetailsMap.try_emplace(commandDef->commandId);
            teamDetails.commandIdToCommandDetailsMap[commandDef->commandId].commandId = commandDef->commandId;
            teamDetails.commandIdToCommandDetailsMap[commandDef->commandId].spawnPoint = commandDef->spawnPoint;
            teamDetails.commandIdToCommandDetailsMap[commandDef->commandId].teamId = team->teamId;
            teamDetails.commandIdToCommandDetailsMap[commandDef->commandId].playerId = -1;
        }
    }
    for(auto playerPair : globalTrenchWarsManager->getPlayerMap())
    {
        playerPair.second->commandId = -1;
        playerPair.second->teamId = -1;
    }
    for(auto & playerPair : _playerLobbyStatusMap)
    {
        playerPair.second.playerIsReady = false;
        if(playerPair.second.remotePlayer)
        {
            playerPair.second.playerNeedsScenario = true;
        }
    }
    _dataChanged = true;
    
    if(globalTrenchWarsManager->hasLocalPlayer())
    {
        auto fullPath = FileUtils::getInstance()->fullPathForFilename(_selectedScenario->terrainName);
        Data data = FileUtils::getInstance()->getDataFromFile(fullPath);
        _lobbyScene->setScenarioImage(data.getBytes(), data.getSize());
        _lobbyScene->scenarioSelected(_selectedScenario->scenarioName);
    }
}


void ServerLobbyManager::playerJoinedGame(Player * player)
{
    _playerLobbyStatusMap.try_emplace(player->playerId);
    _playerLobbyStatusMap[player->playerId].remotePlayer = player->remotePlayer;
    _dataChanged = true;
}

void ServerLobbyManager::playerLeftGame(int playerId)
{
    _playerLobbyStatusMap.erase(playerId);
    for(auto & teamPair : _teamIdToTeamDetailsMap)
    {
        for(auto & commandPair : teamPair.second.commandIdToCommandDetailsMap)
        {
            if(commandPair.second.playerId == playerId)
            {
                commandPair.second.playerId = -1;
                break;
            }

        }
    }
    _dataChanged = true;
}


bool ServerLobbyManager::isPlayerReady(int playerId)
{
    if(_playerLobbyStatusMap.find(playerId) == _playerLobbyStatusMap.end())
    {
        return false;
    }
    return _playerLobbyStatusMap[playerId].playerIsReady;
}


void ServerLobbyManager::playerSelectedStartGame(int playerId)
{
    _playerLobbyStatusMap[playerId].playerIsReady = !_playerLobbyStatusMap[playerId].playerIsReady;
    _dataChanged = true;
}

void ServerLobbyManager::shutdown()
{
    Director * director = Director::getInstance();
    Scheduler * scheduler = director->getScheduler();
    scheduler->unschedule("lobby", this);
    _lobbyScene->release();
}
