//
//  TrenchWarsManager.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/12/23.
//
#ifndef TrenchWarsManager_h
#define TrenchWarsManager_h

#include "cocos2d.h"
#include "NetworkInterface.h"
#include "GameStateManager.h"
#include "ScenarioFactory.h"
#include <cereal/archives/binary.hpp>

class EngagementManager;

enum PLAYER_STATE
{
    PLAYER_STATE_NEEDS_GAME_STATE,
    PLAYER_STATE_LOADING_GAME_STATE,
    PLAYER_STATE_READY,
    PLAYER_STATE_DELAYED, // Haven't received an update in awhile I guess?
    PLAYER_STATE_ERROR,
    PLAYER_STATE_GONE
};

struct Player
{
    int playerId;
    int commandId = -1;
    int teamId = -1;
    std::string playerName;
    PLAYER_STATE playerState = PLAYER_STATE_NEEDS_GAME_STATE;
    bool remotePlayer = false;
};

/** TrenchWarsManager
 * Purpose: This class is a top level manager for non-UI (i.e., cocos2d) aspects of the game
 * It controls creation/loading of Engagements, networking, etc.
 */
class TrenchWarsManager
{
private:
    NETWORK_MODE _currentNetworkMode;
    GAME_STATE _gameState;
    GameStateManager * _activeStateManager;
    int _nextPlayerId;
    bool _hasLocalPlayer;
    
    std::unordered_map<int, Player *> _playerIdToPlayerMap;
    std::unordered_map<int, Player *> _clientIdToPlayerMap;

    void shutdownNetwork();
    void setNetworkMode(NETWORK_MODE mode);
    void handlePlayerStateChange(int playerId);

public:
    TrenchWarsManager(bool hasLocalPlayer);
    
    void joinServer(const std::string & ipAddr);
    void startLobby(NETWORK_MODE networkMode);
    void exitToMainMenu();
    
    int remotePlayerJoinedGame(int playerId = -1);
    void playerLeftGame(int playerId);
    void playerSetName(int playerId, const std::string & name);
    void setLocalPlayerId(int playerId);

    void receivedNetworkMessage(GameMessage * message);
    void startEngagementFromScenario(ScenarioDefinition * scenario);
    void startEngagementAsClient(const unsigned char * mapData, size_t mapDataSize, Vector<TeamDefinition *> & teams);

    void startEngagementFromSaveGame(const std::string & saveFile);
    
    bool hasLocalPlayer() {return _hasLocalPlayer;}
    Player * getPlayerForPlayerId(int playerId);
    Player * getLocalPlayer();
    int getLocalPlayerId();
    const std::unordered_map<int, Player *> & getPlayerMap() {return _playerIdToPlayerMap;}
    
    void update(float deltaTime);
    
    NETWORK_MODE getNetworkMode() {return _currentNetworkMode;}
};

extern TrenchWarsManager * globalTrenchWarsManager;
#endif /* TrenchWarsManager_h */
