//
//  UnitEmblem.h
//  TrenchWars
//
//  Created by Paul Reed on 12/8/22.
//

#ifndef UnitEmblem_h
#define UnitEmblem_h

#include "Emblem.h"
#include "Objective.h"
#include "CountTo.h"
class UnitEmblem : public Emblem
{
private:
    Unit * _unit;
    Objective * _objectiveDraggedOver;
    virtual bool init(Unit * ent, EMBLEM_TYPE type, Vec2 position);
    
    CountTo _circleMenuCountdown;
    bool _showCircleMenuAfterCount;
    
    UnitEmblem();
    
public:
    static UnitEmblem * create(Unit * ent, EMBLEM_TYPE type, Vec2 position);
    
    virtual void update(float deltaTime);
    virtual void setEmblemType(EMBLEM_TYPE type);


    virtual void emblemDragged(Vec2 position);
    virtual void emblemDragEnded(Vec2 position);
    
    virtual void emblemHoverEnter(Vec2 position);
    virtual void emblemHoverLeave(Vec2 position);
};
#endif /* UnitEmblem_h */


