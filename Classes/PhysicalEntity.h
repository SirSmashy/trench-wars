#ifndef __PHYSICAL_ENTITY_H__
#define __PHYSICAL_ENTITY_H__

//
//  Entity.h
//  TrenchWars
//
//  Created by Paul Reed on 1/29/12.
//  Copyright (c) 2012. All rights reserved.
//

#include "cocos2d.h"
#include "Entity.h"
#include "Timing.h"
#include "CountTo.h"
#include "AnimatedSprite.h"
#include "TerrainCharacteristics.h"
#include "EntityDefinitions.h"
#include "AnimatedSpriteProxy.h"
#include "VectorMath.h"

USING_NS_CC;

////////
#define MELEE_RANGE 20

static double experienceMultiplier [5] = {1.0, 0.85 , 0.7 , 0.55 , 0.4};

struct WorkPosition
{
    Vec2 position;
    Entity * owner;
    
    WorkPosition(Vec2 offset, Entity * ent)
    {
        position = offset;
        owner = ent;
    }
};

class Command;
class Team;
class CollisionCell;
class CollisionGrid;
class NavigationGrid;
class World;

/*
 * An entity with a physical (i.e., graphics) representation in the game world. An AnimatedSprite is used to display the entity.
 */
class PhysicalEntity : public Entity
{
protected:
    size_t _hashedEntityName;
    
    Vec2 _position;
    DIRECTION _facing;
    Rect _collisionBounds;
    float _collisionScale;
    TerrainCharacteristics _inTerrain;
    std::vector<WorkPosition *> _workPositions;
        
    std::mutex _workPositionsMutex;

    Label * testText;
    AnimatedSpriteProxy * _sprite;

    bool _debugging;
    bool _alwaysUpdateSprite;
    
    void loadFromDefinition(PhysicalEntityDefinition * definition);

CC_CONSTRUCTOR_ACCESS :
    
    PhysicalEntity();
    ~PhysicalEntity();
    
    virtual bool init(PhysicalEntityDefinition * definition, Vec2 position, bool alwaysUpdateSprite);
        
public:
    virtual void postLoad();
    std::unordered_map<ENTITY_ID, CollisionCell *> _collisionCellMembership;
    CollisionCell * _primaryCollisionCell;
    virtual int getFreeWorkPositions();
    virtual Vec2 claimWorkPosition(PhysicalEntity * ent);
    virtual void releaseWorkPosition(PhysicalEntity * ent);
    
    AnimatedSpriteProxy * getSprite() {return _sprite;}
    virtual size_t getHashedDefinitionName() {return _hashedEntityName;}

    // Work related
    virtual void addWork(float deltaTime);
    virtual bool isWorkComplete();

    // Terrain characteristics
    unsigned char getConcealmentValue(char height)
    {
        return _inTerrain.getConcealmentValue() + _inTerrain.getHeightChanceToHit(height);
    }    
    
    // Terrain characteristics
    unsigned char getConcealmentValueWithoutHeight()
    {
        return _inTerrain.getConcealmentValue();
    }
    
    unsigned short getTotalSecurity() {return _inTerrain.getTotalSecurity();}

    short getTerrainHeight() {return _inTerrain.getHeight();}
    const TerrainCharacteristics & getTerrainCharacteristics() {return _inTerrain;}
    
    // physical characteristics
    inline Vec2 getPosition()  {return _position;}
    virtual void setPosition(Vec2 position);
    CollisionGrid * getCurrentCollisionGrid();
    DIRECTION getFacing() {return _facing;}
    virtual Vec2 getVelocity() {return Vec2::ZERO;}
    virtual Vec2 getAverageVelocity() {return Vec2::ZERO;}
    virtual Rect & getCollisionBounds() {return _collisionBounds;}
    
    // Collision
    virtual bool testCollisionWithBounds(Rect otherBounds);
    virtual bool testCollisionWithLine(Vec2 start, Vec2 end);
    virtual bool testCollisionWithPoint(Vec2 point);
    virtual bool testCollisionWithCircle(Vec2 center, float radius);
    
    virtual void receiveDamage(int damage) {};
    
    // Debug and Test
    virtual void displayTestInfo();
    virtual void hideTestInfo();
    virtual void updateTestInfo();
    virtual void setDebugMe() {_debugging = true;}
    virtual void clearDebugMe() {_debugging = false;}
    virtual bool isDebugMe() {return _debugging;}

    
    virtual void removeFromGame();
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
    virtual void populateDebugPanel(GameMenu * menu, Vec2 location);
};


typedef std::unordered_set<PhysicalEntity *, ComparableRefPointerHash, ComparableRefPointerCompare> PhysicalEntitySet;

#endif // __PHYSICAL_ENTITY_H__
