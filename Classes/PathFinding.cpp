//
//  PathFinding.cpp
//  TrenchWars
//
//  Created by Paul Reed on 12/26/19.
//

#include "PathFinding.h"
#include "VectorMath.h"
#include "EngagementManager.h"
#include "PrimativeDrawer.h"
#include "Timing.h"
#include "CollisionGrid.h"
#include "PathBuildingNode.h"
#include "Path.h"
#include "TestBed.h"
#include "GameStatistics.h"
#include "RedBlackTree.h"

//#define SHOW_PATH_DEBUG 1

PathFinding * globalPathFinding;

PathFinding::PathFinding()
{
    globalPathFinding = this;
    pathDebugFront = nullptr;
    pathDebugEnd = nullptr;
}

/* Makes use of a standard A* search, but with modifications to the movement cost and movement heuristic calculations
 * 
 *
 */
double PathFinding::getPath(Vec2 start, Vec2 end, List<PathNode *> * pathList, MOVEMENT_PREFERENCE movePreference, int team,  bool debug)
{
    int patchesTouched = 0;
    int patchesVisited = 0;
        
//    MicroSecondSpeedTimer timer( [&patchesVisited, &patchesTouched] (long long int time) {
//        LOG_DEBUG("    Path Time: %f V: %d T: %d \n", time/1000.0, patchesVisited, patchesTouched);
//    });

#ifdef SHOW_PATH_DEBUG
    if(debug)
    {
        clearPathDebug();
        pathDebug.clear();
    }
#endif
    
    CollisionCell * goalCell = world->getCollisionGrid(end)->getCellForCoordinates(end);
    CollisionCell * startCell = world->getCollisionGrid(start)->getCellForCoordinates(start);
    
    if(goalCell->getTerrainCharacteristics().getImpassable() ||
       startCell->getTerrainCharacteristics().getImpassable())
    {
        return -1;
    }
    
    NavigationPatch * goalPatch = goalCell->getNavigationPatch();
    NavigationPatch * startPatch = startCell->getNavigationPatch();
    
    //Same patch, just walk directly to the goal
    if(goalPatch == startPatch)
    {
        PathNode * node = new PathNode(end, goalCell, team, true);
        pathList->pushFront(node);
        node->release();
        return 1;
    }

    RedBlackTree pathsToExamineTree;
    Map<ENTITY_ID, PatchPathBuildingNode *>  shortestDistancePathsMap;
    Map<ENTITY_ID, PatchPathBuildingNode *>  towardsGoalPathsMap;
        
    PatchPathBuildingNode * initialPathBuilder = new PatchPathBuildingNode(startCell, startPatch, goalCell, team);
    PatchPathBuildingNode * currentPathBuilder = initialPathBuilder;
    PatchPathBuildingNode * examinePathBuilder;
    
    //determine the best path to follow by moving toward the goal node
    while(currentPathBuilder->getRemainingCostHeuristic() != 0)
    {
        patchesVisited++;
        currentPathBuilder->setExamined(true);
        
        if(currentPathBuilder->entranceToThisPatch->getXIndex() == 867 && currentPathBuilder->entranceToThisPatch->getYIndex() == 553)
        {
            debug = debug;
        }
        
#ifdef SHOW_PATH_DEBUG
        if(debug)
        {
            pathDebug.insert(currentPathBuilder->uID(), currentPathBuilder);
            createPathDebugRecord(currentPathBuilder, false, true, movePreference);
        }
#endif
        // Examine all the paths that go from the current patch to another patch
        for(auto entry : currentPathBuilder->getNavigationPatch()->getNeighbors())
        {
            PatchPathBuildingNode * parent = (PatchPathBuildingNode *) currentPathBuilder->getParent();
            if(!entry.second->_neighboringPatch->isTraversable() ||
               (parent != nullptr && entry.second->_neighboringPatch->uID() == parent->getNavigationPatch()->uID()))
            {
                continue;
            }
            
            /////////////
            /// Node for the shortest path to this patch
            examinePathBuilder = shortestDistancePathsMap.at(entry.second->_neighboringPatch->uID());
            if(examinePathBuilder == nullptr)
            {
                examinePathBuilder = new PatchPathBuildingNode(currentPathBuilder, entry.second->_neighboringPatch,
                                                                goalCell, MINIMUM_PATH, team);
                shortestDistancePathsMap.insert(entry.second->_neighboringPatch->uID(), examinePathBuilder);
                examinePathBuilder->release();
            }
            
            if(examinePathBuilder->isExamined())
            {
                continue;
            }
            
#ifdef SHOW_PATH_DEBUG
            if(debug)
            {
                createPathDebugRecord(examinePathBuilder, true, !examinePathBuilder->isExamined(), movePreference);
                pathDebug.insert(examinePathBuilder->uID(), examinePathBuilder);
            }
#endif

            if(examinePathBuilder->update(currentPathBuilder, movePreference))
            {
                pathsToExamineTree.addObject(examinePathBuilder, examinePathBuilder->getEstimatedTotalCost());
            }
            patchesTouched++;
            
            ////////////
            /// Node for the most direct route to the goal
            examinePathBuilder = towardsGoalPathsMap.at(entry.second->_neighboringPatch->uID());
            if(examinePathBuilder == nullptr)
            {
                examinePathBuilder = new PatchPathBuildingNode(currentPathBuilder, entry.second->_neighboringPatch,
                                                                goalCell, TOWARD_GOAL_PATH, team);
                towardsGoalPathsMap.insert(entry.second->_neighboringPatch->uID(), examinePathBuilder);
                examinePathBuilder->release();
            }
            
            if(examinePathBuilder->isExamined())
            {
                continue;
            }
            
#ifdef SHOW_PATH_DEBUG
            if(debug)
            {
                createPathDebugRecord(examinePathBuilder, true, !examinePathBuilder->isExamined() , movePreference);
                pathDebug.insert(examinePathBuilder->uID(), examinePathBuilder);
            }
#endif
            
            if(examinePathBuilder->update(currentPathBuilder, movePreference))
            {
                pathsToExamineTree.addObject(examinePathBuilder, examinePathBuilder->getEstimatedTotalCost());
            }
            patchesTouched++;
            
            if(currentPathBuilder->getNavigationPatch() == goalPatch)
            {
                examinePathBuilder = shortestDistancePathsMap.at(goalCell->uID());
                if(examinePathBuilder == nullptr)
                {
                    examinePathBuilder = new GoalCellPathBuildingNode(currentPathBuilder, goalPatch,
                                                                   goalCell, team);
                    shortestDistancePathsMap.insert(goalCell->uID(), examinePathBuilder);
                    examinePathBuilder->release();
                }
                
#ifdef SHOW_PATH_DEBUG
                if(debug)
                {
                    createPathDebugRecord(examinePathBuilder, true, !examinePathBuilder->isExamined(), movePreference);
                    pathDebug.insert(examinePathBuilder->uID(), examinePathBuilder);
                }
#endif
                
                if(examinePathBuilder->update(currentPathBuilder, movePreference))
                {
                    pathsToExamineTree.addObject(examinePathBuilder, examinePathBuilder->getEstimatedTotalCost());
                }
                patchesTouched++;
            }
        }
        
        //choose the cell with the lowest heuristic as the next cell
        currentPathBuilder =  (PatchPathBuildingNode *) pathsToExamineTree.findAndRemoveMinimum();
        if(patchesVisited > 40000 || currentPathBuilder == nullptr)
        {
            LOG_ERROR("Invalid path %d: S: %d,%d E: %d,%d \n", patchesVisited, startCell->getXIndex(), startCell->getYIndex(), goalCell->getXIndex(), goalCell->getYIndex());
            initialPathBuilder->release();
            return -1;
        }
    }
#ifdef SHOW_PATH_DEBUG
    if(debug)
    {
        printPathDebug(startCell,goalCell);
    }
#endif
    
//    LOG_DEBUG("Patch Visited %d touched %d \n", patchesVisited,  patchesTouched);
    smoothPath(currentPathBuilder, goalCell);
    
    //Build the path node list
    PathNode * node;
    ENTITY_ID parentPatchId = -1;
    while(currentPathBuilder->getParent() != nullptr)
    {
        currentPathBuilder->usedInPath = true;
        parentPatchId = ((PatchPathBuildingNode *) currentPathBuilder->getParent())->getNavigationPatch()->uID();
        node = new PathNode(currentPathBuilder->entranceToThisPatch->getCellPosition(), currentPathBuilder->entranceToThisPatch, team, true, parentPatchId);
        node->pivot = currentPathBuilder->pivot;
        pathList->pushFront(node);
        node->release();
        

        
        if(currentPathBuilder->useExitCellInPath)
        {
            node = new PathNode(currentPathBuilder->exitFromPreviousPatch->getCellPosition(), currentPathBuilder->exitFromPreviousPatch, team, false);
            pathList->pushFront(node);
            node->pivot = currentPathBuilder->pivot;
            node->release();
        }
        currentPathBuilder = (PatchPathBuildingNode *) currentPathBuilder->getParent();
    }
    
    if(pathList->size() > 0)
    {
        pathList->back()->overridePosition(end);
    }
    
    initialPathBuilder->release();
    return currentPathBuilder->getCostToTravelHere();
}

void PathFinding::smoothPath(PatchPathBuildingNode * pathTracker, CollisionCell * goalCell)
{
    //Step 1: Walk through the node list, looking for unnecessary 'exit' nodes or 'pivot' nodes
    // Nodes are an exit node if there is a previous node that belongs to the same patch (known as the entrance node)
    // Exit nodes are unnecessary if the angle between the lines to and from the node is close to 180 (i.e., the two lines are nearly parallel)
    // Pivot nodes are nodes where the angle between the lines to and from the node is less than 135 (i.e., the two lines are closer to perpendicular)
    PatchPathBuildingNode * previousNode = pathTracker;
    PatchPathBuildingNode * node = (PatchPathBuildingNode *) previousNode->getParent();
    PatchPathBuildingNode * nextNode = (PatchPathBuildingNode *) node->getParent();
    float turnAngle = M_PI - M_PI_4; //Any angle smaller than this is considered a turn
    
    Vector<PatchPathBuildingNode *> pivotPoints;
    pivotPoints.pushBack(pathTracker);
    pathTracker->pivot = true;
    
    while(nextNode != nullptr)
    {
        if(node->entranceToThisPatch->getTerrainCharacteristics() == node->exitFromPreviousPatch->getTerrainCharacteristics() &&
           node->_crossedBorder != nullptr &&
           node->_crossedBorder->lineCrossesBorder(nextNode->entranceToThisPatch, previousNode->exitFromPreviousPatch))
        {
            // Non pivot
            node->useExitCellInPath = false;
        }
        else
        {
            node->pivot = true;
            pivotPoints.pushBack(node);
        }
        
        previousNode = node;
        node = nextNode;
        nextNode = (PatchPathBuildingNode *) nextNode->getParent();
    }
    
    // Start point is always a pivot point
    pivotPoints.pushBack(node);
    
    
    // Initial pivot point is the goal cell, which doesn't have an associated path tracker
    CollisionCell * firstPivotPoint = goalCell;
    CollisionCell * secondPivotPoint;
    PatchPathBuildingNode * segmentNode = pathTracker;
    
    for(int i = 0; i < pivotPoints.size(); i++)
    {
        secondPivotPoint = pivotPoints.at(i)->entranceToThisPatch;
        while(segmentNode != pivotPoints.at(i))
        {
            if(segmentNode->_crossedBorder != nullptr)
            {
                segmentNode->entranceToThisPatch = segmentNode->_crossedBorder->getBorderCellNearestLine(firstPivotPoint, secondPivotPoint);
                segmentNode->exitFromPreviousPatch = segmentNode->_crossedBorder->getOppositeBorder()->getBorderCellNearestLine(firstPivotPoint, secondPivotPoint);
            }
            segmentNode = (PatchPathBuildingNode *) segmentNode->getParent();
        }
        firstPivotPoint = pivotPoints.at(i)->entranceToThisPatch;
    }
}

double PathFinding::getPathCostWithinInBounds(CollisionCell * startCell, CollisionCell * goalCell, const NavigableRect & bounds, MOVEMENT_PREFERENCE movePreference)
{
    int patchesTouched = 0;
    int patchesVisited = 0;
    
    if(goalCell->getTerrainCharacteristics().getImpassable() ||
       startCell->getTerrainCharacteristics().getImpassable())
    {
        return -1;
    }

    NavigationPatch * goalPatch = goalCell->getNavigationPatch();
    NavigationPatch * startPatch = startCell->getNavigationPatch();
    
    //Same patch, just walk directly to the goal
    if(goalPatch == startPatch)
    {
        return goalCell->getVectorToOtherCell(startCell).length() * startPatch->getMovementCost(movePreference);
    }
    
    RedBlackTree pathsToExamineTree;
    Map<ENTITY_ID, PatchPathBuildingNode *>  shortestDistancePathsMap;
    Map<ENTITY_ID, PatchPathBuildingNode *>  towardsGoalPathsMap;
    
    PatchPathBuildingNode * initialPathBuilder = new PatchPathBuildingNode(startCell, startPatch, goalCell);
    PatchPathBuildingNode * currentPathBuilder = initialPathBuilder;
    PatchPathBuildingNode * examinePathBuilder;
    
    //determine the best path to follow by moving toward the goal node
    while(currentPathBuilder->getNavigationPatch() != goalPatch)
    {
        patchesVisited++;
        currentPathBuilder->setExamined(true);

        // Examine all the paths that go from the current patch to another patch
        for(auto entry : currentPathBuilder->getNavigationPatch()->getNeighbors())
        {
            NavigationPatch * nieghbor = entry.second->_neighboringPatch;
            PatchPathBuildingNode * parent = (PatchPathBuildingNode *) currentPathBuilder->getParent();
            if(!nieghbor->isTraversable() ||
               !nieghbor->_patchRect.intersectsRect(bounds) ||
               // Don't look backwards
               (parent != nullptr && nieghbor->uID() == parent->getNavigationPatch()->uID()))
            {
                continue;
            }
            
            /////////////
            /// Node for the shortest path to this patch
            examinePathBuilder = shortestDistancePathsMap.at(nieghbor->uID());
            if(examinePathBuilder == nullptr)
            {
                examinePathBuilder = new PatchPathBuildingNode(currentPathBuilder, nieghbor,goalCell, MINIMUM_PATH);
                shortestDistancePathsMap.insert(nieghbor->uID(), examinePathBuilder);
                examinePathBuilder->release();
            }
            
            if(examinePathBuilder->isExamined())
            {
                continue;
            }
            
            if(examinePathBuilder->update(currentPathBuilder, movePreference))
            {
                pathsToExamineTree.addObject(examinePathBuilder, examinePathBuilder->getEstimatedTotalCost());
            }
            patchesTouched++;
            
            ////////////
            /// Node for the most direct route to the goal
            examinePathBuilder = towardsGoalPathsMap.at(nieghbor->uID());
            if(examinePathBuilder == nullptr)
            {
                examinePathBuilder = new PatchPathBuildingNode(currentPathBuilder, nieghbor,goalCell, TOWARD_GOAL_PATH);
                towardsGoalPathsMap.insert(nieghbor->uID(), examinePathBuilder);
                examinePathBuilder->release();
            }
            
            if(examinePathBuilder->isExamined())
            {
                continue;
            }
            
            if(examinePathBuilder->update(currentPathBuilder, movePreference))
            {
                pathsToExamineTree.addObject(examinePathBuilder, examinePathBuilder->getEstimatedTotalCost());
            }
            patchesTouched++;
        }
        
        //choose the cell with the lowest heuristic as the next cell
        currentPathBuilder =  (PatchPathBuildingNode *) pathsToExamineTree.findAndRemoveMinimum();
        if(patchesVisited > 1000 || currentPathBuilder == nullptr)
        {
            initialPathBuilder->release();
            return -1;
        }
    }
    
    initialPathBuilder->release();
    return currentPathBuilder->getEstimatedTotalCost();
}

MapSector * PathFinding::getMapSectorPathNodeNearCell(CollisionCell * cell)
{
    MapSector * sector = globalSectorGrid->sectorForCell(cell);
    if(sector == nullptr)
    {
        return nullptr;
    }
    if(sector->getPathingCell() != nullptr)
    {
        double cost = getPathCostWithinInBounds(sector->getPathingCell(), cell, sector->getCellsRect(), MOVE_STRONGLY_PREFER_COVER);
        if(cost != -1)
        {
            return sector;
        }
    }
    int xIndex = sector->getXIndex();
    int yIndex = sector->getYIndex();
    
    //Try right sector
    sector = globalSectorGrid->sectorAtIndex(xIndex + 1, yIndex);
    if(sector != nullptr && sector->getPathingCell() != nullptr)
    {
        double cost = getPathCostWithinInBounds(sector->getPathingCell(), cell, sector->getCellsRect(), MOVE_STRONGLY_PREFER_COVER);
        if(cost != -1)
        {
            return sector;
        }
    }
    
    //Try down sector
    sector = globalSectorGrid->sectorAtIndex(xIndex, yIndex -1);
    if(sector != nullptr && sector->getPathingCell() != nullptr)
    {
        double cost = getPathCostWithinInBounds(sector->getPathingCell(), cell, sector->getCellsRect(), MOVE_STRONGLY_PREFER_COVER);
        if(cost != -1)
        {
            return sector;
        }
    }
    
    //Try left sector
    sector = globalSectorGrid->sectorAtIndex(xIndex -1, yIndex);
    if(sector != nullptr && sector->getPathingCell() != nullptr)
    {
        double cost = getPathCostWithinInBounds(sector->getPathingCell(), cell, sector->getCellsRect(), MOVE_STRONGLY_PREFER_COVER);
        if(cost != -1)
        {
            return sector;
        }
    }
    //Try up sector
    sector = globalSectorGrid->sectorAtIndex(xIndex, yIndex +1);
    if(sector != nullptr && sector->getPathingCell() != nullptr)
    {
        double cost = getPathCostWithinInBounds(sector->getPathingCell(), cell, sector->getCellsRect(), MOVE_STRONGLY_PREFER_COVER);
        if(cost != -1)
        {
            return sector;
        }
    }
    return nullptr;
}


double PathFinding::getRoute(Vec2 start, Vec2 end, List<RouteNode *> * routeList, int team, bool debug)
{
//    MicroSecondSpeedTimer timer( [] (long long int time) {
//        LOG_DEBUG("    Route Time: %f \n", time/1000.0);
//    });
    
    CollisionCell * goalCell = world->getCollisionGrid(end)->getCellForCoordinates(end);
    CollisionCell * startCell = world->getCollisionGrid(start)->getCellForCoordinates(start);
    
    if(goalCell->getTerrainCharacteristics().getImpassable() ||
       startCell->getTerrainCharacteristics().getImpassable())
    {
        return -1;
    }
    
#ifdef SHOW_PATH_DEBUG
    if(debug)
    {
        clearPathDebug();
        pathDebug.clear();
    }
#endif
    
    // Step one: find the sector node to start and end at
    // The start position MUST be able to directly path to the start node, likewise for the end position
    MapSector * startSector = getMapSectorPathNodeNearCell(startCell);
    MapSector * goalSector = getMapSectorPathNodeNearCell(goalCell);
    
    if(startSector == nullptr || goalSector == nullptr)
    {
        return -1;
    }
    
    RedBlackTree pathsToExamineTree;
    Map<ENTITY_ID, SectorPathBuildingNode *>  pathNodeMap;
    
    SectorPathBuildingNode * initialNode = new SectorPathBuildingNode(startSector, goalSector, team);
    SectorPathBuildingNode * currentNode = initialNode;
    SectorPathBuildingNode * examineNode;
    while(currentNode->getMapSector() != goalSector)
    {
#ifdef SHOW_PATH_DEBUG
        if(debug)
        {
            createPathDebugRecord(currentNode, false, true, MOVE_STRONGLY_PREFER_COVER);
            pathDebug.insert(currentNode->uID(), currentNode);
        }
#endif
        for(auto pair : currentNode->getMapSector()->getPathsToNeighbors())
        {
            examineNode = pathNodeMap.at(pair.first->uID());
            if(examineNode == nullptr)
            {
                examineNode = new SectorPathBuildingNode(currentNode, pair.first, goalSector, team);
                pathNodeMap.insert(pair.first->uID(), examineNode);
                examineNode->release();
            }
            
            if(examineNode->isExamined())
            {
                continue;
            }
            
            if(examineNode->update(currentNode, MOVE_STRONGLY_PREFER_COVER))
            {
                pathsToExamineTree.addObject(examineNode, examineNode->getEstimatedTotalCost());
            }
            
#ifdef SHOW_PATH_DEBUG
            if(debug)
            {
                createPathDebugRecord(examineNode, true, !examineNode->isExamined() , MOVE_STRONGLY_PREFER_COVER);
                pathDebug.insert(examineNode->uID(), examineNode);
            }
#endif
        }
        //choose the cell with the lowest heuristic as the next cell
        currentNode =  (SectorPathBuildingNode *) pathsToExamineTree.findAndRemoveMinimum();
        if(currentNode == nullptr)
        {
            initialNode->release();
            return -1;
        }
    }
    
#ifdef SHOW_PATH_DEBUG
    if(debug)
    {
        printPathDebug(startCell,goalCell);
    }
#endif
    
    
    // Smooth the route by removing nodes that lie in same navigation patch
    SectorPathBuildingNode * smoothNode = currentNode;
    NavigationPatch * currentPatch = smoothNode->getMapSector()->getPathingCell()->getNavigationPatch();
    SectorPathBuildingNode * patchStartNode = smoothNode;

    while (smoothNode->getParent() != nullptr)
    {
        if(smoothNode->getMapSector()->getPathingCell()->getNavigationPatch() != currentPatch)
        {
            patchStartNode->setParent(smoothNode);
            patchStartNode = smoothNode;
            currentPatch = smoothNode->getMapSector()->getPathingCell()->getNavigationPatch();
        }
        smoothNode = (SectorPathBuildingNode *) smoothNode->getParent();
    }
    if(smoothNode->getMapSector()->getPathingCell()->getNavigationPatch() == currentPatch)
    {
        patchStartNode->setParent(smoothNode);
    }

    ////
    SectorRouteNode * node;
    while (currentNode->getParent() != nullptr)
    {
        node = new SectorRouteNode(currentNode->getMapSector(), team);
        routeList->pushFront(node);
        node->release();
        currentNode = (SectorPathBuildingNode *) currentNode->getParent();
    }
    initialNode->release();
}


void PathFinding::createPathDebugRecord(PathBuildingNode * pathingInfo, bool touch , bool valid, MOVEMENT_PREFERENCE movePreference)
{
    PATH_DEBUG * newDebug = new PATH_DEBUG();
        
    newDebug->cellX = pathingInfo->getPosition()->getXIndex();
    newDebug->cellY = pathingInfo->getPosition()->getYIndex();
    
    if(pathingInfo->getParent() != nullptr)
    {
        PatchPathBuildingNode * parent = (PatchPathBuildingNode *) pathingInfo->getParent();
        newDebug->parentX = parent->getPosition()->getXIndex();
        newDebug->parentY = parent->getPosition()->getYIndex();
    }
    else
    {
        newDebug->parentX = -1;
        newDebug->parentY = -1;
    }
    
    newDebug->costSoFar = pathingInfo->getCostToTravelHere();
    newDebug->remainingCostHeuristic = pathingInfo->getRemainingCostHeuristic();
    newDebug->totalCostEstimate = pathingInfo->getEstimatedTotalCost();
    
//    newDebug->patchId = pathingInfo->getNavigationPatch()->uID();
    newDebug->touch = touch;
    newDebug->type = pathingInfo->getPathTypeInt();
    newDebug->validToUpdate = valid;
    newDebug->next = nullptr;
    newDebug->terrainMovementCost = pathingInfo->getPosition()->getTerrainCharacteristics().getMovementCost(movePreference);
    newDebug->updateNumber = pathingInfo->updates;
    
    if(pathDebugFront == NULL)
    {
        pathDebugFront = newDebug;
        pathDebugEnd = newDebug;
    }
    else
    {
        pathDebugEnd->next = newDebug;
        pathDebugEnd = newDebug;
    }
}

void PathFinding::printPathDebug(CollisionCell * start, CollisionCell * goal)
{
    // lol, well isn't this dangerous
    pathDebugFilePointer = fopen("/Users/paulreed/desktop/pathDebug.txt" ,"w");
    if(pathDebugFilePointer == nullptr)
    {
        LOG_ERROR ("Error opening file: %s\n",strerror(errno));
        return;
    }
    
    fprintf(pathDebugFilePointer,"START: %d %d \n",start->getXIndex(),start->getYIndex());
    fprintf(pathDebugFilePointer,"GOAL: %d %d \n",goal->getXIndex(),goal->getYIndex());

    PATH_DEBUG * old;
    while(pathDebugFront != nullptr)
    {
        if(pathDebugFront->touch)
        {
            fprintf(pathDebugFilePointer,"     ");
        }
        if(!pathDebugFront->validToUpdate)
        {
            fprintf(pathDebugFilePointer,"!");
        }
        fprintf(pathDebugFilePointer, "Cell: %d %d Parent: %d %d P: %lld CSF: %f RCH: %f TE: %f Type: %d Cost: %f U: %d",
                pathDebugFront->cellX,pathDebugFront->cellY,
                pathDebugFront->parentX,pathDebugFront->parentY,
                pathDebugFront->patchId,
                pathDebugFront->costSoFar, pathDebugFront->remainingCostHeuristic, pathDebugFront->totalCostEstimate,
                pathDebugFront->type,
                pathDebugFront->terrainMovementCost,
                pathDebugFront->updateNumber);
        
        fprintf(pathDebugFilePointer,"\n");
        old = pathDebugFront;
        pathDebugFront = pathDebugFront->next;
        delete old;
    }
    fclose(pathDebugFilePointer);
    
}


void PathFinding::clearPathDebug()
{
    PATH_DEBUG * old;
    while(pathDebugFront != nullptr)
    {
        old = pathDebugFront;
        pathDebugFront = pathDebugFront->next;
        delete old;
    }
}
