//
//  Transporter.cpp
//  TrenchWars
//
//  Created by Paul Reed on 1/13/22.
//

#include "Courier.h"
#include "EngagementManager.h"
#include "CollisionGrid.h"
#include "EntityFactory.h"
#include "PrimativeDrawer.h"
#include "GameStatistics.h"
#include "MultithreadedAutoReleasePool.h"
#include "EntityManager.h"
#include "World.h"

Courier::Courier() : Human()
{
    
}

bool Courier::init(HumanDefinition * definition, Vec2 position, Command * command, CommandPost * owningPost)
{
    if(Human::init(definition, position, command))
    {
        _status = COURIER_WAITING;
        _owningPost = owningPost;
        _packageToDeliver = nullptr;
        
        if(getTeamID() == 1)
        {
            _sprite->setColor(Color3B(255, 204, 204));
        }
    }
    else
    {
        CCLOG("oh no!!! ");
    }
    
    return true;
}

Courier * Courier::create(HumanDefinition * definition, Vec2 position, Command * command, CommandPost * owningPost)
{
    Courier * newCourier = new Courier();
    if(newCourier->init(definition, position, command, owningPost))
    {
        globalAutoReleasePool->addObject(newCourier);
        return newCourier;
    }
    CC_SAFE_DELETE(newCourier);
    return nullptr;
}


Courier * Courier::create(const std::string & definitionName, Vec2 position, Command * command, CommandPost * owningPost)
{
    HumanDefinition * definition = (HumanDefinition *) globalEntityFactory->getPhysicalEntityDefinitionWithName(definitionName);
    if(definition != nullptr)
    {
        return Courier::create(definition, position, command, owningPost);
    }
    return nullptr;
}

Courier * Courier::create()
{
    Courier * newCourier = new Courier();
    globalAutoReleasePool->addObject(newCourier);
    return newCourier;
}

void Courier::query(float deltaTime)
{
    Human::query(deltaTime);
    
    if(atGoalPosition())
    {
        if(_status == COURIER_TRAVELLING)
        {
            _packageToDeliver->deliverPackage();
            if(_packageToDeliver->deliveryComplete())
            {
                _packageToDeliver->release();
                _packageToDeliver = nullptr;
            }
            
            _status = COURIER_RETURNING;
            setGoalPosition(getCourierWaitPosition());
        }
        else if(_status == COURIER_RETURNING)
        {
            _status = COURIER_WAITING;
            if(_packageToDeliver != nullptr)
            {
                _packageToDeliver->release();
                _packageToDeliver = nullptr;
            }
        }
    }
}

void Courier::transportPackage(Package * package)
{
    _packageToDeliver = package;
    _packageToDeliver->retain();
    _status = COURIER_TRAVELLING;
    
    setGoalPosition(_packageToDeliver->getDestination());
}


/**
 Determine where couriers should wait when they aren't deliverying a package. TODO FIX this is stupid, the courier should obviously be the one to do this....
 */
Vec2 Courier::getCourierWaitPosition()
{
    // Look for a valid spot in the CommandPost's underground area (the bunker)
    WorldSection * undergroundArea = _owningPost->getUndergroundArea();
    CollisionGrid * grid = world->getCollisionGrid(undergroundArea->_areaBounds.origin);
    CollisionCell * best = nullptr;
    short bestValue = -1;
    int searchRadius = 10; //TODO FIX MAGIC NUMBER
    grid->iterateCellsInCircle(undergroundArea->_areaBounds.origin, searchRadius, [this, &best, &bestValue] (CollisionCell * cell)
    {
        if(cell->isTraversable() && _owningTeam->tryToClaimPosition(cell, uID()))
        {
            short cover = cell->getTerrainCharacteristics().getTotalSecurity();
            if(cover > bestValue)
            {
                if(best != nullptr)
                {
                    _owningTeam->releasePositionClaim(best);
                }
                bestValue = cover;
                best = cell;
            }
            else
            {
                _owningTeam->releasePositionClaim(cell);
            }
        }
        return true;
    });
    
    if(best != nullptr)
    {
        _owningTeam->releasePositionClaim(best);
        return best->getCellPosition();
    }
    
    // Failed to find a spot below ground, now try above ground in a radius around the CommandPost
    grid = world->getCollisionGrid(_owningPost->getPosition());
    best = nullptr;
    bestValue = -1;
    grid->iterateCellsInCircle(_owningPost->getPosition(), searchRadius, [this, &best, &bestValue] (CollisionCell * cell)
                               {
        if(cell->isTraversable() && _owningTeam->tryToClaimPosition(cell, uID()))
        {
            short cover = cell->getTerrainCharacteristics().getTotalSecurity();
            if(cover > bestValue)
            {
                if(best != nullptr)
                {
                    _owningTeam->releasePositionClaim(best);
                }
                bestValue = cover;
                best = cell;
            }
            else
            {
                _owningTeam->releasePositionClaim(cell);
            }
        }
        return true;
    });
    if(best != nullptr)
    {
        _owningTeam->releasePositionClaim(best);
        return best->getCellPosition();
    }
    return _position;
}

void Courier::die()
{
    if(_packageToDeliver != nullptr)
    {
        _owningPost->courierDied(this);
        _packageToDeliver->release();
        _packageToDeliver = nullptr;
    }
    Human::die();
}

void Courier::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Human::loadFromArchive(archive);
    int packageType;
    archive(packageType);
    if(packageType == AMMO_PACKAGE)
    {
        _packageToDeliver = AmmoPackage::create();
        _packageToDeliver->loadFromArchive(archive);
        _packageToDeliver->retain();
    }
    else if(packageType == ORDER_PACKAGE)
    {
        _packageToDeliver = OrderPackage::create();
        _packageToDeliver->loadFromArchive(archive);
        _packageToDeliver->retain();
    }
    else
    {
        _packageToDeliver = nullptr;
    }
    ENTITY_ID thingId;
    
    archive(thingId);
    if(thingId != -1)
    {
        _owningPost = dynamic_cast<CommandPost *>(globalEntityManager->getEntity(thingId));
    }
    else
    {
        _owningPost = nullptr;
    }
    
    int status;
    archive(status);
    _status = (COURIER_STATUS) status;
}

void Courier::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Human::saveToArchive(archive);
    if(_packageToDeliver != nullptr)
    {
        archive((int) _packageToDeliver->getPackageType());
        _packageToDeliver->saveToArchive(archive);
    }
    else
    {
        archive((int) -1);
    }
    if(_owningPost != nullptr)
    {
        archive(_owningPost->uID());
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
    archive((int)_status);
    
}
