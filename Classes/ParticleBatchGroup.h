//
//  ParticleBatchGroup.hpp
//  TrenchWars
//
//  Created by Paul Reed on 7/23/22.
//

#ifndef ParticleBatchGroup_h
#define ParticleBatchGroup_h

#include <utility>
#include "cocos2d.h"
#include "Refs.h"

USING_NS_CC;

class ParticleBatchGroup : public Ref
{
    Vector<ParticleBatchNode *> _batches;    
    int _currentBatchIndex;
    std::string _particleName;
    Vec2 _position;
    
    ParticleSystemQuad * _templateParticle;
    void init(const std::string & particleName, Vec2 position);
    
    ParticleSystemQuad * copyParticle(ParticleSystemQuad * particleToCopy, bool dynamic, bool permanent);

    
public:
    
    static ParticleBatchGroup * create(const std::string & particleName, Vec2 position);

    void update(float deltaTime);
    ParticleSystem * addParticle(Vec2 position, float height, bool dynamic = false, bool permanent = false);
    void setVisible(bool visible);
    void setPositionOffset(Vec2 position);
    void removeParticleForOwner(ENTITY_ID owner);
};


#endif /* ParticleBatchGroup_h */
