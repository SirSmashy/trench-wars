//
//  DebugEntity.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/22/22.
//

#include "DebugEntity.h"
#include "MultithreadedAutoReleasePool.h"
#include "World.h"
#include "CollisionGrid.h"
#include "EntityManager.h"
#include "TestBed.h"
#include "EngagementManager.h"
#include "LineDrawer.h"

void DebugEntity::init(Vec2 position, const std::string & labelString, Vec2 labelOffset, bool hideOnNotVisible)
{
    Entity::init();
    _hideOnNotVisible = hideOnNotVisible;
    _label = nullptr;
    _labelText = labelString;
    _labelOffset = labelOffset;
    _position = position;
    primativeDrawer = PrimativeDrawer::create();
    primativeDrawer->retain();
    primativeDrawer->drawDot(_position, 1, Color4F::RED);
    _debugging = false;
    if(_hideOnNotVisible)
    {
        primativeDrawer->setVisible(false);
    }
    else
    {
        primativeDrawer->setOpacity(50);
    }

}

DebugEntity * DebugEntity::create(Vec2 position, const std::string & labelString, Vec2 labelOffset, bool hideOnNotVisible)
{
    DebugEntity * ent = new DebugEntity();
    ent->init(position, labelString, labelOffset, hideOnNotVisible);
    globalAutoReleasePool->addObject(ent);
    
    return ent;
}

void DebugEntity::setPosition(Vec2 position)
{
    Vec2 oldPosition = _position;
    _position = position;
    if(_label != nullptr)
    {
        _label->setPosition(position + _labelOffset);
    }
    globalTestBed->updateDebugEntityAtPosition(this, oldPosition);
    primativeDrawer->clear();
    primativeDrawer->drawDot(_position, 1, Color4F::RED);
}


void DebugEntity::addLine(Vec2 start, Vec2 end, Color3B color, bool flowing, bool dotted)
{
    LineDrawer * line = LineDrawer::create(start, end);
    line->setColor(color);
    line->makeDashed();
    line->setFlowing(flowing);
    line->setOpacity(50);
    _lines.pushBack(line);
}

void DebugEntity::clearLines()
{
    for(auto line : _lines)
    {
        line->remove();
    }
    _lines.clear();
}

void DebugEntity::setDebugMe()
{
//    _label->setVisible(true);
    _debugging = true;
}

bool DebugEntity::isDebugMe()
{
    return _debugging;
}

void DebugEntity::clearDebugMe()
{
//    _label->setVisible(false);
    _debugging = false;
}

void DebugEntity::setVisible(bool visible)
{
    if(visible)
    {
        _label = Label::createWithTTF(_labelText, "arial.ttf", 24);
        _label->retain();
        _label->setPosition(_position + _labelOffset);
        _label->setPositionZ(Z_POSITION_MENU_LAYER);
        world->addChild(_label);
        if(_hideOnNotVisible)
        {
            primativeDrawer->setVisible(visible);
        }
        else
        {
            primativeDrawer->setOpacity(255);
            for(auto line : _lines)
            {
                line->setOpacity(255);
            }
        }
    }
    else
    {
        if(_label !=  nullptr)
        {
            world->removeChild(_label);
            _label->release();
            _label = nullptr;
        }
        if(_hideOnNotVisible)
        {
            primativeDrawer->setVisible(visible);
        }
        else
        {
            primativeDrawer->setOpacity(50);
            for(auto line : _lines)
            {
                line->setOpacity(50);
            }
        }
    }

}

void DebugEntity::removeFromGame()
{
    primativeDrawer->remove();
    primativeDrawer->release();
    if(_label !=  nullptr)
    {
        world->removeChild(_label);
        _label->release();
        _label = nullptr;
    }
    globalEntityManager->removeEntity(this);
}
