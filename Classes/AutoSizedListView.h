//
//  AutoSizedListView.hpp
//  TrenchWars
//
//  Created by Paul Reed on 8/8/21.
//

#ifndef AutoSizedListView_h
#define AutoSizedListView_h

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::ui;
//forward declations


class AutoSizedListView : public ListView
{
protected:
    virtual bool init() override;
    void updateContentSize();
    
public:
    AutoSizedListView();
    static AutoSizedListView * create();
    void addSeperater();

    virtual void doLayout() override;
};
#endif /* AutoSizedListView_h */
