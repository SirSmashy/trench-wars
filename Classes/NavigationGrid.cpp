//
//  NavigationGrid.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/13/21.
//

#include "NavigationGrid.h"
#include "CollisionGrid.h"
#include <set>
#include <unordered_set>
#include "BackgroundTaskHandler.h"
#include "EngagementManager.h"
#include "NavigationSystem.h"
#include "GameCamera.h"

NavigationGrid::NavigationGrid(Size  size, CollisionGrid *  collisionGrid, const std::vector<NavigationPatchRect> & navPatchRects)
{
    _shouldDrawNavigationPatches = false;
    nextNavigationPatchId = 0;
    _collisionGrid = collisionGrid;
    
    NavigationPatch * initialPatch;
    for(auto rect : navPatchRects)
    {
        initialPatch = new NavigationPatch(rect, this);
        globalNavigationSystem->addPatchToInit(initialPatch);
        globalNavigationSystem->addPatchToGenerateBorders(initialPatch);
        _navigationPatches.insert(initialPatch->uID(), initialPatch);
        initialPatch->release();
    }
    _primitiveDrawer = PrimativeDrawer::create();
}

NavigationGrid::NavigationGrid(Size size, CollisionGrid *  collisionGrid)
{
    _shouldDrawNavigationPatches = false;
    nextNavigationPatchId = 0;
    _collisionGrid = collisionGrid;
    createInitialPatches(size.width, size.height);
    _primitiveDrawer = PrimativeDrawer::create();
}

void NavigationGrid::createInitialPatches(int gridSizeX, int gridSizeY)
{

//    NavigationPatchRect rect = NavigationPatchRect(0,gridSizeX-1,0,gridSizeY-1);
//    NavigationPatch * initialPatch = new NavigationPatch(rect, this);
    
    int xMin = 0;
    int xMax = MAX_PATCH_SIZE < gridSizeX ? MAX_PATCH_SIZE : gridSizeX -1;
    NavigationPatch * initialPatch;
    while(xMin < gridSizeX)
    {
        int yMin = 0;
        int yMax = MAX_PATCH_SIZE < gridSizeY ? MAX_PATCH_SIZE : gridSizeY - 1;
        while(yMin < gridSizeY)
        {
            NavigationPatchRect rect = NavigationPatchRect(xMin,xMax,yMin,yMax);
            initialPatch = new NavigationPatch(rect, this);
            initialPatch->claimCollisionCells();
//            globalNavigationSystem->addPatchToInit(initialPatch);
            globalNavigationSystem->addPatchToGenerateBorders(initialPatch);
            _navigationPatches.insert(initialPatch->uID(), initialPatch);
            initialPatch->release();
            yMin = yMax +1;
            yMax = yMax + MAX_PATCH_SIZE >= gridSizeY ? gridSizeY -1 : yMax + MAX_PATCH_SIZE;
        }
        xMin = xMax +1;
        xMax = xMax + MAX_PATCH_SIZE >= gridSizeX ? gridSizeX -1 : xMax + MAX_PATCH_SIZE;
    }
}

bool NavigationGrid::patchIdValid(ENTITY_ID patchId)
{
    NavigationPatch * patch =  _navigationPatches.at(patchId);
    return patch != nullptr;
}


bool NavigationGrid::patchContainsPoint(ENTITY_ID patchId, Vec2 position)
{
    CollisionCell * cell = _collisionGrid->getCellForCoordinates(position);
    NavigationPatch * patch =  _navigationPatches.at(patchId);
    if(patch == nullptr || cell == nullptr)
    {
        return false;
    }
    return patch->containsCell(cell);
}

/** Splits a rectangle into 3-5 smaller rectanges. 3 rectangles if the split point is in the corner of the old rectangle, 4 if the split point is along the border of the old rectangle, and 5 otherwise
 
 */
std::list<NavigationPatchRect> NavigationGrid::getRectsForSplitPoint(NavigationPatch * patchingBeingSplit, int splitX, int splitY, const TerrainCharacteristics & terrain)
{
    std::list<NavigationPatchRect> splitRects;
    DIRECTION border = patchingBeingSplit->_patchRect.getBorderForCoordinates(splitX, splitY);
    TerrainCharacteristics patchBeingSplitTerrain = patchingBeingSplit->_patchRect.getTerrain();
    
    // right then down
    NavigationPatchRect splitRight = NavigationPatchRect(splitX + 1, patchingBeingSplit->_patchRect.getMaxX(), patchingBeingSplit->_patchRect.getMinY(), splitY, patchBeingSplitTerrain);
    //up then right
    NavigationPatchRect splitUp = NavigationPatchRect(splitX, patchingBeingSplit->_patchRect.getMaxX(), splitY + 1, patchingBeingSplit->_patchRect.getMaxY(), patchBeingSplitTerrain);
    // left then up
    NavigationPatchRect splitLeft = NavigationPatchRect(patchingBeingSplit->_patchRect.getMinX(), splitX - 1, splitY, patchingBeingSplit->_patchRect.getMaxY(), patchBeingSplitTerrain);
    //down then left
    NavigationPatchRect splitDown = NavigationPatchRect(patchingBeingSplit->_patchRect.getMinX(), splitX, patchingBeingSplit->_patchRect.getMinY(), splitY - 1, patchBeingSplitTerrain);
    // center
    NavigationPatchRect splitRect = NavigationPatchRect(splitX, splitX, splitY, splitY, terrain);
    splitRects.push_back(splitRect);
    switch (border) {
        case NONE:
            if(splitRight.isValidRect()) splitRects.push_back(splitRight);
            if(splitUp.isValidRect()) splitRects.push_back(splitUp);
            if(splitLeft.isValidRect()) splitRects.push_back(splitLeft);
            if(splitDown.isValidRect()) splitRects.push_back(splitDown);
            break;
        case UP:
            if(splitRight.isValidRect()) splitRects.push_back(splitRight);
            if(splitLeft.isValidRect()) splitRects.push_back(splitLeft);
            if(splitDown.isValidRect()) splitRects.push_back(splitDown);
            break;
        case RIGHT:
            if(splitUp.isValidRect()) splitRects.push_back(splitUp);
            if(splitLeft.isValidRect()) splitRects.push_back(splitLeft);
            if(splitDown.isValidRect()) splitRects.push_back(splitDown);
            break;
        case LEFT:
            if(splitRight.isValidRect()) splitRects.push_back(splitRight);
            if(splitUp.isValidRect()) splitRects.push_back(splitUp);
            if(splitDown.isValidRect()) splitRects.push_back(splitDown);
            break;
        case DOWN:
            if(splitRight.isValidRect()) splitRects.push_back(splitRight);
            if(splitUp.isValidRect()) splitRects.push_back(splitUp);
            if(splitLeft.isValidRect()) splitRects.push_back(splitLeft);
            break;
        case UP_RIGHT:
            if(splitLeft.isValidRect()) splitRects.push_back(splitLeft);
            if(splitDown.isValidRect()) splitRects.push_back(splitDown);
            break;
        case UP_LEFT:
            if(splitRight.isValidRect()) splitRects.push_back(splitRight);
            if(splitDown.isValidRect()) splitRects.push_back(splitDown);
            break;
        case DOWN_LEFT:
            if(splitRight.isValidRect()) splitRects.push_back(splitRight);
            if(splitUp.isValidRect()) splitRects.push_back(splitUp);
            break;
        case DOWN_RIGHT:
            if(splitLeft.isValidRect()) splitRects.push_back(splitLeft);
            if(splitUp.isValidRect()) splitRects.push_back(splitUp);
            break;
        default:
            break;
    }

    return splitRects;
}

static int totalReshapeCalls;


NavigationPatchRect NavigationGrid::getMinMergeRemainder(NavigationPatch * patch, NavigationPatch * other, NavigationPatchRect & combination, DIRECTION border)
{
    NavigationPatchRect mergeRemainderMinimum;

    if(border == UP || border == DOWN)
    {
        // Check for X dis-intersections
        if(patch->_patchRect.getMinX() < combination.getMinX())
        {
            mergeRemainderMinimum.set(patch->_patchRect.getMinX(), combination.getMinX()-1,  patch->_patchRect.getMinY(), patch->_patchRect.getMaxY(), patch->_patchRect.getTerrain());
        }
        else if(other->_patchRect.getMinX() < combination.getMinX())
        {
            mergeRemainderMinimum.set(other->_patchRect.getMinX(), combination.getMinX()-1,  other->_patchRect.getMinY(), other->_patchRect.getMaxY(), patch->_patchRect.getTerrain());
        }
    }
    else
    {
        // Check  for y disintersections
        if(patch->_patchRect.getMinY() < combination.getMinY())
        {
            mergeRemainderMinimum.set(patch->_patchRect.getMinX(), patch->_patchRect.getMaxX(),  patch->_patchRect.getMinY(), combination.getMinY()-1, patch->_patchRect.getTerrain());
        }
        else if(other->_patchRect.getMinY() < combination.getMinY())
        {
            mergeRemainderMinimum.set(other->_patchRect.getMinX(), other->_patchRect.getMaxX(),  other->_patchRect.getMinY(), combination.getMinY()-1, patch->_patchRect.getTerrain());
        }
    }
    return mergeRemainderMinimum;
}

NavigationPatchRect NavigationGrid::getMaxMergeRemainder(NavigationPatch * patch, NavigationPatch * other, NavigationPatchRect & combination, DIRECTION border)
{
    NavigationPatchRect mergeRemainderMaximum;

    if(border == UP || border == DOWN)
    {
        if(patch->_patchRect.getMaxX() > combination.getMaxX())
        {
            mergeRemainderMaximum.set(combination.getMaxX()+1, patch->_patchRect.getMaxX(),  patch->_patchRect.getMinY(), patch->_patchRect.getMaxY(), patch->_patchRect.getTerrain());
        }
        else if(other->_patchRect.getMaxX() > combination.getMaxX())
        {
            mergeRemainderMaximum.set(combination.getMaxX()+1, other->_patchRect.getMaxX(),  other->_patchRect.getMinY(), other->_patchRect.getMaxY(), patch->_patchRect.getTerrain());
        }
    }
    else
    {
        if(patch->_patchRect.getMaxY() > combination.getMaxY())
        {
            mergeRemainderMaximum.set(patch->_patchRect.getMinX(), patch->_patchRect.getMaxX(),  combination.getMaxY()+1, patch->_patchRect.getMaxY(), patch->_patchRect.getTerrain());
        }
        else if(other->_patchRect.getMaxY() > combination.getMaxY())
        {
            mergeRemainderMaximum.set(other->_patchRect.getMinX(), other->_patchRect.getMaxX(),  combination.getMaxY()+1, other->_patchRect.getMaxY(), patch->_patchRect.getTerrain());
        }

    }
    return mergeRemainderMaximum;
}


bool NavigationGrid::shouldMergePatches(NavigationPatch * patch, NavigationPatch * other, std::list<NavigationPatchRect> & mergedPatches)
{
    if(patch == other || !patch->hasSameTerrain(other))
    {
        return false;
    }
    totalReshapeCalls++;
    
    ///////////// Find the  cells of the two patches that are within the border coordinates  of the two patches
    ///
    /// 111111111oooo
    /// 111111111oooo
    /// ooo222222222
    /// ooo222222222
    /// ooo222222222
    /// In the above image  '1'  represents cells only in the 1st patch, '2' represents cells in the 2nd patch, and 'o' represents cells in other patches
    /// The combination of the above patches would create the following:
    /// 111ccccccoooo
    /// 111ccccccooo
    /// ooocccccc222
    /// ooocccccc222
    /// ooocccccc222
    /// In this image 'c' represents the new combined rect
    DIRECTION border = patch->_patchRect.getBorderDirectionForRect(other->_patchRect);
    NavigationPatchRect combination;
    
    if(border == UP || border == DOWN)
    {
        combination.set(std::max(patch->_patchRect.getMinX(), other->_patchRect.getMinX()),
                        std::min(patch->_patchRect.getMaxX(), other->_patchRect.getMaxX()),
                        std::min(patch->_patchRect.getMinY(), other->_patchRect.getMinY()),
                        std::max(patch->_patchRect.getMaxY(), other->_patchRect.getMaxY()),
                        patch->_patchRect.getTerrain());
    }
    else if(border == LEFT || border == RIGHT)
    {
        combination.set(std::min(patch->_patchRect.getMinX(), other->_patchRect.getMinX()),
                        std::max(patch->_patchRect.getMaxX(), other->_patchRect.getMaxX()),
                        std::max(patch->_patchRect.getMinY(), other->_patchRect.getMinY()),
                        std::min(patch->_patchRect.getMaxY(), other->_patchRect.getMaxY()),
                        patch->_patchRect.getTerrain());
    }
    else
    {
        return false;
    }
    
    bool combinationAspectValid = combination.hasValidAspectRatio();
    double sumAspect = combination.getAspectRatio();
    double oldAspect = patch->_patchRect.getAspectRatio() + other->_patchRect.getAspectRatio();
    if((!combinationAspectValid  && sumAspect > oldAspect) || combination.getArea() > MAX_PATCH_AREA)
    {
        return false;
    }
    
    mergedPatches.push_back(combination);

    // Now create up to 2 additional rects:
    // One for the remainder of the first patch that is not a part of the combination patch
    // A second for the remainder of the second patch that is not a part of the combination patch
    ////////////
    ///
    ///

    NavigationPatchRect mergeRemainder = getMinMergeRemainder(patch, other, combination, border);

    if(mergeRemainder.isValidRect())
    {
        sumAspect += mergeRemainder.getAspectRatio();
        if( (!mergeRemainder.hasValidAspectRatio() && sumAspect > oldAspect) || mergeRemainder.getArea() > MAX_PATCH_AREA)
        {
            return false;
        }
        mergedPatches.push_back(mergeRemainder);
    }
    
    mergeRemainder = getMaxMergeRemainder(patch, other, combination, border);

    ///////
    if(mergeRemainder.isValidRect())
    {
        sumAspect += mergeRemainder.getAspectRatio();
        if( (!mergeRemainder.hasValidAspectRatio() && sumAspect > oldAspect) || mergeRemainder.getArea() > MAX_PATCH_AREA)
        {
            return false;
        }
        mergedPatches.push_back(mergeRemainder);
    }
    
    
    // FIX todo need something here to allow for small rects to combine even when the aspect ratio is ugly
    if(mergedPatches.size() > 0 &&
       ((mergedPatches.size() == 1 && combinationAspectValid) || // encourage merging  into a single block, even if the aspect ratio isn't better than the non-merged
        sumAspect < oldAspect))
    {
        return true;
    }
    return false;
}

/**
 *
 *
 */
void NavigationGrid::mergePatches(std::list<NavigationPatch *> patches, std::list<NavigationPatch *> neighbors)
{
    std::unordered_set<NavigationPatch *> patchesToExamine(patches.begin(), patches.end());
    std::unordered_set<NavigationPatch *> otherPatches(patches.begin(), patches.end());
    
    patchesToExamine.insert(neighbors.begin(),neighbors.end());
    patchesToExamine.reserve(64);
    otherPatches.reserve(512);

    totalReshapeCalls = 0;
    int totalExamined = 0;
    int totalLooks = 0;
    int totalReshapes = 0;
    while(patchesToExamine.size() > 0)
    {
        totalExamined ++;
        auto patch = *(patchesToExamine.begin());
        patchesToExamine.erase(patch);
        for(auto other : otherPatches)
        {
            totalLooks++;
            std::list<NavigationPatchRect> mergedPatches;
            if(shouldMergePatches(patch, other, mergedPatches))
            {
                totalReshapes++;
                for(auto newRect : mergedPatches)
                {
                    NavigationPatch * newPatch = new NavigationPatch(newRect, this);
                    patchesToExamine.insert(newPatch);
                    otherPatches.insert(newPatch);
                }

                // Since the patch to examine and the other patch have been reshaped, add their neighbors to the patches to examine
                //  since the neighbors might flip as well now
                for(auto neighbor : patch->getNeighbors())
                {
                    if(neighbor.second->_neighboringPatch->uID() == other->uID() || neighbor.second->_neighboringPatch->isMarkedForRemoval())
                    {
                        continue;
                    }
                    patchesToExamine.insert(neighbor.second->_neighboringPatch);
                }

                for(auto neighbor : other->getNeighbors())
                {
                    if(neighbor.second->_neighboringPatch->uID() == patch->uID() || neighbor.second->_neighboringPatch->isMarkedForRemoval())
                    {
                        continue;
                    }
                    patchesToExamine.insert(neighbor.second->_neighboringPatch);
                }

                patchesToExamine.erase(other);
                otherPatches.erase(other);
                otherPatches.erase(patch);
                
                markPatchForRemoval(other);
                markPatchForRemoval(patch);

                break;
            }
        }
    }
    
    for(NavigationPatch * patch : otherPatches)
    {
        if(_navigationPatches.find(patch->uID()) == _navigationPatches.end())
        {
            globalNavigationSystem->addPatchToInit(patch);
            _navigationPatches.insert(patch->uID(), patch);
            patch->release();
        }
        globalNavigationSystem->addPatchToGenerateBorders(patch);

    }
//    LOG_DEBUG("Total examined %d total touch %d total reshape %d total tried %d \n\n", totalExamined, totalLooks, totalReshapes, totalReshapeCalls);
}

void NavigationGrid::addSplitCell(CollisionCell * splitCell)
{
    if(globalEngagementManager->getEngagementState() == ENGAGEMENT_STATE_LOADING)
    {
        return;
    }

    splitPointsMutex.lock();
    if(splitCell == theInvalidCell)
    {
        LOG_DEBUG("NO! BAD! \n");
    }
    _splitPoints.insert(splitCell);
    splitPointsMutex.unlock();
}


void NavigationGrid::splitNavigationPatches()
{
    splitPointsMutex.lock();
    if(_splitPoints.size() == 0)
    {
        splitPointsMutex.unlock();
        return;
    }
    
    auto front = _splitPoints.begin();
    CollisionCell * splitCell = *front;
    _splitPoints.erase(front);
    splitPointsMutex.unlock();

    int splitX = splitCell->getXIndex();
    int splitY = splitCell->getYIndex();
    NavigationPatch * oldPatch = splitCell->getNavigationPatch();
    
    std::list<NavigationPatchRect> splitRects = getRectsForSplitPoint(oldPatch, splitX, splitY, splitCell->getTerrainCharacteristics());
    std::list<NavigationPatch *> patchesToUpdate;
    
    //See if the NavigationPatch's neighbors can expand into any of the split rectangles
    for(NavigationPatchRect rect : splitRects)
    {
        NavigationPatch * splitPatch = new NavigationPatch(rect, this);
        patchesToUpdate.push_back(splitPatch);
    }
    
    std::list<NavigationPatch *> neighbors;
    for(auto neighbor : oldPatch->getNeighbors())
    {
        neighbors.push_back(neighbor.second->_neighboringPatch);
    }
    
    markPatchForRemoval(oldPatch, true);
    mergePatches(patchesToUpdate, neighbors);
}



void NavigationGrid::markPatchForRemoval(NavigationPatch * patch, bool immediatelyDelete)
{
    patch->markForRemoval();
    globalNavigationSystem->removePatchToGenerateBorders(patch);
    if(immediatelyDelete)
    {
        patch->remove();
    }
    else
    {
        globalNavigationSystem->addPatchToRemove(patch);
        patch->retain();
    }
    _navigationPatches.erase(patch->uID());
}

void NavigationGrid::drawNavigationGrid(Vec2 drawNearLocation)
{
    if(!_shouldDrawNavigationPatches)
    {
        return;
    }

    for(ENTITY_ID key : _navigationPatches.keys())
    {
        NavigationPatch * patch = _navigationPatches.at(key);
        float length =  patch->_patchRect.minimumDistanceToCoordinates(drawNearLocation.x, drawNearLocation.y);
        if(length < 100 || patch->_patchRect.containsCoordinates(drawNearLocation.x, drawNearLocation.y))
        {
            patch->draw();
        }
        else
        {
            patch->stopDrawing();
        }
    }
}

void NavigationGrid::toggleShowNavigationPatches()
{
    _shouldDrawNavigationPatches = !_shouldDrawNavigationPatches;
    if(!_shouldDrawNavigationPatches)
    {
        _primitiveDrawer->clear();
        for(ENTITY_ID key : _navigationPatches.keys())
        {
            NavigationPatch * patch = _navigationPatches.at(key);
            patch->stopDrawing();
        }
    }
}

void NavigationGrid::shutdown()
{
    Vector<NavigationPatch *> removals;
    for(auto patchPair : _navigationPatches)
    {
        removals.pushBack(patchPair.second);
    }
    for(auto patch : removals)
    {
        patch->remove();
    }
    
    _navigationPatches.clear();
    _splitPoints.clear();
    _primitiveDrawer->remove();
}

