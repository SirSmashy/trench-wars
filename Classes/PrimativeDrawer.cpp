//
//  PrimativeDrawer.cpp
//  TrenchWars
//
//  Created by Paul Reed on 11/16/14.
//

#include "PrimativeDrawer.h"
#include "EngagementManager.h"
#include "VectorMath.h"
#include "MultithreadedAutoReleasePool.h"

PrimativeDrawer::PrimativeDrawer() : DrawNode()
{
    
}

PrimativeDrawer::~PrimativeDrawer()
{
}

bool PrimativeDrawer::init(bool addToPlayeLayer)
{
    if(DrawNode::init())
    {
        if(addToPlayeLayer)
        {
            globalPlayerLayer->addChild(this, 0); //999
        }
        return true;
    }
    return false;
}

PrimativeDrawer * PrimativeDrawer::create(bool addToPlayeLayer)
{
    PrimativeDrawer * newDrawer = new PrimativeDrawer();
    if(newDrawer && newDrawer->init(addToPlayeLayer))
    {
        globalAutoReleasePool->addObject(newDrawer);
        return newDrawer;
    }
    CC_SAFE_DELETE(newDrawer);
    return nullptr;
}


void PrimativeDrawer::clear()
{
    DrawNode::clear();
}

void PrimativeDrawer::drawBounds(Rect bounds, Color4F color, int lineRadius)
{
    drawSegment(bounds.origin, Vec2(bounds.origin.x + bounds.size.width, bounds.origin.y), lineRadius, color);
    
    drawSegment(Vec2(bounds.origin.x + bounds.size.width, bounds.origin.y), Vec2(bounds.origin.x + bounds.size.width, bounds.origin.y + bounds.size.height), lineRadius, color);
    
    drawSegment( Vec2(bounds.origin.x + bounds.size.width,bounds.origin.y + bounds.size.height) ,Vec2(bounds.origin.x,bounds.origin.y + bounds.size.height) , lineRadius, color);

    drawSegment(Vec2(bounds.origin), Vec2(bounds.origin.x,bounds.origin.y + bounds.size.height), lineRadius, color);
}

void PrimativeDrawer::drawSolidRectWithCornerRadius(Rect bounds, Color4F color, int cornerRadius, int vertsPerCorner)
{
    // The corner radius is the hypotenuse of a right triangle, find the length of the opposite
    double offset = SIN_45 * cornerRadius;
    double radiansPerCornerVert = (M_PI_2) / vertsPerCorner;
    unsigned int vertCounter = 0;
    std::vector<Vec2> vertices;
    vertices.reserve(vertsPerCorner * 4);   
    
    Vec2 nextVert;
    
    //Bottom left curve
    Vec2 curveCenter = bounds.origin;
    curveCenter.x += cornerRadius;
    curveCenter.y += cornerRadius;
    for(int i = 0; i < vertsPerCorner; i++) {
        nextVert = curveCenter;
        nextVert.x -= offset * sin(radiansPerCornerVert * i);
        nextVert.y -= offset * cos(radiansPerCornerVert * i);
        vertices[vertCounter] = nextVert;
        vertCounter++;
    }
    
    // top left curve
    curveCenter = bounds.origin;
    curveCenter.x += cornerRadius;
    curveCenter.y += bounds.size.height - cornerRadius;
    for(int i = 0; i < vertsPerCorner; i++) {
        nextVert = curveCenter;
        nextVert.x -= offset * cos(radiansPerCornerVert * i);
        nextVert.y += offset * sin(radiansPerCornerVert * i);
        vertices[vertCounter] = nextVert;
        vertCounter++;
    }
    
    // top right curve
    curveCenter = bounds.origin;
    curveCenter.x += bounds.size.width - cornerRadius;
    curveCenter.y += bounds.size.height - cornerRadius;
    for(int i = 0; i < vertsPerCorner; i++) {
        nextVert = curveCenter;
        nextVert.x += offset * sin(radiansPerCornerVert * i);
        nextVert.y += offset * cos(radiansPerCornerVert * i);
        vertices[vertCounter] = nextVert;
        vertCounter++;
    }
    
    // bottom right curve
    curveCenter = bounds.origin;
    curveCenter.x += bounds.size.width - cornerRadius;
    curveCenter.y += cornerRadius;
    for(int i = 0; i < vertsPerCorner; i++) {
        nextVert = curveCenter;
        nextVert.x += offset * cos(radiansPerCornerVert * i);
        nextVert.y -= offset * sin(radiansPerCornerVert * i);
        vertices[vertCounter] = nextVert;
        vertCounter++;
    }
    drawSolidPoly(vertices.data(), vertsPerCorner * 4, color );
}


void PrimativeDrawer::drawTriangle(Vec2 position, float triangleWidth, Color4F color)
{
    //Draw an equalateral triangle
    Vec2 p1(position.x - (triangleWidth / 2), position.y - (triangleWidth / 2));
    Vec2 p2(position.x + (triangleWidth / 2), position.y - (triangleWidth / 2));
    Vec2 p3(position.x, position.y + (triangleWidth / 2));

    cocos2d::DrawNode::drawTriangle(p1, p2, p3, color);
}

void PrimativeDrawer::remove()
{
    globalPlayerLayer->removeChild(this);

}
