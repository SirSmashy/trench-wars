//
//  ServerEventController.h
//  TrenchWars
//
//  Created by Paul Reed on 1/17/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#ifndef __SERVER_EVENT_CONTROLLER_H__
#define __SERVER_EVENT_CONTROLLER_H__

#include "cocos2d.h"
#include "GameEventController.h"

struct IssuedOrder
{
    Order * issuedOrder;
    ENTITY_ID unitIssuedOrder;
    ENTITY_ID planOrderIsPartOf;
    
    IssuedOrder(Order * order, ENTITY_ID unit, ENTITY_ID plan)
    {
        issuedOrder = order;
        unitIssuedOrder = unit;
        planOrderIsPartOf = plan;
    }
};

struct CompletedOrder
{
    ENTITY_ID completedOrder;
    ENTITY_ID unitCompleting;
    bool unitFailedOrder;
    CompletedOrder(ENTITY_ID order, ENTITY_ID unit, bool failed)
    {
        completedOrder = order;
        unitCompleting = unit;
        unitFailedOrder = failed;
    }
};

struct PlanChange
{
    
};

struct TeamChanges
{
    std::vector<IssuedOrder> newIssuedOrders;
    std::vector<CompletedOrder> orderCompleted;
    
    std::unordered_set<ENTITY_ID> plansRemoved;
    std::vector<std::pair<ENTITY_ID, PLAN_ACTION>> planChanges;
};


class ServerEventController : public GameEventController
{
protected:
    std::unordered_map<int, TeamChanges> _teamChanges;

public:

    ServerEventController();
    virtual void init();
    virtual void postLoad();


    static ServerEventController * create();
    virtual void query(float deltaTime);
    
    
    void buildClientUpdate(cereal::BinaryOutputArchive & archive, int teamId);
    void parseUpdateFromClient(cereal::BinaryInputArchive & archive, int teamId, int playerId);

    
    // ////////  New Event Requests ////////
    // Orders
    virtual void issueMoveOrder(Vec2 position, Unit * commandable, Plan * addToPlan);
    virtual void issueOccupyOrder(Objective * objective, Unit * commandable, Plan * addToPlan);
    virtual void issueDigTrenchesOrder(Objective * objective, Unit * commandable, Plan * addToPlan);
    virtual void issueDigBunkersOrder(Objective * objective, Unit * commandable, Plan * addToPlan);
    virtual void issueBuildCommandPostOrder(Objective * objective, Unit * commandable, Plan * addToPlan);
    virtual void issueClearAreaOrder(Objective * objective, Unit * commandable, Plan * addToPlan);
    virtual void issueAttackPositionOrder(Objective * objective, Unit * commandable, Plan * addToPlan);
    virtual void issueStopOrder(Unit * commandable, Plan * addToPlan);

    //Objective
    virtual void createNewObjective(std::vector<Vec2> & points, bool enclosed, Unit * unitCreating);
    virtual void removeObjective(Objective * objective);
    
    // Command Posts
    virtual void createCommandPostInObjective(Objective * objective, Vec2 location);
    
    // Trench Lines
    virtual void createTrenchLineInObjective(Objective * objective);
    virtual void removeTrenchLineInObjective(Objective * objective);
    
    // Bunkers
    virtual void createBunkersInObjective(Objective * objective);
    virtual void removeBunkersInObjective(Objective * objective);
    
    // Plans
    virtual void createNewPlan(int playerId);
    virtual void removePlan(Plan * plan);
    virtual void executePlan(Plan * plan);
    virtual void stopPlan(Plan * plan);
    virtual void renamePlan(Plan * plan, const std::string & newName);
    
    // Team status
    virtual void updateTeamVisibility(Team * team, 
                                      std::unordered_map<ENTITY_ID, double> & sectorVisibleTime,
                                      std::unordered_set<ENTITY_ID> & sectorsNewlyHidden,
                                      std::unordered_set<ENTITY_ID> & sectorsNewlyVisible
                                      );


    // //////////////////////////////////////////////////////
    // ///////// Handling of newly created events ///////////////
    // Orders
    virtual void handleEventOrderIssued(Order * order, Unit * unit, Plan * addToPlan);
    virtual void handleEventOrderComplete(Unit * entCompleting, Order * order, bool failed = false);
    virtual void handleEventOrderCancelled(Unit * commandable);

    // Objectives
    virtual void handleEventObjectiveCreated(Objective * objective);
    virtual void handleEventObjectiveRemoved(Objective * objective);

    // visible
    virtual void handleEventSectorVisible(MapSector * sector);
    virtual void handleEventTeamVisibilityUpdate(Team * team, std::unordered_map<ENTITY_ID, double> & sectorVisibleTime);

    // command post related
    virtual void handleEventNewCommandPost(CommandPost * commandPost);
    virtual void handleEventCommandPostDestroyed(CommandPost * commandPost);
        
    // Unit related
    virtual void handleEventNewUnit(Unit * unit);
    virtual void handleEventUnitDestroyed(Unit * unit);
        
    // Plan related
    virtual void handleEventNewPlan(Plan * plan);
    virtual void handleEventPlanRemoved(Plan * plan);
    virtual void handleEventPlanExecuting(Plan * plan);
    virtual void handleEventPlanStopped(Plan * plan);
    virtual void handleEventOrderRemovedFromPlan(Unit * unit, Plan * plan);
    virtual void handleEventPlanRenamed(Plan * plan, const std::string & newName);

    //friendly unit presence
    virtual void handleEventUnitInSector(MapSector * sector, Unit * unit);


    //enemy unit presence
    virtual void handleEventEnemyUnitSpotted(Unit * enemy);
    virtual void handleEventEnemyCommandPostSpotted(CommandPost *enemy);
    virtual void handleEventEnemyCanEngageSector(MapSector * sector, Human * enemy);
    virtual void handleEventEnemyArtilleryInSector(MapSector * sector);
    //////////// end low level event functions
};

#endif // __LOCAL_COMMAND_CONTROLLER_H__
