//
//  LobbyManager.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/21/23.
//

#ifndef LobbyManager_h
#define LobbyManager_h


#include "cocos2d.h"
#include "ScenarioFactory.h"
#include "GameStateManager.h"
#include "NetworkInterface.h"
#include <cereal/archives/binary.hpp>

class Player;
class LobbyScene;


struct PlayerLobbyStatus
{
    bool playerIsReady = false;
    bool playerNeedsScenario = true;
    bool remotePlayer = true;
};

struct CommandDetails
{
    int commandId;
    int playerId;
    int teamId;
    Vec2 spawnPoint;
};

struct TeamDetails
{
    int teamId;
    std::map<int, CommandDetails> commandIdToCommandDetailsMap;
};

class LobbyManager : public GameStateManager
{    
protected:
    std::map<int, TeamDetails> _teamIdToTeamDetailsMap;

    std::map<int, PlayerLobbyStatus> _playerLobbyStatusMap;

    LobbyScene * _lobbyScene;
    NETWORK_MODE _lobbyNetworkMode;
    
    bool _dataChanged;
public:
        
    const std::map<int, TeamDetails>  & getTeamDetailsMap() {return _teamIdToTeamDetailsMap;}

        
    virtual bool isPlayerReady(int playerId) = 0;
    
    //UI to Manager Functions
    virtual void playerSelectedStartGame(int playerId) = 0;
    virtual void playerSetName(int playerId, const std::string & name) = 0;
    virtual void playerSelectedCommand(int teamId, int playerId, int commandId) = 0;
    virtual void playerSetScenario(const std::string & scenarioName) = 0;
    
    virtual void joinServer(const std::string serverIp) {};

    
    GAME_STATE getState() {return GAME_STATE_IN_LOBBY;}
    
    virtual void shutdown() = 0;
};

#endif /* LobbyManager_hpp */
