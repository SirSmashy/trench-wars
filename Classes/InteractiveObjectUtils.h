//
//  InteractiveObjectUtils.hpp
//  TrenchWars
//
//  Created by Paul Reed on 12/12/22.
//

#ifndef InteractiveObjectUtils_h
#define InteractiveObjectUtils_h

#include "cocos2d.h"
#include "Entity.h"
#include "PlanningObject.h"
#include "InteractiveObject.h"

USING_NS_CC;


class InteractiveObjectUtils
{
public:
    static Entity * createEntityOfType(ENTITY_TYPE type);
    static Vec2 getPositionForObject(InteractiveObject * object);
    static Vec2 getCenterOfObjectPosition(InteractiveObject * object);
    static Size getObjectSize(InteractiveObject * object);
    static InteractiveObject * getInteractiveObject(ENTITY_ID objectID, INTERACTIVE_OBJECT_TYPE type);
    
    static std::string getEntityTypeName(const Entity * ent);
    static std::string getEntityTypeName(ENTITY_TYPE type);
};


#endif /* InteractiveObjectUtils_h */
