//
//  Path.cpp
//  TrenchWars
//
//  Created by Paul Reed on 1/19/23.
//

#include "Path.h"
#include "NavigationGrid.h"
#include "CollisionGrid.h"
#include "PathFinding.h"
#include "TestBed.h"


//////////////////////////////////
///
///
///
PathNode::~PathNode()
{
    if(_addCongestion && world->getNavGrid()->patchIdValid(_patchId))
    {
        _navigationPatch->removeCongestion(_team, _neighboringPatchId);
        _cell->removeCongestion(_team);
    }
//    LOG_DEBUG("Goodbye path node %lld \n ", uID());
}

PathNode::PathNode(Vec2 position, CollisionCell * cell, int team, bool addCongestion, ENTITY_ID neighborPatchId)
{
//    LOG_DEBUG("Hello path node %lld \n ", uID());
    _neighboringPatchId = neighborPatchId;
    _team = team;
    _addCongestion = addCongestion;
    pivot = false;
    _position = position;
    _cell = cell;
    _navigationPatch = cell->getNavigationPatch();
    _patchId = _navigationPatch->uID();
    if(_addCongestion)
    {
        _navigationPatch->addCongestion(_team, _neighboringPatchId);
        _cell->addCongestion(_team);
    }
}

SectorRouteNode::~SectorRouteNode()
{
    _sector->removeCongestion(_team);
}


SectorRouteNode::SectorRouteNode(MapSector * sector, int team)
{
    _team = team;
    _position = sector->getPathingCell()->getCellPosition();
    _sector = sector;
    _sector->addCongestion(_team);
}

TransitionRouteNode::TransitionRouteNode(Transition transition, bool toUnderground) :
_transition(transition)
{
    _toUnderground = toUnderground;
}

Vec2 TransitionRouteNode::getTransitionStart()
{
    if(_toUnderground)
    {
        return _transition._end;
    }
    return _transition._start;
}

Vec2 TransitionRouteNode::getTransitionEnd()
{
    if(_toUnderground)
    {
        return _transition._start;
    }
    return _transition._end;
}

Path::Path(int team, bool debug, Vec2 routeOffset)
{
    _routeOffset = routeOffset;
    _team = team;
    _debug = debug;
    _currentRouteNode = nullptr;
}

void Path::buildPath(Vec2 start, Vec2 goalPosition, MOVEMENT_PREFERENCE movePreference)
{
    clear();
    Vec2 currentPosition = start;
    Vec2 overlandGoalPosition = goalPosition;
    double cost = 0;
    const WorldSection * startArea = world->getWorldSectionForPosition(start);
    const WorldSection * endArea = world->getWorldSectionForPosition(goalPosition);
    
    CollisionCell * startCell = world->getCollisionGrid(start)->getCellForCoordinates(start);
    CollisionCell * goalCell = world->getCollisionGrid(goalPosition)->getCellForCoordinates(goalPosition);
    if(startCell == theInvalidCell ||
       goalCell == theInvalidCell ||
       goalCell->getTerrainCharacteristics().getImpassable() ||
       startCell->getTerrainCharacteristics().getImpassable())
    {
        LOG_ERROR("Invalid path due to impassable goal/start %.0f %.0f \n", goalPosition.x, goalPosition.y);
        return;
    }
    
    List<PathNode *> segmentList;
    
    if(startArea->_areaDestroyed || endArea->_areaDestroyed)
    {
        return;
    }
    
    _goalPosition = goalPosition;
    _movePreference = movePreference;
    
    if(startArea != endArea)
    {
        if(startArea->_underground)
        {
            Transition transition = startArea->_transitionsToOtherGrids.at(0);
            TransitionRouteNode * route = new TransitionRouteNode(transition, false);
            _routeNodes.pushBack( route);
            route->release();
            currentPosition = transition._end;
        }
        if(endArea->_underground)
        {
            Transition transition = endArea->_transitionsToOtherGrids.at(0);
            overlandGoalPosition = transition._end;
        }
    }
    
    if(currentPosition.distance(overlandGoalPosition) > (globalSectorGrid->getSectorGridSize().width * 2))
    {
        // build a route
        List<RouteNode *> intermediateRoute;
        globalPathFinding->getRoute(currentPosition, overlandGoalPosition, &intermediateRoute, _team, _debug && globalTestBed->pathTesting);
        if(intermediateRoute.size() > 0)
        {
            intermediateRoute.popBack(); // Don't really need the last node
        }
        _routeNodes.pushBack(intermediateRoute);
    }
    
    if(startArea != endArea && endArea->_underground)
    {
        Transition transition = endArea->_transitionsToOtherGrids.at(0);
        TransitionRouteNode * route = new TransitionRouteNode(transition, true);
        _routeNodes.pushBack( route);
        route->release();
    }
    _routeNodes.pushBack(new RouteNode(goalPosition));
}


bool Path::getPathToSectorRouteNode(Vec2 from, SectorRouteNode * routeNode, Vec2 * modifiedPosition)
{
    double totalCost = 0;
    List<PathNode *> intermediatePath;
    Vec2 routePosition = routeNode->getPosition();
    
    CollisionCell * cell = world->getCollisionGrid(from)->getCellForCoordinates(from);
    cell = routeNode->getSector()->getSectorCellNearestCell(cell);
    cell = routeNode->getSector()->getSectorBorderCellWithLeastCongestion(cell, _team);
    
    if(cell != theInvalidCell && cell->isTraversable())
    {
        routePosition = cell->getCellPosition();
    }
    
    if(modifiedPosition != nullptr)
    {
        modifiedPosition->x = routePosition.x;
        modifiedPosition->y = routePosition.y;
    }
    
    if(globalPathFinding->getPath(from, routePosition, &intermediatePath, _movePreference, _team, _debug && globalTestBed->pathTesting) == -1)
    {
        return false;
    }
    for(auto pathPoint : intermediatePath)
    {
        _pathNodes.pushBack(pathPoint);
        if(globalSectorGrid->sectorForPosition(pathPoint->getPosition()) == routeNode->getSector())
        {
            break;
        }
    }
    return true;
}

bool Path::getPathToTransitionNode(Vec2 from, TransitionRouteNode * transitionNode)
{
    double totalCost = 0;
    List<PathNode *> intermediatePath;
    if(globalPathFinding->getPath(from, transitionNode->getTransitionStart(), &intermediatePath, _movePreference, _team, _debug && globalTestBed->pathTesting) == -1)
    {
        return false;
    }
    
    Vec2 end = transitionNode->getTransitionEnd();
    CollisionCell * cell = world->getCollisionGrid(end)->getCellForCoordinates(end);
    
    PathNode * node = new PathNode(end, cell, _team, false);
    intermediatePath.pushBack(node);
    _pathNodes.pushBack(intermediatePath);
    node->release();
}

PathStatus Path::getPathStatus()
{
    bool goToNextRoutePoint = true;
    if(_pathNodes.size() > 0)
    {
        if(!world->getNavGrid(_pathNodes.front()->getPosition())->patchIdValid(_pathNodes.front()->getPatchId()))
        {
            _pathNodes.clear();
            goToNextRoutePoint = false;
        }
        else
        {
            return PATH_STATUS_READY;
        }
    }
    
    if(goToNextRoutePoint)
    {
        if(_currentRouteNode != nullptr)
        {
            _currentRouteNode->release();
            _currentRouteNode = nullptr;
        }
        if(_routeNodes.size() <= 0)
        {
            return PATH_STATUS_DONE;
        }

        _currentRouteNode = _routeNodes.front();
        _currentRouteNode->retain();
        _routeNodes.popFront();
    }
    
    return PATH_STATUS_NEEDS_NEXT_SEGMENT;
}

void Path::buildNextPathSegment(Vec2 currentPosition, Vec2 * modifiedPosition)
{
    //repath, need to determine what to repath
    if(_currentRouteNode->getRouteType() == ROUTE_TYPE_SECTOR)
    {
        SectorRouteNode * route = (SectorRouteNode *) _currentRouteNode;
        getPathToSectorRouteNode(currentPosition, route, modifiedPosition);

    }
    //repath, need to determine what to repath to
    else if(_currentRouteNode->getRouteType() == ROUTE_TYPE_TRANSITION)
    {
        TransitionRouteNode * transition = (TransitionRouteNode *) _currentRouteNode;
        if(modifiedPosition != nullptr)
        {
            modifiedPosition->x = transition->getTransitionStart().x;
            modifiedPosition->y = transition->getTransitionStart().y;
        }
        getPathToTransitionNode(currentPosition, transition);
    }
    //repath, need to determine what to repath to
    else if(_currentRouteNode->getRouteType() == ROUTE_TYPE_POSITION)
    {
        RouteNode * path = (RouteNode *) _currentRouteNode;
        if(modifiedPosition != nullptr)
        {
            modifiedPosition->x = path->getPosition().x;
            modifiedPosition->y = path->getPosition().y;
        }

        globalPathFinding->getPath(currentPosition, path->getPosition(), &_pathNodes, _movePreference, _team, _debug && globalTestBed->pathTesting);
    }
}

PathNode * Path::getNextPathNode()
{
    if(_pathNodes.size() > 0)
    {
        return _pathNodes.front();
    }
    return nullptr;
}

Vec2 Path::getNextPathPosition()
{
    Vec2 position;
    if(_pathNodes.size() > 0)
    {
        position = _pathNodes.front()->getPosition();
        _pathNodes.popFront();
        return position;
    }
}

void Path::clear()
{
    if(_currentRouteNode != nullptr)
    {
        _currentRouteNode->release();
    }
    _currentRouteNode = nullptr;
    _pathNodes.clear();
    _routeNodes.clear();
}
