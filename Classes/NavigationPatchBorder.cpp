//
//  PatchNeighbor.cpp
//  TrenchWars
//
//  Created by Paul Reed on 3/10/20.
//

#include "NavigationPatchBorder.h"
#include "EngagementManager.h"
#include "PathFinding.h"
#include <algorithm>
#include "TestBed.h"
#include "GameVariableStore.h"

NavigationPatchBorder::NavigationPatchBorder(int min, int max, int perpendicular, DIRECTION direction, NavigationPatch * owner, NavigationPatch * neighbor)
{
    _neighboringPatch = neighbor;
    _owningPatch = owner;
    
    _direction = direction;
    _borderIndexMin = min;
    _borderIndexMax = max;
    _borderIndexPerpendicular = perpendicular;
    
    double difference = max - min;
    if(difference < 1)
    {
        difference = 1.0;
    }
    
    for(int i = 0; i < 10; i++)
    {
        _congestionOnBorder[i] = 1.0;
    }
    _congestionPerPerson = (1.0 / difference) * globalVariableStore->getVariable(PathingCongestionCostMultiplier);
}

NavigationPatchBorder::~NavigationPatchBorder()
{
}

CollisionCell * NavigationPatchBorder::borderCellNearestCoordinates(int x, int y)
{
    
    int indexAlongBorder;
    if(_direction == LEFT || _direction == RIGHT)
    {
        if(y >= _borderIndexMax)
        {
            indexAlongBorder = _borderIndexMax;
        }
        else if(y <= _borderIndexMin)
        {
            indexAlongBorder = _borderIndexMin;
        }
        else
        {
            indexAlongBorder = y;
        }
        
        return _owningPatch->getCollisionGrid()->getCell(_borderIndexPerpendicular, indexAlongBorder);
    }
    else
    {
        if(x >= _borderIndexMax)
        {
            indexAlongBorder = _borderIndexMax;
        }
        else if(x <= _borderIndexMin)
        {
            indexAlongBorder = _borderIndexMin;
        }
        else
        {
            indexAlongBorder = x;
        }
        return _owningPatch->getCollisionGrid()->getCell(indexAlongBorder, _borderIndexPerpendicular);
    }
}
CollisionCell * NavigationPatchBorder::borderCellNearestCell(CollisionCell * cell)
{
    return borderCellNearestCoordinates(cell->getXIndex(), cell->getYIndex());
}

CollisionCell * NavigationPatchBorder::getBorderMinimumCell()
{
    if(_direction == LEFT || _direction == RIGHT)
    {
        return _owningPatch->getCollisionGrid()->getCell(_borderIndexPerpendicular, _borderIndexMin);
    } else {
        return _owningPatch->getCollisionGrid()->getCell(_borderIndexMin, _borderIndexPerpendicular);
    }
}

CollisionCell * NavigationPatchBorder::getBorderMaximumCell()
{
    if(_direction == LEFT || _direction == RIGHT)
    {
        return _owningPatch->getCollisionGrid()->getCell(_borderIndexPerpendicular, _borderIndexMax);
    } else {
        return _owningPatch->getCollisionGrid()->getCell(_borderIndexMax, _borderIndexPerpendicular);
    }
}

CollisionCell * NavigationPatchBorder::getBorderMidpointCell()
{
    if(getBorderSize() < 3)
    {
        return getBorderMinimumCell();
    }
    int mid = ((_borderIndexMax - _borderIndexMin) / 2) + _borderIndexMin;
    if(_direction == LEFT || _direction == RIGHT)
    {
        return _owningPatch->getCollisionGrid()->getCell(_borderIndexPerpendicular, mid);
    } else {
        return _owningPatch->getCollisionGrid()->getCell(mid, _borderIndexPerpendicular);
    }
}

CollisionCell * NavigationPatchBorder::getBorderCellNearestLine(CollisionCell * lineStart, CollisionCell * lineEnd)
{
    if(lineStart == lineEnd) {
        return borderCellNearestCell(lineStart);
    }
    
    double xDiff = lineEnd->getXIndex() - lineStart->getXIndex();
    double yDiff = lineEnd->getYIndex() - lineStart->getYIndex();

    double percentOfLine;
    
    if(_direction == LEFT || _direction == RIGHT) {
        percentOfLine = xDiff == 0 ? 0 : (_borderIndexPerpendicular - lineStart->getXIndex()) / xDiff;
    } else {
        percentOfLine = yDiff == 0 ? 0 : (_borderIndexPerpendicular - lineStart->getYIndex()) / yDiff;
    }
    
    int xIntersection = (xDiff * percentOfLine) + lineStart->getXIndex();
    int yIntersection = (yDiff * percentOfLine) + lineStart->getYIndex();
    
    return borderCellNearestCoordinates(xIntersection, yIntersection);
}

CollisionCell * NavigationPatchBorder::getBorderCellNearestNeighbor(NavigationPatchBorder * neighbor)
{
    return getBorderCellNearestLine(neighbor->getBorderMinimumCell(), neighbor->getBorderMaximumCell());
}

CollisionCell * NavigationPatchBorder::getBorderCellAtIndex(int index)
{
    if(index < _borderIndexMin || index > _borderIndexMax) {
        return nullptr;
    }
    if(_direction == LEFT || _direction == RIGHT)
    {
        return _owningPatch->getCollisionGrid()->getCell(_borderIndexPerpendicular, index);
    }
    else
    {
        return _owningPatch->getCollisionGrid()->getCell(index, _borderIndexPerpendicular);
    }

}

CollisionCell * NavigationPatchBorder::getBorderCellWithLeastConjestion(CollisionCell * idealCell, int team)
{
    if(!coordinateOnBorder(idealCell->getXIndex(), idealCell->getYIndex())) {
        return nullptr;
    }
    int startIndex = idealCell->getXIndex();
    if(_direction == LEFT || _direction == RIGHT)
    {
        startIndex = idealCell->getYIndex();
    }
    
    double bestValue = idealCell->getCongestion(team);
    double startValue = bestValue;

    // If the ideal cell already has low conjestion, just use it
    if(bestValue == 0) {
        return idealCell;
    }
    
    //Starting at the index of the ideal cell, walk both directions along the border looking for the least conjested cell
    // Cells further from the start cell position are less ideal, and thus will have a higher cost
    int count = _borderIndexMax - _borderIndexMin + 1;
    double value;
    CollisionCell * bestCell = idealCell;
    CollisionCell * nextCell;
    for(int distanceFromIdeal = 1; distanceFromIdeal < count; distanceFromIdeal++) {
        nextCell = getBorderCellAtIndex(startIndex + distanceFromIdeal);
        if(nextCell != nullptr) {
            value = nextCell->getCongestion(team) + distanceFromIdeal;
            if(value < bestValue) {
                bestCell = nextCell;
                bestValue = value;
            }
        }
        nextCell = getBorderCellAtIndex(startIndex - distanceFromIdeal);
        if(nextCell != nullptr) {
            value = nextCell->getCongestion(team) + distanceFromIdeal;
            if(value < bestValue) {
                bestCell = nextCell;
                bestValue = value;
            }
        }
        // If at any point we find a cell with no congestion then just use that
        if(bestValue < startValue) {
            break;
        }
    }
        
    return bestCell;
}

NavigationPatchBorder * NavigationPatchBorder::getOppositeBorder()
{
    return _neighboringPatch->neighborForPatch(_owningPatch);
}

bool NavigationPatchBorder::lineCrossesBorder(CollisionCell * lineStart, CollisionCell * lineEnd)
{
    if(lineStart == nullptr || lineEnd == nullptr)
    {
        return false;
    }
    double xDiff = lineEnd->getXIndex() - lineStart->getXIndex();
    double yDiff = lineEnd->getYIndex() - lineStart->getYIndex();
    double percentOfLine;
    int intersection;
    
    if(_direction == LEFT || _direction == RIGHT) {
        percentOfLine = (_borderIndexPerpendicular - lineStart->getXIndex()) / xDiff;
        intersection = (yDiff * percentOfLine) + lineStart->getYIndex();
        return coordinateOnBorder(_borderIndexPerpendicular, intersection);

    } else {
        percentOfLine = (_borderIndexPerpendicular - lineStart->getYIndex()) / yDiff;
        intersection = (xDiff * percentOfLine) + lineStart->getXIndex();
        return coordinateOnBorder(intersection, _borderIndexPerpendicular);
    }
}

bool NavigationPatchBorder::coordinateOnBorder(int x, int y)
{
     if(_direction == LEFT || _direction == RIGHT)
     {
         if(y >= _borderIndexMin && y <= _borderIndexMax && x == _borderIndexPerpendicular)
         {
             return true;
         }
     }
    else
    {
        if(x >= _borderIndexMin && x <= _borderIndexMax && y == _borderIndexPerpendicular)
        {
            return true;
        }
    }
    return false;
}

bool NavigationPatchBorder::cellOnBorder(CollisionCell * cell)
{
    return coordinateOnBorder(cell->getXIndex(), cell->getYIndex());
}

bool NavigationPatchBorder::isOtherBorderParallel(NavigationPatchBorder * otherBorder)
{
    if(_direction == otherBorder->_direction)
    {
        return true;
    }
    else if(_direction == RIGHT)
    {
        if(otherBorder->_direction == LEFT)
        {
            return true;
        }
    }
    else if(_direction == LEFT)
    {
        if(otherBorder->_direction == RIGHT)
        {
            return true;
        }
    }
    else if(_direction == UP)
    {
        if(otherBorder->_direction == DOWN)
        {
            return true;
        }
    }
    else if(_direction == DOWN)
    {
        if(otherBorder->_direction == UP)
        {
            return true;
        }
    }
    return false;
}

double NavigationPatchBorder::getBorderDirectionAsAngle()
{
    switch(_direction)
    {
        case RIGHT: {
            return 0;
        }
        case UP: {
            return M_PI_2;
        }
        case LEFT: {
            return M_PI;
        }
        case DOWN: {
            return -M_PI_2;
        }
        default:
            return 0;
    }
}


void NavigationPatchBorder::addCongestion(int team)
{
    _congestionOnBorder[team] += _congestionPerPerson;
}

void NavigationPatchBorder::removeCongestion(int team)
{
    _congestionOnBorder[team] -= _congestionPerPerson;
    if (_congestionOnBorder[team] < 1.00)
    {
        _congestionOnBorder[team] = 1.00;
    }
}

double NavigationPatchBorder::getCongestionForTeam(int team)
{
    return _congestionOnBorder[team];
}


void NavigationPatchBorder::remove()
{

}
