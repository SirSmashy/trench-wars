//
//  Package.hpp
//  TrenchWars
//
//  Created by Paul Reed on 5/3/23.
//

#ifndef Package_h
#define Package_h

#include "Refs.h"
#include <cereal/archives/binary.hpp>

enum PACKAGE_TYPE
{
    ORDER_PACKAGE,
    AMMO_PACKAGE
};

class CommandPost;
class Order;
class Unit;
class Command;

class Package : public ComparableRef
{
protected:
    Command * _owningCommand;
public:
    virtual void deliverPackage() = 0;
    virtual bool deliveryComplete() = 0;
    virtual Vec2 getDestination() = 0;
    virtual CommandPost * getOrigin() = 0;
    virtual PACKAGE_TYPE getPackageType() = 0;
    virtual void packageLost() {};
    Command * getOwningCommand() {return _owningCommand;}
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive) = 0;
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const = 0;

};

class OrderPackage : public Package
{
    Order * _ordersToDelivery;
    Unit * _destination;
    CommandPost * _origin;
    
    OrderPackage();
    OrderPackage(CommandPost * origin, Unit * destination, Order * orders, Command * owningCommand);
public:
    
    static OrderPackage * create(CommandPost * origin, Unit * destination, Order * orders, Command * owningCommand);
    static OrderPackage * create();

    ~OrderPackage();
    
    virtual void deliverPackage();
    virtual Vec2 getDestination();
    virtual bool deliveryComplete() {return true;};
    virtual CommandPost * getOrigin() {return _origin;}
    
    Unit * getUnit() {return _destination;}
    Order * getOrder() {return _ordersToDelivery;}
    
    PACKAGE_TYPE getPackageType() {return ORDER_PACKAGE;}
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
    
    virtual void packageLost();
};

class AmmoPackage : public Package
{
protected:
    CommandPost * _destination;
    CommandPost * _origin;
    int _ammoAmount;
    bool _ammoPickedup;
    bool _ammoDelivered;
    
    AmmoPackage(CommandPost * origin, CommandPost * destination, int ammo, bool needToRetrieve, Command * owningCommand);
    AmmoPackage();
public:
    
    static AmmoPackage * create(CommandPost * origin, CommandPost * destination, int ammo, bool needToRetrieve, Command * owningCommand);
    static AmmoPackage * create();
    
    virtual void deliverPackage();
    virtual bool deliveryComplete() {return _ammoDelivered;}
    virtual Vec2 getDestination();
    virtual CommandPost * getOrigin() {return _origin;}
    bool wasAmmoPickedUp() {return _ammoPickedup;}
    
    int getAmmoAmount() {return _ammoAmount;}
    virtual CommandPost * getAmmoDestination() {return _destination;}
    PACKAGE_TYPE getPackageType() {return AMMO_PACKAGE;}
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

#endif /* Package_h */
