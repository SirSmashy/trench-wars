#include "MainMenuScene.h"
#include "SimpleAudioEngine.h"
#include "EntityFactory.h"
#include "Timing.h"
#include "VectorMath.h"
#include "Weapon.h"
#include <cereal.hpp>
#include <cereal/archives/binary.hpp>
#include <sstream>
#include "ScenarioFactory.h"
#include "TestingScene.h"
#include "TrenchWarsManager.h"
#include "LobbyScene.h"

USING_NS_CC;


Scene* MainMenuScene::createScene()
{
    return MainMenuScene::create();
}

// -----------------------------------------------------------------------
bool MainMenuScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    // Create a colored background (Dark Grey)
    LayerColor * backgroundColor = LayerColor::create(Color4B::GRAY, visibleSize.width, visibleSize.height);
    this->addChild(backgroundColor);
    
    
    // Game Label
    auto label = Label::createWithTTF("Trench Wars", "arial.ttf", 36);
    label->setPositionNormalized(Vec2(0.5,0.65));
    label->setColor(Color3B::RED);
    this->addChild(label);
    
    auto hostButton = MenuItemLabel::create( Label::createWithTTF("Host Multiplayer Game","arial.ttf", 18),CC_CALLBACK_1(MainMenuScene::hostGameCallback,this));
    hostButton->setPositionNormalized(Vec2(.5,.55));
    
    
    auto joinButton = MenuItemLabel::create(Label::createWithTTF("Join Multiplayer Game","arial.ttf", 18),CC_CALLBACK_1(MainMenuScene::joinGameCallback,this));
    joinButton->setPositionNormalized(Vec2(.5,.50));
    
    auto startButton = MenuItemLabel::create(Label::createWithTTF("Start Single Player Game","arial.ttf", 18),CC_CALLBACK_1(MainMenuScene::startSinglePlayerCallback,this));
    startButton->setPositionNormalized(Vec2(.5,.45));
    
    
    // Save Button
    auto saveButton = MenuItemLabel::create(Label::createWithTTF("Load Save Game","arial.ttf", 18),CC_CALLBACK_1(MainMenuScene::showSavesButtonCallback,this));
    saveButton->setPositionNormalized(Vec2(.5,.40));
    
    // Playground Button
    auto playgroundButton = MenuItemLabel::create(
                        Label::createWithTTF("Playground","arial.ttf", 18),
                        CC_CALLBACK_1(MainMenuScene::startPlaygroundCallback,this));
    playgroundButton->setPositionNormalized(Vec2(.5,.35));
    
    // Test Button
    auto testButton = MenuItemLabel::create(
                        Label::createWithTTF("Tests","arial.ttf", 18),
                        CC_CALLBACK_1(MainMenuScene::testCallback,this));
    testButton->setPositionNormalized(Vec2(.5,.30));
    
    
    // close Button
    auto closeButton = MenuItemLabel::create(
                        Label::createWithTTF("Close","arial.ttf", 18),
                        CC_CALLBACK_1(MainMenuScene::menuCloseCallback,this));
    
    closeButton->setPositionNormalized(Vec2(.5,.15));
    
    mainMenu = Menu::create(hostButton, joinButton, startButton,saveButton,playgroundButton,testButton, closeButton, NULL);
    mainMenu->setPosition(Vec2::ZERO);
    this->addChild(mainMenu);
    createSaveMenu();
    return true;
}

void MainMenuScene::createSaveMenu()
{
    saveGameMenu = Menu::create(NULL);
    
    auto saveList = FileUtils::getInstance()->listFiles("/Users/paulreed/Desktop");
    float y = 0.3;
    for(auto saveName : saveList)
    {
        if(FileUtils::getInstance()->getFileExtension(saveName) == ".tws")
        {
            auto saveButton = MenuItemLabel::create( Label::createWithTTF(saveName,"arial.ttf", 18),CC_CALLBACK_1(MainMenuScene::startSaveCallback,this));
            saveButton->setPositionNormalized(Vec2(.5,y));
            saveGameMenu->addChild(saveButton);
            y += .10;
        }
    }
    auto backButton = MenuItemLabel::create( Label::createWithTTF("back","arial.ttf", 18),CC_CALLBACK_1(MainMenuScene::backButtonCallback,this));
    backButton->setPositionNormalized(Vec2(.4,.2));
    saveGameMenu->addChild(backButton);
    saveGameMenu->setPosition(Vec2::ZERO);
    saveGameMenu->setVisible(false);
    this->addChild(saveGameMenu);
}

void MainMenuScene::showSavesButtonCallback(cocos2d::Ref* pSender)
{
    mainMenu->setVisible(false);
    saveGameMenu->setVisible(true);
}

void MainMenuScene::backButtonCallback(cocos2d::Ref* pSender)
{
    mainMenu->setVisible(true);
    saveGameMenu->setVisible(false);
}

void MainMenuScene::hostGameCallback(cocos2d::Ref* pSender)
{
    globalTrenchWarsManager->startLobby(NETWORK_MODE_SERVER);
}

void MainMenuScene::joinGameCallback(cocos2d::Ref* pSender)
{
    globalTrenchWarsManager->startLobby(NETWORK_MODE_CLIENT);
}

void MainMenuScene::startSinglePlayerCallback(Ref* pSender)
{
    globalTrenchWarsManager->startLobby(NETWORK_MODE_SINGLE);
}

void MainMenuScene::startSaveCallback(cocos2d::Ref* pSender)
{
    MenuItemLabel * label = (MenuItemLabel *) pSender;
    auto name = label->getString();
    globalTrenchWarsManager->startEngagementFromSaveGame(name);
}

void MainMenuScene::startPlaygroundCallback(cocos2d::Ref* pSender)
{
    auto scenario = globalScenarioFactory->getScenario("EmptyPlayground");
    globalTrenchWarsManager->getLocalPlayer()->teamId = scenario->teams.at(0)->teamId;
    globalTrenchWarsManager->getLocalPlayer()->commandId = scenario->teams.at(0)->commands.at(0)->commandId;
    scenario->teams.at(0)->commands.at(0)->controllingPlayerId = 0;
    scenario->teams.at(0)->commands.at(0)->aiCommand = 0;
    globalTrenchWarsManager->startEngagementFromScenario(scenario);
}


void MainMenuScene::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
    
    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/
    
    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
    
}

void MainMenuScene::testCallback(cocos2d::Ref* pSender)
{
    Director::getInstance()->replaceScene(TransitionFade::create(0.5, TestingScene::create(), Color3B(0,255,255)));

}

