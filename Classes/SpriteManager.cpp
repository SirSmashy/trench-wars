//
//  CocosObjectManager.cpp
//  TrenchWars
//
//  Created by Paul Reed on 6/19/23.
//

#include "CocosObjectManager.h"

CocosObjectManager * globalCocosObjectManager;

CocosObjectManager::CocosObjectManager()
{
    globalCocosObjectManager = this;
}


void CocosObjectManager::requestSpriteCreation(CocosProxy * proxy)
{
    _spriteMutex.lock();
    _proxiesToCreate.pushBack(proxy);
    _spriteMutex.unlock();

}

void CocosObjectManager::requestSpriteUpdate(CocosProxy * proxy)
{
    _spriteMutex.lock();
    _spritesToUpdate.insert(proxy);
    _spriteMutex.unlock();

}

void CocosObjectManager::requestSpriteRemoval(CocosProxy * proxy)
{
    _spriteMutex.lock();
    _proxiesToRemove.pushBack(proxy);
    _spriteMutex.unlock();

}


void CocosObjectManager::updateSprites()
{
    AnimatedSprite * sprite;
    for(auto proxy : _proxiesToCreate)
    {
        proxy->createCocosObject();
        _spriteProxies.insert(proxy->uID(), proxy);
    }
    _proxiesToCreate.clear();
    
    for(auto proxy : _proxiesToRemove)
    {
        _spriteProxies.erase(proxy->uID());
        _spritesToUpdate.erase(proxy);
    }
    _proxiesToRemove.clear();
    
    std::vector<CocosProxy *> stopUpdating;
    stopUpdating.reserve(_spritesToUpdate.size());
    
    for(auto sprite : _spritesToUpdate)
    {
        if(!sprite->updateCocosObject())
        {
            stopUpdating.push_back(sprite);
        }
    }
    
    for(auto sprite : stopUpdating)
    {
        _spritesToUpdate.erase(sprite);
    }
}
