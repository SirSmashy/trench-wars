//
//  CrewWeaponUnit.hpp
//  TrenchWars
//
//  Created by Paul Reed on 1/22/22.
//

#ifndef CrewWeaponUnit_h
#define CrewWeaponUnit_h

#include "Unit.h"
#include "CrewWeapon.h"

class CrewWeaponUnit : public Unit
{
protected:
    CrewWeapon * _crewWeapon;
    Infantry * _weaponOwner;
    int _maxUnitAmmo;
    int _currentUnitAmmo;
    
    // more later
    CC_CONSTRUCTOR_ACCESS :
    
    CrewWeaponUnit();
    virtual bool init(UnitDefinition * definition, Vec2 position, Command * command);
    
    virtual void doWorkOrder(WorkOrder * work);
    virtual void doMoveOrder(MoveOrder * move);
    virtual void doAttackPositionOrder(AttackPositionOrder * attack);
    virtual void clearOrder();
    virtual void handleMemberRemoval(Infantry * member);

    
public:
    static CrewWeaponUnit * create(UnitDefinition * definition, Vec2 position, Command * command);
    static CrewWeaponUnit * create();
    
    virtual void postLoad();
    virtual UNIT_TYPE getUnitType() {return WEAPON_UNIT;}
    virtual bool canBombard() {return true;}
    virtual bool positionValidForBombardment(Vec2 position);
    virtual int getUnitAmmo(int requestedAmount);

    virtual void crewWeaponNeedsWork(bool helpReloading);
    bool isIndirectFireWeapon();
    
    virtual void removeFromGame();
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

#endif /* CrewWeaponUnit_h */
