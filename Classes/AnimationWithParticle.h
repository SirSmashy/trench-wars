//
//  AnimationWithParticle.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/18/20.
//

#ifndef AnimationWithParticle_h
#define AnimationWithParticle_h

#include "cocos2d.h"
#include "EntityFactory.h"
#include "AutoSizeAnimation.h"

USING_NS_CC;

class AnimationWithParticle : public AutoSizeAnimation
{
private:
    std::string _projectileName;
    EntityAnimation * _entityAnimation;
    
    AnimationWithParticle();
    bool init(EntityAnimation *animation, Size size);
    
public:
    static AnimationWithParticle* create(EntityAnimation *animation, Size size);
    virtual void update(float t) override;
};

#endif /* AnimationWithParticle_h */
