//
//  TesterEmblem.hpp
//  TrenchWars
//
//  Created by Paul Reed on 12/10/22.
//

#ifndef TesterEmblem_h
#define TesterEmblem_h


#include "Emblem.h"
#include "Tester.h"

class TesterEmblem : public Emblem
{
private:
    Tester * _tester;
    bool _updateOnDragStart;
    
    virtual bool init(Tester * tester, Vec2 position, bool updateOnDragStart);
    
    
public:
    static TesterEmblem * create(Tester * tester, Vec2 position, bool updateOnDragStart);
    
    virtual void emblemDragged(Vec2 position);
    virtual void emblemDragEnded(Vec2 position);
    
    virtual void emblemHoverEnter(Vec2 position);
    virtual void emblemHoverLeave(Vec2 position);
    
    
    void onHover();
};

#endif /* TesterEmblem_h */
