//
//  MapLoader.h
//  TrenchWars
//
//  Created by Paul Reed on 8/24/22.
//

#ifndef MapLoader_h
#define MapLoader_h

#include <unordered_set>
#include "cocos2d.h"
#include "Refs.h"
#include "CollisionCell.h"
#include "NavigationPatchRect.h"
#include "VectorMath.h"
#include "LineDrawer.h"


USING_NS_CC;



struct MapArea : public ComparableRef
{
    MapArea();
    ~MapArea();
    
    Vec2 minimumCellIndex;
    int minX = 999999999;
    int maxX = 0;
    int minY = 99999999;
    int maxY = 0;
    
    std::unordered_set<Vec2, Vec2Hash, Vec2Compare> cellIndicesInArea;
    size_t terrainType;
    
    void addPosition(Vec2 position);
    void printArea();
};

struct CombinationArea : public Ref
{
    std::vector<MapArea *> areasToCombine;
};

class MapLoader : public Ref
{
    std::unordered_set<MapArea *> _mapAreas;
    std::vector<std::vector<MapArea *>> _pixelToAreaMap;
    std::vector<NavigationPatchRect> _navigationRects;
    std::mutex _navPatchesMutex;
    std::mutex _candidatesMutex;
    std::vector<std::vector<size_t>> _mapData;
    Vec2 _mapWorldOrigin;
    Vec2 _mapVisualPositionOffset;
    bool _createStaticEntities;
    
    int _mapWidth;
    int _mapHeight;
    
    void createMapAreasForSection(int yStart, int yEnd);
    void createOrUpdateMapArea(int yIndex, int xStart, int xEnd, size_t terrain, MapArea * currentMapArea, std::vector<MapArea *> & previousRow);
    MapArea * mergeMapAreas(MapArea * areaOne, MapArea * areaTwo, int currentAreaStartIndex, int currentX, std::vector<MapArea *> * previousRow);
    
    bool patchValid(int minX, int maxX, int minY, int maxY);
    void buildNavigationPatch(Vec2 & start, MapArea * area, TerrainCharacteristics & terrain, bool print);
public:
    
    MapLoader(Image * map, bool createStaticEntities);
    MapLoader(std::vector<std::vector<size_t>> & mapData, Vec2 mapWorldOrigin, Vec2 mapVisualPositionOffset);
    
    void parseImageFile();
    void createMapAreas();
    void createPolygonLandscapeObjects();
    void createNavigationPatches();
    
    const std::vector<NavigationPatchRect> & getNavigationRects() {return _navigationRects;}
};

#endif /* MapLoader_h */
