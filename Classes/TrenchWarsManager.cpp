//
//  TrenchWarsManager.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/12/23.
//

#include "TrenchWarsManager.h"
#include "NetworkInterface.h"
#include "GameVariableStore.h"
#include "PlayerVariableStore.h"
#include "ScenarioFactory.h"
#include "StaticEntity.h"
#include "ColorToTerrainTypeStore.h"
#include "EntityFactory.h"
#include "NetworkInterface.h"
#include "ServerEngagementManager.h"
#include "ClientEngagementManager.h"
#include "LobbyManager.h"
#include "MainMenuScene.h"
#include "ServerLobbyManager.h"
#include "ClientLobbyManager.h"
#include "BackgroundTaskHandler.h"
#include "MultithreadedAutoReleasePool.h"
#include "NetworkServer.h"
#include "NetworkClient.h"

TrenchWarsManager * globalTrenchWarsManager;

TrenchWarsManager::TrenchWarsManager(bool hasLocalPlayer)
{
    RandomNumbers * rands = RandomNumbers::create();
    rands->retain();
    _nextPlayerId = 0;
    _currentNetworkMode = NETWORK_MODE_NONE;
    globalTrenchWarsManager = this;
    _activeStateManager = nullptr;
    
    GlobalVariableStore * globalVariableStore = new GlobalVariableStore();
    PlayerVariableStore * playerVariableStore = new PlayerVariableStore();
    
    globalVariableStore->parseGlobalVariableFile("GlobalVariables.xml");
    playerVariableStore->parsePlayerVariableFile("PlayerVariables.xml");
    
    ScenarioFactory * scenarioLoader = new ScenarioFactory();
    scenarioLoader->parseScenarioFile("Scenarios.xml");
    
    ColorToTerrainTypeStore * terrain = new ColorToTerrainTypeStore();
    EntityFactory * entityFactory = new EntityFactory();
    
    int maxThreadCount = std::thread::hardware_concurrency() -1;
    maxThreadCount = maxThreadCount < 1 ? 1 : maxThreadCount;
    
    MultithreadedAutoReleasePool * releasePool = new MultithreadedAutoReleasePool(); // create the global release pool
    BackgroundTaskHandler * backgroundTaskHandler = new BackgroundTaskHandler(maxThreadCount);
    
    _gameState = GAME_STATE_NONE;
    _hasLocalPlayer = hasLocalPlayer;
    if(_hasLocalPlayer)
    {
        Player * newPlayer = new Player();
        newPlayer->playerId = _nextPlayerId++;
        newPlayer->remotePlayer = false;
        
        // TEST ONLY
        char * randomName = new char[10];
        for(int i = 0; i < 10; i++)
        {
            randomName[i] = (char) globalRandom->randomInt(65, 90);
        }
        newPlayer->playerName = std::string(randomName);
        LOG_INFO("Player name is %s \n", newPlayer->playerName.c_str());
        // END TEST ONLY
        
        _playerIdToPlayerMap.emplace(newPlayer->playerId, newPlayer);
        if(_activeStateManager != nullptr)
        {
            _activeStateManager->playerJoinedGame(newPlayer); // I don't think this is ever called
        }
    }
    
    Director * director = Director::getInstance();
    Scheduler * scheduler = director->getScheduler();
    scheduler->schedule([=] (float dt) { update(dt); }, this, 1.0 / 30.0, false, "game");

}

void TrenchWarsManager::update(float deltaTime)
{
    if(_activeStateManager != nullptr)
    {
        _activeStateManager->update(deltaTime);
        globalAutoReleasePool->clearPool();
    }
}


void TrenchWarsManager::setNetworkMode(NETWORK_MODE mode)
{
    if(_currentNetworkMode == NETWORK_MODE_SERVER || _currentNetworkMode == NETWORK_MODE_CLIENT)
    {
        shutdownNetwork();
        delete globalNetworkInterface;
        //probably some other stuff here
    }
    _currentNetworkMode = mode;
    if(_currentNetworkMode == NETWORK_MODE_SERVER)
    {
        NetworkServer * globalNetworkServer = new NetworkServer();
        globalNetworkServer->startListening();
    }
    else if(_currentNetworkMode == NETWORK_MODE_CLIENT)
    {
        NetworkClient * globalClient = new NetworkClient();
    }
}

void TrenchWarsManager::startLobby(NETWORK_MODE networkMode)
{
    setNetworkMode(networkMode);
    if(_gameState == GAME_STATE_NONE)
    {
        _gameState = GAME_STATE_IN_LOBBY;
    }
    if(networkMode == NETWORK_MODE_SERVER || networkMode == NETWORK_MODE_SINGLE)
    {
        _activeStateManager = ServerLobbyManager::create();
        _activeStateManager->retain();
    }
    else if(networkMode == NETWORK_MODE_CLIENT)
    {
        _activeStateManager = ClientLobbyManager::create();
        _activeStateManager->retain();
    }
}

void TrenchWarsManager::joinServer(const std::string & ipAddr)
{
    setNetworkMode(NETWORK_MODE_SINGLE);
    globalNetworkInterface->connectToServer(ipAddr);
}

void TrenchWarsManager::exitToMainMenu()
{
    setNetworkMode(NETWORK_MODE_NONE);
    if(_activeStateManager != nullptr)
    {
        _activeStateManager->shutdown();
        _activeStateManager->release();
        _activeStateManager = nullptr;
    }
    _gameState = GAME_STATE_NONE;
    
    Director::getInstance()->replaceScene(TransitionFade::create(0.5, MainMenuScene::createScene(), Color3B(0,255,255)));
}


void TrenchWarsManager::startEngagementFromScenario(ScenarioDefinition * scenario)
{
    LOG_INFO("Start game! \n");
    GameStateManager * oldManager = _activeStateManager;
    ServerEngagementManager * engagement = ServerEngagementManager::createFromScenario(scenario, NETWORK_MODE_SERVER);
    engagement->retain();
    engagement->unPauseGame();
    _activeStateManager = engagement;
    
    if(oldManager != nullptr)
    {
        oldManager->shutdown();
        oldManager->release();
    }
}

void TrenchWarsManager::startEngagementAsClient(const unsigned char * mapData, size_t mapDataSize, Vector<TeamDefinition *> & teams)
{
    GameStateManager * oldManager = _activeStateManager;

    ClientEngagementManager * engagement = ClientEngagementManager::create(mapData, mapDataSize, teams);
    engagement->retain();
    engagement->unPauseGame();
    _activeStateManager = engagement;
    if(oldManager != nullptr)
    {
        oldManager->shutdown();
        oldManager->release();
    }
}

void TrenchWarsManager::startEngagementFromSaveGame(const std::string & saveFile)
{
    GameStateManager * oldManager = _activeStateManager;
    ServerEngagementManager * engagement = ServerEngagementManager::createFromSaveGame(saveFile);
    engagement->retain();
    engagement->unPauseGame();
    _activeStateManager = engagement;
    if(oldManager != nullptr)
    {
        oldManager->shutdown();
        oldManager->release();
    }
}

int TrenchWarsManager::remotePlayerJoinedGame(int playerId)
{
    Player * newPlayer = new Player();
    if(playerId != -1)
    {
        newPlayer->playerId = playerId;
    }
    else
    {
        newPlayer->playerId = _nextPlayerId++;
    }
    newPlayer->remotePlayer = true;
    _playerIdToPlayerMap.emplace(newPlayer->playerId, newPlayer);
    if(_activeStateManager != nullptr)
    {
        _activeStateManager->playerJoinedGame(newPlayer);
    }
    return newPlayer->playerId;
}

void TrenchWarsManager::playerLeftGame(int playerId)
{
    if(_playerIdToPlayerMap.find(playerId) != _playerIdToPlayerMap.end())
    {
        if(_activeStateManager != nullptr)
        {
            _activeStateManager->playerLeftGame(playerId);
        }
        Player * remove = _playerIdToPlayerMap[playerId];
        _playerIdToPlayerMap.erase(playerId);
        delete remove;
    }
}

Player * TrenchWarsManager::getPlayerForPlayerId(int playerId)
{
    if(_playerIdToPlayerMap.find(playerId) == _playerIdToPlayerMap.end())
    {
        return nullptr;
    }
    return _playerIdToPlayerMap[playerId];
}

void TrenchWarsManager::playerSetName(int playerId, const std::string & name)
{
    if(_playerIdToPlayerMap.find(playerId) == _playerIdToPlayerMap.end())
    {
        LOG_ERROR("Error: Cannot set name, player %d does not exist \n", playerId);
        return;
    }
    LOG_INFO("Player %d has name: %s\n",playerId, name.c_str());
    _playerIdToPlayerMap[playerId]->playerName = name;
}

void TrenchWarsManager::setLocalPlayerId(int playerId)
{
    Player * local = getLocalPlayer();
    _playerIdToPlayerMap.erase(local->playerId);
    local->playerId = playerId;
    _playerIdToPlayerMap.emplace(local->playerId, local);
    LOG_INFO("Received remote ID %d \n", local->playerId);
}

Player * TrenchWarsManager::getLocalPlayer()
{
    if(! _hasLocalPlayer)
    {
        return nullptr;
    }
    for(auto pair : _playerIdToPlayerMap)
    {
        if(pair.second->remotePlayer == false)
        {
            return pair.second;
        }
    }
    return nullptr;
}

int TrenchWarsManager::getLocalPlayerId()
{
    Player * local = getLocalPlayer();
    if(local != nullptr)
    {
        return local->playerId;
    }
    return -1;
}



void TrenchWarsManager::receivedNetworkMessage(GameMessage * message)
{
    if(message->getPlayerId() != -1 && _playerIdToPlayerMap.find(message->getPlayerId()) == _playerIdToPlayerMap.end())
    {
        LOG_WARNING("Unknown player!!!! \n");
        return;
    }
    
    MESSAGE_TYPE type = (MESSAGE_TYPE) message->getHeader()->messageType;
    _activeStateManager->parseNetworkMessage(message);
    
    //    if( (messageTypeBits | PLAYER_MESSAGE_NAME) == 1)
    //    {
    //        std::string name;
    //        iArchive(name);
    //        playerSetName(playerId, name);
    //    }
    //    if( (messageTypeBits | PLAYER_MESSAGE_STATE) == 1)
    //    {
    //        int state;
    //        iArchive(state);
    //        _playerIdToPlayerMap[playerId].playerState = (PLAYER_STATE) state;
    ////        handlePlayerStateChange(playerId);
    //
    //    }
    //    if( (messageTypeBits | PLAYER_MESSAGE_CHAT) == 1)
    //    {
    //        std::string chatMessage;
    //        iArchive(chatMessage);
    //        // Do stuff with the message
    //    }
    
}

void TrenchWarsManager::handlePlayerStateChange(int playerId)
{
    
}

void TrenchWarsManager::shutdownNetwork()
{
    globalNetworkInterface->shutdown();
}
