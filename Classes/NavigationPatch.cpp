//
//  NavigationPatch.cpp
//  TrenchWars
//
//  Created by Paul Reed on 1/13/20.
//

#include "NavigationPatch.h"
#include "NavigationPatchBorder.h"
#include "EngagementManager.h"
#include "NavigationGrid.h"
#include "TestBed.h"
#include "GameVariableStore.h"
#include "CollisionGrid.h"

NavigationPatch::NavigationPatch(NavigationPatchRect & patchRect, NavigationGrid * grid)
{
    globalTestBed->addPatch++;
    _owningGrid = grid;
    _patchRect = patchRect;
    _markedForRemoval = false;
    _averageNeighborMovementCost = 0;
    _cachedCosts.costs[MOVE_STRONGLY_PREFER_COVER] = _patchRect.getTerrain().getMovementCost(MOVE_STRONGLY_PREFER_COVER);
    _cachedCosts.costs[MOVE_PREFER_COVER] = _patchRect.getTerrain().getMovementCost(MOVE_PREFER_COVER);
    _cachedCosts.costs[MOVE_NO_PREFERNCE] = _patchRect.getTerrain().getMovementCost(MOVE_NO_PREFERNCE);
    _cachedCosts.costs[MOVE_PREFER_SPEED] = _patchRect.getTerrain().getMovementCost(MOVE_PREFER_SPEED);
    _cachedCosts.costs[MOVE_STRONGLY_PREFER_SPEED] = _patchRect.getTerrain().getMovementCost(MOVE_STRONGLY_PREFER_SPEED);
    
    for(int i = 0; i < 10; i++)
    {
        _congestionInPatch[i] = 1;
    }
    
    _congestionPerPerson = (1.0 / _patchRect.getArea()) * globalVariableStore->getVariable(PathingCongestionCostMultiplier);
}

NavigationPatch::~NavigationPatch()
{
    for(auto line : _displayLines)
    {
        line->remove();
    }
    _displayLines.clear();
    _neighbors.clear();
}

void NavigationPatch::claimCollisionCells()
{
    for(int i = _patchRect.getMinX(); i <= _patchRect.getMaxX(); i++)
    {
        for(int j = _patchRect.getMinY(); j <= _patchRect.getMaxY(); j++)
        {
            CollisionCell * cell = getCollisionGrid()->getCell(i, j);
            if(cell != nullptr)
            {
                cell->setNavigationPatch(this);
            }
        }
    }
}

//EXPENSIVE
void NavigationPatch::findNeighbors()
{
    // right neighbors
    CollisionCell * cell;
    NavigationPatch * neighbor;
    int min = _patchRect.getMinY();
    while(min <= _patchRect.getMaxY())
    {
        cell = getCollisionGrid()->getCell(_patchRect.getMaxX() + 1, min);
        neighbor = cell->getNavigationPatch();
        if(neighbor == nullptr)
        {
            break;
        }
        if(addNeighbor(neighbor, RIGHT))
        {
            neighbor->addNeighbor(this, LEFT);
        }
        min = neighbor->_patchRect.getMaxY() +1;
    }

    // up neighbors
    min = _patchRect.getMinX();
    while(min <= _patchRect.getMaxX())
    {
        cell = getCollisionGrid()->getCell(min, _patchRect.getMaxY() + 1);
        neighbor = cell->getNavigationPatch();
        if(neighbor == nullptr)
        {
            break;
        }
        if(addNeighbor(neighbor, UP))
        {
            neighbor->addNeighbor(this, DOWN);
        }
        min = neighbor->_patchRect.getMaxX() +1;
    }
    
    //left neighbors
    min = _patchRect.getMinY();
    while(min <= _patchRect.getMaxY())
    {
        cell = getCollisionGrid()->getCell(_patchRect.getMinX() - 1, min);
        neighbor = cell->getNavigationPatch();
        if(neighbor == nullptr)
        {
            break;
        }
        if(addNeighbor(neighbor, LEFT))
        {
            neighbor->addNeighbor(this, RIGHT);
        }
        min = neighbor->_patchRect.getMaxY() +1;
    }
    
    //down neighbors
    min = _patchRect.getMinX();
    while(min <= _patchRect.getMaxX())
    {
        cell = getCollisionGrid()->getCell(min, _patchRect.getMinY() - 1);
        neighbor = cell->getNavigationPatch();
        if(neighbor == nullptr)
        {
            break;
        }
        if(addNeighbor(neighbor, DOWN))
        {
            neighbor->addNeighbor(this, UP);
        }
        min = neighbor->_patchRect.getMaxX() +1;
    }
    float borderCells = (_patchRect.getXSize() + _patchRect.getYSize()) * 2;
    
    _averageNeighborMovementCost = 0;
    for(auto neighbor : _neighbors)
    {
        _averageNeighborMovementCost += (neighbor.second->_neighboringPatch->_patchRect.getTerrain().getMovementCost(MOVE_STRONGLY_PREFER_COVER, true) * (neighbor.second->getBorderSize()));
    }
    _averageNeighborMovementCost /= (borderCells * 5);
}

 
bool NavigationPatch::addNeighbor(NavigationPatch * neighbor, DIRECTION direction)
{
    int minBorder, maxBorder, borderCoordinate;
    switch (direction) {
        case RIGHT:
            minBorder = std::max(_patchRect.getMinY(), neighbor->_patchRect.getMinY());
            maxBorder = std::min(_patchRect.getMaxY(), neighbor->_patchRect.getMaxY());
            borderCoordinate = _patchRect.getMaxX();
            break;
        case LEFT:
            minBorder = std::max(_patchRect.getMinY(), neighbor->_patchRect.getMinY());
            maxBorder = std::min(_patchRect.getMaxY(), neighbor->_patchRect.getMaxY());
            borderCoordinate = _patchRect.getMinX();
            break;
        case UP:
            minBorder = std::max(_patchRect.getMinX(), neighbor->_patchRect.getMinX());
            maxBorder = std::min(_patchRect.getMaxX(), neighbor->_patchRect.getMaxX());
            borderCoordinate = _patchRect.getMaxY();
            break;
        case DOWN:
            minBorder = std::max(_patchRect.getMinX(), neighbor->_patchRect.getMinX());
            maxBorder = std::min(_patchRect.getMaxX(), neighbor->_patchRect.getMaxX());
            borderCoordinate = _patchRect.getMinY();
            break;
            
        default:
            return false;
    }
    
    std::lock_guard<std::recursive_mutex> lock(_modificationMutex);
    if(_neighbors.at(neighbor->uID()) != nullptr)
    {
        NavigationPatchBorder * existingBorder = _neighbors.at(neighbor->uID());
        if(existingBorder->getBorderMin() == minBorder && existingBorder->getBorderMax() == maxBorder)
        {
            return false;
        }
    }
    
    NavigationPatchBorder * newNeighbor = new NavigationPatchBorder(minBorder, maxBorder, borderCoordinate, direction, this, neighbor);
    _neighbors.insert(neighbor->uID(),newNeighbor);
    newNeighbor->release();
    return true;
}

void NavigationPatch::markForRemoval()
{
    _markedForRemoval = true;
}

NavigationPatchBorder * NavigationPatch::neighborForPatch(NavigationPatch * patch)
{
    return _neighbors.at(patch->uID());
}

bool NavigationPatch::hasSameTerrain(NavigationPatch * other)
{
    return _patchRect.hasSameTerrain(other->_patchRect);
}

bool NavigationPatch::isTraversable()
{
    return !_patchRect.getTerrain().getImpassable();
}

double NavigationPatch::getMovementCost(MOVEMENT_PREFERENCE movePreference)
{
    return (_cachedCosts.costs[movePreference]); // + _averageNeighborMovementCost);

}


double NavigationPatch::minimumDistanceToCell(CollisionCell * cell)
{
    return _patchRect.minimumDistanceToCoordinates(cell->getXIndex(), cell->getYIndex());
}

bool NavigationPatch::containsCell(CollisionCell * cell)
{
    return _patchRect.containsCoordinates(cell->getXIndex(), cell->getYIndex());
}

CollisionGrid * NavigationPatch::getCollisionGrid()
{
    return _owningGrid->getCollisionGrid();
}

double NavigationPatch::NavigationPatch::getCachedPathCost(CollisionCell * start, CollisionCell * end, MOVEMENT_PREFERENCE movePreference)
{
    if(_pathCostCache.find(start->uID()) != _pathCostCache.end())
    {
        if(_pathCostCache[start->uID()].find(end->uID()) != _pathCostCache[start->uID()].end())
        {
            return _pathCostCache[start->uID()][end->uID()].costs[movePreference];
        }
    }
    return -1;
}

void NavigationPatch::cachePathCost(CollisionCell * start, CollisionCell * end, MOVEMENT_PREFERENCE movePreference, double cost)
{
    if(_pathCostCache.find(start->uID()) == _pathCostCache.end())
    {
        _pathCostCache[start->uID()] = std::unordered_map<ENTITY_ID, PathCosts>();
    }
    _pathCostCache[start->uID()][end->uID()].costs[movePreference] = cost;
}

void NavigationPatch::addCongestion(int team, ENTITY_ID otherPatchId)
{
    _congestionMutex.lock();
    _congestionPerPerson = (1.0 / _patchRect.getArea()) * globalVariableStore->getVariable(PathingCongestionCostMultiplier);
    
    auto it = _neighbors.find(otherPatchId);
    if(it != _neighbors.end())
    {
        it->second->addCongestion(team);
        it->second->getOppositeBorder()->addCongestion(team);
    }

    _congestionInPatch[team] += _congestionPerPerson;
    _congestionMutex.unlock();
}

void NavigationPatch::removeCongestion(int team, ENTITY_ID otherPatchId)
{    
    _congestionMutex.lock();

    auto it = _neighbors.find(otherPatchId);
    if(it != _neighbors.end())
    {
        it->second->removeCongestion(team);
        it->second->getOppositeBorder()->removeCongestion(team);

    }

    _congestionInPatch[team] -= _congestionPerPerson;
    if (_congestionInPatch[team] < 1.00)
    {
        _congestionInPatch[team] = 1.00;
    }
    _congestionMutex.unlock();

}

double NavigationPatch::getCongestionForTeam(int team, ENTITY_ID otherPatchId)
{
    double congestion = 0; //_congestionInPatch[team];
    auto it = _neighbors.find(otherPatchId);
    if(it != _neighbors.end())
    {
        congestion += it->second->getCongestionForTeam(team);
    }
    return congestion;
}



void NavigationPatch::draw()
{

    if(_displayLines.size() == 0)
    {
        Vec2 p1 = getCollisionGrid()->getCell(_patchRect.getMinX(), _patchRect.getMinY())->getCellPosition();
        Vec2 p2 = getCollisionGrid()->getCell(_patchRect.getMinX(), _patchRect.getMaxY())->getCellPosition();
        p2.y += 1;
        
        Vec2 p3 = getCollisionGrid()->getCell(_patchRect.getMaxX(), _patchRect.getMaxY())->getCellPosition();
        p3.x += 1;
        p3.y += 1;

        Vec2 p4 = getCollisionGrid()->getCell(_patchRect.getMaxX(), _patchRect.getMinY())->getCellPosition();
        p4.x += 1;
        
        p1 *= WORLD_TO_GRAPHICS_SIZE;
        p2 *= WORLD_TO_GRAPHICS_SIZE;
        p3 *= WORLD_TO_GRAPHICS_SIZE;
        p4 *= WORLD_TO_GRAPHICS_SIZE;
        
        LineDrawer * line = LineDrawer::create(p1, p2, 2);
        line->setColor(Color3B::WHITE);
        _displayLines.pushBack(line);
        
        line = LineDrawer::create(p2, p3, 2);
        line->setColor(Color3B::WHITE);
        _displayLines.pushBack(line);
        
        line = LineDrawer::create(p3, p4, 2);
        line->setColor(Color3B::WHITE);
        _displayLines.pushBack(line);
        
        line = LineDrawer::create(p4, p1, 2);
        line->setColor(Color3B::WHITE);
        _displayLines.pushBack(line);
    }
    
    for(auto line : _displayLines)
    {
        line->setVisible(true);
    }
}


void NavigationPatch::stopDrawing()
{
    for(auto line : _displayLines)
    {
        line->setVisible(false);
    }
}

void NavigationPatch::print(std::string start, bool neighbors)
{
    CCLOG("%sID: %lld P: %d,%d to %d,%d T: %d N: %ld ", start.c_str() , uID(), _patchRect.getMinX(), _patchRect.getMinY(), _patchRect.getMaxX(), _patchRect.getMaxY(), _patchRect.getTerrain().getMovementCost(MOVE_NO_PREFERNCE), _neighbors.size());
    if(neighbors) {
        CCLOG("   %s N: ", start.c_str());
        for(int key : _neighbors.keys())
        {
            NavigationPatchBorder * neighbor = _neighbors.at(key);
            CCLOG("%lld ",  neighbor->_neighboringPatch->uID());
        }
        CCLOG("");
    }
}

void NavigationPatch::removeNeighbor(NavigationPatch * neighbor)
{
    _modificationMutex.lock();
//    LOG_DEBUG("Patch %lld removing neighbor: %lld \n ", uID(), neighbor->uID());
    auto neighborIt = _neighbors.find(neighbor->uID());
    if(neighborIt != _neighbors.end())
    {
        neighborIt->second->remove();
        _neighbors.erase(neighborIt);
    }
    _modificationMutex.unlock();

}

void NavigationPatch::remove()
{
//    LOG_DEBUG("Removing patch %lld with %d neighbors at frame %lld \n", uID(), _neighbors.size(), globalEngagementManager->getFrameCount());
    _modificationMutex.lock();
    while(_neighbors.size() > 0)
    {
        auto it = _neighbors.begin();
        NavigationPatchBorder * neighbor = it->second;
        neighbor->retain();
        _neighbors.erase(it);
        _modificationMutex.unlock();
        neighbor->_neighboringPatch->removeNeighbor(this);
        neighbor->remove();
        neighbor->release();
        _modificationMutex.lock();
    }
    _modificationMutex.unlock();
}
