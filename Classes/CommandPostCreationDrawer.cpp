//
//  CommandPostCreationDrawer.cpp
//  TrenchWars
//
//  Created by Paul Reed on 2/28/21.
//

#include "CommandPostCreationDrawer.h"
#include "CommandPost.h"
#include "Command.h"
#include "PlayerLayer.h"
#include "TeamManager.h"

CommandPostCreationDrawer::CommandPostCreationDrawer() : DrawNode()
{
    
}


CommandPostCreationDrawer::~CommandPostCreationDrawer()
{
    
}


bool CommandPostCreationDrawer::init()
{
    if(DrawNode::init()) 
    {
        globalPlayerLayer->addChild(this, 0); //1000
        update();
        
        Director * director = Director::getInstance();
        Scheduler * scheduler = director->getScheduler();
        scheduler->schedule([=] (float dt) { update(); }, this, 0.333333333, false, "commandPostDrawer");
        return true;
    }
    return false;
}


CommandPostCreationDrawer * CommandPostCreationDrawer::create()
{
    CommandPostCreationDrawer * drawer = new CommandPostCreationDrawer();
    if(drawer->init())
    {
        drawer->autorelease();
        return  drawer;
    }
    
    CC_SAFE_DELETE(drawer);
    return nullptr;
}


void CommandPostCreationDrawer::update()
{
    clear();
    for(auto command : globalTeamManager->getLocalPlayersTeam()->getCommands())
    {
        for(CommandPost * post : command->getCommandPosts())
        {
            Vec2 center = post->getPosition();
            drawSolidCircle(center, post->getControlRadius()  * 2, 0, 100, Color4F(0,0,1,.1));
            drawSolidCircle(center, post->getControlRadius(), 0, 100, Color4F(0,1,0,.1));
            drawSolidCircle(center, post->getCreationRadius(), 0, 100, Color4F(1,0,0,.15));
            
            Color4F linkColor = post->isLinkedToPrimaryCommandPost() ? Color4F(0, 1, 0, .3) : Color4F(1, 0, 0, .3);
            for(CommandPost * link : post->getLinkedCommandPosts())
            {
                drawLine(post->getPosition(), link->getPosition(), linkColor);
            }
        }
    }
}

void CommandPostCreationDrawer::remove()
{
    globalPlayerLayer->removeChild(this);
    Director * director = Director::getInstance();
    Scheduler * scheduler = director->getScheduler();
    scheduler->unschedule("commandPostDrawer", this);
}
