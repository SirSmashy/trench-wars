//
//  Random.cpp
//  TrenchWars
//
//  Created by Paul Reed on 8/1/22.
//

#include "Random.h"
#include <time.h>
#include <chrono>


RandomNumbers * globalRandom;

RandomNumbers::RandomNumbers()
{
    globalRandom = this;
    long long currentTime =  std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count();
    mcg_state = 2 * currentTime + 1;
    randomNumber();
}

RandomNumbers * RandomNumbers::create()
{
    RandomNumbers * random = new RandomNumbers();
    random->autorelease();
    return random;
}

uint32_t RandomNumbers::randomNumber(void)
{
    uint64_t x = mcg_state;
    unsigned count = (unsigned)(x >> 61);    // 61 = 64 - 3
    
    mcg_state = x * multiplier;
    x ^= x >> 22;
    return (uint32_t)(x >> (22 + count));    // 22 = 32 - 3 - 7
}

int RandomNumbers::randomInt(int min, int  max)
{
    int range = max - min;
    int random = min + (randomNumber() % range);
    return random;
}

double RandomNumbers::randomDouble(double min, double max)
{
    if(min == max)
        return min;
    int range = (max * 1000000) - (min * 1000000);
    //    double result = min;
    return min + ((randomNumber() % range) / 1000000.0);
}
