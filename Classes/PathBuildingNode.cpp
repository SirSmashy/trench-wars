//
//  PathBuildingNode.cpp
//  TrenchWars
//
//  Created by Paul Reed on 9/16/22.
//

#include "PathBuildingNode.h"
#include "NavigationPatch.h"
#include "VectorMath.h"
#include "TestBed.h"
#include "NavigationPatchBorder.h"
#include "MapSector.h"
#include "GameVariableStore.h"

PathBuildingNode::PathBuildingNode()
{
    _parent = nullptr;
    _costToTravelHere = 0;
    _totalEstimatedCost = 0;
    _remainingTravelCostHeuristic = MAXFLOAT;
    _examined = false;
    _distanceFromParent = 0;
    _team = -1;
    totalDistance = 0;
    updates = 0;
    congestion = 0.0;
    usedInPath = 0.0;
}

//////////////////////////////////////////////////////////////////////////////////////
/// SectorPathNode
//////////////////////////////////////////////////////////////////////////////////////
///

SectorPathBuildingNode::~SectorPathBuildingNode()
{
}

SectorPathBuildingNode::SectorPathBuildingNode(MapSector * sector, MapSector * goalSector, int team)
: PathBuildingNode()
{
    _team = team;
    _sector = sector;
    //Get the estimated cost to travel from this patch to the goal cell
    Vec2 diff = goalSector->getPathingCell()->getVectorToOtherCell(_sector->getPathingCell());
    float diffLength = VectorMath::fastSqrt(diff.lengthSquared());
    _remainingTravelCostHeuristic = diffLength;
    float estimate = _sector->getPathingCell()->getTerrainCharacteristics().getMovementCost(MOVE_STRONGLY_PREFER_COVER);
    _remainingTravelCostHeuristic *=  estimate;
    _distanceFromParent = -1;
    totalDistance = -1;
    _costToTravelHere = 0;
}

SectorPathBuildingNode::SectorPathBuildingNode(SectorPathBuildingNode * parent, MapSector * sector, MapSector * goalSector, int team)
: SectorPathBuildingNode(sector,goalSector, team)
{
    _parent = parent;
    _costToTravelHere = MAXFLOAT;

}



bool SectorPathBuildingNode::update(SectorPathBuildingNode * potentialNewParent, MOVEMENT_PREFERENCE movePreference)
{
    double fromParentCost = potentialNewParent->getMapSector()->getPathsToNeighbors().at(_sector);
    double congestionCost = 1; //(potentialNewParent->getMapSector()->getCongestionForTeam(_team) + _sector->getCongestionForTeam(_team)) / 2;

    double newTravelCost = (fromParentCost + potentialNewParent->getCostToTravelHere()) * congestionCost;
    if(newTravelCost < _costToTravelHere)
    {
        _parent = potentialNewParent;
        _costToTravelHere = newTravelCost;
        if (_costToTravelHere > 30000000000)
        {
            LOG_DEBUG_ERROR("hmmmm %f", _costToTravelHere);
        }
        _totalEstimatedCost = _remainingTravelCostHeuristic + _costToTravelHere;
        return true;
    }
    return false;
}

CollisionCell * SectorPathBuildingNode::getPosition()
{
    return _sector->getPathingCell();
}

double SectorPathBuildingNode::getCongestionMultiplier()
{
    return _sector->getCongestionForTeam(_team);
}



//////////////////////////////////////////////////////////////////////////////////////
/// PatchPathBuildingNode
//////////////////////////////////////////////////////////////////////////////////////
PatchPathBuildingNode::~PatchPathBuildingNode()
{
    
}

PatchPathBuildingNode::PatchPathBuildingNode()
: PathBuildingNode()
{
}

PatchPathBuildingNode::PatchPathBuildingNode(CollisionCell * start, NavigationPatch * patch, CollisionCell * goalCell, int team)
: PathBuildingNode()
{
    useExitCellInPath = true;
    _patch = patch;
    _goalCell = goalCell;
    _pathType = MINIMUM_PATH;
    _crossedBorder = nullptr;
    _team = team;
    
    entranceToThisPatch = start;
    exitFromPreviousPatch = start;
}

PatchPathBuildingNode::PatchPathBuildingNode(PatchPathBuildingNode * parent,
                                     NavigationPatch * patch,
                                     CollisionCell * goalCell,
                                     PATH_TYPE pathType,
                                     int team)
: PathBuildingNode()
{
    _costToTravelHere = MAXFLOAT;
    _totalEstimatedCost = MAXFLOAT;
    _remainingTravelCostHeuristic = MAXFLOAT;
    _parent = parent;
    
    _patch = patch;
    _goalCell = goalCell;
    _pathType = pathType;
    _team = team;
    useExitCellInPath = true;

    NavigationPatchBorder * border = patch->neighborForPatch(parent->getNavigationPatch());
    if(pathType == MINIMUM_PATH)
    {
        entranceToThisPatch = border->borderCellNearestCell(parent->entranceToThisPatch);
    }
    else
    {
        entranceToThisPatch = border->getBorderCellNearestLine(parent->entranceToThisPatch, _goalCell);
    }

    exitFromPreviousPatch = border->getOppositeBorder()->borderCellNearestCell(entranceToThisPatch);
}

void PatchPathBuildingNode::calculateHeuristic(MOVEMENT_PREFERENCE movePreference)
{
    //Get the estimated cost to travel from this patch to the goal cell
    Vec2 diff = _goalCell->getVectorToOtherCell(entranceToThisPatch);
    float diffLength = VectorMath::fastSqrt(diff.lengthSquared());
    
    _remainingTravelCostHeuristic = diffLength;
    float estimate = getNavigationPatch()->getMovementCost(movePreference) * globalVariableStore->getVariable(PathingHeuristicMultiplier);
    _remainingTravelCostHeuristic *=  estimate;
}

bool PatchPathBuildingNode::update(PatchPathBuildingNode * potentialNewParent, MOVEMENT_PREFERENCE movePreference)
{
    //Get the cost to travel through the parent patch to here
    NavigationPatchBorder * border = _patch->neighborForPatch(potentialNewParent->getNavigationPatch());
    CollisionCell * exit;
    if(_pathType == MINIMUM_PATH)
    {
        exit = border->getOppositeBorder()->borderCellNearestCell(potentialNewParent->entranceToThisPatch);
    }
    else
    {
        exit = border->getOppositeBorder()->getBorderCellNearestLine(potentialNewParent->entranceToThisPatch, _goalCell);
    }
    
    exit = border->getOppositeBorder()->getBorderCellWithLeastConjestion(exit, _team);
    
    Vec2 diff = exit->getVectorToOtherCell(potentialNewParent->entranceToThisPatch);
    
    float diffLength = VectorMath::fastSqrt(diff.lengthSquared());
    if(diffLength < 1.0)
    {
        diffLength = 1.0;
    }

    float newTravelCost = potentialNewParent->getNavigationPatch()->getMovementCost(movePreference);
    double newCong = _patch->getCongestionForTeam(_team,potentialNewParent->getNavigationPatch()->uID());

    newTravelCost *= newCong;
    newTravelCost *= diffLength;
    
    updates++;
    
    newTravelCost += potentialNewParent->getCostToTravelHere();
    //check to see if this cell has not been touched or if its cost has changed
    if(newTravelCost < _costToTravelHere)
    {
        if(newCong > 20)
        {
            newCong = newCong;
        }
        _distanceFromParent = diffLength;
        totalDistance = potentialNewParent->totalDistance + _distanceFromParent;
        _parent = potentialNewParent;
        _crossedBorder = border;
        _costToTravelHere = newTravelCost;
        entranceToThisPatch = border->borderCellNearestCell(exit);

        congestion = newCong;
        exitFromPreviousPatch = exit;
        calculateHeuristic(movePreference);
        _totalEstimatedCost = _remainingTravelCostHeuristic + _costToTravelHere;
        return true;
    }
    return false;
}

CollisionCell * PatchPathBuildingNode::getPosition()
{
    return entranceToThisPatch;
}

double PatchPathBuildingNode::getCongestionMultiplier()
{
    return _patch->getCongestionForTeam(_team);
}


GoalCellPathBuildingNode::GoalCellPathBuildingNode(PatchPathBuildingNode * parent, NavigationPatch * patch, CollisionCell * goalCell, int team)
{
    _costToTravelHere = MAXFLOAT;
    _totalEstimatedCost = MAXFLOAT;
    _remainingTravelCostHeuristic = 0;
    entranceToThisPatch = goalCell;
    exitFromPreviousPatch = goalCell;
    _parent = parent;
    
    _patch = patch;
    _goalCell = goalCell;
    _team = team;
    useExitCellInPath = false;
    _crossedBorder = nullptr;
}

bool GoalCellPathBuildingNode::update(PatchPathBuildingNode * potentialNewParent, MOVEMENT_PREFERENCE movePreference)
{
    Vec2 diff = potentialNewParent->entranceToThisPatch->getVectorToOtherCell(_goalCell);
    
    float diffLength = VectorMath::fastSqrt(diff.lengthSquared());
    
    float newTravelCost = _patch->getMovementCost(movePreference);
    double newCong = _patch->getCongestionForTeam(_team,potentialNewParent->getNavigationPatch()->uID());
    newTravelCost *= newCong;
    newTravelCost *= diffLength;
    
    newTravelCost += potentialNewParent->getCostToTravelHere();
    //check to see if this cell has not been touched or if its cost has changed
    if(newTravelCost < _costToTravelHere)
    {
        _distanceFromParent = diffLength;
        totalDistance = potentialNewParent->totalDistance + _distanceFromParent;
        _parent = potentialNewParent;
        _costToTravelHere = newTravelCost;
        
        congestion = newCong;
        _totalEstimatedCost = _costToTravelHere;
        return true;
    }
    return false;
}

