//
//  AnimatedSpriteProxy.cpp
//  trench-wars
//
//  Created by Paul Reed on 10/3/23.
//

#include "AnimatedSpriteProxy.h"
#include "CocosObjectManager.h"
#include "World.h"
#include "EntityFactory.h"
#include "MultithreadedAutoReleasePool.h"
#include "CollisionGrid.h"

AnimatedSpriteProxy::AnimatedSpriteProxy() :
_pendingPosition(Vec2()),
_pendingScale(Vec2(1.0, 1.0)),
_pendingOpacity(255),
_pendingAnimation(DEFAULT),
_pendingAnimationRepeat(false),
_pendingAnimationDirection(NONE),
_pendingVisibility(true),
_pendingColor(Color3B()),
_pendingZIndex(0),
_pendingZPosition(0),
_pendingTransform(AffineTransform()),
_pendingFlashing(false),
_pendingAnchorPoint(Vec2()),
_pendingSpriteSize(Size(1,1))
{
    
}

void AnimatedSpriteProxy::init(const std::string & spriteName, Vec2 position, bool alwaysUpdate)
{
    _positionOffset = Vec2();
    _currentZPosition = 0;
    _zIndexOffset = 0;
    _zPositionOffset = 0;
    _positionUnderground = false;
    _updateZPositionUponMove = true;
    _spriteName = spriteName;
    _sprite = nullptr;
    setPosition(position);
    _nodeToJoin = nullptr;
    _alwaysUpdate = alwaysUpdate;
    globalCocosObjectManager->requestCocosObjectCreation(this);
    globalCocosObjectManager->requestCocosObjectUpdate(this);
}

void AnimatedSpriteProxy::init(const std::string & spriteName, Vec2 position, Node * nodeToJoin, bool createImmediately)
{
    _positionOffset = Vec2();
    _currentZPosition = 0;
    _zIndexOffset = 0;
    _zPositionOffset = 0;
    _positionUnderground = false;
    _updateZPositionUponMove = false;
    _spriteName = spriteName;
    _sprite = nullptr;
    setPosition(position);
    _nodeToJoin = nodeToJoin;
    _alwaysUpdate = true;
    globalCocosObjectManager->requestCocosObjectCreation(this);
    globalCocosObjectManager->requestCocosObjectUpdate(this);
}

AnimatedSpriteProxy::~AnimatedSpriteProxy()
{
    if(_sprite != nullptr && _nodeToJoin != nullptr)
    {
        _nodeToJoin->removeChild(_sprite, true);
        _nodeToJoin->release();
        _sprite->release();
    }
}

AnimatedSpriteProxy * AnimatedSpriteProxy::create(const std::string & spriteName, Vec2 position, bool alwaysUpdate)
{
    AnimatedSpriteProxy  * newProxy = new AnimatedSpriteProxy();
    newProxy->init(spriteName, position, alwaysUpdate);
    globalAutoReleasePool->addObject(newProxy);

    return newProxy;
}

AnimatedSpriteProxy * AnimatedSpriteProxy::createWithNode(const std::string & spriteName, Vec2 position, Node * nodeToJoin)
{
    AnimatedSpriteProxy  * newProxy = new AnimatedSpriteProxy();
    newProxy->init(spriteName, position, nodeToJoin, false);
    globalAutoReleasePool->addObject(newProxy);
    return newProxy;
}


void AnimatedSpriteProxy::createCocosObject()
{
    EntityAnimationSet * animationSet = globalEntityFactory->getEntityAnimationSet(_spriteName);

    int zOrder = 0;
    Vec2 position = _pendingPosition;
    if(_updateZPositionUponMove && world->isPositionUnderground(position))
    {
        zOrder += UNDERGROUND_Z_OFFSET;
    }
    _sprite = AnimatedSprite::create(animationSet, Vec2());
    _sprite->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    _sprite->retain();
    _animationSet = animationSet;
    
    if(_nodeToJoin == nullptr)
    {
        _nodeToJoin = animationSet->_batchGroup->addSprite(_sprite, zOrder);
    }
    else
    {
        _nodeToJoin->addChild(_sprite, zOrder, 1);
    }
    _nodeToJoin->retain();
    
    updateCocosObject();
}

void AnimatedSpriteProxy::setNeedsUpdate()
{
    _needsUpdate = true;
    if(!_alwaysUpdate)
    {
        globalCocosObjectManager->requestCocosObjectUpdate(this);
    }
}

void AnimatedSpriteProxy::setPosition(Vec2 position)
{
    _pendingPosition = position;
    if(_updateZPositionUponMove)
    {
        CollisionGrid * grid = world->getCollisionGrid(position);
        float zPosition = 0;
        float zIndex = 0;
        if(!_positionUnderground)
        {
            zIndex = (position.y - grid->getWorldBounds().origin.y);
            zIndex = grid->getGridSize().height - zIndex;
            zPosition = zIndex / (grid->getGridSize().height * 2); // TODO fix oooookay this is expensive and magic and probably doesn't even belong here
        }
        if(zPosition != _currentZPosition)
        {
            setPositionZ(zPosition);
            //            setZIndex(zIndex);
        }
    }
    setNeedsUpdate();
}

void AnimatedSpriteProxy::setScale(Vec2 scale)
{
    _pendingScale = scale;
    setNeedsUpdate();
}

void AnimatedSpriteProxy::setAnimation(ANIMATION_TYPE animation, bool repeat, DIRECTION direction)
{
    _pendingAnimation = animation;
    _pendingAnimationRepeat = repeat;
    _pendingAnimationDirection = direction;
    setNeedsUpdate();
}

void AnimatedSpriteProxy::setOpacity(int opacity)
{
    _pendingOpacity = opacity;
    setNeedsUpdate();
    
}

void AnimatedSpriteProxy::setVisible(bool visible)
{
    _pendingVisibility = visible;
    setNeedsUpdate();
}

void AnimatedSpriteProxy::setColor(const Color3B & color)
{
    _pendingColor = color;
    setNeedsUpdate();
}

void AnimatedSpriteProxy::setZIndex(int index)
{
    _pendingZIndex = index;
    setNeedsUpdate();
}

void AnimatedSpriteProxy::setPositionZ(float zPosition)
{
    _pendingZPosition = zPosition;
    setNeedsUpdate();
}

void AnimatedSpriteProxy::setAdditionalTransform(AffineTransform & transform)
{
    _pendingTransform = transform;
    setNeedsUpdate();
}

void AnimatedSpriteProxy::setPositionOffset(Vec2 offset)
{
    _positionOffset = offset;
    if(_sprite != nullptr)
    {
        setPosition(_sprite->getPosition()); //force update of position
    }
}

void AnimatedSpriteProxy::setZIndexOffset(int offset)
{
    _zIndexOffset = offset;
    if(_sprite != nullptr)
    {
        setZIndex(_sprite->getLocalZOrder()); // force update of z index
    }
}

void AnimatedSpriteProxy::setPositionZOffset(float offset)
{
    _zPositionOffset = offset;
    float zPosition = 0;
    if(_sprite != nullptr)
    {
        zPosition = _sprite->getPositionZ();
    }
    setPositionZ(zPosition); // Force update of z position
}


void AnimatedSpriteProxy::setFlashing(bool flashing)
{
    _pendingFlashing = flashing;
    setNeedsUpdate();
}

void AnimatedSpriteProxy::setAnchorPoint(Vec2 anchorPoint)
{
    _pendingAnchorPoint = anchorPoint;
    setNeedsUpdate();
}

void AnimatedSpriteProxy::setSpriteSize(Size spriteSize)
{
    _pendingSpriteSize = spriteSize;
    setNeedsUpdate();
}

void AnimatedSpriteProxy::setUnderground(bool underground)
{
    _positionUnderground = underground;
    float zPosition = 0;
    if(_sprite != nullptr)
    {
        zPosition = _sprite->getPositionZ();
    }
    if(underground)
    {
        zPosition -= 1; // TODO fix careful with this one
    }
    setPositionZ(zPosition); // Force update of z position
}


Rect AnimatedSpriteProxy::getBoundingBox()
{
    return _sprite->getBoundingBox();
}

Size AnimatedSpriteProxy::getContentSize()
{
    return _sprite->getContentSize();
}


ANIMATION_TYPE AnimatedSpriteProxy::getCurrentAnimationState()
{
    return _sprite->getCurrentAnimationState();
}

DIRECTION AnimatedSpriteProxy::getCurrentAnimationDirection()
{
    return _sprite->getCurrentAnimationDirection();
}

bool AnimatedSpriteProxy::updateCocosObject()
{
    if(!_needsUpdate || _sprite == nullptr)
    {
        return _alwaysUpdate;
    }
    
    if(_pendingPosition.isChanged())
    {
        _sprite->setPosition((_pendingPosition + _positionOffset) * WORLD_TO_GRAPHICS_SIZE);
        _pendingPosition.clearChange();
    }
    
    if(_pendingScale.isChanged())
    {
        _sprite->setScale(_pendingScale.get().x, _pendingScale.get().y);
        _pendingScale.clearChange();
    }
    
    if(_pendingOpacity.isChanged())
    {
        _sprite->setOpacity(_pendingOpacity);
        _pendingOpacity.clearChange();
    }
    if(_pendingSpriteSize.isChanged())
    {
        _sprite->setSpriteSize(_pendingSpriteSize);
        _pendingSpriteSize.clearChange();
    }
    if(_pendingAnimation.isChanged())
    {
        _sprite->setAnimation(_pendingAnimation,  _pendingAnimationRepeat, _pendingAnimationDirection);
        _pendingAnimation.clearChange();
        _pendingAnimationRepeat.clearChange();
        _pendingAnimationDirection.clearChange();
    }
    
    if(_pendingVisibility.isChanged())
    {
        _sprite->setVisible(_pendingVisibility);
        _pendingVisibility.clearChange();
    }
    
    if(_pendingColor.isChanged())
    {
        _sprite->setColor(_pendingColor);
        _pendingColor.clearChange();
    }
    if(_pendingZIndex.isChanged())
    {
        _sprite->setLocalZOrder(_pendingZIndex + _zIndexOffset);
//        _animationSet->_batchNode->reorderBatch(true); //hmmmm
        _pendingZIndex.clearChange();
    }
    if(_pendingZPosition.isChanged())
    {
        _currentZPosition = _pendingZPosition;
        double z = _currentZPosition + _zPositionOffset;
        _sprite->setPositionZ(z);
    }
    if(_pendingTransform.isChanged())
    {
        /// This is going to get ugly but lets try it
        ///
        float scale = _sprite->getScale();
        AffineTransform transform = _pendingTransform;
        transform.tx /= scale;
        transform.ty /= scale;
        _sprite->setAdditionalTransform(transform);
        _pendingTransform.clearChange();
    }
    if(_pendingFlashing.isChanged())
    {
        bool flashing = _pendingFlashing;
        _pendingFlashing.clearChange();
        if(flashing)
        {
            if(_sprite->getActionByTag(999) == nullptr)
            {
                auto repeat = RepeatForever::create(Sequence::create(FadeTo::create(0.4f, 255), FadeTo::create(0.4f, 100), NULL));
                repeat->setTag(999);
                _sprite->runAction(repeat);
            }
        }
        else
        {
            _sprite->stopActionByTag(999);
            _sprite->setOpacity(255); ///FIXME: hmmmmm this should probably be reset to the correct opacity
        }
    }
    if(_pendingAnchorPoint.isChanged())
    {
        _sprite->setAnchorPoint(_pendingAnchorPoint);
        _pendingAnchorPoint.clearChange();
    }
    
    _needsUpdate = false;
    return _alwaysUpdate;
}

void AnimatedSpriteProxy::remove()
{
    globalCocosObjectManager->requestCocosObjectRemoval(this);
}
