//
//  Logging.h
//  TrenchWars
//
//  Created by Paul Reed on 1/6/25.
//

#define LOG_ERROR(...) printf(__VA_ARGS__)
#define LOG_WARNING(...) printf(__VA_ARGS__)
#define LOG_INFO(...) printf(__VA_ARGS__)
#define LOG_DEBUG(...) printf(__VA_ARGS__)
#define LOG_DEBUG_ERROR(...) printf(__VA_ARGS__)
