//
//  Human.cpp
//  TrenchWars
//
//  Created by Paul Reed on 2/20/12.
//  Copyright (c) 2012 . All rights reserved.
//
#include <iostream>
#include "Human.h"
#include "VectorMath.h"
#include "EntityFactory.h"
#include "GameMenu.h"
#include "PrimativeDrawer.h"
#include "EngagementManager.h"
#include "StaticEntity.h"
#include "GameVariableStore.h"
#include "Order.h"
#include "EntityManager.h"
#include "MultithreadedAutoReleasePool.h"
#include "CollisionGrid.h"
#include "MapSector.h"
#include "Command.h"
#include "Team.h"
#include "World.h"
#include "BackgroundTaskHandler.h"
#include "GameStatistics.h"
#include "SectorGrid.h"
#include "TestBed.h"

USING_NS_CC;

#define LOOKS_PER_FRAME 10
static const float MIN_LOOK_ANGLE_INCREMENT = D2R / 2;
static const float MAX_LOOK_ANGLE_INCREMENT = D2R * 3;
static const float STOP_VISION_THRESHOLD = 150;

Human::Human() : MovingPhysicalEntity() ,
_pendingHealthChange(0),
_pendingStress(0.0f),
_pendingExperience(0.0f)
{
    
}

Human::~Human()
{
}

bool Human::init(HumanDefinition * definition, Vec2 position, Command * command)
{
    if(MovingPhysicalEntity::init(definition,position, command->getOwningTeam()))
    {
        _owningCommand = command;
        _lifeState = ALIVE;
        _shouldDie = false;
        _experience = 1;
        _stress = 0;
        _currentStressState = STRESS_CALM;
        _stressReductionRate = globalVariableStore->getVariable(BaseStressReductionRate); //FIXME: FIX incorporate experience here somehow
        _currentWorkEntity = -1;
        _takingCoverBehindEnt = -1;
        _timeSinceLastVisibleAction = 1000;
        _movingTowardGoal = false;
        _workingOnGoal = false;
        
        _lastLookAngleRadians = globalRandom->randomDouble(0, M_PI * 2);

        _visionBudget = LOOKS_PER_FRAME;
        if(definition != nullptr)
        {
            _health = definition->_health;
            _maxHealth = definition->_health;
            _sightRadius = definition->_sightRadius;
        }

    }
    else
    {
        CCLOG("oh no!!!");
    }
    return true;
}

Human * Human::create(HumanDefinition * definition, Vec2 position, Command * command)
{
    Human * newActor = new Human();
    if(newActor && newActor->init(definition, position, command))
    {
        globalAutoReleasePool->addObject(newActor);
        return newActor;
    }
    CC_SAFE_DELETE(newActor);
    return nullptr;
}

Human * Human::create()
{
    Human * newActor = new Human();
    globalAutoReleasePool->addObject(newActor);
    return newActor;
}

void Human::query(float deltaTime)
{
//    MicroSecondSpeedTimer timer( [=](long long int time) {
//        globalGameStatistics->setTestValue("H Query: ", (double)time/1000.0);
//    });
    
    _visionBudget = globalTestBed->looksPerFrame;

    if(_lifeState == DEAD)
        return;
        
    if(_pendingHealthChange.isChanged())
    {
        _health -= _pendingHealthChange;
        _pendingHealthChange.clearChange();
        
        if(_health <= 0 && _lifeState == ALIVE)
        {
            _shouldDie = true;
        }
    }
    
    if(_pendingExperience.isChanged())
    {
        _experience += _pendingExperience;
        _pendingExperience.clearChange();
    }
    
    bool orderMayHaveChanged = false;
    float prevStress = _stress;
    _stress -= _stressReductionRate * deltaTime;
    if(_stress < 0)
    {
        _stress = 0;
    }
    
    if(_pendingStress.isChanged())
    {
        _stress += _pendingStress;
        if(_stress > 100)
        {
            _stress = 100;
        }
        _pendingStress.clearChange();
    }
    
    if(prevStress != _stress)
    {
        updateStress();
    }
    if(_pendingGoals.isChanged())
    {
        auto newGoals = _pendingGoals.getAndClear();
        for(auto goalPair : newGoals)
        {
            Goal * existing = _goals.at(goalPair.first);
            if(existing != nullptr)
            {
                // maybe do something?
            }
            _goals.insert(goalPair.first, goalPair.second);
        }
    }
    
    if(_debugging)
    {
        _debugging = _debugging;
    }
    
    updateGoals(deltaTime);
    
    if(_shouldLieProne)
    {
        _prone = true;
        _moveRate = _maximumMoveRate * .25;
    }
    else
    {
        _prone = false;
        _moveRate = _maximumMoveRate;
    }
    
    if(!(globalTestBed->behaviorsToSkip & LOOK_AROUND_BEHAVIOR))
    {
        while(_visionBudget > 0)
        {
            float accumulated = 0;
            float maxCover = 75;
            Vec2 start = getPosition();
            Vec2 end = Vec2(1000, 0);
            int maxHeight = 0;
            end.rotate(Vec2::ZERO, _lastLookAngleRadians);
            _lastLookAngleRadians += globalRandom->randomDouble(MIN_LOOK_ANGLE_INCREMENT, MAX_LOOK_ANGLE_INCREMENT) ; // increment by a random amount
            if(_lastLookAngleRadians > (M_PI * 2))
            {
                _lastLookAngleRadians = 0;
            }
            bool canSee = doLineVisibilityTestWithCells(start, end, accumulated, 75);
            _visionBudget--;
        }
    }


    _timeSinceLastVisibleAction += deltaTime;
//    timer.stop();
    MovingPhysicalEntity::query(deltaTime);
}


void Human::act(float deltaTime)
{
    if(_lifeState == DEAD)
    {
        if(lastUpdateTime + (globalVariableStore->getVariable(SecondsBeforeRemovingCorpse)) < globalEngagementManager->getEngagementTime())
        {
            removeFromGame();
        }
        return;
    }
    
    if(_shouldDie)
    {
        _shouldDie = false;
        die();
        return;
    }
    
    MovingPhysicalEntity::act(deltaTime);
}

void Human::updateGoals(float deltaTime)
{
    if((globalTestBed->behaviorsToSkip & UPDATE_GOALS_BEHAVIOR))
    {
        return;
    }
    std::vector<Goal *> orderedGoals;
    for(auto goalPair : _goals)
    {
        orderedGoals.push_back(goalPair.second);
    }
    std::sort(orderedGoals.begin(), orderedGoals.end(), [] (Goal * a, Goal * b) {
        return a->getImportance() > b->getImportance();
    });
        
    _movingTowardGoal = false;
    _workingOnGoal = false;
    _shouldLieProne = false;
    
    for(auto goal : orderedGoals)
    {
//        if(_currentStressState == STRESS_PANIC && goal->getImportance() < 3)
//        {
//            break;
//        }
//        else
            
        // If this human is too stressed it won't work on low importance goals
        if(_currentStressState >= STRESS_SEEK_COVER && goal->getImportance() <= 1)
        {
            break;
        }
        switch(goal->getGoalType())
        {
            case MOVE_GOAL:
            {
                updateMoveGoal((MoveGoal *) goal, deltaTime);
                break;
            }          
            case LIE_PRONE_GOAL:
            {
                updateLieProneGoal((LieProneGoal *) goal, deltaTime);
                break;
            }
            case STOP_GOAL:
            {
                updateStopGoal((StopGoal *) goal, deltaTime);
                break;
            }
            case OBJECTIVE_WORK_GOAL:
            case ENTITY_WORK_GOAL:
            {
                updateWorkGoal((WorkGoal *) goal, deltaTime);
                break;
            }
            case RELOAD_WEAPON_GOAL:
            {
                updateReloadGoal((ReloadWeaponGoal *) goal, deltaTime);
                break;
            }
            case ATTACK_TARGET_GOAL:
            {
                updateAttackTargetGoal((AttackTargetGoal *) goal, deltaTime);
                break;
            }
            case ATTACK_POSITION_GOAL:
            {
                updateAttackPositionGoal((AttackPositionGoal *) goal, deltaTime);
                break;
            }
        }
    }
}

void Human::updateMoveGoal(MoveGoal * move, float deltaTime)
{
    if((globalTestBed->behaviorsToSkip & MOVE_GOAL_BEHAVIOR))
    {
        return;
    }
    if(move->getNeedsOptimization())
    {
        if(!move->getWaitingForOptimize())
        {
            move->setWaitingForOptimize(true);
            move->retain();
            globalBackgroundTaskHandler->addPathfindingTask([move, this] ()
            {
                Vec2 position = move->getPosition();
                position = world->getCollisionGrid(position)->getValidMoveCellAroundPosition(position);
                CollisionCell * goalCell = world->getCollisionGrid(position)->getCellForCoordinates(position);
                CollisionCell * optimum = getOptimumCellAroundCell(goalCell);
                if(optimum != nullptr)
                {
                    goalCell = optimum;
                }
                move->setPosition(goalCell->getCellPosition());
                move->setNeedsOptimization(false);
                move->setWaitingForOptimize(false);
                move->release();
                return false;
            });
        }

    }
    else
    {
        if(_movingTowardGoal)
        {
            return;
        }
        _movingTowardGoal = true;
        if(_goalPosition.position != move->getPosition())
        {
            setGoalPosition( move->getPosition(), false);
        }
        else if(atGoalPosition())
        {
            _movingTowardGoal = false;
            handleGoalComplete(move);
        }
    }
}

void Human::updateLieProneGoal(LieProneGoal * move, float deltaTime)
{
    if(!_workingOnGoal || !_movingTowardGoal)
    {
        _shouldLieProne = true;
    }
}

void Human::updateStopGoal(StopGoal * stop, float deltaTime)
{
    if(_movingTowardGoal)
    {
        return;
    }
    
    setGoalPosition(_position);
    handleGoalComplete(stop);
}

void Human::updateWorkGoal(WorkGoal * goal, float deltaTime)
{
    if((globalTestBed->behaviorsToSkip & WORK_GOAL_BEHAVIOR))
    {
        return;
    }
    if(_movingTowardGoal)
    {
        return;
    }
    
    PhysicalEntity * workEnt = (PhysicalEntity *) globalEntityManager->getEntity(_currentWorkEntity);

    // Do we need a new object to work on?
    if(workEnt == nullptr || !goal->containsEntity(workEnt))
    {
        workEnt = goal->getEntityNearestPosition(getPosition());
        if(workEnt == nullptr)
        {
            handleGoalComplete(goal);
            _currentWorkEntity = -1;
            return;
        }
        else
        {
            _currentWorkEntity = workEnt->uID();
            _workPositionOffset = workEnt->claimWorkPosition(this);
        }
    }
        
    // work is complete, throw out the current work entity and return
    if(workEnt->isWorkComplete())
    {
        workEnt->releaseWorkPosition(this);
        goal->workCompleteForEntity(workEnt);
        _currentWorkEntity = -1;
        return;
    }
    
    // See if we need to move toward the workEnt
    _movingTowardGoal = true;
    bool inWorkRange = moveTowardWorkEnt(workEnt);
    // If we are able to work, and we are at the work postion, then work
    if(!_shouldLieProne && !_workingOnGoal && inWorkRange) //TODO fix magic number
    {
        _workingOnGoal = true;
        workEnt->addWork(deltaTime);
        
        if(_velocity.isZero())
        {
            setFacing(VectorMath::facingFromVector(workEnt->getPosition() - getPosition()));
        }
        
        if(workEnt->getEntityType() == PROPOSED_TRENCH ||
           workEnt->getEntityType() == PROPOSED_BUNKER ||
           workEnt->getEntityType() == PROPOSED_COMMAND_POST)
        {
            setPerformingAction(ACTION_DIG);
            _timeSinceLastVisibleAction = 0;
        }
        else if(workEnt->getEntityType() == CREW_WEAPON)
        {
            setPerformingAction(ACTION_HELP);
        }
    }
    return;
}

bool Human::moveTowardWorkEnt(PhysicalEntity * workEnt)
{
    if(workEnt->getAverageVelocity().x != 0 || workEnt->getAverageVelocity().y != 0)
    {
        float angle = workEnt->getVelocity().getAngle(Vec2(1,0));
        _workPositionOffset.rotate(Vec2(0,0), angle * D2R);
    }
    _workPosition = _workPositionOffset + workEnt->getPosition();
    
    if(_goalPosition.position.distanceSquared(_workPosition) >= POSITIONS_SAME_DISTANCE)
    {
        // to far away from the work ent, so move toward it
        setGoalPosition(_workPosition);
    }
    
    return _position.distanceSquared(_workPosition) <= POSITIONS_SAME_DISTANCE_SQ;
}



void Human::handleGoalComplete(Goal * goal)
{
    for(auto goalPair : _goals)
    {
        if(goalPair.second == goal)
        {
            _goals.erase(goalPair.first);
            return;
        }
    }
}


void Human::updateStress()
{
    if((globalTestBed->behaviorsToSkip & UPDATE_STRESS_BEHAVIOR))
    {
        return;
    }
    STRESS_STATE previous = _currentStressState;
    
    switch (previous) {
        case STRESS_PANIC:
        {
            if(_stress < globalVariableStore->getVariable(ImmediateCoverStressLevel) * 0.9)
            {
                _currentStressState = STRESS_SEEK_COVER;
            }
            break;
        }
        case STRESS_SEEK_COVER:
        {
            if(_stress > globalVariableStore->getVariable(ImmediateCoverStressLevel))
            {
                _currentStressState = STRESS_SEEK_COVER;
            }
            else if(_stress < globalVariableStore->getVariable(SeekCoverStressLevel) * 0.9)
            {
                _currentStressState = STRESS_CALM;
            }
            break;
        }
        case STRESS_CALM:
        {
            if(_stress > globalVariableStore->getVariable(SeekCoverStressLevel))
            {
                _currentStressState = STRESS_SEEK_COVER;
            }
            break;
        }
        default:
            break;
    }
    
    bool stressStateChanged = previous != _currentStressState;
    if(stressStateChanged && previous != STRESS_PANIC)
    {
        switch (_currentStressState)
        {
            case STRESS_PANIC:
            {
                // TODO fix this shoud be some kind of retreat, mabye towards a nearby commandpost or something?
                if(_goals.at(GOAL_ORIGIN_COVER) == nullptr)
                {
                    Vec2 position = _position;
                    if(!_velocity.isZero())
                    {
                        position += VectorMath::unitVectorFromFacing(_facing) * 5;
                    }
                    MoveGoal * cover = MoveGoal::create(position);
                    cover->setImportance(7);
                    _goals.insert(GOAL_ORIGIN_COVER, cover);
                }
                if(_goals.at(GOAL_ORIGIN_PRONE) == nullptr)
                {
                    _goals.insert(GOAL_ORIGIN_PRONE, LieProneGoal::create());
                }

                _goals.at(GOAL_ORIGIN_PRONE)->setImportance(5);

                break;
            }
            case STRESS_SEEK_COVER:
            {
                if(_goals.at(GOAL_ORIGIN_COVER) == nullptr)
                {
                    Vec2 position = _position;
                    if(!_velocity.isZero())
                    {
                        position += VectorMath::unitVectorFromFacing(_facing) * 5;
                    }
                    MoveGoal * cover = MoveGoal::create(position);
                    cover->setImportance(7);
                    _goals.insert(GOAL_ORIGIN_COVER, cover);
                }
                if(_goals.at(GOAL_ORIGIN_PRONE) == nullptr)
                {
                    _goals.insert(GOAL_ORIGIN_PRONE, LieProneGoal::create());
                }
                
                _goals.at(GOAL_ORIGIN_PRONE)->setImportance(5);
                break;
            }
            case STRESS_WORRIED:
            {
                if(_goals.at(GOAL_ORIGIN_PRONE) != nullptr)
                {
                    _goals.at(GOAL_ORIGIN_PRONE)->setImportance(3);
                }
                break;
            }
            case STRESS_CALM:
            {
                _goals.erase(GOAL_ORIGIN_PRONE);
                break;
            }
            default:
                break;
        }
    }
}

bool Human::isPerformingAction()
{
    return _performingAction != ACTION_NONE;
}

bool Human::isPerformingVisibleAction()
{
    return _timeSinceLastVisibleAction < 1.0;
}



// //////////////////////////////////////////
bool Human::canSeeOtherHuman(Human * other, int & maximumHeightToTarget)
{
    if(other->getCurrentCollisionGrid()->getGridId() != getCurrentCollisionGrid()->getGridId())
    {
        return false;
    }
    //Can't see behind us if we are moving
    if(!_velocity.isZero())
    {
        DIRECTION enemyFacing = VectorMath::facingFromVector(other->getPosition() - getPosition());
        if(abs( VectorMath::facingDifference(getFacing(),enemyFacing)) > 2)
        {
            return false;
        }
    }
    CollisionCell * start = getCurrentCollisionGrid()->getCellForCoordinates(getPosition());
    CollisionCell * end = other->getCurrentCollisionGrid()->getCellForCoordinates(other->getPosition());
    
    maximumHeightToTarget = std::max(start->getTerrainCharacteristics().getHeight(), end->getTerrainCharacteristics().getHeight());
    
    int distance = start->getVectorToOtherCell(end).length();
    
    
    float accumulatedCover = other->getConcealmentValue(_inTerrain.getHeight());
    
    float maximumCover = other->isProne() ? 25 : 75; // todo FIX add some nuance here or make it a game variable or something
    if(other->isMoving() || other->isPerformingVisibleAction())
    {
        maximumCover *= 2;
    }
    if(accumulatedCover > maximumCover)
    {
        return false;
    }
    
    accumulatedCover -= (other->getConcealmentValueWithoutHeight() + 50);
    Vec2 endPosition = other->getPosition();

    // Not visible if a friendly unit or a static entity is in the way (like a tree)
    bool canSee = getCurrentCollisionGrid()->iterateCellsInLine(getPosition(), endPosition, [this, &accumulatedCover, maximumCover, &endPosition] (CollisionCell * cell, float distanceTraveled, bool minorCell)
    {
        accumulatedCover += cell->getConcealmentInCell();
        // Need to handle clouds somehow
        if(accumulatedCover > maximumCover)
        {
            endPosition = cell->getCellPosition() + Vec2(.5,.5);
            return false;
        }
        return true;
    });
    
    std::unordered_set<ENTITY_ID> sectors;
    std::unordered_set<Cloud *> cloudsEncountered;
    Vec2 startPosition = getPosition();
    globalSectorGrid->iterateSectorsInLine(startPosition, endPosition, [startPosition, endPosition, &sectors, &accumulatedCover, maximumCover, &cloudsEncountered] (MapSector * sector)
    {
        sectors.insert(sector->uID());
        for(auto cloud : sector->getCloudsInSector())
        {
            if(cloudsEncountered.find(cloud.second) == cloudsEncountered.end())
            {
                cloudsEncountered.insert(cloud.second);
                
                Vec2 intersect1, intersect2;
                auto intersections = VectorMath::getIntersectionBetweenLineSegmentAndCircle(startPosition, endPosition, cloud.second->getPosition(), cloud.second->getRadius(), intersect1, intersect2);
                if(intersections.first || intersections.second)
                {
                    accumulatedCover += intersect1.distance(intersect2) * cloud.second->getDensity();
                }
            }
        }
        if(accumulatedCover > maximumCover)
        {
            return false;
        }
        return true;
    });
    _sectorsVisibleMutex.lock();
    _sectorsVisible.insert(sectors.begin(), sectors.end());
    _sectorsVisibleMutex.unlock();

    return canSee;
}

bool Human::doLineVisibilityTestWithCells(Vec2 start, Vec2 endPosition, float &accumulatedCover, float maximumCover)
{
    auto grid = getCurrentCollisionGrid();
    CollisionCell * startCell = grid->getCellForCoordinates(start);
    Vec2 originalEnd = endPosition;

    bool canSee = grid->iterateCellsInLine(start, endPosition, [this, &accumulatedCover, maximumCover, &endPosition] (CollisionCell * cell, float distanceTraveled, bool minorCell)
    {
        accumulatedCover += cell->getConcealmentInCell();
        if(accumulatedCover > maximumCover)
        {
            endPosition = cell->getCellPosition() + Vec2(.5,.5);
            return false;
        }
        return true;
    });
    
    std::unordered_set<ENTITY_ID> sectors;
    std::unordered_set<Cloud *> cloudsEncountered;
    Vec2 startPosition = getPosition();
    globalSectorGrid->iterateSectorsInLine(startPosition, endPosition, [startPosition, endPosition, &sectors, &accumulatedCover, maximumCover, &cloudsEncountered] (MapSector * sector)
    {
        sectors.insert(sector->uID());
        for(auto cloud : sector->getCloudsInSector())
        {
            if(cloudsEncountered.find(cloud.second) == cloudsEncountered.end())
            {
                cloudsEncountered.insert(cloud.second);
                
                Vec2 intersect1, intersect2;
                auto intersections = VectorMath::getIntersectionBetweenLineSegmentAndCircle(startPosition, endPosition, cloud.second->getPosition(), cloud.second->getRadius(), intersect1, intersect2);
                if(intersections.first || intersections.second)
                {
                    accumulatedCover += intersect1.distance(intersect2) * cloud.second->getDensity();
                }
            }
        }
        if(accumulatedCover > maximumCover)
        {
            return false;
        }
        return true;
    });
    
    _sectorsVisibleMutex.lock();
    _sectorsVisible.insert(sectors.begin(), sectors.end());
    _sectorsVisibleMutex.unlock();
    return canSee;
}

void Human::getLastVisibleSectors(std::unordered_set<ENTITY_ID> & sectorsVisible)
{
    _sectorsVisibleMutex.lock();
    sectorsVisible.insert(_sectorsVisible.begin(), _sectorsVisible.end());
    _sectorsVisible.clear();
    _sectorsVisibleMutex.unlock();
}

void Human::addGoal(GOAL_ORIGIN origin, Goal * goal)
{
    _pendingGoals.insert(origin, goal);
}

void Human::addGoalFromOrder(Order * order)
{
    Goal * goal;
    if(order != nullptr)
    {
        switch(order->getOrderType())
        {
            case OCCUPY_ORDER:
            {
                OccupyOrder * move = (OccupyOrder *) order;
                goal = MoveGoal::create(move->getPosition() + _unitOffset, move->shouldIgnoreCover());
                break;
            }
            case MOVE_ORDER:
            {
                MoveOrder * move = (MoveOrder *) order;
                goal = MoveGoal::create(move->getPosition(), move->shouldIgnoreCover());
                break;
            }
            case OBJECTIVE_WORK_ORDER:
            {
                ObjectiveWorkOrder * objWork = (ObjectiveWorkOrder *) order;
                goal = ObjectiveWorkGoal::create( objWork->getWorkObjects());
                break;

            }
            case ENTITY_WORK_ORDER:
            {
                EntityWorkOrder * objWork = (EntityWorkOrder *) order;
                goal = EntityWorkGoal::create( objWork->getWorkObject());
                break;
            }
            case ATTACK_POSITION_ORDER:
            {
                AttackPositionOrder * attack = (AttackPositionOrder *) order;
                goal = AttackPositionGoal::create(attack->getTargetObjective());
                break;
            }
        }
    }
    goal->setImportance(1);
    goal->setAssociatedOrderId(order->uID());
    _pendingGoals.insert(GOAL_ORIGIN_ORDER, goal);
}

void Human::addHelpGoal(Goal * goal, bool helpReload)
{
    if(helpReload)
    {
        _pendingGoals.insert(GOAL_ORIGIN_RELOAD_CREW, goal);
    }
    else
    {
        _pendingGoals.insert(GOAL_ORIGIN_HELP, goal);
    }
}

void Human::clearUnitGoal()
{
    StopGoal * stop = StopGoal::create();
    addGoal(GOAL_ORIGIN_ORDER, stop);
}

void Human::receiveDamage(int damage)
{
    _pendingHealthChange += damage;
}

float Human::getPercentOfMaximumExperience()
{
    return _experience / MAXIMUM_EXPERIENCE;
}

void Human::addStress(float stress)
{
    _pendingStress += stress;
}



void Human::die()
{
    if(_lifeState == DEAD)
        return;
    _lifeState = DEAD;

    
    for(auto cell : _collisionCellMembership)
    {
        cell.second->removeMovingPhysicalEntity(this); //Need to be in acting state for this
    }
    _collisionCellMembership.clear();
    
    if(_currentMapSector != nullptr)
    {
        _currentMapSector->removeEntity(this);
    }
    lastUpdateTime = globalEngagementManager->getEngagementTime();
//    _sprite->stopAllActions(); TODO FIX verify this isn't needed
    _sprite->setAnimation(DIE, false, _facing);
    
    if(_claimedPosition != nullptr)
    {
        _owningTeam->releasePositionClaim(_claimedPosition);
    }
    
    _pendingGoals.clearChange();
    _pendingStress.clearChange();
    _pendingGoalPosition.clearChange();
    _pendingExperience.clearChange();
    _pendingHealthChange.clearChange();
}

void Human::removeFromGame()
{    
    if(_lifeState != DEAD)
    {
        Human::die();
    }
    MovingPhysicalEntity::removeFromGame();
}


int Human::hasUpdateToSendOverNetwork()
{
    int variables = MovingPhysicalEntity::hasUpdateToSendOverNetwork();
    
    if(_humanNetworkUpdate.shouldDie != _shouldDie)
    {
        variables++;
    }
    if(_humanNetworkUpdate.takingCoverBehindEnt != _takingCoverBehindEnt)
    {
        variables++;
    }
    if(_humanNetworkUpdate.stress != _stress)
    {
        variables++;
    }    
    return variables;
}

void Human::saveNetworkUpdate(cereal::BinaryOutputArchive & archive)
{
    MovingPhysicalEntity::saveNetworkUpdate(archive);
    unsigned char modifiedVars = 0;
    if(_humanNetworkUpdate.shouldDie != _shouldDie)
    {
        modifiedVars += 1;
    }
    if(_humanNetworkUpdate.takingCoverBehindEnt != _takingCoverBehindEnt)
    {
        modifiedVars += 2;
    }
    if(_humanNetworkUpdate.stress != _stress)
    {
        modifiedVars += 4;
    }
    
    archive(modifiedVars);
    
    if(_humanNetworkUpdate.shouldDie != _shouldDie)
    {
        archive(_shouldDie);
        _humanNetworkUpdate.shouldDie = _shouldDie;
    }
    if(_humanNetworkUpdate.takingCoverBehindEnt != _takingCoverBehindEnt)
    {
        archive(_takingCoverBehindEnt);
        _humanNetworkUpdate.takingCoverBehindEnt = _takingCoverBehindEnt;
    }
    if(_humanNetworkUpdate.stress != _stress)
    {
        archive(_stress);
        _humanNetworkUpdate.stress = _stress;
    }
}

void Human::updateFromNetwork(cereal::BinaryInputArchive & archive)
{
    MovingPhysicalEntity::updateFromNetwork(archive);
    unsigned char modifiedVars;
    archive(modifiedVars);
    
    if(modifiedVars & 1)
    {
        archive(_shouldDie);
    }
    if(modifiedVars & 2)
    {
        archive(_takingCoverBehindEnt);
    }
    if(modifiedVars & 4)
    {
        archive(_stress);
    }
}



void Human::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    MovingPhysicalEntity::saveToArchive(archive);
    
    archive((int) _lifeState);
    
    archive(_health);
    archive(_maxHealth);
    archive(_sightRadius);
    archive(_pendingHealthChange);
    
    archive(_goals.size());
    for(auto goalPair : _goals)
    {
        archive(goalPair.first);
        archive(goalPair.second->getGoalType());
        goalPair.second->saveToArchive(archive);
    }
    
    archive(_pendingGoals.size());
    auto pendingGoals = _pendingGoals.get();
    for(auto goalPair : pendingGoals)
    {
        archive(goalPair.first);
        archive(goalPair.second->getGoalType());
        goalPair.second->saveToArchive(archive);
    }
    
    archive(_shouldDie);
    archive(_experience);
    archive(_stress);
    archive((int) _currentStressState);
    archive(_pendingStress);
    archive(_pendingExperience);
    archive(_unitOffset);

    archive(_workPosition);
    archive(_workPositionOffset);
    archive(_currentWorkEntity);
    
    archive(_takingCoverBehindEnt);
    archive(_timeSinceLastVisibleAction);
    archive(lastUpdateTime);
}


void Human::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    MovingPhysicalEntity::loadFromArchive(archive);
    int state;
    archive(state);
    _lifeState = (LIFE_STATE) state;
    archive(_health);
    archive(_maxHealth);
    archive(_sightRadius);
    archive(_pendingHealthChange);

    size_t size;
    archive(size);
    for(int i = 0; i < size; i++)
    {
        GOAL_ORIGIN origin;
        GOAL_TYPE type;
        archive(origin);
        archive(type);
        Goal * goal = Goal::createGoalOfType(type);
        goal->loadFromArchive(archive);
        _goals.insert(origin, goal);
    }
    
    archive(size);
    for(int i = 0; i < size; i++)
    {
        GOAL_ORIGIN origin;
        GOAL_TYPE type;
        archive(origin);
        archive(type);
        Goal * goal = Goal::createGoalOfType(type);
        goal->loadFromArchive(archive);
        _pendingGoals.insert(origin, goal);
    }
    
    archive(_shouldDie);
    archive(_experience);
    archive(_stress);
    int stressState;
    archive(stressState);
    _currentStressState = (STRESS_STATE) stressState;
    
    archive(_pendingStress);
    archive(_pendingExperience);
    archive(_unitOffset);
    
    archive(_workPosition);
    archive(_workPositionOffset);
    
    archive(_currentWorkEntity);
    
    archive(_takingCoverBehindEnt);
    archive(_timeSinceLastVisibleAction);
    archive(lastUpdateTime);
}

void Human::populateDebugPanel(GameMenu * menu, Vec2 location)
{
    MovingPhysicalEntity::populateDebugPanel(menu, location);
    std::string text;
    
    std::ostringstream outStream;
    outStream << std::fixed;
    outStream.precision(1);
    outStream << "Health: " << _health;
    text += outStream.str();
    menu->addText(text);

    menu->addSeperater();
    for(auto goal : _goals)
    {
        outStream.str("");
        text.clear();
        outStream << "Goal: " << goal.second->getGoalTypeAsString();
        outStream << " I: " << goal.second->getImportance();
        switch(goal.second->getGoalType())
        {
            case MOVE_GOAL:
            {
                MoveGoal * move = (MoveGoal *) goal.second;
                outStream << " Pos: " << move->getPosition().x << ", " << move->getPosition().y;
                break;
            }
            default:
                break;
        }
        text += outStream.str();
        menu->addText(text);
    }
    menu->addSeperater();

    text.clear();
    text = "Work Ent: ";
    if(_currentWorkEntity == -1)
    {
        text += "N/A";
    }
    else
    {
        outStream.str("");
        outStream << _currentWorkEntity;
        text += outStream.str();
        
    }
    menu->addText(text);
    text.clear();
    
    switch(_performingAction)
    {
        case ACTION_NONE:
        {
            text = "No Action";
            break;
        }
            
        case ACTION_ATTACK: {
            text = "Attacking";
            break;
        }
        case ACTION_RELOADING: {
            text = "Reloading";
            break;
        }
        case ACTION_DIG: {
            text = "Digging";
            break;
        }
        case ACTION_HELP: {
            text = "Helping";
            break;
        }
    }
    menu->addText(text);
    
    text.clear();
    outStream.str("");
    outStream << "Stress: " << _stress;
    text += outStream.str();
    menu->addText(text);
    
    if(_lifeState == ALIVE)
    {
        menu->addText("Alive");
    }
    else
    {
        menu->addText("Dead");
    }
    menu->addSeperater();
}

void Human::updateTestInfo()
{
    std::ostringstream os;
    os << _id;
    
    os << " " << getPosition().x << " " << getPosition().y;
    if(_primaryCollisionCell != nullptr)
    {
        os << " " << _primaryCollisionCell->getXIndex() << " " << _primaryCollisionCell->getYIndex() << " " << _primaryCollisionCell->getCellPosition().x << " " << _primaryCollisionCell->getCellPosition().y;
        
        StaticEntity * object = _primaryCollisionCell->getStaticEntity();
        if(object != nullptr)
        {
            os << " " << object->getPosition().x << " " << object->getPosition().y;
        }
    }
    
    
    std::string info = os.str();
    
    testText->setString(os.str());
    testText->setPosition(getPosition());
}
