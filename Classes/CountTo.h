//
//  Counter.hpp
//  TrenchWars
//
//  Created by Paul Reed on 5/18/20.
//

#ifndef CountTo_h
#define CountTo_h

#include "Random.h"

class CountTo
{
private:
    int _currentCount;
    int _countTo;
public:
    CountTo(int countTo, bool randomStart = false);
    bool count();
    void setToMaximum();
    void reset();
    
    template<class A>
    void serialize(A & archive)
    {
        archive(_currentCount);
        archive(_countTo);
    }
};

#endif /* CountTo_h */
