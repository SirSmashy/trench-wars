//
//  MapSector.h
//  TrenchWars
//
//  Created by Paul Reed on 10/10/12.
//  Copyright (c) 2012 . All rights reserved.
//

#ifndef __MAP_SECTOR_H__
#define __MAP_SECTOR_H__

#include "cocos2d.h"
#include "CollisionGrid.h"
#include "Refs.h"
#include "MovingPhysicalEntity.h"
#include "NavigableRect.h"

class StaticEntity;
class MapSectorAnalysis;
class Cloud;

struct SectorVisiblity : public Ref
{
    std::unordered_set<ENTITY_ID> entityAdditions;
    std::unordered_set<ENTITY_ID> entityRemovals;
    std::unordered_set<ENTITY_ID> currentlyOrPreviouslyVisibleEnts;
    bool currentlyVisible;
};

class MapSector : public ComparableRef
{
private:
    CollisionCell *** _collisionCellsInSector;
    
    NavigableRect _cellsRect;
    int     _mapSectorIndexX;
    int     _mapSectorIndexY;
    int     _sectorWidthInCells;

    Rect  _sectorBounds;
    std::vector<std::unordered_set<MovingPhysicalEntity *>> _teamStrengthsInSector;
    std::vector<SectorVisiblity *> _teamVisiblityForSector;
    std::mutex _modificationMutex;
    CollisionCell * _pathingCell;
    
    std::unordered_map<ENTITY_ID, Cloud *> _cloudsInSector;
    
    double _congestionInSector [10];
    double _congestionPerPerson;
    
    float _totalSecurity;
    std::vector<float> _availableTotalSecurityForTeam;
    
    std::unordered_map<MapSector *, double> _neighboringPaths;
    double _rebuildPathTime;
    
    void findPathsToNeighbors();
    
    long long _lastUpdateFrame;
    
protected:
    void setPathToNeighbor(MapSector * neighbor, double cost);
    
    std::list<Vec2> getIntersectionsWithLine(Vec2 start, Vec2 end);
    CollisionCell * getEdgeCellIntersectingLine(Vec2 start, Vec2 end);

    void findConcealmentAlongLine(CollisionCell * start, CollisionCell * end);

public:
    MapSector(int indexX, int indexY, int xCellSize, int yCellSize, int cellsInSector);
    static MapSector * create(int indexX, int indexY, int xCellSize, int yCellSize, int cellsInSector);
    
    void addTeam();
    CollisionCell * getCell(int x, int y);
    
    // Geography related
    int getXIndex() {return _mapSectorIndexX;}
    int getYIndex() {return _mapSectorIndexY;}
    
    Vec2 getSectorOrigin() {return _sectorBounds.origin;}
    Rect getSectorBounds() {return _sectorBounds;}
    
    void constrainPointToSector(Vec2 & point);
    
    float gridDistanceFromSector(MapSector * other);

    NavigableRect & getCellsRect() {return _cellsRect;}
    CollisionCell * getSectorCellNearestCell(CollisionCell * other);
    CollisionCell * getMiddleCell();

    std::mutex & getModificationMutex() {return _modificationMutex;}

    // Entities
    void addEntity(MovingPhysicalEntity * ent);
    void removeEntity(MovingPhysicalEntity * ent);
    const std::unordered_set<MovingPhysicalEntity *> & getEntitiesForTeam(int team);
    
    // Static ents
    void staticEntAddedToSector(StaticEntity * ent);
    void staticEntRemovedFromSector(StaticEntity * ent);
    
    // Claimed positions
    void entityClaimedCellInSector(int entTeamId, CollisionCell * cell);
    void entityReleasedClaimInSector(int entTeamId, CollisionCell * cell);
    float getAvailableTotalSecurityForTeam(int teamId);
    
    // Clouds
    void addCloudToSector(Cloud * cloud);
    void removeCloudFromSector(Cloud * cloud);
    const std::unordered_map<ENTITY_ID, Cloud *> & getCloudsInSector() {return _cloudsInSector;}
    
    // Pathing related
    void buildPathToNeighbor(MapSector * neighbor);
    void rebuildNeighborPaths(bool recurse = false);
    const std::unordered_map<MapSector *, double> & getPathsToNeighbors() {return _neighboringPaths;}
    CollisionCell * getPathingCell() {return _pathingCell;}

    // Congestion
    void addCongestion(int team);
    void removeCongestion(int team);
    double getCongestionForTeam(int team);
    CollisionCell * getSectorBorderCellWithLeastCongestion(CollisionCell * other, int team);

    // Security
    void changeTotalSecurity(float change);
    float getTotalSecurity() {return _totalSecurity;}
    
    //
    int getStrengthForTeam(int team);
    int getStrengthForTeamsOtherThanTeam(int team);

    //
    SectorVisiblity * getSectorVisibilityForTeam(int team);
    void markSectorVisibleForTeam(int team);
    void markSectorHiddenForTeam(int team);
    
    void shutdown();
};


#endif //__MAP_SECTOR_H__
