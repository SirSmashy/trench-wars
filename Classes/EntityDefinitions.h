//
//  EntityDefinitions.h
//  TrenchWars
//
//  Created by Paul Reed on 2/28/21.
//

#ifndef EntityDefinitions_h
#define EntityDefinitions_h

#include "cocos2d.h"
#include "Refs.h"
#include <vector>

USING_NS_CC;

enum ENTITY_TYPE
{
    INFANTRY,
    COURIER,
    CREW_WEAPON,
    COMMANDPOST,
    BUNKER,
    PROJECTILE,
    UNIT,
    CREW_WEAPON_UNIT,
    STATIC_ENTITY,
    PROPOSED_TRENCH,
    PROPOSED_COMMAND_POST,
    PROPOSED_BUNKER,
    OBJECTIVE_ENTITY,
    DEBUG_ENTITY,
    CLOUD_ENTITY,
    OTHER,
};

enum WEAPON_PROJECTILE_TYPE
{
    DIRECT,
    INDIRECT_LOW,
    INDIRECT_HIGH,
    MELEE,
    SHRAPNEL
};


//FIXME: There has got to be a better way to do this...
enum ANIMATION_TYPE
{
    DEFAULT,
    ////// repeating animations
    MOVE,
    PRONE_IDLE,
    PRONE_MOVE,
    WORK,
    DIG,
    ///////single run animations
    FIRE,
    DIE,
    
    EMBLEM_DEFAULT,
    EMBLEM_BACKGROUND_DIAMOND,
    EMBLEM_BACKGROUND_CIRCLE,
    EMBLEM_INFANTRY,
    EMBLEM_MG,
    EMBLEM_ARTY,
    EMBLEM_ORDER,
    EMBLEM_DIG,
    EMBLEM_ATTACK,
    EMBLEM_MOVE,
    EMBLEM_STATUS_CASUALTY_25,
    EMBLEM_STATUS_CASUALTY_50,
    EMBLEM_STATUS_CASUALTY_75,
    EMBLEM_STATUS_LOW_AMMO,
    EMBLEM_STATUS_NO_COMMAND,
    EMBLEM_STATUS_STRESSED,
    
    LINE_SOLID,
    LINE_DASHED,
    LINE_DOTTED,
    
    LANDSCAPE_DAMAGED_1,
    LANDSCAPE_DAMAGED_2,
    LANDSCAPE_DAMAGED_3,
    LANDSCAPE_DAMAGED_4,
    LANDSCAPE_DAMAGED_5,
    LANDSCAPE_DESTROYED,
};

struct ParticleDefinition
{
    std::string name = "";
    Vec2 offset = Vec2::ZERO;
    float height = 0.0;
    bool particlePerCell = 0.0;
    bool permanent = false;
};

struct ObjectDefinition : public Ref
{
    std::string _entityName = "";
    size_t _hashedName = 0;
};


struct CloudDefinition : public ObjectDefinition
{
    std::string particleName = "";
    float burstRadiusChangePerSecond;
    float burstTime;
    float lifeSpan;
    float initialDensity;
    float densityChangePerSecond;
    float densityToAlphaMultiple;
    float enduringRadiusChangePerSecond;
    float particleCount;
    float particleSizeToRadiusRatio;
};

struct PhysicalEntityDefinition : public ObjectDefinition
{
    int _workTime = 0;
    std::vector<Vec2> workPositions;
    std::string _spriteName = "";
    ENTITY_TYPE _type = OTHER;
    
    Vec2 _spriteSize = Vec2(-1,-1); //use native sprite size by default
    Vec2 _collisionSize = Vec2(1,1);

    std::string getLowerCaseSpriteName()
    {
        std::string lower = _spriteName;
        std::transform(lower.begin(), lower.end(), lower.begin(), ::tolower);
        return lower;
    }

};

struct MovingPhysicalEntityDefinition : public PhysicalEntityDefinition
{
    float _speed;
};

struct ProjectileDefinition : public PhysicalEntityDefinition
{
    bool    _explosive = false;
    bool    _airBurst = false;
    float   _burstAngle = 360;
    float   _burstHeight = 0;
    int     _damage;
    float   _speed = 1000;
    int   _ammoCost = 1;
    
    std::string  _detonateParticleName;
    std::string  _smokeParticleName;
    
    ProjectileDefinition()
    {
        _type = PROJECTILE;
    }
};

struct StaticEntityDefinition : public PhysicalEntityDefinition
{
    int _height = 0;
    int _cover = 0;
    int _hardness = 0;
    bool _impassable = false;
    int _health = -1;
    bool _allowRotation = false;
    Color4B _mapColor;
    
    std::vector<ParticleDefinition> _spawnParticles;

    std::string _spawnOnDeath;
    ANIMATION_TYPE _animationName;
    StaticEntityDefinition()
    {
        _type = STATIC_ENTITY;
    }
};


struct HumanDefinition : public MovingPhysicalEntityDefinition
{
    int _health;
    float _sightRadius = 1000;
};


struct WeaponDefinition : public ObjectDefinition
{
    float _rechamberRate = 1.0;
    float _reloadRate = 1.0;
    float _readyRate = 1.0;
    float _recoilPerShot = 1.0;
    int _magazineSize = 1;
    WEAPON_PROJECTILE_TYPE _type;
    int _meleeDamage = 100;
    ProjectileDefinition * projectile = nullptr;
    
    float _maximumRange = -1;
    float _minimumRange = 0;
    std::string _muzzleParticleName;
    
    int _preferenceOrder = 1;
    int _imprecisionMOA = 1.0;
};

struct CrewWeaponDefinition : public MovingPhysicalEntityDefinition
{
public:
    WeaponDefinition * _weapon = nullptr;
    int  _crewWeaponAmmo = 10;
    Vec2 _muzzlePositionOffset = Vec2::ZERO;

    CrewWeaponDefinition()
    {
        _type = CREW_WEAPON;
    }
};

struct InfantryDefinition : public HumanDefinition
{
public:
    float _targetAquireSpeed = 1.0;
    float _chargeRadius = 0;
    int  _ammoReserve = 10;
    Vec2 _muzzlePositionOffset = Vec2::ZERO;
    Vector<WeaponDefinition *> _weapons;
    
    InfantryDefinition()
    {
        _type = INFANTRY;
    }
};

struct UnitMemberDefinition : public ObjectDefinition
{
public:
    float _xOffset;
    float _yOffset;
    std::string _memberName;
};

struct UnitDefinition : public ObjectDefinition
{
public:
    Vector<UnitMemberDefinition *> _members;
    int _strength;
    int _type;
    int _supplyCost;
    int _crewWeaponAmmoReserve = 0;
    std::string _spriteName;
    std::string _crewWeapaon = "";
};

struct PolygonLandscapeObjectDefiniton : public ObjectDefinition
{
    std::string _textureName;
    float _textureXSize = 1.0;
    float _textureYSize = 1.0;
    Color4B _mapColor;

    std::string _boundaryTextureName;
    float _boundaryTextureSize = 1.0;

    int _height = 0;
    int _cover = 0;
    int _hardness = 0;
    bool _impassable = false;
};

class UnitSpawn : public ObjectDefinition
{
public:
    Vec2 location;
    std::string unitDefinitionName;
};

class FormationDefinition : public ObjectDefinition
{
public:
    std::string name;
    Vector<UnitSpawn *> units;
};

#endif /* EntityDefinitions_h */
