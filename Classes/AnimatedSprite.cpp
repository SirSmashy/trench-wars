//
//  AnimatedSprite.cpp
//  TrenchWars
//
//  Created by Paul Reed on 9/19/20.
//

#include "AnimatedSprite.h"
#include "AnimationWithParticle.h"
#include "CollisionGrid.h"



EntityAnimation * EntityAnimationSet::getAnimation(ANIMATION_TYPE type, DIRECTION direction)
{
    auto it = _animationDictionary.find(type);
    if(it != _animationDictionary.end())
    {
        auto dirIt = it->second->find(direction);
        if(dirIt != it->second->end())
        {
            return dirIt->second;
        }
    }
    return nullptr;
}

EntityAnimation * EntityAnimationSet::getFirstValidAnimation()
{
    for(auto set : _animationDictionary)
    {
        for(auto anim : *(set.second))
        {
            if(anim.second->_animation->getFrames().size() > 0)
            {
                return anim.second;
            }
        }
    }
    return nullptr;
}



bool AnimatedSprite::init(EntityAnimationSet * spriteBatch, Vec2 position)
{
    EntityAnimation * entAnimation = spriteBatch->getAnimation(DEFAULT, NONE);
    _positionOffset = spriteBatch->_positionOffset;
    if(entAnimation == nullptr)
    {
        entAnimation = spriteBatch->getFirstValidAnimation();
        if(entAnimation == nullptr)
        {
            LOG_ERROR("Invalid sprite batch! \n");
        }
    }
    
    AnimationFrame * frame = entAnimation->_animation->getFrames().at(0);
    if(frame == nullptr)
    {
        CCLOG("Sprite Animation does not contain any frames, cannot create Entity ");
        return false;
    }
    if(initWithSpriteFrame(frame->getSpriteFrame()))
    {
        setPosition(position);
        _spriteBatch = spriteBatch;
        _spriteSize.width = -1;
        setAnimation(DEFAULT, false);
        return true;
    }
    return false;
}

AnimatedSprite * AnimatedSprite::create(EntityAnimationSet * spriteBatch, Vec2 position)
{
    AnimatedSprite * sprite = new AnimatedSprite();
    if(sprite->init(spriteBatch, position)) {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}


void AnimatedSprite::setPosition(Vec2 position)
{
    Sprite::setPosition(position + _positionOffset);
}

void AnimatedSprite::setScale(float scaleX, float scaleY)
{
    Vec2 position = _position - _positionOffset;
    _positionOffset *= scaleX;
    Sprite::setScale(scaleX, scaleY);
    setPosition(position);
}

void AnimatedSprite::setSpriteSize(Size size)
{
    _spriteSize = size * WORLD_TO_GRAPHICS_SIZE;
    setAnimation(currentAnimationState, currentAnimationRepeat);
}

void AnimatedSprite::setAnimation(ANIMATION_TYPE animationType, bool repeat, DIRECTION direction)
{
    EntityAnimation * entAnimation = _spriteBatch->getAnimation(animationType, direction);
    if(entAnimation != nullptr)
    {
        Animation * animation = entAnimation->_animation;
        if(animation->getFrames().size() > 0)
        {
            Animate * animate;
            if(entAnimation->_createParticle)
            {
                animate = AnimationWithParticle::create(entAnimation, _spriteSize);
            }
            else
            {
                 animate = AutoSizeAnimation::create(entAnimation, _spriteSize);
            }
            stopAllActions();

            if(repeat)
            {
               runAction(RepeatForever::create(animate));
            }
            else
            {
               runAction(animate);
            }
        }
        else
        {
            CCLOG("oh no! ");
        }
    }
    currentAnimationDirection = direction;
    currentAnimationState = animationType;
    currentAnimationRepeat = repeat;
}

