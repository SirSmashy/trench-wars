//
//  EntityManager.cpp
//  TrenchWars
//
//  Created by Paul Reed on 5/2/12.
//  Copyright (c) 2012 . All rights reserved.
//

#include "EntityFactory.h"
#include <algorithm>

USING_NS_CC;

#include "WeaponDefinitionXMLParser.h"
#include "EntityDefinitionXMLParser.h"
#include "SpriteDefinitionXMLParser.h"
#include "ProjectileDefinitionXMLParser.h"
#include "PolygonLandscapeDefinitionXMLParser.h"
#include "FormationDefinitionXMLParser.h"
#include "Infantry.h"
#include "Projectile.h"
#include "CrewWeapon.h"
#include "Unit.h"
#include "UnitGroup.h"
#include "CommandPost.h"
#include "Command.h"
#include "EngagementManager.h"
#include "StaticEntity.h"
#include "Emblem.h"
#include "Bunker.h"
#include "PlayerLayer.h"
#include "ColorToTerrainTypeStore.h"

EntityFactory * globalEntityFactory;

std::unordered_map<std::string, ANIMATION_TYPE> nameToAnimationMap ({
    {"default", DEFAULT},
    {"idle", DEFAULT},
    {"move", MOVE},
    {"proneIdle", PRONE_IDLE},
    {"proneMove", PRONE_MOVE},
    {"dig", DIG},
    {"work", WORK},
    {"fire", FIRE},
    {"die", DIE},
    {"EmblemDefault", EMBLEM_DEFAULT},
    {"EmblemInfantry", EMBLEM_INFANTRY},
    {"EmblemMG", EMBLEM_MG},
    {"EmblemArty", EMBLEM_ARTY},
    {"EmblemOrder", EMBLEM_ORDER},
    {"EmblemDig", EMBLEM_DIG},
    {"EmblemAttack", EMBLEM_ATTACK},
    {"EmblemMove", EMBLEM_MOVE},
    {"EmblemBackgroundDiamond", EMBLEM_BACKGROUND_DIAMOND},
    {"EmblemBackgroundCircle", EMBLEM_BACKGROUND_CIRCLE},
    {"EmblemStatusCasualty25", EMBLEM_STATUS_CASUALTY_25},
    {"EmblemStatusCasualty50", EMBLEM_STATUS_CASUALTY_50},
    {"EmblemStatusCasualty75", EMBLEM_STATUS_CASUALTY_75},
    {"EmblemStatusLowAmmo", EMBLEM_STATUS_LOW_AMMO},
    {"EmblemStatusNoCommand", EMBLEM_STATUS_NO_COMMAND},
    {"EmblemStatusStressed", EMBLEM_STATUS_STRESSED},
    {"LineSolid", LINE_SOLID},
    {"LineDashed", LINE_DASHED},
    {"LineDotted", LINE_DOTTED},
    {"LandscapeDamaged1", LANDSCAPE_DAMAGED_1},
    {"LandscapeDamaged2", LANDSCAPE_DAMAGED_2},
    {"LandscapeDamaged3", LANDSCAPE_DAMAGED_3},
    {"LandscapeDamaged4", LANDSCAPE_DAMAGED_4},
    {"LandscapeDamaged5", LANDSCAPE_DAMAGED_5},
    {"LandscapeDestroyed", LANDSCAPE_DESTROYED}
});

std::unordered_map<std::string, DIRECTION> nameToDirectionMap ({
    {"Right", RIGHT},
    {"UpRight", UP_RIGHT},
    {"Up", UP},
    {"UpLeft", UP_LEFT},
    {"Left", LEFT},
    {"DownLeft", DOWN_LEFT},
    {"Down", DOWN},
    {"DownRight", DOWN_RIGHT},
    {"RightLeft", RIGHT_LEFT},
    {"UpDown", UP_DOWN},
    {"RightDownLeft", RIGHT_DOWN_LEFT},
    {"DownLeftUp", DOWN_LEFT_UP},
    {"LeftUpRight", LEFT_UP_RIGHT},
    {"UpRightDown", UP_RIGHT_DOWN},
    {"All", ALL},
    {"default", NONE},
    {"none", NONE}
});

EntityFactory::EntityFactory()
{
    entityDefinitionParser = new EntityDefinitionXMLParser(this);
    spriteDefinitionParser = new SpriteDefinitionXMLParser(this);
    weaponDefinitionParser = new WeaponDefinitionXMLParser(this);
    projectileDefinitionParser = new ProjectileDefinitionXMLParser(this);
    PolygonLandscapeObjectParser = new PolygonLandscapeObjectXMLParser(this);
    formationDefinitionParser = new FormationDefinitionXMLParser(this);
    
    //projectiles MUST be first, weapons MUST be second
    // TODO gross FIX this
    projectileDefinitionParser->parseFile("Projectiles.xml");
    weaponDefinitionParser->parseFile("Weapons.xml");
    entityDefinitionParser->parseFile("Humans.xml");
    entityDefinitionParser->parseFile("Units.xml");
    entityDefinitionParser->parseFile("StaticEntities.xml");
    spriteDefinitionParser->parseFile("SpriteDefinitions.xml");
    PolygonLandscapeObjectParser->parseFile("PolygonLandscapeObjects.xml");
    formationDefinitionParser->parseFile("FormationDefinitions.xml");
    
    // Setup the color to terrain type mapping
    for(auto ent : physicalEntityTypes)
    {
        if(ent.second->_type == STATIC_ENTITY ||
           ent.second->_type == COMMANDPOST ||
           ent.second->_type == BUNKER)
        {
            StaticEntityDefinition * stat = (StaticEntityDefinition *) ent.second;
            globalColorToTerrainTypeStore->addTerrainType(stat->_entityName, stat->_mapColor, stat->_hashedName, true);
        }
    }
    
    for(auto ent : landscapeObjectTypes)
    {
        PolygonLandscapeObjectDefiniton * land = ent.second;
        globalColorToTerrainTypeStore->addTerrainType(land->_entityName, land->_mapColor, land->_hashedName, false);
    }
    
    globalEntityFactory = this;
}

EntityAnimationSet * EntityFactory::createEntityAnimationSet(const std::string & name, const std::string & plistFile)
{
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(plistFile);
    EntityAnimationSet * newSpriteBatch = new EntityAnimationSet();
    size_t hash = std::hash<std::string>{}(name);
    entityBatches.insert(hash, newSpriteBatch);
    return newSpriteBatch;
}

EntityAnimationSet * EntityFactory::getEntityAnimationSet(const std::string & name)
{
    size_t hash = std::hash<std::string>{}(name);
    return entityBatches.at(hash);
}

void EntityFactory::createSpriteBatches()
{
    for(auto pair : entityBatches)
    {
        EntityAnimation * entAnim = pair.second->getFirstValidAnimation();
        if(entAnim == nullptr)
        {
            LOG_ERROR("Unable to create Sprite Batch for Animation Set %s \n", pair.first);
            continue;
        }

//        Animation * animation = entAnim->_animation;
//        SpriteBatchNode * batchNode = SpriteBatchNode::createWithTexture(animation->getFrames().at(0)->getSpriteFrame()->getTexture());
//        pair.second->_batchNode = batchNode;
//        if(pair.second->_uiLayer)
//        {
//            globalPlayerLayer->addChild(batchNode);
//        }
//        else
//        {
//            world->addChild(batchNode);
//        }
        SpriteBatchGroup * batchGroup = SpriteBatchGroup::create(pair.second);
        pair.second->_batchGroup = batchGroup;

    }
}

void EntityFactory::addPolygonLandscapeDefinition(PolygonLandscapeObjectDefiniton * def)
{
    landscapeObjectTypes.insert(def->_hashedName, def);
}

void EntityFactory::addPhysicalEntityDefinition(PhysicalEntityDefinition * entDef)
{
    physicalEntityNames.push_back(entDef->_entityName);
    physicalEntityTypes.insert(entDef->_hashedName, entDef);
}
void EntityFactory::addUnitDefinition(UnitDefinition * unitDef)
{
    unitNames.push_back(unitDef->_entityName);
    unitTypes.insert(unitDef->_hashedName, unitDef);
}

void EntityFactory::addWeaponDefinition(WeaponDefinition * weaponDef)
{
    weaponTypes.insert(std::hash<std::string>{}(weaponDef->_entityName), weaponDef);
}

void EntityFactory::addFormationDefinition(FormationDefinition * formationDef)
{
    formationNames.push_back(formationDef->_entityName);
    formationTypes.insert(std::hash<std::string>{}(formationDef->_entityName), formationDef);
}

void EntityFactory::addCloudDefinition(CloudDefinition * cloudDef)
{
    cloudNames.push_back(cloudDef->_entityName);
    cloudTypes.insert(std::hash<std::string>{}(cloudDef->_entityName), cloudDef);
}


PhysicalEntityDefinition * EntityFactory::getPhysicalEntityDefinitionWithName(const std::string & name)
{
    return physicalEntityTypes.at(std::hash<std::string>{}(name));
}

UnitDefinition * EntityFactory::getUnitDefinitionWithName(const std::string & name)
{
    return unitTypes.at(std::hash<std::string>{}(name));
}

WeaponDefinition * EntityFactory::getWeaponDefinitionWithName(const std::string & name)
{
    return weaponTypes.at(std::hash<std::string>{}(name));
}

PolygonLandscapeObjectDefiniton * EntityFactory::getPolygonLandscapeObjectDefinitionWithName(const std::string & name)
{
    return landscapeObjectTypes.at(std::hash<std::string>{}(name));
}

PhysicalEntityDefinition * EntityFactory::getPhysicalEntityDefinitionWithHashedName(size_t hashedName)
{
    return physicalEntityTypes.at(hashedName);
}

UnitDefinition * EntityFactory::getUnitDefinitionWithHashedName(size_t hashedName)
{
    return unitTypes.at(hashedName);
}

WeaponDefinition * EntityFactory::getWeaponDefinitionWithHashedName(size_t hashedName)
{
    return weaponTypes.at(hashedName);
}

PolygonLandscapeObjectDefiniton * EntityFactory::getPolygonLandscapeObjectDefinitionWithHashedName(size_t hashedName)
{
    return landscapeObjectTypes.at(hashedName);
}

FormationDefinition * EntityFactory::getFormationDefinition(const std::string & name)
{
    return formationTypes.at(std::hash<std::string>{}(name));
}

CloudDefinition * EntityFactory::getCloudDefinition(const std::string & name)
{
    return cloudTypes.at(std::hash<std::string>{}(name));
}

std::vector<std::string> EntityFactory::getAllUnitNames() const
{
    return unitNames;
}

std::vector<std::string> EntityFactory::getAllPhysicalEntityNames() const
{
    return physicalEntityNames;
}

std::vector<std::string> EntityFactory::getAllFormationNames() const
{
    return formationNames;
}

std::vector<std::string> EntityFactory::getAllCloudNames() const
{
    return cloudNames;
}


ANIMATION_TYPE EntityFactory::nameToAnimationType(const std::string & name)
{
    auto it = nameToAnimationMap.find(name);
    if(it != nameToAnimationMap.end())
    {
        return it->second;
    }
    return DEFAULT;
}

DIRECTION EntityFactory::nameToDirection(const std::string & name)
{
    auto it = nameToDirectionMap.find(name);
    if(it != nameToDirectionMap.end())
    {
        return it->second;
    }
    return NONE;
}
