//
//  Order.cpp
//  Trench Wars
//
//  Created by Paul Reed on 8/20/14.
//  Copyright (c) 2014 Paul Reed. All rights reserved.
//

#include "Order.h"
#include "Entity.h"
#include "Objective.h"
#include "ObjectiveObjectCollection.h"
#include "CollisionCell.h"
#include "Unit.h"
#include "MultithreadedAutoReleasePool.h"
#include "Human.h"
#include "EntityManager.h"
#include "InteractiveObjectUtils.h"
#include "Command.h"
#include "EngagementManager.h"
#include "TeamManager.h"
#include "UIController.h"
#include "GameEventController.h"



Order::Order()
{
    // Command stuff
    _owningPlanId = -1;
}

Order::~Order()
{
}

Vec2 Order::getPosition()
{
    return Vec2();
}

std::string Order::getOrderTypeAsString()
{
    switch(getOrderType())
    {
        case MOVE_ORDER:
        {
            return "Move Order";
        }
        case OCCUPY_ORDER:
        {
            return "Occupy Order";
        }
        case OBJECTIVE_WORK_ORDER:
        case ENTITY_WORK_ORDER: {
            return "Work Order";
        }
        case ATTACK_POSITION_ORDER: {
            return "Attack Position Order";
        }
        case ATTACK_TARGET_ORDER: {
            return "Attack Target Order";
        }
        case STOP_ORDER: {
            return "Stop Order";
        }
        case WAIT_ORDER: {
            return "Wait Order";
        }
    }
}

void Order::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    ComparableRef::loadFromArchive(archive);
    archive( _owningPlanId);
}

void Order::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    ComparableRef::saveToArchive(archive);
    archive(_owningPlanId);
}


Order * Order::createOrderOfType(ORDER_TYPE type)
{
    Order * order = nullptr;
    switch (type) {
        case MOVE_ORDER:
        {
            order = MoveOrder::create();
            break;
        }
        case OCCUPY_ORDER:
        {
            order = OccupyOrder::create();
            break;
        }
        case OBJECTIVE_WORK_ORDER: {
            order = ObjectiveWorkOrder::create();
            break;

        }
        case ENTITY_WORK_ORDER: {
            order = EntityWorkOrder::create();
            break;
        }
        case ATTACK_POSITION_ORDER: {
            order = AttackPositionOrder::create();
            break;
        }
        case STOP_ORDER: {
            order = StopOrder::create();
            break;
        }
        case WAIT_ORDER: {
            order = WaitUntilOrder::create();
            break;
        }
    }
    return order;
}


MoveOrder::MoveOrder() : Order()
{
    
}

MoveOrder::MoveOrder(Vec2 location, bool ignoreCover) : Order()
{
    _position = location;
    _ignoreCover = ignoreCover;
}

MoveOrder::~MoveOrder()
{

}


MoveOrder * MoveOrder::create(Vec2 location, bool ignoreCover)
{
    MoveOrder * newOrder = new MoveOrder(location, ignoreCover);
    globalAutoReleasePool->addObject(newOrder);
    return newOrder;
}

MoveOrder * MoveOrder::create()
{
    MoveOrder * newOrder = new MoveOrder();
    globalAutoReleasePool->addObject(newOrder);
    return newOrder;
}

void MoveOrder::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Order::loadFromArchive(archive);
    archive(_position);
    archive(_ignoreCover);
}

void MoveOrder::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Order::saveToArchive(archive);
    archive(_position);
    archive(_ignoreCover);
}


OccupyOrder::OccupyOrder() : MoveOrder()
{
    
}

OccupyOrder::OccupyOrder(Objective * objective, bool ignoreCover) : MoveOrder(Vec2(), ignoreCover)
{
    _objective = objective;
    _objectiveId = objective->uID();
    if(_objective != nullptr)
    {
        _position = _objective->getRandomPointInObjective();
        LOG_DEBUG("Occupy position %.0f %.0f \n", _position.x, _position.y);
    }
}

OccupyOrder::~OccupyOrder()
{
    
}


OccupyOrder * OccupyOrder::create(Objective * objective, bool ignoreCover)
{
    OccupyOrder * newOrder = new OccupyOrder(objective, ignoreCover);
    globalAutoReleasePool->addObject(newOrder);
    return newOrder;
}

OccupyOrder * OccupyOrder::create()
{
    OccupyOrder * newOrder = new OccupyOrder();
    globalAutoReleasePool->addObject(newOrder);
    return newOrder;
}

void OccupyOrder::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    MoveOrder::loadFromArchive(archive);
    archive(_objectiveId);
    if(_objectiveId != -1)
    {
        _objective = dynamic_cast<Objective *>(globalEntityManager->getEntity(_objectiveId));
        if(_objective == nullptr)
        {
            globalEngagementManager->requestMainThreadFunction([this]() {
                _objective = dynamic_cast<Objective *>(globalEntityManager->getEntity(_objectiveId));
            });
        }
    }
}

void OccupyOrder::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    MoveOrder::saveToArchive(archive);
    archive(_objectiveId);
}

WorkOrder::WorkOrder() : Order()
{
    
}

WorkOrder::~WorkOrder()
{

}

EntityWorkOrder::EntityWorkOrder(PhysicalEntity * workObject)
{
    _workObject = workObject;
}


EntityWorkOrder * EntityWorkOrder::create(PhysicalEntity * workObject)
{
    EntityWorkOrder * newOrder = new EntityWorkOrder(workObject);
    globalAutoReleasePool->addObject(newOrder);
    return newOrder;
}


Vec2 EntityWorkOrder::getAverageWorkPosition()
{
    return _workObject->getPosition();
}

void EntityWorkOrder::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Order::loadFromArchive(archive);
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        _workObject = dynamic_cast<PhysicalEntity *>(globalEntityManager->getEntity(thingId));
    }
}

void EntityWorkOrder::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Order::saveToArchive(archive);
    archive(_workObject->uID());
}

ObjectiveWorkOrder * ObjectiveWorkOrder::create(ObjectiveObjectCollection * workObjects)
{
    ObjectiveWorkOrder * newOrder = new ObjectiveWorkOrder(workObjects);
    globalAutoReleasePool->addObject(newOrder);
    return newOrder;
}

ObjectiveWorkOrder::ObjectiveWorkOrder(ObjectiveObjectCollection * workObjects)
{
    _workObjects = workObjects;
}

Vec2 ObjectiveWorkOrder::getAverageWorkPosition()
{
    return _workObjects->getAveragePosition();
}

void ObjectiveWorkOrder::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Order::loadFromArchive(archive);
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        Objective * obj = dynamic_cast<Objective *>(globalEntityManager->getEntity(thingId));
        ObjectCollectionType type;
        archive(type);
        globalEngagementManager->requestMainThreadFunction([this,obj,type]() {
            if(type == OBJECT_COLLECTION_BUNKERS)
            {
                _workObjects = obj->getBunkers();
            }
            else if(type == OBJECT_COLLECTION_TRENCH_LINE)
            {
                _workObjects = obj->getTrenchLine();
            }
        });
    }
}

void ObjectiveWorkOrder::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Order::saveToArchive(archive);
    archive(_workObjects->getOwningObjective()->uID());
    archive(_workObjects->getObjectCollectionType());
}

AttackPositionOrder::AttackPositionOrder() : Order()
{
}

AttackPositionOrder::AttackPositionOrder(Objective * objective)  : Order()
{
    _objective = objective;
}

AttackPositionOrder::~AttackPositionOrder()
{

}

AttackPositionOrder * AttackPositionOrder::create(Objective * objective)
{
    AttackPositionOrder * newOrder = new AttackPositionOrder(objective);
    globalAutoReleasePool->addObject(newOrder);
    return newOrder;
}

AttackPositionOrder * AttackPositionOrder::create()
{
    AttackPositionOrder * newOrder = new AttackPositionOrder();
    globalAutoReleasePool->addObject(newOrder);
    return newOrder;
}

Vec2 AttackPositionOrder::getRandomTargetPoint()
{
    return _objective->getRandomPointInObjective();
}

void AttackPositionOrder::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Order::loadFromArchive(archive);
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        _objective = dynamic_cast<Objective *>(globalEntityManager->getEntity(thingId));
        if(_objective == nullptr)
        {
            globalEngagementManager->requestMainThreadFunction([this,thingId]() {
                _objective = dynamic_cast<Objective *>(globalEntityManager->getEntity(thingId));
            });
        }
    }

}

void AttackPositionOrder::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Order::saveToArchive(archive);
    archive(_objective->uID());
}


StopOrder::StopOrder() : Order()
{
}

StopOrder::~StopOrder()
{
}



StopOrder * StopOrder::create()
{
    StopOrder * newOrder = new StopOrder();
    globalAutoReleasePool->addObject(newOrder);
    return newOrder;
}

WaitUntilOrder::WaitUntilOrder() : Order()
{
    
}

WaitUntilOrder::WaitUntilOrder(WAIT_TYPE type, Human * waitingEnt, double threshold) : Order()
{
    _waitType = type;
    _waitTheshold = threshold;
    _waitingEnt = waitingEnt;
    _waitEntId = _waitingEnt->uID();
}

WaitUntilOrder * WaitUntilOrder::create(WAIT_TYPE type, Human * waitingEnt, double threshold)
{
    WaitUntilOrder * newOrder = new WaitUntilOrder(type, waitingEnt, threshold);
    globalAutoReleasePool->addObject(newOrder);
    return newOrder;
}

WaitUntilOrder * WaitUntilOrder::create()
{
    WaitUntilOrder * newOrder = new WaitUntilOrder();
    globalAutoReleasePool->addObject(newOrder);
    return newOrder;
}

bool WaitUntilOrder::isWaitComplete()
{
    switch (_waitType) {
        case WAIT_STRESS:
            return _waitingEnt->getStress() < _waitTheshold;
            break;
        default:
            return true;
    }
}

void WaitUntilOrder::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Order::loadFromArchive(archive);
    int waitType;
    archive(waitType);
    _waitType = (WAIT_TYPE) waitType;
    archive(_waitTheshold);
    archive(_waitEntId);
    if(_waitEntId != -1)
    {
        _waitingEnt = dynamic_cast<Human *>(globalEntityManager->getEntity(_waitEntId));

        if(_waitingEnt == nullptr)
        {
            globalEngagementManager->requestMainThreadFunction([this]() {
                _waitingEnt = dynamic_cast<Human *>(globalEntityManager->getEntity(_waitEntId));
                
            });
        }
    }
}

void WaitUntilOrder::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Order::saveToArchive(archive);
    archive((int) _waitType);
    archive(_waitTheshold);
    archive(_waitEntId);
}
