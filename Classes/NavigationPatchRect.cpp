//
//  NavigationPatchRect.cpp
//  TrenchWars
//
//  Created by Paul Reed on 2/8/20.
//

#include "NavigationPatchRect.h"

NavigationPatchRect::NavigationPatchRect() : // initially invalid
NavigableRect()
{
}

NavigationPatchRect::NavigationPatchRect(int xMin, int xMax, int yMin, int yMax)  :
NavigableRect(xMin, xMax, yMin, yMax)
{
}

NavigationPatchRect::NavigationPatchRect(int xMin, int xMax, int yMin, int yMax, const TerrainCharacteristics & terrain) :
NavigableRect(xMin, xMax, yMin, yMax),
_terrain(terrain)
{
}

void NavigationPatchRect::set(int xMin, int xMax, int yMin, int yMax, const TerrainCharacteristics & terrain)
{
    _terrain = terrain;
    NavigableRect::set(xMin, xMax, yMin, yMax);
}

bool NavigationPatchRect::hasSameTerrain(NavigationPatchRect & other) const
{
    if(other._terrain == _terrain) {
        return true;
    }
    return false;
}
