//
//  InputAction.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/31/20.
//

#include "InputAction.h"



bool InputChord::operator==(InputChord & other)
{
    if(keys.size() != other.keys.size() || mouseInput != other.mouseInput || mouseDrag != other.mouseDrag)
    {
        return false;
    }
    
    bool match;
    for(EventKeyboard::KeyCode key : keys)
    {
        match = false;
        for(EventKeyboard::KeyCode otherKey : other.keys)
        {
            if(key == otherKey)
            {
                match = true;
                break;
            }
        }
        if(!match)
        {
            return false;
        }
    }
    return true;
}


InputAction::InputAction()
{
    chordToActivate.mouseInput = MOUSE_NONE;
    chordToActivate.mouseDrag = false;
}


bool InputAction::inputMatches(InputChord chord)
{
    return chordToActivate == chord;
}
