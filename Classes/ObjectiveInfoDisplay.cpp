//
//  CommandPostInfo.cpp
//  TrenchWars
//
//  Created by Paul Reed on 3/12/22.
//

#include "ObjectiveInfoDisplay.h"
#include "PlayerLayer.h"
#include "CommandPost.h"

ObjectiveInfoDisplay::ObjectiveInfoDisplay() : AutoSizedLayout()
{
    
}

ObjectiveInfoDisplay::~ObjectiveInfoDisplay()
{
}


bool ObjectiveInfoDisplay::init(Objective * objective)
{
    AutoSizedLayout::init();
    
    LinearLayoutParameter * params = LinearLayoutParameter::create();
    params->setMargin(Margin(0, 0, 0, 0));
    setLayoutParameter(params);
    setLayoutType(Layout::Type::VERTICAL);
    
    _objective = objective;
    _objectiveName = nullptr;
    _objectiveAmmoReserves = nullptr;
    globalPlayerLayer->addChild(this);
    setPosition(objective->getCenterPoint());
    doLayout();
    return true;
}


ObjectiveInfoDisplay * ObjectiveInfoDisplay::create(Objective * objective)
{
    ObjectiveInfoDisplay * newInfo = new ObjectiveInfoDisplay();
    if(newInfo->init(objective))
    {
        newInfo->autorelease();
        return newInfo;
    }
    
    CC_SAFE_DELETE(newInfo);
    return nullptr;
}

void ObjectiveInfoDisplay::addText(std::string textString)
{
    double fontSize = 20 / globalPlayerLayer->getScale();
    if(fontSize < 12 ) {
        fontSize = 12;
    }
    Text * text = Text::create(textString, "arial.ttf", fontSize);
    addChild(text);
    doLayout();
}


void ObjectiveInfoDisplay::openNameTextField()
{
    if(_objectiveName == nullptr)
    {
        _objectiveName = TextField::create(_name, "arial.ttf", 40);
        _objectiveName->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        addChild(_objectiveName);
        doLayout();
    }
    
    _objectiveName->addEventListener( [=] (Ref * sender, TextField::EventType eventType) {
        if(eventType == TextField::EventType::DETACH_WITH_IME)
        {
            _objective->setName(_objectiveName->getString());
            _objectiveName->setInsertText(false);
            _objectiveName->setCursorEnabled(false);
            _objectiveName->setEnabled(false);
        }
    });
    _objectiveName->setEnabled(true);
    _objectiveName->setInsertText(true);
    _objectiveName->setCursorEnabled(true);
    _objectiveName->attachWithIME();
}


void ObjectiveInfoDisplay::update()
{
    Rect bounds = _objective->getBounds();
    setPosition(Vec2(bounds.getMidX(), bounds.getMidY()));
    
    double scale = 1 / globalPlayerLayer->getScale();
    setScale(scale);
    
    if(_objective->hasCommandPost())
    {
        CommandPost * post = _objective->getCommandPost();
        if(_objectiveAmmoReserves == nullptr)
        {
            _objectiveAmmoReserves = Text::create("", "arial.ttf", 20);
            addChild(_objectiveAmmoReserves);
            doLayout();
        }
        
        _objectiveAmmoReserves->setString(std::to_string(post->getAmmoStockpile()));
        
    }
    else if(_objectiveAmmoReserves != nullptr)
    {
        removeChild(_objectiveAmmoReserves);
        _objectiveAmmoReserves = nullptr;
        doLayout();
    }
}
