//
//  TestingScene.h
//  TrenchWars
//
//  Created by Paul Reed on 9/19/23.
//

#ifndef TestingScene_h
#define TestingScene_h


#include "cocos2d.h"
#include "RedBlackTree.h"
#include "Entity.h"

using namespace cocos2d::ui;
USING_NS_CC;


class TestingScene : public cocos2d::Scene
{
    
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    //button callbacks    
    void backButtonCallback(cocos2d::Ref* pSender);
    void testCallback(cocos2d::Ref* pSender);
    
    void test(Vec2 vec);
    void test2(Vec2 vec);
    
    void menuBackCallback(cocos2d::Ref* pSender);
    
    
    // implement the "static create()" method manually
    CREATE_FUNC(TestingScene);
    
    Menu * mainMenu;
};

#endif /* TestingScene_hpp */
