#ifndef __SPRITE_ANIMATIONS_DEFINITION_XML_PARSER_H__
#define __SPRITE_ANIMATIONS_DEFINITION_XML_PARSER_H__
//
//  SpriteAnimationDefinitionXMLParser.h
//  Trench Wars
//
//  Created by Paul Reed on 10/6/13.
//  Copyright (c) 2013 . All rights reserved.
//

#include "pugixml/pugixml.hpp"
#include "EntityFactory.h"
#include "cocos2d.h"

using namespace pugi;
USING_NS_CC;

class SpriteDefinitionXMLParser :public xml_tree_walker
{
    EntityAnimationSet * currentEntitySprite;
    EntityAnimation * currentAnimation;
    ANIMATION_TYPE currentAnimationType;
    EntityFactory * entityFactory;
    
    
public:
    SpriteDefinitionXMLParser(EntityFactory * factory);
    
    void parseFile(std::string const & filename);
    virtual bool for_each(xml_node& node);
};

#endif //__SPRITE_ANIMATIONS_DEFINITION_XML_PARSER_H__
