//
//  NavigationPatch.hpp
//  TrenchWars
//
//  Created by Paul Reed on 1/13/20.
//

#ifndef NavigationPatch_h
#define NavigationPatch_h

#include "cocos2d.h"
#include "Refs.h"
#include "CollisionCell.h"
#include "PrimativeDrawer.h"
#include "NavigationPatchRect.h"
#include "LineDrawer.h"

#define CROWDEDNESS_COST 2
#define CROWDEDNESS_COST_2 0

struct PathCosts
{
    double costs [5] = {-1,-1,-1,-1,-1};
};

class NavigationPatchBorder;
class NavigationGrid;

class NavigationPatch : public ComparableRef
{
public:
    NavigationPatchRect _patchRect;
private:
    
    NavigationGrid * _owningGrid;
    Map<ENTITY_ID, NavigationPatchBorder *> _neighbors;

    Vector<LineDrawer *> _displayLines;
    std::recursive_mutex _modificationMutex;
    bool _markedForRemoval;
    bool _initialPatch;
    PathCosts _cachedCosts;
    std::unordered_map<ENTITY_ID, std::unordered_map<ENTITY_ID, PathCosts>> _pathCostCache;
    double _averageNeighborMovementCost;
    
//    std::vector<std::atomic<double> *> _congestionInPatch;
    std::mutex _congestionMutex;
    double _congestionInPatch [10];
    double _congestionPerPerson;

    
public:
    NavigationPatch(NavigationPatchRect & patchRect, NavigationGrid * grid);
    ~NavigationPatch();
    
    void findNeighbors();
    bool addNeighbor(NavigationPatch * neighbor, DIRECTION direction);
    
    void removeNeighbor(NavigationPatch * neighbor);
    void claimCollisionCells();
    
    void markForRemoval();
    bool isMarkedForRemoval() {return _markedForRemoval;}
        
    NavigationPatchBorder * neighborForPatch(NavigationPatch * patch);
    const Map<ENTITY_ID,NavigationPatchBorder *> & getNeighbors() {return _neighbors;}
    
    NavigationGrid * getGrid() {return _owningGrid;}
    CollisionGrid * getCollisionGrid();
    
    
    double getAverageNeighborMovementCost() {return _averageNeighborMovementCost;}
    double minimumDistanceToCell(CollisionCell * cell);
    bool containsCell(CollisionCell * cell);
    bool hasSameTerrain(NavigationPatch * other);
    bool isTraversable();
    double getMovementCost(MOVEMENT_PREFERENCE movePreference);
    
    double getCachedPathCost(CollisionCell * start, CollisionCell * end, MOVEMENT_PREFERENCE movePreference);
    void cachePathCost(CollisionCell * start, CollisionCell * end, MOVEMENT_PREFERENCE movePreference, double cost);
    
    void addCongestion(int team, ENTITY_ID otherPatchId = -1);
    void removeCongestion(int team, ENTITY_ID otherPatchId = -1);
    double getCongestionForTeam(int team, ENTITY_ID otherPatchId = -1);
    
    void print(std::string start, bool neighbors = false);
    void draw();
    void stopDrawing();
    void remove();
};

#endif /* NavigationPatch_h */
