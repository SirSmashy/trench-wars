//
//  GameStateManager.h
//  TrenchWars
//
//  Created by Paul Reed on 10/21/23.
//

#ifndef GameStateManager_h
#define GameStateManager_h

#include "cocos2d.h"
#include "NetworkInterface.h"
#include <cereal/archives/binary.hpp>

USING_NS_CC;

enum GAME_STATE
{
    GAME_STATE_NONE,
    GAME_STATE_IN_LOBBY,
    GAME_STATE_IN_ENGAGEMENT
};

class Player;


class GameStateManager : public Ref
{
public:
    virtual void update(float deltaTime) = 0;
    virtual void parseNetworkMessage(GameMessage * message) = 0;
    virtual GAME_STATE getState() = 0;
    
    virtual void playerJoinedGame(Player * player) = 0;
    virtual void playerLeftGame(int playerId) = 0;
    
    virtual void shutdown() = 0;
};


#endif /* GameStateManager_h */
