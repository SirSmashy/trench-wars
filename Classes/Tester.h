//
//  Tester.h
//  TrenchWars
//
//  Created by Paul Reed on 7/25/24.
//

#ifndef Tester_h
#define Tester_h

#include "InteractiveObject.h"
#include "Refs.h"


class Tester : public InteractiveObject
{
public:
    virtual INTERACTIVE_OBJECT_TYPE getInteractiveObjectType() {return TESTER_INTERACTIVE_OBJECT;}

    virtual void update() = 0;
    virtual Vec2 getPosition() = 0;
    virtual void removeFromGame() = 0;
    virtual bool testCollisionWithPoint(Vec2 point) = 0;
};

#endif /* Tester_h */
