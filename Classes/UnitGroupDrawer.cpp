//
//  UnitGroupDrawer.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/12/20.
//

#include "UnitGroupDrawer.h"
#include "Unit.h"
#include "Command.h"
#include "EntityFactory.h"
#include "InteractiveObjectHighlighter.h"
#include "PlayerLayer.h"
#include "EmblemManager.h"
#include "TeamManager.h"
#include "UIController.h"

UnitGroupDrawer::UnitGroupDrawer()  : DrawNode()
{
    
}


UnitGroupDrawer::~UnitGroupDrawer()
{
}


bool UnitGroupDrawer::init()
{
    if(DrawNode::init()) {
        _unitGroup = nullptr;
        _highlighter = InteractiveObjectHighlighter::create();
        globalPlayerLayer->addChild(this, 0); //1000
        return true;
    }
    return false;
}
    
UnitGroupDrawer * UnitGroupDrawer::create()
{
    UnitGroupDrawer * newDrawer = new UnitGroupDrawer();
    if(newDrawer->init())
    {
        newDrawer->autorelease();
        return newDrawer;
    }
    
    CC_SAFE_DELETE(newDrawer);
    return nullptr;
}

void UnitGroupDrawer::dragStart(Vec2 point)
{
    startPosition = point;
}


void UnitGroupDrawer::dragMove(Vec2 point)
{
    clear();
    int minX = std::min(point.x, startPosition.x);
    int maxX = std::max(point.x, startPosition.x);
    int minY = std::min(point.y, startPosition.y);
    int maxY = std::max(point.y, startPosition.y);
    
    Rect bounds (minX, minY, maxX - minX, maxY - minY);
    
    Vector<Unit *> selection;
    std::unordered_set<ENTITY_ID> selectionIds;
    
    globalTeamManager->getLocalPlayersTeam()->unitsInRect(bounds, selection, true);
    
    _unitsInGroup.clear();
    _unitsInGroup.pushBack(selection);
        
    for(Unit * unit : _unitsInGroup)
    {
        Emblem * emblem = globalEmblemManager->getEmblemForCommandableObjectWithType(unit->uID(), POSITION_EMBLEM);
        if(emblem != nullptr)
        {
            selectionIds.insert(emblem->uID());
            _highlighter->addObject(emblem);
        }
    }
    
    std::unordered_set<ENTITY_ID> highlightedEnts = _highlighter->getHighlightedObjects();
    for(auto entId : highlightedEnts)
    {
        if(!selectionIds.contains(entId))
        {
            _highlighter->removeObject(entId);
        }
    }
    
    drawRect(startPosition, point, Color4F(0,1.0,0,1));
}

void UnitGroupDrawer::dragEnd(Vec2 point)
{
    if(_unitsInGroup.size() > 0)
    {
        bool matchesExistingGroup = false;
        Map<ENTITY_ID, UnitGroup *> groups = globalUIController->getUnitGroups();
        for(auto pair : groups)
        {
            matchesExistingGroup = true;
            Vector<Unit *> units = pair.second->getUnits();
            if(units.size() != _unitsInGroup.size()) {
                matchesExistingGroup = false;
                break;
            }
            for(Unit * unit : _unitsInGroup)
            {
                if(!units.contains(unit))
                {
                    matchesExistingGroup = false;
                    break;
                }
            }
            if(!matchesExistingGroup)
            {
                break;
            }
        }
        if(!matchesExistingGroup)
        {
            _unitGroup = UnitGroup::create(_unitsInGroup, globalTeamManager->getLocalPlayersTeam());
        }
    }
    _highlighter->clearObjects();
    
    _unitsInGroup.clear();
    globalPlayerLayer->removeChild(this);
}
