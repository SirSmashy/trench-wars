//
//  UserInputParser.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/31/20.
//

#ifndef UserInputParser_h
#define UserInputParser_h

#include "cocos2d.h"
#include "pugixml/pugixml.hpp"
#include <stdio.h>
#include "InputAction.h"

using namespace pugi;
USING_NS_CC;


class RawInputHandler;

class UserInputParser : public Ref, xml_tree_walker
{
    InputAction * currentAction;
    void setAction(const std::string & string);
    void setInput(const std::string & inputString);
    
    RawInputHandler * _inputHandler;
    
public:
    
    UserInputParser(RawInputHandler * inputHandler);
    
    void parseUserInputFile(const std::string & filename);
    virtual bool for_each(xml_node& node);
};

#endif /* UserInputParser_h */
