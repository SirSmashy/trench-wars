#ifndef __COMMAND_POST_H__
#define __COMMAND_POST_H__
//
//  TWCommandPost.h
//  TrenchWars
//
//  Created by Paul Reed on 2/15/12.
//  Copyright (c) 2012. All rights reserved.
//

#include "PrimativeDrawer.h"
#include "List.h"
#include "Bunker.h"
#include "Courier.h"
#include "Package.h"

class Unit;
class Command;

#define REQUEST_AMMO_CHECK_FREQUENCY 30

class CommandPost : public Bunker
{
private:
    float _controlRadius;
    bool _primary;
    float workUntilCreated;
    
    double _currentCaptureTime;
    double _currentCheckTime;
    bool  _beingCaptured;
    bool  _shocked;
    
    std::atomic_int _ammoStockpile;
    std::atomic_int   _ammoBeingDelivered;
    std::atomic_int   _ammoDebt;
    
    Command * _owningCommand;
    CommandPost * _postToSendAmmoTo;

    int   _ammoStockpileMax;
    float _recentAmmoUsage;

    int _maxCouriers;
    double _lastCourierSpawn;
    
    int _linkStepsToPrimaryCommandPost;
    List<CommandPost *> _linkedCommandPosts;
    
    List<Package *> _packagesToSend;
    Vector<Courier *> _couriers;
    
    std::mutex _packagesMutex;
    CountTo requestAmmoCounter;
    
CC_CONSTRUCTOR_ACCESS :
    
    CommandPost();
    virtual bool init(Vec2 position, Command * command);
    
protected:
    void requestAmmoFromNeighbors();
    void sendPackagesInQueue();
    void sendAmmoToPost();

public:
    
    static CommandPost * create(Vec2 position, Command * command);
    static CommandPost * create();

    virtual void postLoad();
    
    virtual void query(float deltaTime);
    virtual void act(float deltaTime);
    virtual ENTITY_TYPE getEntityType() const {return COMMANDPOST;}
    
    bool getIsPrimary() {return _primary;}
    void setIsPrimary(bool primary);
    
    void updateLinking();
    void setLinkStepsToPrimaryCommandPost(int linkLevel);
    int  getLinkStepsToPrimaryCommandPost() {return _linkStepsToPrimaryCommandPost;}
    bool isLinkedToPrimaryCommandPost() {return _linkStepsToPrimaryCommandPost >= 0;}
    
    void depositAmmo(int ammo);
    void promiseAmmoDeposit(int amount);
    int withdrawAmmo(int requestedAmount, bool forTransfer = false);
    
    int getAmmoStockpile() {return _ammoStockpile.load();}
    float getAmmoNeeded();
    float getAmmoSupplyWeighting();
    float getRecentAmmoUsage() {return _recentAmmoUsage;}
    
    void promiseAmmoWithdrawal(int amount);
    const Vector<Courier *> & getCouriers() {return _couriers;}
    
    void requestAmmoForPost(CommandPost * post);

    void sendOrderToUnit(Order * order, Unit * unit);
    void courierDied(Courier * courier);
    
    void populateDebugPanel(GameMenu * menu, Vec2 location);

    List<CommandPost *> & getLinkedCommandPosts() {return _linkedCommandPosts;}

    void setControlRadius(int radius);
    float getControlRadius() {return _controlRadius;}
    float getCreationRadius();
    
    virtual void removeFromGame();
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

#endif // __COMMAND_POST_H__
