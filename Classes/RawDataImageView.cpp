//
//  TextureImageView.cpp
//  TrenchWars
//
//  Created by Paul Reed on 11/14/23.
//

#include "RawDataImageView.h"


RawDataImageView::~RawDataImageView()
{
    delete _imageTexture;
    delete _image;
    delete[] _imageData;
}


RawDataImageView::RawDataImageView() : ImageView()
{
    
}

void RawDataImageView::initWithData(const unsigned char * data, size_t size)
{
    _imageData = new unsigned char[size];
    memcpy(_imageData,data,size);
    
    _image = new Image();
    _image->initWithImageData(_imageData, size);
    
    
    _imageTexture = new Texture2D();
    _imageTexture->initWithImage(_image);
    
    _textureFile = "";
    _imageTexType = TextureResType::LOCAL;
    
    initRenderer();
    _imageRenderer->initWithTexture(_imageTexture);
    
    //FIXME: https://github.com/cocos2d/cocos2d-x/issues/12249
    if (!_ignoreSize && _customSize.equals(Size::ZERO)) {
        _customSize = _imageRenderer->getContentSize();
    }
    setupTexture();
}

RawDataImageView * RawDataImageView::createWithData(const unsigned char * data, size_t size)
{
    RawDataImageView *widget = new (std::nothrow) RawDataImageView;
    widget->initWithData(data,size);
    widget->autorelease();
    return widget;
}
