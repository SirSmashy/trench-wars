//
//  PlanningMenu.hpp
//  TrenchWars
//
//  Created by Paul Reed on 8/2/21.
//

#ifndef PlanningMenu_h
#define PlanningMenu_h

#include "ui/CocosGUI.h"
#include "cocos2d.h"
#include "AutoSizedListView.h"
#include "List.h"
#include "Plan.h"

USING_NS_CC;
using namespace cocos2d::ui;


class PlanningMenu : public AutoSizedListView
{
private:
    Map<ENTITY_ID, AutoSizedListView *> _planMenus;
    List<AutoSizedListView *> _beingCreatedPlanMenus;

    ENTITY_ID _activePlanId;
    PlanningMenu();
    Button * createButton(std::string name, const AbstractCheckButton::ccWidgetClickCallback & callback, bool icon = false);
    
    void createNewPlanMenu();

public:
    static PlanningMenu * create();

    // Actions
    void addPlan(Plan * plan);
    void removePlan(Plan * plan);
    void setActivePlan(Plan * plan);
    
    // Event handlers
    void planRenamed(Plan * plan, const std::string & newName);
    void planExecuting(Plan * plan);
    void planStopped(Plan * plan);


    void clear();
    void remove();
};


#endif /* PlanningMenu_h */
