//
//  OrderDrawer.cpp
//  TrenchWars
//
//  Created by Paul Reed on 4/8/21.
//

#include "OrderDrawer.h"
#include "PlayerLayer.h"
#include "EmblemManager.h"
#include "EntityManager.h"
#include "UIController.h"
#include "CollisionGrid.h"

OrderDrawer::OrderDrawer()
{
    
}

bool OrderDrawer::init()
{
    _plan = nullptr;
    return true;
}

OrderDrawer * OrderDrawer::create()
{
    OrderDrawer * drawer = new OrderDrawer();
    if(drawer->init())
    {
        drawer->autorelease();
        return drawer;
    }
    
    CC_SAFE_DELETE(drawer);
    return nullptr;
}

void OrderDrawer::drawOrders(const std::unordered_map<ENTITY_ID, ENTITY_ID> & orders, bool drawConnectionLines)
{
    std::unordered_set<ENTITY_ID> validUnits;
    std::vector<ENTITY_ID> invalidUnits;
    // Walk through all the orders in this list and create emblems and lines to them
    for(auto orderPair : orders)
    {
        Unit * unit = (Unit *) globalEntityManager->getEntity(orderPair.first);
        auto order = unit->getOwningCommand()->getOrderWithId(orderPair.second);
        if(unit == nullptr || order == nullptr)
        {
            // Missing order or unit, get rid of any lines or emblems for the order/unit pair
            invalidUnits.push_back(orderPair.first);
            continue;
        }
        validUnits.insert(orderPair.first);

        // Create an emblem if necessary
        Emblem * emblem = globalEmblemManager->getEmblemForCommandableObjectWithType(unit->uID(), ORDER_EMBLEM);
        if(emblem == nullptr)
        {
            emblem = globalEmblemManager->createEmblemForUnit(unit, ORDER_EMBLEM);
        }
        // Position it
        emblem->setAnimationFromOrderType(order->getOrderType());
        emblem->setCanDrag(false);
        emblem->removeStatusIcon(EMBLEM_STATUS_NO_COMMAND);
        emblem->setFlashing(false);
        globalEmblemManager->moveEmblemToOrderPosition(order, emblem);
        
        if(drawConnectionLines)
        {
            // draw a line from the unit to the order
            LineDrawer * line = _unitToLineMap.at(unit->uID());
            Vec2 endPoint = order->getPosition();
            if(line == nullptr)
            {
                line = LineDrawer::create(unit->getAveragePosition() * WORLD_TO_GRAPHICS_SIZE, endPoint, 2);
                line->makeDashed();
                line->setColor(Color3B::BLUE);
                line->setFlowing(true);
                _unitToLineMap.insert(unit->uID(), line);
            }
            else
            {
                line->setLinePosition(unit->getAveragePosition() * WORLD_TO_GRAPHICS_SIZE, endPoint);
            }
        }
    }
    
    // Find lines that don't correspond to a valid order/unit pair
    for(auto linePair : _unitToLineMap)
    {
        if(!validUnits.contains(linePair.first))
        {
            invalidUnits.push_back(linePair.first);
        }
    }
    
    // Remove lines and ebmlems for invalid order/unit pairs
    for(auto unitId : invalidUnits)
    {
        if(_unitToLineMap.find(unitId) != _unitToLineMap.end())
        {
            _unitToLineMap.at(unitId)->remove();
            _unitToLineMap.erase(unitId);
        }
        Emblem * emblem = globalEmblemManager->getEmblemForCommandableObjectWithType(unitId, ORDER_EMBLEM);
        if(emblem != nullptr)
        {
            globalEmblemManager->removeEmblemForCommandableObject(unitId, emblem);
        }
    }
    
    drawUnitGroupOrders(drawConnectionLines);
}

void OrderDrawer::drawUnitGroupOrders(bool drawConnectionLines)
{
    auto unitGroups = globalUIController->getUnitGroups();
    for(auto groupPair : unitGroups)
    {
        // ////////
        ENTITY_ID planId = -1;
        Vec2 position;
        if(globalUIController->getCurrentPlan() != nullptr)
        {
            planId = globalUIController->getCurrentPlan()->uID();
        }
        Emblem * emblem = globalEmblemManager->getEmblemForCommandableObjectWithType(groupPair.second->uID(), ORDER_EMBLEM);
        
        if(groupPair.second->isParticipatingInPlan(planId))
        {
            if(emblem == nullptr)
            {
                emblem = globalEmblemManager->createEmblemForUnitGroup(groupPair.second, ORDER_EMBLEM);
            }
            
            IssuedPlanOrder orderForPlan = groupPair.second->getPlanOrder(planId);
            
            emblem->setAnimationFromOrderType(orderForPlan.orderType);
            emblem->setPosition(orderForPlan.position);
            emblem->setCanDrag(false);
            emblem->removeStatusIcon(EMBLEM_STATUS_NO_COMMAND);
            emblem->setFlashing(false);
            
            if(drawConnectionLines)
            {
                LineDrawer * line = _unitGroupToLineMap.at(groupPair.second->uID());
                if(line == nullptr)
                {
                    line = LineDrawer::create(groupPair.second->getPosition() * WORLD_TO_GRAPHICS_SIZE, orderForPlan.position * WORLD_TO_GRAPHICS_SIZE, 2);
                    line->makeDashed();
                    line->setColor(Color3B::BLUE);
                    line->setFlowing(true);
                    _unitGroupToLineMap.insert(groupPair.second->uID(), line);
                }
                else
                {
                    line->setLinePosition(groupPair.second->getPosition() * WORLD_TO_GRAPHICS_SIZE, orderForPlan.position);
                }
            }
        }
        else
        {
            if(_unitGroupToLineMap.find(groupPair.second->uID()) != _unitGroupToLineMap.end())
            {
                _unitGroupToLineMap.at(groupPair.second->uID())->remove();
                _unitGroupToLineMap.erase(groupPair.second->uID());
            }
            if(emblem != nullptr)
            {
                globalEmblemManager->removeEmblemForCommandableObject(groupPair.second->uID(), emblem);
            }
        }
    }
}

void OrderDrawer::update()
{
    for(auto pair : _unitToLineMap)
    {
        Unit * unit = (Unit *) globalEntityManager->getEntity(pair.first);
        if(unit == nullptr)
        {
            continue;
        }
        Vec2 startPoint = unit->getAveragePosition();
        pair.second->setStartPosition(startPoint);
    }
    
    for(auto pair : _unitGroupToLineMap)
    {
        UnitGroup * group = globalUIController->getUnitGroup(pair.first);
        if(group == nullptr)
        {
            continue;
        }
        Vec2 startPoint = group->getPosition();
        pair.second->setStartPosition(startPoint);
    }
}

void OrderDrawer::clearLines()
{
    for(auto pair : _unitToLineMap)
    {
        pair.second->remove();
    }
    _unitToLineMap.clear();
    
    for(auto pair : _unitGroupToLineMap)
    {
        pair.second->remove();
    }
    _unitGroupToLineMap.clear();
}

void OrderDrawer::clear()
{
    globalEmblemManager->removeAllEmblemsWithType(ORDER_EMBLEM);
    clearLines();
}

void OrderDrawer::remove()
{
    clear();
//    Director * director = Director::getInstance();
//    Scheduler * scheduler = director->getScheduler();
//    scheduler->unschedule("OrderDrawer", this);
}
