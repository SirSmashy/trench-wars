//
//  CocosObjectManager.h
//  TrenchWars
//
//  Created by Paul Reed on 6/19/23.
//

#ifndef CocosObjectManager_h
#define CocosObjectManager_h

#include "cocos2d.h"
#include "Refs.h"
#include <mutex>
#include "AnimatedSpriteProxy.h"

#include <unordered_set>


class CocosObjectManager
{
    Map<ENTITY_ID, CocosProxy *> _cocosObjectProxies;
    std::unordered_set<CocosProxy *> _proxiesToUpdate;
    Vector<CocosProxy *> _proxiesToCreate;
    Vector<CocosProxy *> _proxiesToRemove;
    std::mutex _proxyMutex;
    
public:
    
    CocosObjectManager();
    
    void requestCocosObjectCreation(CocosProxy * proxy);
    void requestCocosObjectUpdate(CocosProxy * proxy);
    void requestCocosObjectRemoval(CocosProxy * proxy);
    
    void updateProxies();

};

extern CocosObjectManager * globalCocosObjectManager;

#endif /* CocosObjectManager_h */
