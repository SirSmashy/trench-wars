//
//  TestBed.hpp
//  TrenchWars
//
//  Created by Paul Reed on 7/31/22.
//

#ifndef TestBed_h
#define TestBed_h

#include "Refs.h"
#include "cocos2d.h"
#include "DebugEntity.h"
#include <unordered_map>
#include <list>
#include "CellHighlighter.h"
#include "Cloud.h"

USING_NS_CC;

class EntityDebugDrawer;

enum MOVING_ENTITY_BEHAVIORS
{
    NULL_BEHAVIOR = 0,
    AVOID_COLLISION_BEHAVIOR = 1,
    GET_NEXT_MOVE_BEHAVIOR = 2,
    UPDATE_STRESS_BEHAVIOR = 4,
    UPDATE_GOALS_BEHAVIOR = 8,
    ATTACK_TARGET_GOAL_BEHAVIOR = 16,
    MOVE_GOAL_BEHAVIOR = 32,
    WORK_GOAL_BEHAVIOR = 64,
    LOOK_FOR_TARGET_BEHAVIOR = 128,
    LOOK_AROUND_BEHAVIOR = 256,
    UPDATE_WEAPON_BEHAVIOR = 512,
};

enum TEST_COLLISION_TYPE
{
    OLD_COLLISION,
    NEW_COLLISION,
    NO_COLLISION
};

enum CLICK_TEST_ACTION
{
    CLICK_ACTION_NONE = 0,
    CLICK_ACTION_SPLIT = 1,
    CLICK_ACTION_DAMAGE = 2,
    CLICK_ACTION_BOOM_800 = 3,
    CLICK_ACTION_BOOM_300 = 4,
    CLICK_ACTION_BOOM_RANDOM = 5,
    CLICK_ACTION_TREE = 6,
    CLICK_ACTION_BUSH = 7,
    CLICK_ACTION_TRENCH = 8,
    CLICK_ACTION_BUNKER = 9,
    CLICK_ACTION_FOREST = 10,
    CLICK_ACTION_TREE_LINE = 11,
    CLICK_ACTION_TRENCH_LINE = 12,
    CLICK_ACTION_CLOUD = 13,
    CLICK_ACTION_END = 14
};

class TestBed : public Ref
{
    CLICK_TEST_ACTION _clickAction;
    EntityDebugDrawer * debugDrawer;

    void init();
    void resetPatchNumbers();
    
    std::unordered_map<ENTITY_ID, std::list<DebugEntity *>> _collisionCellIdToDebugEntityMap;
    std::list<DebugEntity *> _hoveredDebugEnts;
    
    CellHighlighter * _debugCellHighlighter;
    
    Cloud * _debugCloud;
    bool _drawCells;

public:
    
    std::atomic_int addPatch;
    std::atomic_int removePatch;
    
    std::atomic_int addBorder;
    std::atomic_int removeBorder;
    
    std::atomic_int addNode;
    std::atomic_int removeNode;
    
    std::atomic_int addEdge;
    std::atomic_int removeEdge;
    
    bool pathTesting;
    bool updateGrids;
    
    int looksPerFrame;
    
    int visionType;
    
    unsigned short behaviorsToSkip;
    
    double testTextureTime;
    
    TEST_COLLISION_TYPE testCollisionType = OLD_COLLISION;
    
    void printPatchNumbers();
    
    static TestBed * create();
    
    DebugEntity * createDebugEntityAtPosition(Vec2 position,  const std::string & labelText, Vec2 labelOffset, bool hideOnNotVisible);
    void updateDebugEntityAtPosition(DebugEntity * debug, Vec2 oldPosition);
    const std::list<DebugEntity *> & getDebugEntitiesAtPosition(Vec2 position);
    DebugEntity * getDebugEntityNearPosition(Vec2 position);

    DebugEntity * removeDebugEntity(DebugEntity * debug);
    
    void checkDebugEntityHover(Vec2 position);
    
    void update(float deltaTime);
    
    void toggleDebugDrawing();
    void createTestCombatWave(Vec2 location);
    void createTestRandomUnits(Vec2 location);
    
    void createAttackWave(Vec2 location);
    void createDefenseWave(Vec2 location);
    
    void selectTestInfoUnitAtLocation(Vec2 location);
    
    void createTestCloud(Vec2 location);
    Cloud * getTestCloudNearPosition(Vec2 position);
    
    void toggleDrawCells();
    void setCellHighlight(CELL_HIGHLIGHT_DATA_TYPE type);
    
    void renderSectorToImage(Rect rect);
    void createHistorical();
    
    void setClickAction(CLICK_TEST_ACTION click) {_clickAction = click;}
    CLICK_TEST_ACTION getClickAction() {return _clickAction;}
    void doClickAction(InteractiveObject * objClicked, Vec2 location);
    std::string clickActionToName(CLICK_TEST_ACTION action);
    
    void setMovingEntBehavior(MOVING_ENTITY_BEHAVIORS behavior, bool enabled);
protected:


};

extern TestBed * globalTestBed;


#endif /* TestBed_h*/
