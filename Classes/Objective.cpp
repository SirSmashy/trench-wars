//
//  Objective.cpp
//  TrenchWars
//
//  Created by Paul Reed on 6/26/21.
//

#include "Objective.h"
#include "EntityManager.h"
#include "VectorMath.h"
#include "GameVariableStore.h"
#include "Command.h"
#include "Unit.h"
#include "Order.h"
#include "PlayerLayer.h"
#include "PlayerVariableStore.h"
#include "MultithreadedAutoReleasePool.h"
#include "World.h"
#include "CollisionGrid.h"
#include "EngagementManager.h"
#include "TeamManager.h"
#include "MapSector.h"
#include "GameEventController.h"
#include <cereal/types/string.hpp>
#include "ProposedEntity.h"

/**
 *
 */
Objective::Objective() {}

/**
 *
 */
Objective::~Objective()
{
}

/**
 *
 */
bool Objective::init(Team * team, std::vector<Vec2> & points, bool enclosed)
{
    Entity::init();
    _team = team;
    _name = "Objective";
    PlanningObject::init(_team);


    _points.assign(points.begin(), points.end());
    _isEnclosedArea = enclosed;
    _trenchLine = nullptr;
    _bunkers = nullptr;
    
    _visible = true;
    _commandPost = nullptr;
    _proposedCommandPost = nullptr;
    calculateCellsInObjective();
    return true;
}

bool Objective::init(Team * team, CommandPost * post)
{
    std::vector<Vec2> points;
    Vec2 position = post->getPosition();

    float change = (M_PI * 2) / 20;
    for(int i = 0; i < 20; i++)
    {
        Vec2 offset(20,0);
        offset.rotate(Vec2(), change * i);
        points.push_back( position + offset);
    }

    init(team, points, true);
    _commandPost = post;
    return true;
}

/**
 *
 */
Objective * Objective::create(Team * team, std::vector<Vec2> & points, bool enclosed)
{
    Objective * objective = new Objective();
    if(objective->init(team, points, enclosed))
    {
        globalAutoReleasePool->addObject(objective);
        return objective;
    }
    CC_SAFE_DELETE(objective);
    
    return nullptr;
}

Objective * Objective::create(Team * team, CommandPost * post)
{
    Objective * objective = new Objective();
    if(objective->init(team, post))
    {
        globalAutoReleasePool->addObject(objective);
        return objective;
    }
    CC_SAFE_DELETE(objective);
    
    return nullptr;
}

Objective * Objective::create()
{
    Objective * objective = new Objective();
    globalAutoReleasePool->addObject(objective);
    return objective;
}

void Objective::postLoad()
{
    drawObjective();
}
/**
 *
 */
void Objective::drawObjective()
{
    double r = globalPlayerVariableStore->getVariable(ObjectiveColorR);
    double g = globalPlayerVariableStore->getVariable(ObjectiveColorG);
    double b = globalPlayerVariableStore->getVariable(ObjectiveColorB);

    Color3B color(r,g ,b);
    for(int i = 1; i < _points.size(); i++)
    {
        LineDrawer * line = LineDrawer::create(_points[i-1] * WORLD_TO_GRAPHICS_SIZE, _points[i] * WORLD_TO_GRAPHICS_SIZE, 2);
        line->setColor(color);
        line->setScaleWidthWithParentScale(true);
        _lines.pushBack(line);
    }
    if(_isEnclosedArea)
    {
        LineDrawer * line = LineDrawer::create(_points[_points.size() - 1] * WORLD_TO_GRAPHICS_SIZE, _points[0] * WORLD_TO_GRAPHICS_SIZE, 2);
        line->setColor(color);
        line->setScaleWidthWithParentScale(true);
        _lines.pushBack(line);
    }
}

/**
 *
 */
double Objective::getArea()
{
    return 0;
}

/**
 *
 */
double Objective::getPerimeterLength()
{
    return 0;
}

/**
 *
 */
bool Objective::isEnclosedArea()
{
    return _isEnclosedArea;
}

void Objective::setVisible(bool visible)
{
    _visible = visible;
    for(auto line : _lines)
    {
        line->setVisible(visible);
    }
}

/**
 *
 */
void Objective::calculateCellsInObjective()
{
    double maxX = -9999999;
    double maxY = -9999999;
    _bounds.origin.x = 999999;
    _bounds.origin.y = 999999;
    for(Vec2 point : _points)
    {
        if(point.x < _bounds.origin.x) {
            _bounds.origin.x = point.x;
        }
        if(point.y < _bounds.origin.y) {
            _bounds.origin.y = point.y;
        }
        if(point.x > maxX) {
            maxX = point.x;
        }
        if(point.y > maxY) {
            maxY = point.y;
        }
    }
    _bounds.size.width = maxX - _bounds.origin.x;
    _bounds.size.height = maxY - _bounds.origin.y;
    
    if(_bounds.size.width < 1)
    {
        _bounds.size.width = 1;
    }
    
    if(_bounds.size.height < 1)
    {
        _bounds.size.height = 1;
    }
    
    //// Find the cells along the border of the plan area
    std::vector<CollisionCell *> cellsInLine;
    for(int i = 1; i < _points.size(); i++)
    {
        world->getCollisionGrid()->cellsInLine(_points[i-1], _points[i],  &cellsInLine);
        for(CollisionCell * cell : cellsInLine)
        {
            if(_cellsMap.find(cell->uID()) == _cellsMap.end())
            {
                _cellsAlongObjectiveBorder.push_back(cell);
                _cellsMap.emplace(cell->uID(), cell);
            }
        }
    }
    
    if(_isEnclosedArea)
    {
        std::vector<CollisionCell *> cellInPolygon;
        world->getCollisionGrid()->cellsInPolygon(_points, &cellInPolygon);
        
        for(CollisionCell * cell : cellInPolygon)
        {
            _cellsMap.emplace(cell->uID(), cell);
            _cellsInObjective.push_back(cell);
        }
    }
    else
    {
        for(auto iterator : _cellsMap)
        {
            _cellsInObjective.push_back(iterator.second);
        }
    }
    
    for(auto cell : _cellsInObjective)
    {
        MapSector * sector = cell->getMapSector();
        if(_sectorsMap.find(sector->uID()) == _sectorsMap.end())
        {
            _sectorsMap.emplace(sector->uID(), sector);
            _sectorsInObjective.push_back(sector);
        }
    }
}

/**
 *
 */
bool Objective::pointInObjective(Vec2 point)
{
    CollisionCell * cell = world->getCollisionGrid()->getCellForCoordinates(point);
    if(_cellsMap.find(cell->uID()) != _cellsMap.end()) {
        return true;
    }
    return false;
}

Vec2 Objective::getRandomPointInObjective()
{
    int index = globalRandom->randomInt(0, (int) _cellsInObjective.size() -1);
    return _cellsInObjective[index]->getCellPosition();
}

Vec2 Objective::getCenterPoint()
{
    return Vec2(_bounds.getMidX(), _bounds.getMidY());
}

void Objective::createTrenchLine()
{
    _trenchLine = ObjectiveObjectCollection::create(this, OBJECT_COLLECTION_TRENCH_LINE);
    _trenchLine->retain();
    const std::vector<CollisionCell *> cells = cellsAlongBorder();
    for(CollisionCell * cell : cells)
    {
        if(!cell->hasTrench())
        {
            auto trench = ProposedTrench::create(cell->getCellPosition(), _trenchLine);
            _trenchLine->addEntity(trench);
        }
    }
}

void Objective::removeTrenchLine()
{
    _trenchLine->remove();
    _trenchLine->release();
    _trenchLine = nullptr;
}


DIRECTION Objective::getBunkerEntrance(PhysicalEntity * trench)
{
    CollisionCell * trenchCell = world->getCollisionGrid()->getCellForCoordinates(trench->getPosition());
    CollisionCell * cell = world->getCollisionGrid()->getCell(trenchCell->getXIndex(), trenchCell->getYIndex() +1);

    auto terrain =  trench->getTerrainCharacteristics();
    if(terrain != cell->getTerrainCharacteristics())
    {
        return UP;
    }

    cell = world->getCollisionGrid()->getCell(trenchCell->getXIndex(), trenchCell->getYIndex() -1);
    if(terrain != cell->getTerrainCharacteristics())
    {
        return DOWN;
    }

    cell = world->getCollisionGrid()->getCell(trenchCell->getXIndex() -1, trenchCell->getYIndex());
    if(terrain != cell->getTerrainCharacteristics())
    {
        return LEFT;
    }


    cell = world->getCollisionGrid()->getCell(trenchCell->getXIndex() +1, trenchCell->getYIndex());
    if(terrain != cell->getTerrainCharacteristics())
    {
        return RIGHT;
    }

    return RIGHT;
}

void Objective::createBunkers()
{
    // FIXME: todo fix this is all just dumb... why do it this way?
    _bunkers = ObjectiveObjectCollection::create(this, OBJECT_COLLECTION_BUNKERS);
    _bunkers->retain();
    if(_trenchLine != nullptr)
    {
        int count = 0;
        for(auto ent : *_trenchLine)
        {
            count++;
            if(count > 20 && !world->isBunkerEntranceNearPosition(ent->getPosition(), 120))
            {
                DIRECTION facing = getBunkerEntrance(ent);
                Vec2 position = ent->getPosition() + VectorMath::unitVectorFromFacing(facing);
                Size bunkerSize(globalVariableStore->getVariable(BunkerSizeX),globalVariableStore->getVariable(BunkerSizeY));
                
                ProposedBunker * proposed = ProposedBunker::create(position, "trench", bunkerSize, facing, _bunkers);
                _bunkers->addEntity(proposed);
                count = 0;
            }
        }
    }
}


void Objective::removeBunkers()
{
    _bunkers->remove();
    _bunkers->release();
    _bunkers = nullptr;
}

void Objective::setHighlighted(bool highlighted)
{
    int width = highlighted ? 5 : 2;
    for(auto line : _lines)
    {
        line->setWidth(width);
    }
}

bool Objective::hasCommandPost()
{
    return _commandPost != nullptr || _proposedCommandPost != nullptr;
}

void Objective::createCommandPost(Vec2 location)
{
//    _proposedCommandPost = ProposedCommandPost::create(location, "CommandPost", _command, this); // FIX
}

void Objective::setCommandPost(CommandPost * post)
{
    _commandPost = post;
    _proposedCommandPost = nullptr;
}

void Objective::setName(const std::string & name)
{
    _name = name;
}

Vec2 Objective::findAreaInObjectiveWithBestCoverValue(int searchSize)
{
    // TODO fix this needs to be improved
    MapSector * bestSector = nullptr;
    CollisionCell * best = nullptr;
    double bestValue = -1;
    double value;
    Rect cellRect;
    Rect previousRect;
    
    cellRect.size.width = searchSize;
    cellRect.size.height = searchSize;
    
    test.clear();
    
    CollisionGrid * grid = world->getCollisionGrid();
    
    for(MapSector * sector : _sectorsInObjective)
    {
        double security = sector->getAvailableTotalSecurityForTeam(_team->getTeamId());
        if(security > bestValue)
        {
            bestValue = security;
            bestSector = sector;
        }
    }
    
    return bestSector->getPathingCell()->getCellPosition();
    
//    for(CollisionCell * cell : _cellsInObjective)
//    {
//        cellRect.origin.x = cell->getXIndex() - searchSize;
//        cellRect.origin.y = cell->getYIndex() - searchSize;
//        
//        value = grid->findCoverValueInRect(cellRect, cell, _team);
//
//        if(value > bestValue)
//        {
//            bestValue = value;
//            best = cell;
//        }
//        
//        previousRect = cellRect;
//    }
//    return best->getCellPosition();
}

void Objective::removeFromGame()
{
    for(auto line : _lines)
    {
        line->remove();
    }
    _lines.clear();
    _team->removePlanningObject(this);
    PlanningObject::removeFromGame();
}


void Objective::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    PlanningObject::loadFromArchive(archive);
    archive(_bounds);

    size_t size;
    archive(size);
    
    for(int i = 0; i < size; i++)
    {
        Vec2 point;
        archive(point);
        _points.push_back(point);
    }
    
    int team;
    archive(team);
    _team = globalTeamManager->getTeam(team);
    
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        _trenchLine = ObjectiveObjectCollection::create(this);
        _trenchLine->loadFromArchive(archive);
    }
    else
    {
        _trenchLine = nullptr;
    }
    
    archive(thingId);
    if(thingId != -1)
    {
        _bunkers = ObjectiveObjectCollection::create(this);
        _bunkers->loadFromArchive(archive);
    }
    else
    {
        _bunkers = nullptr;
    }
    
    archive(thingId);
    if(thingId != -1)
    {
        _proposedCommandPost = dynamic_cast<ProposedCommandPost *>(globalEntityManager->getEntity(thingId));
    }
    else
    {
        _proposedCommandPost = nullptr;
    }
    
    archive(thingId);
    if(thingId != -1)
    {
        _commandPost = dynamic_cast<CommandPost *>(globalEntityManager->getEntity(thingId));
    }
    else
    {
        _commandPost = nullptr;
    }
    archive(_visible);
    archive(_isEnclosedArea);
    archive(_name);
    
    CollisionGrid * grid = world->getCollisionGrid();
    unsigned short x, y;
    archive(size);
    for(size_t i = 0; i < size; i++)
    {
        archive(x);
        archive(y);
        CollisionCell * cell = grid->getCell(x, y);
        _cellsInObjective.push_back(cell);
        _cellsMap.emplace(cell->uID(), cell);
    }
    
    
    archive(size);
    for(size_t i = 0; i < size; i++)
    {
        archive(x);
        archive(y);
        CollisionCell * cell = grid->getCell(x, y);
        _cellsAlongObjectiveBorder.push_back(cell);
    }
}



void Objective::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    PlanningObject::saveToArchive(archive);
    archive(_bounds);
    archive(_points.size());
    for(auto point : _points)
    {
        archive(point);
    }
    
    archive(_team->getTeamId());
    
    if(_trenchLine != nullptr)
    {
        archive(_trenchLine->uID());
        _trenchLine->saveToArchive(archive);
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
    
    
    if(_bunkers != nullptr)
    {
        archive(_bunkers->uID());
        _bunkers->saveToArchive(archive);
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
    
    if(_proposedCommandPost != nullptr)
    {
        archive(_proposedCommandPost->uID());
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
    
    if(_commandPost != nullptr)
    {
        archive(_commandPost->uID());
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
    
    archive(_visible);
    archive(_isEnclosedArea);
    archive(_name);
    
    archive(_cellsInObjective.size());
    for(auto cell : _cellsInObjective)
    {
        archive(cell->getXIndex());
        archive(cell->getYIndex());
    }
    
    archive(_cellsAlongObjectiveBorder.size());
    for(auto cell : _cellsAlongObjectiveBorder)
    {
        archive(cell->getXIndex());
        archive(cell->getYIndex());
    }
}

