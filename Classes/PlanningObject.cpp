//
//  PlanningObject.cpp
//  TrenchWars
//
//  Created by Paul Reed on 1/6/25.
//


#include "PlanningObject.h"
#include "Team.h"
#include <climits>

void PlanningObject::init(Team * owningTeam)
{
    _owningTeam = owningTeam;
    if(_owningTeam != nullptr)
    {
        _owningTeam->addPlanningObject(this);
    }
}

void PlanningObject::removeFromGame()
{
    if(_owningTeam != nullptr)
    {
        _owningTeam->removePlanningObject(this);
    }
}



void PlanningObject::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    ComparableRef::loadFromArchive(archive);
    if(_owningTeam != nullptr)
    {
        archive(_owningTeam->getTeamId());
    }
}

void PlanningObject::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    ComparableRef::saveToArchive(archive);
    if(_owningTeam != nullptr)
    {
        archive(_owningTeam->getTeamId());
    }
    else
    {
        LOG_DEBUG_ERROR("Oooooo no! \n");
        archive((ENTITY_ID) -1); //This shouldn't ever happen... maybe throw an error?
    }
}
