//
//  ParticleSystemProxy.cpp
//  TrenchWars
//
//  Created by Paul Reed on 7/29/24.
//

#include "ParticleSystemProxy.h"
#include "ParticleManager.h"
#include "MultithreadedAutoReleasePool.h"
#include "CocosObjectManager.h"
#include "CollisionGrid.h"

ParticleSystemProxy::ParticleSystemProxy() :
_pendingPosition(Vec2()),
_pendingHeight(0),
_pendingRadius(0.0),
_pendingAlpha(1.0),
_pendingSize(0.0),
_pendingCloudSetup(ParticleCloudProperties())
{
    
}

ParticleSystemProxy::~ParticleSystemProxy()
{
    globalParticleManager->requestParticleRemoval(uID());
}

void ParticleSystemProxy::init(const std::string & name, Vec2 position)
{
    _particleName = name;
    _particleSystem = nullptr;
    setPosition(position);

    globalCocosObjectManager->requestCocosObjectCreation(this);
    globalCocosObjectManager->requestCocosObjectUpdate(this);
    globalAutoReleasePool->addObject(this);
}


ParticleSystemProxy * ParticleSystemProxy::create(const std::string & name, Vec2 position)
{
    ParticleSystemProxy  * newProxy = new ParticleSystemProxy();
    newProxy->init(name, position);
    return newProxy;
}


void ParticleSystemProxy::createCocosObject()
{
    _particleSystem = (DynamicParticleSystem *) globalParticleManager->createParticle(_particleName, _pendingPosition.get() * WORLD_TO_GRAPHICS_SIZE, uID(), 2, false, true);
}

bool ParticleSystemProxy::updateCocosObject()
{
    if(_pendingPosition.isChanged())
    {
        _particleSystem->setPosition(_pendingPosition.get() * WORLD_TO_GRAPHICS_SIZE);
        _pendingPosition.clearChange();
    }    
    if(_pendingHeight.isChanged())
    {
        _particleSystem->setLocalZOrder(_pendingHeight);
        _pendingHeight.clearChange();
    }
    if(_pendingSize.isChanged())
    {
        _particleSystem->setSize(_pendingSize * WORLD_TO_GRAPHICS_SIZE);
        _pendingSize.clearChange();
    }
    if(_pendingAlpha.isChanged())
    {
        _particleSystem->setAlpha(_pendingAlpha);
        _pendingAlpha.clearChange();
    }
    if(_pendingRadius.isChanged())
    {
        _particleSystem->setRadius(_pendingRadius.get() * WORLD_TO_GRAPHICS_SIZE );
        _pendingRadius.clearChange();
    }
    if(_pendingCloudSetup.isChanged())
    {
        _particleSystem->setupParticles(_pendingCloudSetup);
        _pendingCloudSetup.clearChange();
    }

    return true;
}

void ParticleSystemProxy::setPosition(Vec2 position)
{
    _pendingPosition = position;
}

void ParticleSystemProxy::setAlpha(float alpha)
{
    _pendingAlpha = alpha;
}

void ParticleSystemProxy::setSize(float size)
{
    _pendingSize = size;
}

void ParticleSystemProxy::setRadius(float change)
{
    _pendingRadius = change;
}


void ParticleSystemProxy::setupParticles(ParticleCloudProperties & properties)
{
    _pendingCloudSetup = properties;
}

void ParticleSystemProxy::remove()
{
    globalCocosObjectManager->requestCocosObjectRemoval(this);
}
