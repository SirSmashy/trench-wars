//
//  DebugEntity.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/22/22.
//

#ifndef DebugEntity_h
#define DebugEntity_h

#include "cocos2d.h"
#include "PrimativeDrawer.h"
#include "Entity.h"


USING_NS_CC;

class LineDrawer;

class DebugEntity : public Entity
{
    Label * _label;
    Vec2 _position;
    Vec2 _labelOffset;
    bool _hideOnNotVisible;
    void init(Vec2 position, const std::string & labelString, Vec2 labelOffset, bool hideOnNotVisible);
    std::string _labelText;
    bool _debugging;
    Vector<LineDrawer *> _lines;
    
public:
    PrimativeDrawer * primativeDrawer;

    static DebugEntity * create(Vec2 position, const std::string & labelString,  Vec2 labelOffset, bool hideOnNotVisible);
    virtual ENTITY_TYPE getEntityType() const {return DEBUG_ENTITY;}

    
    void setVisible(bool visible);
    
    Vec2 getPosition() {return _position;}
    void setPosition(Vec2 position);
    
    void addLine(Vec2 start, Vec2 end, Color3B color, bool flowing, bool dotted);
    void clearLines();
    
    virtual void setDebugMe();
    virtual bool isDebugMe();
    virtual void clearDebugMe();
    
    virtual void removeFromGame();
    

};
#endif /* DebugEntity_h */
