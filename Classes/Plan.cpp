//
//  Plan.cpp
//  TrenchWars
//
//  Created by Paul Reed on 4/6/21.
//

#include "Plan.h"
#include "Unit.h"
#include "UnitGroup.h"
#include "EmblemManager.h"
#include "GameEventController.h"
#include "MultithreadedAutoReleasePool.h"
#include <cereal/types/string.hpp>
#include "EngagementManager.h"
#include "TeamManager.h"

Plan::Plan(const std::string & name, int owningPlayerId)
{
    _name = name;
    _executing = false;
    _owningPlayerId = owningPlayerId;
    _owningTeam =  globalTeamManager->getTeamForPlayer(owningPlayerId);
}

Plan::Plan()
{
    _executing = false;
}

Plan * Plan::create(const std::string & name, int owningPlayerId)
{
    Plan * newPlan = new Plan(name, owningPlayerId);
    globalAutoReleasePool->addObject(newPlan);
    return newPlan;
}

Plan * Plan::create()
{
    Plan * newPlan = new Plan();
    globalAutoReleasePool->addObject(newPlan);
    return newPlan;
}


void Plan::addOrder(Unit * orderEntity, Order * order)
{
    order->setOwningPlanId(uID());
    _ordersInPlan.insert_or_assign(orderEntity->uID(), order->uID());
}

void Plan::orderComplete(Unit * orderEntity)
{
    _ordersInPlan.erase(orderEntity->uID());
}

void Plan::removeOrderForUnit(Unit * entityToOrder)
{
    auto orderId = _ordersInPlan.find(entityToOrder->uID());
    if(orderId != _ordersInPlan.end())
    {
        auto order = entityToOrder->getOwningCommand()->getOrderWithId(orderId->second);
        if(order != nullptr)
        {
            entityToOrder->getOwningCommand()->removeOrder(order);
        }
    }
    _ordersInPlan.erase(entityToOrder->uID());
}

void Plan::setName(const std::string & name)
{
    _name = name;
}

void Plan::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    ComparableRef::loadFromArchive(archive);

    archive(_name);
    archive(_executing);
    archive(_owningPlayerId);
    int teamId;
    archive(teamId);
    _owningTeam = globalTeamManager->getTeam(teamId);
    size_t size;
    archive(size);
    ENTITY_ID commandableId;
    ENTITY_ID orderId;
    for(int i = 0; i < size; i++)
    {
        archive(commandableId);
        archive(orderId);
        _ordersInPlan.emplace(commandableId, orderId);
    }
    _owningTeam->addPlan(this);
}

void Plan::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    ComparableRef::saveToArchive(archive);

    archive(_name);
    archive(_executing);
    archive(_owningPlayerId);
    archive(_owningTeam->getTeamId());
    archive(_ordersInPlan.size());
    for(auto orderPair : _ordersInPlan)
    {
        archive(orderPair.first);
        archive(orderPair.second);
    }
}
