//
//  SpriteAnimationDefinitionXMLParser.m
//  Trench Wars
//
//  Created by Paul Reed on 10/6/13.
//  Copyright (c) 2013 . All rights reserved.
//

#include "SpriteDefinitionXMLParser.h"
#include "StringUtils.h"
#include <stdexcept>


SpriteDefinitionXMLParser::SpriteDefinitionXMLParser(EntityFactory * factory)
{
    entityFactory = factory;
    currentEntitySprite = nullptr;
    currentAnimation = nullptr;
}

void SpriteDefinitionXMLParser::parseFile(std::string const & filename)
{
    std::string fileContents = FileUtils::getInstance()->getStringFromFile(filename);
    if(!fileContents.empty())
    {
        xml_document doc;
        pugi::xml_parse_result result = doc.load(fileContents.c_str());
        if(result.status == xml_parse_status::status_ok)
        {
            bool parsedSuccessfully = doc.traverse(*this);
            if(!parsedSuccessfully)
            {
                CCLOG("parse error!");
            }
//            entityFactory->createSpriteBatches();
        }
        else
        {
            CCLOG("parse error %s ",result.description());
        }
    }
    else
    {
        CCLOG("SpriteDefinitionXMLParser: Could not Open File %s",filename.c_str());
    }
}


bool SpriteDefinitionXMLParser::for_each(xml_node& node)
{
    ci_string name = node.name();
    std::string value = node.first_child().value();
    
    try
    {
        if(name.compare("Sprite") == 0)
        {
            std::string entityId;
            std::string plistFile;
            
            auto attribute = node.attribute("id");
            if(attribute != nullptr)
            {
                entityId = attribute.value();
            }
            attribute = node.attribute("plistFile");
            if(attribute != nullptr)
            {
                plistFile = attribute.value();
            }
            currentEntitySprite = entityFactory->createEntityAnimationSet(entityId, plistFile);
            attribute = node.attribute("uiLayer");
            if(attribute != nullptr)
            {
                currentEntitySprite->_uiLayer = attribute.value();
            }
            else
            {
                currentEntitySprite->_uiLayer = false;
            }
        }
        else if(name.compare("AnimationSet") == 0)
        {
            auto attribute = node.attribute("name");
            if(attribute != nullptr)
            {
                currentAnimationType =  entityFactory->nameToAnimationType(attribute.value());
                if(currentEntitySprite->_animationDictionary.find(currentAnimationType) == currentEntitySprite->_animationDictionary.end())
                {
                    currentEntitySprite->_animationDictionary[currentAnimationType] = new Map<DIRECTION, EntityAnimation *>();
                }
            }
        }
        else if(name.compare("SpriteOffset") == 0)
        {
            currentEntitySprite->_positionOffset = parseVector(value);
        }
        else if(name.compare("Animation") == 0)
        {
           if(currentEntitySprite != nullptr)
           {
               auto attribute = node.attribute("name");
               std::string name;
               if(attribute != nullptr)
               {
                   name = attribute.value();
               }
               currentAnimation = new EntityAnimation();
               currentAnimation->_animation = Animation::create();
               currentAnimation->_animation->setDelayPerUnit(0.1f);
               currentAnimation->_animation->retain();
               currentAnimation->_tag = name;
               currentAnimation->_createParticle = false;
               DIRECTION direction = entityFactory->nameToDirection(name);
               currentEntitySprite->_animationDictionary[currentAnimationType]->insert(direction, currentAnimation);
           }
        }

        else if(name.compare("frame") == 0)
        {
            SpriteFrame * newFrame = SpriteFrameCache::getInstance()->getSpriteFrameByName(value);
            if(newFrame != nullptr)
            {
                currentAnimation->_animation->addSpriteFrame(newFrame);
            }
        }
        else if(name.compare("tag") == 0)
        {
            currentAnimation->_tag = value;
        }
        else if(name.compare("particleName") == 0)
        {
            currentAnimation->_createParticle = true;
            currentAnimation->_particleName = value;
        }
        else if(name.compare("particlesCreationFrame") == 0)
        {
            currentAnimation->_particleCreationFrame = std::stoi(value);
        }
        else if(name.compare("particleOffset") == 0)
        {
            currentAnimation->_particleOffset = parseVector(value);
        }
        else if(name.compare("particleHeight") == 0)
        {
            currentAnimation->_particleHeight = std::stof(value);
        }
       
    }
    catch (const std::invalid_argument& ia)
    {
        CCLOG("Invalid argument: %s for node %s with value %s ", ia.what(), name.c_str(), value.c_str());
    }
    
    return true;
}

