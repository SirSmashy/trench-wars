//
//  UnitGroupEmblem.hpp
//  TrenchWars
//
//  Created by Paul Reed on 1/4/24.
//

#ifndef UnitGroupEmblem_h
#define UnitGroupEmblem_h

#include "Emblem.h"
#include "Objective.h"
#include "UnitGroup.h"
#include "CountTo.h"

class UnitGroupEmblem : public Emblem
{
private:
    Objective * _objectiveDraggedOver;
    UnitGroup * _group;
    virtual bool init(UnitGroup * group, EMBLEM_TYPE type, Vec2 position);
    CountTo _circleMenuCountdown;
    bool _showCircleMenuAfterCount;
    
    UnitGroupEmblem();
    ~UnitGroupEmblem();

public:
    static UnitGroupEmblem * create(UnitGroup * group, EMBLEM_TYPE type, Vec2 position);
    
    virtual void update(float deltaTime);
    virtual void setEmblemType(EMBLEM_TYPE type);
    
    virtual void emblemDragged(Vec2 position);
    virtual void emblemDragEnded(Vec2 position);
    
    virtual void emblemHoverEnter(Vec2 position);
    virtual void emblemHoverLeave(Vec2 position);
};

#endif /* UnitGroupEmblem_h */
