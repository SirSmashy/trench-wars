//
//  MovingPhysicalEntity.cpp
//  TrenchWars
//
//  Created by Paul Reed on 1/29/22.
//

#include "MovingPhysicalEntity.h"
#include "World.h"
#include "PathFinding.h"
#include "CollisionGrid.h"
#include "VectorMath.h"
#include "Command.h"
#include "Team.h"
#include "MapSector.h"
#include "SectorGrid.h"
#include "EngagementManager.h"
#include "GameStatistics.h"
#include "Path.h"
#include "GameMenu.h"
#include "InteractiveObjectUtils.h"
#include "TestBed.h"
#include "BackgroundTaskHandler.h"
#include "TeamManager.h"

#include <cereal/types/list.hpp>

MovingPhysicalEntity::MovingPhysicalEntity() :
_pathReady(false),
_pendingGoalPosition(GoalPosition()),
PhysicalEntity(),
_checkFriendCollisionCounter(3,true)

{
}

bool MovingPhysicalEntity::init(MovingPhysicalEntityDefinition * definition, Vec2 position,  Team * team)
{
    if(PhysicalEntity::init(definition,position, true))
    {
        if(definition != nullptr)
        {
            int teamID = team != nullptr ? team->getTeamId() : -1;
            _movePath = new Path(teamID, false, Vec2(0,0));
            _moveRate = definition->_speed;
            _maximumMoveRate = definition->_speed;
            _currentMapSector = nullptr;
            _nextMovePosition = position;
            _performingAction = ACTION_NONE;
            
            _primaryCollisionCell = nullptr;
            _claimedPosition = nullptr;
            collisionTest = false;
            _atGoalPosition = true;
            _goalFacing = RIGHT;
            _waitingForPath = false;
            _timeSinceLastAction = 1000;
            _prone = false;

            _goalPosition.position = position;
            
            _owningTeam = team;
        }
        return true;
    }
    return false;
}

void MovingPhysicalEntity::postLoad()
{
    if(!_atGoalPosition)
    {
        getPath();
    }
//    setGoalPosition(getPosition(), true);
    updateCollision();
}


void MovingPhysicalEntity::query(float deltaTime)
{
//    MicroSecondSpeedTimer timer( [=](long long int time) {
//        globalGameStatistics->setTestValue("M Query: ", (double)time/1000.0);
//    });
    
    if(_collisionsPerSecond > 0)
    {
        _collisionsPerSecond -= deltaTime;
    }
    
    if(_pathReady.isChanged())
    {
        _waitingForPath = false;
        _pathReady.clearChange();
        determineNextMovePosition();
    }
    
    if(!_waitingForPath && _pendingGoalPosition.isChanged())
    {
        startNewMoveGoal(_pendingGoalPosition.get());
        _pendingGoalPosition.clearChange();
    }
    
    if(_waitTime > 0)
    {
        _waitTime -= deltaTime;
    }
    
    if(_atGoalPosition)
    {
        return;
    }
    
    
    //check to see if this actor ran into another actor moving in the same direction
    if(!(globalTestBed->behaviorsToSkip & AVOID_COLLISION_BEHAVIOR)
       && !_avoidingCollision
       && _waitTime <= 0
       && !_waitingForPath
       && _checkFriendCollisionCounter.count()
       && didCollideWithFriendly())
    {
        _collisionsPerSecond += 1;
        if(_collisionsPerSecond > 10000) // FIX magic number
        {
            // Enter collision avoidance mode; find a new path that is less crowded
            avoidCollisions();
        }
        else
        {
            // Stop
            _waitTime = .5; // FIX magic number
        }
    }
    // Have we reached the next point of the move path?
    else if(_nextMovePosition.fuzzyEquals(getPosition(), POSITIONS_SAME_DISTANCE))
    {
        //Ent has reached nextPatch, check to see if there are other nodes to move to along the path
        determineNextMovePosition();
    }
}

void MovingPhysicalEntity::act(float deltaTime)
{
//    MicroSecondSpeedTimer queryTimer( [=](long long int time) {
//        globalEngagementManager->setTestValue("MPE Act: ", (double)time/1000.0);
//    });
    
    bool oneBeforeQueryFrame = globalEngagementManager->isOneBeforeQueryFrame();
    if(oneBeforeQueryFrame)
    {
        calculateAverageVelocity();
    }
    
    _timeSinceLastAction += deltaTime;
    if(_timeSinceLastAction > 1.0) //FIXME: magic number
        _performingAction = ACTION_NONE;

    if(_moveVector.isZero() || _waitTime > 0)
    {
        _velocity.setZero();
        changeAnimationState();
        return;
    }


    Vec2 distance = _nextMovePosition - getPosition();
    _velocity = _moveVector * _moveRate;

    Vec2 translatate = _velocity * deltaTime;
    if(translatate.lengthSquared() > distance.lengthSquared())
    {
        setPosition(_nextMovePosition);
        _velocity.setZero();
    }
    else
    {
        setPosition(getPosition() + translatate);
    }

    setFacing(VectorMath::facingFromVector(translatate));

    if(oneBeforeQueryFrame)
    {
        updateCollision();
    }

    changeAnimationState();
}


void MovingPhysicalEntity::calculateAverageVelocity()
{
    //    MicroSecondSpeedTimer queryTimer( [=](long long int time) {
    //        globalEngagementManager->setTestValue("Average Vel: ", (double)time/1000.0);
    //    });
    while(_velocityMovingWindow.size() >= 3)
    {
        _velocityAccumulator -= _velocityMovingWindow.front();
        _velocityMovingWindow.pop_front();
    }
    _velocityMovingWindow.push_back(_velocity);
    _velocityAccumulator += _velocity;
    
    if(_velocityAccumulator.lengthSquared() < 0.01)
    {
        _velocityAccumulator.setZero();
    }
    
    _averageVelocity = _velocityAccumulator / _velocityMovingWindow.size();
}

Vec2 MovingPhysicalEntity::getGoalPosition()
{
    return _goalPosition.position;
}

void MovingPhysicalEntity::avoidCollisions()
{
    _collisionsPerSecond = 0;
    
    Vec2 vectorToMovePosition = _nextMovePosition - getPosition();
    vectorToMovePosition.normalize();
    int perpendicularDirection = globalRandom->randomInt(0,1);
    Vec2 avoidanceVector = perpendicularDirection == 0 ? vectorToMovePosition.getRPerp() : vectorToMovePosition.getPerp();
    avoidanceVector += vectorToMovePosition;
    avoidanceVector = avoidanceVector.getNormalized() * 2;
    avoidanceVector += getPosition();
    // Check if we can actually move this direction
    bool canMove = getCurrentCollisionGrid()->iterateCellsInLine(getPosition(), avoidanceVector,
    [] (CollisionCell * cell, float distanceTraveled, bool minorCell) {
       return !cell->getTerrainCharacteristics().getImpassable();
    });
    if(!canMove)
    {
        //Can't move this direction, try going the other perpendicular direction
        avoidanceVector = perpendicularDirection == 1 ? vectorToMovePosition.getRPerp() : vectorToMovePosition.getPerp();
        avoidanceVector += vectorToMovePosition;
        avoidanceVector = avoidanceVector.getNormalized()  * 2;
        avoidanceVector += getPosition();
        bool canMove = getCurrentCollisionGrid()->iterateCellsInLine(getPosition(), avoidanceVector,
        [] (CollisionCell * cell, float distanceTraveled, bool minorCell) {
            return !cell->getTerrainCharacteristics().getImpassable();
        });
        // Check if we can actually move this direction
        if(!canMove)
        {
            // Can't avoid the obstacle, give up and follow the standard path
            return;
        }
    }
        
    _avoidingCollision = true;
    setMovePosition(avoidanceVector);
}


bool MovingPhysicalEntity::didCollideWithFriendly()
{
    std::list<PhysicalEntity *> friendMoveCollisions;
    int facingAngle = VectorMath::degreesFromFacing(getFacing());
    Vec2 positionDifference;
    double collisionAngle;
    double facingAngleDifference;
    
    for(auto pair : _collisionCellMembership)
    {
        CollisionCell * cell = pair.second;
        cell->findCollisionsWithEnt(this, getTeamID(), COLLIDE_OWN_TEAM_HUMAN, &friendMoveCollisions);
    }
    
    for(PhysicalEntity * collidedEnt : friendMoveCollisions)
    {
        MovingPhysicalEntity * collidedActor = (MovingPhysicalEntity *) collidedEnt;
        // freely move past actors that aren't moving (they step out of the way)
        if(!collidedActor->isMoving())
            continue;
        
        //FIXME: Crew Weapons don't collide with units in their own unit
//        if( (getEntityType() == CREW_WEAPON || collidedActor->getEntityType() == CREW_WEAPON) && getOwningUnit() == collidedActor->getOwningUnit())
//            continue;
        
        positionDifference = collidedActor->getPosition() - getPosition();
        collisionAngle =  positionDifference.getAngle() * R2D;
        
        if(collisionAngle < 0)
            collisionAngle += 360;
        collisionAngle = fabs(collisionAngle - facingAngle);
        facingAngleDifference = abs(facingAngle - VectorMath::degreesFromFacing(collidedActor->getFacing()));
        
        //if two (or more) actors are right on top of each other then we can get into strange situations in which they take turns blocking each other.
        //To prevent them from blocking each other the actor with the highest entity ID wins
        //I know this is a crap solution resulting from poor collision handling, but it will do..... for now
        if(positionDifference.lengthSquared() < 1)
        {
            if(uID() > collidedActor->uID())
            {
                return true;
            }
        }
        else if(collisionAngle <= 60 || collisionAngle > 300)
        {
            // Only collide if the other actor is moving in a similar direction to this actor (not perpendicular, or opposite)
            if(facingAngleDifference < 100 ||  facingAngleDifference > 260)
            {
                return true;
            }
        }
    }
    return false;
}

void MovingPhysicalEntity::setMovePosition(Vec2 position)
{
    if(!world->arePositionsInSameArea(getPosition(), position))
    {
        transitionToArea(position);
    }
    
    _nextMovePosition = position;
    _moveVector = _nextMovePosition - getPosition();
    _moveVector.normalize();
    if(_moveVector.isZero())
    {
        _velocity = Vec2();
    }
}

void MovingPhysicalEntity::setFacing(DIRECTION newFacing)
{
    _goalFacing = newFacing;
}

void MovingPhysicalEntity::setPerformingAction(ACTIONTYPE action)
{
    _performingAction = action;
    _timeSinceLastAction = 0;
}

void MovingPhysicalEntity::startNewMoveGoal(GoalPosition goal)
{
    Vec2 newGoalPosition = goal.position;

    if(newGoalPosition.fuzzyEquals(_goalPosition.position, POSITIONS_SAME_DISTANCE))
    {
        return;
    }
    _atGoalPosition = false;
    _goalPosition.position = newGoalPosition;
    _goalPosition.ignoreCover = goal.ignoreCover;
    _movePath->clear();

    startedMoving();
    
    if(!checkIfMoveNeedsPath())
    {
        CollisionCell * goalCell = world->getCollisionGrid(_goalPosition.position)->getCellForCoordinates(_goalPosition.position);
        setClaimedPosition(goalCell);
        setMovePosition(_goalPosition.position);
        return;
    }

    getPath();
}

bool MovingPhysicalEntity::checkIfMoveNeedsPath()
{
    CollisionCell * current = getCurrentCollisionGrid()->getCellForCoordinates(getPosition());
    CollisionCell * goalCell = world->getCollisionGrid(_goalPosition.position)->getCellForCoordinates(_goalPosition.position);
    
    
    if(current->isTraversable() &&
       goalCell->isTraversable())
    {
        Vec2 distance = goalCell->getVectorToOtherCell(current);
        if(fabs(distance.x) <= 1 || fabs(distance.y) <= 1)
        {
            return false;
        }
    }
    return true;

}

void MovingPhysicalEntity::setGoalPosition(Vec2 position, bool ignoreCover)
{
    GoalPosition goal;
    goal.position = position;
    goal.ignoreCover = ignoreCover;
    _pendingGoalPosition = goal;
    _atGoalPosition = false; //Hmmmmmmmm, this may cause a bug
}

CollisionCell * MovingPhysicalEntity::getOptimumCellAroundCell(CollisionCell * center)
{
    CollisionGrid * grid = world->getCollisionGrid(center->getCellPosition());
    CollisionCell * best = nullptr;
    short bestValue = -1;
    int searchRadius = 10;
    
    grid->iterateCellsInCircle(center->getCellPosition(), searchRadius, [this, &best, &bestValue] (CollisionCell * cell)
    {
        if(cell->isTraversable() && _owningTeam->tryToClaimPosition(cell, uID()))
        {
            short cover = cell->getTerrainCharacteristics().getTotalSecurity();
            if(cover > bestValue)
            {
                if(best != nullptr)
                {
                    _owningTeam->releasePositionClaim(best);
                }
                bestValue = cover;
                best = cell;
            }
            else
            {
                _owningTeam->releasePositionClaim(cell);
            }
        }
        return true;
    });
    return best;
}

void MovingPhysicalEntity::getPath()
{
    //clear the current move path
    _waitingForPath = true;
    retain();
//    globalGameStatistics->setTestValue("Path: ", 1);
    globalBackgroundTaskHandler->addPathfindingTask([this]
    {
//        MicroSecondSpeedTimer timer( [] (long long int time) {
//            LOG_DEBUG("Time: %f \n", time/1000.0);
//        });
        _goalPosition.position = world->getCollisionGrid(_goalPosition.position)->getValidMoveCellAroundPosition(_goalPosition.position);
        CollisionCell * goalCell = world->getCollisionGrid(_goalPosition.position)->getCellForCoordinates(_goalPosition.position);
//        LOG_DEBUG("%lld build path %.1f %.1f \n", uID(), _goalPosition.position.x, _goalPosition.position.y);
        setClaimedPosition(goalCell);
        _movePath->buildPath(getPosition(), _goalPosition.position, MOVE_NO_PREFERNCE);
        _pathReady = true;
//        globalGameStatistics->setTestValue("Path: ", -1);
        release();
        return false;
    });
}

/**
  * Detereminese where  the entity should move next
   * Return: True if the entity has a new move position. False if the entity
 */
void MovingPhysicalEntity::determineNextMovePosition()
{
    if((globalTestBed->behaviorsToSkip & GET_NEXT_MOVE_BEHAVIOR))
    {
        return;
    }
    if(_waitingForPath)
    {
        return;
    }
    
    if(_avoidingCollision)
    {
        _avoidingCollision = false;
        getPath();
        return;
    }
    
    PathStatus status = _movePath->getPathStatus();
    //Just finished crossing a border into the next patch, get the next patch in the move path
    if(status == PATH_STATUS_READY)
    {
        Vec2 newPosition = _movePath->getNextPathPosition();
        setMovePosition(newPosition);
    }
    else if(status == PATH_STATUS_NEEDS_NEXT_SEGMENT)
    {
        
        _waitingForPath = true;
        retain();
//        globalGameStatistics->setTestValue("Path: ", 1);
        globalBackgroundTaskHandler->addPathfindingTask([this]
        {
            _movePath->buildNextPathSegment(getPosition());
//            globalGameStatistics->setTestValue("Path: ", -1);
            release();
            _pathReady = true;
            return false;
        });
    }
    else
    {
        _atGoalPosition = true;
        _moveVector.setZero();
        goalPositionReached();
    }
}

void MovingPhysicalEntity::startedMoving()
{
    
}

void MovingPhysicalEntity::goalPositionReached()
{
    
}



/*changeAnimationState
 * Check to see if the current animation state is different than the next animation state.
 * If different, set the current animation to the next state.
 *
 */
void MovingPhysicalEntity::changeAnimationState()
{
    ANIMATION_TYPE nextAnimationState = _sprite->getCurrentAnimationState();
//    bool moving = ! _averageVelocity.isZero(); //TODO fix why did I make this use average velocity? I assume I had a good reason. Once I figure it out I really need to document it here
    bool moving = ! _velocity.isZero();
    if(_facing != _goalFacing)
    {
        _facing = _goalFacing;
    }
    
    if(_prone)
    {
        if(moving) // TODO fix  && !_averageVelocity.equals(_velocity))
        {
            nextAnimationState = PRONE_MOVE;
        }
        else
        {
            nextAnimationState = PRONE_IDLE;
        }
    }
    else
    {
        if(moving) // TODO fix  && !_averageVelocity.equals(_velocity))
        {
            nextAnimationState = MOVE;
        }
        else if(_performingAction == ACTION_DIG)
        {
            nextAnimationState = DIG;
        }
        else if(_performingAction == ACTION_HELP)
        {
            nextAnimationState = WORK;
        }
        else if(!moving)
        {
            nextAnimationState = DEFAULT;
        }
    }

    if(_sprite->getCurrentAnimationState() != nextAnimationState || _facing != _sprite->getCurrentAnimationDirection())
    {
        _sprite->setAnimation(nextAnimationState, true, _facing);
    }
}

void MovingPhysicalEntity::setClaimedPosition(CollisionCell * newPosition)
{
    if(_claimedPosition != nullptr)
    {
        _owningTeam->releasePositionClaim(_claimedPosition);
    }
    _claimedPosition = newPosition;
    _owningTeam->setPositionClaim(_claimedPosition, uID());
}

void MovingPhysicalEntity::updateCollisionAlternative()
{
//    MicroSecondSpeedTimer queryTimer( [=](long long int time) {
//        globalEngagementManager->setTestValue("Collision: ", (double)time/1000.0);
//    });
    _collisionCellMembership.clear();
    
    std::vector<CollisionCell *> cells;
    std::unordered_set<ENTITY_ID> cellIdSet;
    
    getCurrentCollisionGrid()->cellsInRect(getCollisionBounds(), &cells);
    _primaryCollisionCell = cells.at(0);
    for(auto cell : cells)
    {
        cellIdSet.emplace(cell->uID());
        if(_collisionCellMembership.find(cell->uID()) == _collisionCellMembership.end())
        {
            cell->addMovingPhysicalEntity(this);
            _collisionCellMembership.emplace(cell->uID(), cell);
        }
    }
    
    std::list<ENTITY_ID> cellsToRemove;
    for(auto pair : _collisionCellMembership)
    {
        if(cellIdSet.find(pair.first) == cellIdSet.end())
        {
            cellsToRemove.push_back(pair.first);
            pair.second->removeMovingPhysicalEntity(this);
        }
    }
    
    for(auto cellId : cellsToRemove)
    {
        
        _collisionCellMembership.erase(cellId);
    }

    
    //FIXME: update the map sector this actor is in
    MapSector * sector = globalSectorGrid->sectorForCell(_primaryCollisionCell);
    if(sector != nullptr && _currentMapSector != sector)
    {
        if(_currentMapSector != nullptr)
        {
            _currentMapSector->removeEntity(this);
        }

        _currentMapSector = sector;
        _currentMapSector->addEntity(this);
    }
}


void MovingPhysicalEntity::updateCollision()
{
    for(auto cell : _collisionCellMembership)
    {
        cell.second->removeMovingPhysicalEntity(this);
    }
    _collisionCellMembership.clear();
    
    std::vector<CollisionCell *> cells;
    getCurrentCollisionGrid()->cellsInRect(getCollisionBounds(), &cells);
    _primaryCollisionCell = cells.at(0);
    _inTerrain = _primaryCollisionCell->getTerrainCharacteristics();

    for(auto cell : cells)
    {
        cell->addMovingPhysicalEntity(this);
        _collisionCellMembership.emplace(cell->uID(), cell);
    }
    
    //FIXME: update the map sector this actor is in
    MapSector * sector = globalSectorGrid->sectorForCell(_primaryCollisionCell);
    if(sector != nullptr && _currentMapSector != sector)
    {
        if(_currentMapSector != nullptr)
        {
            _currentMapSector->removeEntity(this);
        }

        _currentMapSector = sector;
        _currentMapSector->addEntity(this);
    }
}

int MovingPhysicalEntity::getTeamID()
{
    return _owningTeam != nullptr ? _owningTeam->getTeamId() : -1;
}


void MovingPhysicalEntity::transitionToArea(Vec2 positionInArea)
{
    world->transitionEntityToNewArea(getPosition(), positionInArea, this);
    setPosition(positionInArea);
}

void MovingPhysicalEntity::setDebugMe()
{
    PhysicalEntity::setDebugMe();
    _movePath->setDebug(true);
}

void MovingPhysicalEntity::clearDebugMe()
{
    PhysicalEntity::clearDebugMe();
    _movePath->setDebug(false);
}

void MovingPhysicalEntity::removeFromCollisionGrid()
{
    for(auto cell : _collisionCellMembership)
    {
        cell.second->removeMovingPhysicalEntity(this);
    }
    _collisionCellMembership.clear();
}

void MovingPhysicalEntity::removeFromGame()
{
    removeFromCollisionGrid();
    if(_currentMapSector != nullptr)
    {
        _currentMapSector->removeEntity(this);
    }
    if(_claimedPosition != nullptr)
    {
        _owningTeam->releasePositionClaim(_claimedPosition);
    }
    PhysicalEntity::removeFromGame();
}

int MovingPhysicalEntity::hasUpdateToSendOverNetwork()
{
    int variables = PhysicalEntity::hasUpdateToSendOverNetwork();
    
    
    if(_moveRate != _networkVariables.moveRate)
    {
        variables++;
    }
    if(_nextMovePosition != _networkVariables.nextMovePosition)
    {
        variables++;
    }
    if(_moveVector != _networkVariables.moveVector)
    {
        variables++;
    }
    if(_atGoalPosition != _networkVariables.atGoalPosition)
    {
        variables++;
    }
    if(_performingAction != _networkVariables.performingAction)
    {
        variables++;
    }
    if(_goalFacing != _networkVariables.goalFacing)
    {
        variables++;
    }
    if(_waitTime != _networkVariables.waitTime)
    {
        variables++;
    }
    if(_prone != _networkVariables.prone)
    {
        variables++;
    }
    
    return variables;
}


void MovingPhysicalEntity::saveNetworkUpdate(cereal::BinaryOutputArchive & archive)
{
    PhysicalEntity::saveNetworkUpdate(archive);
    unsigned short modifiedVariables = 0;
    if(_moveRate != _networkVariables.moveRate)
    {
        modifiedVariables += 1;
    }
    if(_nextMovePosition != _networkVariables.nextMovePosition)
    {
        modifiedVariables += 2;
    }
    if(_moveVector != _networkVariables.moveVector)
    {
        modifiedVariables += 4;
    }
    if(_atGoalPosition != _networkVariables.atGoalPosition)
    {
        modifiedVariables += 8;
    }
    if(_performingAction != _networkVariables.performingAction)
    {
        modifiedVariables += 16;
    }
    if(_goalFacing != _networkVariables.goalFacing)
    {
        modifiedVariables += 32;
    }
    if(_waitTime != _networkVariables.waitTime)
    {
        modifiedVariables += 64;
    }
    if(_prone != _networkVariables.prone)
    {
        modifiedVariables += 128;
    }
    archive(modifiedVariables);
    if(_moveRate != _networkVariables.moveRate)
    {
        _networkVariables.moveRate = _moveRate;
        archive(_moveRate);
    }
    if(_nextMovePosition != _networkVariables.nextMovePosition)
    {
        _networkVariables.nextMovePosition = _nextMovePosition;
        archive(_nextMovePosition);
    }
    if(_moveVector != _networkVariables.moveVector)
    {
        _networkVariables.moveVector = _moveVector;
        archive(_moveVector);
    }
    if(_atGoalPosition != _networkVariables.atGoalPosition)
    {
        _networkVariables.atGoalPosition = _atGoalPosition;
        archive(_atGoalPosition);
    }
    if(_performingAction != _networkVariables.performingAction)
    {
        _networkVariables.performingAction = _performingAction;
        archive(_performingAction);
    }
    if(_goalFacing != _networkVariables.goalFacing)
    {
        _networkVariables.goalFacing = _goalFacing;
        archive(_goalFacing);
    }
    if(_waitTime != _networkVariables.waitTime)
    {
        _networkVariables.waitTime = _waitTime;
        archive(_waitTime);
    }
    if(_prone != _networkVariables.prone)
    {
        _networkVariables.prone = _prone;
        archive(_prone);
    }

//    archive(_position);
}

void MovingPhysicalEntity::updateFromNetwork(cereal::BinaryInputArchive & archive)
{
    PhysicalEntity::updateFromNetwork(archive);
    unsigned short modifiedVariables;
    archive(modifiedVariables);
    
    if(modifiedVariables & 1)
    {
        archive(_moveRate);
    }
    if(modifiedVariables & 2)
    {
        archive(_nextMovePosition);
    }
    if(modifiedVariables & 4)
    {
        archive(_moveVector);
    }
    if(modifiedVariables & 8)
    {
        archive(_atGoalPosition);
    }
    if(modifiedVariables & 16)
    {
        archive(_performingAction);
    }
    if(modifiedVariables & 32)
    {
        archive(_goalFacing);
    }
    if(modifiedVariables & 64)
    {
        archive(_waitTime);
    }   
    if(modifiedVariables & 128)
    {
        archive(_prone);
    }
//    Vec2 position;
//    archive(position);
//    if(position != _position)
//    {
//        LOG_ERROR("Oh no positions don't line up! %.0f %.0f vs %.0f %.0f \n", position.x, position.y, _position.x, _position.y );
//    }

}


void MovingPhysicalEntity::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    PhysicalEntity::saveToArchive(archive);
    archive( _velocity);
    archive( _averageVelocity);
    archive( _velocityAccumulator);
    archive( _velocityMovingWindow);
    archive( _moveRate);
    archive( _maximumMoveRate);
    archive( _nextMovePosition);
    archive( _moveVector);
    
    if(_owningTeam != nullptr)
    {
        archive(_owningTeam->getTeamId());
    }
    else
    {
        archive(-1);
    }

    archive( _atGoalPosition);
    archive( (int) _performingAction);
    archive( _timeSinceLastAction);
    archive( (int) _goalFacing);
    archive( _goalPosition);
    archive( _pendingGoalPosition);
    archive( _checkFriendCollisionCounter);
    archive( _collisionsPerSecond);
    archive( _avoidingCollision);
    archive( _waitTime);
}

void MovingPhysicalEntity::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    PhysicalEntity::loadFromArchive(archive);
    archive( _velocity);
    archive( _averageVelocity);
    archive( _velocityAccumulator);
    archive( _velocityMovingWindow);
    archive( _moveRate);
    archive( _maximumMoveRate);
    archive( _nextMovePosition);
    archive( _moveVector);
    
    int teamID;
    archive(teamID);
    _owningTeam = globalTeamManager->getTeam(teamID);
    
    archive( _atGoalPosition);
    int performing;
    archive( performing);
    _performingAction = (ACTIONTYPE) performing;
    archive( _timeSinceLastAction);
    int facing;
    archive(facing);
    _goalFacing = (DIRECTION) facing;
    archive( _goalPosition);
    archive( _pendingGoalPosition);
    archive( _checkFriendCollisionCounter);
    archive( _collisionsPerSecond);
    archive( _avoidingCollision);
    archive( _waitTime);
    
    _waitingForPath = false;
    _claimedPosition = nullptr;
    int teamId = _owningTeam != nullptr ? _owningTeam->getTeamId() : -1;
    _movePath = new Path(teamID, false, Vec2(0,0));
    _currentMapSector = nullptr;
}



void MovingPhysicalEntity::populateDebugPanel(GameMenu * menu, Vec2 location)
{
    PhysicalEntity::populateDebugPanel(menu,location);
    std::ostringstream outStream;
    outStream << std::fixed;
    outStream.precision(0);

    outStream.str("");
    outStream << "Vel: " << _averageVelocity.x << " " << _averageVelocity.y;
    menu->addText(outStream.str());
    
    outStream.str("");
    outStream << "Prone: " << _prone;
    menu->addText(outStream.str());
    
    outStream.str("");
    outStream << "Cover: " << (int) getTotalSecurity();
    menu->addText(outStream.str());
    
    menu->addSeperater();

    
    if(!atGoalPosition())
    {
        outStream.str("");
        outStream << "Move: " << _nextMovePosition.x << " " << _nextMovePosition.y;
        menu->addText(outStream.str());
        
        outStream.str("");
        Vec2 goal = getGoalPosition();
        outStream << "Goal: " << goal.x << " " << goal.y;
        menu->addText(outStream.str());
        
        outStream.str("");
        outStream << "Path: ";
        menu->addText(outStream.str());
        
        for(auto path : _movePath->getPathNodes())
        {
            outStream.str("");
            outStream << "   Pos: " << path->getPosition().x << " " << path->getPosition().y;
            menu->addText(outStream.str());
        }
    }
    
    menu->addSeperater();

    outStream.str("");
    outStream << "Wait: " << _waitTime;
    menu->addText(outStream.str());
    
}
