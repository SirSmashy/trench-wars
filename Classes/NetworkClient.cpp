//
//  NetworkClient.cpp
//  TrenchWars
//
//  Created by Paul Reed on 3/11/24.
//

#include "NetworkClient.h"
#include "TrenchWarsManager.h"

const uint16 DEFAULT_CLIENT_PORT = 27020;


NetworkClient::NetworkClient()
{    // Select instance to use.  For now we'll always use the default.
    
}

void NetworkClient::connectToServer(const std::string & serverIp)
{
    SteamNetworkingIPAddr addrServer;
    addrServer.Clear();
    if ( !addrServer.ParseString( serverIp.c_str()) )
    {
        LOG_ERROR( "Invalid server address '%s'", serverIp.c_str());
        return; // TODO add fatal error handling
    }
    
    if ( addrServer.m_port == 0 )
        addrServer.m_port = DEFAULT_CLIENT_PORT;
    
    SteamNetworkingUtils()->SetGlobalConfigValueInt32( k_ESteamNetworkingConfig_SendBufferSize, 3072000 );
    
    // Start connecting
    char szAddr[ SteamNetworkingIPAddr::k_cchMaxString ];
    addrServer.ToString( szAddr, sizeof(szAddr), true );
    LOG_INFO( "Connecting to server at %s", szAddr );
    SteamNetworkingConfigValue_t opt;
    opt.SetPtr( k_ESteamNetworkingConfig_Callback_ConnectionStatusChanged, (void*)NetworkInterface::SteamNetConnectionStatusChangedCallback );
    _connectionToServer = _networkInterface->ConnectByIPAddress( addrServer, 1, &opt );
    
    if ( _connectionToServer == k_HSteamNetConnection_Invalid )
    {
        _networkStatus = NETWORK_STATUS_ERROR;
        LOG_ERROR( "Failed to create connection! \n" );
    }
    
    _networkStatus = NETWORK_STATUS_CONNECTING_TO_SERVER;
}

void NetworkClient::OnSteamNetConnectionStatusChanged( SteamNetConnectionStatusChangedCallback_t * connectionInfo )
{
    assert( connectionInfo->m_hConn == _connectionToServer || _connectionToServer == k_HSteamNetConnection_Invalid );
    
    // What's the state of the connection?
    switch ( connectionInfo->m_info.m_eState )
    {
        case k_ESteamNetworkingConnectionState_None:
            // NOTE: We will get callbacks here when we destroy connections.  You can ignore these.
            break;
            
        case k_ESteamNetworkingConnectionState_ClosedByPeer:
        case k_ESteamNetworkingConnectionState_ProblemDetectedLocally:
        {
            // Print an appropriate message
            if ( connectionInfo->m_eOldState == k_ESteamNetworkingConnectionState_Connecting )
            {
                // Note: we could distinguish between a timeout, a rejected connection,
                // or some other transport problem.
                LOG_ERROR( "We sought the remote host, yet our efforts were met with defeat.  (%s) \n", connectionInfo->m_info.m_szEndDebug );
            }
            else if ( connectionInfo->m_info.m_eState == k_ESteamNetworkingConnectionState_ProblemDetectedLocally )
            {
                LOG_ERROR( "Alas, troubles beset us; we have lost contact with the host.  (%s) \n", connectionInfo->m_info.m_szEndDebug );
            }
            else
            {
                // NOTE: We could check the reason code for a normal disconnection
                LOG_INFO( "The host hath bidden us farewell.  (%s) \n", connectionInfo->m_info.m_szEndDebug );
            }
            
            _networkFailures.push_back(NetworkFailure(NETWORK_FAILURE_CONNECT_TO_SERVER_FAILED, connectionInfo->m_hConn, 0));
            
            // Clean up the connection.  This is important!
            // The connection is "closed" in the network sense, but
            // it has not been destroyed.  We must close it on our end, too
            // to finish up.  The reason information do not matter in this case,
            // and we cannot linger because it's already closed on the other end,
            // so we just pass 0's.
            _networkInterface->CloseConnection( connectionInfo->m_hConn, 0, nullptr, false );
            _connectionToServer = k_HSteamNetConnection_Invalid;
            _networkStatus = NETWORK_STATUS_ERROR;
            
            break;
        }
            
        case k_ESteamNetworkingConnectionState_Connecting:
            // We will get this callback when we start connecting.
            // We can ignore this.
            break;
            
        case k_ESteamNetworkingConnectionState_Connected:
            _networkStatus = NETWORK_STATUS_CONNECTED_TO_SERVER;
            LOG_INFO( "Connected to server OK \n" );
            break;
            
        default:
            // Silences -Wswitch
            break;
    }
}

void NetworkClient::pollIncomingMessages()
{
    ISteamNetworkingMessage ** messages = new ISteamNetworkingMessage*[16];
    if(_networkStatus != NETWORK_STATUS_CONNECTED_TO_SERVER)
    {
        return;
    }
    int numMsgs = _networkInterface->ReceiveMessagesOnConnection( _connectionToServer, messages, 16 );
    if ( numMsgs == 0 )
    {
        return;
    }
    if ( numMsgs < 0 )
    {
        LOG_ERROR( "Error checking for messages! \n" );
    }
    
    for(int i = 0; i < numMsgs; i++)
    {
        char * data = (char *) messages[i]->m_pData;
        MessageHeaderUnion header;
        header.byteValue = data;
        if(header.header->messageCount > 1)
        {
            parseChunkedMessage(data, messages[i]->m_cbSize);
        }
        else
        {
            parseStandardMessage(data, messages[i]->m_cbSize);
        }
        messages[i]->Release();
    }
}

void NetworkClient::sendPreparedDataToPlayer(MessageBuffer * buffer, int playerId)
{
    auto it = _inUseBuffers.find(buffer->_bufferId);
    if(it == _inUseBuffers.end())
    {
        return;
    }
    
    buffer->connectionsToSendTo.clear();
    buffer->connectionsToSendTo.push_back(_connectionToServer);
    _bufferMutex.lock();
    _buffersWaitingToBeSent.push_back(buffer);
    _inUseBuffers.erase(buffer->_bufferId);
    _bufferMutex.unlock();
}

