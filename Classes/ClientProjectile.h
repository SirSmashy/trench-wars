//
//  ClientProjectile.hpp
//  TrenchWars
//
//  Created by Paul Reed on 4/8/24.
//

#ifndef ClientProjectile_h
#define ClientProjectile_h

#include "Projectile.h"

class ClientProjectile : public Projectile
{
    virtual void act(float deltaTime);
    virtual void createExplosion();

};

#endif /* ClientProjectile_hpp */
