//
//  TeamManager.cpp
//  TrenchWars
//
//  Created by Paul Reed on 6/25/23.
//

#include "TeamManager.h"
#include "SectorGrid.h"
#include "AICommand.h"
#include "MenuLayer.h"
#include "Team.h"
#include "TrenchWarsManager.h"


TeamManager * globalTeamManager;

TeamManager::TeamManager(Vector<TeamDefinition *> & teams)
{
    globalTeamManager = this;
    _localTeamId = globalTrenchWarsManager->getLocalPlayer()->teamId;
    int playerId;
    for(TeamDefinition * teamDef : teams)
    {
        globalSectorGrid->addTeam();
        Team * team = Team::create(teamDef);
        _teams.pushBack(team);
    }
}

TeamManager::TeamManager(cereal::BinaryInputArchive & inputArchive)
{
    globalTeamManager = this;
    size_t size;
    inputArchive(size);
    for(int i = 0; i < size; i++)
    {
        globalSectorGrid->addTeam();
        Team * newTeam = Team::create();
        _teams.pushBack(newTeam);
    }
    
    for(auto team : _teams)
    {
        team->loadFromArchive(inputArchive);
    }
}

void TeamManager::postLoad()
{
    for(auto team : _teams)
    {
        team->postLoad();
    }
}

void TeamManager::query(float deltaTime)
{
    for(Team * team : _teams)
    {
        team->query(deltaTime);
    }
}

Team * TeamManager::getTeam(int teamId)
{
    for(Team * team : _teams)
    {
        if(team->getTeamId() == teamId)
        {
            return team;
        }
    }
    return nullptr;
}

Team * TeamManager::getLocalPlayersTeam()
{
    for(Team * team : _teams)
    {
        if(team->getTeamId() == _localTeamId)
        {
            return team;
        }
    }
    return nullptr;
}

Team * TeamManager::getTeamForPlayer(int playerId)
{
    Player * player = globalTrenchWarsManager->getPlayerForPlayerId(playerId);
    if(player != nullptr)
    {
        for(Team * team : _teams)
        {
            if(team->getTeamId() == player->teamId)
            {
                return team;
            }
        }
    }
    
    return nullptr;
}

Command * TeamManager::getCommand(int commandId)
{
    for(Team * team : _teams)
    {
        Command * command = team->getCommandForId(commandId);
        if(command != nullptr)
        {
            return command;
        }
    }
    return nullptr;
}


void TeamManager::shutdown()
{
    for(Team * team : _teams)
    {
        for(Command * command : team->getCommands())
        {
            command->shutdown();
        }
    }

}


void TeamManager::saveToArchive(cereal::BinaryOutputArchive & archive)
{
    archive(_teams.size());
    for(Team * team : _teams)
    {
        team->saveToArchive(archive);
    }
}
