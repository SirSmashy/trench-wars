//
//  ObjectiveObjectCollection.cpp
//  TrenchWars
//
//  Created by Paul Reed on 9/19/20.
//

#include "ObjectiveObjectCollection.h"
#include "GameVariableStore.h"
#include "Objective.h"
#include "CollisionCell.h"
#include "StaticEntity.h"
#include "EngagementManager.h"
#include "EntityManager.h"
#include "VectorMath.h"
#include "Bunker.h"
#include "ProposedEntity.h"
#include "CollisionGrid.h"
#include "MultithreadedAutoReleasePool.h"
#include "InteractiveObjectUtils.h"

void ObjectiveObjectCollection::init(Objective * owningObjective, ObjectCollectionType type)
{
    _owningObjective = owningObjective;
    _objectCollectionType = type;
}

ObjectiveObjectCollection * ObjectiveObjectCollection::create(Objective * owningObjective, ObjectCollectionType type)
{
    ObjectiveObjectCollection * newCollection = new ObjectiveObjectCollection();
    newCollection->init(owningObjective, type);
    globalAutoReleasePool->addObject(newCollection);
    return newCollection;
}


PhysicalEntity * ObjectiveObjectCollection::getEntityNearestPosition(Vec2 position, bool weightByAvailableWorkSlots)
{
    entitiesMutex.lock();
    double bestDistance = 99999999;
    PhysicalEntity * bestEnt = nullptr;
    
    for(PhysicalEntity * ent : _entities)
    {
        double distance = ent->getPosition().distanceSquared(position);
        if(weightByAvailableWorkSlots)
        {
            distance -= ent->getFreeWorkPositions() * 10; //FIXME: TODO fix magic number
        }
        if(distance < bestDistance)
        {
            bestEnt = ent;
            bestDistance = distance;
        }
    }
    entitiesMutex.unlock();
    return bestEnt;
}

Vec2 ObjectiveObjectCollection::getAveragePosition()
{
    entitiesMutex.lock();
    Vec2 position;
    for(PhysicalEntity * ent : _entities)
    {
        position += ent->getPosition();
    }
    position.x /= _entities.size();
    position.y /= _entities.size();

    entitiesMutex.unlock();
    return position;
}

void ObjectiveObjectCollection::addEntity(PhysicalEntity * ent)
{
    entitiesMutex.lock();
    _entities.emplace(ent);
    entitiesMutex.unlock();
}

void ObjectiveObjectCollection::removeEntity(PhysicalEntity * ent)
{
    entitiesMutex.lock();
    _entities.erase(ent);
    entitiesMutex.unlock();
}

bool ObjectiveObjectCollection::containsEntity(PhysicalEntity * ent)
{
    std::lock_guard<std::mutex> lock(entitiesMutex);
    return _entities.contains(ent);
}

bool ObjectiveObjectCollection::testCollisionWithPoint(Vec2 point)
{
    entitiesMutex.lock();
    for(PhysicalEntity * ent : _entities)
    {
        if(ent->testCollisionWithPoint(point))
        {
            entitiesMutex.unlock();
            return true;
        }
    }
    entitiesMutex.unlock();
    return false;
}

bool ObjectiveObjectCollection::isWorkComplete()
{
    for(auto ent : _entities)
    {
        if(!ent->isWorkComplete())
        {
            return false;
        }
    }
    return true;
}

void ObjectiveObjectCollection::remove()
{
    entitiesMutex.lock();
    for(PhysicalEntity * ent : _entities)
    {
        if(!ent->isWorkComplete())
        {
            ent->removeFromGame();
        }
    }
    _entities.clear();
    entitiesMutex.unlock();
}

void ObjectiveObjectCollection::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    size_t size;
    archive(size);
    for(int i = 0; i < size; i++)
    {
        ENTITY_ID thingId;
        archive(thingId);
        PhysicalEntity * ent = dynamic_cast<PhysicalEntity *>(globalEntityManager->getEntity(thingId));
        _entities.emplace(ent);
    }
    
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        _owningObjective = dynamic_cast<Objective *>(globalEntityManager->getEntity(thingId));
    }
    else
    {
        _owningObjective = nullptr;
    }
}

void ObjectiveObjectCollection::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    archive(_entities.size());
    for(PhysicalEntity * ent : _entities)
    {
        archive(ent->uID());
    }
    
    if(_owningObjective != nullptr)
    {
        archive(_owningObjective->uID());
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
}
