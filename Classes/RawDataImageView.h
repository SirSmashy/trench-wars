//
//  TextureImageView.hpp
//  TrenchWars
//
//  Created by Paul Reed on 11/14/23.
//

#ifndef RawDataImageViewh
#define RawDataImageView_h

#include "cocos2d.h"
#include "ui/CocosGUI.h"

using namespace cocos2d::ui;

USING_NS_CC;


class RawDataImageView : public ImageView
{
    RawDataImageView();
    
protected:
    unsigned char * _imageData;
    Image * _image;
    Texture2D * _imageTexture;

    void initWithData(const unsigned char * data, size_t size);
public:
    ~RawDataImageView();
    static RawDataImageView * createWithData(const unsigned char * data, size_t size);
    
};


#endif /* TextureImageView_h */
