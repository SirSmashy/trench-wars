//
//  Camera.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/4/20.
//

#ifndef Game_Camera_h
#define Game_Camera_h

#include <stdio.h>

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "AutoSizedLayout.h"
#include <cereal/archives/binary.hpp>


USING_NS_CC;
using namespace cocos2d::ui;

class GameCamera : public Ref
{
private:
    double _currentScale;
    Vec2 _currentCammeraPosition;
    Rect cameraBounds;
    
    Vec2 convertPointToNodeSpace(Vec2 point, Node * node);
    Vec2 convertPointWorldSpace(Vec2 touchLocation, Node * node);
    
    void setCameraPosition(Vec2 newCameraPosition);
    
    bool init();
public:
    GameCamera();
    
    static GameCamera * create();
    void panScreen(Vec2 delta);
    void setScreenPosition(Vec2 position);
    void zoom(float zoomScale, Vec2 towardPoint);
    Vec2 getCameraPositionWorld();
    
    void saveToArchive(cereal::BinaryOutputArchive & archive);
    void loadFromArchive(cereal::BinaryInputArchive & archive);

    
    void reset();
};

extern GameCamera * globalCamera;
#endif /* Camera_h */
