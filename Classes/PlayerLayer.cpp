//
//  PlayerLayer.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/4/20.
//

#include "PlayerLayer.h"


PlayerLayer * globalPlayerLayer;

PlayerLayer::PlayerLayer()
{
    
}


bool PlayerLayer::init()
{
    if(Node::init())
    {
        Size size;
        size.height = 1000000;
        size.width = 1000000;
        setContentSize(size);
        globalPlayerLayer = this;
        return true;
    }
    return false;
}
    
PlayerLayer * PlayerLayer::create()
{
    PlayerLayer * layer =  new PlayerLayer();
    if(layer->init())
    {
        layer->autorelease();
        return layer;
    }
    layer->release();
    return nullptr;
}
