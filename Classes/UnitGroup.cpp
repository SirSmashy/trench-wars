//
//  UnitGroup.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/12/20.
//

#include "UnitGroup.h"
#include "EntityManager.h"
#include "ObjectiveObjectCollection.h"
#include "CollisionCell.h"
#include "Emblem.h"
#include "Command.h"
#include "World.h"
#include "Objective.h"
#include "EmblemManager.h"
#include "MultithreadedAutoReleasePool.h"
#include "CollisionGrid.h"
#include "EngagementManager.h"
#include "TeamManager.h"
#include "GameEventController.h"
#include "UIController.h"

UnitGroup::UnitGroup()
{
    
}

UnitGroup::~UnitGroup()
{

}

bool UnitGroup::init(Vector<Unit *> & units, Team * owningTeam)
{
    _owningTeam = owningTeam;
    _unitsInGroup.pushBack(units);
    _lineToOrder = nullptr;
    globalUIController->reportNewUnitGroup(this);
    return true;
}
    
/*
 *
 */
UnitGroup * UnitGroup::create(Vector<Unit *> & units, Team * owningTeam)
{
    UnitGroup * newGroup = new UnitGroup();
    if(newGroup->init(units, owningTeam)) {
        globalAutoReleasePool->addObject(newGroup);
        return newGroup;
    }
    CC_SAFE_DELETE(newGroup);
    return nullptr;
}

/*
 *
 */
UnitGroup * UnitGroup::create()
{
    UnitGroup * newGroup = new UnitGroup();
    globalAutoReleasePool->addObject(newGroup);
    return newGroup;
}

void UnitGroup::recordOrder(ORDER_TYPE orderType)
{
    Vec2 position = getOrderEmblemPosition();
    ENTITY_ID planId = -1;
    if(globalUIController->getCurrentPlan() != nullptr)
    {
        planId = globalUIController->getCurrentPlan()->uID();
    }
    if(_participatingInPlans.find(planId) == _participatingInPlans.end())
    {
        _participatingInPlans.try_emplace(planId);
    }
    
    _participatingInPlans[planId].orderType = orderType;
    _participatingInPlans[planId].position = position;
    for(auto unit : _unitsInGroup)
    {
        _participatingInPlans[planId].unitsIssuedOrder.insert(unit->uID());
    }
}

void UnitGroup::unitInGroupCompletedOrder(Unit * unit, ENTITY_ID planId)
{
    if(_participatingInPlans.find(planId) != _participatingInPlans.end())
    {
        _participatingInPlans[planId].unitsIssuedOrder.erase(unit->uID());
        if(_participatingInPlans[planId].unitsIssuedOrder.size() <= 0)
        {
            _participatingInPlans.erase(planId);
        }
    }
}


Vec2 UnitGroup::getOrderEmblemPosition()
{
    Emblem * emblem = globalEmblemManager->getEmblemForCommandableObjectWithType(uID(), ORDER_EMBLEM);
    if(emblem != nullptr)
    {
        return emblem->getPosition();
    }
    return Vec2::ZERO;
}


void UnitGroup::issueMoveOrder(Vec2 position)
{
    recordOrder(MOVE_ORDER);
    for(Unit * unit : _unitsInGroup)
    {
        globalGameEventController->issueMoveOrder(position,unit, globalUIController->getCurrentPlan());
        position.x -= unit->getDefaultUnitSize().width;
    }
}


void UnitGroup::issueOccupyOrder(Objective * objective)
{
    recordOrder(OCCUPY_ORDER);
    for(Unit * unit : _unitsInGroup)
    {
        globalGameEventController->issueOccupyOrder(objective, unit, globalUIController->getCurrentPlan());
    }
}

void UnitGroup::issueDigTrenchesOrder(Objective * objective)
{
    recordOrder(OBJECTIVE_WORK_ORDER);
    for(Unit * unit : _unitsInGroup)
    {
        globalGameEventController->issueDigTrenchesOrder(objective,unit, globalUIController->getCurrentPlan());
    }
}

void UnitGroup::issueDigBunkersOrder(Objective * objective)
{
    recordOrder(OBJECTIVE_WORK_ORDER);
    for(Unit * unit : _unitsInGroup)
    {
        globalGameEventController->issueDigBunkersOrder(objective,unit, globalUIController->getCurrentPlan());
    }
}

void UnitGroup::issueBuildCommandPostOrder(Objective * objective)
{
    recordOrder(OBJECTIVE_WORK_ORDER);
    for(Unit * unit : _unitsInGroup)
    {
       globalGameEventController->issueBuildCommandPostOrder(objective,unit, globalUIController->getCurrentPlan());
    }
}

void UnitGroup::issueClearAreaOrder(Objective * objective)
{
    
}

void UnitGroup::issueAttackPositionOrder(Objective * objective)
{
    recordOrder(ATTACK_POSITION_ORDER);
    for(Unit * unit : _unitsInGroup)
    {
        globalGameEventController->issueAttackPositionOrder(objective,unit, globalUIController->getCurrentPlan());
    }
}

void UnitGroup::issueStopOrder()
{
    if(globalUIController->getCurrentPlan() != nullptr)
    {
        _participatingInPlans.erase(globalUIController->getCurrentPlan()->uID());
    }
    else
    {
        _participatingInPlans.erase(-1);
    }
    for(Unit * unit : _unitsInGroup)
    {
        globalGameEventController->issueStopOrder(unit, globalUIController->getCurrentPlan());
    }
}

Vec2 UnitGroup::getPosition()
{
    Vec2 position;
    for(Unit * unit : _unitsInGroup)
    {
        position += unit->getAveragePosition();
    }
    position.x /= _unitsInGroup.size();
    position.y /= _unitsInGroup.size();
    return position;
}

bool UnitGroup::containsUnitType(UNIT_TYPE type)
{
    for(Unit * unit : _unitsInGroup)
    {
        // only MG or arty groups
        if(unit->getUnitType() == type)
        {
            return true;
        }
    }
    return false;
}

bool UnitGroup::containsUnit(Unit * unit)
{
    return _unitsInGroup.contains(unit);
}

bool UnitGroup::canBombard()
{
    for(Unit * unit : _unitsInGroup)
    {
        if(unit->canBombard())
        {
            return true;
        }
    }
    return false;
}

bool UnitGroup::positionValidForBombardment(Vec2 position)
{
    for(Unit * unit : _unitsInGroup)
    {
        if(unit->canBombard())
        {
            if(!unit->positionValidForBombardment(position))
            {
                return false;
            }
        }
    }
    return true;

}


bool UnitGroup::isParticipatingInPlan(ENTITY_ID planId)
{
    return _participatingInPlans.find(planId) != _participatingInPlans.end();
}

const IssuedPlanOrder & UnitGroup::getPlanOrder(ENTITY_ID planId)
{
    if(_participatingInPlans.find(planId) == _participatingInPlans.end())
    {
        _participatingInPlans.try_emplace(planId);
    }
    return _participatingInPlans[planId];

}


bool UnitGroup::hasOrder()
{
    for(Unit * unit : _unitsInGroup)
    {
        if(unit->hasOrder())
        {
            return true;
        }
    }
    return false;
}

void UnitGroup::addUnit(Unit * unit)
{
    _unitsInGroup.pushBack(unit);
}

void UnitGroup::removeUnit(Unit * unit)
{
    std::list<ENTITY_ID> plansToRemove;
    for(auto plan : _participatingInPlans)
    {
        plan.second.unitsIssuedOrder.erase(unit->uID());
        if(plan.second.unitsIssuedOrder.size() <= 0)
        {
            plansToRemove.emplace_back(plan.first);
        }
    }
    for(auto removeId : plansToRemove)
    {
        _participatingInPlans.erase(removeId);
    }
    
    _unitsInGroup.eraseObject(unit);
    if(_unitsInGroup.size() == 0)
    {
        removeFromGame();
    }
}

void UnitGroup::removeFromGame()
{
    globalUIController->reportUnitGroupRemoved(this);
}

void UnitGroup::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    size_t count;
    archive(count);
    for(int i = 0; i < count; i++)
    {
        ENTITY_ID thingId;
        archive(thingId);
        Unit * unit = dynamic_cast<Unit *>(globalEntityManager->getEntity(thingId));
        _unitsInGroup.pushBack(unit);
    }
    
    int team;
    archive(team);
    _owningTeam = globalTeamManager->getTeam(team);
}

void UnitGroup::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    archive(_unitsInGroup.size());
    for(auto unit : _unitsInGroup)
    {
        archive(unit->uID());
    }
    
    archive(_owningTeam->getTeamId());
}
