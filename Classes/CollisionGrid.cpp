//
//  TWCollisionGrid.cpp
//  TrenchWars
//
//  Created by Paul Reed on 2/25/12.
//  Copyright (c) 2012 . All rights reserved.
//

#include "CollisionGrid.h"
#include "CollisionCell.h"
#include "EngagementManager.h"
#include "VectorMath.h"
#include "BackgroundTaskHandler.h"
#include "PhysicalEntity.h"
#include "Team.h"
#include <unordered_set>

CollisionGrid::CollisionGrid(Size size, int positionOffset)
{
    _gridSize = size;
    _gridId = gridIds++;
    _gridOffset = positionOffset;
    _worldBounds.origin = Vec2(0 , positionOffset);
    _worldBounds.size.width = size.width;
    _worldBounds.size.height = size.height;
    
    Rect rect(-1,-1,1,1);
    if(theInvalidCell == nullptr)
    {
        theInvalidCell = new CollisionCell(rect, -1, -1);
    }
    
    _collisionGrid = new CollisionCell ** [_gridSize.width];
    for(int x = 0; x < _gridSize.width; x++)
    {
        _collisionGrid[x] = new CollisionCell * [_gridSize.height];
        for(int y = 0; y < _gridSize.height; y++)
        {
            rect.origin.x = (x);
            rect.origin.y = (y) + positionOffset;
            _collisionGrid[x][y] = new CollisionCell(rect, x, y);
        }
    }
}

bool CollisionGrid::iterateCellsInLine(Vec2 start, Vec2 end, const std::function<bool (CollisionCell * cell, float distanceTraveled, bool minorCell)> & iteratorFunction)
{
    CollisionCell * startCell = getCellForCoordinates(start);
    if(startCell == theInvalidCell)
    {
        return false;
    }
    
    end = constrainPosition(end);
    CollisionCell * endCell = getCellForCoordinates(end);
        
    // get the magnitude of x and y
    int xDiff = endCell->getXIndex() - startCell->getXIndex();
    int yDiff = endCell->getYIndex() - startCell->getYIndex();
    
    Vec2 stepChange;
    int steps;
    float inverseStepChange;
    float toBorder;
    
    float * majorAxisStep;
    float * minorAxisStep;
    float * minorAxis;
    float * majorAxis;
    float lastMinorAxis;
    float lastMajorAxis;
    
    Vec2 majorAxisDir;
    Vec2 minorAxisDir;
        
    Vec2 currentPosition = start;
    Vec2 previousPosition = start;

    
    if(abs(end.x - start.x) >= abs(end.y - start.y))
    {
        steps = abs(xDiff);
        stepChange.y = (end.y - start.y) / abs(end.x - start.x);
        stepChange.x = xDiff > 0 ? 1 : -1;
        majorAxisDir = Vec2(stepChange.x, 0);
        minorAxisDir = Vec2(0, stepChange.y > 0 ? 1 : -1);
        
        
        majorAxisStep = &stepChange.x;
        minorAxisStep = &stepChange.y;
        
        majorAxis = &currentPosition.x;
        minorAxis = &currentPosition.y;
    }
    else
    {
        steps = abs(yDiff);
        stepChange.x = (end.x - start.x) / abs(end.y - start.y);
        stepChange.y = yDiff > 0 ? 1 : -1;
        
        majorAxisDir = Vec2(0, stepChange.y);
        minorAxisDir = Vec2(stepChange.x > 0 ? 1 : -1, 0);

        majorAxisStep = &stepChange.y;
        minorAxisStep = &stepChange.x;
        
        majorAxis = &currentPosition.y;
        minorAxis = &currentPosition.x;
    }
    
    toBorder = (*majorAxis - ((int) *majorAxis));
    if(*majorAxisStep > 0)
    {
        toBorder = 1 - toBorder;
    }
    
    if(*minorAxisStep == 0)
    {
        inverseStepChange = 1000;
    }
    else
    {
        inverseStepChange = fabs(1 / *minorAxisStep);
    }
    
    lastMinorAxis = *minorAxis;
    lastMajorAxis = *majorAxis;
 
    float distancePerCell = stepChange.length();
    
    CollisionCell * cell = getCell(currentPosition.x, currentPosition.y);
            
    for(int i = 0; i < steps; i++)
    {
        //////check to see if both axis were incremented for the next cell
        //if so, check an additional cell for collisions: [previousX,nextY] or [nextX,previousY]
        if( ((int)lastMinorAxis) != ((int)*minorAxis))
        {
            float toMinorAxisBorder = lastMinorAxis - ((int)lastMinorAxis);
            if(*minorAxisStep > 0)
            {
                toMinorAxisBorder = 1 - toMinorAxisBorder;
            }
            toMinorAxisBorder *= inverseStepChange;

            Vec2 adjacent = previousPosition;
            if(toMinorAxisBorder < toBorder)
            {
                adjacent += minorAxisDir;
            }
            else
            {
                adjacent += majorAxisDir;
            }
            cell = _collisionGrid[(int) (adjacent.x)][(int)(adjacent.y)];
            if(iteratorFunction(cell, distancePerCell, true) == false)
            {
                return false;
            }
        }
        
        cell = _collisionGrid[(int) currentPosition.x][(int)currentPosition.y];
        if(iteratorFunction(cell,distancePerCell, false) == false)
        {
            return false;
        }
        //increment the position variables
        lastMajorAxis = *majorAxis;
        lastMinorAxis = *minorAxis;
        previousPosition = currentPosition;
        currentPosition += stepChange;
    }
    
    // One last adjacent check
    if( ((int)lastMinorAxis) != ((int)*minorAxis))
    {
        float toMinorAxisBorder = lastMinorAxis - ((int)lastMinorAxis);
        if(*minorAxisStep > 0)
        {
            toMinorAxisBorder = 1 - toMinorAxisBorder;
        }
        toMinorAxisBorder *= inverseStepChange;
        
        Vec2 adjacent = previousPosition;
        if(toMinorAxisBorder < toBorder)
        {
            adjacent += minorAxisDir;
        }
        else
        {
            adjacent += majorAxisDir;
        }
        cell = _collisionGrid[(int) (adjacent.x)][(int)(adjacent.y)];
        if(iteratorFunction(cell, distancePerCell, true) == false)
        {
            return false;
        }
    }
    
    if(iteratorFunction(endCell,distancePerCell, false) == false)
    {
        return false;
    }
    return true;
}



bool CollisionGrid::iterateCellsInCircle(Vec2 center, float radius, const std::function<bool (CollisionCell * cell)> & iteratorFunction)
{
    CollisionCell * cell = getCellForCoordinates( constrainPosition( center));
    CollisionCell * examineCell;
    
    int xCell;
    int yCell;
    
    int xChange;
    int yChange;
    
    int cellRadius = ceil(radius);
    
    // Start with the center
    if(iteratorFunction(cell) == false)
    {
        return false;
    }
    
    //walk in a circle clockwise around the center cell
    // with each loop, increase the radius of the circle
    for(int i = 1; i <= cellRadius; i++)
    {
        xCell = cell->getXIndex() -i;
        yCell = cell->getYIndex() + i;
        int steps = (i*2);
        //loop for each quadrent of the circle
        for(int circleQuadrent = 0; circleQuadrent < 4; circleQuadrent++)
        {
            xChange = 0;
            yChange = 0;
            if(circleQuadrent == 0)
            {
                xChange = 1;
            }
            else if(circleQuadrent == 1)
            {
                yChange = -1;
            }
            else if(circleQuadrent == 2)
            {
                xChange = -1;
            }
            else
            {
                yChange = 1;
                steps--;
            }
            
            //walk across the quadrent
            for(int walk = 0; walk < steps;walk++ )
            {
                examineCell = getCell(xCell,yCell);
                if(examineCell != theInvalidCell && examineCell->circleInCell(center, radius))
                {
                    if(iteratorFunction(examineCell) == false)
                    {
                        return false;
                    }
                }
                xCell += xChange;
                yCell += yChange;
            }
        }
    }
    return true;
}

/*
 *
 *
 */
void CollisionGrid::cellsInLine(Vec2 start, Vec2 end, std::vector<CollisionCell *> * cellArray)
{
    // Get the starting and ending cells of the line
    CollisionCell * startCell = getCellForCoordinates(start);
    CollisionCell * endCell = getCellForCoordinates(end);
    
    if(startCell == nullptr || endCell == nullptr)
        return;
    // get the magnitude of x and y
    double xDiff = end.x - start.x;
    double yDiff = end.y - start.y;
    
    double yChange;
    double xChange;
    int walkCount;
    if(fabs(xDiff) > fabs(yDiff))
    {
        walkCount = ceil(fabs(xDiff));
        xChange = xDiff > 0 ? 1 : -1;
        yChange = (double)yDiff / (double)abs(xDiff);
    } else {
        walkCount = ceil(fabs(yDiff));
        yChange = yDiff > 0 ? 1 : -1;
        xChange = (double)xDiff / (double)abs(yDiff);
    }
    
    double xPosition = start.x;
    double yPosition = (start.y) - _gridOffset;
    
    CollisionCell * cell;
    
    int lastX = xPosition;
    int lastY = yPosition;

    for(int i = 0; i <= walkCount; i++)
    {
        //////check to see if both axis were incremented to the next cell
        //if so, check two additional cells for collisions: [previousX,nextY] and [nextX,previousY]
        if(lastY != (int)yPosition && lastX != (int) xPosition)
        {
            cell = getCell(xPosition,lastY);
            if(cell != theInvalidCell && cell->lineInCell(start, end))
            {
                cellArray->push_back(cell);
            }
            cell = getCell(lastX, yPosition);
            if(cell != theInvalidCell && cell->lineInCell(start, end))
            {
                cellArray->push_back(cell);
            }
        }
        
        cell = getCell(xPosition,yPosition);
        if(cell != theInvalidCell && cell->lineInCell(start, end))
        {
            cellArray->push_back(cell);
        }
        
        lastY = yPosition;
        lastX = xPosition;
        yPosition += yChange;
        xPosition += xChange;
    }
}

/*
 *
 *
 */
void CollisionGrid::cellsInCircle(Vec2 origin, float radius, std::vector<CollisionCell *> * cellArray)
{
    origin = constrainPosition(origin);
    CollisionCell * centerCell = getCellForCoordinates(origin);
    
    float minX = constrainXIndex(centerCell->getXIndex() - radius);
    float maxX = constrainXIndex(centerCell->getXIndex() + radius);
    float minY = constrainYIndex(centerCell->getYIndex() - radius);
    float maxY = constrainYIndex(centerCell->getYIndex() + radius);
    
    for(int i = minY; i <= maxY; i++)
    {
        for(int j = minX; j <= maxX;j++)
        {
            CollisionCell * cell = getCell(j,i);
            if(cell->circleInCell(origin, radius))
            {
                cellArray->push_back(cell);
            }
            else
            {
                if(j > origin.x)
                {
                    break;
                }
            }
        }
    }
}

void CollisionGrid::cellsInRect(Rect rect, std::vector<CollisionCell *> * cellArray)
{
    CollisionCell * minCell = getCellForCoordinates(constrainPosition(rect.origin));


    int minX = minCell->getXIndex();
    int minY = minCell->getYIndex();
    
    int maxX = constrainXIndex(minX + (rect.size.width));
    int maxY = constrainYIndex(minY + (rect.size.height));
    
    
    for(int i = minY; i <= maxY; i++)
    {
        for(int j = minX; j <= maxX;j++)
        {
            cellArray->push_back(_collisionGrid[j][i]);
        }
    }
}

void CollisionGrid::cellsInRect(Vec2 start, Vec2 end, std::vector<CollisionCell *> * cellArray)
{
    Rect rect(start.x , start.y, end.x - start.x, end.y - start.y );
    cellsInRect(rect, cellArray);
}

void CollisionGrid::cellsInPolygon(std::vector<Vec2> & polygon, std::vector<CollisionCell *> * cellArray)
{
    double maxX = -9999999;
    double maxY = -9999999;
    Rect bounds;
    bounds.origin.x = 999999;
    bounds.origin.y = 999999;
    for(Vec2 point : polygon)
    {
        if(point.x < bounds.origin.x) {
            bounds.origin.x = point.x;
        }
        if(point.y < bounds.origin.y) {
            bounds.origin.y = point.y;
        }
        if(point.x > maxX) {
            maxX = point.x;
        }
        if(point.y > maxY) {
            maxY = point.y;
        }
    }
    bounds.size.width = maxX - bounds.origin.x;
    bounds.size.height = maxY - bounds.origin.y;
    
    if(bounds.size.width < 1)
    {
        bounds.size.width = 1;
    }
    
    if(bounds.size.height < 1)
    {
        bounds.size.height = 1;
    }
    
    //// Find the cells along the border of the polygon
    std::vector<CollisionCell *> cellsInLineVector;
    for(int i = 1; i < polygon.size(); i++)
    {
        cellsInLine(polygon[i-1], polygon[i],  &cellsInLineVector);
        for(CollisionCell * cell : cellsInLineVector)
        {
            cellArray->push_back(cell);
        }
    }
    
    std::vector<CollisionCell *> cellsInBounds;
    cellsInRect(bounds, &cellsInBounds);
    
    for(CollisionCell * cell : cellsInBounds)
    {
        Vec2 testPoint = cell->getCellPosition();
        testPoint.x += cell->getCellSize().width / 2;
        testPoint.y += cell->getCellSize().height / 2;
        bool inside = false;
        for(int i = 0, j = polygon.size() - 1; i < polygon.size(); j = i++)
        {
            if( ((polygon[i].y > testPoint.y) != (polygon[j].y > testPoint.y)) &&
               testPoint.x < (polygon[j].x - polygon[i].x) * (testPoint.y - polygon[i].y) / (polygon[j].y- polygon[i].y) + polygon[i].x)
            {
                inside = !inside;
            }
        }
        
        if(inside)
        {
            cellArray->push_back(cell);
        }
    }
}


double CollisionGrid::getCoverAndHeightValueForCell(CollisionCell * cell, CollisionCell * center)
{
    // FIX wtf is this?
    double value = cell->getTerrainCharacteristics().getMovementCost(MOVE_STRONGLY_PREFER_COVER);
    
    double distance = Vec2(cell->getXIndex(), cell->getYIndex()).distance(Vec2(center->getXIndex(), center->getYIndex())) / 2;
    if(distance < 1) {
        distance = 1;
    }
    value /= (distance);
    return value;
}


double CollisionGrid::findCoverValueInRect(Rect area, CollisionCell * center, Team * team)
{
    double sumValue = 0;
    CollisionGrid * grid = world->getCollisionGrid(center->getCellPosition());
    for(int x = area.origin.x; x < area.getMaxX(); x++)
    {
        for(int y = area.origin.y; y < area.getMaxY(); y++)
        {
            CollisionCell * cell = grid->getCell(x, y);
            if(cell != theInvalidCell && !team->isPositionClaimed(cell)) {
                sumValue += getCoverAndHeightValueForCell(cell, center);
            }
        }
    }
    
    return sumValue;
}

bool CollisionGrid::canSeeAroundCoverAtPosition(CollisionCell * cell, DIRECTION directionToLook)
{
    Vec2 direction = VectorMath::unitVectorFromFacing(directionToLook);
    direction.x = ceil(direction.x);
    direction.y = ceil(direction.y);
    
    Vec2 leftDirection = Vec2(-direction.y, direction.x);
    CollisionCell * neighborCell = getCell(cell->getXIndex() + leftDirection.x , cell->getYIndex() + leftDirection.y);
    if(neighborCell != theInvalidCell &&
       (neighborCell->getStaticEntity() == nullptr ||
       neighborCell->getStaticEntity()->getTerrainCharacteristics().getCoverValue() < 100)) //Fix magic number
    {
        return true;
    }
    
    Vec2 rightDirection = Vec2(direction.y, -direction.x);
    neighborCell = getCell(cell->getXIndex() + rightDirection.x , cell->getYIndex() + rightDirection.y);
    if(neighborCell != theInvalidCell &&
       (neighborCell->getStaticEntity() == nullptr ||
        neighborCell->getStaticEntity()->getTerrainCharacteristics().getCoverValue() < 100)) //Fix magic number
    {
        return true;
    }
    return false;
}

/*
 *
 *
 */
CollisionCell * CollisionGrid::getCellForCoordinates(Vec2 coordinates)
{
    return getCell(coordinates.x, coordinates.y - _gridOffset);
}

/**
 *  Valid cells are those that do NOT have an impassible obstacle in them (e.g., a tree)
 *
 */
Vec2 CollisionGrid::getValidMoveCellAroundPosition(Vec2 position, int searchRadius)
{
    Vec2 constrainedPosition = constrainPosition( position);
    CollisionCell * cell = getCellForCoordinates( constrainPosition( position));
    
    // if passed in position is already valid then just return it
    if(cell->isTraversable() && constrainedPosition == position)
        return position;
    CollisionCell * examineCell;
    
    int xCell;
    int yCell;
    
    int xChange;
    int yChange;
    
    //walk in a circle clockwise around the center cell
    // with each loop, increase the radius of the circle
    for(int i = 1; i < searchRadius; i++)
    {
        xCell = cell->getXIndex() -i;
        yCell = cell->getYIndex() + i;
        //loop for each quadrent of the circle
        for(int circleQuadrent = 0; circleQuadrent < 4; circleQuadrent++)
        {
            xChange = 0;
            yChange = 0;
            if(circleQuadrent == 0)
                xChange = 1;
            else if(circleQuadrent == 1)
                yChange = -1;
            else if(circleQuadrent == 2)
                xChange = -1;
            else
                yChange = 1;
            
            //walk across the quadrent
            for(int walk = 0; walk <= (i*2);walk++ )
            {
                xCell += xChange;
                yCell += yChange;
                if(cellIndexValid(xCell, yCell))
                {
                    examineCell = getCell(xCell,yCell);
                    if(examineCell->isTraversable())
                    {
                        return examineCell->getCellPosition();
                    }
                }
            }
        }
    }
    return Vec2(-1,-1);
}

int CollisionGrid::constrainXIndex(int index)
{
    if(index < 0)
    {
        return 0;
    }
    else if(index >= _gridSize.width)
    {
        return _gridSize.width - 1;
    }
    else
    {
        return index;
    }
}


int CollisionGrid::constrainYIndex(int index)
{
    if(index < 0)
    {
        return 0;
    }
    else if(index >= _gridSize.height)
    {
        return _gridSize.height - 1;
    }
    else
    {
        return index;
    }
}

Vec2 CollisionGrid::constrainPosition(Vec2 position)
{
    if(position.x < _worldBounds.origin.x )
    {
        position.x = _worldBounds.origin.x;
    }
    else if(position.x >= (_worldBounds.origin.x + _worldBounds.size.width))
    {
        position.x = _worldBounds.origin.x + _worldBounds.size.width -1;
    }
    
    if(position.y < _worldBounds.origin.y)
    {
        position.y = _worldBounds.origin.y;
    }
    else if(position.y >= (_worldBounds.origin.y + _worldBounds.size.height))
    {
        position.y = _worldBounds.origin.y + _worldBounds.size.height -1;
    }
    return position;
}

bool CollisionGrid::constrainLine(Vec2 & start, Vec2 & end)
{
    Rect rect(0,0,_gridSize.width, _gridSize.height);
    if(!cellIndexValid(start.x, start.y))
    {
        auto intersections = VectorMath::getLineIntersectionsWithRect(start,end, rect);
        if(!cellIndexValid(end.x, end.y))
        {
            if(intersections.size() != 2)
            {
                return false;
            }
            else
            {
                start = intersections.front();
                end = intersections.back();
                return true;
            }
        }
        start = intersections.front();
    }
    else if(!cellIndexValid(end.x, end.y))
    {
        auto intersections = VectorMath::getLineIntersectionsWithRect(start,end, rect);
        end = intersections.back();
    }
    return true;
}


void CollisionGrid::shutdown()
{
    for(int x = 0; x < _gridSize.width; x++)
    {
        for(int y = 0; y < _gridSize.height; y++)
        {
            _collisionGrid[x][y]->shutdown();
        }
        delete [] _collisionGrid[x];
    }
    delete [] _collisionGrid;
    _gridSize.width = 0;
    _gridSize.height = 0;

}
