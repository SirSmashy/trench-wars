//
//  Bunker.cpp
//  TrenchWars
//
//  Created by Paul Reed on 9/5/21.
//

#include "Bunker.h"
#include "EngagementManager.h"
#include "GameVariableStore.h"
#include "VectorMath.h"
#include "NavigationGrid.h"
#include "GameStatistics.h"
#include "EntityFactory.h"
#include "MultithreadedAutoReleasePool.h"
#include "World.h"
#include "CollisionGrid.h"

Bunker::Bunker() : StaticEntity()
{
    
}

bool Bunker::init(StaticEntityDefinition * definition, Vec2 position, Size bunkerSize, DIRECTION entranceFacing)
{
    if(StaticEntity::init(definition, position))
    {
        _bunkerSize = bunkerSize;
        _facing = entranceFacing;
        // If the bunker is facing UP or DOWN then swap the width and height
        if(entranceFacing == UP || entranceFacing == DOWN)
        {
            float swap = _bunkerSize.width;
            _bunkerSize.width = _bunkerSize.height;
            _bunkerSize.height = swap;
        }
        
        Vec2 bunkerOrigin = _position;
        
        switch(_facing)
        {
            case RIGHT:
            {
                bunkerOrigin.y -= (_bunkerSize.height  / 2) - (0.5);
                break;
            }
            case UP: {
                bunkerOrigin.x -= (_bunkerSize.width  / 2) - (0.5);
                break;
            }
            case LEFT: {
                bunkerOrigin.y -= (_bunkerSize.height  / 2) - (0.5);
                bunkerOrigin.x -= _bunkerSize.width - (0.5);
                break;
            }
            case DOWN: {
                bunkerOrigin.x -= (_bunkerSize.width  / 2) - (0.5);
                bunkerOrigin.y -= _bunkerSize.height - (0.5);
                break;
            }
            default: {
                bunkerOrigin.y -= (_bunkerSize.height  / 2) - (0.5);
            }
        }
        
        world->createUndergroundArea(bunkerOrigin, _bunkerSize, getPosition(), this);
        return true;
    }
    return false;
}


bool Bunker::init(Vec2 position, Size bunkerSize, DIRECTION entranceFacing)
{
    StaticEntityDefinition * definition = (StaticEntityDefinition *) globalEntityFactory->getPhysicalEntityDefinitionWithName("Bunker");
    if(definition != nullptr)
    {
        init(definition, position, bunkerSize, entranceFacing);
        return true;
    }
    
    return false;
}

Bunker * Bunker::create(Vec2 position, Size bunkerSize, DIRECTION entranceFacing)
{
    Bunker * newBunker = new Bunker();
    if(newBunker->init(position, bunkerSize, entranceFacing))
    {
        globalAutoReleasePool->addObject(newBunker);
        return newBunker;
    }
    CC_SAFE_DELETE(newBunker);
    return nullptr;
}

Bunker * Bunker::create()
{
    Bunker * newBunker = new Bunker();
    globalAutoReleasePool->addObject(newBunker);
    return newBunker;
}


void Bunker::removeFromGame()
{
    world->destroyUndergroundArea(_undergroundArea->_areaBounds.origin);
    StaticEntity::removeFromGame();
}

void Bunker::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    StaticEntity::loadFromArchive(archive);
    archive(_bunkerSize);
    Vec2 undergroundOrigin;
    archive(undergroundOrigin);
    _undergroundArea = world->getWorldSectionForPosition(undergroundOrigin);
}

void Bunker::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    StaticEntity::saveToArchive(archive);
    archive(_bunkerSize);
    archive(_undergroundArea->_areaBounds.origin);
}
