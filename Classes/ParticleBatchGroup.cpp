//
//  ParticleBatchGroup.cpp
//  TrenchWars
//
//  Created by Paul Reed on 7/23/22.
//

#include "ParticleBatchGroup.h"
#include "World.h"
#include "EngagementManager.h"
#include "CollisionGrid.h"
#include "DynamicParticleSystem.h"

void ParticleBatchGroup::init(const std::string & particleName, Vec2 position)
{
    _position = position * WORLD_TO_GRAPHICS_SIZE;
    _particleName = particleName;
    _currentBatchIndex = 0;
    
    _templateParticle = ParticleSystemQuad::create(_particleName);
    _templateParticle->retain();
    
    Director * director = Director::getInstance();
    Scheduler * scheduler = director->getScheduler();
    scheduler->schedule([=] (float dt) { update(dt); }, this, 10.0, false, "BatchGroup");
}

ParticleBatchGroup * ParticleBatchGroup::create(const std::string & particleName, Vec2 position)
{
    ParticleBatchGroup * group = new ParticleBatchGroup();
    group->init(particleName, position);
    return group;
}

void ParticleBatchGroup::update(float deltaTime)
{
    for(int i = 0; i < _batches.size(); i++)
    {
        if(_batches.at(i)->getTextureAtlas()->getTotalQuads() < 10000)
        {
            _currentBatchIndex = i;
            break;
        }
    }
}

ParticleSystem * ParticleBatchGroup::addParticle(Vec2 position, float height, bool dynamic, bool permanent)
{
    ParticleBatchNode * batch = nullptr;
    while(_currentBatchIndex < _batches.size())
    {
        if(_batches.at(_currentBatchIndex)->getTextureAtlas()->getTotalQuads() < 15000)
        {
            batch = _batches.at(_currentBatchIndex);
            break;
        }
        _currentBatchIndex++;
    }
    
    
    ParticleSystemQuad * particle = copyParticle(_templateParticle, dynamic, permanent);

    if(batch == nullptr)
    {
        batch = ParticleBatchNode::createWithTexture(_templateParticle->getTexture(), 60000);
        batch->setPosition(_position);
        batch->setPositionZ(height); 
        world->addChild(batch, 8000000);
        _batches.pushBack(batch);
    }
    
    particle->setPosition(position * WORLD_TO_GRAPHICS_SIZE);
    particle->setPositionType(ParticleSystem::PositionType::GROUPED);
    
    batch->addChild(particle, 8000000);
    return particle;
}

void ParticleBatchGroup::setVisible(bool visible)
{
    for(auto batch : _batches)
    {
        batch->setVisible(visible);
    }
}
void ;

void ParticleBatchGroup::setPositionOffset(Vec2 position)
{
    for(auto batch : _batches)
    {
        batch->setPosition(position);
    }

}


ParticleSystemQuad * ParticleBatchGroup::copyParticle(ParticleSystemQuad * particleToCopy, bool dynamic, bool permanent)
{
    ParticleSystemQuad * particle;
    if(permanent)
    {
        particle = PermanentParticleSystem::createWithTotalParticles(particleToCopy->getTotalParticles());
    }
    else if(dynamic)
    {
        particle = DynamicParticleSystem::createWithTotalParticles(particleToCopy->getTotalParticles());
    }
    else
    {
        particle = ParticleSystemQuad::createWithTotalParticles(particleToCopy->getTotalParticles());
    }
    
    particle->setTotalParticles(particleToCopy->getTotalParticles());
    particle->setAngle(particleToCopy->getAngle());
    particle->setAngleVar(particleToCopy->getAngleVar());
    particle->setDuration(particleToCopy->getDuration());
    
    BlendFunc blendFunc;
    blendFunc.src = particleToCopy->getBlendFunc().src;
    blendFunc.dst = particleToCopy->getBlendFunc().dst;
    blendFunc.dst = particleToCopy->getBlendFunc().dst;
    particle->setBlendFunc(blendFunc);
    
    //color
    particle->setStartColor(particleToCopy->getStartColor());
    particle->setStartColorVar(particleToCopy->getStartColorVar());
    particle->setEndColor(particleToCopy->getEndColor());
    particle->setEndColorVar(particleToCopy->getEndColorVar());
    
    // particle size
    particle->setStartSize(particleToCopy->getStartSize());
    particle->setStartSizeVar(particleToCopy->getStartSizeVar());
    particle->setEndSize(particleToCopy->getEndSize());
    particle->setEndSizeVar(particleToCopy->getEndSizeVar());
    
    particle->setSourcePosition(particleToCopy->getSourcePosition());
    particle->setPosVar(particleToCopy->getPosVar());
    
    particle->setStartSpin(particleToCopy->getStartSpin());
    particle->setStartSpinVar(particleToCopy->getStartSpinVar());
    
    particle->setEndSpin(particleToCopy->getEndSpin());
    particle->setEndSpinVar(particleToCopy->getEndSpinVar());
    
    particle->setEmitterMode(particleToCopy->getEmitterMode());
    
    if(particle->getEmitterMode() == cocos2d::ParticleSystem::Mode::GRAVITY)
    {
        particle->setGravity(particleToCopy->getGravity());
        particle->setSpeed(particleToCopy->getSpeed());
        particle->setSpeedVar(particleToCopy->getSpeedVar());
        
        particle->setRadialAccel(particleToCopy->getRadialAccel());
        particle->setRadialAccelVar(particleToCopy->getRadialAccelVar());
        
        particle->setTangentialAccel(particleToCopy->getTangentialAccel());
        particle->setTangentialAccelVar(particleToCopy->getTangentialAccelVar());
        particle->setRotationIsDir(particleToCopy->getRotationIsDir());
    }
    else if(particle->getEmitterMode() == cocos2d::ParticleSystem::Mode::RADIUS)
    {
        particle->setStartRadius(particleToCopy->getStartRadius());
        particle->setStartRadiusVar(particleToCopy->getStartRadiusVar());
        
        particle->setEndRadius(particleToCopy->getEndRadius());
        particle->setEndRadiusVar(particleToCopy->getEndRadiusVar());
        
        particle->setRotatePerSecond(particleToCopy->getRotatePerSecond());
        particle->setRotatePerSecondVar(particleToCopy->getRotatePerSecondVar());
    }
    
    particle->setLife(particleToCopy->getLife());
    particle->setLifeVar(particleToCopy->getLifeVar());
    
    particle->setEmissionRate(particleToCopy->getEmissionRate());
    particle->setTexture(particleToCopy->getTexture());
    
    
    if(permanent)
    {
        particle->setDuration(MAXFLOAT);
        particle->setLife(MAXFLOAT);
        particle->setEmissionRate(10000000);
    }
    else
    {
        particle->setAutoRemoveOnFinish(true);
        if(particleToCopy->getDuration() == -1)
        {
            particle->setEmissionRate(10000000);
            particle->setDuration(0.01);
            particle->setAutoRemoveOnFinish(true);
        }
    }
    
    return particle;
}
