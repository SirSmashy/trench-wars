//
//  Entity.hpp
//  TrenchWars
//
//  Created by Paul Reed on 9/2/20.
//

#ifndef Entity_h
#define Entity_h

#include "InteractiveObject.h"
#include "EntityDefinitions.h"
#include "SerializationHelpers.h"
#include <cereal/archives/binary.hpp>

USING_NS_CC;

class GameMenu;


/*
 * Entities are independent "objects" in the Trench Wars world
 * Entities include: infantry (actors), groups of infantry (units), objectives, projectiles, smoke clouds, etc...
 * Entities may have a physical presence in the world (such as infantry), or be abstract things (such as an objective area)
 * Entities may "query", and "act"
 */
class Entity : public InteractiveObject
{
public:
    virtual ~Entity();
    Entity();
    
    virtual INTERACTIVE_OBJECT_TYPE getInteractiveObjectType() {return ENTITY_INTERACTIVE_OBJECT;}

    
    virtual void init();
    
    /*
     * Perform functions that involve the entity querying the world and deciding what to do
     * All entities query concurrently, so functions that occur during querying MUST be thread safe
     * Note that cocos-2dx is single threaded, so interacting with the cocos2d engine must NOT occur during querying
     * This includes changing position, changing animations, creating entities or particles, etc.
     * If an entity decides to do one of the above things while querying, then the addAction function should be called
     */
    virtual void query(float deltaTime) {};
    
    /*
     * Perform functions that involve the moving entity and updating the collision grid
     * ALL updates to the collision grid MUST occur here.
     * Note that moving is NOT thread safe, so move functions should only touch the data of the moving entity
     */
    virtual void act(float deltaTime) {};
    
    virtual bool shouldQuery() {return false;}
    virtual bool shouldAct() {return false;}
        
    /*
     * Get the type of this game object
     */
    virtual ENTITY_TYPE getEntityType() const {return OTHER;}

    /*
     * Add debug info to an GameMenu object
     */
    virtual void populateDebugPanel(GameMenu * menu, Vec2 location) {};
    
    /*
     *
     */
    virtual void removeFromGame();
    virtual void postLoad() {};
    
    virtual void setDebugMe() {};
    virtual void clearDebugMe() {};
    virtual bool isDebugMe() {return false;}
    
    virtual size_t getHashedDefinitionName() {return -1;}
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive)
    {
        ComparableRef::loadFromArchive(archive);
    };
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const
    {
        ComparableRef::saveToArchive(archive);
    };
    
    virtual bool isNetworkEntity() {return true;}

    
    virtual int hasUpdateToSendOverNetwork() {return 0;}
    /*
     * Store variables that should be transmitted across the network from the server to clients
     * These are generally variables that are modified while querying, and consumed while acting
     */
    virtual void saveNetworkUpdate(cereal::BinaryOutputArchive & archive) {};
    virtual void updateFromNetwork(cereal::BinaryInputArchive & archive) {};
};


#endif /* Entity_h */



