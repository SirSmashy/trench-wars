//
//  GameMenu.cpp
//  TrenchWars
//
//  Created by Paul Reed on 11/18/22.
//

#include "GameMenu.h"
#include "MenuLayer.h"
#include "PlayerLayer.h"
#include "World.h"
#include "CollisionGrid.h"


GameMenu::GameMenu() : AutoSizedLayout()
{
}



bool GameMenu::init(GameMenu * parent)
{
    _keepMenuAtGamePosition = false;
    _parent = parent;
    AutoSizedLayout::init();
    LinearLayoutParameter * params = LinearLayoutParameter::create();
    params->setMargin(Margin(3, 1, 0, 0));
    
    setLayoutParameter(params);
    setLayoutType(Layout::Type::VERTICAL);
    globalMenuLayer->addChild(this, 0); //9999
    setBackGroundColor(Color3B::GRAY);
    setBackGroundColorType(BackGroundColorType::SOLID);
    setBackGroundColorOpacity(255);
    setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    
    Director * director = Director::getInstance();
    Scheduler * scheduler = director->getScheduler();
    scheduler->schedule([=] (float dt) { update(dt); }, this, 0.0333333333, false, "GameMenu");

    
    return true;
}


GameMenu * GameMenu::create(GameMenu * parent)
{
    GameMenu * newMenu = new GameMenu();
    if(newMenu->init(parent))
    {
        newMenu->autorelease();
        return newMenu;
    }
    
    CC_SAFE_DELETE(newMenu);
    return nullptr;
}

void GameMenu::update(float deltaTime)
{
    if(_keepMenuAtGamePosition)
    {
        Vec2 screenLocation = globalPlayerLayer->convertToWorldSpace(world->getAboveGroundPosition(_gamePosition) * WORLD_TO_GRAPHICS_SIZE);
        setPosition(screenLocation);
    }
}

void GameMenu::setPosition(const Vec2 &position)
{
    Widget::setPosition(position);
    for(int i = 0; i < _childMenus.size();i++)
    {
        auto childMenu = _childMenus.at(i);
        Vec2 childPosition;
        childPosition.y = getPosition().y + _childMenuButtons.at(i)->getTopBoundary() - getContentSize().height;
        childPosition.x = getRightBoundary();
        childMenu->setPosition(childPosition);
    }
}

void GameMenu::setPositionInGameWorld(Vec2 position)
{
    _gamePosition = position;
}


void GameMenu::setKeepMenuAtGamePosition(bool keep)
{
    _keepMenuAtGamePosition = keep;
}

void GameMenu::setVisible(bool visible)
{
    if(visible == false)
    {
        for(auto child : _childMenus)
        {
            child->setVisible(visible);
        }
    }

    AutoSizedLayout::setVisible(visible);
}

GameMenu * GameMenu::addChildMenu(std::string name, const std::function<void (GameMenu *)> & menuCreationFunction)
{
    Button * button = Button::create();
    button->setPressedActionEnabled(true);
    button->setTitleText(name + ">");
    button->setTitleFontName("arial.ttf");
    button->setTag(1);
    double fontSize = 20; //FIXME: make adjustable
    button->setTitleFontSize(fontSize);
    _widgets.pushBack(button);
    addChild(button);
    doLayout();
    
    GameMenu * menu = GameMenu::create(this);
    button->addClickEventListener([this, button, menu, menuCreationFunction] (Ref * sender) {
        closeSubMenus();
        menuCreationFunction(menu);
    });
    
    _childMenuButtons.pushBack(button);
    Vec2 position;
    position.y = getPosition().y + button->getTopBoundary() - getContentSize().height;
    position.x = getRightBoundary();
    menu->setPosition(position);
    _childMenus.pushBack(menu);

    return menu;
}

void GameMenu::addButton(std::string name, const AbstractCheckButton::ccWidgetClickCallback & callback, MENU_BUTTON_ACTION menuAction)
{
    Button * button = Button::create();
    button->setPressedActionEnabled(true);
    button->setTitleText(name);
    button->setTitleFontName("arial.ttf");
    button->setTag(1);
    button->setTitleFontSize(20); //FIXME: make adjustable
    
    button->addClickEventListener( [this, callback, menuAction] (Ref* sender) {
        callback(sender);
        if(menuAction == MENU_BUTTON_CLOSE_MENU)
        {
            getRootMenu()->clear();
        }
        else if(menuAction == MENU_BUTTON_CLOSE_SUB_MENU)
        {
            if(_parent != nullptr)
            {
                _parent->closeSubMenus();
            }
        }
    });
    
    _widgets.pushBack(button);
    addChild(button);
    doLayout();
}

void GameMenu::addSeperater()
{
    Layout * layout = Layout::create();
    layout->setBackGroundColor(Color3B::WHITE);
    layout->setBackGroundColorType(BackGroundColorType::SOLID);
    layout->setTag(2);

    layout->setContentSize(Size(getContentSize().width,2));
    _widgets.pushBack(layout);
    addChild(layout);
    doLayout();
}

void GameMenu::addText(std::string textString)
{
    double fontSize = 20; //FIXME: make adjustable
    Text * text = Text::create(textString, "arial.ttf", fontSize);
    text->setTag(3);
    addChild(text);
    _widgets.pushBack(text);
    doLayout();
}

void GameMenu::closeSubMenus()
{
    for(auto menu : _childMenus)
    {
        menu->clear();
    }
}

GameMenu * GameMenu::getRootMenu()
{
    if(_parent == nullptr)
    {
        return this;
    }
    return _parent->getRootMenu();
}

void GameMenu::doLayout()
{
    AutoSizedLayout::doLayout();
    float fullWidth = getContentSize().width;
    for(auto widget : _widgets)
    {
        if(widget->getTag() == 1)
        {
            Button * button = (Button *) widget;
            Size size = button->getTitleLabel()->getContentSize();
            size.width = fullWidth;
            button->getTitleLabel()->setContentSize(size);
        }
        Size size = widget->getContentSize();
        size.width = fullWidth;
        widget->setContentSize(size);
    }
}

void GameMenu::clear()
{
    for(auto menu : _childMenus)
    {
        menu->remove();
    }
    
    _childMenus.clear();
    _widgets.clear();
    removeAllChildren();
    doLayout();
}

void GameMenu::remove()
{
    clear();
    globalMenuLayer->removeChild(this);
    Director * director = Director::getInstance();
    Scheduler * scheduler = director->getScheduler();
    scheduler->unschedule("GameMenu", this);
}
