//
//  LandscapeObjectiveBuilder.h
//  TrenchWars
//
//  Created by Paul Reed on 7/2/21.
//

#ifndef LandscapeObjectiveTracker_h
#define LandscapeObjectiveTracker_h

#include "CellHighlighter.h"
#include "Emblem.h"
#include "cocos2d.h"
#include "Refs.h"

class CollisionCell;
class Command;
class CircularMenu;
class Objective;

class LandscapeObjectiveBuilder : public ComparableRef
{
    std::vector<CollisionCell *> _cells;
    std::unordered_map<int, CollisionCell *> _cellsMap;
    CellHighlighter * _cellHighlighter;
    
    bool _locked;
    bool init();
    void addCellRecursive(CollisionCell * cell);
public:
    
    static LandscapeObjectiveBuilder * create();
    void update(Vec2 mousePosition);
    void lock() {_locked = true;}
    void unlock() {_locked = false;}
    bool isLocked() {return _locked;}

    void clear();
    
    // Entity functions
    virtual bool pointInObjective(Vec2 point);
    virtual std::vector<CollisionCell *> & getCells() {return _cells;}
    
    Objective * buildLandscapeObjective();

};


extern LandscapeObjectiveBuilder * globalLandscapeObjectiveBuilder;

#endif /* LandscapeObjectiveTracker_h */
