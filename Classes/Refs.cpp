//
//  Refs.cpp
//  TrenchWars
//
//  Created by Paul Reed on 4/6/24.
//

#include "Refs.h"


std::atomic_llong nextId = 0;


ENTITY_ID ComparableRef::fetchAddId()
{
    return nextId.fetch_add(1);
};
    
ComparableRef::ComparableRef()
{
    _id = fetchAddId();
}
    
ENTITY_ID ComparableRef::getMaxId()
{
    return nextId;
}
    
    
void ComparableRef::setNextId(ENTITY_ID next)
{
    nextId = next;
}
    
bool ComparableRef::operator==(const ComparableRef & other) const
{
    return uID() == other.uID();
}

bool ComparableRef::operator==(const ComparableRef * other) const
{
    return uID() == other->uID();
}

void ComparableRef::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    archive( _id );
}
void ComparableRef::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    archive( _id );
}

