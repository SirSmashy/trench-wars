//
//  DoubleBuffer.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/12/24.
//

#ifndef DoubleBuffer_h
#define DoubleBuffer_h

template<class T>
class DoubleBuffer
{
    T bufferA;
    T bufferB;
    
    bool _bufferAIsWrite;
    
public:
    
    DoubleBuffer() :
    _bufferAIsWrite(true)
    {
    }
    
    T & getWriteBuffer()
    {
        return _bufferAIsWrite ? bufferA : bufferB;
    }
    
    T & getReadBuffer()
    {
        return _bufferAIsWrite ? bufferB : bufferA;
    }
    
    void switchBuffers()
    {
        _bufferAIsWrite = !_bufferAIsWrite;
    }
};

#endif /* DoubleBuffer_h */
