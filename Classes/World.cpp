//
//  World.cpp
//  TrenchWars
//
//  Created by Paul Reed on 11/14/21.
//

#include "World.h"
#include "StaticEntity.h"
#include "CollisionGrid.h"
#include "CollisionCell.h"
#include "OverlayDrawer.h"
#include "EngagementManager.h"
#include "EngagementScene.h"
#include "Bunker.h"
#include "MapLoader.h"
#include "PolygonLandscapeObject.h"
#include "EntityFactory.h"
#include "MultithreadedAutoReleasePool.h"
#include "SerializationHelpers.h"

World * world;

void WorldSection::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    archive(_areaBounds);
    archive(_offset);
    archive(_underground);
    archive(_areaDestroyed);
    
    size_t transitionSize;
    archive(transitionSize);
    for(int i = 0; i < transitionSize; i++)
    {
        ENTITY_ID areaId;
        Vec2 start, end;
        archive(areaId);
        archive(start);
        archive(end);
        _transitionsToOtherGrids.emplace(areaId, Transition(start,end));
    }
    
    Size gridIndicesSize;
    archive(gridIndicesSize);
    Size gridSize = gridIndicesSize;
    _collisionGrid = new CollisionGrid(gridSize, _areaBounds.origin.y);
    std::vector<std::vector<size_t>> mapCells;
    size_t hashedName;
    mapCells.resize(gridIndicesSize.width);
    for(int x = 0; x < gridIndicesSize.width; x++)
    {
        mapCells[x].resize(gridIndicesSize.height);
        
        for(int y = 0; y < gridIndicesSize.height; y++)
        {
            archive(hashedName);
            mapCells[x][y] = hashedName;
        }
    }
    
    Vec2 visualPosition = _areaBounds.origin + _offset;    
    MapLoader mapLoader(mapCells, _areaBounds.origin, visualPosition);
    mapLoader.createMapAreas();
    mapLoader.createPolygonLandscapeObjects();
    mapLoader.createNavigationPatches();
    _navGrid = new NavigationGrid(gridSize, _collisionGrid, mapLoader.getNavigationRects());
}


void WorldSection::saveToArchive(cereal::BinaryOutputArchive & archive)
{
    archive(_areaBounds);
    archive(_offset);
    archive(_underground);
    archive(_areaDestroyed);
    archive(_transitionsToOtherGrids.size());
    for(auto transition : _transitionsToOtherGrids)
    {
        archive(transition.first);
        archive(transition.second._start);
        archive(transition.second._end);
    }
    Size gridSize = _collisionGrid->getGridSize();
    archive(gridSize);
    for(int x = 0; x < gridSize.width; x++)
    {
        for(int y = 0; y < gridSize.height; y++)
        {
            Color4B cellColor(0,0,0,0);
            CollisionCell * cell = _collisionGrid->getCell(x, y);
            if(cell->getStaticEntity() != nullptr)
            {
                archive(cell->getStaticEntity()->getHashedDefinitionName());
            }
            else if(cell->getLandscapeObject() != nullptr)
            {
                archive(cell->getLandscapeObject()->getHashedDefinitionName());
            }
            else
            {
                archive((size_t) 0);
            }
        }
    }
}

PolygonLandscapeObject * WorldSection::getLandscapeObjectAtPosition()
{
    
}


World::World()
{
    
}

// on "init" you need to initialize your instance
bool World::init(Image * map, EngagementScene * scene, bool createStaticEnts)
{
    MicroSecondSpeedTimer timer( [=](long long int time) {
        LOG_INFO("  Create World Total : %.2f \n", time / 1000000.0);
    });
    world = this;
    world->retain();
        
    _worldSize.width = map->getWidth();
    _worldSize.height = map->getHeight();
    
    _undergroundVisible = false;
    _belowgroundAreaOffset = _worldSize.height * 4;
    
    _backgroundMask = OverlayDrawer::create(Color4F(.2,.2,.2,.7));
    _backgroundMask->setVisible(false);
    _backgroundMask->retain();
    
    _worldLayer = WorldLayer::create(); //todo FIX replace this with a Node object later
    _worldLayer->retain();
    _worldLayer->setContentSize(_worldSize * WORLD_TO_GRAPHICS_SIZE);
    scene->addChild(_worldLayer);
    globalEntityFactory->createSpriteBatches();

    // Create the above ground world area
    WorldSection * aboveGround = createWorldSection(Vec2(0,0), _worldSize);
    aboveGround->_underground = false;
    
    MapLoader mapLoader(map, createStaticEnts);
    mapLoader.createMapAreas();
    mapLoader.createPolygonLandscapeObjects();
    mapLoader.createNavigationPatches();
    _worldSections.at(0)->_navGrid = new NavigationGrid(_worldSize, _worldSections.at(0)->_collisionGrid, mapLoader.getNavigationRects());
    
    timer.stop();
    map->release();
    return true;
}


bool World::init(cereal::BinaryInputArchive & inputArchive, EngagementScene * scene)
{
    world = this;
    world->retain();
    
    inputArchive(_worldSize);
    
    _backgroundMask = OverlayDrawer::create(Color4F(.2,.2,.2,.7));
    _backgroundMask->setVisible(false);
    _undergroundVisible = false;
    _belowgroundAreaOffset = _worldSize.height * 4;
    
    _worldLayer = WorldLayer::create();
    _worldLayer->retain();
    _worldLayer->setContentSize(_worldSize * WORLD_TO_GRAPHICS_SIZE);
    scene->addChild(_worldLayer);
    globalEntityFactory->createSpriteBatches();

    
    size_t size;
    inputArchive(size);
    for(int i = 0; i < size; i++)
    {
        WorldSection * section = new WorldSection();
        _worldSections.pushBack(section);
        section->loadFromArchive(inputArchive);
    }
    return true;
}


World * World::create(Image * map, EngagementScene * scene, bool createStaticEnts)
{
    World * newWorld = new World();
    if(newWorld->init(map, scene, createStaticEnts))
    {
        globalAutoReleasePool->addObject(newWorld);
        return newWorld;
    }
    newWorld->release();
    return nullptr;
}

World * World::create(cereal::BinaryInputArchive & inputArchive, EngagementScene * scene)
{
    World * newWorld = new World();
    if(newWorld->init(inputArchive, scene))
    {
        globalAutoReleasePool->addObject(newWorld);
        return newWorld;
    }
    newWorld->release();
    return nullptr;
}

WorldSection * World::createWorldSection(Vec2 origin, Size areaSize)
{
    int areaOffset = _belowgroundAreaOffset * _worldSections.size();

    WorldSection * newArea = new WorldSection();
    newArea->_offset = Vec2(origin.x, origin.y -areaOffset);
    newArea->_collisionGrid = new CollisionGrid(areaSize, areaOffset);
    newArea->_areaBounds.setRect(0, areaOffset, areaSize.width, areaSize.height);
    newArea->_areaDestroyed = false;
    
    _worldSections.pushBack(newArea);
    return newArea;
}


void World::createUndergroundArea(Vec2 origin, Size areaSize, Vec2 entranceToArea, Bunker * entrance)
{
    globalEngagementManager->requestMainThreadFunction([this, origin, areaSize, entranceToArea, entrance] () {
        WorldSection * newArea = createWorldSection(origin, areaSize);
        newArea->_navGrid = new NavigationGrid(areaSize, newArea->_collisionGrid);
        
        Vec2 exitToSurfaace = (entranceToArea - origin) + newArea->_areaBounds.origin;
        newArea->_underground = true;
                
        WorldSection * aboveGround = _worldSections.at(0);
        aboveGround->_transitionsToOtherGrids.emplace(newArea->uID(), Transition(entranceToArea, exitToSurfaace));
        newArea->_transitionsToOtherGrids.emplace(aboveGround->uID(), Transition(exitToSurfaace, entranceToArea));
        
        // Set the terrain of the cells at the transition to/from the underground area as unique
        CollisionCell * exitToSurfaceCell = newArea->_collisionGrid->getCellForCoordinates(exitToSurfaace);
        CollisionCell * entranceToAreaCell = aboveGround->_collisionGrid->getCellForCoordinates(entranceToArea);
        TerrainCharacteristics transitionTerrain(0,0,true, true);
        
        StaticEntity * exit = StaticEntity::create("Bunker", newArea->_transitionsToOtherGrids.at(0)._start);
        exit->getSprite()->setPositionOffset(newArea->_offset);
        exit->getSprite()->setVisible(_undergroundVisible);
        std::unordered_set<Vec2, Vec2Hash, Vec2Compare> cellIndicesInArea;
        Size gridSize = areaSize;

        for(int y = 0; y < gridSize.height; y++)
        {
            for(int x = 0; x < gridSize.width; x++)
            {
                cellIndicesInArea.emplace(Vec2(x,y));
            }
        }
        Rect areaBounds(0,0, gridSize.width -1, gridSize.height -1);
        Vec2 polygonVisualPosition = newArea->_areaBounds.origin + newArea->_offset;
        
        // TODO fix Shouldn't hardcode this
        PolygonLandscapeObject * landscape = PolygonLandscapeObject::create("dirt", newArea->_areaBounds.origin, polygonVisualPosition, Vec2::ZERO, areaBounds, cellIndicesInArea);
        landscape->setVisible(_undergroundVisible);
        
        if(entrance != nullptr)
        {
            entrance->setUndergroundArea(newArea);
        }
        
        _worldSectionRequests.emplace_back(origin, areaSize, entranceToArea);
    });
}

void World::addWorldSection(WorldSection * section)
{
    _worldSections.pushBack(section);
}


void World::destroyUndergroundArea(Vec2 positionInArea)
{
    globalEngagementManager->requestMainThreadFunction([this, positionInArea] () {
        WorldSection * section = getWorldSectionForPosition(positionInArea);
        assert(section->uID() != 0);
        
        _worldSectionsRemoved.push_back(positionInArea);
        
        Vector<PhysicalEntity *> entsToRemove;
        for(auto entPair : section->_physicalEntities)
        {
            entsToRemove.pushBack(entPair.second);
        }
        section->_physicalEntities.clear();
        
        for(auto ent : entsToRemove)
        {
            ent->removeFromGame();
        }
        
        section->_areaDestroyed = true;
        section->_collisionGrid->shutdown();
        section->_navGrid->shutdown();
        section->_particleBatches.clear();
        section->_permanentParticleBatches.clear();
    });
}


WorldSection * World::getWorldSectionForPosition(Vec2 position)
{
    int areaIndex = round(position.y / _belowgroundAreaOffset);
    if(areaIndex >= _worldSections.size() || areaIndex < 0)
    {
        return _worldSections.at(0);
    }
    
    return _worldSections.at(areaIndex);
}


CollisionGrid * World::getCollisionGrid(Vec2 position)
{
    return getWorldSectionForPosition(position)->_collisionGrid;
}

NavigationGrid * World::getNavGrid(Vec2 position)
{
    return getWorldSectionForPosition(position)->_navGrid;
}

NavigationGrid * World::getNavGridForIndex(int sectionIndex)
{
    if(sectionIndex < 0 || sectionIndex >= _worldSections.size())
    {
        return nullptr;
    }
    return _worldSections.at(sectionIndex)->_navGrid;
}

void World::addChild(Node * node, int zOrder)
{
    _worldLayer->addChild(node, zOrder);
}

void World::removeChild(Node * node)
{
    _worldLayer->removeChild(node);
}

void World::toggleShowUnderground()
{
    _undergroundVisible = !_undergroundVisible;
   _backgroundMask->setVisible(_undergroundVisible);
    for(int i = 1; i < _worldSections.size(); i++)
    {
        if(!_worldSections.at(i)->_areaDestroyed)
        {
            for(auto pair : _worldSections.at(i)->_physicalEntities)
            {
                pair.second->getSprite()->setVisible(_undergroundVisible);
            }
            for(auto pair : _worldSections.at(i)->_landscapeEntities)
            {
                pair.second->getSprite()->setVisible(_undergroundVisible);
            }
            for(auto pair : _worldSections.at(i)->_particleBatches)
            {
                pair.second->setVisible(_undergroundVisible);
            }
            for(auto pair : _worldSections.at(i)->_permanentParticleBatches)
            {
                pair.second->setVisible(_undergroundVisible);
            }
        }
    }
}

void World::addPhysicalEntity(PhysicalEntity * entity)
{
    entitieseMapMutex.lock();
    WorldSection * section = getWorldSectionForPosition(entity->getPosition());
    WorldSection * section1 = _worldSections.at(0);

    if(section->_areaDestroyed)
    {
        entitieseMapMutex.unlock();
        return;
    }
    section->_physicalEntities.emplace(entity->uID(), entity);
    if(section->_underground)
    {
        entity->getSprite()->setVisible(_undergroundVisible);
        entity->getSprite()->setPositionOffset(section->_offset);
        entity->getSprite()->setPositionZOffset(Z_POSITION_UNDERGROUND + .1);
    }
    entitieseMapMutex.unlock();

}

void World::removePhysicalEntity(PhysicalEntity * entity)
{
    entitieseMapMutex.lock();
    WorldSection * section = getWorldSectionForPosition(entity->getPosition());
    if(section->_areaDestroyed)
    {
        entitieseMapMutex.unlock();
        return;
    }
    section->_physicalEntities.erase(entity->uID());
    entitieseMapMutex.unlock();
}

void World::addLandscapeObject(PolygonLandscapeObject * object)
{
    entitieseMapMutex.lock();
    _landscapeEntities.insert(object->uID(), object);
    WorldSection * section = getWorldSectionForPosition(object->getWorldOrigin());

    if(section->_areaDestroyed)
    {
        entitieseMapMutex.unlock();
        return;
    }
    section->_landscapeEntities.insert(object->uID(), object);
    if(section->_underground)
    {
        object->setVisible(_undergroundVisible);
        object->setPositionZ(Z_POSITION_UNDERGROUND);
    }
    entitieseMapMutex.unlock();
}

PolygonLandscapeObject * World::getLandscapeObject(ENTITY_ID objectId)
{
    std::lock_guard<std::mutex> lock(entitieseMapMutex);
    return _landscapeEntities.at(objectId);
}

void World::removeLandscapeObject(PolygonLandscapeObject * object)
{
    entitieseMapMutex.lock();
    _landscapeEntities.erase(object->uID());

    WorldSection * section = getWorldSectionForPosition(object->getWorldOrigin());
    if(section->_areaDestroyed)
    {
        entitieseMapMutex.unlock();
        return;
    }
    section->_landscapeEntities.erase(object->uID());
    entitieseMapMutex.unlock();
}


void World::addAtmosphericObject(Cloud * entity)
{
    entitieseMapMutex.lock();
    WorldSection * section = getWorldSectionForPosition(entity->getPosition());
    WorldSection * section1 = _worldSections.at(0);
    
    if(section->_areaDestroyed)
    {
        entitieseMapMutex.unlock();
        return;
    }
    section->_atmosphericEntities.emplace(entity->uID(), entity);
//    if(section->_underground)
//    {
//        entity->getSprite()->setVisible(_undergroundVisible);
//        entity->getSprite()->setPositionOffset(section->_offset);
//        entity->getSprite()->setPositionZOffset(Z_POSITION_UNDERGROUND + .1);
//    }
    entitieseMapMutex.unlock();
}

PolygonLandscapeObject * World::getAtmosphericObject(ENTITY_ID objectId)
{
    std::lock_guard<std::mutex> lock(entitieseMapMutex);
    return _landscapeEntities.at(objectId);
}

void World::removeAtmosphericObject(Cloud * entity)
{
    entitieseMapMutex.lock();
    WorldSection * section = getWorldSectionForPosition(entity->getPosition());
    if(section->_areaDestroyed)
    {
        entitieseMapMutex.unlock();
        return;
    }
    section->_atmosphericEntities.erase(entity->uID());
    entitieseMapMutex.unlock();
}


Vec2 World::getWorldPosition(Vec2 visualPosition)
{
    // The underground is not being displayed,
    if(!_undergroundVisible)
    {
        return visualPosition;
    }
    
    for(int i = 1; i < _worldSections.size(); i++)
    {
        Vec2 offsetPosition = visualPosition - _worldSections.at(i)->_offset;
        if(_worldSections.at(i)->_areaBounds.containsPoint(offsetPosition))
        {
            return offsetPosition;
        }
    }
    return visualPosition;
}

Vec2 World::getAboveGroundPosition(Vec2 worldPosition)
{
    // The underground is not being displayed,
    if(!_undergroundVisible)
    {
        return worldPosition;
    }
    
    WorldSection * section = getWorldSectionForPosition(worldPosition);
    return worldPosition + section->_offset;
}


bool World::isPositionUnderground(Vec2 position)
{
    return position.y > _belowgroundAreaOffset;
}

bool World::arePositionsInSameArea(Vec2 firstPosition, Vec2 secondPosition)
{
    WorldSection * firstArea = getWorldSectionForPosition(firstPosition);
    WorldSection * secondArea = getWorldSectionForPosition(secondPosition);
    return firstArea == secondArea;
}

bool World::isBunkerEntranceNearPosition(Vec2 position, float distance)
{
    for(auto transition : _worldSections.at(0)->_transitionsToOtherGrids)
    {
        if(transition.second._start.distance(position) <= distance)
        {
            return true;
        }
    }
    return false;
}


void World::transitionEntityToNewArea(Vec2 fromPosition, Vec2 toPosition, MovingPhysicalEntity * entity)
{
    globalEngagementManager->requestMainThreadFunction([this, fromPosition, toPosition, entity] () {
        WorldSection * oldSection = getWorldSectionForPosition(fromPosition);
        oldSection->_physicalEntities.erase(entity->uID());
        
        WorldSection * newSection = getWorldSectionForPosition(toPosition);
        newSection->_physicalEntities.emplace(entity->uID(), entity);
        entity->getSprite()->setPositionOffset(newSection->_offset);
        entity->updateCollision();
        if(newSection->_underground)
        {
            entity->getSprite()->setVisible(_undergroundVisible);
//            entity->getSprite()->setZIndexOffset(UNDERGROUND_Z_OFFSET);
            entity->getSprite()->setPositionZOffset(Z_POSITION_UNDERGROUND + .01);
        }
        else
        {
            entity->getSprite()->setVisible(true);
//            entity->getSprite()->setZIndexOffset(0);
            entity->getSprite()->setPositionZOffset(Z_POSITION_GROUND_LEVEL);
        }
    });
}


void World::saveToArchive(cereal::BinaryOutputArchive & archive)
{
    archive(_worldSize);
    auto sections = getWorldSections();
    archive(sections.size());
    for(auto section : sections)
    {
        section->saveToArchive(archive);
    }
}

void World::serializeChanges(cereal::BinaryOutputArchive & archive)
{
    archive(_worldSectionRequests.size());
    for(auto & section : _worldSectionRequests)
    {
        archive(section.sectionOrigin);
        archive(section.sectionSize);
        archive(section.entranceToArea);
    }
    
    archive(_worldSectionsRemoved.size());
    for(Vec2 removedLocation : _worldSectionsRemoved)
    {
        archive(removedLocation);
    }
}

void World::deserializeChanges(cereal::BinaryInputArchive & archive)
{
    size_t size;
    Vec2 origin, entrance;
    Size sectionSize;

    archive(size);
    for(int i = 0; i < size; i++)
    {
        archive(origin);
        archive(sectionSize);
        archive(entrance);
        
        createUndergroundArea(origin, sectionSize, entrance, nullptr);
    }
    
    Vec2 sectionToRemove;
    archive(size);
    for(int i = 0; i < size; i++)
    {
        archive(sectionToRemove);
        destroyUndergroundArea(sectionToRemove);
    }
}

void World::clearChanges()
{
    _worldSectionRequests.clear();
    _worldSectionsRemoved.clear();
}

