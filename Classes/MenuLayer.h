//
//  MenuLayer.h
//  TrenchWars
//
//  Created by Paul Reed on 9/16/12.
//  Copyright (c) 2012 . All rights reserved.
//

#ifndef __MENU_LAYER_H__
#define __MENU_LAYER_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "AutoSizedLayout.h"
#include "Entity.h"
#include "CircularMenu.h"

USING_NS_CC;
using namespace cocos2d::ui;
//forward declations

class MovingEntitiesLayer;

struct UnitStatusUI : public Ref
{
private:
    Label * unitName;
    Map<std::string, Label *> statusLabels;

public:
    UnitStatusUI(std::string & name);
    void setStatusTextForLabel(std::string & labelName, std::string & labelValue, Node * scrollView);
    Label * getUnitName() {return unitName;}
    Map<std::string, Label *> * getStatusLabels() {return &statusLabels;}
};

//TODO FIX refactor this mess of a class
class MenuLayer : public AutoSizedLayout
{
private:    
    AutoSizedLayout * testingMenu;
    AutoSizedLayout * infoMenu;
    AutoSizedLayout * timingMenu;
    Layout * gameMenu;

    
    CircularMenu * _primaryCircularMenu;
    void createTestingMenu();
    void createInfoMenu();
    void createTimingMenu();
    void createGameMenu();
    
    ScrollView * unitStatusMenu;
    Node * scrollContent;
    
    //information items, not clickable
    Text * gameInfo;
    Sprite * menuBackground;
    
    Map<std::string, Button *> buttons;
    Map<ENTITY_ID, UnitStatusUI *> unitStatusLabels;
    ENTITY_ID highlightedUnitID;
    double updateTime;
    int framesPerUpdate;
    
    char * infoMenuString = new char[512];
    
    Button * createButton(std::string name, std::string text, Layout * menu, const AbstractCheckButton::ccWidgetClickCallback & callback);
    Button * createText(std::string text, Layout * menu);

    
    AutoSizedLayout * createLayout(Vec2 position, Layout::Type layoutDirection = Layout::Type::HORIZONTAL);
    void updateContentSize();
    
    virtual bool init();

public:
    MenuLayer();
    static MenuLayer * create();
    void updateGameInfo(float deltaTime);
    void update();
    CircularMenu * getPrimaryCircularMenu() {return _primaryCircularMenu;}
    void setUnitStatusVariableForUnit(ENTITY_ID unitID, std::string variableName, std::string value);
    void highlightUnitStatusLabel(ENTITY_ID unitID);
    void layoutUnitStatus();
    
    void setUpMenus();
    
    void toggleTimingMenu();
    void toggleGameMenu();

    bool pointInMenu(Vec2 point, bool click);
};

extern MenuLayer * globalMenuLayer;

#endif //__MENU_LAYER_H__
