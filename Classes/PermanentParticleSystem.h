//
//  PermanentParticleSystem.hpp
//  TrenchWars
//
//  Created by Paul Reed on 7/20/22.
//

#ifndef PermanentParticleSystem_h
#define PermanentParticleSystem_h


#include "cocos2d.h"

USING_NS_CC;

class PermanentParticleSystem : public ParticleSystemQuad
{
    bool _created;
    
    PermanentParticleSystem();
    ~PermanentParticleSystem();

public:
    
    static PermanentParticleSystem * createWithTotalParticles(int numberOfParticles);
    static PermanentParticleSystem * create(const std::string& filename);
    
    virtual void stop();
    virtual void update(float deltaTime);
};

#endif /* PermanentParticleSystem_h */
