//
//  ProjectileDefinitionXMLParser.cpp
//  TrenchWars
//
//  Created by Paul Reed on 4/24/20.
//

#include "ProjectileDefinitionXMLParser.h"
#include "CollisionGrid.h"
#include "StringUtils.h"
#include <stdexcept>


ProjectileDefinitionXMLParser::ProjectileDefinitionXMLParser(EntityFactory * factory)
{
    _entityFactory = factory;
    _currentProjectileDefinition = nullptr;
}

void ProjectileDefinitionXMLParser::parseFile(std::string const& filename)
{
    std::string fileContents = FileUtils::getInstance()->getStringFromFile(filename);
    if(!fileContents.empty())
    {
        xml_document doc;
        pugi::xml_parse_result result = doc.load(fileContents.c_str());
        if(result.status == xml_parse_status::status_ok)
        {
            bool parsedSuccessfully = doc.traverse(*this);
            if(!parsedSuccessfully)
            {
                CCLOG("parse error!");
            }
        }
        else
        {
            CCLOG("parse error %s ",result.description());
        }
    }
    else
    {
        CCLOG("ProjectileDefinitionXMLParser: Could not Open File %s",filename.c_str());
    }
}

bool ProjectileDefinitionXMLParser::for_each(xml_node& node)
{
    ci_string name = node.name();
    std::string value = node.first_child().value();
    
    try
    {
        if(name.compare("Projectile") == 0)
        {
            _currentProjectileDefinition = new ProjectileDefinition();
            auto attribute = node.attribute("id");
            if(attribute != nullptr)
            {
                _currentProjectileDefinition->_entityName = attribute.value();
                _currentProjectileDefinition->_hashedName = std::hash<std::string>{}(_currentProjectileDefinition->_entityName);

                _entityFactory->addPhysicalEntityDefinition(_currentProjectileDefinition);
            }
        }
        else if(name.compare("damage") == 0)
        {
            _currentProjectileDefinition->_damage =std::stoi(value);
        }
        else if(name.compare("speed") == 0)
        {
            _currentProjectileDefinition->_speed =std::stoi(value);
        }
        else if(name.compare("ammoCost") == 0)
        {
            _currentProjectileDefinition->_ammoCost =std::stoi(value);
        }
        else if(name.compare("spriteName") == 0)
        {
            _currentProjectileDefinition->_spriteName = value;
        }
        else if(name.compare("explosive") == 0)
        {
            _currentProjectileDefinition->_explosive =std::stoi(value);
        }
        else if(name.compare("airBurst") == 0)
        {
            _currentProjectileDefinition->_airBurst =std::stoi(value);
        }
        else if(name.compare("detonateParticleName") == 0)
        {
            _currentProjectileDefinition->_detonateParticleName = value;
        }
        else if(name.compare("smokeParticleName") == 0)
        {
            _currentProjectileDefinition->_smokeParticleName = value;
        }
        else if(name.compare("burstAngle") == 0)
        {
            _currentProjectileDefinition->_burstAngle =std::stof(value);
        }
        else if(name.compare("burstHeight") == 0)
        {
            _currentProjectileDefinition->_burstHeight =std::stof(value);
        }
    }
    catch (const std::invalid_argument& ia)
    {
        CCLOG("Invalid argument: %s for node %s with value %s ", ia.what(), name.c_str(), value.c_str());
    }
    
    return true;
}
