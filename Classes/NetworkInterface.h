//
//  NetworkInterface.hpp
//  TrenchWars
//
//  Created by Paul Reed on 8/29/23.
//

#ifndef NetworkInterface_h
#define NetworkInterface_h

#include "GameNetworkingSockets/steamnetworkingsockets.h"
#include "GameNetworkingSockets/isteamnetworkingutils.h"
#include "FixedStreamBuf.h"
#include "Logging.h"
#include "List.h"
#include <sstream>
#include <cereal/archives/binary.hpp>


enum NETWORK_MODE
{
    NETWORK_MODE_NONE,
    NETWORK_MODE_SINGLE,
    NETWORK_MODE_SERVER,
    NETWORK_MODE_CLIENT
};

enum CLIENT_STATE
{
    CLIENT_NOT_JOINED,
    CLIENT_IN_LOBBY,
    CLIENT_LOADING_SCENARIO,
    CLIENT_READY,
    CLIENT_DELAYED,
    CLIENT_ERROR
};

enum MESSAGE_TYPE
{
    // Server to Client
    MESSAGE_REMOTE_PLAYER_ID,
    MESSAGE_LOBBY_INFO,
    MESSAGE_SCENARIO_MAP, //?
    MESSAGE_GAME_START,
    MESSAGE_FULL_GAME_STATE,
    MESSAGE_ENTITY_QUERIES,
    MESSAGE_ENTITY_ADDITIONS_REMOVALS,
    MESSAGE_TEAM_STATE,
    
    // Client to Server
    MESSAGE_CLIENT_UPDATE,
    MESSAGE_TEST
};


enum PLAYER_MESSAGE_DATA_TYPES
{
    PLAYER_MESSAGE_NAME = 1,
    PLAYER_MESSAGE_STATE = 2,
    PLAYER_MESSAGE_CHAT = 4,
    PLAYER_MESSAGE_COMMAND_SELECT = 8,
    PLAYER_MESSAGE_READY_TO_START = 16,
    PLAYER_MESSAGE_ORDER = 32,
    PLAYER_MESSAGE_PLAN= 64,
    PLAYER_MESSAGE_OBJECTIVE= 128,
};

struct NetworkClient_t
{
    HSteamNetConnection _connectionId;
    CLIENT_STATE _clientState;
    SteamNetworkingIdentity _networkIdentity;
    int _playerId;
};

enum NETWORK_FAILURE_TYPE
{
    NETWORK_FAILURE_CONNECT_TO_SERVER_FAILED,
    NETWORK_FAILURE_CLIENT_DROPPED,
};

struct NetworkFailure
{
    NETWORK_FAILURE_TYPE failureType;
    HSteamNetConnection failedConnectionId;
    int playerId;
    
    NetworkFailure(NETWORK_FAILURE_TYPE type, HSteamNetConnection connection, int player)
    {
        failureType = type;
        failedConnectionId = connection;
        playerId = player;
    }
};

enum NETWORK_STATUS
{
    NETWORK_STATUS_NONE,
    NETWORK_STATUS_SERVER_LISTENING,
    NETWORK_STATUS_CONNECTING_TO_SERVER,
    NETWORK_STATUS_CONNECTED_TO_SERVER,
    NETWORK_STATUS_ERROR
};

struct MessageHeader
{
    unsigned int messageId;
    unsigned int messageDataSize;
    unsigned int messageCount;
    unsigned int startByte;
    long long frameNumber;
    int messageType;
};

union MessageHeaderUnion
{
    MessageHeader * header;
    char * byteValue;
};

const unsigned char MESSAGE_HEADER_SIZE = sizeof(MessageHeader);


class GameMessage : public Ref
{
protected:
    GameMessage(char * bytes, size_t byteSize, int player, bool copyData);
    ~GameMessage();

    ArrayInputStreamBuf * buffer;
    std::istream * input;
    cereal::BinaryInputArchive * iArchive;

    MessageHeaderUnion headerUnion;
    char * messageBytes;
    size_t messageSize;
    int playerId;
    
public:
    static GameMessage * create(char * bytes, size_t byteSize, int player, bool copyData = true);
    inline cereal::BinaryInputArchive & getArchive() {return *iArchive;}
    inline char * getMessageBytes() {return messageBytes;}
    inline void setMessageBytes(char * bytes) {messageBytes = bytes;}
    
    inline MessageHeader * getHeader() {return headerUnion.header;}
    inline int getPlayerId() {return playerId;}
    
    void createArchive();
};

class ChunkedMessage : public GameMessage
{
protected:

    ChunkedMessage(size_t byteSize, int messageCount, int player);
    ChunkedMessage();
    ~ChunkedMessage();

    int _totalMessages;
    int _messagesReceived;
    
public:
    static ChunkedMessage * create(size_t byteSize, int messageCount, int player);
    
    int getTotalMessage() {return _totalMessages;}
    
    int getMessagesReceived() {return _messagesReceived;}
    void incrementMessagesReceived() {_messagesReceived++;}
};

class MessageBuffer
{
protected:
    char * _messageBuffer;
    std::list<HSteamNetConnection> connectionsToSendTo;
    long long frameNumber;
    MESSAGE_TYPE messageType;
    
    FixedOutputStreamBuf _streamBuf;
    std::ostream _outStream;
    cereal::BinaryOutputArchive _archive;
    
    int _bufferId;
        
    void reset();
    MessageBuffer();
    ~MessageBuffer();
    
    friend class NetworkInterface;
    friend class NetworkServer;
    friend class NetworkClient;

    
public:
    cereal::BinaryOutputArchive & getArchive() {return _archive;}
    
    char * getMessageBuffer() {return _messageBuffer;}
    FixedOutputStreamBuf & getStreamBuf() {return _streamBuf;}
    long long getFrameNumber() {return frameNumber;}
};

class NetworkInterface
{
protected:

    ISteamNetworkingSockets * _networkInterface;
    std::vector<NetworkFailure> _networkFailures;
    NETWORK_STATUS _networkStatus;

    // Sending data
    std::list<MessageBuffer * > _availableBuffers;
    std::list<MessageBuffer * > _buffersWaitingToBeSent;
    std::unordered_map<int, MessageBuffer * > _inUseBuffers;

    std::mutex _bufferMutex;
    int _nextMessageId;
    
    
    // Receiving data
    Map<int, ChunkedMessage *> _chunkedMessageMap;
    List<GameMessage *> _receivedMessageQueue;
    std::mutex _receivedMessagesMutex;
    int _nextBufferId;

    
    
    void parseChunkedMessage(char * data, int dataSize, int playerId = -1);
    void parseStandardMessage(char * data, int dataSize, int playerId = -1);
    
    void sendChunkedMessage(MessageBuffer * message);
    void sendStandardMessage(MessageBuffer * message);
    void sendQueuedMessages();
    
    void stupidTestSend(int playerId);
    void stupidTestReceive(GameMessage * message);
    
    void pollConnectionStateChanges();
    virtual void pollIncomingMessages() {};


public:
    NetworkInterface();
    static void DebugOutput( ESteamNetworkingSocketsDebugOutputType eType, const char *pszMsg );
    static void SteamNetConnectionStatusChangedCallback( SteamNetConnectionStatusChangedCallback_t *pInfo );
    virtual void OnSteamNetConnectionStatusChanged( SteamNetConnectionStatusChangedCallback_t *connectionInfo ) {};
    
    virtual void sendPreparedDataToPlayer(MessageBuffer * buffer, int playerId = 0) {};
    virtual void sendPreparedDataToPlayers(MessageBuffer * buffer, std::list<int> & players) {};

    virtual void startListening() {};
    virtual void connectToServer(const std::string & serverIp) {};
    virtual void update();
    
    void drainReceivedQueue();
    
    NETWORK_STATUS getNetworkStatus() {return _networkStatus;}
    MessageBuffer * prepareNetworkPackage(MESSAGE_TYPE messageType, long long frameNumber = -1);
    
    const std::vector<NetworkFailure> getFailures() {return _networkFailures;}
    void clearFailures() {_networkFailures.clear();}

    void shutdown();
};

extern NetworkInterface * globalNetworkInterface;

#endif /* NetworkInterface_h */
