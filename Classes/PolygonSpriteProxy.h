//
//  PolygonSpriteProxy.h
//  trench-wars
//
//  Created by Paul Reed on 10/3/23.
//

#ifndef PolygonSpriteProxy_h
#define PolygonSpriteProxy_h

#include "cocos2d.h"
#include "Refs.h"
#include "EntityDefinitions.h"
#include "ChangeTrackingVariable.h"
#include "VectorMath.h"
#include "CocosProxy.h"

USING_NS_CC;

class PolygonSpriteProxy  : public CocosProxy
{
    Sprite * _sprite;
    std::vector<Vec2> _edgeVertices;
    std::unordered_map<Vec2, Vec2, Vec2Hash, Vec2Compare> _textureCoords;
    
    std::string _textureName;
    Vec2 _textureSize;
    Vec2 _positionOffset;
    Vec2 _origin;
    
    
    PolygonLandscapeObjectDefiniton * _definition;
    PrimitiveChangeTrackingVariable<Vec2> _pendingPosition;
    SimpleChangeTrackingVariable<int> _pendingZIndex;
    SimpleChangeTrackingVariable<float> _pendingZPosition;
    
    SimpleChangeTrackingVariable<bool> _pendingVisible;
    SimpleChangeTrackingVariable<AffineTransform> _pendingTransform;
    
    bool _needsUpdate;
    bool _boundaryPolygon;
    PolygonSpriteProxy();
    void init(std::vector<Vec2> & edgeVertices, const std::string & textureName, Vec2 textureSize, bool boundaryPolygon);
    
    TrianglesCommand::Triangles generateTriangles(const std::vector<Vec2>& points);
    void calculateStandardUV(const Rect& rect, V3F_C4B_T2F* verts, ssize_t count);
    void calculatePolygonFollowingUV(V3F_C4B_T2F* verts, ssize_t count);
    
    void createBoundaryVertices();
    
public:
    
    ~PolygonSpriteProxy();
    static PolygonSpriteProxy * create(std::vector<Vec2> & edgeVertices, const std::string & textureName, Vec2 _textureSize, bool boundaryPolygon);
    
    virtual Sprite * getSprite() {return _sprite;}
    virtual ProxyType getProxyType() {return POLYGON_SPRITE_PROXY;}
    
    
    virtual void createCocosObject();
    virtual bool updateCocosObject();
    virtual void setPosition(Vec2 position);
    virtual void setZIndex(int index);
    virtual void setPositionZ(float position);
    virtual void setVisible(bool visible);
    virtual void setAdditionalTransform(AffineTransform & transform);
    
    void setPositionOffset(Vec2 offset);
    void remove();
};

#endif /* PolygonSpriteProxy_h */
