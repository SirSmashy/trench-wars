//
//  InteractiveObjectHighlighter.cpp
//  TrenchWars
//
//  Created by Paul Reed on 2/21/21.
//

#include "InteractiveObjectHighlighter.h"

#include "Unit.h"
#include "Infantry.h"
#include "UnitGroup.h"
#include "PlayerLayer.h"
#include "EmblemManager.h"
#include "InteractiveObjectUtils.h"
#include "EntityManager.h"
#include "CrewWeapon.h"
#include "World.h"
#include "CrewWeaponUnit.h"
#include "UIController.h"
#include "CollisionGrid.h"

#define PRIMARY_COLOR Color4F(.9,.9,.3,.5);
#define SECONDARY_COLOR Color4F(.4,.4,.3,.4);

InteractiveObjectHighlighter * globalDebugHighlighter;


LineRecord::LineRecord(LineRecord const &other)
{
    _originator = other._originator;
    _fromEntity = other._fromEntity;
    _toEntity = other._toEntity;
    
    _originatorType = other._originatorType;
    _fromType = other._fromType;
    _toType = other._toType;
    
    _line = other._line;
    _id = other.uID();
}

LineRecord::LineRecord(ENTITY_ID originator,
           INTERACTIVE_OBJECT_TYPE originatorType,
           ENTITY_ID fromEntity,
           INTERACTIVE_OBJECT_TYPE fromType,
           ENTITY_ID toEntity,
           INTERACTIVE_OBJECT_TYPE toType,
           LineDrawer * line)
{
    _originator = originator;
    _fromEntity = fromEntity;
    _toEntity = toEntity;
    
    _originatorType = originatorType;
    _fromType = fromType;
    _toType = toType;
    
    _line = line;
}

InteractiveObject * getToObject();
InteractiveObject * getFromObject();
InteractiveObject * getOriginatorObject();


DotRecord::DotRecord(DotRecord const &other)
{
    _originator = other._originator;
    _originatorType = other._originatorType;
    
    _entity = other._entity;
    _entType = other._entType;
    
    _smallHighlight = other._smallHighlight;
    _id = other.uID();
}


DotRecord::DotRecord(ENTITY_ID originator, INTERACTIVE_OBJECT_TYPE originatorType,  ENTITY_ID entity, INTERACTIVE_OBJECT_TYPE entType, bool smallHighlight)
{
    _originator = originator;
    _originatorType = originatorType;
    _entity = entity;
    _entType = entType;
    _smallHighlight = smallHighlight;
}

InteractiveObjectHighlighter::InteractiveObjectHighlighter() : DrawNode()
{
    
}


InteractiveObjectHighlighter::~InteractiveObjectHighlighter()
{
}


bool InteractiveObjectHighlighter::init()
{
    if(DrawNode::init()) {
        globalPlayerLayer->addChild(this, 0); //1000
        Director * director = Director::getInstance();
        Scheduler * scheduler = director->getScheduler();
        scheduler->schedule([=] (float dt) { update(dt); }, this, 0.0333333333, false, "highlight");
        return true;
    }
    return false;
}


InteractiveObjectHighlighter * InteractiveObjectHighlighter::create()
{
    InteractiveObjectHighlighter * highlighter = new InteractiveObjectHighlighter();
    if(highlighter->init())
    {
        highlighter->autorelease();
        return  highlighter;
    }
    
    CC_SAFE_DELETE(highlighter);
    return nullptr;
}


void InteractiveObjectHighlighter::setObject(InteractiveObject * entity)
{
    if(entity == nullptr)
    {
        // If the entity is null just clear everything out
        clear();
        clearRecords();
        _objects.clear();
    }
    else
    {
        // InteractiveObject is NOT null, so look at ALL the lines and dots and remove any that aren't originated by this ent
        // If no lines or dots are found for this ent, then create the appropiate highlights
        std::vector<LineRecord> invalidLines;
        std::vector<DotRecord> invalidDots;

        bool createHighlights = true;
        for(auto lineRecord : _lineRecords)
        {
            if(lineRecord._originator == entity->uID())
            {
                createHighlights = false;
            }
            else
            {
                invalidLines.push_back(lineRecord);
            }
        }
        for(auto dotRecord : _dotRecords)
        {
            if(dotRecord._originator == entity->uID())
            {
                createHighlights = false;
            }
            else
            {
                invalidDots.push_back(dotRecord);
            }
        }
        
        for(auto badLine : invalidLines)
        {
            badLine._line->remove();
            _lineRecords.erase(badLine);
        }
        
        for(auto badDot : invalidDots)
        {
            _dotRecords.erase(badDot);
        }
        
        _objects.clear();
        _objects.insert(entity->uID());
        if(createHighlights)
        {
            createHighlightsForObject(entity);
        }
    }
}

void InteractiveObjectHighlighter::addObject(InteractiveObject * entity)
{
    if(entity != nullptr && !_objects.contains(entity->uID()))
    {
        _objects.insert(entity->uID());
        createHighlightsForObject(entity);
    }
}

void InteractiveObjectHighlighter::removeObject(ENTITY_ID entity)
{
    std::vector<LineRecord> invalidLines;
    std::vector<DotRecord> invalidDots;
    
    for(auto lineRecord : _lineRecords)
    {
        if(lineRecord._originator == entity)
        {
            invalidLines.push_back(lineRecord);
        }
    }
    for(auto dotRecord : _dotRecords)
    {
        if(dotRecord._originator == entity)
        {
            invalidDots.push_back(dotRecord);
        }
    }
    
    for(auto badLine : invalidLines)
    {
        badLine._line->remove();
        _lineRecords.erase(badLine);
    }
    
    for(auto badDot : invalidDots)
    {
        _dotRecords.erase(badDot);
    }
    
    _objects.erase(entity);
}

void InteractiveObjectHighlighter::removeObject(InteractiveObject * entity)
{
    removeObject(entity->uID());
}

void InteractiveObjectHighlighter::clearObjects()
{
    _objects.clear();
    clearRecords();
    clear();
}

void InteractiveObjectHighlighter::createHighlightsForObject(InteractiveObject * object)
{
    if(object == nullptr)
    {
        return;
    }
    if(object->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
    {
        Entity * ent = dynamic_cast<Entity *>(object);
        if(ent->getEntityType() == INFANTRY)
        {
            createDot(ent, ent, false);
            Infantry * infantry = dynamic_cast<Infantry *>(ent);
            Emblem * unitEmblem = globalEmblemManager->getEmblemForCommandableObjectWithType(infantry->getOwningUnit()->uID(),POSITION_EMBLEM);
            if(unitEmblem != nullptr)
            {
                createLine(ent, ent, unitEmblem);
            }
        }
        else if(ent->getEntityType() == CREW_WEAPON)
        {
            createDot(ent, ent, false);
            CrewWeapon * crew = dynamic_cast<CrewWeapon *> (ent);
            if(crew->getOperator() != nullptr)
            {
                createLine(ent, ent, crew->getOperator(), Color3B::RED);
            }
            Emblem * unitEmblem = globalEmblemManager->getEmblemForCommandableObjectWithType(crew->getOwningUnit()->uID(),POSITION_EMBLEM);
            if(unitEmblem != nullptr)
            {
                createLine(ent, ent, unitEmblem);
            }
        }
        else if(ent->getEntityType() == COURIER)
        {
            createDot(ent, ent, false);
            Courier * courier = dynamic_cast<Courier *>(ent);
            createLine(ent, courier, courier->getOwningPost());
        }
        else if(ent->getEntityType() == COMMANDPOST)
        {
            createDot(ent, ent, false);
            CommandPost * post = dynamic_cast<CommandPost *>(ent);
            for(Courier * courier : post->getCouriers())
            {
                createDot(ent, courier, true);
                createLine(ent, ent, courier);
            }
        }
    }
    
    else if(object->getInteractiveObjectType() == EMBLEM_INTERACTIVE_OBJECT)
    {
        createDot(object, object, false);
        Emblem * emblem = (Emblem *) object;
        InteractiveObject * commandable = emblem->getCommandableObject();
        
        Emblem * positionEmblem = globalEmblemManager->getEmblemForCommandableObjectWithType(commandable->uID(),POSITION_EMBLEM);
        bool isPositionEmblem = true;
        if(positionEmblem != nullptr && emblem != positionEmblem)
        {
            createLine(object, object, positionEmblem);
            isPositionEmblem = false;
        }
        if(commandable->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
        {
            Unit * unit = (Unit *) commandable;
            for(auto ent : *unit)
            {
                Infantry * unitMember = (Infantry *) ent;
                createDot(object, unitMember, true);
                if(isPositionEmblem)
                {
                    createLine(object, object, unitMember);
                }
            }
            if(isPositionEmblem)
            {
                for(auto group : globalUIController->getUnitGroups())
                {
                    if(group.second->containsUnit(unit))
                    {
                        Emblem * groupEmblem = globalEmblemManager->getEmblemForCommandableObjectWithType(group.second->uID(),POSITION_EMBLEM);
                        if(groupEmblem != nullptr)
                        {
                            createLine(object, object, groupEmblem);
                        }
                        else
                        {
                            LOG_DEBUG_ERROR("WHy! \n");
                        }
                    }
                }
            }

        }
        else if(commandable->getInteractiveObjectType() == UNIT_GROUP_INTERACTIVE_OBJECT)
        {
            UnitGroup * group = (UnitGroup *) commandable;
            for(Unit * unit : group->getUnits())
            {
                Emblem * unitEmblem = globalEmblemManager->getEmblemForCommandableObjectWithType(unit->uID(),POSITION_EMBLEM);
                createDot(object, unitEmblem, true);
                if(isPositionEmblem)
                {
                    createLine(object, object, unitEmblem);
//                    for(auto ent : unit)
//                    {
//                        Infantry * unitMember = (Infantry *) ent;
//                        createDot(object, unitMember, true);
//                        createLine(object, unitEmblem, unitMember);
//                    }
                }
            }
        }
    }
}

void InteractiveObjectHighlighter::update(float deltaTime)
{
    clear();
    std::vector<LineRecord> invalidLines;
    std::vector<DotRecord> invalidDots;
    
    for(auto lineRecord : _lineRecords)
    {
        InteractiveObject * originator = getObject(lineRecord._originatorType, lineRecord._originator);
        InteractiveObject * from = getObject(lineRecord._fromType, lineRecord._fromEntity);
        InteractiveObject * to = getObject(lineRecord._toType, lineRecord._toEntity);

        if(!_objects.contains(lineRecord._originator) ||
           originator == nullptr)
        {
            invalidLines.push_back(lineRecord);
            continue;
        }
        if(from == nullptr || to == nullptr)
        {
            continue;
        }

        
        Vec2 start = world->getAboveGroundPosition(InteractiveObjectUtils::getCenterOfObjectPosition(from)) * WORLD_TO_GRAPHICS_SIZE;
        Vec2 end = world->getAboveGroundPosition(InteractiveObjectUtils::getCenterOfObjectPosition(to)) * WORLD_TO_GRAPHICS_SIZE;
        float width = 2 / globalPlayerLayer->getScale();
        lineRecord._line->setWidth(width);
        lineRecord._line->setLinePosition(start, end);
    }
    
    for(auto dotRecord : _dotRecords)
    {
        InteractiveObject * originator = getObject(dotRecord._originatorType, dotRecord._originator);
        InteractiveObject * ent = getObject(dotRecord._entType, dotRecord._entity);
        
        if(!_objects.contains(dotRecord._originator) ||
           originator == nullptr ||
           ent == nullptr)
        {
            invalidDots.push_back(dotRecord);
            continue;
        }
        
        Vec2 position = world->getAboveGroundPosition(InteractiveObjectUtils::getCenterOfObjectPosition(ent));
        Color4F color = PRIMARY_COLOR;
        if(dotRecord._smallHighlight)
        {
            color = SECONDARY_COLOR;
        }
        
        Size size = InteractiveObjectUtils::getObjectSize(ent) * WORLD_TO_GRAPHICS_SIZE;
        float radius = std::max(size.width, size.height);
        drawDot(position * WORLD_TO_GRAPHICS_SIZE, radius, color);
    }
    
    for(auto badLine : invalidLines)
    {
        badLine._line->remove();
        _lineRecords.erase(badLine);
    }
    
    for(auto badDot : invalidDots)
    {
        _dotRecords.erase(badDot);
    }
}

void InteractiveObjectHighlighter::createLine(InteractiveObject * originator, InteractiveObject * from, InteractiveObject * to, Color3B color)
{
    Vec2 fromPosition = InteractiveObjectUtils::getPositionForObject(from);
    Vec2 toPosition = InteractiveObjectUtils::getPositionForObject(to);
    
    LineDrawer * line = LineDrawer::create(fromPosition * WORLD_TO_GRAPHICS_SIZE, toPosition * WORLD_TO_GRAPHICS_SIZE);
    line->retain();
    line->setColor(color);
    line->setOpacity(200);
    line->makeDashed();
    line->setFlowing(true);
    
    _lineRecords.insert(LineRecord(originator->uID(),originator->getInteractiveObjectType(),
                                   from->uID(), from->getInteractiveObjectType(),
                                   to->uID(), to->getInteractiveObjectType(),
                                   line));
}

void InteractiveObjectHighlighter::createDot(InteractiveObject * originator, InteractiveObject * other, bool smallHighlight)
{
    _dotRecords.insert(DotRecord(originator->uID(),originator->getInteractiveObjectType(),
                                 other->uID(),other->getInteractiveObjectType(),
                                 smallHighlight));
}

InteractiveObject * InteractiveObjectHighlighter::getObject(INTERACTIVE_OBJECT_TYPE type, ENTITY_ID objectId)
{
    if(type == ENTITY_INTERACTIVE_OBJECT)
    {
        return globalEntityManager->getEntity(objectId);
    }
    else if(type == EMBLEM_INTERACTIVE_OBJECT)
    {
        return globalEmblemManager->getEmblem(objectId);
    }
    return nullptr;
}


void InteractiveObjectHighlighter::clearRecords()
{
    for(auto record : _lineRecords)
    {
        record._line->remove();
    }
    _lineRecords.clear();
    _dotRecords.clear();
}

void InteractiveObjectHighlighter::remove()
{
    _objects.clear();
    clearRecords();
    clear();
    globalPlayerLayer->removeChild(this);
    Director * director = Director::getInstance();
    Scheduler * scheduler = director->getScheduler();
    scheduler->unschedule("highlight", this);
}
