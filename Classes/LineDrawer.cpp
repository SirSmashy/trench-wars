//
//  LineDrawer.cpp
//  TrenchWars
//
//  Created by Paul Reed on 12/5/22.
//

#include "LineDrawer.h"
#include "EntityFactory.h"
#include "PlayerLayer.h"
#include "VectorMath.h"

LineDrawer::~LineDrawer()
{
}

bool LineDrawer::init(Vec2 startPosition, Vec2 endPosition, float width)
{
    _lineWidth = width;
    EntityAnimationSet * set = globalEntityFactory->getEntityAnimationSet("line");
    AnimatedSprite::init(set, startPosition);
    
    Texture2D::TexParams params;
    params.magFilter = params.minFilter = GL_LINEAR;
    params.wrapS = GL_REPEAT;
    params.wrapT = GL_REPEAT;
    getTexture()->setTexParameters(params);
    setStretchEnabled(false);

    setLinePosition(startPosition, endPosition);
    setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    globalPlayerLayer->addChild(this);
    
    return true;
}

LineDrawer * LineDrawer::create(Vec2 startPosition, Vec2 endPosition, float width)
{
    LineDrawer * sprite = new LineDrawer();
    if(sprite->init(startPosition, endPosition, width)) {
        sprite->autorelease();
        return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
}

void LineDrawer::setLinePosition(Vec2 startPosition, Vec2 endPosition)
{
    _endPosition = endPosition;
    setPosition(startPosition);
    
    Size size = getContentSize();
    float length = endPosition.distance(startPosition);
    Vec2 startToEnd = _endPosition - startPosition;
    
    setRotation(VectorMath::signedAngleBetweenVecs(startToEnd, Vec2(1,0)) * R2D);
    setScale(length / size.width, _lineWidth / size.height);
    scheduleUpdate();
}

void LineDrawer::setEndPosition(Vec2 endPosition)
{
    setLinePosition(getPosition(), endPosition);
}

void LineDrawer::setStartPosition(Vec2 startPosition)
{
    setLinePosition(startPosition, _endPosition);
}


void LineDrawer::update(float delta)
{
    auto spriteFrame = getSpriteFrame();
    if(spriteFrame != nullptr)
    {
        Size size = getContentSize();
        float length = _endPosition.distance(getPosition());
        Rect textureCoords = spriteFrame->getRect();
        float lineWidth = _lineWidth;
        if(_flowing)
        {
            float textLength = textureCoords.size.width;
            textureCoords.origin.x = _previousTextureCoords.origin.x - (textLength * delta);
            if(textureCoords.origin.x < 0)
            {
                textureCoords.origin.x += textLength;
            }
        }
        else if(_scaleWidth)
        {
            lineWidth /= globalPlayerLayer->getScale();
        }
        else
        {
            unscheduleUpdate();
        }
        
        textureCoords.size.width = length;
        _previousTextureCoords = textureCoords;
        setTextureCoords(textureCoords);
        setScale(length / size.width, lineWidth / size.height);
    }
    AnimatedSprite::update(delta);
}

void LineDrawer::setWidth(float width)
{
    _lineWidth = width;
    scheduleUpdate();
}

void LineDrawer::setScaleWidthWithParentScale(bool scaleWidth)
{
    _scaleWidth = scaleWidth;
    scheduleUpdate();
}

void LineDrawer::setFlowing(bool flowing)
{
    _flowing = flowing;
    scheduleUpdate();
}

void LineDrawer::setAnimation(ANIMATION_TYPE animation, bool repeat)
{
    AnimatedSprite::setAnimation(animation, repeat);
    scheduleUpdate();
}

void LineDrawer::makeSolid()
{
    setAnimation(LINE_SOLID, false);
}

void LineDrawer::makeDashed()
{
    setAnimation(LINE_DASHED, false);
}

void LineDrawer::makeDotted()
{
    setAnimation(LINE_DOTTED, false);
}

void LineDrawer::remove()
{
    unscheduleUpdate();
    globalPlayerLayer->removeChild(this);
}
