//
//  EngagementManager.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/2/20.
//

#include <fstream>
#include <ios>
#include <sstream>
#include <unordered_set>

#include "EngagementManager.h"
#include "EngagementScene.h"
#include "EmblemManager.h"
#include "Timing.h"
#include "World.h"
#include "MultithreadedAutoReleasePool.h"
#include "TestBed.h"
#include "GameStatistics.h"
#include "NavigationSystem.h"
#include "EntityManager.h"
#include "ParticleManager.h"
#include "CocosObjectManager.h"
#include "SectorGrid.h"
#include "TeamManager.h"
#include "Command.h"
#include "InteractiveObjectHighlighter.h"
#include "BackgroundTaskHandler.h"
#include "UIController.h"

EngagementManager * globalEngagementManager;

EngagementManager::EngagementManager()
{
}



void EngagementManager::prepareGame()
{
    MicroSecondSpeedTimer timer( [=](long long int time) {
        LOG_INFO("Prepare game  : %.2f \n", time / 1000000.0);
    });
    globalEngagementManager = this;
    _commandLayer = PlayerLayer::create();
    _commandLayer->retain();
    _commandLayer->setPositionZ(Z_POSITION_command_LAYER);
    
    _menuLayer = MenuLayer::create();
    _menuLayer->retain();
    _menuLayer->setPositionZ(Z_POSITION_MENU_LAYER);
        
    new EntityManager();
    new ParticleManager();
    GameStatistics * stats = GameStatistics::create();
    stats->retain();
    new CocosObjectManager();
    new NavigationSystem();
    new EmblemManager();
    globalDebugHighlighter = InteractiveObjectHighlighter::create();
    
    TestBed * testBed = TestBed::create();
    testBed->retain();
    testEntity = nullptr;
    
    _frameCount = 0;
    _frameTime = CURRENT_TIME;
    _engagementTime = 0;
    _framesSinceQuerying = 1000;
    _queryDeltaTime = 0;
    _didPathingLastFrame = false;
        
    _engagementScene = EngagementScene::create();
    _engagementScene->addChild(globalPlayerLayer, 10); // 10
    _engagementScene->addChild(globalMenuLayer, 9999); // 1000
    Director::getInstance()->replaceScene(TransitionFade::create(0.5, _engagementScene, Color3B(0,255,255)));
}



void EngagementManager::act(float deltaTime)
{
    MicroSecondSpeedTimer actTimer( [=](long long int time) {
        globalGameStatistics->setTestValue("Act: ", (double)time/1000.0);
    });
    globalEntityManager->act(deltaTime);
    
    //While other threads process entity acting, update emblems and split the navigation grids (as needed)
    globalEmblemManager->update(deltaTime);
    globalNavigationSystem->splitNavigationPatches();
    globalTestBed->update(deltaTime);
    
    while(globalBackgroundTaskHandler->runNextBackgroundTask()) {}
    while(_tasksRunning.load() > 0) {}
}

void EngagementManager::updateManagerObjects(float deltaTime)
{
    /////////////// UPDATE ////////////////////////////////////
    ///////////////////////////////////////////////////////////
    MicroSecondSpeedTimer updateTimer( [=](long long int time) {
        globalGameStatistics->setTestValue("Update: ", (double)time/1000.0);
    });
    
    //Update the navigation grid, don't allow pathfinding to occur while the grid is being updated
    _updatingNavigation.store(false);
    globalBackgroundTaskHandler->_canRunPathfindingTasks.store(false);
    bool navGridsChanged = globalNavigationSystem->updateNavigationGrids();
    
    // Additional main thread updates
    globalParticleManager->update(deltaTime);
    globalCocosObjectManager->updateProxies();
    executeMainThreadFunctions();
    globalEntityManager->update(deltaTime);
    if(globalUIController !=  nullptr)
    {
        globalUIController->update(deltaTime);
    }
    //Wait for all background tasks to complete
    while(globalBackgroundTaskHandler->runNextBackgroundTask()) {}
    while(_updatingNavigation.load() == true) 
    {
    }
    
    // If pathfinding occured this frame AND the nav grid needs updating, then don't allow pathing to occur until the nav grid update is complete
    if(globalNavigationSystem->getNeedsUpdating() && (_didPathingLastFrame || !navGridsChanged))
    {
        _didPathingLastFrame = false;
        globalBackgroundTaskHandler->_canRunPathfindingTasks.store(false);
    }
    else
    {
        _didPathingLastFrame = true;
        globalBackgroundTaskHandler->_canRunPathfindingTasks.store(true);
    }
    
    if(navGridsChanged)
    {
        globalTestBed->updateGrids = true;
    }
    
    if(globalSectorGrid->isDrawingSectors() && globalSectorGrid->needToRedraw())
    {
        globalSectorGrid->drawSectors();
    }
}


bool EngagementManager::isOneBeforeQueryFrame()
{
    return _framesSinceQuerying == (QUERY_EVERY_X_FRAME -1);
}


void EngagementManager::requestMainThreadFunction(const std::function<void (void)> & function)
{
    _mainThreadMutex.lock();
    _mainThreadFunctions.push_back(function);
    _mainThreadMutex.unlock();
}

void EngagementManager::executeMainThreadFunctions()
{
    _mainThreadMutex.lock();
    int functionsToRun = _mainThreadFunctions.size();
    _mainThreadMutex.unlock();
    while( functionsToRun > 0)
    {
        _mainThreadMutex.lock();
        auto function = _mainThreadFunctions.front();
        _mainThreadFunctions.pop_front();
        _mainThreadMutex.unlock();
        function();
        functionsToRun--;
    }
}

void EngagementManager::pauseGame()
{
    Director * director = Director::getInstance();
    director->pause();
    _engagementState = ENGAGEMENT_STATE_PAUSED;
}

void EngagementManager::unPauseGame()
{
    Director * director = Director::getInstance();
    director->resume();
    _engagementState = ENGAGEMENT_STATE_RUNNING;
}

void EngagementManager::togglePause()
{
    Director * director = Director::getInstance();
    if(_engagementState == ENGAGEMENT_STATE_RUNNING)
    {
        director->pause();
        _engagementState = ENGAGEMENT_STATE_PAUSED;
    }
    else
    {
        director->resume();
        _engagementState = ENGAGEMENT_STATE_RUNNING;
    }
}

void EngagementManager::playerLost(Team * losingTeam)
{
    _engagementScene->finish();
}

void EngagementManager::saveGame(const std::string & savePath)
{
    std::ofstream os(savePath, std::ios::binary);
    serializeGame(os);
    os.close();
}

void EngagementManager::serializeGame(std::ostream & outStream)
{
    cereal::BinaryOutputArchive archive( outStream );
    archive(ComparableRef::getMaxId());
    world->saveToArchive(archive);
    globalTeamManager->saveToArchive(archive);
    globalEntityManager->serializeEntities(archive, true);
}

void EngagementManager::shutdown()
{
    globalTeamManager->shutdown();
    globalEntityManager->shutdown();
    globalSectorGrid->shutdown();
    _menuLayer->release();
    _menuLayer = nullptr;
    _commandLayer->release();
    _commandLayer = nullptr;
    
    delete globalNavigationSystem;
    delete globalCocosObjectManager;
}
