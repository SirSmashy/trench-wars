//
//  ClientLobbyManager.cpp
//  TrenchWars
//
//  Created by Paul Reed on 11/20/23.
//

#include "ClientLobbyManager.h"
#include "TrenchWarsManager.h"
#include "LobbyScene.h"
#include "SerializationHelpers.h"
#include <cereal/types/string.hpp>


void ClientLobbyManager::init()
{
    // Create the Join Server scene here
    _lobbyNetworkMode = globalTrenchWarsManager->getNetworkMode();
    _joinScene = JoinServerScene::create(this);
    _joinScene->retain();
    _lobbyScene = nullptr;
    Director::getInstance()->replaceScene(TransitionFade::create(0.5, _joinScene, Color3B(0,255,255)));
    
    auto players = globalTrenchWarsManager->getPlayerMap();
    for(auto playerPair : players)
    {
        _playerLobbyStatusMap[playerPair.second->playerId].playerIsReady = false;
        _playerLobbyStatusMap[playerPair.second->playerId].playerNeedsScenario = true;
    }
    _waitingForConnection = false;
    _dataChanged = true;
    _receivedScenarioMap = false;
    _canStartGame = false;
}

ClientLobbyManager * ClientLobbyManager::create()
{
    ClientLobbyManager * lobby = new ClientLobbyManager();
    lobby->init();
    lobby->autorelease();
    return lobby;
}


void ClientLobbyManager::update(float deltaTime)
{
    globalNetworkInterface->update();
    globalNetworkInterface->drainReceivedQueue();
    
    if(_waitingForConnection)
    {
        if(globalNetworkInterface->getNetworkStatus() == NETWORK_STATUS_CONNECTED_TO_SERVER)
        {            
            _waitingForConnection = false;
            _lobbyScene = LobbyScene::create(this);
            _lobbyScene->retain();
            Director::getInstance()->replaceScene(TransitionFade::create(0.5, _lobbyScene, Color3B(0,255,255)));
        }
        else if(globalNetworkInterface->getNetworkStatus() == NETWORK_STATUS_ERROR)
        {
            _waitingForConnection = false;
            _joinScene->displayError("Failed to Connect");
            //Display an error
        }
    }
    
    if(_dataChanged && _lobbyScene != nullptr)
    {
        _lobbyScene->updateCommandsMenu();
    }
    
    _dataChanged = false;
    
    if(_canStartGame && _receivedScenarioMap)
    {
        //off we go
        Vector<TeamDefinition *> teams;
        int aiId = -1;
        for(auto teamPair : _teamIdToTeamDetailsMap)
        {
            TeamDefinition * teamDef = new TeamDefinition();
            teamDef->autorelease();
            teamDef->teamId = teamPair.second.teamId;
            teams.pushBack(teamDef);
            for(auto commandPair : teamPair.second.commandIdToCommandDetailsMap)
            {
                CommandDefinition * command = new CommandDefinition();
                command->autorelease();
                command->commandId = commandPair.second.commandId;
                command->controllingPlayerId = commandPair.second.playerId;
                command->teamId = commandPair.second.teamId;
                command->spawnPoint = commandPair.second.spawnPoint;
                teamDef->commands.pushBack(command);
            }
        }
        globalTrenchWarsManager->startEngagementAsClient(_mapData, _mapDataSize, teams);
    }
}

void ClientLobbyManager::playerSetName(int playerId, const std::string & name)
{
    globalTrenchWarsManager->playerSetName(playerId, name);
    _dataChanged = true;
}


void ClientLobbyManager::playerSelectedCommand(int teamId, int playerId, int commandId)
{
    Player * player = globalTrenchWarsManager->getPlayerForPlayerId(playerId);
    if(player == nullptr)
    {
        LOG_ERROR("Error! Player %d does not exist \n", playerId);
    }
    if(_teamIdToTeamDetailsMap.find(teamId) == _teamIdToTeamDetailsMap.end())
    {
        LOG_ERROR("Error! Team Id %d does not exist \n", teamId);
        return;
    }
    
    auto & newTeam = _teamIdToTeamDetailsMap.at(teamId);
    if(newTeam.commandIdToCommandDetailsMap.find(commandId) == newTeam.commandIdToCommandDetailsMap.end())
    {
        LOG_ERROR("Error! Command Id %d does not exist \n", commandId);
        return;
    }
    else if(newTeam.commandIdToCommandDetailsMap.at(commandId).playerId != -1)
    {
        LOG_ERROR("Error! Command %d already selected by plyaer %d \n", commandId, newTeam.commandIdToCommandDetailsMap.at(commandId).playerId);
        return;
    }
    
    // Set the player's old command to NO player
    if(_teamIdToTeamDetailsMap.find(player->teamId) != _teamIdToTeamDetailsMap.end())
    {
        auto & oldTeam = _teamIdToTeamDetailsMap.at(player->teamId);
        if(oldTeam.commandIdToCommandDetailsMap.find(player->commandId) != oldTeam.commandIdToCommandDetailsMap.end())
        {
            oldTeam.commandIdToCommandDetailsMap[player->commandId].playerId = -1; //NO Player
        }
    }
    
    newTeam.commandIdToCommandDetailsMap[commandId].playerId = playerId;
    newTeam.commandIdToCommandDetailsMap[commandId].teamId = teamId;
    player->commandId = commandId;
    player->teamId = teamId;
    
    _playerLobbyStatusMap[playerId].playerIsReady = false;
    MessageBuffer * buffer = globalNetworkInterface->prepareNetworkPackage(MESSAGE_CLIENT_UPDATE);
    cereal::BinaryOutputArchive & archive = buffer->getArchive();
    
    unsigned int updateFields = PLAYER_MESSAGE_COMMAND_SELECT;
    archive(updateFields);
    archive(teamId);
    archive(commandId);
    
    globalNetworkInterface->sendPreparedDataToPlayer(buffer);
    ////// Send data to server
    _dataChanged = true;
}


void ClientLobbyManager::playerSetScenario(const std::string & scenarioName)
{
}

void ClientLobbyManager::joinServer(const std::string serverIp)
{
    globalNetworkInterface->connectToServer(serverIp);
    _waitingForConnection = true;
}

void ClientLobbyManager::playerJoinedGame(Player * player)
{
    _playerLobbyStatusMap.try_emplace(player->playerId);
    _playerLobbyStatusMap[player->playerId].remotePlayer = player->remotePlayer;

    _dataChanged = true;
}

void ClientLobbyManager::playerLeftGame(int playerId)
{
    _playerLobbyStatusMap.erase(playerId);
    for(auto & teamPair : _teamIdToTeamDetailsMap)
    {
        for(auto & commandPair : teamPair.second.commandIdToCommandDetailsMap)
        {
            if(commandPair.second.playerId == playerId)
            {
                commandPair.second.playerId = -1;
                break;
            }
        }
    }
    _dataChanged = true;
}


void ClientLobbyManager::parseNetworkMessage(GameMessage * message)
{
    MESSAGE_TYPE messageType = (MESSAGE_TYPE) message->getHeader()->messageType;
    if(messageType == MESSAGE_REMOTE_PLAYER_ID)
    {
        auto & archive = message->getArchive();
        unsigned int typeInt;
        archive(typeInt);
        globalTrenchWarsManager->setLocalPlayerId(typeInt);
        
        MessageBuffer * buffer = globalNetworkInterface->prepareNetworkPackage(MESSAGE_CLIENT_UPDATE);
        cereal::BinaryOutputArchive & outArchive = buffer->getArchive();
        unsigned int updateFields = PLAYER_MESSAGE_NAME;
        outArchive(updateFields);
        Player * player = globalTrenchWarsManager->getLocalPlayer();
        outArchive(player->playerName);
        globalNetworkInterface->sendPreparedDataToPlayer(buffer);
    }
    else if(messageType == MESSAGE_LOBBY_INFO)
    {
        parseLobbyStatusMessage(message->getArchive());
    }
    else if(messageType == MESSAGE_SCENARIO_MAP)
    {
        parseScenarioMapMessage(message->getArchive());
    }
    else if(messageType == MESSAGE_GAME_START)
    {
        parseGameStartMessage(message->getArchive());
    }
}

void ClientLobbyManager::parseLobbyStatusMessage(cereal::BinaryInputArchive & iArchive)
{
    auto playerMap = globalTrenchWarsManager->getPlayerMap();
    int intValue;
    std::string archiveString;
    
    // Parse info about all players in the lobby, note that this contains info about the local player too
    size_t playerCount;
    iArchive(playerCount); // player count
    
    for(int i = 0; i < playerCount; i++)
    {
        iArchive(intValue); // player ID
        if(playerMap.find(intValue) == playerMap.end())
        {
            globalTrenchWarsManager->remotePlayerJoinedGame(intValue);
        }
        Player * player = globalTrenchWarsManager->getPlayerForPlayerId(intValue);
        iArchive(player->playerName);
        iArchive(player->commandId);
        iArchive(player->teamId);
        iArchive(intValue); // player ready
        _playerLobbyStatusMap[player->playerId].playerIsReady = intValue;
    }
    
    // Parse info about the commands for this scenario and which player is controlling each
    size_t teamCount, commandCount;
    int teamId, playerId, commandId;
    Vec2 spawnPoint;
    iArchive(teamCount);
    for(int i = 0; i < teamCount; i++)
    {
        iArchive(teamId);
        if(_teamIdToTeamDetailsMap.find(teamId) == _teamIdToTeamDetailsMap.end())
        {
            _teamIdToTeamDetailsMap.try_emplace(teamId);
        }
        auto & team = _teamIdToTeamDetailsMap.at(teamId);
        team.teamId = teamId;
        iArchive(commandCount);
        for(int j = 0; j < commandCount; j++)
        {
            iArchive(commandId);
            iArchive(playerId);
            iArchive(spawnPoint);
            if(team.commandIdToCommandDetailsMap.find(commandId) == team.commandIdToCommandDetailsMap.end())
            {
                team.commandIdToCommandDetailsMap.try_emplace(commandId);
            }
            
            team.commandIdToCommandDetailsMap[commandId].commandId = commandId;
            team.commandIdToCommandDetailsMap[commandId].teamId = teamId;
            team.commandIdToCommandDetailsMap[commandId].playerId = playerId;
            team.commandIdToCommandDetailsMap[commandId].spawnPoint = spawnPoint;
        }
    }
    
    std::string scenarioName;
    iArchive(scenarioName);
    _lobbyScene->scenarioSelected(scenarioName);
    _dataChanged = true;

}


void ClientLobbyManager::parseScenarioMapMessage(cereal::BinaryInputArchive & iArchive)
{
    if(_mapData != nullptr)
    {
        delete [] _mapData;
    }
    iArchive(_mapDataSize);
    _mapData = new unsigned char[_mapDataSize];
    for(int i = 0; i < _mapDataSize; i++)
    {
        iArchive(_mapData[i]);
    }
    _lobbyScene->setScenarioImage(_mapData, _mapDataSize);
    _receivedScenarioMap = true;
}

void ClientLobbyManager::parseGameStartMessage(cereal::BinaryInputArchive & iArchive)
{
    size_t size;
    int playerId, commandId, teamId;
    Vec2 spawnPoint;
    iArchive(size);
    for(int i = 0; i < size; i++)
    {
        size_t commandSize;
        iArchive(teamId);
        iArchive(commandSize);
        
        if(_teamIdToTeamDetailsMap.find(teamId) == _teamIdToTeamDetailsMap.end())
        {
            _teamIdToTeamDetailsMap.try_emplace(teamId);
        }
        
        for(int j = 0; j < commandSize; j++)
        {
            auto & teamDetails = _teamIdToTeamDetailsMap[teamId];
            iArchive(commandId);
            iArchive(playerId);
            iArchive(spawnPoint);
            if(teamDetails.commandIdToCommandDetailsMap.find(commandId) == teamDetails.commandIdToCommandDetailsMap.end())
            {
                teamDetails.commandIdToCommandDetailsMap.try_emplace(commandId);
            }
            teamDetails.commandIdToCommandDetailsMap[commandId].playerId = playerId;
            teamDetails.commandIdToCommandDetailsMap[commandId].teamId = teamId;
            teamDetails.commandIdToCommandDetailsMap[commandId].spawnPoint = spawnPoint;

        }
    }
    _canStartGame = true;
}

bool ClientLobbyManager::isPlayerReady(int playerId)
{
    if(_playerLobbyStatusMap.find(playerId) == _playerLobbyStatusMap.end())
    {
        return false;
    }
    return _playerLobbyStatusMap[playerId].playerIsReady;
}


void ClientLobbyManager::playerSelectedStartGame(int playerId)
{
    _playerLobbyStatusMap[playerId].playerIsReady = !_playerLobbyStatusMap[playerId].playerIsReady;
    
    // Tell the server we are ready
    MessageBuffer * buffer = globalNetworkInterface->prepareNetworkPackage(MESSAGE_CLIENT_UPDATE);
    cereal::BinaryOutputArchive & archive = buffer->getArchive();
    unsigned int updateFields = PLAYER_MESSAGE_READY_TO_START;
    archive(updateFields);
    archive((int)_playerLobbyStatusMap[playerId].playerIsReady);

    globalNetworkInterface->sendPreparedDataToPlayer(buffer);
}

void ClientLobbyManager::shutdown()
{
    Director * director = Director::getInstance();
    Scheduler * scheduler = director->getScheduler();
    scheduler->unschedule("lobby", this);
    _lobbyScene->release();
}
