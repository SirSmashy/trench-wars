//
//  InteractiveObjectHighlighter.hpp
//  TrenchWars
//
//  Created by Paul Reed on 2/21/21.
//

#ifndef InteractiveObjectHighlighter_h
#define InteractiveObjectHighlighter_h

#include <unordered_set>
#include "cocos2d.h"
#include "refs.h"
#include "LineDrawer.h"
#include "VectorMath.h"
#include "InteractiveObject.h"

USING_NS_CC;


class Entity;
class Emblem;
class MovingPhysicalEntity;
class Unit;
class UnitGroup;
class CommandPost;

struct LineRecord : public ComparableRef
{
    ENTITY_ID _fromEntity;
    ENTITY_ID _toEntity;
    ENTITY_ID _originator;
    
    INTERACTIVE_OBJECT_TYPE _fromType;
    INTERACTIVE_OBJECT_TYPE _toType;
    INTERACTIVE_OBJECT_TYPE _originatorType;
    
    LineDrawer * _line;

    LineRecord(LineRecord const &other);
    
    LineRecord(ENTITY_ID originator,
               INTERACTIVE_OBJECT_TYPE originatorType,
               ENTITY_ID fromEntity,
               INTERACTIVE_OBJECT_TYPE fromType,
               ENTITY_ID toEntity,
               INTERACTIVE_OBJECT_TYPE toType,
               LineDrawer * line);
    
    bool operator==(LineRecord const & other)
    {
        return _originator == other._originator && _fromEntity == other._fromEntity && _toEntity == other._toEntity;
    }
    
};

struct DotRecord : public ComparableRef
{
    ENTITY_ID _originator;
    ENTITY_ID _entity;
    
    INTERACTIVE_OBJECT_TYPE _originatorType;
    INTERACTIVE_OBJECT_TYPE _entType;

    
    bool _smallHighlight;
    
    DotRecord(DotRecord const &other);
    
    DotRecord(ENTITY_ID originator, INTERACTIVE_OBJECT_TYPE originatorType,  ENTITY_ID entity, INTERACTIVE_OBJECT_TYPE entType, bool smallHighlight);
    
    bool operator==(DotRecord const & other)
    {
        return _originator == other._originator && _entity == other._entity;
    }
};

class InteractiveObjectHighlighter : public DrawNode
{
    std::unordered_set<ENTITY_ID> _objects;
    std::unordered_set<LineRecord, ComparableRefHash, ComparableRefCompare> _lineRecords;
    std::unordered_set<DotRecord, ComparableRefHash, ComparableRefCompare> _dotRecords;
    
    Vector<LineDrawer *> _lines;

    InteractiveObjectHighlighter();
    ~InteractiveObjectHighlighter();
    bool init();
    
    void createHighlightsForObject(InteractiveObject * entity);
    void clearRecords();
    
    void createLine(InteractiveObject * originator, InteractiveObject * from, InteractiveObject * to, Color3B color = Color3B(114,255,0));
    void createDot(InteractiveObject * originator, InteractiveObject * other, bool smallHighlight);
    
    InteractiveObject * getObject(INTERACTIVE_OBJECT_TYPE type, ENTITY_ID objectId);
    
public:
    static InteractiveObjectHighlighter * create();
    void setObject(InteractiveObject * entity);
    void addObject(InteractiveObject * entity);
    std::unordered_set<ENTITY_ID> getHighlightedObjects() {return _objects;}
    void removeObject(InteractiveObject * object);
    void removeObject(ENTITY_ID entity);

    void clearObjects();
    virtual void update(float deltaTime);
    void remove();
};

extern InteractiveObjectHighlighter * globalDebugHighlighter;

#endif /* InteractiveObjectHighlighter_h */
