//
//  SectorGrid.h
//  TrenchWars
//
//  Created by Paul Reed on 10/11/12.
//  Copyright (c) 2012 . All rights reserved.
//

#ifndef __SECTOR_GRID_H__
#define __SECTOR_GRID_H__

#include "cocos2d.h"
#include "MapSector.h"
#include "CollisionGrid.h"
#include "LineDrawer.h"

class Command;

class SectorGrid
{
private:
    MapSector *** _sectorGrid;
    
    bool _drawingSectors;
    Vector<LineDrawer *> _sectorLines; //TODO FIX this doesn't belong here
    Vector<LineDrawer *> _sectorConnectionLines; //TODO FIX this doesn't belong here
    Map<ENTITY_ID, MapSector *> _sectorIdToSectorMap;
    
    int _sectorWidthInCells;
    Size _sectorGridSize;
    
    bool _redrawSectors;
    
public:
    SectorGrid(Size cellGridSize, int cellsInSector);
    
    void addTeam();
    MapSector * sectorForCell(CollisionCell * cell);
    MapSector * sectorAtIndex(int x, int y);
    MapSector * sectorForCellIndex(int x, int y);
    MapSector * sectorForPosition(Vec2 position);
    MapSector * sectorForUID(ENTITY_ID entId);
    void sectorsInBounds(Rect bounds, std::vector<MapSector *> * array);
    void sectorsInCircleWithCenter(Vec2 position, double radius, std::vector<MapSector *> * array);
    
    bool iterateSectorsInLine(Vec2 start, Vec2 end, const std::function<bool (MapSector * sector)> & iteratorFunction);
    
    Vec2 constrainPosition(Vec2 position);
    
    void toggleDrawSectors();
    void drawSectors();
    
    void generateSectorGridData();
    
    bool needToRedraw() {return _redrawSectors;}
    void setRedraw() {_redrawSectors = true; }
    void updateSectors();
    bool isDrawingSectors() {return _drawingSectors;}
    void shutdown();
    
    int getCellsInSector() {return _sectorWidthInCells;}
    Size getSectorGridSize() {return _sectorGridSize;}
};

extern SectorGrid * globalSectorGrid;
#endif //__SECTOR_GRID_H__
