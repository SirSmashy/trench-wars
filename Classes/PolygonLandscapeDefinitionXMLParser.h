//
//  PolygonLandscapeObjectParser.hpp
//  TrenchWars
//
//  Created by Paul Reed on 8/16/22.
//

#ifndef PolygonLandscapeDefinitionXMLParser_h
#define PolygonLandscapeDefinitionXMLParser_h

#include "pugixml/pugixml.hpp"
#include "EntityFactory.h"
#include "cocos2d.h"

using namespace pugi;
USING_NS_CC;

class PolygonLandscapeObjectXMLParser :public xml_tree_walker
{
    PolygonLandscapeObjectDefiniton * _currentDefinition;
    EntityFactory * _entityFactory;
    
    void checkFinishedDefinition();
public:
    PolygonLandscapeObjectXMLParser(EntityFactory * factory);
    
    void parseFile(std::string const & filename);
    virtual bool for_each(xml_node& node);
};
#endif /* PolygonLandscapeDefinitionXMLParser_h */
