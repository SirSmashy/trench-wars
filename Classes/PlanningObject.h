//
//  PlanningObject.h
//  TrenchWars
//
//  Created by Paul Reed on 1/5/25.
//

#ifndef PlanningObject_h
#define PlanningObject_h

#include "Entity.h"
#include "SerializationHelpers.h"
#include <cereal/archives/binary.hpp>


class Team;

class PlanningObject : public Entity
{
protected:
    Team * _owningTeam;
public:
    
    virtual void init(Team * owningTeam);
    Team * getOwningTeam() {return _owningTeam;}
    
    
    virtual bool testCollisionWithPoint(Vec2 point) {return false;}
    
    virtual void removeFromGame();
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;

};
#endif /* PlanningObject_h */
