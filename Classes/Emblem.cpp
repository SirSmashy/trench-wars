//
//  Emblem.cpp
//  TrenchWars
//
//  Created by Paul Reed on 9/19/20.
//

#include "Emblem.h"
#include "Unit.h"
#include "EntityManager.h"
#include "EntityFactory.h"
#include "Order.h"
#include "GameStatistics.h"
#include "PlayerVariableStore.h"
#include "CocosObjectManager.h"
#include "MultithreadedAutoReleasePool.h"
#include "World.h"
#include "PlayerLayer.h"
#include "EngagementManager.h"
#include "EmblemManager.h"
#include "TrenchWarsManager.h"
#include "CollisionGrid.h"

#define STATUS_SCALE .5

Emblem::Emblem() :
_pendingAnimation(EMBLEM_DEFAULT),
_pendingFlashing(false)
{
}


Emblem::~Emblem()
{
    _background->remove();
    _icon->remove();

    _background->release();
    _icon->release();
    removeAllStatusIcons();
}

bool Emblem::init(InteractiveObject * object, EMBLEM_TYPE type, Vec2 position)
{
    _ent = object;
    _scale = 1.0f;
    _emblemType = type;
    _visible = false;
    _disabled = false;
    _position = position;
    _background = AnimatedSpriteProxy::createWithNode("emblem", _position, nullptr);
    _background->retain();
    
    _isLocalPlayersEmblem = false;
    INTERACTIVE_OBJECT_TYPE objType = _ent->getInteractiveObjectType();
    if(objType == ENTITY_INTERACTIVE_OBJECT)
    {
        Unit * unit = (Unit *) _ent;
        if(unit->getOwningCommand()->isLocalPlayer())
        {
            _isLocalPlayersEmblem = true;
        }
    }
    else if(objType == UNIT_GROUP_INTERACTIVE_OBJECT ||
            objType == TESTER_INTERACTIVE_OBJECT)
    {
        _isLocalPlayersEmblem = true;
    }
    _canDrag = _isLocalPlayersEmblem;

    _background->setVisible(false);
    _icon = nullptr;
    //Perform the following on the main thread to ensure it occurs AFTER the sprite has been created
    globalEngagementManager->requestMainThreadFunction([this] () {
        double size = globalPlayerVariableStore->getVariable(EmblemSize);
        Size backgroundSize = _background->getContentSize();
        
        _icon = AnimatedSpriteProxy::createWithNode("emblem", Vec2(), _background->getSprite());
        _icon->retain();
        
//        _spriteToDefaultSizeScale =  Vec2(1,1);
        _spriteToDefaultSizeScale = Vec2(size / backgroundSize.width, size / backgroundSize.height);
        _background->setScale(_spriteToDefaultSizeScale);
        
        Color3B backgroundColor;
        
        if(_isLocalPlayersEmblem)
        {
            backgroundColor.r = globalPlayerVariableStore->getVariable(EmblemColorR);
            backgroundColor.g = globalPlayerVariableStore->getVariable(EmblemColorG);
            backgroundColor.b = globalPlayerVariableStore->getVariable(EmblemColorB);
        }
        else
        {
            backgroundColor.r = globalPlayerVariableStore->getVariable(TeamEmblemColorR);
            backgroundColor.g = globalPlayerVariableStore->getVariable(TeamEmblemColorG);
            backgroundColor.b = globalPlayerVariableStore->getVariable(TeamEmblemColorB);

        }
        _background->setColor(backgroundColor);
        if(getBackgroundLuminance(backgroundColor) > 0.5) //FIXME: check this
        {
            _icon->setColor(Color3B::BLACK);
        }
        
        _icon->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
        _background->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        _background->setZIndex(globalRandom->randomInt(0, 200000)); //WTF.... I think this guarentees that some emblems always render over others.... prevents z-fighting
        
        Size contentSize = _background->getSprite()->getContentSize();
        contentSize.height *= 1.5;
        _background->getSprite()->setContentSize(contentSize);
        
        updateScale();
        _visible = true;
        _background->setVisible(true);
        setEmblemType(_emblemType);
    });
    return true;
}

Emblem * Emblem::create(InteractiveObject * object, EMBLEM_TYPE type, Vec2 position)
{
    Emblem * newEmblem = new Emblem();
    if(newEmblem->init(object, type, position)) {
        globalAutoReleasePool->addObject(newEmblem);
        return newEmblem;
    }
    CC_SAFE_DELETE(newEmblem);
    return nullptr;
}


void Emblem::update(float deltaTime)
{
    if(_icon == nullptr)
    {
        return;
    }
    updateScale();
    if(_pendingAnimation.isChanged())
    {
        ANIMATION_TYPE animation = _pendingAnimation;
        _icon->setAnimation(animation, false);
        _pendingAnimation.clearChange();
    }
    if(_pendingStatusAdditions.isChanged())
    {
        std::vector<ANIMATION_TYPE> statuses = _pendingStatusAdditions.get();
        _pendingStatusAdditions.clearChange();
        for(auto status : statuses)
        {
            createStatusIcon(status);
        }
    }
    if(_pendingStatusRemovals.isChanged())
    {
        std::vector<ANIMATION_TYPE> statuses = _pendingStatusRemovals.get();
        _pendingStatusRemovals.clearChange();
        for(auto status : statuses)
        {
            removeStatusIconInternal(status);
        }
    }
    if(_pendingFlashing.isChanged())
    {
        bool flashing = _pendingFlashing;
        _pendingFlashing.clearChange();
        _background->setFlashing(flashing);
        _icon->setFlashing(flashing);
    }
}

void Emblem::setAnimationFromOrderType(ORDER_TYPE order)
{
    switch(order)
    {
        case MOVE_ORDER:
        case OCCUPY_ORDER:
        {
            setAnimation(EMBLEM_MOVE, false);
            break;
        }
        case OBJECTIVE_WORK_ORDER:
        case ENTITY_WORK_ORDER:
        {
            setAnimation(EMBLEM_DIG, false);
            break;
        }
        case ATTACK_POSITION_ORDER:
        {
            setAnimation(EMBLEM_ATTACK, false);
            break;
        }
        default:
        {
            setAnimation(EMBLEM_DEFAULT, false);
        }
    }
}

void Emblem::setAnimation(ANIMATION_TYPE animation, bool repeat)
{
    _pendingAnimation = animation;
}

void Emblem::setPosition(Vec2 position)
{
    _position = world->getAboveGroundPosition(position);
    _background->setPosition(_position);
}

void Emblem::setEmblemType(EMBLEM_TYPE type) 
{
    _emblemType = type;
    if(_emblemType == ORDER_EMBLEM)
    {
        _background->setAnimation(EMBLEM_BACKGROUND_CIRCLE, false);
    }
}

Vec2 Emblem::getPosition()
{
    return _position;
}

void Emblem::createStatusIcon(ANIMATION_TYPE statusType)
{
    if(_statusIcons.find(statusType) == _statusIcons.end())
    {
        EntityAnimationSet * set = globalEntityFactory->getEntityAnimationSet("emblem");
        double size = globalPlayerVariableStore->getVariable(EmblemSize);
        float xPosition = size * _statusIcons.size() + 1;
        
        Vec2 position(xPosition, _background->getSprite()->getContentSize().height);
        
        StatusIcon * status =  new StatusIcon();
        status->_icon = AnimatedSpriteProxy::createWithNode("emblem", position * ONE_OVER_WORLD_TO_GRAPHICS_SIZE, _background->getSprite());
        status->_background = AnimatedSpriteProxy::createWithNode("emblem", position * ONE_OVER_WORLD_TO_GRAPHICS_SIZE, _background->getSprite());

        status->_icon->retain();
        status->_background->retain();
        
        Rect backgroundSize = _background->getBoundingBox();
        status->_icon->setScale(Vec2(STATUS_SCALE, STATUS_SCALE));
        status->_background->setScale(Vec2(STATUS_SCALE, STATUS_SCALE));
        
        status->_icon->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
        status->_icon->setZIndex(2);
        status->_icon->setAnimation(statusType, false);
        
        status->_background->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
        
        Color3B backgroundColor;
        backgroundColor.r = globalPlayerVariableStore->getVariable(EmblemColorR);
        backgroundColor.g = globalPlayerVariableStore->getVariable(EmblemColorG);
        backgroundColor.b = globalPlayerVariableStore->getVariable(EmblemColorB);
        
        status->_background->setColor(backgroundColor);
        if(getBackgroundLuminance(backgroundColor) > 0.5)
        {
            status->_icon->setColor(Color3B::BLACK);
        }
        _statusIcons.insert(statusType, status);
        status->release();
    }
}

void Emblem::removeStatusIconInternal(ANIMATION_TYPE statusType)
{
    if(_statusIcons.find(statusType) != _statusIcons.end())
    {
        _statusIcons.at(statusType)->_icon->remove();
        _statusIcons.at(statusType)->_background->remove();        
        _statusIcons.at(statusType)->_icon->release();
        _statusIcons.at(statusType)->_background->release();
        _statusIcons.erase(statusType);
    }
}

void Emblem::addStatusIcon(ANIMATION_TYPE statusType)
{
    if(_statusIcons.find(statusType) == _statusIcons.end())
    {
        _pendingStatusAdditions.addValue(statusType);
    }
}

void Emblem::removeStatusIcon(ANIMATION_TYPE statusType)
{
    if(_statusIcons.find(statusType) != _statusIcons.end())
    {
        _pendingStatusRemovals.addValue(statusType);
    }
}

void Emblem::removeAllStatusIcons()
{
    _pendingStatusRemovals.addValue(EMBLEM_STATUS_STRESSED);
    _pendingStatusRemovals.addValue(EMBLEM_STATUS_NO_COMMAND);
    _pendingStatusRemovals.addValue(EMBLEM_STATUS_LOW_AMMO);
    _pendingStatusRemovals.addValue(EMBLEM_STATUS_CASUALTY_25);
    _pendingStatusRemovals.addValue(EMBLEM_STATUS_CASUALTY_50);
    _pendingStatusRemovals.addValue(EMBLEM_STATUS_CASUALTY_75);
}

double Emblem::getBackgroundLuminance(Color3B backgroundColor)
{
    double luminance = (backgroundColor.r / 255.0) * 0.2126;
    luminance += (backgroundColor.g / 255.0) * 0.7152;
    luminance += (backgroundColor.b / 255.0) * 0.0722;
    return luminance;
}

void Emblem::setDisabled(bool disabled)
{
    _disabled = disabled;
    if(_disabled)
    {
        _background->setOpacity(125);
    }
    else
    {
        _background->setOpacity(255);
    }
}

bool Emblem::isDisabled()
{
    return _disabled;
}

bool Emblem::canDrag()
{
    return _canDrag;
}

void Emblem::setCanDrag(bool canDrag)
{
    if(_isLocalPlayersEmblem)
    {
        _canDrag = canDrag;
    }
}

void setCancel(bool cancel)
{
    
}

bool isCanceled()
{
    return false;
}

void Emblem::setScale(float scale)
{
    _scale = scale;
}

void Emblem::setVisible(bool visable)
{
    _visible = visable;
    _background->setVisible(visable);
}

void Emblem::setColor(const Color3B & color)
{
    // calculate luminance here??
    _background->setColor(color);
}

void Emblem::setFlashing(bool flashing)
{
    _pendingFlashing = flashing;
}

void Emblem::updateScale()
{
    float newScale = (1 / globalPlayerLayer->getScale()) * _scale;
    _background->setScale(Vec2(newScale * _spriteToDefaultSizeScale.x, newScale * _spriteToDefaultSizeScale.y));
}

Rect Emblem::getBoundingBox()
{
    return _background->getBoundingBox();
}

void Emblem::emblemDragged(Vec2 position)
{
    if(!_canDrag)
    {
        return;
    }
    setPosition(position);
}

void Emblem::emblemDragEnded(Vec2 position)
{
    if(!_canDrag)
    {
        return;
    }
    setPosition(position);
}

void Emblem::emblemHoverEnter(Vec2 position)
{
    
}

void Emblem::emblemHoverLeave(Vec2 position)
{
    
}



bool Emblem::testCollisionWithCircle(Vec2 center, float radius)
{
    if(!_visible)
    {
        return false;
    }

    Rect collisionBounds = _background->getBoundingBox();
    if(collisionBounds.containsPoint(center))
    {
        return true;
    }
    
    float xClosest = std::clamp(center.x,collisionBounds.origin.x,collisionBounds.origin.x + collisionBounds.size.width);
    float yClosest = std::clamp(center.y,collisionBounds.origin.y,collisionBounds.origin.y + collisionBounds.size.height);
    
    float distX = center.x - xClosest;
    float distY = center.y - yClosest;
    
    if( ((distX * distX) + (distY * distY)) < (radius * radius))
        return true;
    
    return false;
}

void Emblem::removeFromGame()
{
    setVisible(false);
}
