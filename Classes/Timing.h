//
//  Timing.h
//  TrenchWars
//
//  Created by Paul Reed on 5/9/20.
//

#ifndef Timing_h
#define Timing_h

#include <stdio.h>
#include <time.h>
#include <chrono>
#include "cocos2d.h"

#define CURRENT_TIME std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now().time_since_epoch()).count()
#define CURRENT_TIME_MICRO std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count()
#define MS_TO_SECOND 1000

/**
 
 */
class MicroSecondSpeedTimer
{
private:
    std::function<void(long long int)> callBackFunction;
    long long int startTime;
    bool timerRunning;
public:
    MicroSecondSpeedTimer(long long int startTime,const std::function<void(long long int)> & timerCompleteCallBack);
    MicroSecondSpeedTimer(const std::function<void(long long int)> & timerCompleteCallBack);
    ~MicroSecondSpeedTimer();
    
    void stop();
};

#define FUNCTION_TIMER auto& _func_ = __func__; MicroSecondSpeedTimer _timer_([=] (long long time) {printf("%s took %lld \n", _func_, time);});
#endif /* Timing_h */
