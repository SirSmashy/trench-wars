//
//  DebugObjectMenu.cpp
//  TrenchWars
//
//  Created by Paul Reed on 2/22/21.
//

#include "DebugObjectMenu.h"
#include "Infantry.h"
#include "Unit.h"
#include "EngagementManager.h"
#include "InteractiveObjectUtils.h"
#include "CollisionGrid.h"

DebugObjectMenu::DebugObjectMenu() : GameMenu()
{
    
}
bool DebugObjectMenu::init()
{
    if(GameMenu::init())
    {
        _followObject = false;
        _object =  nullptr;
        setVisible(false);
        setAnchorPoint(Vec2::ANCHOR_MIDDLE_TOP);
        return true;
    }
    return false;
}


DebugObjectMenu * DebugObjectMenu::create()
{
    DebugObjectMenu * newMenu = new DebugObjectMenu();
    if(newMenu->init())
    {
        newMenu->autorelease();
        return newMenu;
    }
    
    CC_SAFE_DELETE(newMenu);
    return nullptr;
}


void DebugObjectMenu::setObject(InteractiveObject * object, bool followObject)
{
    if(!isVisible())
    {
        return;
    }
    if(_object != nullptr)
    {
        // Don't change the object if we are following it, unless we should also follow the new object
        if(_followObject && !followObject)
        {
            return;
        }
        if(_object->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
        {
            Entity * ent = dynamic_cast<Entity *>(_object);
            ent->clearDebugMe();
        }
        _object->release();
    }
    _object = object;
    _followObject = followObject;
    if(_object != nullptr)
    {
        if(_object->getInteractiveObjectType() == EMBLEM_INTERACTIVE_OBJECT)
        {
            Emblem * emblem = (Emblem *) object;
            InteractiveObject * owner = emblem->getCommandableObject();
            _object = owner;
        }
        if(_object->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
        {
            Entity * ent = dynamic_cast<Entity *>(_object);
//            if(ent->getEntityType() == CREW_WEAPON)
//            {
//                CrewWeapon * weapon = dynamic_cast<CrewWeapon *> (_object);
//                if(weapon->getOperator() != nullptr)
//                {
//                    _object = weapon->getOperator();
//                }
//            }
            globalEngagementManager->testEntity = ent;
            ent->setDebugMe();
        }
        _object->retain();
    }
}

void DebugObjectMenu::setVisible(bool visible)
{
//    LOG_DEBUG("Debug menu: %d \n", visible);
    GameMenu::setVisible(visible);
}

void DebugObjectMenu::clearObject()
{
    if(_object != nullptr)
    {
        if(_object->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
        {
            Entity * ent = dynamic_cast<Entity *>(_object);
            ent->clearDebugMe();
        }
        _object->release();
    }
    _object = nullptr;
    _followObject = false;
}


void DebugObjectMenu::update(float deltaTime)
{
    clear();
    
    if(isVisible() && _object != nullptr)
    {
        if(_object->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
        {
            Entity * entity = dynamic_cast<Entity *>(_object);
            if(_followObject && (entity->getEntityType() == INFANTRY || entity->getEntityType() == CREW_WEAPON))
            {
                Vec2 position = InteractiveObjectUtils::getPositionForObject(_object);
                Vec2 screenLocation = globalPlayerLayer->convertToWorldSpace(world->getAboveGroundPosition(position)  * WORLD_TO_GRAPHICS_SIZE);
                setPosition(screenLocation);
            }
            entity->populateDebugPanel(this, getPosition());

//            globalEngagementManager->requestMainThreadFunction([entity, this] {
//                Vec2 loc = getPosition();
//                LOG_DEBUG("Draw at %.0f %.0f \n", loc.x, loc.y);
//            });
        }
    }
    GameMenu::update(deltaTime);
}
