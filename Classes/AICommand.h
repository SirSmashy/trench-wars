#ifndef __AI_COMMAND_H__
#define __AI_COMMAND_H__

//
//  AICommand.h
//  TrenchWars
//
//  Created by Paul Reed on 1/17/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#include "cocos2d.h"
#include "Command.h"
#include "AIStrategy.h"

class MapSectorAnalysis;

class UnitLocationReport : public Ref
{
public:
    MapSectorAnalysis * analysis;
    Vector<MapSectorAnalysis *> enemyStrengthProjection;
    int frameWhenReported;
};


class StrategyTypeUnit : public Ref
{
public:
    AI_GOAL_TYPE _typeOfStrategy;
    Vector<AIStrategy *> strategies;
    float averageValue;
public:
    StrategyTypeUnit(AI_GOAL_TYPE type);
};




class AICommand : public Command
{
public:
    // TODO FIX make these private later
    Map<int, UnitLocationReport *> enemyUnitsLocationReportMap;
    Map<int, UnitLocationReport *> friendyUnitsLocationReportMap;
    
    int resetEnemySetsCounter;
    Map<int, MapSectorAnalysis *> sectorAnalyses;
    Map<int, AIStrategy *> strategyForUnitMap;
    
    std::mutex strategyMutex;
    Vector<AIStrategy *> strategies;
    Vector<AIStrategy *> strategiesThatWantUnits; //strategies that are important enough that units should start being allocated to them
    Vector<AIStrategy *> strategiesToUpdate; //those strategies in need of being updated (newly created, finished a stage, sectros in strat changed...)
    Map<int,StrategyTypeUnit *> strategyTypeUnits; //Holds the set of
    Vector<Unit *> unitsNotInStrategy;
    
    std::unordered_map<int, Vector<MapSectorAnalysis *> *> sectorsToAnalyzeTypeUnits;
    
    int unitsNeededForMinimum; //how many units are needed so that every plan with an execution has at least the minimum number of units
    int totalValue;
    int queryAboutGoalsCounter;
    
    int strategyIDCounter;
    
    std::vector<std::string> executionStateNames;
    std::vector<std::string> goalTypeNames;
    FILE * aiFilePointer;
    
    std::mutex _strategyLockMutex;
private:
    void queryAboutGoals();
    
    void updateTypeUnitAverages();
    void drawGoalInfo(AIStrategy * strategy);
    
    float getStrategyDeleteThreshold(StrategyTypeUnit * typeUnit);
    Unit *  selectBestUnitForStrategy(AIStrategy * strategy);
    void addUnitsToExistingExecutions();
    void deleteStrategy(AIStrategy * strategy);
    
    void considerCreationOfNewStrategyAtMapSector(MapSectorAnalysis * sector, AI_GOAL_TYPE strategyType, float value, Vector<AIStrategy *> stratsToConsider);
    
    MapSectorAnalysis *  getMapSectorAnalysisForSector(MapSector * sector);

    void createStrategyUpdateRequests();
    void evaluateSectorAnalysis(MapSectorAnalysis * sector, AI_GOAL_TYPE goalType, float value);


public:
    ////////// Generation functions //////////
    
    AICommand();
    virtual void init(Team * owningTeam, CommandDefinition * definition);
    static AICommand * create(Team * owningTeam, CommandDefinition * definition);
    
    virtual void addCommandPost(CommandPost * post);
    virtual void addUnit(Unit * unit);
    virtual void removeUnit(Unit * unit);
    
    
    void unitCompletedOrder(Unit * entCompleting, Order * order, bool failed);
    void enemyUnitSpotted(Unit * enemy);
    void enemyCommandPostSpotted(CommandPost *enemy);
    void enemyCanEngageSector(MapSector * sector, Human * enemy);
    void enemyArtilleryInSector(MapSector * sector);

    ////////
    virtual void query(float deltaTime);
    void removeUnitFromStrategy(Unit * unit);
    void changeToSectorAnalysis(MapSectorAnalysis * sector, AI_GOAL_TYPE goalType);
    void performAnalysisOfSectorsForGoalType(AI_GOAL_TYPE goalType);
        
    AIStrategy * strategyForUnit(Unit * unit);
    
    ///////// low level sector report functions  //////////////////
    
    
};

#endif //__AI_COMMAND_H__

