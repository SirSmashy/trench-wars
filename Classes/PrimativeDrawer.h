#ifndef __PRIMATIVE_DRAWER_H__
#define __PRIMATIVE_DRAWER_H__

//
//  PrimativeDrawer.h
//  TrenchWars
//
//  Created by Paul Reed on 11/16/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#include "cocos2d.h"

USING_NS_CC;

class PrimativeDrawer : public DrawNode
{
CC_CONSTRUCTOR_ACCESS :

    PrimativeDrawer();
    ~PrimativeDrawer();

    virtual bool init(bool addToPlayeLayer);
    
public:
    static PrimativeDrawer * create(bool addToPlayeLayer = true);
    void clear();
    void remove();
    void drawBounds(Rect bounds, Color4F color, int lineRadius);
    void drawSolidRectWithCornerRadius(Rect bounds, Color4F color, int cornerRadius, int vertsPerCorner = 25);
    void drawTriangle(Vec2 position, float triangleWidth, Color4F color);
};

#endif //__PRIMATIVE_DRAWER_H__
