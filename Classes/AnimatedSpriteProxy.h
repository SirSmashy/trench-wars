//
//  AnimatedSpriteProxy.hpp
//  trench-wars
//
//  Created by Paul Reed on 10/3/23.
//

#ifndef AnimatedSpriteProxy_h
#define AnimatedSpriteProxy_h


#include "cocos2d.h"
#include "Refs.h"
#include "AnimatedSprite.h"
#include "EntityDefinitions.h"
#include "ChangeTrackingVariable.h"
#include "VectorMath.h"
#include "CocosProxy.h"
#include "SpriteBatchGroup.h"

USING_NS_CC;

class PhysicalEntity;

class AnimatedSpriteProxy : public CocosProxy
{
    AnimatedSprite * _sprite;
    PrimitiveChangeTrackingVariable<Vec2> _pendingPosition;
    PrimitiveChangeTrackingVariable<Vec2> _pendingScale;
    SimpleChangeTrackingVariable<int> _pendingOpacity;
    SimpleChangeTrackingVariable<bool> _pendingFlashing;
    SimpleChangeTrackingVariable<ANIMATION_TYPE> _pendingAnimation;
    SimpleChangeTrackingVariable<bool> _pendingAnimationRepeat;
    SimpleChangeTrackingVariable<DIRECTION> _pendingAnimationDirection;
    SimpleChangeTrackingVariable<bool> _pendingVisibility;
    SimpleChangeTrackingVariable<Color3B> _pendingColor;
    SimpleChangeTrackingVariable<AffineTransform> _pendingTransform;
    SimpleChangeTrackingVariable<int> _pendingZIndex;
    SimpleChangeTrackingVariable<float> _pendingZPosition;
    SimpleChangeTrackingVariable<Size> _pendingSpriteSize;
    SimpleChangeTrackingVariable<Vec2> _pendingAnchorPoint;
    
    Node * _nodeToJoin;
    SpriteBatchGroup * _batchGroupToJoin;
    
    bool _needsUpdate;
    bool _alwaysUpdate;
    bool _positionUnderground;
    bool _updateZPositionUponMove;
    double _maxPosition;
    std::string _spriteName;
    Vec2 _positionOffset;
    float _zPositionOffset;
    int _zIndexOffset;
    float _currentZPosition;
    
    
    EntityAnimationSet * _animationSet;
    
    AnimatedSpriteProxy();
    void init(const std::string & spriteName, Vec2 position, bool alwaysUpdate);
    void init(const std::string & spriteName, Vec2 position, Node * nodeToJoin, bool createImmediately);

    
    void setNeedsUpdate();
public:
    
    ~AnimatedSpriteProxy();
    static AnimatedSpriteProxy * create(const std::string & spriteName, Vec2 position, bool alwaysUpdate);
    static AnimatedSpriteProxy * createWithNode(const std::string & spriteName, Vec2 position, Node * nodeToJoin);

    
    virtual void createCocosObject();
    
    const std::string & getSpriteName() {return _spriteName;}
    virtual Sprite * getSprite() {return _sprite;}
    virtual ProxyType getProxyType() {return ANIMATED_SPRITE_PROXY;}
    
    EntityAnimationSet * getAnimationSet() {return _animationSet;}
    
    
    void setPosition(Vec2 position);
    void setScale(Vec2 scale);
    void setAnimation(ANIMATION_TYPE animation, bool repeat, DIRECTION direction = NONE);
    void setOpacity(int opacity);
    void setVisible(bool visible);
    void setColor(const Color3B & color);
    // A note on zIndex vs zPosition:
    // zPosition ALWAYS takes precendance over zIndex
    // A Cocos2d node with a larger zPosition will render on top of a node with a smaller zPosition, regardless of zIndex
    // However, transparency is based upon zIndex.... maybe
    void setZIndex(int index);
    void setPositionZ(float zPosition);
    void setAdditionalTransform(AffineTransform & transform);
    void setPositionOffset(Vec2 offset);
    void setZIndexOffset(int offset);
    void setPositionZOffset(float offset);
    void setFlashing(bool flashing);
    void setAnchorPoint(Vec2 anchorPoint);
    void setSpriteSize(Size spriteSize);
    void setUnderground(bool underground);
    
    ANIMATION_TYPE getCurrentAnimationState();
    DIRECTION       getCurrentAnimationDirection();
    
    Rect getBoundingBox();
    Size getContentSize();
    
    bool spriteExists() {return _sprite != nullptr;}
    bool needsUpdate() {return _needsUpdate;}
    bool updateCocosObject();
    
    void remove();
};
#endif /* AnimatedSpriteProxy_h */
