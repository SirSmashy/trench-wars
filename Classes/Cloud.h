//
//  Cloud.hpp
//  TrenchWars
//
//  Created by Paul Reed on 7/30/24.
//

#ifndef Cloud_h
#define Cloud_h

#include "Entity.h"
#include "ParticleSystemProxy.h"

class MapSector;

class Cloud : public Entity
{
    Vec2 _position;
    Vec2 _velocity;
    
    float _initialBurstTimeLeft;
    float _burstRadiusChangePerSecond;

    float _radius;
    float _radiusSquared;
    float _density;

    float _enduringRadiusChangePerSecond;
    float _densityChangePerSecond;
    float _densityToAlphaMultiple;
    float _lifeSpan;
    
    
    std::unordered_set<MapSector *> _sectorsInCloud;
    ParticleSystemProxy * _particleProxy;
    virtual bool init(CloudDefinition * definition, Vec2 position, float initialRadius);
        
    CC_CONSTRUCTOR_ACCESS :
    
    Cloud();
    ~Cloud();
    
public:
    static Cloud * create(CloudDefinition * definition, Vec2 position, float initialRadius);
    static Cloud * create(const std::string & definition, Vec2 position, float initialRadius);
    static Cloud * create();
    virtual void postLoad();
    virtual ENTITY_TYPE getEntityType() const {return CLOUD_ENTITY;}
    
    virtual bool shouldQuery() {return false;}
    virtual bool shouldAct() {return true;}
    
    virtual void query(float deltaTime);
    virtual void act(float deltaTime);

    virtual void setPosition(Vec2 position);
    Vec2 getPosition() {return _position;}
    
    void changeRadius(float amount);
    void changeAlpha(float amount);
    
    float getDensity() {return _density;}
    float getRadius() {return _radius;}
    
    void removeFromGame();
    
    void debugClick();
    
    virtual bool testCollisionWithPoint(Vec2 point);

    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
    
    virtual void setDebugMe();

};

#endif /* Cloud_h */
