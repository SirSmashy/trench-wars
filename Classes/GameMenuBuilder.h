//
//  GameMenuBuilder.h
//  TrenchWars
//
//  Created by Paul Reed on 9/7/20.
//

#ifndef EntityMenu_h
#define EntityMenu_h

#include "ui/CocosGUI.h"
#include "cocos2d.h"
#include "GameMenu.h"
#include "Unit.h"
#include "UnitGroup.h"
#include "Objective.h"
#include "Tester.h"
#include "VisibilityTester.h"
#include "InteractiveObject.h"


USING_NS_CC;
using namespace cocos2d::ui;

class GameMenuBuilder
{
public:
    static void populateFromUnit(GameMenu * menu, Unit * unit, Vec2 location);
    static void populateFromUnitGroup(GameMenu * menu, UnitGroup * group, Vec2 location);
    static void populateFromObjective(GameMenu * menu, Objective * objective, Vec2 location);
    static void populateFromTester(GameMenu * menu, Tester * tester);

    static void populateForObject(GameMenu * menu, InteractiveObject * object, Vec2 location);
    static void populateTestMenu(GameMenu * menu, Vec2 location);
    
    static void populateSpawnMenu(GameMenu * menu, Vec2 location);
    static void populateClickActionMenu(GameMenu * menu, Vec2 location);
};


#endif /* EntityMenu_h */
