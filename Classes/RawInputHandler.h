//
//  RawInputHandler.h
//  TrenchWars
//
//  Created by Paul Reed on 8/16/20.
//

#ifndef RawInputHandler_h
#define RawInputHandler_h

#include "cocos2d.h"
#include "InputAction.h"
#include "InteractiveObjectHighlighter.h"
#include "InteractiveObject.h"

USING_NS_CC;
using namespace cocos2d::ui;

#define DRAG_THRESHOLD 10

class InputActionHandler;
class GameCamera;

class RawInputHandler : public Ref
{
private:

    Vector<InputAction *> _inputActions;    
    GameCamera * _camera;
    EventListenerMouse * mouseListener;

    InputActionHandler * _actionHandler;
    bool _gestureStarted;
    
    Vec2 lastMouseLocation;
    float lastMouseScroll;
    
    EventMouse::MouseButton gestureButton;
    Vec2 gestureStartLocationGame;
    Vec2 lastGestureLocationScreen;
    Vec2 gestureStartLocationScreen;
    InteractiveObject * _gestureObject;
    std::unordered_map<ENTITY_ID, INTERACTIVE_OBJECT_TYPE> _hoveredObjects;

    InteractiveObjectHighlighter * _hoveredHighlighter;

    InputChord currentChord;
    InputAction * actionInProgress;
    
    bool _drawKeyPressed;
    bool _gestureIsDrag;
    
    void recognizeAction();
    void doAction(InputAction * action, bool endAction);
    void setupUserInput();
    
    void updateObjectHovering(Vec2 point);
    void handleObjectHoverEnter(InteractiveObject * ent, Vec2 gameLocation);
    void handleObjectHoverLeave(InteractiveObject * ent, Vec2 gameLocation);


public:
    RawInputHandler(InputActionHandler * actionHandler, GameCamera * camera);
        
    void onPointerDown(EventMouse * event);
    void onPointerMove(EventMouse * event);
    void onPointerUp(EventMouse * event);
    void onPointerScroll(EventMouse * event);
    
    void onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event);
    void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event);
    
    void addInputAction(InputAction * action);
    
    InteractiveObject * getObjectAtPoint(Vec2 point);

    
    void remove();
};
#endif /* RawInputHandler_h */
