//
//  VisibilityTester.hpp
//  TrenchWars
//
//  Created by Paul Reed on 7/25/24.
//

#ifndef VisibilityTester_h
#define VisibilityTester_h

#include "Tester.h"
#include "Emblem.h"
#include "PrimativeDrawer.h"
#include "List.h"
#include "PathBuildingNode.h"
#include "Path.h"
#include "DebugEntity.h"
#include "LineDrawer.h"
#include "CellHighlighter.h"
#include "Human.h"

class CollisionCell;


class VisibilityTester : public Tester
{
protected:
    PrimativeDrawer * _primativeDrawer;
    LineDrawer * _lineDrawer;
    LineDrawer * _sectorLineDrawer;
        
    Emblem * _startEmblem;
    Emblem * _endEmblem;
    
    Vec2 startPosition;
    Vec2 endPosition;

    
    CellHighlighter * _cellHighlighter;
    CellHighlighter * _cellHighlighter2;
    
    std::unordered_set<CollisionCell *> _visibleCells;
    
    double lastUpdateTime;

    VisibilityTester();
    void init(Vec2 position);
    
    bool doCellLineTest(Vec2 start, Vec2 endPosition);
    bool doCellLineTestFull(Vec2 start, Vec2 endPosition);    
    void draw();
    
    void doLineVisionTest();

    
public:
    static VisibilityTester * create(Vec2 position);
    virtual Vec2 getPosition() {return Vec2();}
    
    bool testCollisionWithPoint(Vec2 point);
    virtual void update();
    
    Emblem * getEmblemAtPoint(Vec2 location);
    virtual void removeFromGame();
};

#endif /* VisibilityTester_hpp */
