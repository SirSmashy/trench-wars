#ifndef __COMMAND_H__
#define __COMMAND_H__

//
//  Command.h
//  TrenchWars
//
//  Created by Paul Reed on 1/29/12.
//  Copyright (c) 2012. All rights reserved.
//

#include "cocos2d.h"
#include "Refs.h"
#include "ScenarioFactory.h"
#include "Unit.h"
#include "CommandPost.h"
#include "Objective.h"
#include <cereal/archives/binary.hpp>

USING_NS_CC;

class GameEventController;
class CollisionCell;
class Team;

/* Command
 * This class represents a command in a military sense. A collection of units, formations, etc under the control of a single individual (in this case either a human or AI)
 */

class Command : public ComparableRef
{
protected:
    Team * _owningTeam;
    
    Vector<Unit *> _units;
    Vector<CommandPost *> _commandPosts;
    CommandPost * _primaryCommandPost;
    std::mutex _entitiesMutex;
    std::mutex _ordersMutex;

    Map<ENTITY_ID, Order *> _orders;

    int     _commandID;
    float _supplyAccumulationRate;
    float _pendingSupply;
    Vec2  _spawnPosition;
    
    PrimativeDrawer * _primativeDrawer;
    
    CC_CONSTRUCTOR_ACCESS :
    
    Command();
    void init(Team * owningTeam, CommandDefinition * definition);    
    void doSpawnOrderForUnit(Unit * unit);

public:
    DEBUG_SPAWN_MODE _debugSpawnMode;

    static Command * create(Team * owningTeam, CommandDefinition * definition);
    void spawnInitialUnits(CommandDefinition * commandDefinition);
    
    void query(float deltaTime);
    
    CommandPost * getPrimaryCommandPost() {return _primaryCommandPost;}
    
    Vec2 getSpawnPosition() {return _spawnPosition;}
    int getCommandID() {return _commandID;}
    
    Team * getOwningTeam() {return _owningTeam;}
    int getTeamId();
    
    // Command Posts
    const Vector<CommandPost *> & getCommandPosts() {return _commandPosts;}
    virtual void addCommandPost(CommandPost * post);
    virtual void removeCommandPost(CommandPost * post);
    
    // Units
    virtual void addUnit(Unit * unit);
    virtual void removeUnit(Unit * unit);
    virtual const Vector<Unit *> & getUnits() {return _units;}
    
    // Orders
    virtual void addOrder(Order * order);
    Order * getOrderWithId(ENTITY_ID orderId);
    virtual void unitCompletedOrder(Unit * unit, Order * order, bool failed);
    void removeOrder(Order * order);

    // Formation
    void spawnFormation(const std::string & formationName, Vec2 location);
    
    // Location queries
    void unitsInRect(Rect rect, Vector<Unit *> & units);
    
    // CommandPost location queries
    CommandPost * getCommandPostAtPoint(Vec2 point);
    CommandPost * getNearestCommandPost(Vec2 position, bool onlyLinked = true);
    bool isPositionInCommandArea(Vec2 position);
    bool isPositionValidForNewCommandPost(Vec2 position);
    Vec2 getValidCommandPostPositionTowardPosition(Vec2 position);
    void updateCommandPostLinking();
    
    // Ammo
    int withdrawAmmo(PhysicalEntity * requestingEntity, int requestedAmount);
    int getTotalAmmo();
    bool isLocalPlayer();
    
    ///////
    void saveBasicCommandInfoToArchive(cereal::BinaryOutputArchive & archive);
    void saveOrdersToArchive(cereal::BinaryOutputArchive & archive);
    void saveToArchive(cereal::BinaryOutputArchive & archive);
    
    void loadBasicCommandInfoFromArchive(cereal::BinaryInputArchive & archive);
    void loadOrdersFromArchive(cereal::BinaryInputArchive & archive);
    void loadFromArchive(cereal::BinaryInputArchive & archive);
    
    ///////////////////
    void shutdown();
};


#endif //__COMMAND_H__
