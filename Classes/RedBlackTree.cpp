//
//  RedBlackTree.cpp
//  Trench Wars
//
//  Created by Paul Reed on 1/29/14.
//  Copyright (c) 2014 Paul Reed. All rights reserved.
//

#include "RedBlackTree.h"

USING_NS_CC;


RedBlackTree::RedBlackTree()
{
    NIL = new RedBlackNode();
    NIL->value = -1;
    NIL->red = false;
    NIL->leftChild = NIL;
    NIL->rightChild = NIL;
    
    root = NIL;
    minimumValue = NIL;
    maximumValue = NIL;
}

RedBlackTree::~RedBlackTree()
{
    NIL->release();
}


void RedBlackTree::leftRotation(RedBlackNode * x)
{
    RedBlackNode * y = x->rightChild;
    x->rightChild = y->leftChild;
    if(y->leftChild != NIL)
        y->leftChild->parent = x;
    
    if(y != NIL)
        y->parent = x->parent;
    
    if(x->parent == NIL)
        root = y;
    else if(x->parent->leftChild == x)
        x->parent->leftChild = y;
    else
        x->parent->rightChild = y;
    
    y->leftChild = x;
    if(x != NIL)
        x->parent = y;
}

void RedBlackTree::rightRotation(RedBlackNode * x)
{
    RedBlackNode * y = x->leftChild;
    x->leftChild = y->rightChild;
    if(y->rightChild != NIL)
        y->rightChild->parent = x;
    
    if(y != NIL)
        y->parent = x->parent;
    
    if(x->parent == NIL)
        root = y;
    else if(x->parent->leftChild == x)
        x->parent->leftChild = y;
    else
        x->parent->rightChild = y;
    y->rightChild = x;
    if(x != NIL)
        x->parent = y;
}

void RedBlackTree::removeNode(RedBlackNode * nodeToRemove)
{
    RedBlackNode *x, *replacementNode;
    
    // If the node to be removed is the minimum value, find the new minimum value after deletion
    if(nodeToRemove == minimumValue)
    {
        if(nodeToRemove->leftChild != NIL) //Only happens with equal values
        {
            while(minimumValue->leftChild != NIL)
            {
                minimumValue = minimumValue->leftChild;
            }
        }
        else if(nodeToRemove->rightChild != NIL)
        {
            minimumValue = nodeToRemove->rightChild;
            while(minimumValue->leftChild != NIL)
            {
                minimumValue = minimumValue->leftChild;
            }
        }
        else if(nodeToRemove->parent != NIL)
        {
            minimumValue = nodeToRemove->parent;
        }
        else minimumValue = NIL;
    }
    // If the node to be removed is the maximum value, find the new maximum value after deletion
    if(nodeToRemove == maximumValue)
    {
        if(nodeToRemove->rightChild != NIL) // Only happens when nodes have an equal value
        {
            while(maximumValue->rightChild != NIL)
                maximumValue = maximumValue->rightChild;
        }
        else if(nodeToRemove->leftChild != NIL)
        {
            maximumValue = nodeToRemove->leftChild;
            while(maximumValue->rightChild != NIL)
                maximumValue = maximumValue->rightChild;
        }
        else if(nodeToRemove->parent != NIL)
            maximumValue = nodeToRemove->parent;
        else maximumValue = NIL;
    }
    
    // Perform the actual deletion of the node
    // First find a node to replace the node being deleted
    if (nodeToRemove->leftChild == NIL || nodeToRemove->rightChild == NIL)
    {
        /* replacementNode has a NIL node as a child */
        replacementNode = nodeToRemove;
    }
    else
    {
        /* find tree successor with a NIL node as a child */
        replacementNode = nodeToRemove->rightChild;
        while (replacementNode->leftChild != NIL)
        {
            replacementNode = replacementNode->leftChild;
        }
    }
    
    /* x is replacementNode's only child */
    if (replacementNode->leftChild != NIL)
        x = replacementNode->leftChild;
    else
        x = replacementNode->rightChild;
    
    /* remove replacementNode from the parent chain */
    x->parent = replacementNode->parent;
    if (replacementNode->parent != NIL)
    {
        if (replacementNode == replacementNode->parent->leftChild)
            replacementNode->parent->leftChild = x;
        else
            replacementNode->parent->rightChild = x;
    }
    else
        root = x;
    
    if (replacementNode->red == false)
    {
        while (x != root && x->red == false)
        {
            if (x == x->parent->leftChild)
            {
                RedBlackNode *w = x->parent->rightChild;
                if (w->red == true)
                {
                    w->red = false;
                    x->parent->red = true;
                    leftRotation(x->parent);
                    w = x->parent->rightChild;
                }
                if (w->leftChild->red == false && w->rightChild->red == false)
                {
                    w->red = true;
                    x = x->parent;
                }
                else
                {
                    if (w->rightChild->red == false)
                    {
                        w->leftChild->red = false;
                        w->red = true;
                        rightRotation(w);
                        w = x->parent->rightChild;
                    }
                    w->red = x->parent->red;
                    x->parent->red = false;
                    w->rightChild->red = false;
                    leftRotation(x->parent);
                    x = root;
                }
            }
            else
            {
                RedBlackNode *w = x->parent->leftChild;
                if (w->red == true)
                {
                    w->red = false;
                    x->parent->red = true;
                    rightRotation(x->parent);
                    w = x->parent->leftChild;
                }
                if (w->rightChild->red == false && w->leftChild->red == false)
                {
                    w->red = true;
                    x = x->parent;
                }
                else
                {
                    if (w->leftChild->red == false)
                    {
                        w->rightChild->red = false;
                        w->red = true;
                        leftRotation(w);
                        w = x->parent->leftChild;
                    }
                    w->red = x->parent->red;
                    x->parent->red = false;
                    w->leftChild->red = false;
                    rightRotation(x->parent);
                    x = root;
                }
            }
        }
        x->red = false;
    }
    
    if(replacementNode != nodeToRemove)
    {
        nodeToRemove->data = replacementNode->data;
        nodeToRemove->value = replacementNode->value;
        dataToNodeDictionary.erase(replacementNode->data->uID());
        dataToNodeDictionary.insert(nodeToRemove->data->uID(), nodeToRemove); //update the data to node dictionary
    }
    else
    {
        dataToNodeDictionary.erase(nodeToRemove->data->uID());
    }
}

RedBlackNode * RedBlackTree::findNode(ComparableRef * object, double compareValue)
{
    RedBlackNode * search = root;
    while(search != NIL)
    {
        if(compareValue > search->value)
        {
            search = search->rightChild;
        }
        else if(compareValue == search->value && *(search->data) == *object)
        {
            return search;
        }
        else
        {
            search = search->leftChild;
        }
    }
    return nullptr;
}

void RedBlackTree::addObject(ComparableRef * object, double compareValue)
{
    RedBlackNode * nodeToAdd = dataToNodeDictionary.at(object->uID());
    if(nodeToAdd != nullptr)
    {
        // Remove the old instance of the object
        removeNode(nodeToAdd);
    }
    
    nodeToAdd = new RedBlackNode();
    nodeToAdd->data = object;
    nodeToAdd->value = compareValue;
    nodeToAdd->leftChild = NIL;
    nodeToAdd->rightChild = NIL;
    nodeToAdd->red = true;
    nodeToAdd->parent = NIL;
    dataToNodeDictionary.insert(object->uID(), nodeToAdd);
    nodeToAdd->release();
    
    if(root == NIL)
    {
        root = nodeToAdd;
        nodeToAdd->parent = NIL;
        root->red = false;
        minimumValue = nodeToAdd;
        maximumValue = nodeToAdd;
        return;
    }
    
    //step one, standard binary tree insert
    RedBlackNode * search = root;
    while(true)
    {
        if(nodeToAdd->value > search->value)
        {
            if(search->rightChild == NIL)
            {
                search->rightChild = nodeToAdd;
                nodeToAdd->parent = search;
                break;
            }
            search = search->rightChild;
        }
        else
        {
            if(search->leftChild == NIL)
            {
                search->leftChild = nodeToAdd;
                nodeToAdd->parent = search;
                break;
            }
            search = search->leftChild;
        }
    }
    
    if(minimumValue == NIL || minimumValue->value >= nodeToAdd->value)
        minimumValue = nodeToAdd;
    if(maximumValue == NIL || maximumValue->value <= nodeToAdd->value)
        maximumValue = nodeToAdd;
    
    RedBlackNode * x = nodeToAdd;
    RedBlackNode * y;
    
    
    while ( (x != root) && (x->parent->red == true) )
    {
        if ( x->parent == x->parent->parent->leftChild )
        {
            /* If x's parent is a left, y is x's right 'uncle' */
            y = x->parent->parent->rightChild;
            if ( y->red == true )
            {
                /* case 1 - change the colours */
                x->parent->red = false;
                y->red = false;
                x->parent->parent->red = true;
                /* Move x up the tree */
                x = x->parent->parent;
            }
            else
            {
                /* y is a black node */
                if ( x == x->parent->rightChild )
                {
                    /* and x is to the right */
                    /* case 2 - move x up and rotate */
                    x = x->parent;
                    leftRotation(x);
                }
                /* case 3 */
                x->parent->red = false;
                x->parent->parent->red = true;
                rightRotation(x->parent->parent);
            }
        }
        else
        {
            /* If x's parent is a left, y is x's right 'uncle' */
            y = x->parent->parent->leftChild;
            if ( y->red == true )
            {
                /* case 1 - change the colours */
                x->parent->red = false;
                y->red = false;
                x->parent->parent->red = true;
                /* Move x up the tree */
                x = x->parent->parent;
            }
            else
            {
                /* y is a black node */
                if ( x == x->parent->leftChild )
                {
                    /* and x is to the right */
                    /* case 2 - move x up and rotate */
                    x = x->parent;
                    rightRotation(x);
                }
                /* case 3 */
                x->parent->red = false;
                x->parent->parent->red = true;
                leftRotation(x->parent->parent);
            }
        }
    }
    /* Colour the root black */
    root->red = false;
}

void RedBlackTree::clear()
{
    root = NIL;
    minimumValue = NIL;
    maximumValue = NIL;
    dataToNodeDictionary.clear();
}

void RedBlackTree::removeObject(ComparableRef * object)
{
    RedBlackNode * node = dataToNodeDictionary.at(object->uID());
    if(node != nullptr)
        removeNode(node);
}

ComparableRef * RedBlackTree::findAndRemoveMinimum()
{
    ComparableRef * data = minimumValue->data;
    if(data != nullptr)
    {
        removeNode(minimumValue);
    }

    return data;
}

ComparableRef * RedBlackTree::findMinimum()
{
    return minimumValue->data;
}

ComparableRef * RedBlackTree::findMaximum()
{
    return maximumValue->data;
}

RedBlackNode *  RedBlackTree::findMinimumNode()
{
    return minimumValue;
}


RedBlackNode * RedBlackTree::findMaximumNode()
{
    return maximumValue;
}

RedBlackNode * RedBlackTree::findLessThan(RedBlackNode * node)
{
    RedBlackNode * search = node;

    //1st case: This node has a left child node
    //find the largest child of the left child node by continually finding the right child node
    if(search->leftChild != NIL)
    {
        search = search->leftChild;
        while(search->rightChild != NIL)
        {
            search = search->rightChild;
        }
        return search;
    }
    else
    {
        //2nd case
        //if the node is the left child of its parent, search upward looking for a right child
        while(search->parent != NIL)
        {
            if(search->parent->rightChild == search)
            {
                return search->parent;
            }
            search = search->parent;
        }
    }
    
    return nullptr;
}

RedBlackNode * RedBlackTree::findGreaterThan(RedBlackNode * node)
{
    RedBlackNode * search = node;
    
    //1st case: This node has a right child node
    //find the smallest child of the right child node by continually finding the left child node
    if(search->rightChild != NIL)
    {
        search = search->rightChild;
        while(search->leftChild != NIL)
        {
            search = search->leftChild;
        }
        return search;
    }
    else
    {
        //2nd case
        //if the node is the right child of its parent, search upward looking for a left child
        while(search->parent != NIL)
        {
            if(search->parent->leftChild == search)
            {
                return search->parent;
            }
            search = search->parent;
        }
    }
    
    return nullptr;
}


ComparableRef *  RedBlackTree::findLessThanObject(ComparableRef * object)
{
    RedBlackNode * node = dataToNodeDictionary.at(object->uID());
    node = findLessThan(node);
    if(node != nullptr)
    {
        return node->data;
    }
    return nullptr;

}

ComparableRef * RedBlackTree::findGreaterThanObject(ComparableRef * object)
{
    RedBlackNode * node = dataToNodeDictionary.at(object->uID());
    node = findGreaterThan(node);
    if(node != nullptr)
    {
        return node->data;
    }
    return nullptr;
}


void RedBlackTree::walkTree(void (*callOnEachNode)(RedBlackNode *, int level))
{
    if(root != NIL)
    {
        walkTreeRecursive(root, 0, callOnEachNode);
    }
}

void RedBlackTree::walkTreeRecursive(RedBlackNode * nextNode, int level, void (*callOnEachNode)(RedBlackNode *, int level))
{
    callOnEachNode(nextNode, level);
    if(nextNode->leftChild != NIL)
    {
        walkTreeRecursive(nextNode->leftChild, level +1, callOnEachNode);
    }
    if(nextNode->rightChild != NIL)
    {
        walkTreeRecursive(nextNode->rightChild, level + 1, callOnEachNode);
    }
}

void RedBlackTree::lookForCycleRecursive(RedBlackNode * nextNode)
{
    if(nextNode == NIL)
    {
        return;
    }
    if(nextNode->leftChild == nextNode)
    {
        LOG_DEBUG_ERROR("Found cycle in left node!!! \n");
        return;
    }
    if(nextNode->rightChild == nextNode)
    {
        LOG_DEBUG_ERROR("Found cycle in right node!!! \n");
        return;
    }
    
    if(nextNode->parent == nextNode)
    {
        LOG_DEBUG_ERROR("Found cycle in parent!!! \n");
        return;
    }
    
    lookForCycleRecursive(nextNode->leftChild);
    lookForCycleRecursive(nextNode->rightChild);
    
}

void RedBlackTree::lookForCycle()
{
    if(root == NIL)
    {
        return;
    }
    else if(root->data == nullptr)
    {
        LOG_DEBUG_ERROR("WTF!!!~ \n");
    }
    lookForCycleRecursive(root);
}
