//
//  PolygonLandscapeObject.cpp
//  TrenchWars
//
//  Created by Paul Reed on 8/12/22.
//

#include "PolygonLandscapeObject.h"
#include "MultithreadedAutoReleasePool.h"
#include "World.h"
#include "CollisionGrid.h"
#include "EngagementManager.h"
#include "EntityManager.h"
#include "EntityFactory.h"

void PolygonLandscapeObject::init(const std::string & name, Vec2 worldOrigin, Vec2 visualPositionOffset, Vec2 minimumCellIndex, Rect cellIndexBounds, std::unordered_set<Vec2, Vec2Hash, Vec2Compare> & cellIndicesInEntity)
{
    _debugging = false;
    _cellIndicesInEntity = cellIndicesInEntity;
    _definition = globalEntityFactory->getPolygonLandscapeObjectDefinitionWithName(name);
    _visualPositionOffset = visualPositionOffset;
    _worldOrigin = worldOrigin;
    findEdgePoints(minimumCellIndex, cellIndexBounds);
    _sprite = PolygonSpriteProxy::create(_edgePoints, _definition->_textureName, Vec2(_definition->_textureXSize, _definition->_textureYSize), false);
    
    if(_smoothedEdgePoints.size() == 0)
    {
        LOG_DEBUG_ERROR("Why! \n");
    }
    _boundarySprite = PolygonSpriteProxy::create(_smoothedEdgePoints, _definition->_boundaryTextureName, Vec2(1, _definition->_boundaryTextureSize), true);
    
    //Establish reasonable ordering of z-index to prevent z-fighting
    // PolygonObjects are ordered from left to right, and then bottom to top
    _sprite->setZIndex( ((minimumCellIndex.x * 1000) + minimumCellIndex.y) * 2);
    _boundarySprite->setZIndex((((minimumCellIndex.x * 1000) + minimumCellIndex.y) * 2 ) + 1);
    _terrainCharacteristics.set(_definition->_height, _definition->_cover,_definition->_hardness, _definition->_impassable);    
    
    CollisionGrid * grid = world->getCollisionGrid(worldOrigin);
    for(auto index : _cellIndicesInEntity)
    {
        CollisionCell * cell = grid->getCell(index.x, index.y);
        cell->setLandscapeObject(this);
    }
    world->addLandscapeObject(this);
}

PolygonLandscapeObject * PolygonLandscapeObject::create(const std::string & name, Vec2 worldOrigin, Vec2 visualPositionOffset, Vec2 minimumCellIndex, Rect cellIndexBounds, std::unordered_set<Vec2, Vec2Hash, Vec2Compare> & cellIndicesInEntity)
{
    PolygonLandscapeObject * newObject = new PolygonLandscapeObject();
    newObject->init(name, worldOrigin, visualPositionOffset, minimumCellIndex, cellIndexBounds, cellIndicesInEntity);
    globalAutoReleasePool->addObject(newObject);
    return newObject;
}

size_t PolygonLandscapeObject::getHashedDefinitionName()
{
    return _definition->_hashedName;
}

Vec2 PolygonLandscapeObject::cellCornerForDirection(int direction)
{
    direction = direction % 8;
    if(direction < 0)
    {
        direction += 8;
    }
    if(direction == LEFT)
    {
        return Vec2();
    }
    else if(direction == UP)
    {
        return Vec2(0, 1);
    }
    else if(direction == RIGHT)
    {
        return Vec2(1, 1);
    }
    else if(direction == DOWN)
    {
        return Vec2(1, 0);
    }
    return Vec2();
}

void PolygonLandscapeObject::findEdgePoints(Vec2 minimumCellIndex, Rect cellIndexBounds)
{
    if(_cellIndicesInEntity.size() == 1)
    {
        _edgePoints.push_back( (minimumCellIndex) + _visualPositionOffset);
        _edgePoints.push_back( ((minimumCellIndex + cellCornerForDirection(UP))) + _visualPositionOffset);
        _edgePoints.push_back( ((minimumCellIndex + cellCornerForDirection(RIGHT))) + _visualPositionOffset);
        _edgePoints.push_back( ((minimumCellIndex + cellCornerForDirection(DOWN))) + _visualPositionOffset);
        
        _smoothedEdgePoints.insert(_smoothedEdgePoints.begin(), _edgePoints.begin(),_edgePoints.end());

        return;
    }
    Vec2 currentPosition = minimumCellIndex;
    DIRECTION currentDirection = UP;
    
    bool foundTop = false;
    bool foundBottom = false;
    bool foundLeft = false;
    bool foundRight = false;
    
    _edgePoints.push_back(currentPosition);
    do
    {
        if(currentPosition.x == cellIndexBounds.getMaxX())
        {
            foundRight = true;
        }
        if(currentPosition.x == cellIndexBounds.getMinX())
        {
            foundLeft = true;
        }
        if(currentPosition.y == cellIndexBounds.getMaxY())
        {
            foundTop = true;
        }
        if(currentPosition.y == cellIndexBounds.getMinY())
        {
            foundBottom = true;
        }
        Vec2 straightDirection = VectorMath::unitVectorFromFacing(currentDirection);
        
        // Check LEFT direction (relative to current cell side)
        Vec2 offset = VectorMath::unitVectorFromFacing(currentDirection + 2);
        Vec2 adjacentPosition = currentPosition + offset;
        if(_cellIndicesInEntity.contains(adjacentPosition ))
        {
            _edgePoints.push_back(currentPosition + cellCornerForDirection(currentDirection + 2));
            currentPosition = adjacentPosition;
            currentDirection = VectorMath::rotateDirectionBySteps(currentDirection, 2);
            continue;
        }
        
        // Check STRAIGHT direction (relative to current side )
        offset = straightDirection;
        adjacentPosition = currentPosition + offset;
        if(_cellIndicesInEntity.contains(adjacentPosition))
        {
            currentPosition = adjacentPosition;
            continue;
        }
        
        // Check RIGHT direction (relative to current side )
        offset = VectorMath::unitVectorFromFacing(currentDirection - 2);
        adjacentPosition = currentPosition + offset;
        if(_cellIndicesInEntity.contains(adjacentPosition))
        {
            _edgePoints.push_back(currentPosition + cellCornerForDirection(currentDirection));
            currentDirection = VectorMath::rotateDirectionBySteps(currentDirection, -2);
            currentPosition = adjacentPosition;
            continue;
        }
        
        // Couldn't go left, straight, or right. Only option is to turn around
        offset = VectorMath::unitVectorFromFacing(currentDirection + 4);
        adjacentPosition = currentPosition + offset;
        if(_cellIndicesInEntity.contains(adjacentPosition))
        {
            _edgePoints.push_back(currentPosition + cellCornerForDirection(currentDirection));
            _edgePoints.push_back(currentPosition + cellCornerForDirection(currentDirection - 2));

            currentDirection = VectorMath::rotateDirectionBySteps(currentDirection, 4);
            currentPosition = adjacentPosition;
            continue;
        }
    }
    while(! (currentPosition == _edgePoints.at(0) &&
          foundRight && foundLeft && foundTop && foundBottom));
    
    //Need one more point if we are heading down when we stop
    if(currentDirection == DOWN)
    {
        _edgePoints.push_back(currentPosition + cellCornerForDirection(currentDirection));
    }
    
    for(int i = 0; i < _edgePoints.size(); i++)
    {
        _edgePoints[i] = (_edgePoints[i]) + _visualPositionOffset;
    }
    smoothEdges();
}

/**
  * Gets the midpoint of the line between the previous point and the next point
 */
Vec2 PolygonLandscapeObject::getLaplacian(Vec2 prev, Vec2 point, Vec2 next)
{
    return ( (prev - point) * .5) + ( (next - point) * .5);
}

/**
  *
 */
void PolygonLandscapeObject::smoothEdges()
{
    if(_edgePoints.size() <= 4)
    {
        _smoothedEdgePoints.insert(_smoothedEdgePoints.begin(), _edgePoints.begin(),_edgePoints.end());
        return;
    }
    std::unordered_map<Vec2, int, Vec2Hash, Vec2Compare> _pointsSeen;
    _pointsSeen.reserve(_edgePoints.size());

    std::vector<Vec2> updatedPoints(_edgePoints.begin(), _edgePoints.end());
    Vec2 prev = _edgePoints[_edgePoints.size() -1];
    for(int i = 0; i < _edgePoints.size() -1; i++)
    {
        Vec2 laplace = getLaplacian(prev, _edgePoints[i], _edgePoints[i+1]);
        Vec2 prevToLaplace = (_edgePoints[i] + laplace) - prev;
        Vec2 prevToPoint = _edgePoints[i] - prev;
        float angle = VectorMath::signedAngleBetweenVecs(prevToPoint, prevToLaplace);
        if(laplace.lengthSquared() < 10000)
        {
            if(angle >= 0)
            {
                updatedPoints[i] += laplace;
            }
        }
        if(_pointsSeen.contains(updatedPoints[i]) || _pointsSeen.contains(_edgePoints[i]))
        {
            int duplicateIndex = _pointsSeen[updatedPoints[i]];
            for(int j = duplicateIndex+1; j <= i;j ++)
            {
                updatedPoints[j].x = -99999;
            }
            prev = updatedPoints[duplicateIndex];
        }
        else
        {
            _pointsSeen.emplace(updatedPoints[i], i);
            _pointsSeen.emplace(_edgePoints[i], i);
            prev = _edgePoints[i];
        }
    }
    
    int lastIndex = _edgePoints.size() - 1;

    Vec2 laplace = getLaplacian(_edgePoints[lastIndex-1], _edgePoints[lastIndex], _edgePoints[0]);
    Vec2 prevToLaplace = (_edgePoints[lastIndex] + laplace) - _edgePoints[lastIndex-1];
    Vec2 prevToPoint = _edgePoints[lastIndex] - _edgePoints[lastIndex-1];
    float angle = VectorMath::signedAngleBetweenVecs(prevToPoint, prevToLaplace);
    if(laplace.lengthSquared() < 40000)
    {
        if(angle >= 0)
        {
            updatedPoints[lastIndex] += laplace;
        }
    }
    
    _edgePoints.clear();

    for(int i = 0; i < updatedPoints.size(); i++)
    {
        if(updatedPoints[i].x != -99999)
        {
            _smoothedEdgePoints.push_back(updatedPoints[i]);
            _edgePoints.push_back(updatedPoints[i]);
        }
    }
}

bool PolygonLandscapeObject::containsPoint(Vec2 position)
{
    CollisionCell * cell = world->getCollisionGrid()->getCellForCoordinates(position);
    if(cell != nullptr && _cellIndicesInEntity.contains(Vec2(cell->getXIndex(),cell->getYIndex())))
    {
        return true;
    }
    return false;
}

void PolygonLandscapeObject::setVisible(bool visible)
{
    _sprite->setVisible(visible);
    _boundarySprite->setVisible(visible);
}

void PolygonLandscapeObject::setPositionZ(float zPosition)
{
    _sprite->setPositionZ(zPosition);
    _boundarySprite->setPositionZ(zPosition);
}

void PolygonLandscapeObject::setDebugMe()
{
    _debugging = true;
//    _boundarySprite->setVisible(true);

    _primativeDrawer = PrimativeDrawer::create();
    _primativeDrawer->retain();

    if(_smoothedEdgePoints.size() == 0)
    {
        return;
    }
    
    std::vector<Vec2> test(_smoothedEdgePoints.begin(), _smoothedEdgePoints.end());
    
    Vec2 startToEndConnection = (test[test.size() -1] - test[0]).getNormalized() * 6;
    startToEndConnection += test[0];
    test.push_back(startToEndConnection);

    std::vector<Vec2> test2(test.begin(), test.end());
    
    _primativeDrawer->drawDot(test[0] * WORLD_TO_GRAPHICS_SIZE, 1, Color4F::GREEN);
    _primativeDrawer->drawDot(test[test.size()-1] * WORLD_TO_GRAPHICS_SIZE, 1, Color4F::RED);



    int lastIndex = test.size() - 1;
    int initialSize = test.size();
    
    Vec2 previousLine = (test[lastIndex] - test[lastIndex - 1]).getNormalized();
    
    for(int i = lastIndex; i > 0; i--)
    {
        Vec2 nextLine = (test[i] - test[i - 1]).getNormalized();
        Vec2 line = (nextLine + previousLine) / 2;
        line = line.getPerp().getNormalized();
        test.push_back(test[i] + (line * 2));
        test2[i] -= (line * 4);
        previousLine = nextLine;
    }
    
    Vec2 nextLine = (test[0] - test[lastIndex]).getNormalized();
    Vec2 line = nextLine.getNormalized();
    test.push_back(test[0] + (line * 2));
    test2[0] -= (line * 4);
    
    for(int i = 0; i < test2.size(); i++)
    {
        test[i] = test2[i];
    }
    
    Color4F color(1,1,1,1);
    lastIndex = test.size() - 1;
    for(int i = 1; i <= lastIndex; i++)
    {
        _primativeDrawer->drawLine(test[i-1] * WORLD_TO_GRAPHICS_SIZE, test[i] * WORLD_TO_GRAPHICS_SIZE, color);
        color.g -= 1.0 / test.size();
        color.b -= 1.0 / test.size();
    }
    _primativeDrawer->drawLine(test[lastIndex] * WORLD_TO_GRAPHICS_SIZE, test[0] * WORLD_TO_GRAPHICS_SIZE, Color4F::ORANGE);
}

void PolygonLandscapeObject::clearDebugMe()
{
    _debugging = false;

//    _boundarySprite->setVisible(false);

    if(_primativeDrawer != nullptr)
    {
        _primativeDrawer->clear();
        _primativeDrawer->release();
        _primativeDrawer = nullptr;
    }
}
