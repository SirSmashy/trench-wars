#ifndef Ref_h
#define Ref_h
//
//  ComparableRef.h
//  TrenchWars
//
//  Created by Paul Reed on 10/1/17.
//
//



#include "cocos2d.h"
#include "Logging.h"
#include <cereal/archives/binary.hpp>
#include <mutex>

USING_NS_CC;

typedef long long int ENTITY_ID;

extern std::atomic_llong nextId;

class ComparableRef : public Ref
{
protected:
    ENTITY_ID _id;
    
    static ENTITY_ID fetchAddId();
public:
    
    ComparableRef();
    
    static ENTITY_ID getMaxId();

    
    static void setNextId(ENTITY_ID next);

    virtual ENTITY_ID uID() const {return _id;}
    
    void setID(ENTITY_ID entId)
    {
        _id = entId;
    }
    
    virtual bool operator == (const ComparableRef & other) const;
    virtual bool operator == (const ComparableRef * other) const;
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

struct ComparableRefPointerHash
{
public:
    std::size_t operator() (ComparableRef const * ref) const {
        return (std::size_t) ref->uID();
    }
};

struct ComparableRefPointerCompare
{
public:
    bool operator() (ComparableRef const * refA, ComparableRef const * refB) const {
        return refA->uID() == refB->uID();
    }
};

struct ComparableRefHash
{
public:
    std::size_t operator() (ComparableRef const & ref) const {
        return (std::size_t) ref.uID();
    }
};

struct ComparableRefCompare
{
public:
    bool operator() (ComparableRef const &refA, ComparableRef const &refB) const {
        return refA.uID() == refB.uID();
    }
};

#endif /* Ref_h */
