//
//  ServerLobbyManager.hpp
//  TrenchWars
//
//  Created by Paul Reed on 11/19/23.
//

#ifndef ServerLobbyManager_h
#define ServerLobbyManager_h
#include "LobbyManager.h"

class ServerLobbyManager : public LobbyManager
{
private:
    ScenarioDefinition * _selectedScenario;

    void sendLobbyStatusToClients();
    void init();
    
    void startGame();
    
public:
    
    static ServerLobbyManager * create();
    virtual bool isPlayerReady(int playerId);
    
    //UI to Manager Functions
    virtual void playerSelectedStartGame(int playerId);
    virtual void playerSetName(int playerId, const std::string & name);
    virtual void playerSelectedCommand(int teamId, int playerId, int commandId);
    virtual void playerSetScenario(const std::string & scenarioName);
    
    
    virtual void update(float deltaTime);
    virtual void parseNetworkMessage(GameMessage * message);
    
    virtual void playerJoinedGame(Player * player);
    virtual void playerLeftGame(int playerId);
    
    virtual void shutdown();
};

#endif /* ServerLobbyManager_h */
