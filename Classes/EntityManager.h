//
//  EntityManager.hpp
//  TrenchWars
//
//  Created by Paul Reed on 12/28/22.
//

#ifndef EntityManager_h
#define EntityManager_h

#include "cocos2d.h"
#include "Entity.h"
#include <unordered_set>
USING_NS_CC;


class EntityManager
{
    Map<ENTITY_ID, Entity *> _entities; //master list of all entities
    
    std::unordered_map<ENTITY_TYPE, std::unordered_set<Entity *>> _entityTypes;
    std::unordered_map<ENTITY_ID, Entity *> _queryingEntitiesA;
    std::unordered_map<ENTITY_ID, Entity *> _queryingEntitiesB;
    std::unordered_map<ENTITY_ID, Entity *> _actingEntities;
    
    std::vector<Entity *> _queryVector;
    std::vector<Entity *> _actVector;

    
    Vector<Entity *> _entitiesToAdd; //list of entities to be added to the world
    Vector<Entity *> _entitiesToRemove; //list of entities to be removed from the world
    std::mutex _entitiesMutex;
    
    std::unordered_set<ENTITY_ID> _entitiesJustAdded;
    std::unordered_set<ENTITY_ID> _entitiesJustRemoved;
    
    bool _queryANext;

        
public:
    
    EntityManager();
    
    void query(float deltaTime);
    void act(float deltaTime);
    void update(float deltaTime);

    const Map<ENTITY_ID, Entity *> & getEntities() {return _entities;}

    Entity * getEntity(ENTITY_ID entId);
    void registryEntity(Entity * ent);
    void removeEntity(Entity * ent);
        
    void serializeEntityQueryChanges(cereal::BinaryOutputArchive & archive);
    void serializeEntityAdditionsAndRemovals(cereal::BinaryOutputArchive & archive);

    void serializeEntities(cereal::BinaryOutputArchive & archive, bool includeProjectiles);
    void serializeEntitiesOfType(cereal::BinaryOutputArchive & archive, ENTITY_TYPE type);

    void deserializeEntities(cereal::BinaryInputArchive & archive);
    void deserializeEntityQueryChanges(cereal::BinaryInputArchive & archive);
    void deserializeEntityAdditionsAndRemovals(cereal::BinaryInputArchive & archive);

    
    void shutdown();
};

extern EntityManager * globalEntityManager;
#endif /* EntityManager_h */
