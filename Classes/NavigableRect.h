//
//  NavigableRect.hpp
//  TrenchWars
//
//  Created by Paul Reed on 1/9/23.
//

#ifndef NavigableRect_h
#define NavigableRect_h


#include "cocos2d.h"
#include "CollisionCell.h"
#include "VectorMath.h"

#define MAX_PATCH_SIZE 500
#define MAX_PATCH_AREA MAX_PATCH_SIZE * MAX_PATCH_SIZE
#define SIZE_TO_ASPECT_RATIO_CONSTANT 25 //(5^2)


class NavigableRect
{
protected:
    int _minX;
    int _maxX;
    int _minY;
    int _maxY;
    
    int _area;
    int _sqrtArea;
    double _aspectRatio;
    
public:
    
    NavigableRect();
    NavigableRect(int xMin, int xMax, int yMin, int yMax);
    
    void set(int xMin, int xMax, int yMin, int yMax);
    
    inline int getMinX() const {return _minX;}
    inline int getMaxX() const {return _maxX;}
    inline int getMinY() const {return _minY;}
    inline int getMaxY() const {return _maxY;}
    inline int getArea() {
        if(_area == -1)
        {
            _area = (_maxX - _minX + 1) * (_maxY - _minY + 1);
        }
        return _area;
    }
    
    inline int getSqrtArea() {
        if(_sqrtArea == -1)
        {
            _sqrtArea = sqrt(getArea());
        }
        return _sqrtArea;
    }
    inline double getAspectRatio() {
        if(_aspectRatio == -1)
        {
            double x = getXSize();
            double y = getYSize();
            _aspectRatio = x > y ? x / y : y / x;
        }
        return _aspectRatio;
    }
    
    
    DIRECTION getBorderDirectionForRect(NavigableRect & other) const;
    DIRECTION getBorderForCoordinates(int x, int y) const;
    bool bordersRect(NavigableRect & other) const;
    bool intersectsRectX(const NavigableRect & other) const;
    bool intersectsRectY(const NavigableRect & other) const;
    bool intersectsRect(const NavigableRect & other) const;

    bool borderEqualSize(NavigableRect & other) const;
    bool isValidRect() const;
    bool hasValidAspectRatio();
    double minimumDistanceToCoordinates(int x, int y) const;
    bool containsCoordinates(int x, int y) const;
    
    
    bool operator<(const NavigableRect & other) const;
    bool operator==(const NavigableRect & other) const;
    bool operator!=(const NavigableRect & other) const;
    
    int getXUnion(NavigableRect & other)  const;
    int getYUnion(NavigableRect & other)  const;
        
    inline int getXMid() const{return _minX + ((_maxX - _minX) /2);}
    int getYMid() const {return _minY + ((_maxY - _minY) /2);}
    
    int getXSize() const {return (_maxX - _minX) + 1;}
    int getYSize() const {return (_maxY - _minY) + 1;}
};

#endif /* NavigableRect_hpp */
