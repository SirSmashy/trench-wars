//
//  GameStatistics.hpp
//  TrenchWars
//
//  Created by Paul Reed on 7/31/22.
//

#ifndef GameStatistics_h
#define GameStatistics_h

#include "Refs.h"
#include "EntityDefinitions.h"

struct TestValue
{
    int count = 0;
    double accumulation = 0;
    double min = 9999999999.0;
    double max = 0;
    
    void addValue(double value)
    {
        count++;
        accumulation += value;
        
        if(value < min)
        {
            min = value;
        }
        if(value > max)
        {
            max = value;
        }
    }    
};

class GameStatistics : public Ref
{
    std::unordered_map<std::string, TestValue> _testValues; //Map storing various values
    std::mutex testMutex;
    std::unordered_map<int, int> _entityCountMap; //Total counts for each entity type

    void init();
    
public:
    
    static GameStatistics * create();
    void incrementEntityTypeCount(ENTITY_TYPE type);
    void decrementEntityTypeCount(ENTITY_TYPE type);
    void setTestValue(const std::string & valueId, double newValue);
    void clearTestValues();
    
    int livingHumansForTeam(int team);
    
    size_t getTestValuesCount() {return _testValues.size();}
    std::unordered_map<std::string, TestValue> & getTestValues() {return _testValues;}
    int  getGameObjectTypeCount(ENTITY_TYPE type);
};

extern GameStatistics * globalGameStatistics;
#endif /* GameStatistics_h */
