//
//  CircleMenu.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/18/20.
//

#ifndef CircleMenu_h
#define CircleMenu_h

#include <stdio.h>
#include "cocos2d.h"
#include "AutoSizedLayout.h"
#include "ui/CocosGUI.h"
#include "PrimativeDrawer.h"

USING_NS_CC;
using namespace cocos2d::ui;
//forward declations

class Entity;
class Emblem;
class Objective;
class LandscapeObjectiveBuilder;

struct MenuOption : public Ref
{
    Label * label;
    std::function<void(Vec2)> callback;
    bool disabled;
    
    ~MenuOption()
    {
        label->release();
    }
};


class CircularMenu : public Layout
{
    PrimativeDrawer * _primativeDrawer;    
    Vector<MenuOption *> _options;
    
    Vec2 _gamePosition;
    float _overallOpacity;
    float _radius;
    float _holeRadius;
    float _startAngle;
    float _endAngle;
    int _selectedIndex;
    virtual bool init();
    
    void drawOptionBackground(float startAngle, float endAngle, bool disabled, bool highlight);
    void drawOptionBorder(float startAngle, float endAngle, bool disabled, bool highlight);
    float getAngleFromStart(Vec2 point);

public:
    CircularMenu();
    static CircularMenu * create();
    
    virtual void update(float deltaTime);
    void setHoleRadius(float radius);
    void setCircleExtent(float startAngle, float endAngle);
    void addOption(std::string name, const std::function<void(Vec2)> & callback, bool disabled = false);
    void populateFromLandscapeObjectiveBuilder(LandscapeObjectiveBuilder * builder, Emblem * draggedEmblem, Vec2 location);

    
    bool pointInMenu(Vec2 point);
    void clearHighlightedOption();
    void highlightOptionAtPoint(Vec2 point);
    void selectOptionAtPoint(Vec2 point);
    
    void setGamePosition(Vec2 position);
    virtual void setVisible(bool visible);
    virtual void setOpacity(GLubyte opacity);
    
    
    void doLayout();
    void clear();
    void remove();
};

#endif /* CircleMenu_h */
