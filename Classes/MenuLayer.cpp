//
//  MenuLayer.m
//  TrenchWars
//
//  Created by Paul Reed on 9/16/12.
//  Copyright (c) 2012 . All rights reserved.
//

#include "MenuLayer.h"
#include "World.h"
#include "EngagementManager.h"
#include "GameStatistics.h"
#include "TeamManager.h"
#include "UIController.h"

#define WIDTH_OFFSET 300
#define FONT_SIZE 20
#define SPACING 5

MenuLayer * globalMenuLayer;

UnitStatusUI::UnitStatusUI(std::string & name)
{
    unitName = Label::createWithTTF(name, "arial.ttf", FONT_SIZE);
    unitName->setAnchorPoint(Vec2());
}



void UnitStatusUI::setStatusTextForLabel(std::string & labelName, std::string  & labelValue, Node * scrollView)
{
    std::string combinedString = labelName;
    combinedString += ": ";
    combinedString += labelValue;
    
    Label * labelText = statusLabels.at(labelName);
    
    if(labelText == nullptr)
    {
        labelText = Label::createWithTTF(combinedString, "arial.ttf", FONT_SIZE);
        
        labelText->setAnchorPoint(Vec2());
        statusLabels.insert(labelName, labelText);
        scrollView->addChild(labelText);
    }
    else
    {
        labelText->setString(combinedString);
    }
}

MenuLayer::MenuLayer() : AutoSizedLayout()
{
}

// on "init" you need to initialize your instance
bool MenuLayer::init()
{
    if(Layout::init())
    {
        highlightedUnitID = -1;
        setUpMenus();
        updateTime = 0;
        framesPerUpdate = 0;
        globalMenuLayer = this;
        menuBackground = nullptr;
        _primaryCircularMenu = CircularMenu::create();
        _primaryCircularMenu->retain();
        return true;
    }
    return false;
}

MenuLayer * MenuLayer::create()
{
    MenuLayer * layer = new MenuLayer();
    if(layer->init())
    {
        layer->autorelease();
        return layer;
    }
    layer->release();
    return nullptr;
}


Button * MenuLayer::createButton(std::string name, std::string text, Layout * menu, const AbstractCheckButton::ccWidgetClickCallback & callback)
{
    LinearLayoutParameter * params = LinearLayoutParameter::create();
    params->setMargin(Margin(5, 0, 5, 0));
    Button * button = Button::create();
    button->setTitleText(text);
    button->setTitleFontName("arial.ttf");
    button->setTitleFontSize(FONT_SIZE);
    button->addClickEventListener(callback);
    button->setLayoutParameter(params);
    buttons.insert(name, button);
    if(menu != nullptr)
    {
        menu->addChild(button);
    }
    else
    {
        addChild(button);
    }
    return button;
}


AutoSizedLayout * MenuLayer::createLayout(Vec2 position, Layout::Type layoutDirection)
{
    LinearLayoutParameter * params = LinearLayoutParameter::create();
    params->setMargin(Margin(5, 0, 5, 0));
    params->setGravity(LinearLayoutParameter::LinearGravity::LEFT);
    AutoSizedLayout * layout = AutoSizedLayout::create();
    layout->setPosition(position);
    layout->setLayoutParameter(params);
    layout->setLayoutType(layoutDirection);
    
    layout->setBackGroundColor(Color3B::GRAY);
    layout->setBackGroundColorType(BackGroundColorType::SOLID);
    layout->setBackGroundColorOpacity(125);
    
    addChild(layout); //20
    return layout;
}


void MenuLayer::createTestingMenu()
{
    testingMenu = createLayout(Vec2(WIDTH_OFFSET, 10));
    createButton("","Hide Objectives", testingMenu,[=](Ref* sender)
    {
        Button * button = (Button *) sender;
        if(globalUIController->togglePlanningMode())
        {
            button->setTitleText("Show Objectives");
        }
        else
        {
            button->setTitleText("Hide Objectives");
        }
    });
}

void MenuLayer::createInfoMenu()
{
    Size winSize = Director::getInstance()->getVisibleSize();
    infoMenu = createLayout(Vec2(winSize.width,winSize.height - 10));
    
    infoMenu->setAnchorPoint(Vec2::ANCHOR_TOP_RIGHT);

    /////////////create Info Item
    gameInfo = Text::create("", "arial.ttf", FONT_SIZE);
    LinearLayoutParameter * params = LinearLayoutParameter::create();
    params->setMargin(Margin(5, 0, 5, 0));
    gameInfo->setLayoutParameter(params);
    infoMenu->addChild(gameInfo);
}

void MenuLayer::createTimingMenu()
{
    Size winSize = Director::getInstance()->getVisibleSize();
    timingMenu = createLayout(Vec2(10,winSize.height - 10), Layout::Type::VERTICAL);
    timingMenu->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
    //timingMenu->setVisible(false);
}

void MenuLayer::createGameMenu()
{
    Size winSize = Director::getInstance()->getVisibleSize();
    gameMenu = Layout::create();
    Vec2 position = Vec2((winSize.width * .5) ,winSize.height * .5);
    gameMenu->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
    gameMenu->setBackGroundColor(Color3B::GRAY);
    gameMenu->setBackGroundColorType(BackGroundColorType::SOLID);
    gameMenu->setBackGroundColorOpacity(255);
    gameMenu->setPosition(position);
    gameMenu->setLayoutType(Layout::Type::VERTICAL);


    addChild(gameMenu, 0); //20
    
    LinearLayoutParameter * params = LinearLayoutParameter::create();
    params->setMargin(Margin(5, 5, 5, 5));
    params->setGravity(LinearGravity::CENTER_HORIZONTAL);
    gameMenu->setLayoutParameter(params);
    
    createButton("","Save EngagementManager", gameMenu,[=](Ref* sender)
    {
        globalEngagementManager->saveGame("/Users/paulreed/Desktop/TWSave.tws");
    });
    gameMenu->setVisible(false);
    gameMenu->setContentSize(Size(winSize.width * .5, winSize.height * .5));
}

void MenuLayer::toggleGameMenu()
{
    gameMenu->setVisible(!gameMenu->isVisible());
}


// set up the Menus
void MenuLayer::setUpMenus()
{
    Size winSize = Director::getInstance()->getVisibleSize();
    
    createInfoMenu();
    createTestingMenu();
    createTimingMenu();
    createGameMenu();
    
    
    unitStatusMenu = ScrollView::create();
    unitStatusMenu->setBounceEnabled(false);
    unitStatusMenu->setPosition(Vec2(winSize.width-200, 0));
    addChild(unitStatusMenu, 0); //20
}


void MenuLayer::toggleTimingMenu()
{
    timingMenu->setVisible(!timingMenu->isVisible());
}



void MenuLayer::updateGameInfo(float deltaTime)
{
    updateTime += deltaTime;
    framesPerUpdate++;
    if(updateTime < 1) {
        return;
    }
    
    update();
    
    int ammo = globalTeamManager->getCommand(0)->getTotalAmmo();
    int projectileCount = globalGameStatistics->getGameObjectTypeCount(PROJECTILE);
    int infantryCount = globalGameStatistics->getGameObjectTypeCount(INFANTRY);
    int staticsCount = globalGameStatistics->getGameObjectTypeCount(STATIC_ENTITY);

    
    int team0Count = globalGameStatistics->livingHumansForTeam(0);
    int team1Count = globalGameStatistics->livingHumansForTeam(1);
    
    sprintf(infoMenuString, "FPS: %3d Stats: %d Inf: %d Projs: %d Ammo: %d T0: %d T1: %d", (int) Director::getInstance()->getFrameRate(),staticsCount, infantryCount, projectileCount, ammo,
            team0Count, team1Count);
    gameInfo->setString(infoMenuString);
    framesPerUpdate = 0;
    updateTime = 0;
}

void MenuLayer::update()
{
    if(timingMenu->isVisible())
    {
        for(auto node : timingMenu->getChildren())
        {
            Text * text = (Text *) node;
            text->setColor(Color3B::RED);
        }
        for(auto entry : globalGameStatistics->getTestValues())
        {
            auto testValue = entry.second;
            
            std::string combinedString = entry.first; // Label
            combinedString += " ";
            
            std::ostringstream values;
            values.precision(1);
            double average = testValue.accumulation / testValue.count;
            values << std::fixed << " Avg(ms) " << average << " Min(ms) " << testValue.min << " Max(ms) " << testValue.max << " #/Sec: " << testValue.count << " Sum(ms):" << testValue.accumulation;
            combinedString += values.str();
            Text * textField = nullptr;
            
            for(auto node : timingMenu->getChildren())
            {
                Text * text = (Text *) node;
                if(text->getName().compare(entry.first) == 0)
                {
                    textField = text;
                    break;
                }
            }
            if(textField == nullptr)
            {
                textField = Text::create(combinedString, "arial.ttf", FONT_SIZE);
                LinearLayoutParameter * params = LinearLayoutParameter::create();
                params->setMargin(Margin(0, 0, 0, 0));
                textField->setLayoutParameter(params);
                textField->setName(entry.first);
                textField->setTextHorizontalAlignment(TextHAlignment::LEFT);
                textField->setAnchorPoint(Vec2::ANCHOR_TOP_LEFT);
                timingMenu->addChild(textField);
            }
            else
            {
                textField->setString(combinedString);
            }
            textField->setColor(Color3B::BLACK);
        }
        timingMenu->doLayout();
    }
    
    globalGameStatistics->clearTestValues();
}


void MenuLayer::setUnitStatusVariableForUnit(ENTITY_ID unitID, std::string variableName, std::string value)
{
    UnitStatusUI * unitStatus = unitStatusLabels.at(unitID);
    if(unitStatus == nullptr)
    {
        std::string name = std::to_string(unitID);
        unitStatus = new UnitStatusUI(name);
        unitStatusLabels.insert(unitID, unitStatus);
        scrollContent->addChild(unitStatus->getUnitName(), 25);
    }
    
    unitStatus->setStatusTextForLabel(variableName, value, scrollContent);
    layoutUnitStatus();
}

void MenuLayer::highlightUnitStatusLabel(ENTITY_ID unitID)
{
    UnitStatusUI * unitStatus;
    if(highlightedUnitID != -1)
    {
        unitStatus = unitStatusLabels.at(highlightedUnitID);
        if(unitStatus != nullptr)
        {
            unitStatus->getUnitName()->setColor(Color3B(255,255,255));
        }
    }
    
    highlightedUnitID = unitID;
    unitStatus = unitStatusLabels.at(highlightedUnitID);
    if(unitStatus != nullptr)
    {
        unitStatus->getUnitName()->setColor(Color3B(255,0,0));
    }
}

void MenuLayer::layoutUnitStatus()
{
    int yPostion = scrollContent->getContentSize().height - 14; //FIXME: magic number
    
    for(auto labelIterator = unitStatusLabels.begin(); labelIterator != unitStatusLabels.end(); labelIterator++)
    {
        UnitStatusUI * unitStatus = labelIterator->second;
        
        unitStatus->getUnitName()->setPosition(Vec2(0,yPostion));
        yPostion -= 14; //moves the label UP the screen
        Map<std::string, Label *> * statusLabels;
        
        for(auto statusIt = statusLabels->begin(); statusIt != statusLabels->end(); statusIt++)
        {
            Label * label = statusIt->second;
            label->setPosition(Vec2(20,yPostion));
            yPostion -= 14;
        }
    }
}



bool MenuLayer::pointInMenu(Vec2 point, bool click)
{
    if(_primaryCircularMenu->pointInMenu(point))
    {
        if(click)
        {
            _primaryCircularMenu->selectOptionAtPoint(point);
        }
        return true;
    }
    
    for(Node * child : getChildren() )
    {
        if(child->isVisible() && child->getBoundingBox().containsPoint(point))
        {
            return true;
        }
    }
    return false;
}
