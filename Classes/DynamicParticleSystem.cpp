//
//  DynamicParticleSystem.cpp
//  TrenchWars
//
//  Created by Paul Reed on 7/27/24.
//

#include "DynamicParticleSystem.h"
#include "Random.h"

inline void normalize_point(float x, float y, particle_point* out)
{
    float n = x * x + y * y;
    // Already normalized.
    if (n == 1.0f)
        return;
    
    n = sqrt(n);
    // Too close to zero.
    if (n < MATH_TOLERANCE)
        return;
    
    n = 1.0f / n;
    out->x = x * n;
    out->y = y * n;
}

DynamicParticleSystem::~DynamicParticleSystem()
{
    LOG_DEBUG("bye bye particles! \n");
}

DynamicParticleSystem::DynamicParticleSystem() : cocos2d::ParticleSystemQuad(),
_pendingAlpha(0.0),
_pendingRadius(0.0),
_pendingSize(0.0)
{
    _created = false;
    setPositionType(PositionType::RELATIVE);
}

DynamicParticleSystem * DynamicParticleSystem::createWithTotalParticles(int numberOfParticles)
{
    DynamicParticleSystem * system = new DynamicParticleSystem();
    
    if(system->initWithTotalParticles(numberOfParticles))
    {
        system->autorelease();
        return system;
    }
    
    CC_SAFE_DELETE(system);
    return nullptr;
}


DynamicParticleSystem * DynamicParticleSystem::create(const std::string& filename)
{
    DynamicParticleSystem * system =  new DynamicParticleSystem();
    if (system && system->initWithFile(filename))
    {
        system->autorelease();
        return system;
    }
    CC_SAFE_DELETE(system);
    return system;
}

void DynamicParticleSystem::setAlpha(float alpha)
{
    _pendingAlpha = alpha;
}

void DynamicParticleSystem::setRadius(float radius)
{
    _pendingRadius = radius;
}

void DynamicParticleSystem::setSize(float size)
{
    _pendingSize = size;
}

void DynamicParticleSystem::setupParticles(ParticleCloudProperties & cloudProperties)
{
    float maxRadiusPercent = 1 - (cloudProperties.cloudRadiusToMaxParticleSizePercent / 2);
    addParticles(cloudProperties.particleCount);
    for (int i = 0; i < _particleCount; ++i)
    {
        _particleData.modeB.deltaRadius[i] = maxRadiusPercent - pow(maxRadiusPercent - globalRandom->randomDouble(0, maxRadiusPercent),1.8);
        _particleData.deltaSize[i] = globalRandom->randomDouble(cloudProperties.cloudRadiusToMinParticleSizePercent, cloudProperties.cloudRadiusToMaxParticleSizePercent);
        _particleData.deltaColorA[i] = globalRandom->randomDouble(0.5, 1.0);

    }
}



void DynamicParticleSystem::update(float dt)
{
    if(_pendingAlpha.isChanged())
    {
        for (int i = 0 ; i < _particleCount; ++i)
        {
            _particleData.colorA[i] = _pendingAlpha * _particleData.deltaColorA[i];
        }
    }
    
    for (int i = 0; i < _particleCount; ++i)
    {
        if (_particleData.timeToLive[i] <= 0.0f)
        {
            int j = _particleCount - 1;
            while (j > 0 && _particleData.timeToLive[j] <= 0)
            {
                _particleCount--;
                j--;
            }
            _particleData.copyParticle(i, _particleCount - 1);
            if (_batchNode)
            {
                //disable the switched particle
                int currentIndex = _particleData.atlasIndex[i];
                _batchNode->disableParticle(_atlasIndex + currentIndex);
                //switch indexes
                _particleData.atlasIndex[_particleCount - 1] = currentIndex;
            }
            --_particleCount;
            if( _particleCount == 0 && _isAutoRemoveOnFinish )
            {
                this->unscheduleUpdate();
                _parent->removeChild(this, true);
                return;
            }
        }
    }
    
    if(_pendingRadius.isChanged())
    {
        float radius = _pendingRadius;
        for (int i = 0; i < _particleCount; ++i)
        {
            _particleData.modeB.radius[i] = radius * _particleData.modeB.deltaRadius[i] ;
        }
        _pendingRadius.clearChange();
    }
    for (int i = 0; i < _particleCount; ++i)
    {
        _particleData.modeB.angle[i] += _particleData.modeB.degreesPerSecond[i] * dt;
        _particleData.posx[i] = - cosf(_particleData.modeB.angle[i]) * _particleData.modeB.radius[i];
        _particleData.posy[i] = - sinf(_particleData.modeB.angle[i]) * _particleData.modeB.radius[i] * _yCoordFlipped;
    }
    
    
    if(_pendingSize.isChanged())
    {
        for (int i = 0 ; i < _particleCount; ++i)
        {
            _particleData.size[i] = _pendingSize * _particleData.deltaSize[i];
        }
        _pendingSize.clearChange();
    }
        
    for (int i = 0 ; i < _particleCount; ++i)
    {
        _particleData.rotation[i] += _particleData.deltaRotation[i] * dt;
    }
        
    updateParticleQuads();
    _transformSystemDirty = false;
    
    
    // only update gl buffer when visible
    if (_visible && ! _batchNode)
    {
        postStep();
    }
}

void DynamicParticleSystem::stop()
{
    this->unscheduleUpdate();
    _parent->removeChild(this, true);
}
