//
//  NavigationSystem.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/5/22.
//

#include "NavigationSystem.h"
#include "NavigationGrid.h"
#include "EngagementManager.h"
#include "World.h"
#include "BackgroundTaskHandler.h"

NavigationSystem * globalNavigationSystem;

NavigationSystem::NavigationSystem()
{
    globalNavigationSystem = this;
}

void NavigationSystem::splitNavigationPatches()
{
    if(globalBackgroundTaskHandler->_pathfindingTasksActive.load() > 0)
    {
        return;
    }
    
    for(auto section : world->getWorldSections())
    {
        if(!section->_areaDestroyed)
        {
            section->_navGrid->splitNavigationPatches();
        }
    }
}

void NavigationSystem::addPatchToInit(NavigationPatch *  patch)
{
    _requestsMutex.lock();
    _patchesToInit.insert(patch);
    _requestsMutex.unlock();
}

void NavigationSystem::addPatchToRemove(NavigationPatch *  patch)
{
    _requestsMutex.lock();
    if(globalEngagementManager->_updatingNavigation)
    {
        LOG_DEBUG_ERROR("This probably shouldn't happen!!! \n");
    }
    _patchesToRemove.insert(patch);
    _requestsMutex.unlock();

}

void NavigationSystem::addPatchToGenerateBorders(NavigationPatch *  patch)
{
    _requestsMutex.lock();
    _patchesToGenerateBorders.insert(patch);
    _requestsMutex.unlock();

}

void NavigationSystem::removePatchToInit(NavigationPatch *  patch)
{
    _requestsMutex.lock();
    _patchesToInit.erase(patch);
    _requestsMutex.unlock();

}

void NavigationSystem::removePatchToGenerateBorders(NavigationPatch *  patch)
{
    _requestsMutex.lock();
    _patchesToGenerateBorders.erase(patch);
    _requestsMutex.unlock();

}

bool NavigationSystem::getNeedsUpdating()
{
    return (_patchesToRemove.size() + _patchesToInit.size()) > 0 || world->getNavGrid()->needsSplitting();
}

bool NavigationSystem::updateNavigationGrids()
{
    //Multithreaded updating of the Navigaation grid. Occurs in three phases
    // First: remove old patches and initiate new patches. Do these concurrently.
    // Second: Find neighbors for new patches (or patches that have been updated)
    // Third: Build paths between neighboring nodes
    
    if(globalBackgroundTaskHandler->_pathfindingTasksActive.load() > 0)
    {
        return false;
    }

    if(_patchesToRemove.size() + _patchesToInit.size() == 0)
    {
        return false;
    }
    
    globalEngagementManager->_updatingNavigation.store(true);
    globalEngagementManager->_tasksRunning.store(_patchesToRemove.size() + _patchesToInit.size());
    
    // Note that both the patch removal function here AND the patch init function below could
    // complete after the other and trigger finding neighbors and releasing old patches
    std::vector<std::function<bool(void)>> functions;
    for(NavigationPatch * patch : _patchesToRemove)
    {
        functions.push_back([this, patch] {
            patch->remove();
            if(--globalEngagementManager->_tasksRunning <= 0)
            {
                globalEngagementManager->_tasksRunning.fetch_add(1); // Add one for the thread executing this function
                // All old patches have now been removed, and new patches have been initiated
                findNeighbors();
                releaseOldPatches();
                if(--globalEngagementManager->_tasksRunning <= 0)
                {
                    //All new patches have found neighbors, all done
                    globalEngagementManager->_updatingNavigation.store(false);
                }
            }
            return false;
        });
    }
    
    for(NavigationPatch * patch : _patchesToInit)
    {
        functions.push_back([this, patch] 
        {
            patch->claimCollisionCells();
            if(--globalEngagementManager->_tasksRunning <= 0)
            {
                // All old patches have now been removed, and new patches have been initiated
                // Add one for the thread executing this function
                globalEngagementManager->_tasksRunning.fetch_add(1);
                findNeighbors();
                releaseOldPatches();
                if(--globalEngagementManager->_tasksRunning <= 0)
                {
                    //All new patches have found neighbors, all done
                    globalEngagementManager->_updatingNavigation.store(false);
                }
            }
            return false;
        });
    }
    
    for(auto function : functions)
    {
        globalBackgroundTaskHandler->addBackgroundTask(function);
    }
    _patchesToInit.clear();
    return true;
}

void NavigationSystem::releaseOldPatches()
{
    for(NavigationPatch * patch : _patchesToRemove)
    {
        patch->release();
    }
    _patchesToRemove.clear();
}

void NavigationSystem::findNeighbors()
{
    if(_patchesToGenerateBorders.size() == 0)
    {
        return;
    }
    
    globalEngagementManager->_tasksRunning.fetch_add(_patchesToGenerateBorders.size()); // Add one for the thread executing this function
    for(NavigationPatch * patch : _patchesToGenerateBorders)
    {
        // Find neighbors for each patch
        globalBackgroundTaskHandler->addBackgroundTask([this, patch] {
            patch->findNeighbors();
            if(--globalEngagementManager->_tasksRunning <= 0)
            {
                //All new patches have found neighbors, all done
                globalEngagementManager->_updatingNavigation.store(false);
            }
            return false;
        });
    }
    _patchesToGenerateBorders.clear();
}

void NavigationSystem::updateNavigationGridDrawing(Vec2 drawNearLocation)
{
    for(auto section : world->getWorldSections())
    {
        if(!section->_areaDestroyed)
        {
            section->_navGrid->drawNavigationGrid(drawNearLocation);
        }
    }
}
