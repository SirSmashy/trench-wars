//
//  Math.m
//  Trench Wars
//
//  Created by Paul Reed on 1/8/14.
//  Copyright (c) 2014 . All rights reserved.
//

#include "cocos2d.h"
#include "VectorMath.h"
#include "Weapon.h"
#include "math.h"
#include "GameVariableStore.h"
#include "CollisionGrid.h"

static const double MIN_ANGLE_TO_INT = 1 / ((M_PI * 2) / (ANGLE_FOR_GRID_SIZE * 8));
// This value allows for the encoding (as a simple int) of any angle from one cell to the another in a 2000x2000 grid.

float VectorMath::distanceFromLineSegment(Vec2 start, Vec2 end, Vec2 point)
{
    // Return minimum distance between line segment vw and point p
    
    float l2 = start.distanceSquared(end); // i.e. |w-v|^2 -  avoid a sqrt
    if (l2 == 0.0) return start.distance(point);   // v == w case
    // Consider the line extending the segment, parameterized as v + t (w - v).
    // We find projection of point p onto the line.
    // It falls where t = [(p-v) . (w-v)] / |w-v|^2

    
    float t = Vec2::dot((point - start), (end - start)) / l2;
    if (t < 0.0)
    {
        return point.distance(start);       // Beyond the 'v' end of the segment
    }
    else if (t > 1.0)
    {
        return point.distance(end); // Beyond the 'w' end of the segment
    }
    
    Vec2 projection = start + ( (end - start) * t);
    return point.distance(projection);
}

void VectorMath::quadraticSolver(double a, double b, double c, double * root1, double * root2)
{
    double x = (b * b) - (4 * a * c);
    if(x < 0)
    {
        *root1 = NAN;
        *root2 = NAN;
    }
    x = sqrt(x);
    
    *root1 = (-b + x) / (2 * a);
    *root2 = (-b - x) / (2 * a);
}

Vec2 VectorMath::computePredictedTargetPosition(double projectileVelocity, Vec2 vectorToTarget, Vec2 targetVelocity, WEAPON_PROJECTILE_TYPE type)
{
    
    if(targetVelocity.x == 0 && targetVelocity.y == 0)
    {
        return vectorToTarget;
    }
    
    if(type == DIRECT)
    {
        Vec2 finalTargetPosition;
        double a = (targetVelocity.x * targetVelocity.x) + (targetVelocity.y + targetVelocity.y);
        a -= (projectileVelocity * projectileVelocity);
        double b = 2 * (vectorToTarget.x * targetVelocity.x) + (vectorToTarget.y * targetVelocity.y);
        double c = (vectorToTarget.x * vectorToTarget.x) + (vectorToTarget.y * vectorToTarget.y);
        double root1,root2;
        
        double timeOfFlight;
        VectorMath::quadraticSolver(a, b, c, &root1, &root2);
        
        if(isnan(root1))
        {
            if(!isnan(root2) && root2 > 0)timeOfFlight = root2;
            else timeOfFlight = 0;
        }
        else if(isnan(root2))
        {
            if(root1 > 0)timeOfFlight = root1;
            else timeOfFlight = 0;
        }
        else if(root1 < root2 && root1 > 0) timeOfFlight = root1;
        else if(root2 > 0) timeOfFlight = root2;
        else timeOfFlight = 0;
        
        finalTargetPosition.x = vectorToTarget.x + (targetVelocity.x * timeOfFlight);
        finalTargetPosition.y = vectorToTarget.y + (targetVelocity.y * timeOfFlight);
        return finalTargetPosition;
    }
    else
    {
        Vec2 currentTargetPosition = vectorToTarget;
        double targetPositionChangeLength = 1000;
        
        //CCLOG("1! T: %f L: %f A: %f ",timeOfFlight,targetPositionChangeLength,angle * R2D);
        int loopCount = 0;
        
        while(fabs(targetPositionChangeLength) > 5 && loopCount < 5)
        {
            double angle = VectorMath::computeFiringAngleWithVelocity(projectileVelocity, currentTargetPosition.length(), type);
            double timeOfFlight = VectorMath::computeTimeOfFlightWithVelocity(projectileVelocity, angle, 0);
            if(timeOfFlight < 0)
            {
                return currentTargetPosition;
            }
            
            Vec2 nextTargetPosition = vectorToTarget + (targetVelocity * timeOfFlight);
            targetPositionChangeLength = (currentTargetPosition - nextTargetPosition).length();
            currentTargetPosition = nextTargetPosition;
            loopCount++;
        }        
        return currentTargetPosition;
    }
}


double VectorMath::computeTimeOfFlightWithVelocity(double velocity, double angle, double heightDifference)
{
    double timeOfFlight = 0;
    double gravity = globalVariableStore->getVariable(ProjectileGravity );

    double velocitySinAngle = velocity * sin(fabs(angle));
    timeOfFlight = (velocitySinAngle  * velocitySinAngle) + (2 * gravity * heightDifference);
    if(timeOfFlight <= 0) return -1;
    
    timeOfFlight = sqrt(timeOfFlight);
    timeOfFlight += velocitySinAngle;
    timeOfFlight /= gravity;
    
    return timeOfFlight;
}


double VectorMath::computeFiringAngleWithVelocity(double velocity, double range, WEAPON_PROJECTILE_TYPE highArc)
{
    double velocitySqr = velocity * velocity;
    double gravity = globalVariableStore->getVariable(ProjectileGravity );

    double angle = gravity * (gravity * (range * range));
    
    
    angle = sqrt((velocitySqr * velocitySqr) - angle);
    
    if(highArc == INDIRECT_HIGH)
    {
        angle =  atan((velocitySqr + angle) / (gravity * range));
    }
    else
    {
        angle = atan((velocitySqr - angle) / (gravity * range));
    }
    
    if(isnan(angle) || fabs(angle) > M_PI_2)
    {
        CCLOG("eep! \n");
        return 0;
    }
    
    return angle;
}



double VectorMath::computeMaximumWeaponRangeWithSpeed(double speed)
{
    return ((speed * speed) / globalVariableStore->getVariable(ProjectileGravity)) * .9;
}


Vec2 VectorMath::unitVectorFromFacing(int facing)
{
    facing = facing % 8;
    if(facing < 0)
    {
        facing += 8;
    }

    DIRECTION value = (DIRECTION) facing;
    switch(value)
    {
        case RIGHT:
            return Vec2(1,0);
        case UP_RIGHT:
            return Vec2(COS_45,COS_45);
        case UP:
            return Vec2(0,1);
        case UP_LEFT:
            return Vec2(-COS_45,COS_45);
        case LEFT:
            return Vec2(-1,0);
        case DOWN_LEFT:
            return Vec2(-COS_45,-COS_45);
        case DOWN:
            return Vec2(0,-1);
        case DOWN_RIGHT:
            return Vec2(COS_45,-COS_45);
        default:
            return Vec2(0,0);
    }
}

Vec2 VectorMath::vectorFromFacing(int facing)
{
    facing = facing % 8;
    if(facing < 0)
    {
        facing += 8;
    }

    DIRECTION value = (DIRECTION) facing;
    switch(value)
    {
        case RIGHT:
            return Vec2(1,0);
        case UP_RIGHT:
            return Vec2(1,1);
        case UP:
            return Vec2(0,1);
        case UP_LEFT:
            return Vec2(-1,1);
        case LEFT:
            return Vec2(-1,0);
        case DOWN_LEFT:
            return Vec2(-1,-1);
        case DOWN:
            return Vec2(0,-1);
        case DOWN_RIGHT:
            return Vec2(1,-1);
        default:
            return Vec2(0,0);
    }
}

DIRECTION VectorMath::cardinalDireectionFromFacing(int facing)
{
    if(facing % 2)
    {
        return (DIRECTION) (facing -1);
    }
    return (DIRECTION) facing;
}

DIRECTION VectorMath::rotateDirectionBySteps(int direction, int steps)
{
    direction += steps;
    direction = direction % 8;
    if(direction < 0)
    {
        direction += 8;
    }
    return (DIRECTION) direction;
}


DIRECTION VectorMath::facingFromVector(Vec2 vector)
{
    return facingFromRadians(vector.getAngle());
}

DIRECTION VectorMath::facingFromRadians(double radians)
{
    //  You wouldn't think getting facing using a bunch of if statements (branching) would be particularly fast given that braches are generally very slow, but this method was faster than dividing radians into 8 buckets and casting the result into facing
    if(radians >= 0)
    {
        if(radians >= M_PI_7_8)
        {
            return LEFT;
        }
        else if(radians >= M_PI_5_8)
        {
            return UP_LEFT;
        }
        else if(radians >= M_PI_3_8)
        {
            return UP;
        }
        else if(radians >= M_PI_8)
        {
            return UP_RIGHT;
        }
        else
        {
            return RIGHT;
        }
    }
    else
    {
        radians = -1 * radians;
        if(radians >= M_PI_7_8)
        {
            return LEFT;
        }
        else if(radians >= M_PI_5_8)
        {
            return DOWN_LEFT;
        }
        else if(radians >= M_PI_3_8)
        {
            return DOWN;
        }
        else if(radians >= M_PI_8)
        {
            return DOWN_RIGHT;
        }
        else
        {
            return RIGHT;
        }
    }
}

int VectorMath::degreesFromFacing(DIRECTION facing)
{
    return (int) facing * 45;
}

double VectorMath::radiansFromFacing(DIRECTION facing)
{
    return VectorMath::degreesFromFacing(facing) * D2R;
}

int VectorMath::facingDifference(DIRECTION a, DIRECTION b)
{
    int diff = a - b;
    if( diff < -4)
    {
        diff += 8;
    }
    else if(diff > 4)
    {
        diff -= 8;
    }
    return diff;
}

double VectorMath::signedAngleBetweenVecs(Vec2 from, Vec2 to)
{
    double angle = from.getAngle(to);
    if(angle < -M_PI)
    {
        angle += M_PI * 2;
    }
    else if(angle > M_PI)
    {
        angle -= M_PI * 2;
    }
    
    return angle;
}

int VectorMath::encodedAngleBetweenVecs(Vec2 from, Vec2 to)
{
    Vec2 diff = (to - from).getNormalized();
    double ang = Vec2(1,0).getAngle(diff);
    if(ang < 0)
    {
        ang += M_PI * 2;
    }
    return (ang * MIN_ANGLE_TO_INT);
}

Vec2 VectorMath::encodedAngleToVec(int encodedAng)
{
    double ang = encodedAng;
    ang /= MIN_ANGLE_TO_INT;
    return Vec2::forAngle(ang);
}



/*
 * Check to see whether the circumfrence of a circle runs through a rectangle
 *
 */
bool VectorMath::rectOnCircumfrenceWithCenter(Vec2 center, double radius, Rect rect)
{
    //for each corner of the rectange, see if it is within the radius of the circle
    int pointsWithin = 0;
    if(rect.origin.distance(center) < radius)
        pointsWithin++;
    
    
    if(Vec2(rect.origin.x + rect.size.width,rect.origin.y).distance(center) < radius)
        pointsWithin++;
    
    if(Vec2(rect.origin.x + rect.size.width,rect.origin.y + rect.size.height).distance(center)
 < radius)
        pointsWithin++;
    
    if(Vec2(rect.origin.x,rect.origin.y + rect.size.height).distance(center) < radius)
        pointsWithin++;
    
    //if 0 points or 4 points are within the circle, then the circumfrence does not cross through the rect
    if(pointsWithin > 0 && pointsWithin < 4)
        return true;
    return false;
}

bool VectorMath::lineIntersectsRect(Vec2 start, Vec2 end, Rect & rect)
{
    if(rect.containsPoint(start))
    {
        return true;
    }
    
    ////bottom left to bottom right check
    Vec2 boundEdgeStart = rect.origin;
    Vec2 boundEdgeEnd = rect.origin;
    boundEdgeEnd.x += rect.size.width;
    
    if(Vec2::isSegmentIntersect(start, end, boundEdgeStart, boundEdgeEnd))
    {
        return true;
    }
    
    //bottom right to top right
    boundEdgeStart = boundEdgeEnd;
    boundEdgeEnd.y += rect.size.height;
    
    if(Vec2::isSegmentIntersect(start, end, boundEdgeStart, boundEdgeEnd))
    {
        return true;
    }
    
    //top right to top left
    boundEdgeStart = boundEdgeEnd;
    boundEdgeEnd.x = rect.origin.x;
    
    if(Vec2::isSegmentIntersect(start, end, boundEdgeStart, boundEdgeEnd))
    {
        return true;
    }
    //top left to bottom left
    boundEdgeStart = boundEdgeEnd;
    boundEdgeEnd.y = rect.origin.y;
    
    if(Vec2::isSegmentIntersect(start, end, boundEdgeStart, boundEdgeEnd))
    {
        return true;
    }
    return false;
}

std::list<Vec2> VectorMath::getLineIntersectionsWithRect(Vec2 start, Vec2 end, Rect & rect)
{
    std::list<Vec2> intersections;
    /// hmmmmm why
    Vec2 bottomLeft(rect.getMinX(),rect.getMinY());
    Vec2 bottomRight(rect.getMaxX(),rect.getMinY());
    Vec2 topLeft(rect.getMinX(),rect.getMaxY());
    Vec2 topRight(rect.getMaxX(),rect.getMaxY());
    
    Vec2 leftIntersection = VectorMath::getIntersectionBetweenLineSegments(start, end, bottomLeft, topLeft); //
    if(leftIntersection != Vec2(100000000,-1))
    {
        intersections.push_back(leftIntersection);
    }
    Vec2 rightIntersection  = VectorMath::getIntersectionBetweenLineSegments(start, end, bottomRight, topRight); //
    if(rightIntersection != Vec2(100000000,-1))
    {
        intersections.push_back(rightIntersection);
        if(intersections.size() == 2)
        {
            return intersections;
        }
    }
    Vec2 topIntersection  = VectorMath::getIntersectionBetweenLineSegments(start, end, topLeft, topRight); //
    if(topIntersection != Vec2(100000000,-1))
    {
        intersections.push_back(topIntersection);
        if(intersections.size() == 2)
        {
            return intersections;
        }
    }
    Vec2 bottomIntersection  = VectorMath::getIntersectionBetweenLineSegments(start, end, bottomLeft, bottomRight); //
    if(bottomIntersection != Vec2(100000000,-1))
    {
        intersections.push_back(bottomIntersection);
    }
    return intersections;
}


Vec2 VectorMath::positionOnLineBeyondRect(Vec2 lineStart, Vec2 lineEnd, Rect rect)
{
    if(lineEnd.x > rect.getMaxX())
    {
        //bottom right to top right
        Vec2 boundEdgeStart = Vec2(rect.getMaxX(), rect.origin.y);
        Vec2 boundEdgeEnd = boundEdgeStart;
        boundEdgeEnd.y += rect.size.height;
        Vec2 intersect = Vec2::getIntersectPoint(lineStart, lineEnd, boundEdgeStart, boundEdgeEnd);
        if(!intersect.isZero())
        {
            intersect.x += .1; //slightly out of rect
            return intersect;
        }
    }
    else
    {
        //bottom left to top left
        Vec2 boundEdgeStart = rect.origin;
        Vec2 boundEdgeEnd = boundEdgeStart;
        boundEdgeEnd.y += rect.size.height;
        Vec2 intersect = Vec2::getIntersectPoint(lineStart, lineEnd, boundEdgeStart, boundEdgeEnd);
        if(!intersect.isZero())
        {
            intersect.x -= .1; //slightly out of rect
            return intersect;
        }
    }
    
    if(lineEnd.y > rect.getMaxY())
    {
        //top left to top right
        Vec2 boundEdgeStart = Vec2(rect.origin.x, rect.getMaxY());
        Vec2 boundEdgeEnd = boundEdgeStart;
        boundEdgeEnd.x += rect.size.width;
        Vec2 intersect = Vec2::getIntersectPoint(lineStart, lineEnd, boundEdgeStart, boundEdgeEnd);
        if(!intersect.isZero())
        {
            intersect.y += .1; //slightly out of rect
            return intersect;
        }
    }
    else
    {
        // bottom left to bottom right check
        Vec2 boundEdgeStart = rect.origin;
        Vec2 boundEdgeEnd = boundEdgeStart;
        boundEdgeEnd.x += rect.size.width;
        Vec2 intersect = Vec2::getIntersectPoint(lineStart, lineEnd, boundEdgeStart, boundEdgeEnd);
        if(!intersect.isZero())
        {
            intersect.y -= .1; //slightly out of rect
            return intersect;
        }
    }
    return lineStart;
}


Vec2 VectorMath::getIntersectionBetweenLineSegments(const Vec2& A, const Vec2& B, const Vec2& C, const Vec2& D)
{
    float S, T;
    
    if (Vec2::isLineIntersect(A, B, C, D, &S, &T) &&
        (S >= 0.0f && S <= 1.0f && T >= 0.0f && T <= 1.0f))
    {
        Vec2 P;
        P.x = A.x + S * (B.x - A.x);
        P.y = A.y + S * (B.y - A.y);
        return P;
    }
    
    return Vec2(100000000,-1);
}

std::pair<bool,bool> VectorMath::getIntersectionBetweenLineSegmentAndCircle(const Vec2& lineStart, const Vec2& lineEnd, const Vec2& center, float radius, Vec2& intersection1, Vec2& intersection2)
{
    Vec2 direction = lineEnd - lineStart;
    Vec2 toCenter = lineStart - center;
    
    float a = direction.dot(direction);
    float b = 2 * toCenter.dot(direction);
    float c = toCenter.dot(toCenter) - radius * radius;
    
    float discriminant = b*b-4*a*c;
    if( discriminant < 0 )
    {
        return std::pair<bool, bool>(false, false);
    }
    else
    {
        bool startIntersection = false;
        bool endIntersection = false;
        discriminant = sqrt( discriminant );
        
        // either solution may be on or off the ray so need to test both
        // t1 is always the smaller value, because BOTH discriminant and
        // a are nonnegative.
        float t1 = (-b - discriminant)/(2*a);
        float t2 = (-b + discriminant)/(2*a);
        
        
        // 3x HIT cases:
        //          -o->             --|-->  |            |  --|->
        // Impale(t1 hit,t2 hit), Poke(t1 hit,t2>1), ExitWound(t1<0, t2 hit),
        
        // 3x MISS cases:
        //       ->  o                     o ->              | -> |
        // FallShort (t1>1,t2>1), Past (t1<0,t2<0), CompletelyInside(t1<0, t2>1)
        if( t1 >= 0 && t1 <= 1 )
        {
            // t1 is the intersection, and it's closer than t2
            // (since t1 uses -b - discriminant)
            // Impale, Poke
            intersection1 = lineStart + (direction * t1);
            startIntersection = true;
        }
        else
        {
            intersection1 = lineStart;
        }
        
        // here t1 didn't intersect so we are either started
        // inside the sphere or completely past it
        if( t2 >= 0 && t2 <= 1 )
        {
            // ExitWound
            intersection2 = lineStart + (direction * t2);
            endIntersection = true;
        }
        else
        {
            intersection2 = lineEnd;
        }
        
        if(t1 < 0 && t2 > 1)
        {
            return std::pair<bool, bool>(true, true); // consider completely inside to be intersecting
        }
        
        // no intn: FallShort, Past
        return std::pair<bool, bool>(startIntersection, endIntersection);
    }
}

float VectorMath::fastSqrt(float x)
{
//    return sqrt(x);
    union
    {
        int i;
        float x;
    } u;
    u.x = x;
    u.i = (1<<29) + (u.i >> 1) - (1<<22);
    
    // Two Babylonian Steps (simplified from:)
    // u.x = 0.5f * (u.x + x/u.x);
    // u.x = 0.5f * (u.x + x/u.x);
    u.x =       u.x + x/u.x;
    u.x = 0.25f*u.x + x/u.x;
    
    return u.x;
}  

Rect VectorMath::getRectIntersection(Rect & a, Rect & b)
{
    float x = std::max(a.getMinX(), b.getMinX());
    float y = std::max(a.getMinY(), b.getMinY());
    float xMax = std::min(a.getMaxX(), b.getMaxX());
    float yMax = std::min(a.getMaxY(), b.getMaxY());
    
    return Rect(x, y, xMax - x, yMax - y);
}
