//
//  InteractiveObjectUtils.cpp
//  TrenchWars
//
//  Created by Paul Reed on 12/12/22.
//

#include "InteractiveObjectUtils.h"
#include "Infantry.h"
#include "Emblem.h"
#include "Courier.h"
#include "CrewWeapon.h"
#include "CommandPost.h"
#include "PhysicalEntity.h"
#include "StaticEntity.h"
#include "Projectile.h"

#include "UnitGroup.h"
#include "CrewWeaponUnit.h"
#include "ProposedEntity.h"
#include "Objective.h"
#include "PolygonLandscapeObject.h"
#include "EmblemManager.h"
#include "EntityManager.h"
#include "World.h"
#include "CollisionGrid.h"
#include "VisibilityTester.h"

Entity * InteractiveObjectUtils::createEntityOfType(ENTITY_TYPE type)
{
    switch (type) {
        case INFANTRY: {
            return Infantry::create();
        }
        case COURIER: {
            return Courier::create();
        }
        case CREW_WEAPON: {
            return CrewWeapon::create();
        }
        case COMMANDPOST: {
            return CommandPost::create();
        }
        case BUNKER: {
            return Bunker::create();
        }
        case PROJECTILE: {
            return Projectile::create();
        }
        case STATIC_ENTITY: {
            return StaticEntity::create();
        }
        case OBJECTIVE_ENTITY: {
            return Objective::create();
        }
        case UNIT: {
            return Unit::create();
        }
        case CREW_WEAPON_UNIT: {
            return CrewWeaponUnit::create();
        }
        case PROPOSED_TRENCH: {
            return ProposedTrench::create();
        }
        case PROPOSED_BUNKER: {
            return ProposedBunker::create();
        }
        case PROPOSED_COMMAND_POST: {
            return ProposedCommandPost::create();
        }
        case DEBUG_ENTITY:
        {
            return nullptr;
        }
        case OTHER: {
            return nullptr;
        }
    }
    return nullptr;
}

Vec2 InteractiveObjectUtils::getPositionForObject(InteractiveObject * object)
{
    switch(object->getInteractiveObjectType())
    {
        case ENTITY_INTERACTIVE_OBJECT:
        {
            Entity * entity = dynamic_cast<Entity *>(object);
            switch(entity->getEntityType())
            {
                case STATIC_ENTITY:
                case PROJECTILE:
                case BUNKER:
                case COMMANDPOST:
                case CREW_WEAPON:
                case COURIER:
                case INFANTRY:
                {
                    PhysicalEntity * physics = dynamic_cast<PhysicalEntity *>(entity);
                    return physics->getPosition();
                }
                case CREW_WEAPON_UNIT:
                case UNIT:
                {
                    Unit * command = (Unit *) object;
                    return command->getAveragePosition();
                }
                case OBJECTIVE_ENTITY:
                {
                    Objective * objective = (Objective *) object;
                    return objective->getCenterPoint();
                }
                case OTHER:
                case DEBUG_ENTITY: {
                    return Vec2::ZERO;
                }
            }
        }
        case EMBLEM_INTERACTIVE_OBJECT: {
            Emblem * emblem = (Emblem *) object;
            return emblem->getPosition();
        }
        case POLYGON_LANDSCAPE_INTERACTIVE_OBJECT: {
            return Vec2::ZERO;
        }
        case TESTER_INTERACTIVE_OBJECT:
        {
            Tester * test = (Tester *) object;
            return test->getPosition();
        }
        case UNIT_GROUP_INTERACTIVE_OBJECT:
        {
            UnitGroup * command = (UnitGroup *) object;
            return command->getPosition();
        }
        default:
            return Vec2::ZERO;
    }
    
}


Vec2 InteractiveObjectUtils::getCenterOfObjectPosition(InteractiveObject * object)
{
    switch(object->getInteractiveObjectType())
    {
        case ENTITY_INTERACTIVE_OBJECT:
        {
            Entity * entity = dynamic_cast<Entity *>(object);
            switch(entity->getEntityType())
            {
                case STATIC_ENTITY:
                case PROJECTILE:
                case BUNKER:
                case COMMANDPOST:
                case CREW_WEAPON:
                case COURIER:
                case INFANTRY:
                {
                    PhysicalEntity * physics = dynamic_cast<PhysicalEntity *>(entity);
                    Vec2 position = physics->getPosition();
                    position.x += physics->getCollisionBounds().size.width / 2;
                    position.y += physics->getCollisionBounds().size.height / 2;
                    return position;
                }
                case CREW_WEAPON_UNIT:
                case UNIT:
                {
                    Unit * command = (Unit *) object;
                    return command->getAveragePosition();
                }
                case OBJECTIVE_ENTITY:
                {
                    Objective * objective = (Objective *) object;
                    return objective->getCenterPoint();
                }
                case OTHER:
                case DEBUG_ENTITY: {
                    return Vec2::ZERO;
                }
            }
        }
        case EMBLEM_INTERACTIVE_OBJECT: {
            Emblem * emblem = (Emblem *) object;
            return emblem->getPosition();
        }
        case TESTER_INTERACTIVE_OBJECT:
        {
            Tester * test = (Tester *) object;
            return test->getPosition();
        }
        case UNIT_GROUP_INTERACTIVE_OBJECT:
        {
            UnitGroup * command = (UnitGroup *) object;
            return command->getPosition();
        }
        case POLYGON_LANDSCAPE_INTERACTIVE_OBJECT: {
            return Vec2::ZERO;
        }
    }
}

Size InteractiveObjectUtils::getObjectSize(InteractiveObject * object)
{
    switch(object->getInteractiveObjectType())
    {
        case ENTITY_INTERACTIVE_OBJECT:
        {
            Entity * entity = dynamic_cast<Entity *>(object);
            switch(entity->getEntityType())
            {
                case STATIC_ENTITY:
                case PROJECTILE:
                case BUNKER:
                case COMMANDPOST:
                case CREW_WEAPON:
                case COURIER:
                case INFANTRY:
                {
                    PhysicalEntity * physics = dynamic_cast<PhysicalEntity *>(entity);
                    return physics->getCollisionBounds().size;
                }
                case CREW_WEAPON_UNIT:
                case UNIT:
                case OBJECTIVE_ENTITY:
                case OTHER:
                case DEBUG_ENTITY: {
                    return Size::ZERO;
                }
            }
        }
        case EMBLEM_INTERACTIVE_OBJECT: {
            Emblem * emblem = (Emblem *) object;
            return emblem->getBoundingBox().size * ONE_OVER_WORLD_TO_GRAPHICS_SIZE;
        }
        case TESTER_INTERACTIVE_OBJECT:
        case UNIT_GROUP_INTERACTIVE_OBJECT:
        case POLYGON_LANDSCAPE_INTERACTIVE_OBJECT: {
            return Size::ZERO;
        }
    }
}

std::string InteractiveObjectUtils::getEntityTypeName(const Entity * ent)

{
    return getEntityTypeName(ent->getEntityType());
}

std::string InteractiveObjectUtils::getEntityTypeName(ENTITY_TYPE type)
{
    switch (type) {
        case INFANTRY: {
            return "Infantry";
        }
        case COURIER: {
            return "Courier";
        }
        case CREW_WEAPON: {
            return "CrewWeapon";
        }
        case COMMANDPOST: {
            return "CommandPost";
        }
        case BUNKER: {
            return "Bunker";
        }
        case PROJECTILE: {
            return "Projectile";
        }
        case STATIC_ENTITY: {
            return "StaticEntity";
        }
        case PROPOSED_TRENCH: {
            return "ProposedTrench";
        }
        case PROPOSED_BUNKER: {
            return "ProposedBunker";
        }
        case PROPOSED_COMMAND_POST: {
            return "ProposedCommandPost";
        }
        case DEBUG_ENTITY:
        {
            return "Debug";
        }
        case OTHER: {
            return "Other";
        }
    }
    return "Unknown";
}

InteractiveObject * InteractiveObjectUtils::getInteractiveObject(ENTITY_ID objectID, INTERACTIVE_OBJECT_TYPE type)
{
    switch (type) {
        case ENTITY_INTERACTIVE_OBJECT:
        {
            return globalEntityManager->getEntity(objectID);
        }
        case EMBLEM_INTERACTIVE_OBJECT:
        {
            return globalEmblemManager->getEmblem(objectID);
        }
        case POLYGON_LANDSCAPE_INTERACTIVE_OBJECT:
        {
            return world->getLandscapeObject(objectID);
        }
        default:
        {
            return nullptr;
        }
    }
}
