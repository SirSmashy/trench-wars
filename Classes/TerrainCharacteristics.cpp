//
//  TerrainCharacteristics.cpp
//  TrenchWars
//
//  Created by Paul Reed on 9/25/21.
//

#include "TerrainCharacteristics.h"
#include "GameVariableStore.h"
#include <algorithm>

#define HARD_COVER_MULTIPLIER 10


std::mutex terrainCostsCacheMutex;
std::map<unsigned long long, short> hardCoverCache;
std::map<unsigned long long, short> softCoverCache;

const float SPEED_MULTIPLIERS [] = {-2, -1, 10, 50, 100};
const float MAX_MOVE_COST = 2500;


TerrainCharacteristics::TerrainCharacteristics()
{
}

TerrainCharacteristics::TerrainCharacteristics(char height, unsigned char cover, unsigned char hardness, bool impassable, bool unique)
{
    set(height, cover, hardness, impassable, unique);
}

void TerrainCharacteristics::set(char height, unsigned char cover, unsigned char hardness, bool impassable, bool unique)
{
    _height = height;
    _cover = cover;
    _hardness = hardness;
    _impassable = impassable;
    _unique = unique;
    
    //Derived variables
    double concealment = pow(_cover, globalVariableStore->getVariable(CoverToConcealmentExponent) );
    _concealmentValue = (concealment / globalVariableStore->getVariable(CoverToConcealmentMaxValue)) * 100;
    _inverseCover = 100 - _cover;

    float coverTimesHardness = _hardness;
    coverTimesHardness /= 100;
    coverTimesHardness *= _cover; //range is 0-100
    _securityWithoutHeightCover = (coverTimesHardness * HARD_COVER_MULTIPLIER) + _concealmentValue; //range is 0-1100
    _totalSecurity = _securityWithoutHeightCover + ( getHeightChanceToHit(0) * HARD_COVER_MULTIPLIER);
    _hash = ( static_cast<int>(_height) << 12) + (static_cast<int>(_cover) << 8) + ( static_cast<int>(_hardness) << 4) + _impassable;
}

bool TerrainCharacteristics::operator!= (const TerrainCharacteristics & other ) const
{
    
    if(_unique || other._unique)
    {
        return true;
    }
    
    if(_hash != other._hash)
    {
        return true;
    }
    return false;
}

bool TerrainCharacteristics::operator== (const TerrainCharacteristics & other ) const
{
    if(_unique || other._unique)
    {
        return false;
    }
    
    if(_hash == other._hash)
    {
        return true;
    }
    return false;
}

unsigned char TerrainCharacteristics::getHeightChanceToHit(char height) const
{
    return std::min(100, (int) (std::max(height - _height, 0) * globalVariableStore->getVariable(HeightDifferenceCoverValue)));
}

/*
 * Determine the cost to move through this cell based on the passed in preference.
 * A cost of zero means this is ideal terrain to move through (for the passed in perference). A cost above that is less than ideal
 */
float TerrainCharacteristics::getMovementCost(MOVEMENT_PREFERENCE movePreference, bool ignoreImpassable) const
{
    if(_impassable && !ignoreImpassable)
    {
        return 1000000000;
    }
    
    return MAX_MOVE_COST - (_totalSecurity - (_coverTimesHardness * SPEED_MULTIPLIERS[movePreference]));
}
