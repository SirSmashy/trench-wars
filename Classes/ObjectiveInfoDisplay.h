//
//  CommandPostInfo.hpp
//  TrenchWars
//
//  Created by Paul Reed on 3/12/22.
//

#ifndef ObjectiveInfoDisplay_h
#define ObjectiveInfoDisplay_h

#include "AutoSizedLayout.h"
#include "Objective.h"


class ObjectiveInfoDisplay : public AutoSizedLayout
{
protected:
    Objective * _objective;
    
    TextField * _objectiveName;
    Text * _objectiveAmmoReserves;

    ObjectiveInfoDisplay();
    ~ObjectiveInfoDisplay();

    bool init(Objective * objective);
    
    void addText(std::string textString);

public:
    
    static ObjectiveInfoDisplay * create(Objective * objective);
    void openNameTextField();
    
    void update();
};


#endif /* ObjectiveInfoDisplay_h */
