//
//  WorldLayer.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/19/24.
//

#ifndef WorldLayer_h
#define WorldLayer_h

#include "cocos2d.h"
USING_NS_CC;


class WorldLayer: public Node
{
private:
    WorldLayer();
    virtual bool init();
    
public:
    static WorldLayer * create();
    
    virtual void visit(Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags);
};

#endif /* WorldLayer_h */
