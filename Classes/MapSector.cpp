//
//  MapSector.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/10/12.
//  Copyright (c) 2012 . All rights reserved.
//

#include "MapSector.h"
#include "EngagementManager.h"
#include "MovingPhysicalEntity.h"
#include "SectorGrid.h"
#include "PathFinding.h"
#include "GameVariableStore.h"
#include "BackgroundTaskHandler.h"
#include "VectorMath.h"


MapSector::MapSector(int indexX, int indexY, int xCellSize, int yCellSize, int cellsInSector)
{
    _sectorWidthInCells = cellsInSector;
    int cellIndexX = indexX * _sectorWidthInCells;
    int cellIndexY = indexY * _sectorWidthInCells;

    _cellsRect.set(cellIndexX, cellIndexX + xCellSize -1 , cellIndexY, cellIndexY + yCellSize - 1);
    _mapSectorIndexX = indexX;
    _mapSectorIndexY = indexY;

    _sectorBounds.origin.x = _mapSectorIndexX * _sectorWidthInCells;
    _sectorBounds.origin.y = _mapSectorIndexY * _sectorWidthInCells;
    _sectorBounds.size.width = xCellSize;
    _sectorBounds.size.height = yCellSize;
    
    int xCellCoordinate;
    int yCellCoordinate;
    _collisionCellsInSector = new CollisionCell ** [_sectorWidthInCells];
    _rebuildPathTime = -1;
    _totalSecurity = 0;
    for(int x = 0; x < xCellSize; x++)
    {
        xCellCoordinate = (indexX * _sectorWidthInCells) + x;
        _collisionCellsInSector[x] = new CollisionCell * [_sectorWidthInCells];

        for(int y = 0; y < yCellSize; y++)
        {
            yCellCoordinate = (indexY * _sectorWidthInCells) + y;
            _collisionCellsInSector[x][y] = world->getCollisionGrid()->getCell(xCellCoordinate, yCellCoordinate);
            _collisionCellsInSector[x][y]->setMapSector(this);
            _totalSecurity += _collisionCellsInSector[x][y]->getTerrainCharacteristics().getTotalSecurity();
        }
    }
    
    _pathingCell = _collisionCellsInSector[_cellsRect.getXMid() - _cellsRect.getMinX()][_cellsRect.getYMid() - _cellsRect.getMinY()];
    xCellCoordinate = 0;
    
    for(int i = 0; i < 10; i++)
    {
        _congestionInSector[i] = 1.0;
    }
    _congestionPerPerson = (1.0 / (_sectorWidthInCells * _sectorWidthInCells)) * (globalVariableStore->getVariable(PathingCongestionCostMultiplier));
    
    // The middle cell isn't traversable, so search in an expanding square around the center until a traversable cell is found
    if(!_pathingCell->isTraversable())
    {
        bool foundValid = false;
        int max = std::max(_cellsRect.getXSize(), _cellsRect.getYSize()) / 2;
        
        // With each loop the square grows larger
        for(int maxSize = 1; maxSize < max; maxSize++)
        {
            int minX = std::max(_cellsRect.getMinX(),_cellsRect.getXMid() - maxSize);
            int minY = std::max(_cellsRect.getMinY(),_cellsRect.getYMid() - maxSize);

            int maxX = std::min(_cellsRect.getMaxX(),_cellsRect.getXMid() + maxSize);
            int maxY = std::min(_cellsRect.getMaxY(),_cellsRect.getYMid() + maxSize);
            
            // Walk along the top of the square
            int y = maxY;
            int x = minX;
            while(x <= maxX)
            {
                _pathingCell = _collisionCellsInSector[x - cellIndexX][y - cellIndexY];
                if(_pathingCell->isTraversable())
                {
                    foundValid = true;
                    break;
                }
                x++;
            }
            if(foundValid)
            {
                break;
            }
            //Walk along the right side of the square
            x = maxX;
            y = maxY - 1;
            while(y >= minY)
            {
                _pathingCell = _collisionCellsInSector[x - cellIndexX][y - cellIndexY];
                if(_pathingCell->isTraversable())
                {
                    foundValid = true;
                    break;
                }
                y--;
            }
            if(foundValid)
            {
                break;
            }
            // Walk along the bottom side of the square
            y = minY;
            x = maxX - 1;
            while(x >= minX)
            {
                _pathingCell = _collisionCellsInSector[x - cellIndexX][y - cellIndexY];
                if(_pathingCell->isTraversable())
                {
                    foundValid = true;
                    break;
                }
                x--;
            }
            if(foundValid)
            {
                break;
            }
            // Walk along the left side of the square
            x = minX;
            y = minY + 1;
            while(y < maxY)
            {
                _pathingCell = _collisionCellsInSector[x - cellIndexX][y - cellIndexY];
                if(_pathingCell->isTraversable())
                {
                    foundValid = true;
                    break;
                }
                y++;
            }
            if(foundValid)
            {
                break;
            }
        }
        if(!foundValid)
        {
            _pathingCell = theInvalidCell;
        }
    }
}

MapSector * MapSector::create(int indexX, int indexY, int xCellSize, int yCellSize, int cellsInSector)
{
    MapSector * sector = new MapSector(indexX, indexY, xCellSize, yCellSize, cellsInSector);
    return sector;
}


CollisionCell * MapSector::getCell(int x, int y)
{
    return _collisionCellsInSector[x][y];
}

CollisionCell * MapSector::getSectorCellNearestCell(CollisionCell * other)
{
    int x = other->getXIndex();
    int y = other->getYIndex();
    
    if(x > _cellsRect.getMaxX())
    {
        x = _cellsRect.getMaxX();
    }
    else if(x < _cellsRect.getMinX())
    {
        x = _cellsRect.getMinX();
    }
    
    if(y > _cellsRect.getMaxY())
    {
        y = _cellsRect.getMaxY();
    }
    else if(y < _cellsRect.getMinY())
    {
        y = _cellsRect.getMinY();
    }
    
    x -= _cellsRect.getMinX();
    y -= _cellsRect.getMinY();
    
    return _collisionCellsInSector[x][y];
}

CollisionCell * MapSector::getMiddleCell()
{
    return _collisionCellsInSector[(int)_sectorBounds.size.width/2][(int)_sectorBounds.size.height/2];
}

void MapSector::constrainPointToSector(Vec2 & point)
{
    if(point.x < _sectorBounds.origin.x)
    {
        point.x = _sectorBounds.origin.x;
    }
    else if(point.x >= _sectorBounds.getMaxX())
    {
        point.x = _sectorBounds.getMaxX() -1;
    }
    if(point.y < _sectorBounds.origin.y)
    {
        point.y = _sectorBounds.origin.y;
    }
    else if(point.y >= _sectorBounds.getMaxY())
    {
        point.y = _sectorBounds.getMaxY() -1;
    }
}

float MapSector::gridDistanceFromSector(MapSector * other)
{
    float x = _mapSectorIndexX - other->_mapSectorIndexX;
    float y = _mapSectorIndexY - other->_mapSectorIndexY;
    x *= x;
    y *= y;
    
    return sqrt(x+y);
}

void MapSector::addTeam()
{
    _modificationMutex.lock();
    _teamStrengthsInSector.emplace_back(std::unordered_set<MovingPhysicalEntity *>());
    _availableTotalSecurityForTeam.push_back(_totalSecurity);
    _teamVisiblityForSector.push_back(new SectorVisiblity());
    _modificationMutex.unlock();
}

int MapSector::getStrengthForTeam(int team)
{
    std::lock_guard<std::mutex> lock(_modificationMutex);
    return _teamStrengthsInSector[team].size();
}

int MapSector::getStrengthForTeamsOtherThanTeam(int team)
{
    _modificationMutex.lock();
    int strength = 0;
    for(int i = 0; i < _teamStrengthsInSector.size(); i++)
    {
        if(i == team)
            continue;
        strength += _teamStrengthsInSector[i].size();
    }
    _modificationMutex.unlock();
    return strength;
}

SectorVisiblity * MapSector::getSectorVisibilityForTeam(int team)
{
   return _teamVisiblityForSector[team];
}

void MapSector::markSectorVisibleForTeam(int team)
{
    SectorVisiblity * vis = _teamVisiblityForSector[team];
    for(auto addition : vis->entityAdditions)
    {
        vis->currentlyOrPreviouslyVisibleEnts.insert(addition);
    }
    vis->entityAdditions.clear();
    vis->entityRemovals.clear();
    vis->currentlyVisible = true;
    
}

void MapSector::markSectorHiddenForTeam(int team)
{
    SectorVisiblity * vis = _teamVisiblityForSector[team];
    vis->currentlyVisible = false;
}

// Entitites
void MapSector::addEntity(MovingPhysicalEntity * ent)
{
    _modificationMutex.lock();
    _teamStrengthsInSector[ent->getTeamID()].insert(ent);
    _congestionInSector[ent->getTeamID()] += _congestionPerPerson;
    _modificationMutex.unlock();

}

void MapSector::removeEntity(MovingPhysicalEntity * ent)
{
    _modificationMutex.lock();
    _teamStrengthsInSector[ent->getTeamID()].erase(ent);
    _congestionInSector[ent->getTeamID()] -= _congestionPerPerson;
    _modificationMutex.unlock();
}

void MapSector::staticEntAddedToSector(StaticEntity * ent)
{
    _modificationMutex.lock();
    for(auto vis : _teamVisiblityForSector)
    {
        vis->entityAdditions.insert(ent->uID());
        // Tell the Team something changed?
    }
    _modificationMutex.unlock();

}

void MapSector::staticEntRemovedFromSector(StaticEntity * ent)
{
    _modificationMutex.lock();
    for(auto vis : _teamVisiblityForSector)
    {
        vis->entityRemovals.insert(ent->uID());
        // Tell the Team something changed?
    }
    _modificationMutex.unlock();
}


// Position claiming
void MapSector::entityClaimedCellInSector(int entTeamId, CollisionCell * cell)
{
    _modificationMutex.lock();
    _availableTotalSecurityForTeam[entTeamId] = _availableTotalSecurityForTeam[entTeamId] - cell->getTerrainCharacteristics().getTotalSecurity();
    _modificationMutex.unlock();

}

void MapSector::entityReleasedClaimInSector(int entTeamId, CollisionCell * cell)
{
    _modificationMutex.lock();
    _availableTotalSecurityForTeam[entTeamId] = _availableTotalSecurityForTeam[entTeamId] + cell->getTerrainCharacteristics().getTotalSecurity();
    _modificationMutex.unlock();
}

// Clouds
void MapSector::addCloudToSector(Cloud * cloud)
{
    _modificationMutex.lock();
    _cloudsInSector.emplace(cloud->uID(), cloud);
    _modificationMutex.unlock();
}
void MapSector::removeCloudFromSector(Cloud * cloud)
{
    _modificationMutex.lock();
    _cloudsInSector.erase(cloud->uID());
    _modificationMutex.unlock();
}

const std::unordered_set<MovingPhysicalEntity *> & MapSector::getEntitiesForTeam(int team)
{
    std::lock_guard<std::mutex> lock(_modificationMutex);
    return _teamStrengthsInSector[team];
}

// Pathing
void MapSector::findPathsToNeighbors()
{
    //Right
    MapSector * neighbor = globalSectorGrid->sectorAtIndex(_mapSectorIndexX + 1, _mapSectorIndexY);
    if(neighbor != nullptr)
    {
        buildPathToNeighbor(neighbor);
    }
    //Right-Down
    neighbor = globalSectorGrid->sectorAtIndex(_mapSectorIndexX + 1, _mapSectorIndexY -1);
    if(neighbor != nullptr)
    {
        buildPathToNeighbor(neighbor);
    }
    //Down
    neighbor = globalSectorGrid->sectorAtIndex(_mapSectorIndexX, _mapSectorIndexY -1);
    if(neighbor != nullptr)
    {
        buildPathToNeighbor(neighbor);
    }
    //Left-Down
    neighbor = globalSectorGrid->sectorAtIndex(_mapSectorIndexX-1, _mapSectorIndexY -1);
    if(neighbor != nullptr)
    {
        buildPathToNeighbor(neighbor);
    }
    //Left
    neighbor = globalSectorGrid->sectorAtIndex(_mapSectorIndexX -1, _mapSectorIndexY);
    if(neighbor != nullptr)
    {
        buildPathToNeighbor(neighbor);
    }
    //Left-Up
    neighbor = globalSectorGrid->sectorAtIndex(_mapSectorIndexX -1, _mapSectorIndexY + 1);
    if(neighbor != nullptr)
    {
        buildPathToNeighbor(neighbor);
    }
    //Up
    neighbor = globalSectorGrid->sectorAtIndex(_mapSectorIndexX, _mapSectorIndexY + 1);
    if(neighbor != nullptr)
    {
        buildPathToNeighbor(neighbor);
    }
    //Right-Up
    neighbor = globalSectorGrid->sectorAtIndex(_mapSectorIndexX + 1, _mapSectorIndexY + 1);
    if(neighbor != nullptr)
    {
        buildPathToNeighbor(neighbor);
    }
}


void MapSector::buildPathToNeighbor(MapSector * neighbor)
{
    if(_pathingCell == theInvalidCell || neighbor->getPathingCell() == nullptr)
    {
        return;
    }
    
    int xMin = std::min(_cellsRect.getMinX(), neighbor->getCellsRect().getMinX());
    int xMax = std::max(_cellsRect.getMaxX(), neighbor->getCellsRect().getMaxX());
    int yMin = std::min(_cellsRect.getMinY(), neighbor->getCellsRect().getMinY());
    int yMax = std::max(_cellsRect.getMaxY(), neighbor->getCellsRect().getMaxY());

    NavigableRect rect;
    rect.set(xMin, xMax, yMin, yMax);
    double cost = globalPathFinding->getPathCostWithinInBounds(_pathingCell, neighbor->getPathingCell(), rect, MOVE_STRONGLY_PREFER_COVER);
    setPathToNeighbor(neighbor,cost);
    neighbor->setPathToNeighbor(this, cost);
}

void MapSector::setPathToNeighbor(MapSector * neighbor, double cost)
{
    if(_pathingCell == theInvalidCell || cost == -1)
    {
        _neighboringPaths.erase(neighbor);
    }
    else
    {
        _neighboringPaths.insert_or_assign(neighbor, cost);
    }
}

void MapSector::rebuildNeighborPaths(bool recurse)
{
    // hmmmmmm
    if(globalEngagementManager->getEngagementState() == ENGAGEMENT_STATE_LOADING)
    {
        return;
    }
    
    if(_rebuildPathTime == -1)
    {
        _rebuildPathTime = globalEngagementManager->getEngagementTime() + 2; //FIX magic number
    }
    else if(!recurse)
    {
        return;
    }
    
    globalEngagementManager->requestMainThreadFunction( [this] ()
    {
        if(globalEngagementManager->getEngagementTime() > _rebuildPathTime)
        {
            _rebuildPathTime = -1;
            globalBackgroundTaskHandler->addPathfindingTask([this] () {
                findPathsToNeighbors();
                globalSectorGrid->setRedraw();
                return false;
            });
        }
        else
        {
            rebuildNeighborPaths(true);
        }
    });
}



// Congestion
void MapSector::addCongestion(int team)
{
    _modificationMutex.lock();
    _congestionPerPerson = (1.0 / (_sectorWidthInCells)) * globalVariableStore->getVariable(PathingCongestionCostMultiplier);
    _congestionInSector[team] += _congestionPerPerson;
    _modificationMutex.unlock();
    
}

void MapSector::removeCongestion(int team)
{
    _modificationMutex.lock();
    _congestionInSector[team] -= _congestionPerPerson;
    _modificationMutex.unlock();
}

double MapSector::getCongestionForTeam(int team)
{
    return _congestionInSector[team];
}

CollisionCell * MapSector::getSectorBorderCellWithLeastCongestion(CollisionCell * ideal, int team)
{
    if(!_cellsRect.containsCoordinates(ideal->getXIndex(), ideal->getYIndex()))
    {
        return ideal;
    }
    
    double best = ideal->getCongestion(team);
    if(best == 0)
    {
        return ideal;
    }
    
    bool minX = ideal->getXIndex() == _cellsRect.getMinX();
    bool minY = ideal->getYIndex() == _cellsRect.getMinY();
    
    bool maxX = ideal->getXIndex() == _cellsRect.getMaxX();
    bool maxY = ideal->getYIndex() == _cellsRect.getMaxY();
    
    Vec2 clockwise;
    Vec2 counterClockwise;
    
    
    if(minX && maxY)
    {
        clockwise = Vec2(1,0);
        counterClockwise = Vec2(0,-1);
    }
    else if(minX && minY)
    {
        clockwise = Vec2(0,1);
        counterClockwise = Vec2(1,0);
    }
    else if(maxX && maxY)
    {
        clockwise = Vec2(0,-1);
        counterClockwise = Vec2(-1,0);
    }
    else if(maxX && minY)
    {
        clockwise = Vec2(-1,0);
        counterClockwise = Vec2(0,1);
    }
    else if(minX || maxX)
    {
        clockwise = Vec2(0,1);
        counterClockwise = Vec2(0,-1);
    }
    else
    {
        clockwise = Vec2(1,0);
        counterClockwise = Vec2(-1,-0);
    }
    
    CollisionCell * bestCell = ideal;
    Vec2 position = Vec2(ideal->getXIndex(), ideal->getYIndex());
    position += clockwise;
    int i = 1;
    int x;
    int y;
    while(_cellsRect.containsCoordinates(position.x, position.y))
    {
        x = position.x - _cellsRect.getMinX();
        y = position.y - _cellsRect.getMinY();
        CollisionCell * cell = _collisionCellsInSector[x][y];
        double congestion = cell->getCongestion(team);
        if(congestion <= best)
        {
            //            best = congestion;
            //            bestCell = cell;
            return cell;
        }
        i++;
        position += clockwise;
    }
    
    position = Vec2(ideal->getXIndex(), ideal->getYIndex());
    position += counterClockwise;
    i = 1;
    while(_cellsRect.containsCoordinates(position.x, position.y))
    {
        x = position.x - _cellsRect.getMinX();
        y = position.y - _cellsRect.getMinY();
        CollisionCell * cell = _collisionCellsInSector[x][y];
        double congestion = cell->getCongestion(team);
        if(congestion <= best)
        {
            //            best = congestion;
            //            bestCell = cell;
            return cell;
        }
        i++;
        position += counterClockwise;
    }
    
    return bestCell;
}


// Security
float MapSector::getAvailableTotalSecurityForTeam(int teamId)
{
    return _availableTotalSecurityForTeam[teamId];
}

void MapSector::changeTotalSecurity(float change)
{
    _modificationMutex.lock();
    for(int i = 0; i < _availableTotalSecurityForTeam.size(); i++)
    {
        _availableTotalSecurityForTeam[i] = _availableTotalSecurityForTeam[i] + change;
    }
    _totalSecurity += change;
    _modificationMutex.unlock();
    
}

CollisionCell * MapSector::getEdgeCellIntersectingLine(Vec2 start, Vec2 end)
{
    Vec2 intersectionPoint;
    // Deteremine if the line will insect a y axis (top or bottom ) of the sector bounds or an x axis (left or right) first
    start = (start - _sectorBounds.origin) / _sectorWidthInCells;
    end = (end - _sectorBounds.origin) / _sectorWidthInCells;
    Vec2 diff = end - start;
    
    double xDist = fabs(start.x - ( (int) start.x));
    if ( diff.x > 0)
        xDist = 1.0 - xDist;
    
    double yDist = fabs(start.y - ( (int) start.y));
    if ( diff.y > 0)
        yDist = 1.0 - yDist;
    
    double slope = (fabs(diff.y) / fabs(diff.x));
    double y = fabs(xDist * slope);
    
    if(y < yDist)
    {
        intersectionPoint.x = diff.x < 0 ? 0 : 0.9999; // make sure not to go over 1
        intersectionPoint.y = diff.y > 0 ? xDist * slope : -xDist * slope;
        intersectionPoint.y += start.y;
        if(intersectionPoint.y > .9999)
        {
            intersectionPoint.y = .9999;
        }
    }
    else
    {
        intersectionPoint.y = diff.y < 0 ? 0 : 0.9999; //make sure not to go over 1
        intersectionPoint.x = diff.x > 0 ? yDist / slope : -yDist / slope;
        intersectionPoint.x += start.x;
        if(intersectionPoint.x > .9999)
        {
            intersectionPoint.x = .9999;
        }
    }
    
    intersectionPoint *= _sectorWidthInCells;
    return _collisionCellsInSector[(int)intersectionPoint.x][(int)intersectionPoint.y];
}

std::list<Vec2> MapSector::getIntersectionsWithLine(Vec2 start, Vec2 end)
{
    std::list<Vec2> intersections;
    
    /// hmmmmm why
    Vec2 bottomLeft(_sectorBounds.getMinX(),_sectorBounds.getMinY());
    Vec2 bottomRight(_sectorBounds.getMaxX(),_sectorBounds.getMinY());
    Vec2 topLeft(_sectorBounds.getMinX(),_sectorBounds.getMaxY());
    Vec2 topRight(_sectorBounds.getMaxX(),_sectorBounds.getMaxY());
    
    Vec2 leftIntersection = VectorMath::getIntersectionBetweenLineSegments(start, end, bottomLeft, topLeft); //
    if(leftIntersection != Vec2(100000000,-1))
    {
        constrainPointToSector(leftIntersection);
        intersections.push_back(leftIntersection);
    }
    Vec2 rightIntersection  = VectorMath::getIntersectionBetweenLineSegments(start, end, bottomRight, topRight); //
    if(rightIntersection != Vec2(100000000,-1))
    {
        constrainPointToSector(rightIntersection);
        intersections.push_back(rightIntersection);
        if(intersections.size() == 2)
        {
            return intersections;
        }
    }
    Vec2 topIntersection  = VectorMath::getIntersectionBetweenLineSegments(start, end, topLeft, topRight); //
    if(topIntersection != Vec2(100000000,-1))
    {
        constrainPointToSector(topIntersection);
        intersections.push_back(topIntersection);
        if(intersections.size() == 2)
        {
            return intersections;
        }
    }
    Vec2 bottomIntersection  = VectorMath::getIntersectionBetweenLineSegments(start, end, bottomLeft, bottomRight); //
    if(bottomIntersection != Vec2(100000000,-1))
    {
        constrainPointToSector(bottomIntersection);
        intersections.push_back(bottomIntersection);
    }
    if(intersections.size() == 0)
    {
        LOG_DEBUG_ERROR("oh no! \n");
        leftIntersection = VectorMath::getIntersectionBetweenLineSegments(start, end, bottomLeft, topLeft); //
        rightIntersection  = VectorMath::getIntersectionBetweenLineSegments(start, end, bottomRight, topRight); //
        topIntersection  = VectorMath::getIntersectionBetweenLineSegments(start, end, topLeft, topRight); //
        bottomIntersection  = VectorMath::getIntersectionBetweenLineSegments(start, end, bottomLeft, bottomRight); //
    }
    return intersections;
}



void MapSector::shutdown()
{
    for(int x = 0; x < _sectorWidthInCells; x++)
    {
        delete [] _collisionCellsInSector[x];
    }
    delete [] _collisionCellsInSector;
    _teamStrengthsInSector.clear();
}
