//
//  ClientLobbyManager.hpp
//  TrenchWars
//
//  Created by Paul Reed on 11/20/23.
//

#ifndef ClientLobbyManager_h
#define ClientLobbyManager_h

#include "LobbyManager.h"
#include "JoinServerScene.h"

class ClientLobbyManager : public LobbyManager
{
    JoinServerScene * _joinScene;
    bool _waitingForConnection;
    bool _receivedScenarioMap;
    bool _canStartGame;
    
    unsigned char * _mapData;
    size_t _mapDataSize;
    
    void sendLobbyStatusToClients();
    void init();
    
    void parseGameStartMessage(cereal::BinaryInputArchive & iArchive);
    void parseLobbyStatusMessage(cereal::BinaryInputArchive & iArchive);
    void parseScenarioMapMessage(cereal::BinaryInputArchive & iArchive);
    
public:
    
    static ClientLobbyManager * create();
    
    void joinServer(const std::string serverIp);
    
    virtual bool isPlayerReady(int playerId);

    //UI to Manager Functions
    virtual void playerSelectedStartGame(int playerId);
    virtual void playerSetName(int playerId, const std::string & name);
    virtual void playerSelectedCommand(int teamId, int playerId, int commandId);
    virtual void playerSetScenario(const std::string & scenarioName);
    
    
    virtual void update(float deltaTime);
    virtual void parseNetworkMessage(GameMessage * message);
    
    virtual void playerJoinedGame(Player * player);
    virtual void playerLeftGame(int playerId);
    
    virtual void shutdown();
};

#endif /* ClientLobbyManager_h */
