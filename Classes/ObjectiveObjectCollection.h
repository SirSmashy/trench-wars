//
//  ObjectiveObjectCollection.h
//  TrenchWars
//
//  Created by Paul Reed on 9/19/20.
//

#ifndef ObjectiveObjectCollection_h
#define ObjectiveObjectCollection_h

#include "cocos2d.h"
#include "PhysicalEntity.h"
#include "PlanningObject.h"

USING_NS_CC;

class Objective;

enum ObjectCollectionType
{
    OBJECT_COLLECTION_TRENCH_LINE,
    OBJECT_COLLECTION_BUNKERS,
    OBJECT_COLLECTION_WIRE,
    OBJECT_COLLECTION_FOILAGE,
    OBJECT_COLLECTION_ANY,
};

class ObjectiveObjectCollection : public ComparableRef
{
    /** Iterator, can be used to loop the Vector. */
    using ObjectiveObjectCollectionIterator = typename std::unordered_set<PhysicalEntity *, ComparableRefPointerHash, ComparableRefPointerCompare>::iterator;
    /** Const iterator, can be used to loop the Vector. */
    using const_ObjectiveObjectCollectionIteratorIterator = typename std::unordered_set<PhysicalEntity *, ComparableRefPointerHash, ComparableRefPointerCompare>::const_iterator;

protected:
    Objective * _owningObjective;
    ObjectCollectionType _objectCollectionType;
    std::unordered_set<PhysicalEntity *, ComparableRefPointerHash, ComparableRefPointerCompare> _entities;
    std::mutex entitiesMutex;
    
    void init(Objective * owningObjective, ObjectCollectionType type);
public:
    
    static ObjectiveObjectCollection * create(Objective * owningObjective, ObjectCollectionType type = OBJECT_COLLECTION_ANY);
    
    virtual PhysicalEntity * getEntityNearestPosition(Vec2 position, bool weightByAvailableWorkSlots = false);
    virtual bool testCollisionWithPoint(Vec2 point);
    virtual Vec2 getAveragePosition();
    virtual void addEntity(PhysicalEntity * ent);
    virtual void removeEntity(PhysicalEntity * ent);
    virtual bool containsEntity(PhysicalEntity * ent);
    
    bool isWorkComplete();
    
    virtual void remove();

    virtual Objective * getOwningObjective() {return _owningObjective;}
    ObjectCollectionType getObjectCollectionType() {return _objectCollectionType;}
    
    virtual ObjectiveObjectCollectionIterator begin() { return _entities.begin(); }
    virtual const_ObjectiveObjectCollectionIteratorIterator begin() const { return _entities.begin();  }
    virtual ObjectiveObjectCollectionIterator end() { return _entities.end();  }
    virtual const_ObjectiveObjectCollectionIteratorIterator end() const { return _entities.end();  }
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

#endif /* ObjectiveObjectCollection_h */
