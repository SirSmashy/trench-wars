//
//  UnitGroup.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/12/20.
//

#ifndef UnitGroup_h
#define UnitGroup_h

#include "cocos2d.h"
#include "InteractiveObject.h"
#include "Unit.h"
#include "Order.h"
#include "LineDrawer.h"

USING_NS_CC;

class Objective;
class Team;

struct IssuedPlanOrder {
    std::unordered_set<ENTITY_ID> unitsIssuedOrder;
    ORDER_TYPE orderType;
    Vec2 position;
};

class UnitGroup : public InteractiveObject
{
private:
    EntityAnimationSet * _spriteBatch;
    Vector<Unit *> _unitsInGroup;
    std::unordered_map<ENTITY_ID,IssuedPlanOrder> _participatingInPlans;
    Team * _owningTeam;
    int _owningPlayerId;
    LineDrawer * _lineToOrder;

    
    UnitGroup();
    ~UnitGroup();
    bool init(Vector<Unit *> & units, Team * owningTeam);
    
    void recordOrder(ORDER_TYPE orderType);
    Vec2 getOrderEmblemPosition();
    
public:
    
    /*
     *
     */
    static UnitGroup * create(Vector<Unit *> & units, Team * owningTeam);
    static UnitGroup * create();
            
    virtual INTERACTIVE_OBJECT_TYPE getInteractiveObjectType() {return UNIT_GROUP_INTERACTIVE_OBJECT;}

    virtual void issueMoveOrder(Vec2 position);
    virtual void issueOccupyOrder(Objective * objective);
    virtual void issueDigTrenchesOrder(Objective * objective);
    virtual void issueDigBunkersOrder(Objective * objective);
    virtual void issueBuildCommandPostOrder(Objective * objective);
    virtual void issueClearAreaOrder(Objective * objective);
    virtual void issueAttackPositionOrder(Objective * objective);
    virtual void issueStopOrder();
        
    virtual bool canBombard();
    virtual bool positionValidForBombardment(Vec2 position);
    virtual bool hasOrder();
    virtual Team * getOwningTeam() {return _owningTeam;}
    
    bool isParticipatingInPlan(ENTITY_ID planId);
    const IssuedPlanOrder & getPlanOrder(ENTITY_ID planId);
    void unitInGroupCompletedOrder(Unit * unit, ENTITY_ID planId);
    
    
    bool containsUnitType(UNIT_TYPE type);
    bool containsUnit(Unit * unit);
    void addUnit(Unit * unit);
    void removeUnit(Unit * unit);
    const Vector<Unit *> & getUnits() {return _unitsInGroup;}
    virtual Vec2 getPosition();
        
    /*
     *
     */
    virtual void removeFromGame();
        
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

#endif /* UnitGroup_h */
