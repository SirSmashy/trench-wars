//
//  DebugObjectMenu.hpp
//  TrenchWars
//
//  Created by Paul Reed on 2/22/21.
//

#ifndef DebugObjectMenu_h
#define DebugObjectMenu_h

#include <stdio.h>
#include "ui/CocosGUI.h"
#include "cocos2d.h"
#include "GameMenu.h"
#include "InteractiveObject.h"


USING_NS_CC;
using namespace cocos2d::ui;


class DebugObjectMenu : public GameMenu
{
private:
    DebugObjectMenu();
    bool init();
    InteractiveObject * _object;
    bool _followObject;
public:
    static DebugObjectMenu * create();
    
    void setObject(InteractiveObject * object, bool followObject);
    InteractiveObject * getObject() {return _object;}
    
    bool isFollowingObject() {return _followObject;}
    void clearObject();
    virtual void setVisible(bool visible);
    virtual void update(float deltaTime);
};

#endif /* DebugObjectMenu_hpp */
