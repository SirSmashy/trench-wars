//
//  PolygonLandscapeObject.hpp
//  TrenchWars
//
//  Created by Paul Reed on 8/12/22.
//

#ifndef PolygonLandscapeObject_h
#define PolygonLandscapeObject_h

#include <unordered_set>
#include "Refs.h"
#include "CollisionCell.h"
#include "EntityDefinitions.h"
#include "PolygonSpriteProxy.h"
#include "VectorMath.h"
#include "PrimativeDrawer.h"
#include "InteractiveObject.h"


class PolygonLandscapeObject:  public InteractiveObject
{
    PolygonLandscapeObjectDefiniton * _definition;
    std::vector<Vec2> _edgePoints;
    std::vector<Vec2> _smoothedEdgePoints;
    Vec2 _visualPositionOffset;
    Vec2 _worldOrigin;

    std::unordered_set<Vec2, Vec2Hash, Vec2Compare> _cellIndicesInEntity;
    PolygonSpriteProxy * _sprite;
    PolygonSpriteProxy * _boundarySprite;

    PrimativeDrawer * _primativeDrawer;
    TerrainCharacteristics _terrainCharacteristics;
    
    bool _debugging;

    void init(const std::string & name, Vec2 worldOrigin, Vec2 visualPositionOffset, Vec2 minimumCellIndex, Rect cellIndexBounds, std::unordered_set<Vec2, Vec2Hash, Vec2Compare> & cellIndicesInEntity);
    void findEdgePoints(Vec2 minimumCellIndex, Rect cellIndexBounds);
    void smoothEdges();
    Vec2 cellCornerForDirection(int direction);
    Vec2 getLaplacian(Vec2 prev, Vec2 point, Vec2 next);
    
public:
    
    static PolygonLandscapeObject * create(const std::string & name, Vec2 worldOrigin, Vec2 visualPositionOffset, Vec2 minimumCellIndex, Rect cellIndexBounds, std::unordered_set<Vec2, Vec2Hash, Vec2Compare> & cellIndicesInEntity);
    
    virtual INTERACTIVE_OBJECT_TYPE getInteractiveObjectType() {return POLYGON_LANDSCAPE_INTERACTIVE_OBJECT;}
    virtual size_t getHashedDefinitionName();
    
    Vec2 getWorldOrigin() {return _worldOrigin;}
    const TerrainCharacteristics & getTerrainCharacteristics() {return _terrainCharacteristics;}
    
    PolygonSpriteProxy * getSprite() {return _sprite;}
    bool containsPoint(Vec2 position);
    
    virtual void setVisible(bool visible);
    virtual void setPositionZ(float zPosition);
    
    
    virtual void setDebugMe();
    virtual bool isDebugMe() {return _debugging;}

    virtual void clearDebugMe();
};

#endif /* PolygonLandscapeObject_h */
