//
//  VisibilityTester.cpp
//  TrenchWars
//
//  Created by Paul Reed on 7/25/24.
//

#include "VisibilityTester.h"
#include "NavigationGrid.h"
#include "CollisionGrid.h"
#include "World.h"
#include "EntityManager.h"
#include "EmblemManager.h"
#include "MultithreadedAutoReleasePool.h"
#include "GameStatistics.h"
#include "TestBed.h"
#include "UIController.h"
#include "GameVariableStore.h"
#include "MapSector.h"
#include "Cloud.h"
#include "SectorGrid.h"
#include "EngagementManager.h"

#define ENTITY_COUNT 50
static const float STOP_VISION_THRESHOLD = 150;

VisibilityTester::VisibilityTester()
{
    
}

void VisibilityTester::init(Vec2 position)
{
    _lineDrawer = LineDrawer::create(position * WORLD_TO_GRAPHICS_SIZE, position * WORLD_TO_GRAPHICS_SIZE, 5);
    _lineDrawer->makeDashed();
    _lineDrawer->setColor(Color3B(255,0,0));
    _lineDrawer->setFlowing(true);   
    
    _sectorLineDrawer = LineDrawer::create(position * WORLD_TO_GRAPHICS_SIZE, position * WORLD_TO_GRAPHICS_SIZE, 5);
    _sectorLineDrawer->makeDashed();
    _sectorLineDrawer->setColor(Color3B(255,255,255));
    _sectorLineDrawer->setFlowing(true);

    
    _primativeDrawer = PrimativeDrawer::create();
    _primativeDrawer->retain();
    _primativeDrawer->setLineWidth(3);
    
    _cellHighlighter = CellHighlighter::create();
    _cellHighlighter->setHighlightColor(Color4F::BLACK);

    _cellHighlighter2 = CellHighlighter::create();
    _cellHighlighter2->setHighlightColor(Color4F(0,1,0,0.5));
    
    _startEmblem = globalEmblemManager->createEmblemForTester(this, true);
    _startEmblem->setAnimation(EMBLEM_ORDER, false);
    _startEmblem->setPosition(position);
    position.x -= 5;
    
    _endEmblem = globalEmblemManager->createEmblemForTester(this, true);
    _endEmblem->setAnimation(EMBLEM_MOVE, false);
    _endEmblem->setPosition(position);
    
    Director * director = Director::getInstance();
    Scheduler * scheduler = director->getScheduler();
    scheduler->schedule([=] (float dt) { update(); }, this, 0.333333333, false, "visibilityTester");

    lastUpdateTime = 0;

    update();
    
}

VisibilityTester * VisibilityTester::create(Vec2 position)
{
    VisibilityTester * newTester = new VisibilityTester();
    newTester->init(position);
    globalAutoReleasePool->addObject(newTester);
    return newTester;
}

Emblem * VisibilityTester::getEmblemAtPoint(Vec2 location)
{
    location *= WORLD_TO_GRAPHICS_SIZE;
    if(_startEmblem->testCollisionWithCircle(location, 32))
    {
        return _startEmblem;
    }
    else return _endEmblem;
}


bool VisibilityTester::testCollisionWithPoint(Vec2 point)
{
    point *= WORLD_TO_GRAPHICS_SIZE;
    return _startEmblem->testCollisionWithCircle(point,WORLD_TO_GRAPHICS_SIZE) || _endEmblem->testCollisionWithCircle(point, WORLD_TO_GRAPHICS_SIZE);
}

bool VisibilityTester::doCellLineTest(Vec2 start, Vec2 endPosition)
{
    float accumulatedCover = 0;
    float maximumCover = 75;
    
    auto grid = world->getCollisionGrid(start);
    CollisionCell * startCell = grid->getCellForCoordinates(start);
    
    bool hitEnd = grid->iterateCellsInLine(start, endPosition, [this, &accumulatedCover, maximumCover, &endPosition] (CollisionCell * cell, float distanceTraveled, bool minorCell)
    {
        accumulatedCover += cell->getConcealmentInCell();
        if(accumulatedCover > maximumCover)
        {
            endPosition = cell->getCellPosition() + Vec2(.5,.5);
            return false;
        }
        return true;
    });
    
    std::unordered_set<ENTITY_ID> sectors;
    std::unordered_set<Cloud *> cloudsEncountered;
    globalSectorGrid->iterateSectorsInLine(start, endPosition, [start, endPosition, &sectors, &accumulatedCover, maximumCover, &cloudsEncountered] (MapSector * sector)
   {
        sectors.insert(sector->uID());
        return true;
    });
    LOG_DEBUG("Standard %d \n", sectors.size());
    return hitEnd;
}


bool VisibilityTester::doCellLineTestFull(Vec2 start, Vec2 endPosition)
{
    unsigned short maximumCover = 75;
    unsigned short accumulatedCover = 0;
    
    auto grid = world->getCollisionGrid(start);
    CollisionCell * startCell = grid->getCellForCoordinates(start);
    
    bool hitEnd = grid->iterateCellsInLine(start, endPosition, [this, &accumulatedCover, maximumCover, &endPosition] (CollisionCell * cell, float distanceTraveled, bool minorCell)
    {
        accumulatedCover += cell->getConcealmentInCell();
        if(accumulatedCover > maximumCover)
        {
            endPosition = cell->getCellPosition() + Vec2(.5,.5);
            return false;
        }
        return true;
    });
    
    std::unordered_set<ENTITY_ID> sectors;
    std::unordered_set<Cloud *> cloudsEncountered;
    globalSectorGrid->iterateSectorsInLine(start, endPosition, [start, endPosition, &sectors, &accumulatedCover, maximumCover, &cloudsEncountered] (MapSector * sector)
   {
        sectors.insert(sector->uID());
        for(auto cloud : sector->getCloudsInSector())
        {
            if(cloudsEncountered.find(cloud.second) == cloudsEncountered.end())
            {
                cloudsEncountered.insert(cloud.second);

                Vec2 intersect1, intersect2;
                auto intersections = VectorMath::getIntersectionBetweenLineSegmentAndCircle(start, endPosition, cloud.second->getPosition(), cloud.second->getRadius(), intersect1, intersect2);
                if(intersections.first || intersections.second)
                {
                    accumulatedCover += intersect1.distance(intersect2) * cloud.second->getDensity();
                }
            }
        }
        if(accumulatedCover > maximumCover)
        {
            return false;
        }
        return true;
    });
    LOG_DEBUG("Standard Full %d \n", sectors.size());
    return hitEnd;
}

void VisibilityTester::update()
{
    bool startMoved = false;
    bool endMoved = false;
    Vec2 start = _startEmblem->getPosition();
    Vec2 end = _endEmblem->getPosition();
    
    if(startPosition != start)
    {
        startPosition = start;
        startMoved = true;
    }
    if(endPosition != end)
    {
        endPosition = end;
        endMoved = true;
    }
    
    bool needsUpdate = globalEngagementManager->getEngagementTime() - lastUpdateTime > 0.5;
    if(needsUpdate || startMoved || endMoved)
    {
        lastUpdateTime = globalEngagementManager->getEngagementTime();
        doLineVisionTest();
    }

}


void VisibilityTester::doLineVisionTest()
{
    Vec2 start = _startEmblem->getPosition();
    Vec2 endPosition = _endEmblem->getPosition();

    
    CollisionCell * startCell = world->getCollisionGrid()->getCellForCoordinates(start);
    CollisionCell * endCell = world->getCollisionGrid()->getCellForCoordinates(endPosition);
    
    _cellHighlighter->clearCells();
    _cellHighlighter2->clearCells();

    endPosition = _endEmblem->getPosition();
    Vec2 direction = (endPosition - start).getNormalized();


    
    MicroSecondSpeedTimer timer( [=](long long int time) {
        LOG_DEBUG("Cell time: %f \n", (double)time/1000.0);
    });
    
    bool visible = false;
    if(globalTestBed->visionType == 0)
    {
        visible = doCellLineTest(start, endPosition);
    }
    else if(globalTestBed->visionType == 1)
    {
        visible = doCellLineTestFull(start, endPosition);
    }

    timer.stop();
    
    if(visible)
    {
        _lineDrawer->setColor(Color3B(0,255,0));
    }
    else
    {
        _lineDrawer->setColor(Color3B(255,0,0));
    }
    
    _lineDrawer->setStartPosition(start * WORLD_TO_GRAPHICS_SIZE);
    _lineDrawer->setEndPosition(endPosition * WORLD_TO_GRAPHICS_SIZE);
    _primativeDrawer->clear();
    
    Color4F color = Color4F::GRAY;
    color.a = .6;
    _primativeDrawer->drawLine(start * WORLD_TO_GRAPHICS_SIZE, _endEmblem->getPosition() * WORLD_TO_GRAPHICS_SIZE,color );

}


void VisibilityTester::removeFromGame()
{
    _primativeDrawer->remove();
    _primativeDrawer->release();
    
    globalEmblemManager->removeAllEmblemsForCommandableObject(uID());
    globalUIController->removeTester(this);
    
    Director * director = Director::getInstance();
    Scheduler * scheduler = director->getScheduler();
    scheduler->unschedule("visibilityTester", this);
}

