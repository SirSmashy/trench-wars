//
//  SpriteBatchGroup.cpp
//  trench-wars
//
//  Created by Paul Reed on 10/3/23.
//

#include "SpriteBatchGroup.h"
#include "PlayerLayer.h"
#include "World.h"


void SpriteBatchGroup::init(EntityAnimationSet * animationSet)
{
    _animationSet = animationSet;
    _currentBatchIndex = 0;
    
    EntityAnimation * entAnim = _animationSet->getFirstValidAnimation();
    if(entAnim == nullptr)
    {
        LOG_ERROR("!!Unable to create Sprite Batch for Animation Set \n");
    }
    
    Animation * animation = entAnim->_animation;
    _texture = animation->getFrames().at(0)->getSpriteFrame()->getTexture();
    
    Director * director = Director::getInstance();
    Scheduler * scheduler = director->getScheduler();
    scheduler->schedule([=] (float dt) { update(dt); }, this, 10.0, false, "BatchGroup");
}

SpriteBatchGroup * SpriteBatchGroup::create(EntityAnimationSet * animationSet)
{
    SpriteBatchGroup * group = new SpriteBatchGroup();
    group->init(animationSet);
    return group;
}

void SpriteBatchGroup::update(float deltaTime)
{
    for(int i = 0; i < _batches.size(); i++)
    {
        if(_batches.at(i)->getTextureAtlas()->getTotalQuads() < 10000)
        {
            _currentBatchIndex = i;
            break;
        }
    }
}

SpriteBatchNode * SpriteBatchGroup::addSprite(AnimatedSprite * sprite, int zOrder)
{
    SpriteBatchNode * batch = nullptr;
    while(_currentBatchIndex < _batches.size())
    {
        if(_batches.at(_currentBatchIndex)->getTextureAtlas()->getTotalQuads() < 10000)
        {
            batch = _batches.at(_currentBatchIndex);
            break;
        }
        _currentBatchIndex++;
    }
    
    if(batch == nullptr)
    {
        batch = SpriteBatchNode::createWithTexture(_texture, 10000);
        
        if(_animationSet->_uiLayer)
        {
            globalPlayerLayer->addChild(batch);
        }
        else
        {
            world->addChild(batch);
        }
        _batches.pushBack(batch);
    }
    
    batch->addChild(sprite,zOrder, 1);
    return batch;
}

void SpriteBatchGroup::setVisible(bool visible)
{
    for(auto batch : _batches)
    {
        batch->setVisible(visible);
    }
}

void SpriteBatchGroup::setPositionOffset(Vec2 position)
{
    for(auto batch : _batches)
    {
        batch->setPosition(position);
    }
    
}
