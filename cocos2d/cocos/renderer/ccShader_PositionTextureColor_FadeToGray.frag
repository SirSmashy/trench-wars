
/*
 * cocos2d for iPhone: http://www.cocos2d-iphone.org
 *
 * Copyright (c) 2011 Ricardo Quesada
 * Copyright (c) 2012 Zynga Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

const char* ccPositionTextureColor_FadeToGray_frag = R"(
#ifdef GL_ES
precision lowp float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
uniform sampler2D u_PaperSampler;

void main()
{
    float grayBlend = v_fragmentColor.a;
    if(grayBlend <= .25)
        discard;
    
    float oneMinusBlend = 1.0 - grayBlend;
    
    vec4 color = v_fragmentColor * texture2D(CC_Texture0, v_texCoord); //
    vec4 paperColor = texture2D(CC_Texture1, v_texCoord); //
    paperColor.rgb *= 0.33;

    float grayValue = ((color.r * 0.299) + (color.g * 0.587) + (color.b * 0.114)) * 0.66;
    vec4 grayColor = vec4(grayValue + paperColor.r, grayValue + paperColor.g, grayValue + paperColor.b,1.0) * grayBlend;
//    vec4 grayColor = vec4(grayValue, grayValue, grayValue,1.0) * grayBlend;
    color.rgb = color.rgb * oneMinusBlend;
    gl_FragColor = vec4(color.r + grayColor.r, color.g + grayColor.g, color.b + grayColor.b, 1.0);
}
)";
