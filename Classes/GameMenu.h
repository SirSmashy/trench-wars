//
//  GameMenu.hpp
//  TrenchWars
//
//  Created by Paul Reed on 11/18/22.
//

#ifndef GameMenu_h
#define GameMenu_h

#include "ui/CocosGUI.h"
#include "cocos2d.h"
#include "AutoSizedLayout.h"


USING_NS_CC;
using namespace cocos2d::ui;

enum MENU_BUTTON_ACTION
{
    MENU_BUTTON_DO_NOTHING,
    MENU_BUTTON_CLOSE_SUB_MENU,
    MENU_BUTTON_CLOSE_MENU,
};

class GameMenu : public AutoSizedLayout
{
protected:
    GameMenu();
    Vector<Widget *> _widgets;
    Vector<GameMenu *> _childMenus;
    Vector<Button *> _childMenuButtons;
    Vec2 _gamePosition;
    bool _keepMenuAtGamePosition;
    
    GameMenu * _parent;
    bool init(GameMenu * parent = nullptr);

public:
    static GameMenu * create(GameMenu * parent = nullptr);
    
    virtual void update(float deltaTime);

    typedef std::function<void(Ref*,Vec2)> menuCallback;
    virtual void setPosition(const Vec2 &position);
    virtual void setVisible(bool visible);
    virtual void setPositionInGameWorld(Vec2 position);
    virtual void setKeepMenuAtGamePosition(bool keep);
    
    
    void addSeperater();
    GameMenu * addChildMenu(std::string name, const std::function<void (GameMenu *)> & menuCreationFunction);
    void addButton(std::string name, const AbstractCheckButton::ccWidgetClickCallback & callback, MENU_BUTTON_ACTION menuAction = MENU_BUTTON_CLOSE_MENU);
    void addText(std::string textString);
    void closeSubMenus();
    
    virtual void doLayout() override;
    GameMenu * getRootMenu();
    
    void clear();
    void remove();
};
#endif /* GameMenu_h */
