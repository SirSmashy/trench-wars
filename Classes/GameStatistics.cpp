//
//  GameStatistics.cpp
//  TrenchWars
//
//  Created by Paul Reed on 7/31/22.
//

#include "GameStatistics.h"
#include "EngagementManager.h"
#include "Infantry.h"
#include "TeamManager.h"

GameStatistics * globalGameStatistics;

void GameStatistics::init()
{
    _entityCountMap.emplace(INFANTRY, 0);
    _entityCountMap.emplace(CREW_WEAPON, 0);
    _entityCountMap.emplace(COMMANDPOST, 0);
    _entityCountMap.emplace(PROJECTILE, 0);
    _entityCountMap.emplace(STATIC_ENTITY, 0);
    globalGameStatistics = this;
}

GameStatistics * GameStatistics::create()
{
    GameStatistics * stats = new GameStatistics();
    stats->init();
    stats->autorelease();
    return  stats;
}



int GameStatistics::livingHumansForTeam(int team)
{
    int count = 0;
    Command * command = globalTeamManager->getCommand(team);
    auto units = command->getUnits();
    for(Unit * unit : units)
    {
        for(auto ent : *unit)
        {
            Infantry * infantry = (Infantry *) ent;
            if(infantry->getActorState() == ALIVE)
            {
                count++;
            }
        }
    }
    return count;
}



void GameStatistics::incrementEntityTypeCount(ENTITY_TYPE type)
{
    testMutex.lock();
    _entityCountMap[type] = _entityCountMap[type] + 1;
    testMutex.unlock();

}

void GameStatistics::decrementEntityTypeCount(ENTITY_TYPE type)
{
    testMutex.lock();
    _entityCountMap[type] = _entityCountMap[type] - 1;
    testMutex.unlock();

}

int GameStatistics::getGameObjectTypeCount(ENTITY_TYPE type)
{
    return _entityCountMap.at(type);
}

void GameStatistics::setTestValue(const std::string & valueId, double newValue)
{
    testMutex.lock();
    TestValue test;
    if(_testValues.find(valueId) != _testValues.end()) {
        test = _testValues[valueId];
    }
    test.addValue(newValue);
    _testValues[valueId] = test;
    testMutex.unlock();

}

void GameStatistics::clearTestValues()
{
    testMutex.lock();
    _testValues.clear();
    testMutex.unlock();
}
