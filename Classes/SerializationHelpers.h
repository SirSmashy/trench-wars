//
//  SerializationHelpers.h
//  TrenchWars
//
//  Created by Paul Reed on 5/3/23.
//

#ifndef SerializationHelpers_h
#define SerializationHelpers_h

#include "cocos2d.h"
USING_NS_CC;

namespace cereal
{
    template<class Archive>
    void serialize(Archive & archive,
                   Vec2 & m)
    {
        archive( m.x, m.y);
    }

    template<class Archive>
    void serialize(Archive & archive,
                   Size & m)
    {
        archive( m.width, m.height);
    }


    template<class Archive>
    void serialize(Archive & archive,
                   Rect & m)
    {
        archive( m.origin);
        archive( m.size);
    }

    template<class Archive>
    void serialize(Archive & archive,
                   Color4B & m)
    {
        archive( m.r);
        archive( m.g);
        archive( m.b);
        archive( m.a);
    }

    template<class Archive, class T>
    void serialize(Archive & archive,
                   Color4B & m)
    {
        archive( m.r);
        archive( m.g);
        archive( m.b);
        archive( m.a);
    }
}



#endif /* SerializationHelpers_h */
