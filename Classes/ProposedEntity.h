//
//  ProposedEntity.hpp
//  TrenchWars
//
//  Created by Paul Reed on 2/19/23.
//

#ifndef ProposedEntity_h
#define ProposedEntity_h

#include "cocos2d.h"
#include "PhysicalEntity.h"
#include "ChangeTrackingVariable.h"
#include "ObjectiveObjectCollection.h"

class CollisionCell;
class StaticEntityDefinition;
class World;
class Command;
class Objective;

class ProposedEntity : public PhysicalEntity
{
protected:
    float _workUntilCreated;
    
    std::mutex _workMutex;
    
    CC_CONSTRUCTOR_ACCESS :
    
    ProposedEntity();
    ~ProposedEntity();

    virtual void workComplete() = 0;

public:
    virtual void postLoad();

    virtual void addWork(float deltaTime);
    virtual bool isWorkComplete();
    void completeAllWork();
    
    virtual void populateDebugPanel(GameMenu * menu, Vec2 location);
};


class ProposedTrench : public ProposedEntity
{
    ObjectiveObjectCollection * _onCompleteCollection;
    
    virtual bool init(Vec2 position, PhysicalEntityDefinition * def, ObjectiveObjectCollection * onCompleteCollection);
    
    virtual void workComplete();
    
public:
    static ProposedTrench * create(Vec2 position, ObjectiveObjectCollection * onCompleteCollection);
    
    static ProposedTrench * create(Vec2 position, PhysicalEntityDefinition * def, ObjectiveObjectCollection * onCompleteCollection);
    
    static ProposedTrench * create();

    
    virtual ENTITY_TYPE getEntityType() const {return PROPOSED_TRENCH;}
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

class ProposedBunker : public ProposedEntity
{
    Size _bunkerSize;
    DIRECTION _bunkerFacing;
    ObjectiveObjectCollection * _onCompleteCollection;
        
    virtual bool init(Vec2 position, PhysicalEntityDefinition * def, Size bunkerSize, DIRECTION bunkerFacing,  ObjectiveObjectCollection * onCompleteCollection);

    
    virtual void workComplete();
    
public:
    static ProposedBunker * create(Vec2 position, const std::string & definitionName, Size bunkerSize, DIRECTION bunkerFacing,  ObjectiveObjectCollection * onCompleteCollection);
    
    static ProposedBunker * create(Vec2 position, PhysicalEntityDefinition * def, Size bunkerSize, DIRECTION bunkerFacing,  ObjectiveObjectCollection * onCompleteCollection);
    
    static ProposedBunker * create();

    
    virtual ENTITY_TYPE getEntityType() const {return PROPOSED_BUNKER;}
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

class ProposedCommandPost : public ProposedEntity
{
    Command * _command;
    Objective * _objective;
    virtual bool init(Vec2 position, PhysicalEntityDefinition * def, Command * command, Objective * objective);

    virtual void workComplete();
    
public:
    static ProposedCommandPost * create(Vec2 position, const std::string & definitionName, Command * command, Objective * objective);
    static ProposedCommandPost * create(Vec2 position, PhysicalEntityDefinition * def, Command * command, Objective * objective);
    static ProposedCommandPost * create();
    
    virtual ENTITY_TYPE getEntityType() const {return PROPOSED_COMMAND_POST;}
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};
#endif /* ProposedEntity_h */
