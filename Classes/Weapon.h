#ifndef __WEAPON_H__
#define __WEAPON_H__

//
//  Weapon.h
//  TrenchWars
//
//  Created by Paul Reed on 3/11/12.
//  Copyright (c) 2012 . All rights reserved.
//

#include "cocos2d.h"
#include "Refs.h"
#include "EntityDefinitions.h"
#include <sstream>
#include <cereal/archives/binary.hpp>
#include "GameMenu.h"
#include "ChangeTrackingVariable.h"

USING_NS_CC;


enum WEAPONSTATE
{
    WEAPON_READY,
    WEAPON_RELOADING,
    WEAPON_RECHAMBERING,
    WEAPON_NEEEDS_AMMO,
    WEAPON_STORED,
    WEAPON_UNSTORING,
    WEAPON_MOVING, // Maybe???
    WEAPON_UNKNOWN
};


class Infantry;
class Human;

struct WeaponNetworkUpdate
{
    WEAPONSTATE weaponState = WEAPON_READY;
    int roundsInMagazine = 0;
    double workUntilReady = 0;
    double timeUntilReady = 0;
    std::list<double> randomValuesToSend;
};

class Weapon : virtual public ComparableRef
{
protected:
    Infantry * _owningInfantry;
    WEAPONSTATE _weaponState;
    WeaponDefinition * _definition;
    ProjectileDefinition * _projectile;
    AtomicChangeTrackingVariable<float> _pendingWeaponWork;

    
    double _maxRange;
    double _minRange;
    
    double _imprecision;
    double _workUntilReady;
    double _timeUntilReady;
    int _roundsInMagazine;
    WeaponNetworkUpdate _weaponNetworkUpdate;
    std::list<double> _randomValues;
    
    
CC_CONSTRUCTOR_ACCESS :

    Weapon();
    virtual bool init(WeaponDefinition * definition, Infantry * owner);
    
    double adjustFiringAngleForInaccuracy(double firingAngle);
    Vec2 adjustTargetPositionForInaccuracy(Vec2 targetPosition, Vec2 firePosition);
    
public:
    static Weapon * create(WeaponDefinition * definition, Infantry * owner);
    static Weapon * create();
            
    void setStored(bool stored);
    void updateState();
    
    void updateWeaponChamberState();

    void updateRandomValues();
    void fire(double fireAngle, Vec2 position, Vec2 goalPosition, int height);
    void meleeAttackWithTarget(Human * target, double experienceLevel);
    
    float getProjectileSpeed();
    float getMinimumRange();
    float getMaximumRange();
    virtual int   getPreferenceOrder();
    bool  isExplosiveProjectile();
    virtual bool isCrewWeapon() {return false;}
    
    int getCostToReload();
    int getAmmoCost();
    int getMagazineSize();
    virtual void addAmmo(int ammo);
    float getRecoil();
    
    std::string getName();
    WEAPONSTATE getWeapaonState() {return _weaponState;}
    int getRoundsInMagazine() {return _roundsInMagazine;}

    WEAPON_PROJECTILE_TYPE getProjectileType();
    
    virtual Vec2 getWeaponFirePosition() {return Vec2::ZERO;}
    virtual Vec2 getWeaponWorkPosition();

    virtual void addWorkToWeapon(double deltaTime);
    
    int checkForNetworkUpdate();
    void saveNetworkDate(cereal::BinaryOutputArchive & archive);
    void updateFromNetworkData(cereal::BinaryInputArchive & archive);
        
    void saveToArchive(cereal::BinaryOutputArchive & archive) const;
    void loadFromArchive(cereal::BinaryInputArchive & archive);
    void populateDebugPanelForWeapon(GameMenu * menu, Vec2 location);
};


#endif // __WEAPON_H__
