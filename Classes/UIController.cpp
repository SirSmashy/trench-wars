//
//  UIController.cpp
//  TrenchWars
//
//  Created by Paul Reed on 12/27/23.
//

#include "UIController.h"
#include "PrimativeDrawer.h"
#include "Command.h"
#include "Order.h"
#include "EmblemManager.h"
#include "InputActionHandler.h"
#include "PlanningMenu.h"
#include "ObjectiveInfoDisplay.h"
#include "GameMenuBuilder.h"
#include "Plan.h"
#include "EngagementScene.h"
#include "Entity.h"
#include "TeamManager.h"
#include "MenuLayer.h"
#include "PathfindingTester.h"
#include "VisibilityTester.h"
#include "SectorGrid.h"
#include "EngagementManager.h"
#include "SectorGrid.h"

#define VISIBILITY_MAX_FADE_TIME 30

static const float VISIBILITY_MAX_FADE_MULT = 255/VISIBILITY_MAX_FADE_TIME;

UIController * globalUIController = nullptr;


void UIController::init()
{
    globalUIController = this;
    _currentPlan = nullptr;
    _camera = GameCamera::create(); //Fix this
    _camera->retain();
    
    _inputActionHandler = InputActionHandler::create();
    _inputActionHandler->retain();
    
    _planningMenu = PlanningMenu::create();
    _planningMenu->retain();
    
    _orderDrawer = OrderDrawer::create();
    _orderDrawer->retain();
    
    
    _entityMenu = GameMenu::create();
    _entityMenu->setKeepMenuAtGamePosition(true);
    _landscapeObjectiveBuilder = LandscapeObjectiveBuilder::create();
    _landscapeObjectiveBuilder->retain();
    _commandPostCreationDrawer = nullptr;
    _commandPostCreationDrawer = CommandPostCreationDrawer::create();
    _commandPostCreationDrawer->retain();
    _commandPostCreationDrawer->setVisible(false);
    
    _primativeDrawer = PrimativeDrawer::create();
    
    Size captureSize(SECTOR_SIZE,SECTOR_SIZE);
    
    
    for(int x = 0; x < globalSectorGrid->getSectorGridSize().width; x++)
    {
        for(int y = 0; y < globalSectorGrid->getSectorGridSize().height; y++)
        {
            _sectorVisibilityMap.try_emplace(globalSectorGrid->sectorAtIndex(x, y)->getSectorOrigin(), -1000);
        }
    }
    
    _teamVisibilitySprite = HistoricalViewSprite::create(world->getWorldSize(), captureSize, globalSectorGrid->getSectorGridSize().width, globalSectorGrid->getSectorGridSize().height, world->getWorldCocosNode());
    _teamVisibilitySprite->retain();
    world->getWorldCocosNode()->addChild(_teamVisibilitySprite);
    dumbTest = true;

}

UIController * UIController::create()
{
    UIController * controller = new UIController();
    controller->init();
    return controller;
}



void UIController::postLoad()
{
    if(_currentPlan != nullptr)
    {
        _orderDrawer->drawOrders(_currentPlan->getOrders(), true);
    }
    else
    {
        _orderDrawer->drawOrders(_unitToActiveOrderMap, false);
    }
//    _teamVisibilitySprite->captureEntireWorld();
}


void UIController::update(float deltaTime)
{
    for(auto infoPair : _objectiveInfos)
    {
        infoPair.second->update();
    }
    
    _orderDrawer->update();
    globalMenuLayer->updateGameInfo(deltaTime);
    _teamVisibilitySprite->update();
}

void UIController::toggleHistoricalView()
{
    _teamVisibilitySprite->setVisible(!_teamVisibilitySprite->isVisible());
}

void UIController::captureWorldAtPoint(Vec2 point)
{
    _teamVisibilitySprite->captureWorldAtPoint(point);
}


// Plans
void UIController::setCurrentPlan(Plan * plan)
{
    _currentPlan = plan;
    _orderDrawer->clear();
    if(_currentPlan != nullptr)
    {
        _orderDrawer->drawOrders(_currentPlan->getOrders(), true);
    }
    else
    {
        _orderDrawer->drawOrders(_unitToActiveOrderMap, false);
    }
}

void UIController::addPlan(Plan * plan)
{
    _planningMenu->addPlan(plan);
}

void UIController::removePlan(Plan * plan)
{
    _planningMenu->removePlan(plan);
}


void UIController::setObjectiveName(const std::string & name)
{
    
}

ObjectiveInfoDisplay * UIController::objectiveInfoForObjective(Objective * objective)
{
    return _objectiveInfos.at(objective->uID());
}

// // REPORTING FUNCTIONS ///////
// These functions generally act as model to UI handlers



void UIController::reportTeamVisibilityUpdate(std::unordered_map<ENTITY_ID, double> & sectorVisibleTime)
{
//    MicroSecondSpeedTimer timer1( [=](long long int time) {
//        LOG_DEBUG("Visibility Update  : %.1f \n", time / 1000.0);
//    });
    if(dumbTest)
    {
        _teamVisibilitySprite->captureEntireWorld();
        dumbTest = false;
    }
    for(auto & sectorPair : sectorVisibleTime)
    {
        MapSector * sector = globalSectorGrid->sectorForUID(sectorPair.first);

        int visPercent = sectorPair.second + VISIBILITY_MAX_FADE_TIME;
        if(visPercent < 0)
        {
            visPercent = 0;
        }
        visPercent *= VISIBILITY_MAX_FADE_MULT;
        if(visPercent > 255)
        {
            visPercent = 255;
        }
        visPercent = 255 - visPercent;
        if(_sectorVisibilityMap[sector->getSectorOrigin()] == 0 && visPercent > 0)
        {
            _teamVisibilitySprite->captureWorldAtPoint(sector->getSectorOrigin());
        }
        _sectorVisibilityMap[sector->getSectorOrigin()] = visPercent;
    }
    _teamVisibilitySprite->updateVisibility(_sectorVisibilityMap);
}


//Unit Groups
void UIController::reportNewUnitGroup(UnitGroup * newGroup)
{
    _unitGroupsMap.insert(newGroup->uID(), newGroup);
    globalEmblemManager->createEmblemForUnitGroup(newGroup, POSITION_EMBLEM);
}

UnitGroup * UIController::getUnitGroup(ENTITY_ID groupId)
{
    return _unitGroupsMap.at(groupId);
}

void UIController::reportUnitGroupRemoved(UnitGroup * groupRemoved)
{
    if(_unitGroupsMap.at(groupRemoved->uID()) != nullptr)
    {
        globalEmblemManager->removeAllEmblemsForCommandableObject(groupRemoved->uID());
        _unitGroupsMap.erase(groupRemoved->uID());
    }
}


void UIController::reportSectorVisible(MapSector * sector)
{
    
}

//command post related
void UIController::reportNewCommandPost(CommandPost * commandPost)
{
    // Hmmmm, we should probably have some kind of emblem for CommandPosts
}

void UIController::reportCommandPostDestroyed(CommandPost * commandPost)
{
    
}


void UIController::reportNewObjective(Objective * objective)
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    if(objective->getOwningTeam()->getTeamId() != team->getTeamId())
    {
        return;
    }
    
    ObjectiveInfoDisplay * newInfo = ObjectiveInfoDisplay::create(objective);
    _objectiveInfos.insert(objective->uID(), newInfo);
    newInfo->setVisible(true);
}

void UIController::reportObjectiveRemoved(Objective * objective)
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    if(objective->getOwningTeam()->getTeamId() != team->getTeamId())
    {
        return;
    }
    
    _objectiveInfos.erase(objective->uID());
}


//Unit related
void UIController::reportNewUnit(Unit * unit)
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    if(unit->getOwningTeam()->getTeamId() != team->getTeamId())
    {
        return;
    }
    globalEmblemManager->createEmblemForUnit(unit, POSITION_EMBLEM);
}


void UIController::reportUnitDestroyed(Unit * unit)
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    if(unit->getOwningTeam()->getTeamId() != team->getTeamId())
    {
        return;
    }
    
    globalEmblemManager->removeAllEmblemsForCommandableObject(unit->uID());
    
    // Remove the unit from all unit groups
    // Its possible that removing this unit from a group will result in that groups immediate deletion (alterning _unitGroupsMap),
    // so first copy the group pointers into a vector
    std::vector<UnitGroup *> unitGroups;
    for(auto group : _unitGroupsMap)
    {
        unitGroups.push_back(group.second);
    }
    
    for(auto group : unitGroups)
    {
        group->removeUnit(unit);
    }
    
    _unitToActiveOrderMap.erase(unit->uID());
    if(_currentPlan != nullptr)
    {
        _orderDrawer->drawOrders(_currentPlan->getOrders(), true);
    }
    else
    {
        _orderDrawer->drawOrders(_unitToActiveOrderMap, false);
    }
}

void UIController::reportUnitIssuedOrder(Order * order, Unit * unit)
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    if(unit->getOwningTeam()->getTeamId() != team->getTeamId())
    {
        return;
    }
    
    if( order->getOrderType() == STOP_ORDER)
    {
        _unitToActiveOrderMap.erase(unit->uID());
    }
    else
    {
        _unitToActiveOrderMap.emplace(unit->uID(), order->uID());
    }
    
    if(_currentPlan != nullptr)
    {
        _orderDrawer->drawOrders(_currentPlan->getOrders(), true);
    }
    else
    {
        _orderDrawer->drawOrders(_unitToActiveOrderMap, false);
    }
}



void UIController::reportOrderComplete(Unit * entCompleting, Order * order, bool failed)
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    if(entCompleting->getOwningTeam()->getTeamId() != team->getTeamId())
    {
        return;
    }
    _unitToActiveOrderMap.erase(entCompleting->uID());
    Emblem * emblem = globalEmblemManager->getEmblemForCommandableObjectWithType(entCompleting->uID(), ORDER_EMBLEM);
    if(emblem != nullptr)
    {
        globalEmblemManager->removeEmblemForCommandableObject(entCompleting->uID(), emblem);
    }
    
    // Inform any unit group's this unit is part of that the unit completed an order
    for(auto group : _unitGroupsMap)
    {
        if(group.second->containsUnit(entCompleting))
        {
            group.second->unitInGroupCompletedOrder(entCompleting, order->getOwningPlanId());
        }
    }

    _orderDrawer->drawOrders(_unitToActiveOrderMap, false);
}

void UIController::reportOrderCancelled(Unit * unit)
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    if(unit->getOwningTeam()->getTeamId() != team->getTeamId())
    {
        return;
    }
    _unitToActiveOrderMap.erase(unit->uID());
    Emblem * emblem = globalEmblemManager->getEmblemForCommandableObjectWithType(unit->uID(), ORDER_EMBLEM);
    if(emblem != nullptr)
    {
        globalEmblemManager->removeEmblemForCommandableObject(unit->uID(), emblem);
    }
    _orderDrawer->drawOrders(_unitToActiveOrderMap, false);
}


void UIController::reportCourierTransportingOrder(Order * order, Unit * destination)
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    if(destination->getOwningTeam()->getTeamId() != team->getTeamId())
    {
        return;
    }
    
    Emblem * orderEmblem =globalEmblemManager->getEmblemForCommandableObjectWithType(destination->uID(), ORDER_EMBLEM);
    if(orderEmblem != nullptr)
    {
        orderEmblem->setCanDrag(false);
        orderEmblem->addStatusIcon(EMBLEM_STATUS_NO_COMMAND);
        orderEmblem->setFlashing(true);
    }
}

void UIController::reportNewPlan(Plan * plan)
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    if(plan->getOwningTeam()->getTeamId() != team->getTeamId())
    {
        return;
    }
    _planningMenu->addPlan(plan);
}

void UIController::reportPlanExecuting(Plan * plan)
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    if(plan->getOwningTeam()->getTeamId() != team->getTeamId())
    {
        return;
    }
    _planningMenu->planExecuting(plan);
}


void UIController::reportPlanStopped(Plan * plan)
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    if(plan->getOwningTeam()->getTeamId() != team->getTeamId())
    {
        return;
    }
    _planningMenu->planStopped(plan);
}

void UIController::reportOrderAddedToPlan(Order * order, Unit * unit, Plan * plan)
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    if(plan->getOwningTeam()->getTeamId() != team->getTeamId())
    {
        return;
    }
    if(_currentPlan != nullptr)
    {
        _orderDrawer->drawOrders(_currentPlan->getOrders(), true);
    }
    else
    {
        _orderDrawer->drawOrders(_unitToActiveOrderMap, false);
    }
}

void UIController::reportOrderRemovedFromPlan(Unit * unit, Plan * plan)
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    if(plan->getOwningTeam()->getTeamId() != team->getTeamId())
    {
        return;
    }
    if(_currentPlan != nullptr)
    {
        _orderDrawer->drawOrders(_currentPlan->getOrders(), true);
    }
    else
    {
        _orderDrawer->drawOrders(_unitToActiveOrderMap, false);
    }
}

void UIController::reportPlanRenamed(Plan * plan, const std::string & newName)
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    if(plan->getOwningTeam()->getTeamId() != team->getTeamId())
    {
        return;
    }
    _planningMenu->planRenamed(plan, newName);
}

void UIController::reportPlanRemoved(Plan * plan)
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    if(plan->getOwningTeam()->getTeamId() != team->getTeamId())
    {
        return;
    }
    
    if(_currentPlan == plan)
    {
        _orderDrawer->clear();
    }
}



//friendly unit presence
void UIController::reportUnitInSector(MapSector * sector, Unit * unit)
{
    
}

//enemy unit presence
void UIController::reportEnemyUnitSpotted(Unit * enemy)
{
    
}

void UIController::reportEnemyCommandPostSpotted(CommandPost *enemy)
{
    
}

void UIController::reportEnemyCanEngageSector(MapSector * sector, Human * enemy)
{
    
}

void UIController::reportEnemyArtilleryInSector(MapSector * sector)
{
    
}

////////// End Report Functions

void UIController::drawPositioningBoundsForUnit(Unit * unit, DIRECTION orientation)
{
    _primativeDrawer->clear();
    _primativeDrawer->drawPolygon(positioningBounds, 4, Color4F(0, 0, 0, 0), 2, Color4F(0, 1, 0, 1));
}


bool UIController::togglePlanningMode()
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    _planningMode = !_planningMode;
    for(auto pair : team->getObjectives())
    {
        pair.second->setVisible(_planningMode);
    }
    return _planningMode;
}

void UIController::showTestMenu(Vec2 location)
{
    _entityMenu->clear();
    GameMenuBuilder::populateTestMenu(_entityMenu, location);
    _entityMenu->setPositionInGameWorld(location);
}

void UIController::createPathFindingTester(Vec2 position)
{
    PathFindingTester * tester = PathFindingTester::create(position);
    _testers.pushBack(tester);
}

void UIController::createVisibilityTester(Vec2 position)
{
    VisibilityTester * tester = VisibilityTester::create(position);
    _testers.pushBack(tester);
}

Tester * UIController::getTesterAtPoint(Vec2 point)
{
    for(Tester * tester : _testers)
    {
        if(tester->testCollisionWithPoint(point))
        {
            return tester;
        }
    }
    return nullptr;
}

void UIController::removeTester(Tester * tester)
{
    _testers.eraseObject(tester);
}

void UIController::populateContextMenu(Emblem * emblem, Vec2 location)
{
    _entityMenu->clear();
    _entityMenu->setPositionInGameWorld(location);
    
    if(emblem != nullptr)
    {
        GameMenuBuilder::populateForObject(_entityMenu, emblem, location);
    }
    else
    {
        Team * team = globalTeamManager->getLocalPlayersTeam();
        Objective * objective = team->getObjectiveAtPoint(location);
        if(objective != nullptr)
        {
            GameMenuBuilder::populateForObject(_entityMenu, objective, location);
        }
    }
    
    if(!_commandPostCreationDrawer->isVisible())
    {
        _entityMenu->addButton("Show Command Posts", [=](Ref* sender)
                               {
            _commandPostCreationDrawer->setVisible(true);
            for(auto pair : _objectiveInfos)
            {
                pair.second->setVisible(true);
            }
        });
    }
    else
    {
        _entityMenu->addButton("Hide Command Posts", [=](Ref* sender)
                               {
            _commandPostCreationDrawer->setVisible(false);
            for(auto pair : _objectiveInfos)
            {
                //                pair.second->setVisible(false);
            }
        });
    }
    
    _entityMenu->addButton("Toggle Underground", [=](Ref* sender)
    {
        world->toggleShowUnderground();
    });
}

void UIController::clearEntityMenu()
{
    _entityMenu->clear();
}

void UIController::clearDrawer()
{
    _primativeDrawer->clear();
}
