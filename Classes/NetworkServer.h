//
//  NetworkServer.hpp
//  TrenchWars
//
//  Created by Paul Reed on 3/11/24.
//

#ifndef NetworkServer_h
#define NetworkServer_h

#include "NetworkInterface.h"



class NetworkServer : public NetworkInterface
{
    HSteamListenSocket _listenSocket;
    HSteamNetPollGroup _pollGroup;
    
    std::unordered_map< HSteamNetConnection, NetworkClient_t * > _connectionToClientMap;
    std::unordered_map< int,  HSteamNetConnection > _playerToConnectionMap;
    
    virtual void pollIncomingMessages();
    
    
public:
    NetworkServer();
    virtual void startListening();
    virtual void OnSteamNetConnectionStatusChanged( SteamNetConnectionStatusChangedCallback_t *connectionInfo );
    virtual void sendPreparedDataToPlayer(MessageBuffer * buffer, int playerId = 0);
    virtual void sendPreparedDataToPlayers(MessageBuffer * buffer, std::list<int> & players);
};

#endif /* NetworkServer_h */
