#ifndef __LANDSCAPE_OBJECT_H__
#define __LANDSCAPE_OBJECT_H__
//
//  TrenchWars
//
//  Created by Paul Reed on 4/7/12.
//  Copyright (c) 2012 . All rights reserved.
//

#include "cocos2d.h"
#include "PhysicalEntity.h"
#include "TerrainCharacteristics.h"
#include "ChangeTrackingVariable.h"

class CollisionCell;
class StaticEntityDefinition;
class World;


class StaticEntity : public PhysicalEntity
{
protected:
    unsigned long long int _objectLandscapeType;
    int _updateCounter;
    AtomicChangeTrackingVariable<int> _health;
    int _maxHealth;
        
    StaticEntityDefinition * _definition;

    CC_CONSTRUCTOR_ACCESS :
    
    StaticEntity();
    virtual bool init(StaticEntityDefinition * definition, Vec2 position);
    virtual bool init(const std::string & definitionName, Vec2 position);

    void getContainingCells();

public:
    static StaticEntity * create(StaticEntityDefinition * definition, Vec2 position);
    static StaticEntity * create(const std::string & definitionName, Vec2 position);
    static StaticEntity * create();

    virtual void postLoad();
    
    virtual ENTITY_TYPE getEntityType() const {return STATIC_ENTITY;}

    unsigned int getLandscapeType() {return _objectLandscapeType;}
    void determineFacingWithCounter(int counter);
    
    virtual bool testCollisionWithLine(Vec2 start, Vec2 end);
    
    virtual void receiveDamage(int damage);
    virtual void removeFromGame();
    
    virtual void populateDebugPanel(GameMenu * menu, Vec2 location);
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

#endif //__LANDSCAPE_OBJECT_H__
