#ifndef __AI_STRATEGY_H__
#define __AI_STRATEGY_H__
//
//  AIStrategy.h
//  TrenchWars
//
//  Created by Paul Reed on 6/27/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#include "cocos2d.h"
#include <mutex>


USING_NS_CC;

enum AI_GOAL_TYPE
{
    BUILD_COMMAND_POST,
    OCCUPY,
    ATTACK,
    BOMBARD,
    RETREAT_FROM,
    FORITY
};

enum SECTOR_ACTION
{
    ADD,
    REMOVE,
    REPLACE,
    DO_NOT_CREATE_NEW,
    NOTHING
};

class PlanExecution;
class AICommand;
class MapSectorAnalysis;
class MapSector;

class AIStrategy : public Ref
{
public:
    //FIXME: make these private later
    AI_GOAL_TYPE _goalType;
    AICommand * _owningCommand;
    Vector<MapSectorAnalysis *> sectorsInStrategy;
    
    PlanExecution * execution;
    float importance;
    int requiredUnitStrenth;
    int _strategyID;
    
    Label * testText;
    std::mutex sectorsArrayMutex;

private:
    
    
    bool tryReplacingPrimarySector(MapSectorAnalysis * sector);

public:
    AIStrategy();
    virtual bool init(AI_GOAL_TYPE type, MapSectorAnalysis * initialSector, AICommand * command, int strategyID);
    static AIStrategy * create(AI_GOAL_TYPE type, MapSectorAnalysis * initialSector, AICommand * command, int strategyID);

    static float qualitativeVariableForGoal(AI_GOAL_TYPE goalType, MapSectorAnalysis * analysis);
    
    static std::string stringForGoalType(AI_GOAL_TYPE goalType);
    
    double distanceFromPrincipleSectorInStrategy(MapSector * sector);
    
    SECTOR_ACTION considerSector(MapSectorAnalysis * sector);

    void addSector(MapSectorAnalysis * sector);
    void removeSector(MapSectorAnalysis * sector);
    
    void updateStrategyScore();
    void deleteStrategy();
};

#endif //__AI_STRATEGY_H__
