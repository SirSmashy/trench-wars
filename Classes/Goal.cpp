//
//  Goal.cpp
//  TrenchWars
//
//  Created by Paul Reed on 6/12/24.
//

#include "Goal.h"
#include "Entity.h"
#include "Objective.h"
#include "CollisionCell.h"
#include "Unit.h"
#include "MultithreadedAutoReleasePool.h"
#include "Human.h"
#include "EntityManager.h"
#include "InteractiveObjectUtils.h"
#include "EngagementManager.h"
#include "TeamManager.h"
#include "UIController.h"
#include "GameEventController.h"



Goal::Goal()
{
    _importance = 0;
    _associatedOrderId = -1;
}

Goal::~Goal()
{
}

std::string Goal::getGoalTypeAsString()
{
    switch(getGoalType())
    {
        case MOVE_GOAL:
        {
            return "Move Goal";
        }
        case OBJECTIVE_WORK_GOAL:
        case ENTITY_WORK_GOAL: {
            return "Work Goal";
        }
        case ATTACK_POSITION_GOAL: {
            return "Attack Position Goal";
        }
        case ATTACK_TARGET_GOAL: {
            return "Attack Target Goal";
        }
        case RELOAD_WEAPON_GOAL: {
            return "Reload Weapon";
        }
        case LIE_PRONE_GOAL: {
            return "Lie Prone";
        }
    }
    return "Unknown goal";

}

void Goal::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    ComparableRef::loadFromArchive(archive);
    archive(_associatedOrderId);
    archive(_importance);
}

void Goal::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    ComparableRef::saveToArchive(archive);
    archive(_associatedOrderId);
    archive(_importance);
}

void Goal::removeFromGame()
{

}


Goal * Goal::createGoalOfType(GOAL_TYPE type)
{
    Goal * order = nullptr;
    switch (type) {
        case MOVE_GOAL:
        {
            order = MoveGoal::create();
            break;
        }
        case ENTITY_WORK_GOAL: {
            order = EntityWorkGoal::create();
            break;
        }
        case OBJECTIVE_WORK_GOAL: {
            order = ObjectiveWorkGoal::create();
            break;
        }
        case ATTACK_POSITION_GOAL: {
            order = AttackPositionGoal::create();
            break;
        }     
        case ATTACK_TARGET_GOAL: {
            order = AttackTargetGoal::create();
            break;
        }

    }
    return order;
}


MoveGoal::MoveGoal() : Goal()
{
    
}

MoveGoal::MoveGoal(Vec2 location, bool ignoreCover) : Goal()
{
    _position = location;
    _ignoreCover = ignoreCover;
    _waitingForOptimize = false;
    _needsOptimization = true;
}

MoveGoal::~MoveGoal()
{
//    LOG_DEBUG("Bye bye MOVE GOAL %lld \n", uID());
}


MoveGoal * MoveGoal::create(Vec2 location, bool ignoreCover)
{
    MoveGoal * newGoal = new MoveGoal(location, ignoreCover);
    globalAutoReleasePool->addObject(newGoal);
    return newGoal;
}

MoveGoal * MoveGoal::create()
{
    MoveGoal * newGoal = new MoveGoal();
    globalAutoReleasePool->addObject(newGoal);
    return newGoal;
}

void MoveGoal::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Goal::loadFromArchive(archive);
    archive(_position);
    archive(_ignoreCover);
    archive(_needsOptimization);
}

void MoveGoal::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Goal::saveToArchive(archive);
    archive(_position);
    archive(_ignoreCover);
    archive(_needsOptimization);
}

LieProneGoal::LieProneGoal() : Goal()
{
    
}

LieProneGoal::~LieProneGoal()
{
    //    LOG_DEBUG("Bye bye STOP GOAL %lld \n", uID());
}

LieProneGoal * LieProneGoal::create()
{
    LieProneGoal * newGoal = new LieProneGoal();
    globalAutoReleasePool->addObject(newGoal);
    return newGoal;
}

void LieProneGoal::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Goal::loadFromArchive(archive);
}

void LieProneGoal::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Goal::saveToArchive(archive);
}


WorkGoal::WorkGoal() : Goal()
{
    
}

WorkGoal::~WorkGoal()
{
//    LOG_DEBUG("Bye bye WORK GOAL %lld \n", uID());

}


EntityWorkGoal::EntityWorkGoal(PhysicalEntity * workObject) : WorkGoal()
{
    _workObject = workObject;
}

EntityWorkGoal * EntityWorkGoal::create(PhysicalEntity * workObject)
{
    EntityWorkGoal * newGoal = new EntityWorkGoal(workObject);
    globalAutoReleasePool->addObject(newGoal);
    return newGoal;
}

Vec2 EntityWorkGoal::getAverageWorkPosition()
{
    return _workObject->getPosition();
}

bool EntityWorkGoal::containsEntity(PhysicalEntity * ent)
{
    return _workObject->uID() == ent->uID();
}

PhysicalEntity * EntityWorkGoal::getEntityNearestPosition(Vec2 position)
{
    return _workObject;
}

void EntityWorkGoal::workCompleteForEntity(PhysicalEntity * ent)
{
    _workObject = nullptr;
}

void EntityWorkGoal::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Goal::loadFromArchive(archive);
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        _workObject = dynamic_cast<PhysicalEntity *>(globalEntityManager->getEntity(thingId));
    }
}

void EntityWorkGoal::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Goal::saveToArchive(archive);
    archive(_workObject->uID());

}

// ////////


ObjectiveWorkGoal::ObjectiveWorkGoal(ObjectiveObjectCollection * workObjects) : WorkGoal()
{
    _workObjects = workObjects;
}

ObjectiveWorkGoal * ObjectiveWorkGoal::create(ObjectiveObjectCollection * workObjects)
{
    ObjectiveWorkGoal * newGoal = new ObjectiveWorkGoal(workObjects);
    globalAutoReleasePool->addObject(newGoal);
    return newGoal;
}

Vec2 ObjectiveWorkGoal::getAverageWorkPosition()
{
    return _workObjects->getAveragePosition();
}

bool ObjectiveWorkGoal::containsEntity(PhysicalEntity * ent)
{
    return _workObjects->containsEntity(ent);
}

PhysicalEntity * ObjectiveWorkGoal::getEntityNearestPosition(Vec2 position)
{
    return _workObjects->getEntityNearestPosition(position);
}

void ObjectiveWorkGoal::workCompleteForEntity(PhysicalEntity * ent)
{
    _workObjects->removeEntity(ent);
}

void ObjectiveWorkGoal::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Goal::loadFromArchive(archive);
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        Objective * obj = dynamic_cast<Objective *>(globalEntityManager->getEntity(thingId));
        ObjectCollectionType type;
        archive(type);
        globalEngagementManager->requestMainThreadFunction([this,obj,type]() {
            if(type == OBJECT_COLLECTION_BUNKERS)
            {
                _workObjects = obj->getBunkers();
            }
            else if(type == OBJECT_COLLECTION_TRENCH_LINE)
            {
                _workObjects = obj->getTrenchLine();
            }
        });
    }
}

void ObjectiveWorkGoal::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Goal::saveToArchive(archive);
    archive(_workObjects->getOwningObjective()->uID());
    archive(_workObjects->getObjectCollectionType());
}

// //////////
ReloadWeaponGoal::ReloadWeaponGoal(Weapon * weapon) : Goal()
{
    _weapon = weapon;
}

ReloadWeaponGoal::~ReloadWeaponGoal()
{
//    LOG_DEBUG("Bye bye reload goal %lld \n", uID());
}

ReloadWeaponGoal * ReloadWeaponGoal::create(Weapon * weapon)
{
    ReloadWeaponGoal * newGoal = new ReloadWeaponGoal(weapon);
    globalAutoReleasePool->addObject(newGoal);
    return newGoal;
}

void ReloadWeaponGoal::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Goal::loadFromArchive(archive);
    ENTITY_ID weaponId;
    archive(weaponId);
    _weapon = dynamic_cast<Weapon *>(globalEntityManager->getEntity(weaponId));
}

void ReloadWeaponGoal::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Goal::saveToArchive(archive);
    archive(_weapon->uID());

}


// /////////

AttackPositionGoal::AttackPositionGoal() : Goal()
{
}

AttackPositionGoal::AttackPositionGoal(Objective * objective)  : Goal()
{
    _objective = objective;
}

AttackPositionGoal::~AttackPositionGoal()
{
//    LOG_DEBUG("Bye bye ATTACK POSITION GOAL %lld \n", uID());
}

AttackPositionGoal * AttackPositionGoal::create(Objective * objective)
{
    AttackPositionGoal * newGoal = new AttackPositionGoal(objective);
    globalAutoReleasePool->addObject(newGoal);
    return newGoal;
}


AttackPositionGoal * AttackPositionGoal::create()
{
    AttackPositionGoal * newGoal = new AttackPositionGoal();
    globalAutoReleasePool->addObject(newGoal);
    return newGoal;
}

Vec2 AttackPositionGoal::getRandomTargetPoint()
{
    return _objective->getRandomPointInObjective();
}

void AttackPositionGoal::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Goal::loadFromArchive(archive);
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        _objective = dynamic_cast<Objective *>(globalEntityManager->getEntity(thingId));
        if(_objective == nullptr)
        {
            globalEngagementManager->requestMainThreadFunction([this,thingId]() {
                _objective = dynamic_cast<Objective *>(globalEntityManager->getEntity(thingId));
            });
        }
    }
    
}

void AttackPositionGoal::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Goal::saveToArchive(archive);
    archive(_objective->uID());
}



// //////////////////



AttackTargetGoal::AttackTargetGoal() : Goal() ,
_selectWeaponCount(2),
_checkTargetCount(2)
{
    _shouldCharge = false;
    _selectWeaponCount.setToMaximum();
    _checkTargetCount.setToMaximum();
}

AttackTargetGoal::AttackTargetGoal(Human * target)  : Goal() ,
_selectWeaponCount(2),
_checkTargetCount(2)
{
    _shouldCharge = false;
    _target = target;
    _selectWeaponCount.setToMaximum();
    _checkTargetCount.setToMaximum();
}

AttackTargetGoal::~AttackTargetGoal()
{
//    LOG_DEBUG("Bye bye ATTACK TARGET GOAL %lld \n", uID());
}

AttackTargetGoal * AttackTargetGoal::create(Human * target)
{
    AttackTargetGoal * newGoal = new AttackTargetGoal(target);
    globalAutoReleasePool->addObject(newGoal);
    return newGoal;
}

AttackTargetGoal * AttackTargetGoal::create()
{
    AttackTargetGoal * newGoal = new AttackTargetGoal();
    globalAutoReleasePool->addObject(newGoal);
    return newGoal;
}

bool AttackTargetGoal::shouldSelectWeapon()
{
    return _selectWeaponCount.count();
}

bool AttackTargetGoal::shouldCheckTarget()
{
    return _checkTargetCount.count();
}


void AttackTargetGoal::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Goal::loadFromArchive(archive);
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        _target = dynamic_cast<Human *>(globalEntityManager->getEntity(thingId));
    }
    archive(_targetAcquireTime);
    archive(_targetHeight);
    archive(_shouldCharge);
}

void AttackTargetGoal::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Goal::saveToArchive(archive);
    if(_target != nullptr)
    {
        archive(_target->uID());
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
    archive(_targetAcquireTime);
    archive(_targetHeight);
    archive(_shouldCharge);
}


StopGoal::StopGoal() : Goal()
{
    _importance = 1.5;
}

StopGoal::~StopGoal()
{
//    LOG_DEBUG("Bye bye STOP GOAL %lld \n", uID());
}

StopGoal * StopGoal::create()
{
    StopGoal * newGoal = new StopGoal();
    globalAutoReleasePool->addObject(newGoal);
    return newGoal;
}

void StopGoal::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Goal::loadFromArchive(archive);
}

void StopGoal::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Goal::saveToArchive(archive);
}

