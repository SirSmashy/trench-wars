//
//  TerrainCharacteristics.h
//  TrenchWars
//
//  Created by Paul Reed on 9/22/20.
//

#ifndef TerrainCharacteristics_h
#define TerrainCharacteristics_h

enum GROUNDTYPE
{
    DIRT,
    GRASS,
    SNOW,
    SWAMP,
    ROAD
};

enum MOVEMENT_PREFERENCE
{
    MOVE_STRONGLY_PREFER_COVER = 0,
    MOVE_PREFER_COVER = 1,
    MOVE_NO_PREFERNCE = 2,
    MOVE_PREFER_SPEED = 3,
    MOVE_STRONGLY_PREFER_SPEED = 4
};

class TerrainCharacteristics
{
    char _height = 0;
    unsigned char _cover = 0; // Cover represents how much space in the cell is filled by something
    unsigned char _hardness = 0; // Hardness represents how hard or tough the contents of the cell is
    bool _impassable = 0; //Whether this terrain can be traversed
    
    // Calculated variables
    int _hash = 0;
    unsigned char _concealmentValue; // Concealment represents how easy it is to see a human in this terrain. It is ((_cover ^ 1.8) / (max possible value of _cover ^ 1.8))
    unsigned char _coverTimesHardness;
    unsigned char _inverseCover;
    unsigned short _totalSecurity; //How secure a human in this cell (or behind it) will be. A steel wall would provide both cover and concealment and thus be very secure. A bush would provide good concealment but low cover so would be only somewhat secure
    unsigned short _securityWithoutHeightCover;
    bool _unique = false;
    
    
public:
    TerrainCharacteristics();
    TerrainCharacteristics(char height, unsigned char cover, unsigned char hardness, bool impassable, bool unique = false);
    void set(char height, unsigned char cover, unsigned char hardness, bool impassable, bool unique = false);
    bool operator!= (const TerrainCharacteristics & other ) const;
    bool operator== (const TerrainCharacteristics & other ) const;
    
    unsigned char getConcealmentValue() const {return _concealmentValue;} //Cover that will hide you
    unsigned char getHeightChanceToHit(char height = 0) const; //range of 0-100
    unsigned short getTotalSecurity() const {return _totalSecurity;}
    unsigned short getSecurityWithoutHeight() const {return _securityWithoutHeightCover;}

    float getMovementCost(MOVEMENT_PREFERENCE movePreference = MOVE_NO_PREFERNCE, bool ignoreImpassable = false) const;
    
    char getHeight() const {return _height;}
    unsigned char getCoverValue() const {return _cover;}
    unsigned char getHardness() const {return _hardness;}
    bool  getImpassable() const {return _impassable;}
    
    
    template<class A>
    void save(A & archive) const
    {
        archive(_height);
        archive(_cover);
        archive(_hardness);
        archive(_impassable);
        archive(_unique);

        
        // Might want to just compute these
        archive(_hash);
        archive(_coverTimesHardness);
        archive(_inverseCover);
    }
    
    template<class A>
    void load(A & archive)
    {
        archive(_height);
        archive(_cover);
        archive(_hardness);
        archive(_impassable);
        archive(_unique);

        
        // Might want to just compute these
        archive(_hash);
        archive(_coverTimesHardness);
        archive(_inverseCover);
    }
};

enum LANDSCAPE_OBJECT_TYPE
{
    TRENCH,
    CRATOR,
    BUSH,
    SMALL_TREE,
    TREE,
    RUBBLE,
    WOOD_BUILDING,
    STONE_BUILDING,
    BUNKER_ENTRANCE
};


#endif /* TerrainCharacteristics_h */
