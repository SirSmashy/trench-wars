#ifndef __MAIN_MENU_SCENE_H__
#define __MAIN_MENU_SCENE_H__

#include "cocos2d.h"
using namespace cocos2d::ui;
USING_NS_CC;


class MainMenuScene : public cocos2d::Scene
{
    void createSaveMenu();

public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    //button callbacks
    void hostGameCallback(cocos2d::Ref* pSender);
    void joinGameCallback(cocos2d::Ref* pSender);
    void startSinglePlayerCallback(cocos2d::Ref* pSender);
    void startSaveCallback(cocos2d::Ref* pSender);

    void startPlaygroundCallback(cocos2d::Ref* pSender);

    
    void showSavesButtonCallback(cocos2d::Ref* pSender);

    void backButtonCallback(cocos2d::Ref* pSender);
    
    void testCallback(cocos2d::Ref* pSender);
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    CREATE_FUNC(MainMenuScene);
    
    Menu * mainMenu;
    Menu * saveGameMenu;

};

#endif // __MAIN_MENU_SCENE_H__
