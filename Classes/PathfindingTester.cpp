//
//  PathfindingTester.cpp
//  TrenchWars
//
//  Created by Paul Reed on 11/26/20.
//

#include "PathfindingTester.h"
#include "NavigationGrid.h"
#include "EntityFactory.h"
#include "PathFinding.h"
#include "CollisionGrid.h"
#include "Team.h"
#include "EntityManager.h"
#include "EmblemManager.h"
#include "MultithreadedAutoReleasePool.h"
#include "GameStatistics.h"
#include "TestBed.h"
#include "Path.h"
#include "UIController.h"

#define ENTITY_COUNT 50

PathFindingTester::PathFindingTester()
{
    
}
    
void PathFindingTester::init(Vec2 position)
{
    _movementPreference = MOVE_STRONGLY_PREFER_COVER;
    
    _lineDrawer = LineDrawer::create(position * WORLD_TO_GRAPHICS_SIZE, position * WORLD_TO_GRAPHICS_SIZE, 1);
    _lineDrawer->makeDashed();
    _lineDrawer->setColor(Color3B(255,225,13));
    _lineDrawer->setFlowing(true);
    _primativeDrawer = PrimativeDrawer::create();
    _primativeDrawer->retain();
    _primativeDrawer->setLineWidth(5);
    
    _startEmblem = globalEmblemManager->createEmblemForTester(this, false);
    _startEmblem->setAnimation(EMBLEM_ORDER, false);
    _startEmblem->setPosition(position);
    position.x -= 5;
    
    _endEmblem = globalEmblemManager->createEmblemForTester(this, false);
    _endEmblem->setAnimation(EMBLEM_MOVE, false);
    _endEmblem->setPosition(position);
    
    setPathsCount(1);
    
    _testColors[0] = Color4F::RED;
    _testColors[1] = Color4F::ORANGE;
    _testColors[2] = Color4F::YELLOW;
    _testColors[3] = Color4F::GRAY;
    _testColors[4] = Color4F::BLUE;
    _testColors[5] = Color4F::MAGENTA;
    _testColors[6] = Color4F::WHITE;
    
    for(int i = 0; i < 7; i++)
    {
        _testColors[0].a = 0.5;
    }

    
    update();
}
    
PathFindingTester * PathFindingTester::create(Vec2 position)
{
    PathFindingTester * newTester = new PathFindingTester();
    newTester->init(position);
    globalAutoReleasePool->addObject(newTester);
    return newTester;
}

Emblem * PathFindingTester::getEmblemAtPoint(Vec2 location)
{
    location *= WORLD_TO_GRAPHICS_SIZE;
    if(_startEmblem->testCollisionWithCircle(location, 32))
    {
        return _startEmblem;
    }
    else return _endEmblem;
}
    

bool PathFindingTester::testCollisionWithPoint(Vec2 point)
{
    point *= WORLD_TO_GRAPHICS_SIZE;
    return _startEmblem->testCollisionWithCircle(point,WORLD_TO_GRAPHICS_SIZE) || _endEmblem->testCollisionWithCircle(point, WORLD_TO_GRAPHICS_SIZE);
}

int PathFindingTester::setPathsCount(int pathsCount)
{
    for(auto path : _pathTests)
    {
        path->path->clear();
        path->path->release();
        path->routeNodes.clear();
        path->pathNodes.clear();
        world->removeChild(path->costLabel);
        path->costLabel->release();
    }
    _pathTests.clear();
    
    Vec2 offset;
    for(int i = 0; i < pathsCount; i++)
    {
        PathTest * test = new PathTest();
        test->path = new Path(9, true, offset);
        test->debug = globalTestBed->createDebugEntityAtPosition(offset, "", Vec2::ZERO, false);
        
        test->costLabel = Label::createWithTTF("", "arial.ttf", 30);
        test->costLabel->retain();
        test->offset = offset;
        world->addChild(test->costLabel);
        _pathTests.pushBack(test);
        test->release();
        offset.y += 1;
        if(offset.y > 50)
        {
            offset.x += 4;
            offset.y = 0;
        }
    }
}


void PathFindingTester::update()
{
    for(auto test : _pathTests)
    {
        test->routeNodes.clear();
        test->pathNodes.clear();
    }
    
    _primativeDrawer->clear();
    int i = 0;
    for(auto test : _pathTests)
    {
        
        updateSinglePath(test, _testColors[i]);
        i++;
        if(i > 6)
        {
            i = 0;
        }
    }
    
    //    LOG_DEBUG("Test Path %.0f,%0.f -- %0.f,%0.f \n", start.x, start.y, end.x, end.y);
    
    
}

void PathFindingTester::updateSinglePath(PathTest * pathTest, Color4F color)
{
    bool doDebug = pathTest->debug->isDebugMe();
    if(doDebug)
    {
        for(auto debug : pathDebugEnts)
        {
            globalTestBed->removeDebugEntity(debug);
        }
        pathDebugEnts.clear();
    }

    
    std::vector<Vec2> routes;
    std::vector<Vec2> paths;
    
    Vec2 start = world->getWorldPosition(_startEmblem->getPosition() ) + Vec2(0.5,0.5) + pathTest->offset;
    Vec2 end = world->getWorldPosition(_endEmblem->getPosition() ) + Vec2(0.5,0.5) + pathTest->offset;
    
//    MicroSecondSpeedTimer timer( [] (long long int time) {
//        LOG_DEBUG("Route Time: %f \n", time/1000.0);
//    });
//
    pathTest->debug->clearLines();
    pathTest->debug->setPosition(start);
    pathTest->path->buildPath(start, end, _movementPreference);
        
    for(auto routeNode : pathTest->path->getRouteNodes())
    {
        pathTest->routeNodes.pushBack(routeNode);
        if(routeNode->getRouteType() == ROUTE_TYPE_SECTOR)
        {
            SectorRouteNode * route = (SectorRouteNode *) routeNode;
//            routes.push_back(route->getPosition());
        }
        else if(routeNode->getRouteType() == ROUTE_TYPE_TRANSITION)
        {
            TransitionRouteNode * transition = (TransitionRouteNode *) routeNode;
//            routes.push_back(transition->getTransitionStart());
            
        }
        else if(routeNode->getRouteType() == ROUTE_TYPE_POSITION)
        {
            RouteNode * path = (RouteNode *) routeNode;
//            routes.push_back(path->getPosition());
        }
    }
    
//    timer.stop();
//    MicroSecondSpeedTimer timer2( [] (long long int time) {
//        LOG_DEBUG("  All Paths Time: %f \n", time/1000.0);
//    });
    Vec2 previous  = start;
    double colorMult = 0.25;
    
    PathStatus status = pathTest->path->getPathStatus();
    Color4F debugColor = Color4F::WHITE;
    Vec2 segmentEnd;
    while(status != PATH_STATUS_DONE)
    {
        if(status == PATH_STATUS_NEEDS_NEXT_SEGMENT)
        {
            pathTest->path->buildNextPathSegment(previous, &segmentEnd);
            if(doDebug)
            {
                routes.push_back(segmentEnd);
                updatePathDebug( debugColor );
            }
        }
        else
        {
            pathTest->pathNodes.pushBack(pathTest->path->getNextPathNode());
            previous = pathTest->path->getNextPathPosition();
            paths.push_back(previous);
        }
        status = pathTest->path->getPathStatus();
    }
//    timer2.stop();
    
    if(doDebug)
    {
        Color4F routeColor = Color4F::RED;
        routeColor.a = .25;
        previous = world->getAboveGroundPosition(start);
        for(auto position : routes)
        {
            position = world->getAboveGroundPosition(position);
            _primativeDrawer->drawTriangle(position * WORLD_TO_GRAPHICS_SIZE, 64, routeColor);
            previous = position;
        }
    }
    
    if(doDebug)
    {
        color = Color4F::BLACK;
        color.a = 0.75;
    }
    
    previous  = world->getAboveGroundPosition(start) * WORLD_TO_GRAPHICS_SIZE;
    for(auto position : paths)
    {
        position = world->getAboveGroundPosition(position) * WORLD_TO_GRAPHICS_SIZE;
        pathTest->debug->primativeDrawer->drawDot(position, 8, color);
        pathTest->debug->addLine(previous, position, Color3B(color), true, true);
        pathTest->debug->primativeDrawer->drawLine(previous, position, color);
        previous = position;
    }
    
    start  = world->getAboveGroundPosition(start) * WORLD_TO_GRAPHICS_SIZE;
    end  = world->getAboveGroundPosition(start) * WORLD_TO_GRAPHICS_SIZE;

    pathTest->debug->primativeDrawer->drawDot(start, 12, color);
    pathTest->debug->primativeDrawer->drawDot(end, 12, color);

    
}


void PathFindingTester::updatePathDebug(Color4F color)
{
    int count = 0;
    Color4F lineColor = color;
    lineColor.a *= .5;
    for(auto pair : globalPathFinding->pathDebug)
    {
        Vec2 position = pair.second->getPosition()->getCellPosition();
        std::ostringstream os;
        os.precision(1);
        
        CollisionCell * goalCell = pair.second->getGoalCell();
        int goalX = -1;
        int goalY = -1;
        
        if(goalCell != nullptr)
        {
            goalX = goalCell->getXIndex();
            goalY = goalCell->getYIndex();
        }
        
        double cost = -1;
        
        PathBuildingNode * parent = pair.second->getParent();
        if(parent != nullptr)
        {
            cost = (pair.second->getCostToTravelHere() - parent->getCostToTravelHere()) / 100;
        }
        
        double moveCost = 0;
        
        if(PatchPathBuildingNode * patch = dynamic_cast<PatchPathBuildingNode *>(pair.second))
        {
            moveCost = patch->getNavigationPatch()->getMovementCost(MOVE_STRONGLY_PREFER_COVER);
        }
        
        os << std::fixed << "So Far: " << (pair.second->getCostToTravelHere() / 100)   << " + Heur: " << (pair.second->getRemainingCostHeuristic() / 100) << " = Tot: " << (pair.second->getEstimatedTotalCost() / 100) << std::endl  <<
        "P Dist: " << pair.second->getDistanceFromParent() << " Tot dist : " << pair.second->totalDistance << " U: " << pair.second->updates <<std::endl <<
        " node cost " << cost << " move cost " << (moveCost / 100) << std::endl <<
        "cell: " << pair.second->getPosition()->getXIndex() << ", " << pair.second->getPosition()->getYIndex() << " Goal: " << goalX << ", " << goalY << std::endl <<
        "Patch Cong: " << pair.second->getCongestionMultiplier() << " Actual Cong: " << pair.second->congestion;
        
        Vec2 labelOffset;
        Color4F dotColor = color;
        if(pair.second->getPathTypeInt() == 1)
        {
            position.x += 4;
            position.y += 4;
            dotColor.r *= .5;
            dotColor.g *= .5;
            dotColor.b *= .5;
        }
        
        DebugEntity * debug = globalTestBed->createDebugEntityAtPosition(position, os.str() , labelOffset, true);
        pathDebugEnts.pushBack(debug);
        
        _primativeDrawer->drawDot(position * WORLD_TO_GRAPHICS_SIZE, 4, dotColor);
        if(pair.second->usedInPath)
        {
            _primativeDrawer->drawDot(position * WORLD_TO_GRAPHICS_SIZE, 1.5, Color4F::BLACK);
        }
        if(parent != nullptr)
        {
            _primativeDrawer->drawLine(parent->getPosition()->getCellPosition() * WORLD_TO_GRAPHICS_SIZE,
                                       position * WORLD_TO_GRAPHICS_SIZE, lineColor);
        }
        count++;
    }
}

void PathFindingTester::removeFromGame()
{
    _primativeDrawer->remove();
    _primativeDrawer->release();
    
    globalEmblemManager->removeAllEmblemsForCommandableObject(uID());
    globalUIController->removeTester(this);
}

