//
//  EntityDebugDrawer.hpp
//  TrenchWars
//
//  Created by Paul Reed on 7/8/22.
//

#ifndef EntityDebugDrawer_h
#define EntityDebugDrawer_h

#include <stdio.h>
#include "cocos2d.h"
USING_NS_CC;


class EntityDebugDrawer : public DrawNode
{
    bool _drawTargets;
    EntityDebugDrawer();
    bool init();
    void drawTargettingInfo();
    
public:
    static EntityDebugDrawer * create();
    
    void toggleDrawTargets() {_drawTargets = !_drawTargets;}
    
    virtual void update(float deltaTime);
    void remove();

};

#endif /* EntityDebugDrawer_h */
