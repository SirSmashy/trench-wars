//
//  Team.cpp
//  TrenchWars
//
//  Created by Paul Reed on 12/13/23.
//

#include "Team.h"
#include "Command.h"
#include "Objective.h"
#include "MultithreadedAutoReleasePool.h"
#include "GameEventController.h"
#include "AICommand.h"
#include "MenuLayer.h"
#include "MapSector.h"
#include "TeamManager.h"
#include "UIController.h"
#include "SectorGrid.h"
#include "InteractiveObjectUtils.h"
#include "EntityManager.h"

void Team::init(TeamDefinition * teamDef)
{
    _teamId = teamDef->teamId;
    for(CommandDefinition * command : teamDef->commands)
    {
        createCommand(command);
    }
    
    for(int x = 0; x < globalSectorGrid->getSectorGridSize().width; x++)
    {
        for(int y = 0; y < globalSectorGrid->getSectorGridSize().height; y++)
        {
            _sectorVisibleTime.try_emplace(globalSectorGrid->sectorAtIndex(x, y)->uID(), -100.0);
        }
    }
}

void Team::init()
{
    
}

Team * Team::create(TeamDefinition * teamDef)
{
    Team * team = new Team();
    team->init(teamDef);
    globalAutoReleasePool->addObject(team);
    return team;
}

Team * Team::create()
{
    Team * team = new Team();
    team->init();
    globalAutoReleasePool->addObject(team);
    return team;
}

void Team::createCommand(CommandDefinition * commandDef)
{
    Command * command;
    if(commandDef->aiCommand)
    {
        command = AICommand::create(this, commandDef);
    }
    else
    {
        command = Command::create(this, commandDef);
    }
    _commands.pushBack(command);
    command->_debugSpawnMode = commandDef->spawnAction;
    command->spawnInitialUnits(commandDef);
}

void Team::postLoad()
{

}


void Team::query(float deltaTime)
{
    for(auto command : _commands)
    {
        command->query(deltaTime);
    }
    std::unordered_set<ENTITY_ID> sectorsNewlyHidden;
    for(auto & sectorVis : _sectorVisibleTime)
    {
        if(sectorVis.second > 0 && sectorVis.second < deltaTime)
        {
            sectorsNewlyHidden.insert(sectorVis.first);
        }
        sectorVis.second -= deltaTime;
    }
    
    globalGameEventController->updateTeamVisibility(this, _sectorVisibleTime, sectorsNewlyHidden, _sectorsNewlyVisible);
}

Command * Team::getCommandForId(unsigned int commandId)
{
    for(auto command : _commands)
    {
        if(command->getCommandID() == commandId)
        {
            return command;
        }
    }
    return nullptr;
}

void Team::addPlanningObject(PlanningObject * object)
{
    std::lock_guard<std::mutex> lock(_entitiesMutex);
    _planningObjects.emplace(object->uID(), object);
    if(object->getEntityType() == OBJECTIVE_ENTITY)
    {
        Objective * objective = (Objective *) object;
        _objectives.emplace(objective->uID(), objective);
    }
}

void Team::removePlanningObject(PlanningObject * object)
{
    std::lock_guard<std::mutex> lock(_entitiesMutex);
    _planningObjects.erase(object->uID());
    if(object->getEntityType() == OBJECTIVE_ENTITY)
    {
        _objectives.erase(object->uID());
    }
}


void Team::addPlan(Plan * plan)
{
    _plans.insert(plan->uID(), plan);
}

std::vector<Plan *> Team::getPlans()
{
    std::vector<Plan *> plans;
    for(auto pair : _plans)
    {
        plans.push_back(pair.second);
    }
    return plans;
}

Plan * Team::getPlanWithId(ENTITY_ID planId)
{
    return _plans.at(planId);
}

void Team::removePlan(Plan * plan)
{
    _plans.erase(plan->uID());
}

void Team::updateCommandPostLinking()
{
    for(Command * command : _commands)
    {
        command->updateCommandPostLinking();
    }
}

bool Team::isPositionClaimed(CollisionCell * cell)
{
    std::lock_guard<std::recursive_mutex> guard(_claimedCellsMutex);
    return _claimedCells.find(cell) != _claimedCells.end();
}

bool Team::tryToClaimPosition(CollisionCell * cell, ENTITY_ID claimant)
{
    _claimedCellsMutex.lock();
    auto it = _claimedCells.find(cell);
    if(it == _claimedCells.end() || it->second == claimant) {
        _claimedCells.emplace(cell, claimant);
        if(cell->getMapSector() != nullptr)
        {
            cell->getMapSector()->entityClaimedCellInSector(_teamId, cell);
        }
        _claimedCellsMutex.unlock();
        return true;
    }
    _claimedCellsMutex.unlock();
    return false;
}

void Team::releasePositionClaim(CollisionCell * cell)
{
    _claimedCellsMutex.lock();
    if(cell->getMapSector() != nullptr)
    {
        cell->getMapSector()->entityReleasedClaimInSector(_teamId, cell);
    }
    _claimedCells.erase(cell);
    _claimedCellsMutex.unlock();
}

void Team::setPositionClaim(CollisionCell * cell, ENTITY_ID claimant)
{
    _claimedCellsMutex.lock();
    if(cell->getMapSector() != nullptr)
    {
        cell->getMapSector()->entityClaimedCellInSector(_teamId, cell);
    }
    _claimedCells.insert_or_assign(cell, claimant);
    _claimedCellsMutex.unlock();
}

void Team::unitsInRect(Rect rect, Vector<Unit *> & units, bool controlledByPlayerOnly)
{
    for(auto command : _commands)
    {
        if(!controlledByPlayerOnly || command->isLocalPlayer())
        {
            command->unitsInRect(rect, units);
        }
    }
}

Objective * Team::getObjectiveAtPoint(Vec2 point)
{
    for(auto pair: _objectives)
    {
        if(pair.second->isVisible() && pair.second->pointInObjective(point))
        {
            return pair.second;
        }
    }
    return nullptr;
}

CommandPost * Team::getCommandPostAtPoint(Vec2 point)
{
    CommandPost * post = nullptr;
    for(auto command : _commands)
    {
        post = command->getCommandPostAtPoint(point);
        if(post != nullptr)
        {
            return post;
        }
    }
    return nullptr;
}

Vec2 Team::getValidCommandPostPositionTowardPosition(Vec2 position)
{
    CommandPost * nearest = getNearestCommandPost(position);
    double distance = nearest->getPosition().distance(position);
    Vec2 goalPostion = nearest->getPosition().lerp(position, 500 / distance);
    return goalPostion;
}

CommandPost * Team::getNearestCommandPost(Vec2 position, bool onlyLinked)
{
    CommandPost * nearest = nullptr;
    double distance;
    for(auto command : _commands)
    {
        for(auto post : command->getCommandPosts())
        {
            if(onlyLinked && !post->isLinkedToPrimaryCommandPost())
            {
                continue;
            }
            if(nearest == nullptr)
            {
                nearest = post;
                distance = post->getPosition().distance(position);
                continue;
            }
            else if(post->getPosition().distance(position) < distance)
            {
                nearest = post;
                distance = post->getPosition().distance(position);
            }
        }
    }
    return nearest;
}


bool Team::isPositionInCommandArea(Vec2 position)
{
    position = world->getAboveGroundPosition(position);

    bool inCommandArea = false;
    for(auto command : _commands)
    {
        inCommandArea = command->isPositionInCommandArea(position);
        if(inCommandArea)
        {
            return true;
        }
    }
    return false;
}

bool Team::isPositionValidForNewCommandPost(Vec2 position)
{
    bool withinCreationZone = false;
    for(auto command : _commands)
    {
        for(CommandPost * post : command->getCommandPosts())
        {
            float distance = position.distance(post->getPosition());
            if(distance < post->getCreationRadius())
            {
                return false;
            }
            if(distance < (post->getControlRadius() * 2)) // multiply by 2 because the new command post's control radius must overlap with this command post's control radius
            {
                withinCreationZone = true;
            }
        }
    }
    return withinCreationZone;
}

void Team::markSectorsVisible(std::unordered_set<ENTITY_ID> & sectors)
{
    _visibilityMutex.lock();
    for(auto sector : sectors)
    {
        if(_sectorVisibleTime.at(sector) <= 0)
        {
            _sectorsNewlyVisible.insert(sector);
        }
        _sectorVisibleTime.at(sector) = 10.0; //todo fix magic number
    }
    _visibilityMutex.unlock();
}


void Team::saveToArchive(cereal::BinaryOutputArchive & archive)
{
    saveBasicInfoToArchive(archive);
    archive(_commands.size());
    for(auto command : _commands)
    {
        command->saveToArchive(archive);
    }
    
    archive(_planningObjects.size());
    for(auto planningObj : _planningObjects)
    {
        archive(planningObj.second->uID());
    }
}

void Team::saveBasicInfoToArchive(cereal::BinaryOutputArchive & archive)
{
    archive(_teamId);
    archive(_plans.size());
    for(auto plan : _plans)
    {
        plan.second->saveToArchive(archive);
    }
}


void Team::loadFromArchive(cereal::BinaryInputArchive & inputArchive)
{
    loadBasicInfoFromArchive(inputArchive);
    size_t size;
    inputArchive(size);
    for(int i = 0; i < size; i++)
    {
        Command * command = Command::create(this, nullptr);
        _commands.pushBack(command);
    }

    for(auto command : _commands)
    {
        command->loadFromArchive(inputArchive);
    }
    
    ENTITY_ID planId;
    inputArchive(size);
    for(int i = 0; i < size; i++)
    {
        inputArchive(planId);
        PlanningObject * plan = (PlanningObject *) globalEntityManager->getEntity(planId);
        _planningObjects.emplace(planId, plan);
    }
    
}

void Team::loadBasicInfoFromArchive(cereal::BinaryInputArchive & inputArchive)
{
    inputArchive(_teamId);
    size_t size;
    inputArchive(size);
    for(int i = 0; i < size; i++)
    {
        Plan * plan = Plan::create();
        plan->loadFromArchive(inputArchive);
        _plans.insert(plan->uID(), plan);
    }
}
