//
//  JoinServerScene.cpp
//  TrenchWars
//
//  Created by Paul Reed on 11/20/23.
//

#include "JoinServerScene.h"
#include "ClientLobbyManager.h"
#include "ScenarioFactory.h"
#include "TestingScene.h"
#include "TrenchWarsManager.h"


JoinServerScene * JoinServerScene::create(ClientLobbyManager * lobbyManager)
{
    JoinServerScene * lobby = new JoinServerScene();
    lobby->init(lobbyManager);
    return lobby;
}

bool JoinServerScene::init(ClientLobbyManager * lobbyManager)
{
    if ( !Scene::init() )
    {
        return false;
    }
    
    _localPlayer = globalTrenchWarsManager->getLocalPlayer();
    _lobbyManager = lobbyManager;
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    // Create a colored background (Dark Grey)
    LayerColor * backgroundColor = LayerColor::create(Color4B::GRAY, visibleSize.width, visibleSize.height);
    this->addChild(backgroundColor);
    
    
    _addressLabel = Text::create("Ip Address:", "arial.ttf", 20);
    _addressLabel->setAnchorPoint(Vec2::ZERO);
//    _addressLabel->setLayoutParameter(params);
    _addressLabel->setPositionNormalized(Vec2(.5,.5));
    addChild(_addressLabel);
    
    _statusLabel = Text::create("", "arial.ttf", 20);
    _statusLabel->setAnchorPoint(Vec2::ZERO);
//    _statusLabel->setLayoutParameter(params);
    _statusLabel->setPositionNormalized(Vec2(.5,.4));
    addChild(_statusLabel);

    
    _addressField = TextField::create("192.168.0.0", "arial.ttf", 20);
    _addressField->setAnchorPoint(Vec2::ZERO);
    _addressField->setText("127.0.0.1");
//    _addressField->setLayoutParameter(params);
    _addressField->setPositionNormalized(Vec2(.6,.5));
    _addressField->addEventListener( [this] (Ref * sender, TextField::EventType eventType)
    {
        if(eventType == TextField::EventType::ATTACH_WITH_IME)
        {
            _addressField->setHighlighted(true);
            _addressField->setCursorEnabled(true);
        }
        if(eventType == TextField::EventType::DETACH_WITH_IME)
        {
            _addressField->setHighlighted(false);
            _addressField->setInsertText(false);
            _addressField->setCursorEnabled(false);
            _addressField->setEnabled(false);
            _lobbyManager->joinServer(_addressField->getString());
            _statusLabel->setColor(Color3B::BLACK);
            _statusLabel->setText("Connecting...");
        }
        if(eventType == TextField::EventType::INSERT_TEXT)
        {
            if(_addressField->getString().size() > 0)
            {
                std::string text = _addressField->getString();
                //            text.substr()
                char lastChar = text.at(text.size() -1);
                if(!isdigit(lastChar) && lastChar != '.')
                {
                    text = text.substr(0,text.size() -1);
                    _addressField->setString(text);
                }
            }
        }
    });
    addChild(_addressField);
    
    auto backButton = Button::create();
    backButton->setTitleText("Back");
    backButton->setTitleFontName("arial.ttf");
    backButton->setTitleFontSize(20);
    backButton->setPositionNormalized(Vec2(.2,.1));
    backButton->addClickEventListener([this] (Ref * sender) {
        globalTrenchWarsManager->exitToMainMenu();
    });
    addChild(backButton);
    
    return true;
}

void JoinServerScene::displayError(const std::string & error)
{
    _statusLabel->setColor(Color3B::RED);
    _statusLabel->setText(error);
}


void JoinServerScene::editBoxReturn(EditBox* editBox)
{
    //    field->setInsertText(false);
    //    field->setCursorEnabled(false);
    //    field->setEnabled(false);
//    _lobbyManager->joinServer(editBox->getText());
    
}
