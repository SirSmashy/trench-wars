//
//  Plan.hpp
//  TrenchWars
//
//  Created by Paul Reed on 4/6/21.
//

#ifndef Plan_h
#define Plan_h

#include <stdio.h>
#include <map>
#include "Order.h"
#include "Refs.h"
#include "Emblem.h"
#include <cereal/archives/binary.hpp>

class Team;

static ENTITY_ID globalPlanId;

class Plan : public ComparableRef
{
private:
    std::unordered_map<ENTITY_ID, ENTITY_ID> _ordersInPlan;
    std::string _name;
    bool _executing;
    Team * _owningTeam;
    int _owningPlayerId;
    Plan(const std::string & name, int owningPlayerId);
    Plan();
public:
    static Plan * create(const std::string & name, int owningPlayerId);
    static Plan * create();

    void addOrder(Unit * orderEntity, Order * order);
    void orderComplete(Unit * orderEntity);
    void removeOrderForUnit(Unit * entityToOrder);
    const std::unordered_map<ENTITY_ID, ENTITY_ID> & getOrders() {return _ordersInPlan;}
    
    Team * getOwningTeam() {return _owningTeam;}
    int getOwningPlayerId() {return _owningPlayerId;}
    
    void setName(const std::string & name);
    const std::string & getName() {return _name;}
    
    void setExecuting(bool executing) {_executing = executing;}
    bool isExecuting() {return _executing;}
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

#endif /* Plan_h */
