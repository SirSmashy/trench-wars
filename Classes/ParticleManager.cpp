//
//  ParticlesManager.cpp
//  TrenchWars
//
//  Created by Paul Reed on 12/28/22.
//

#include "ParticleManager.h"
#include "World.h"

ParticleManager * globalParticleManager;


ParticleManager::ParticleManager()
{
    globalParticleManager = this;
    _permanentParticlesVisible = true;
}

void ParticleManager::update(float deltaTime)
{
    for(ENTITY_ID id : _particleRemovalRequests)
    {
        removeParticle(id);
    }
    _particleRemovalRequests.clear();
    
    for(ParticleCreationRequest particleRequest : _particleCreationRequests)
    {
        createParticle(particleRequest.filename, particleRequest.particlePosition, particleRequest.requestingEntity, particleRequest.particleHeight, particleRequest.permanentParticle, false);
    }
    _particleCreationRequests.clear();
}

void ParticleManager::setPermanentParticleVisibility(bool visible)
{
    _permanentParticlesVisible = visible;
    for(auto section : world->getWorldSections())
    {
        for(auto batch : section->_permanentParticleBatches)
        {
            batch.second->setVisible(visible);
        }
    }
}

void ParticleManager::requestParticle(const std::string & filename, Vec2 position, ENTITY_ID requestor, float height, bool permanent )
{
    _requestsMutex.lock();
    _particleCreationRequests.push_back( ParticleCreationRequest(filename, position, requestor, height, permanent));
    _requestsMutex.unlock();
}

void ParticleManager::requestParticleRemoval(ENTITY_ID ownerId)
{
    _requestsMutex.lock();
    _particleRemovalRequests.push_back(ownerId);
    _requestsMutex.unlock();
}



ParticleSystem * ParticleManager::createParticle(const std::string & filename, Vec2 position, ENTITY_ID requestor, float height, bool permanent, bool dynamic)
{
    ParticleSystem * particle = nullptr;
    WorldSection * section = world->getWorldSectionForPosition(position);
    if(section->_areaDestroyed)
    {
        return particle;
    }
    
    ParticleBatchGroup * batch;
    
    if(permanent)
    {
        batch = section->_permanentParticleBatches.at(filename);
    }
    else
    {
        batch = section->_particleBatches.at(filename);
    }
    
    if(batch == nullptr)
    {
        batch = ParticleBatchGroup::create(filename, section->_areaBounds.origin);
        batch->setPositionOffset(section->_offset);
        
        if(section->_underground)
        {
            batch->setVisible(world->isUndergroundVisible());
        }
        else
        {
            batch->setVisible(true);
        }
        if(permanent)
        {
            section->_permanentParticleBatches.insert(filename, batch);
        }
        else
        {
            section->_particleBatches.insert(filename, batch);
        }
    }
    particle = batch->addParticle(position, height, dynamic, permanent);
    if(requestor != -1)
    {
        _particleOwnerToParticleMap.insert(requestor, particle);
    }
    return particle;
}

void ParticleManager::removeParticle(ENTITY_ID owner)
{
    ParticleSystem * particle = _particleOwnerToParticleMap.at(owner);
    _particleOwnerToParticleMap.erase(owner);
    if(particle != nullptr)
    {
        particle->stop();
    }
}
