//
//  PatchNeighbor.hpp
//  TrenchWars
//
//  Created by Paul Reed on 3/10/20.
//

#ifndef NavigationPatchNeighbor_h
#define NavigationPatchNeighbor_h

#include "cocos2d.h"
#include "CollisionCell.h"
#include "NavigationPatchRect.h"
#include "Refs.h"

#define CELLS_PER_NODE 3

class NavigationPatch;

class NavigationPatchBorder : public ComparableRef
{
public:
    DIRECTION _direction;
    NavigationPatch * _owningPatch;
    NavigationPatch * _neighboringPatch;
    
private:    
    int _borderIndexMin;
    int _borderIndexMax;
    int _borderIndexPerpendicular;
    
    double _congestionOnBorder [10];
    double _congestionPerPerson;

    
public:
        
    NavigationPatchBorder(int min, int max, int perpendicular, DIRECTION direction, NavigationPatch * owner, NavigationPatch * neighbor);
    ~NavigationPatchBorder();

    CollisionCell * borderCellNearestCoordinates(int x, int y);
    CollisionCell * borderCellNearestCell(CollisionCell * cell);
    CollisionCell * getBorderMinimumCell();
    CollisionCell * getBorderMidpointCell();
    CollisionCell * getBorderMaximumCell();
    CollisionCell * getBorderCellNearestLine(CollisionCell * lineStart, CollisionCell * lineEnd);
    CollisionCell * getBorderCellNearestNeighbor(NavigationPatchBorder * neighbor);
    CollisionCell * getBorderCellAtIndex(int index);
    CollisionCell * getBorderCellWithLeastConjestion(CollisionCell * idealCell, int team);
    
    NavigationPatchBorder * getOppositeBorder();
    bool lineCrossesBorder(CollisionCell * lineStart, CollisionCell * lineEnd);
    bool coordinateOnBorder(int x, int y);
    bool cellOnBorder(CollisionCell * cell);
    bool isOtherBorderParallel(NavigationPatchBorder * otherBorder);
    
    int getBorderSize() {return _borderIndexMax - _borderIndexMin + 1;}
    int getBorderMin() {return _borderIndexMin;}
    int getBorderMax() {return _borderIndexMax;}
    int getBorderPerpendicular() {return _borderIndexPerpendicular;}
    double getBorderDirectionAsAngle();
    
    void addCongestion(int team);
    void removeCongestion(int team);
    double getCongestionForTeam(int team);
    
    void remove();
};

#endif /* NavigationPatchNeighbor_h */
