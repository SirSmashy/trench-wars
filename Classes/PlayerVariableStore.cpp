//
//  PlayerVariableStore.cpp
//  TrenchWars
//
//  Created by Paul Reed on 12/15/22.
//

#include "PlayerVariableStore.h"
#include "ScenarioFactory.h"
#include "StringUtils.h"
#include <stdexcept>

PlayerVariableStore * globalPlayerVariableStore;

PlayerVariableStore::PlayerVariableStore() {
    globalPlayerVariableStore = this;
}

void PlayerVariableStore::parsePlayerVariableFile(const std::string & filename)
{
    std::string fileContents = FileUtils::getInstance()->getStringFromFile(filename);
    if(!fileContents.empty())
    {
        xml_document doc;
        pugi::xml_parse_result result = doc.load(fileContents.c_str());
        if(result.status == xml_parse_status::status_ok)
        {
            bool parsedSuccessfully = doc.traverse(*this);
            if(!parsedSuccessfully)
            {
                CCLOG("parse error!");
                return;
            }
        }
        else
        {
            CCLOG("parse error %s ",result.description());
        }
    }
    else
    {
        CCLOG("PlayerVariableStore: Could not Open File %s",filename.c_str());
    }
}


bool PlayerVariableStore::for_each(xml_node& node)
{
    ci_string name = node.name();
    std::string value = node.first_child().value();
    
    try
    {
        if(name.compare("EmblemColor") == 0)
        {
            Color3B color = parseColor3B(value);
            playerVariables.emplace(EmblemColorR, color.r);
            playerVariables.emplace(EmblemColorG, color.g);
            playerVariables.emplace(EmblemColorB, color.b);
        }
        else if(name.compare("TeamEmblemColor") == 0)
        {
            Color3B color = parseColor3B(value);
            playerVariables.emplace(TeamEmblemColorR, color.r);
            playerVariables.emplace(TeamEmblemColorG, color.g);
            playerVariables.emplace(TeamEmblemColorB, color.b);
        }
        else if(name.compare("ObjectiveColor") == 0)
        {
            Color3B color = parseColor3B(value);
            playerVariables.emplace(ObjectiveColorR, color.r);
            playerVariables.emplace(ObjectiveColorG, color.g);
            playerVariables.emplace(ObjectiveColorB, color.b);
        }
        else if(name.compare("EmblemSize") == 0)
        {
            playerVariables.emplace(EmblemSize, std::stof(value));
        }
    }
    catch (const std::invalid_argument& ia)
    {
        CCLOG("Failure to Parse PlayerVariable. Invalid argument: %s for node %s with value %s ", ia.what(), name.c_str(), value.c_str());
        return false;
    }
    
    return true;
}
