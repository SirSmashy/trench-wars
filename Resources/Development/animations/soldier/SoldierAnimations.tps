<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.5.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d-x</string>
        <key>textureFileName</key>
        <filename>../../../animations/SoldierAnimations.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>1024</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../../animations/SoldierAnimations.plist</filename>
            </struct>
            <key>header</key>
            <key>source</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">dig/soldier_digDown0.png</key>
            <key type="filename">dig/soldier_digDown1.png</key>
            <key type="filename">dig/soldier_digDown2.png</key>
            <key type="filename">dig/soldier_digDownLeft0.png</key>
            <key type="filename">dig/soldier_digDownLeft1.png</key>
            <key type="filename">dig/soldier_digDownLeft2.png</key>
            <key type="filename">dig/soldier_digDownRight0.png</key>
            <key type="filename">dig/soldier_digDownRight1.png</key>
            <key type="filename">dig/soldier_digDownRight2.png</key>
            <key type="filename">dig/soldier_digLeft1.png</key>
            <key type="filename">dig/soldier_digLeft2.png</key>
            <key type="filename">dig/soldier_digLeft3.png</key>
            <key type="filename">dig/soldier_digRight1.png</key>
            <key type="filename">dig/soldier_digRight2.png</key>
            <key type="filename">dig/soldier_digRight3.png</key>
            <key type="filename">dig/soldier_digUp0.png</key>
            <key type="filename">dig/soldier_digUp1.png</key>
            <key type="filename">dig/soldier_digUp2.png</key>
            <key type="filename">dig/soldier_digUpLeft0.png</key>
            <key type="filename">dig/soldier_digUpLeft1.png</key>
            <key type="filename">dig/soldier_digUpLeft2.png</key>
            <key type="filename">dig/soldier_digUpRight0.png</key>
            <key type="filename">dig/soldier_digUpRight1.png</key>
            <key type="filename">dig/soldier_digUpRight2.png</key>
            <key type="filename">move/soldier_moveDown0.png</key>
            <key type="filename">move/soldier_moveDown1.png</key>
            <key type="filename">move/soldier_moveDown2.png</key>
            <key type="filename">move/soldier_moveDownLeft0.png</key>
            <key type="filename">move/soldier_moveDownLeft1.png</key>
            <key type="filename">move/soldier_moveDownLeft2.png</key>
            <key type="filename">move/soldier_moveDownRight0.png</key>
            <key type="filename">move/soldier_moveDownRight1.png</key>
            <key type="filename">move/soldier_moveDownRight2.png</key>
            <key type="filename">move/soldier_moveLeft0.png</key>
            <key type="filename">move/soldier_moveLeft1.png</key>
            <key type="filename">move/soldier_moveLeft2.png</key>
            <key type="filename">move/soldier_moveRight0.png</key>
            <key type="filename">move/soldier_moveRight1.png</key>
            <key type="filename">move/soldier_moveRight2.png</key>
            <key type="filename">move/soldier_moveUp0.png</key>
            <key type="filename">move/soldier_moveUp1.png</key>
            <key type="filename">move/soldier_moveUp2.png</key>
            <key type="filename">move/soldier_moveUpLeft0.png</key>
            <key type="filename">move/soldier_moveUpLeft1.png</key>
            <key type="filename">move/soldier_moveUpLeft2.png</key>
            <key type="filename">move/soldier_moveUpRight0.png</key>
            <key type="filename">move/soldier_moveUpRight1.png</key>
            <key type="filename">move/soldier_moveUpRight2.png</key>
            <key type="filename">prone/soldier_proneLeft0.png</key>
            <key type="filename">soldier.png</key>
            <key type="filename">soldier1.png</key>
            <key type="filename">soldier2.png</key>
            <key type="filename">soldier_dead.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,8,8</rect>
                <key>scale9Paddings</key>
                <rect>4,4,8,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>dig/soldier_digDown0.png</filename>
            <filename>dig/soldier_digDown1.png</filename>
            <filename>dig/soldier_digDown2.png</filename>
            <filename>dig/soldier_digDownLeft0.png</filename>
            <filename>dig/soldier_digDownLeft1.png</filename>
            <filename>dig/soldier_digDownLeft2.png</filename>
            <filename>dig/soldier_digDownRight0.png</filename>
            <filename>dig/soldier_digDownRight1.png</filename>
            <filename>dig/soldier_digDownRight2.png</filename>
            <filename>dig/soldier_digLeft1.png</filename>
            <filename>dig/soldier_digLeft2.png</filename>
            <filename>dig/soldier_digLeft3.png</filename>
            <filename>dig/soldier_digRight1.png</filename>
            <filename>dig/soldier_digRight2.png</filename>
            <filename>dig/soldier_digRight3.png</filename>
            <filename>dig/soldier_digUp0.png</filename>
            <filename>dig/soldier_digUp1.png</filename>
            <filename>dig/soldier_digUp2.png</filename>
            <filename>dig/soldier_digUpLeft0.png</filename>
            <filename>dig/soldier_digUpLeft1.png</filename>
            <filename>dig/soldier_digUpLeft2.png</filename>
            <filename>dig/soldier_digUpRight0.png</filename>
            <filename>dig/soldier_digUpRight1.png</filename>
            <filename>dig/soldier_digUpRight2.png</filename>
            <filename>move/soldier_moveDown0.png</filename>
            <filename>move/soldier_moveDown1.png</filename>
            <filename>move/soldier_moveDown2.png</filename>
            <filename>move/soldier_moveDownLeft0.png</filename>
            <filename>move/soldier_moveDownLeft1.png</filename>
            <filename>move/soldier_moveDownLeft2.png</filename>
            <filename>move/soldier_moveDownRight0.png</filename>
            <filename>move/soldier_moveDownRight1.png</filename>
            <filename>move/soldier_moveDownRight2.png</filename>
            <filename>move/soldier_moveLeft0.png</filename>
            <filename>move/soldier_moveLeft1.png</filename>
            <filename>move/soldier_moveLeft2.png</filename>
            <filename>move/soldier_moveRight0.png</filename>
            <filename>move/soldier_moveRight1.png</filename>
            <filename>move/soldier_moveRight2.png</filename>
            <filename>move/soldier_moveUp0.png</filename>
            <filename>move/soldier_moveUp1.png</filename>
            <filename>move/soldier_moveUp2.png</filename>
            <filename>move/soldier_moveUpLeft0.png</filename>
            <filename>move/soldier_moveUpLeft1.png</filename>
            <filename>move/soldier_moveUpLeft2.png</filename>
            <filename>move/soldier_moveUpRight0.png</filename>
            <filename>move/soldier_moveUpRight1.png</filename>
            <filename>move/soldier_moveUpRight2.png</filename>
            <filename>soldier.png</filename>
            <filename>soldier1.png</filename>
            <filename>soldier2.png</filename>
            <filename>soldier_dead.png</filename>
            <filename>prone/soldier_proneLeft0.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
