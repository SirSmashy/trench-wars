//
//  Team.hpp
//  TrenchWars
//
//  Created by Paul Reed on 12/13/23.
//

#ifndef Team_h
#define Team_h

#include "cocos2d.h"
#include "Refs.h"
#include "CommandPost.h"
#include "Objective.h"
#include "ScenarioFactory.h"
#include "Plan.h"
#include "UnitGroup.h"
#include <cereal/archives/binary.hpp>

class Command;
class CollisionCell;
class GameEventController;
class MapSector;

class Team : public ComparableRef
{
    std::unordered_map<CollisionCell *, ENTITY_ID> _claimedCells;
    std::recursive_mutex _claimedCellsMutex;
    std::unordered_map<ENTITY_ID, double> _sectorVisibleTime;
    std::unordered_set<ENTITY_ID> _sectorsNewlyVisible;
    
    std::unordered_map<ENTITY_ID, PlanningObject *> _planningObjects;
    std::unordered_map<ENTITY_ID, Unit *> _units;
    std::unordered_map<ENTITY_ID, Objective *> _objectives;

    Map<ENTITY_ID, Plan *> _plans;
    Vector<Command *> _commands;

    std::mutex _entitiesMutex;
    std::mutex _visibilityMutex;

    int _teamId;
    
    void init(TeamDefinition * teamDef);
    void init();
    
    void createCommand(CommandDefinition * commandDef);

public:
    
    static Team * create(TeamDefinition * teamDef);
    static Team * create();
    void postLoad();

    virtual void query(float deltaTime);
    int getTeamId() {return _teamId;}
    
    // Commands
    const Vector<Command *> & getCommands() {return _commands;}
    Command * getCommandForId(unsigned int commandId);
    
    // Planning objects
    void addPlanningObject(PlanningObject * object);
    void removePlanningObject(PlanningObject * object);
    
    const std::unordered_map<ENTITY_ID, Objective *> & getObjectives() {return _objectives;}
    
    // Plans
    void addPlan(Plan * plan);
    std::vector<Plan *> getPlans();
    Plan * getPlanWithId(ENTITY_ID planId);
    void removePlan(Plan * plan);
        
    // Position claiming
    bool isPositionClaimed(CollisionCell * cell);
    bool tryToClaimPosition(CollisionCell * cell, ENTITY_ID claimant = -1);
    void releasePositionClaim(CollisionCell * cell);
    void setPositionClaim(CollisionCell * cell, ENTITY_ID claimant = -1);
    
    // Location queries
    Objective * getObjectiveAtPoint(Vec2 point);
    CommandPost * getCommandPostAtPoint(Vec2 point);
    void unitsInRect(Rect rect, Vector<Unit *> & units, bool controlledByPlayerOnly = false);
    
    // CommandPost location queries
    CommandPost * getNearestCommandPost(Vec2 position, bool onlyLinked = true);
    bool isPositionInCommandArea(Vec2 position);
    bool isPositionValidForNewCommandPost(Vec2 position);
    Vec2 getValidCommandPostPositionTowardPosition(Vec2 position);
    void updateCommandPostLinking();

    //
    void markSectorsVisible(std::unordered_set<ENTITY_ID> & sectors);
    bool isSectorVisible(MapSector * sector);
    
    void saveToArchive(cereal::BinaryOutputArchive & archive);
    void saveBasicInfoToArchive(cereal::BinaryOutputArchive & archive);

    void loadFromArchive(cereal::BinaryInputArchive & archive);
    void loadBasicInfoFromArchive(cereal::BinaryInputArchive & archive);

};

#endif /* Team_hpp */
