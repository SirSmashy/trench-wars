//
//  EngagementManager.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/2/20.
//

#ifndef EngagementManager_h
#define EngagementManager_h

#include "cocos2d.h"
#include "Team.h"
#include "GameStateManager.h"
#include "Random.h"
#include "ScenarioFactory.h"
#include "PlayerLayer.h"
#include "Entity.h"
#include "MenuLayer.h"
#include "TrenchWarsManager.h"

#define Z_POSITION_GROUND_LEVEL 0
#define Z_POSITION_UNDERGROUND 0.5 //This is a little weird, but the idea is that the underground, when visible, should display on top of the ground
#define Z_POSITION_PARTICLE_GROUND 0.5
#define Z_POSITION_PARTICLE_TREETOP 1.0
#define Z_POSITION_HISTORICAL_LANDSCAPE 1.5

#define Z_POSITION_command_LAYER 5.0
#define Z_POSITION_MENU_LAYER 6.0

#define SECTOR_SIZE 32
#define QUERY_EVERY_X_FRAME 4



USING_NS_CC;

class EngagementScene;
class CellHighlighter;

enum ENGAGEMENT_STATE
{
    ENGAGEMENT_STATE_LOADING,
    ENGAGEMENT_STATE_PAUSED,
    ENGAGEMENT_STATE_RUNNING,
    ENGAGEMENT_STATE_ENDING
};


class EngagementManager : public GameStateManager
{
protected:
    // Layers
    MenuLayer *   _menuLayer; //layer for the UI
    PlayerLayer * _commandLayer;
        
    std::mutex _mainThreadMutex;
    std::list<std::function<void(void)>> _mainThreadFunctions;
    
    NETWORK_MODE _gameMode;
    EngagementScene * _engagementScene; //reference to the game scene
        
    // EngagementManager tracking variables
    long long _frameCount;
    long long _frameTime;
    double     _engagementTime;
    float     _queryDeltaTime;
    unsigned short _framesSinceQuerying;
    bool       _didPathingLastFrame;
    int _nextQueryIndex;
    std::mutex _queryIndexMutex;
    
    ENGAGEMENT_STATE _engagementState;
    
    EngagementManager();

    void executeMainThreadFunctions();
    void prepareGame();
    
    void act(float deltaTime);
    void updateManagerObjects(float deltaTime);
    
public:
    std::atomic_int _tasksRunning;
    std::atomic_bool _updatingNavigation;
        
    GAME_STATE getState() {return GAME_STATE_IN_ENGAGEMENT;}

    void requestMainThreadFunction(const std::function<void (void)> & function);
    
    long long getFrameCount() {return _frameCount;}
    double getEngagementTime() {return _engagementTime;}
    bool isOneBeforeQueryFrame();

    void togglePause();
    void pauseGame();
    void unPauseGame();
    
    NETWORK_MODE getGameMode(){return _gameMode;}
    ENGAGEMENT_STATE getEngagementState(){return _engagementState;}
    
    void saveGame(const std::string & savePath);
    void serializeGame(std::ostream & outStream);

    void playerLost(Team * losingTeam);
    void shutdown();
    
    Entity * testEntity;    
};

extern EngagementManager * globalEngagementManager;
#endif /* EngagementManager_h */
