#ifndef __CREW_WEAPON_H__
#define __CREW_WEAPON_H__
//
//  CrewWeapon.h
//  TrenchWars
//
//  Created by Paul Reed on 3/15/13.
//  Copyright (c) 2013 . All rights reserved.
//

#include "cocos2d.h"
#include "MovingPhysicalEntity.h"
#include "PhysicalEntity.h"

#include "EntityDefinitions.h"
#include "Weapon.h"
#include "List.h"
#include "Goal.h"
#include "Order.h"


class PathNode;
class CrewWeaponUnit;

class CrewWeapon : virtual public MovingPhysicalEntity, virtual public Weapon
{
protected:
    // FIX Make these private later
    int _crewSize;
    
    Goal * _goal;
    CrewWeaponUnit * _owningUnit;
    BlockingRefChangeTrackingVariable<Goal> _pendingGoal;
    SimpleChangeTrackingVariable<bool> _pendingRemoval;
    AtomicChangeTrackingVariable<float> _pendingWork;

    
    Vec2 _fireOffsets[8];
    
CC_CONSTRUCTOR_ACCESS :
    ~CrewWeapon();
    CrewWeapon();
    virtual bool init(CrewWeaponDefinition * definition, Vec2 position, Command * command, CrewWeaponUnit * unit);
    
    

public:
    static CrewWeapon * create(CrewWeaponDefinition * definition, Vec2 position, Command * command, CrewWeaponUnit * unit);
    static CrewWeapon * create(const std::string & definitionName, Vec2 position, Command * command, CrewWeaponUnit * unit);
    static CrewWeapon * create();
    
    virtual void query(float deltaTime);
    virtual void act(float deltaTime);
        
    virtual ENTITY_TYPE getEntityType() const {return CREW_WEAPON;}
    void setOperator(Infantry * owner);
    Infantry * getOperator() {return _owningInfantry;}
    
    void setGoal(Goal * goal);
    void setGoalFromOrder(Order * order);
    void clearGoal();
    
    CrewWeaponUnit * getOwningUnit() {return _owningUnit;}
    
    virtual bool isCrewWeapon() {return true;}
    virtual int  getPreferenceOrder() {return 999999;} // Crew weapons ALWAYS have a higher preference
    
    virtual Vec2 getWeaponFirePosition();
    virtual Vec2 getWeaponWorkPosition();

    virtual bool isWorkComplete();
    virtual void addWork(float deltaTime);

    virtual void addWorkToWeapon(double deltaTime);
    
    virtual void markForRemoval();

    virtual CollisionCell * getOptimumCellAroundCell(CollisionCell * center);
    virtual void removeFromGame();
    
    virtual int hasUpdateToSendOverNetwork();
    virtual void saveNetworkUpdate(cereal::BinaryOutputArchive & archive);
    virtual void updateFromNetwork(cereal::BinaryInputArchive & archive);

    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
    
    virtual void populateDebugPanel(GameMenu * menu, Vec2 location);
};

#endif //__CREW_WEAPON_H__
