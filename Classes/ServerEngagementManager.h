//
//  ServerEngagementManager.hpp
//  TrenchWars
//
//  Created by Paul Reed on 12/4/23.
//

#ifndef ServerEngagementManager_h
#define ServerEngagementManager_h

#include "EngagementManager.h"

class ServerEventController;

class ServerEngagementManager : public EngagementManager
{
    std::unordered_map<int, long long> _clientToLastFrameReceivedMap;
    std::unordered_map<int, MessageBuffer *> _inProgressUpdates;

    
    ServerEventController * _serverEventController;
    
    virtual void sendClientsFullState();

    
    void sendEntityQueriesToClients();
    void sendTeamUpdateToClients();

    void createEntityAdditionsRemovalPacket();
    void addFrameToEntityAdditionsRemovalPacket();
    void sendClientsEntityAdditionsRemovalPacket();
    
    void parseClientUpdatePackage(GameMessage * message);


    bool areAllClientsReady();
public:
    virtual void update(float deltaTime);
    virtual void parseNetworkMessage(GameMessage * message);
    
    virtual bool initFromSaveGame(const std::string & saveFilePath);
    static ServerEngagementManager * createFromSaveGame(const std::string & saveFilePath);
    
    virtual bool initFromScenario(ScenarioDefinition * scenario, NETWORK_MODE gameMode);
    static ServerEngagementManager * createFromScenario(ScenarioDefinition * scenario, NETWORK_MODE gameMode);

    
    void playerJoinedGame(Player * player);
    void playerLeftGame(int playerId);
};
#endif /* ServerEngagementManager_h */
