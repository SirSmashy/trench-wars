#ifndef __RED_BLACK_TREE_H__
#define __RED_BLACK_TREE_H__

//
//  RedBlackTree.h
//  Trench Wars
//
//  Created by Paul Reed on 1/29/14.
//  Copyright (c) 2014 Paul Reed. All rights reserved.
//

#include "cocos2d.h"
#include "Refs.h"

USING_NS_CC;

class RedBlackNode : public Ref
{
public:
    RefPtr<ComparableRef> data;
    bool red;
    double value;
    
    RedBlackNode * parent;
    RedBlackNode * leftChild;
    RedBlackNode * rightChild;
};

class RedBlackTree
{
protected:
    RedBlackNode *  root;
    RedBlackNode *  minimumValue;
    RedBlackNode *  maximumValue;
    RedBlackNode *  NIL;
    Map<ENTITY_ID, RedBlackNode *> dataToNodeDictionary;
    
    void leftRotation(RedBlackNode * x);
    void rightRotation(RedBlackNode * x);
    void removeNode(RedBlackNode * nodeToRemove);
    RedBlackNode * findNode(ComparableRef * object, double compareValue);
    RedBlackNode * findMinimumNode();
    RedBlackNode * findMaximumNode();
    RedBlackNode * findLessThan(RedBlackNode * node);
    RedBlackNode * findGreaterThan(RedBlackNode * node);

    void lookForCycleRecursive(RedBlackNode * nextNode);

    void walkTreeRecursive(RedBlackNode * nextNode, int level , void (*callOnEachNode)(RedBlackNode *, int level));

public:
    RedBlackTree();
    ~RedBlackTree();

    void addObject(ComparableRef * object, double compareValue);
    void removeObject(ComparableRef * object);
    void clear();
    
    ComparableRef * findAndRemoveMinimum();
    ComparableRef * findMinimum();
    ComparableRef * findMaximum();
    
    ComparableRef * findLessThanObject(ComparableRef * object);
    ComparableRef * findGreaterThanObject(ComparableRef * object);
    void walkTree(void (*callOnEachNode)(RedBlackNode *, int level));
    
    void lookForCycle();
    size_t size() {return dataToNodeDictionary.size();}
};

#endif // __RED_BLACK_TREE_H__
