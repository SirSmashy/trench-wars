//
//  GameEventController.h
//  TrenchWars
//
//  Created by Paul Reed on 1/17/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#include "GameEventController.h"
#include "Command.h"
#include "EngagementManager.h"
#include "Plan.h"
#include "World.h"
#include "TeamManager.h"


GameEventController * globalGameEventController;

void GameEventController::init()
{
    globalGameEventController = this;
}
