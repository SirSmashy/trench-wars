//
//  NavigableRect.cpp
//  TrenchWars
//
//  Created by Paul Reed on 1/9/23.
//

#include "NavigableRect.h"


NavigableRect::NavigableRect() : // initially invalid
_minX(10),
_maxX(0),
_minY(10),
_maxY(0),
_area(-1),
_sqrtArea(-1),
_aspectRatio(-1)
{
}

NavigableRect::NavigableRect(int xMin, int xMax, int yMin, int yMax)  :
_minX(xMin),
_maxX(xMax),
_minY(yMin),
_maxY(yMax),
_area(-1),
_sqrtArea(-1),
_aspectRatio(-1)
{
}


void NavigableRect::set(int xMin, int xMax, int yMin, int yMax)
{
    _minX = xMin;
    _minY= yMin;
    _maxX = xMax;
    _maxY = yMax;
    
    //recompute
    _area = -1;
    _sqrtArea = -1;
    _aspectRatio = -1;
    
}


DIRECTION NavigableRect::getBorderForCoordinates(int x, int y) const
{
    if(x == _minX) {
        if(y == _minY) {
            return DOWN_LEFT;
        } else if(y == _maxY) {
            return UP_LEFT;
        }
        return LEFT;
    } else if(x == _maxX) {
        if(y == _minY) {
            return DOWN_RIGHT;
        } else if(y == _maxY) {
            return UP_RIGHT;
        }
        return RIGHT;
    } else if(y == _minY) {
        return DOWN;
    } else if(y == _maxY) {
        return UP;
    }
    return NONE;
}

DIRECTION NavigableRect::getBorderDirectionForRect(NavigableRect & other) const
{
    bool xOverlap = intersectsRectX(other);
    bool yOverlap = intersectsRectY(other);
    if(!xOverlap && !yOverlap)
    {
        return NONE;
    }
    if(_maxX + 1 == other.getMinX() && yOverlap) { //right
        return RIGHT;
    } else if(_maxY + 1 == other.getMinY() && xOverlap) { //up
        return UP;
    } else if(_minX - 1 == other.getMaxX() && yOverlap) { //left
        return LEFT;
    } else if(_minY - 1 == other.getMaxY() && xOverlap) { //down
        return DOWN;
    }
    return NONE;
}

bool NavigableRect::bordersRect(NavigableRect & other) const
{
    return getBorderDirectionForRect(other) != NONE;
}

bool NavigableRect::intersectsRectX(const NavigableRect & other) const
{
    if(_maxX < other.getMinX() || _minX > other.getMaxX()) {
        return false;
    }
    return true;
}

bool NavigableRect::intersectsRectY(const NavigableRect & other) const
{
    if(_maxY < other.getMinY() || _minY > other.getMaxY()) {
        return false;
    }
    return true;
}

bool NavigableRect::intersectsRect(const NavigableRect & other) const
{
    return intersectsRectX(other) && intersectsRectY(other);
}


bool NavigableRect::borderEqualSize(NavigableRect & other) const
{
    DIRECTION direction = getBorderDirectionForRect(other);
    switch (direction) {
        case LEFT:
        case RIGHT:
            return _minY == other.getMinY() && _maxY == other.getMaxY();
        case UP:
        case DOWN:
            return _minX == other.getMinX() && _maxX == other.getMaxX();
        default:
            return false;
    }
}

bool NavigableRect::isValidRect() const
{
    return _minX <= _maxX && _minY <= _maxY;
}

bool NavigableRect::hasValidAspectRatio()
{
    double x = getXSize();
    double y = getYSize();
    
    if(x > y)
    {
        _aspectRatio  = x / y;
        return (SIZE_TO_ASPECT_RATIO_CONSTANT / x) - _aspectRatio >= 0;
    }
    else
    {
        _aspectRatio  = y / x;
        return (SIZE_TO_ASPECT_RATIO_CONSTANT / y) - _aspectRatio >= 0;
    }
}

double NavigableRect::minimumDistanceToCoordinates(int x, int y) const
{
    int xDiff, yDiff;
    if(x > _maxX) {
        xDiff = x - _maxX;
    }
    else if(x < _minX)
    {
        xDiff = _minX - x;
    }
    else
    {
        xDiff = 0;
    }
    if(y > _maxY) {
        yDiff = y - _maxY;
    }
    else if(y < _minY)
    {
        yDiff = _minY - y;
    }
    else
    {
        yDiff = 0;
    }
    return sqrt( (xDiff * xDiff) + (yDiff * yDiff));
}

bool NavigableRect::containsCoordinates(int x, int y) const
{
    if(x >= _minX && x <= _maxX && y >= _minY && y <= _maxY)
    {
        return true;
    }
    return false;
}

int NavigableRect::getXUnion(NavigableRect & other) const
{
    int unionMinX = std::min(_minX, other.getMinX());
    int unionMaxX = std::max(_maxX, other.getMaxX());
    
    return unionMaxX - unionMinX;
}

int NavigableRect::getYUnion(NavigableRect & other) const
{
    int unionMinY = std::min(_minY, other.getMinY());
    int unionMaxY = std::max(_maxY, other.getMaxY());
    return unionMaxY - unionMinY;
}

bool NavigableRect::operator==(const  NavigableRect & other) const
{
    return _minX == other.getMinX()  && _minY == other.getMinY() && _maxX == other.getMaxX() && _maxY == other.getMaxY();
}

bool NavigableRect::operator!=(const NavigableRect & other) const
{
    return !(*(this) == other);
}

bool NavigableRect::operator<(const NavigableRect & other) const
{
    if(_minX != other.getMinX())
    {
        return _minX < other.getMinX();
    }
    if(_minY != other.getMinY())
    {
        return _minY < other.getMinY();
    }
    if(_maxX != other.getMaxX())
    {
        return _maxX < other.getMaxX();
    }
    if(_maxY != other.getMaxY())
    {
        return _maxY < other.getMaxY();
    }
    return false;
}
