//
//  ServerEngagementManager.cpp
//  TrenchWars
//
//  Created by Paul Reed on 12/4/23.
//

#include <fstream>

#include "ServerEngagementManager.h"

#include "Timing.h"
#include "NavigationGrid.h"
#include "World.h"
#include "MultithreadedAutoReleasePool.h"
#include "GameStatistics.h"
#include "NavigationSystem.h"
#include "EntityManager.h"
#include "PathFinding.h"
#include "SectorGrid.h"
#include "TeamManager.h"
#include "Command.h"
#include "ServerEventController.h"
#include "UIController.h"
#include "BackgroundTaskHandler.h"

static const int CLIENT_FRAMES_BEHIND_LIMIT = QUERY_EVERY_X_FRAME * 3;


bool ServerEngagementManager::initFromScenario(ScenarioDefinition * scenario, NETWORK_MODE gameMode)
{
    LOG_INFO("Init:\n");
    MicroSecondSpeedTimer fullTimer( [=](long long int time) {
        LOG_INFO("Total Time: %.2f \n", time / 1000000.0);
    });
    _engagementState = ENGAGEMENT_STATE_LOADING;
    prepareGame();
    _serverEventController = ServerEventController::create();
    _gameMode = gameMode;
    
    if(globalNetworkInterface != nullptr)
    {
        globalBackgroundTaskHandler->addBackgroundTask([] () {
            globalNetworkInterface->update();
            return true;
        });
    }
    
    Image * map = new Image();
    map->initWithImageFile(scenario->terrainName);

    World::create(map, _engagementScene);
    new PathFinding();
    
    MicroSecondSpeedTimer timer1( [=](long long int time) {
        LOG_INFO("  Post world: %.2f \n", time / 1000000.0);
    });
    //Update the navigation grid, don't allow pathfinding to occur while the grid is being updated
    _updatingNavigation.store(false);
    bool navGridsChanged = globalNavigationSystem->updateNavigationGrids();
    //Wait for all background tasks to complete
    while(globalBackgroundTaskHandler->runNextBackgroundTask()) {}
    while(_updatingNavigation.load() == true) {}
    
    timer1.stop();
    
    MicroSecondSpeedTimer timer2( [=](long long int time) {
        LOG_INFO("  Sector grid create: %.2f \n", time / 1000000.0);
    });
    new SectorGrid( world->getWorldSize(), SECTOR_SIZE);
    new TeamManager(scenario->teams);
    timer2.stop();
    
    MicroSecondSpeedTimer timer3( [=](long long int time) {
        LOG_INFO("  Post Load: %.2f \n", time / 1000000.0);
    });

    if(globalTrenchWarsManager->hasLocalPlayer())
    {
        UIController::create();
    }
    globalTeamManager->postLoad();
    globalGameEventController->postLoad();
    updateManagerObjects(0);
    timer3.stop();

    MicroSecondSpeedTimer timer4( [=](long long int time) {
        LOG_INFO("  Sector grid: %.2f \n", time / 1000000.0);
    });
    globalSectorGrid->generateSectorGridData();
    
    timer4.stop();
    auto playersMap = globalTrenchWarsManager->getPlayerMap();
    for(auto playerPair : playersMap)
    {
        if(playerPair.second->remotePlayer)
        {
            _clientToLastFrameReceivedMap.emplace(playerPair.second->playerId, 0);
        }
    }
    
    if(globalNetworkInterface != nullptr)
    {
        sendClientsFullState();
    }
    _engagementState = ENGAGEMENT_STATE_RUNNING;
    return true;
}

bool ServerEngagementManager::initFromSaveGame(const std::string & saveFilePath)
{
    prepareGame();
    _gameMode = NETWORK_MODE_SINGLE;
    
    std::fstream input;
    input.open (saveFilePath, std::ifstream::in | std::ifstream::binary);
    cereal::BinaryInputArchive iArchive(input); // Create an input archive
    ENTITY_ID maxRefId;
    iArchive(maxRefId);
    ComparableRef::setNextId(maxRefId + 1);
    World::create(iArchive, _engagementScene);
    new PathFinding();
    
    //Update the navigation grid, don't allow pathfinding to occur while the grid is being updated
    _updatingNavigation.store(false);
    bool navGridsChanged = globalNavigationSystem->updateNavigationGrids();
    //Wait for all background tasks to complete
    while(globalBackgroundTaskHandler->runNextBackgroundTask()) {}
    while(_updatingNavigation.load() == true) {}
    
    new SectorGrid( world->getWorldSize(), SECTOR_SIZE);
    new TeamManager(iArchive);
    globalEntityManager->deserializeEntities(iArchive);
    
//    if(globalCamera != nullptr)
//    {
//        globalCamera->loadFromArchive(iArchive);
//    }
    
    input.close();
    executeMainThreadFunctions();
    globalTeamManager->postLoad();
    return true;
}

ServerEngagementManager * ServerEngagementManager::createFromScenario(ScenarioDefinition * scenario, NETWORK_MODE gameMode)
{
    ServerEngagementManager * game = new ServerEngagementManager();
    if(game->initFromScenario(scenario, gameMode))
    {
        game->autorelease();
        return game;
    }
    
    CC_SAFE_DELETE(game);
    return nullptr;
}



ServerEngagementManager * ServerEngagementManager::createFromSaveGame(const std::string & saveFilePath)
{
    ServerEngagementManager * game = new ServerEngagementManager();
    if(game->initFromSaveGame(saveFilePath))
    {
        game->autorelease();
        return game;
    }
    
    CC_SAFE_DELETE(game);
    return nullptr;
}



void ServerEngagementManager::update(float deltaTime)
{
    MicroSecondSpeedTimer timer( [=](long long int time) {
        globalGameStatistics->setTestValue("Frame: ", (double)time/1000.0);
//        LOG_INFO("Frame: %f \n", (double)time/1000.0);

    });
    
    bool clientsReady = areAllClientsReady();
    bool hasNetwork = globalNetworkInterface != nullptr;
    bool queryFrame = _framesSinceQuerying >= (QUERY_EVERY_X_FRAME);
    
    if(_engagementState == ENGAGEMENT_STATE_RUNNING && clientsReady)
    {
        _frameTime = CURRENT_TIME;
        _engagementTime += deltaTime;
        _queryDeltaTime += deltaTime;
        
        ////////////////  QUERY
        /////////////////////////////////////////////
        if(queryFrame)
        {
            _framesSinceQuerying = 1;
            MicroSecondSpeedTimer queryTimer( [=](long long int time) {
                globalGameStatistics->setTestValue("Query: ", (double)time/1000.0);
            });
            
            globalEntityManager->query(_queryDeltaTime);
            globalTeamManager->query(_queryDeltaTime);
            
            while(globalBackgroundTaskHandler->runNextBackgroundTask()) {}
            while(_tasksRunning.load() > 0) {}
            
            if(hasNetwork)
            {
                // At some point the team update and ent queries packets should be combined
                sendTeamUpdateToClients();
                sendEntityQueriesToClients();
                createEntityAdditionsRemovalPacket();
            }
            
            queryTimer.stop();
            _queryDeltaTime = 0;
        }
        else
        {
            _framesSinceQuerying++;
        }
        act(deltaTime);
    }

    if(hasNetwork)
    {
        globalNetworkInterface->drainReceivedQueue();
    }
    updateManagerObjects(deltaTime);
    
    if(_engagementState == ENGAGEMENT_STATE_RUNNING && clientsReady)
    {
        if(hasNetwork)
        {
            addFrameToEntityAdditionsRemovalPacket();
            if(_framesSinceQuerying == QUERY_EVERY_X_FRAME)
            {
                sendClientsEntityAdditionsRemovalPacket();
            }
        }
        _frameCount++;
    }
    
    int running = _tasksRunning.load();
    if(running > 0)
    {
        LOG_DEBUG_ERROR("ARRRG \n");
    }
}


bool ServerEngagementManager::areAllClientsReady()
{
    for(auto player : _clientToLastFrameReceivedMap)
    {
        if((_frameCount - player.second) > (CLIENT_FRAMES_BEHIND_LIMIT))
        {
            return false;
        }
    }
    return true;
}


void ServerEngagementManager::parseNetworkMessage(GameMessage * message)
{
    if(message->getHeader()->messageType == MESSAGE_CLIENT_UPDATE)
    {
        parseClientUpdatePackage(message);
    }
   
}

void ServerEngagementManager::parseClientUpdatePackage(GameMessage * message)
{
    long long frame;
    message->getArchive()(frame);
    _clientToLastFrameReceivedMap.at(message->getPlayerId()) = frame;
    
    Player * player = globalTrenchWarsManager->getPlayerForPlayerId(message->getPlayerId());
    if(player == nullptr)
    {
        LOG_ERROR("Invalid player! \n");
        return;
    }
    
    _serverEventController->parseUpdateFromClient(message->getArchive(), player->teamId, message->getPlayerId());
}


void ServerEngagementManager::sendClientsFullState()
{
    LOG_DEBUG("Sending Full State! \n");
    for(auto team : globalTeamManager->getTeams())
    {
        MessageBuffer * buffer = globalNetworkInterface->prepareNetworkPackage(MESSAGE_FULL_GAME_STATE);
        cereal::BinaryOutputArchive & archive = buffer->getArchive();
        
        world->serializeChanges(archive);
        archive(team->getTeamId());
        team->saveBasicInfoToArchive(archive);
        
        archive(team->getCommands().size());
        for(auto command : team->getCommands())
        {
            archive(command->getCommandID());
            command->saveBasicCommandInfoToArchive(archive);
            command->saveOrdersToArchive(archive);
        }
        globalEntityManager->serializeEntities(archive, false);
        std::list<int> playersToSend;

        for(auto player : globalTrenchWarsManager->getPlayerMap())
        {
            if(player.second->remotePlayer && player.second->teamId == team->getTeamId())
            {
                playersToSend.push_back(player.first);
            }
        }
        globalNetworkInterface->sendPreparedDataToPlayers(buffer, playersToSend);
    }
    
    world->clearChanges();
}

void ServerEngagementManager::sendTeamUpdateToClients()
{
    for(auto team : globalTeamManager->getTeams())
    {
        MessageBuffer * buffer = globalNetworkInterface->prepareNetworkPackage(MESSAGE_TEAM_STATE, _frameCount);
        cereal::BinaryOutputArchive & archive = buffer->getArchive();
        world->serializeChanges(archive);
        
        _serverEventController->buildClientUpdate(archive, team->getTeamId());
        std::list<int> players;
        for(auto player : globalTrenchWarsManager->getPlayerMap())
        {
            if(player.second->remotePlayer && player.second->teamId == team->getTeamId())
            {
                players.push_back(player.first);
            }
        }
        globalNetworkInterface->sendPreparedDataToPlayers(buffer, players);
    }
    world->clearChanges();
}

void ServerEngagementManager::sendEntityQueriesToClients()
{
    MessageBuffer * buffer = globalNetworkInterface->prepareNetworkPackage(MESSAGE_ENTITY_QUERIES, _frameCount);
    cereal::BinaryOutputArchive & archive = buffer->getArchive();
    globalEntityManager->serializeEntityQueryChanges(archive);
    std::list<int> players;
    for(auto player : globalTrenchWarsManager->getPlayerMap())
    {
        if(player.second->remotePlayer)
        {
            players.push_back(player.first);
        }
    }
    globalNetworkInterface->sendPreparedDataToPlayers(buffer, players);
}


void ServerEngagementManager::createEntityAdditionsRemovalPacket()
{
    MessageBuffer * buffer = globalNetworkInterface->prepareNetworkPackage(MESSAGE_ENTITY_ADDITIONS_REMOVALS, _frameCount);
    _inProgressUpdates.emplace(-1,  buffer);
}

void ServerEngagementManager::addFrameToEntityAdditionsRemovalPacket()
{
    for(auto message : _inProgressUpdates)
    {
        auto & archive = message.second->getArchive();
        globalEntityManager->serializeEntityAdditionsAndRemovals(archive);
    }
}


void ServerEngagementManager::sendClientsEntityAdditionsRemovalPacket()
{
    for(auto message : _inProgressUpdates)
    {
        std::list<int> players;
        int team = message.first;
        for(auto player : globalTrenchWarsManager->getPlayerMap())
        {
            if(player.second->remotePlayer) // todo fix make this team based later  && player.second->teamId == team
            {
                players.push_back(player.first);
            }
        }
        globalNetworkInterface->sendPreparedDataToPlayers(message.second, players);
    }
    _inProgressUpdates.clear();
}

void ServerEngagementManager::playerJoinedGame(Player * player)
{
    
}

void ServerEngagementManager::playerLeftGame(int playerId)
{
    
}


