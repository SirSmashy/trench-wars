//
//  CommandPostCreationDrawer.hpp
//  TrenchWars
//
//  Created by Paul Reed on 2/28/21.
//

#ifndef CommandPostCreationDrawer_h
#define CommandPostCreationDrawer_h

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;


class PrimativeDrawer;

class CommandPostCreationDrawer : public DrawNode
{
private:
    CommandPostCreationDrawer();
    ~CommandPostCreationDrawer();
    bool init();
    
public:
    static CommandPostCreationDrawer * create();
    void update();
    void remove();
};


#endif /* CommandPostCreationDrawer_hpp */
