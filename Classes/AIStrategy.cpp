//
//  AIStrategy.cpp
//  TrenchWars
//
//  Created by Paul Reed on 6/27/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#include "AIStrategy.h"
#include "MapSectorAnalysis.h"
#include "EngagementManager.h"
#include "PlanExecution.h"
#include "MultithreadedAutoReleasePool.h"
#include "MapSector.h"

AIStrategy::AIStrategy()
{
    
}

bool AIStrategy::init(AI_GOAL_TYPE type, MapSectorAnalysis * initialSector, AICommand * command, int strategyID)
{
    _strategyID = strategyID;
    _goalType = type;
    _owningCommand = command;
    sectorsInStrategy.pushBack(initialSector);
    requiredUnitStrenth = 0;
    testText = nullptr;
    importance = 0;
    updateStrategyScore();
    return true;
}

AIStrategy * AIStrategy::create(AI_GOAL_TYPE type, MapSectorAnalysis * initialSector, AICommand * command, int strategyID)
{
    AIStrategy * strat = new AIStrategy();
    if(strat->init(type, initialSector, command, strategyID))
    {
        globalAutoReleasePool->addObject(strat);
        return strat;
    }
    CC_SAFE_DELETE(strat);
    return nullptr;
}


float AIStrategy::qualitativeVariableForGoal(AI_GOAL_TYPE goalType, MapSectorAnalysis * analysis)
{
    switch(goalType)
    {
        case BUILD_COMMAND_POST:
            return analysis->_buildCommandPostGoalPotential;
        case FORITY:
            return analysis->_fortifyGoalPotential;
        case OCCUPY:
            return analysis->_defendGoalPotential;
        case ATTACK:
            return analysis->_attackGoalPotential;
        case BOMBARD:
            return analysis->_bombardGoalPotential;
        case RETREAT_FROM:
            return analysis->_retreatFromGoalPotential;
            
    }
    return 0;
}

std::string AIStrategy::stringForGoalType(AI_GOAL_TYPE goalType)
{
    switch(goalType)
    {
        case BUILD_COMMAND_POST:
            return "BUILD COMMAND";
        case FORITY:
            return "FORTIFY";
        case BOMBARD:
            return "BOMBARD";
        case OCCUPY:
            return "OCCUPY";
        case ATTACK:
            return "ATTACK";
        case RETREAT_FROM:
            return "RETREAT_FROM";
            
    }
    return "";
}

double AIStrategy::distanceFromPrincipleSectorInStrategy(MapSector * sector)
{
    MapSectorAnalysis * principleSector = sectorsInStrategy.front();
    
    Vec2 sectorLoc(sector->getXIndex(), sector->getYIndex());
    Vec2 principleLoc(principleSector->_sector->getXIndex(), principleSector->_sector->getYIndex());

    return sectorLoc.distance(principleLoc);
}

bool AIStrategy::tryReplacingPrimarySector(MapSectorAnalysis * sector)
{
    if(_goalType != BUILD_COMMAND_POST)
        return false;
    
    sectorsArrayMutex.lock();
    if(sectorsInStrategy.contains(sector))
    {
        updateStrategyScore();
        sectorsArrayMutex.unlock();
        return false;
    }
    if( distanceFromPrincipleSectorInStrategy(sector->_sector) < 7.0) //FIXME: magic number
    {
        if( AIStrategy::qualitativeVariableForGoal(_goalType, sector) > importance)
        {
            sectorsInStrategy.clear();
            sectorsInStrategy.pushBack(sector);
        }
        sectorsArrayMutex.unlock();
        return true;
    }
    sectorsArrayMutex.unlock();
    return false;
}

/* Examine a sector regarding this strategy.
 * If the sector is in this strategy, then consider removing the sector
 * If the sector is not in the strategy:
 *      If the strategy type is BUILD_COMMAND_POST, consider replacing the sector in this strategy with another, otherwise return DO_NOT_CREATE_NEW (since the command post radius would overlap).
 *      If the strategy type is NOT BUILD_COMMAND_POST, consider adding the sector to this strategy
 */
SECTOR_ACTION AIStrategy::considerSector(MapSectorAnalysis * sector)
{
    sectorsArrayMutex.lock();
    {
        if(sectorsInStrategy.contains(sector))
        {
            if(_goalType != BUILD_COMMAND_POST && qualitativeVariableForGoal(_goalType, sector) < 10 && sectorsInStrategy.size() > 1)
            {
                return REMOVE;
            }
            updateStrategyScore();
            return NOTHING;
        }
        else
        {
            if(_goalType == BUILD_COMMAND_POST)
            {
                if(distanceFromPrincipleSectorInStrategy(sector->_sector) < 7.0)
                {
                    if(qualitativeVariableForGoal(_goalType, sector) > importance)
                    {
                        sectorsArrayMutex.unlock();
                        return REPLACE;
                    }
                    else{
                        sectorsArrayMutex.unlock();
                        return DO_NOT_CREATE_NEW;
                    }
                }
            }
            else
            {
                if(qualitativeVariableForGoal(_goalType, sector) > 10 && distanceFromPrincipleSectorInStrategy(sector->_sector) < 1.5)
                {
                    sectorsArrayMutex.unlock();
                    return ADD;
                }
            }
        }
    }
    sectorsArrayMutex.unlock();

    return NOTHING;
}



void AIStrategy::addSector(MapSectorAnalysis * sector)
{
    sectorsInStrategy.pushBack(sector);
    updateStrategyScore();
}

void AIStrategy::removeSector(MapSectorAnalysis * sector)
{
    sectorsInStrategy.eraseObject(sector);
    updateStrategyScore();
}

void AIStrategy::updateStrategyScore()
{
    //do not update broken strategies, they will be removed by the AICommand
    if(importance < 0)
        return;
    
    importance = 0;
    int risk = 0;
    int oldRequiredStrength = requiredUnitStrenth;
    requiredUnitStrenth = 0;
    MapSectorAnalysis * mainSector = sectorsInStrategy.front();
    
    for(MapSectorAnalysis * sector : sectorsInStrategy)
    {
        importance += qualitativeVariableForGoal(_goalType, sector);
        risk += sector->_danger * 5;
        requiredUnitStrenth++;
    }
    
    if(execution != nullptr)
    {
        execution->updateNumberOfUnitsNeeded(requiredUnitStrenth - oldRequiredStrength);
    }
    importance /= sectorsInStrategy.size();
    risk /= sectorsInStrategy.size();
    
    if(risk > 0)
        CCLOG("    DANGER ZONE! %d %s %d %d",risk, stringForGoalType(_goalType).c_str(), mainSector->_sector->getXIndex(), mainSector->_sector->getYIndex());

    
    importance -= risk;

    if(importance < 1)
        importance = 1;

}

void AIStrategy::deleteStrategy()
{
    if(testText != nullptr)
    {
//        world->removeChild(testText);
        testText = nullptr;
    }
}
