//
//  FormationDefinitionXMLParser.hpp
//  TrenchWars
//
//  Created by Paul Reed on 11/20/22.
//

#ifndef FormationDefinitionXMLParser_h
#define FormationDefinitionXMLParser_h

#include "pugixml/pugixml.hpp"
#include "EntityFactory.h"
#include "cocos2d.h"

using namespace pugi;
USING_NS_CC;


class FormationDefinitionXMLParser :public xml_tree_walker
{
    FormationDefinition * _currentFormationDefinition;
    UnitSpawn * currentUnitSpawn;
    EntityFactory *  _entityFactory;
    
public:
    FormationDefinitionXMLParser(EntityFactory * factory);
    
    void parseFile(std::string const & filename);
    virtual bool for_each(xml_node& node);
};

#endif /* FormationDefinitionXMLParser_hpp */
