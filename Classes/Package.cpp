//
//  Package.cpp
//  TrenchWars
//
//  Created by Paul Reed on 5/3/23.
//

#include "Package.h"
#include "CommandPost.h"
#include "Order.h"
#include "Unit.h"
#include "MultithreadedAutoReleasePool.h"
#include "EntityManager.h"
#include "Command.h"
#include "TeamManager.h"
#include "EngagementManager.h"

OrderPackage::OrderPackage()
{
    
}

OrderPackage::OrderPackage(CommandPost * origin, Unit * destination, Order * orders, Command * owningCommand)
{
    _owningCommand = owningCommand;
    _origin = origin;
    _destination = destination;
    _ordersToDelivery = orders;
}

OrderPackage * OrderPackage::create(CommandPost * origin, Unit * destination, Order * orders, Command * owningCommand)
{
    OrderPackage * package = new OrderPackage(origin,destination,orders, owningCommand);
    globalAutoReleasePool->addObject(package);
    return package;
}

OrderPackage * OrderPackage::create()
{
    OrderPackage * package = new OrderPackage();
    globalAutoReleasePool->addObject(package);
    return package;
}


OrderPackage::~OrderPackage()
{
}

void OrderPackage::deliverPackage()
{
    _destination->setOrder(_ordersToDelivery);
}

Vec2 OrderPackage::getDestination()
{
    return _destination->getAveragePosition();
}

void OrderPackage::packageLost()
{
    if(_ordersToDelivery != nullptr)
    {
        _owningCommand->removeOrder(_ordersToDelivery);
    }
}

void OrderPackage::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    ComparableRef::loadFromArchive(archive);

    int commandId;
    archive(commandId);
    _owningCommand = globalTeamManager->getCommand(commandId);
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        _ordersToDelivery = dynamic_cast<Order *>(_owningCommand->getOrderWithId(thingId));
    }
    archive(thingId);
    if(thingId != -1)
    {
        _destination = dynamic_cast<Unit *>(globalEntityManager->getEntity(thingId));
    }
    archive(thingId);
    if(thingId != -1)
    {
        _origin = dynamic_cast<CommandPost *>(globalEntityManager->getEntity(thingId));
    }
}

void OrderPackage::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    ComparableRef::saveToArchive(archive);
    archive(_owningCommand->getCommandID());
    archive(_ordersToDelivery->uID());
    archive(_destination->uID());
    archive(_origin->uID());
}


AmmoPackage * AmmoPackage::create(CommandPost * origin, CommandPost * destination, int ammo, bool needToRetrieve, Command * owningCommand)
{
    AmmoPackage * package = new AmmoPackage(origin,destination,ammo,needToRetrieve, owningCommand);
    globalAutoReleasePool->addObject(package);
    return package;
}

AmmoPackage * AmmoPackage::create()
{
    AmmoPackage * package = new AmmoPackage();
    globalAutoReleasePool->addObject(package);
    return package;
}

AmmoPackage::AmmoPackage()
{
    
}

AmmoPackage::AmmoPackage(CommandPost * origin, CommandPost * destination, int ammo, bool needToRetrieve, Command * owningCommand)
{
    _owningCommand = owningCommand;
    _origin = origin;
    _destination = destination;
    _ammoAmount = ammo;
    _ammoPickedup = !needToRetrieve;
    _ammoDelivered = false;
}

void AmmoPackage::deliverPackage()
{
    if(_ammoPickedup)
    {
        _destination->depositAmmo(_ammoAmount);
        _ammoDelivered = true;
    }
    else
    {
        int originalAmount = _ammoAmount;
        // Ask the destination CP to send its own courier to deliver ammo
        _origin->requestAmmoForPost(_destination);
        _ammoAmount = _origin->withdrawAmmo(_ammoAmount, true);
        // We might not get the amount requested, update the amount that is promised if so
        if(originalAmount != _ammoAmount)
        {
            _destination->promiseAmmoDeposit(_ammoAmount - originalAmount); //FIXME: instant information travel.... possibly shouldn't do it this way
            
        }
        // Now  turn around and bring the ammo back home
        _ammoPickedup = true;
    }
}

Vec2 AmmoPackage::getDestination()
{
    if(!_ammoPickedup)
    {
        return _origin->getPosition();
    }
    
    return _destination->getPosition();
}

void AmmoPackage::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    ComparableRef::loadFromArchive(archive);

    int commandId;
    archive(commandId);
    _owningCommand = globalTeamManager->getCommand(commandId);

    archive(_ammoAmount);
    archive(_ammoPickedup);
    archive(_ammoDelivered);
    
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        _destination = dynamic_cast<CommandPost *>(globalEntityManager->getEntity(thingId));
    }
    archive(thingId);
    if(thingId != -1)
    {
        _origin = dynamic_cast<CommandPost *>(globalEntityManager->getEntity(thingId));
    }
}

void AmmoPackage::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    ComparableRef::saveToArchive(archive);
    archive(_owningCommand->getCommandID());
    archive(_ammoAmount);
    archive(_ammoPickedup);
    archive(_ammoDelivered);

    archive(_destination->uID());
    archive(_origin->uID());
}
