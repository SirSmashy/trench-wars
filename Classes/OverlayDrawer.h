//
//  OverlayDrawer.hpp
//  TrenchWars
//
//  Created by Paul Reed on 6/18/21.
//

#ifndef OverlayDrawer_h
#define OverlayDrawer_h

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;


class OverlayDrawer : public Ref
{
private:
    
    ClippingNode * _clippingNode;
    DrawNode * _holeDrawer;
    DrawNode * _backgroundDrawer;
    bool _visible;
    
    std::vector<std::vector<Vec2>> _holes;
    
    Color4F _backgroundColor;
    Color4F _holeColor;
    

    OverlayDrawer();
    ~OverlayDrawer();
    virtual bool init(Color4F backgroundColor);
    
    void drawOverlay();
    
    
public:
    static OverlayDrawer * create(Color4F backgroundColor);
    void addHole(std::vector<Vec2> & hole);
    
    void setVisible(bool visible);
    bool isVisible() {return _visible;}
};

#endif /* OverlayDrawer_h */
