//
//  Weapon.m
//  TrenchWars
//
//  Created by Paul Reed on 3/11/12.
//  Copyright (c) 2012 . All rights reserved.
//

#include "Weapon.h"
#include "EntityFactory.h"
#include "Projectile.h"
#include "EngagementManager.h"
#include "EntityManager.h"
#include "VectorMath.h"
#include "Infantry.h"
#include "GameVariableStore.h"
#include "SimpleAudioEngine.h"
#include "ParticleManager.h"
#include "MultithreadedAutoReleasePool.h"
#include <cereal.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/string.hpp>


using namespace CocosDenshion;

#define RANDOM_VALUE_COUNT 5

Weapon::Weapon() :
_pendingWeaponWork(0)
{
    
}



bool Weapon::init(WeaponDefinition * defintion, Infantry * owner)
{
    if(defintion == nullptr) {
        CCLOG("Error: Weapon must have definition! ");
        return false;
    }
    _definition = defintion;
    _owningInfantry = owner;
    _roundsInMagazine = _definition->_magazineSize;
        
    _projectile = _definition->projectile;
    
    if( (_definition->_type == INDIRECT_LOW || _definition->_type == INDIRECT_HIGH)) //_definition->_maximumRange == -1 &&
    {
        _maxRange = VectorMath::computeMaximumWeaponRangeWithSpeed(_projectile->_speed);
    }
    else
    {
        _maxRange = _definition->_maximumRange;
    }
    
    _minRange = _definition->_minimumRange;
    
    _imprecision = _definition->_imprecisionMOA;
    _imprecision = (_imprecision / 60) * D2R;
        
    _weaponState = WEAPON_READY;
    return true;
}

Weapon * Weapon::create(WeaponDefinition * definition, Infantry * owner)
{
    Weapon * newWeapon = new Weapon();
    if(newWeapon->init(definition,owner))
    {
        globalAutoReleasePool->addObject(newWeapon);
        return newWeapon;
    }
    return nullptr;
}

Weapon * Weapon::create()
{
    Weapon * newWeapon = new Weapon();
    globalAutoReleasePool->addObject(newWeapon);
    return newWeapon;
}

void Weapon::updateState()
{
    if(_pendingWeaponWork.isChanged())
    {
        if(_weaponState == WEAPON_RELOADING || _weaponState == WEAPON_UNSTORING)
        {
            _workUntilReady -= _pendingWeaponWork.get();
            if(_workUntilReady <= 0)
            {
                _weaponState = WEAPON_RECHAMBERING; //Either we just unstowed the weapon, or we just reloaded it. Either way it now needs to be chambered
                _timeUntilReady = _definition->_rechamberRate + globalEngagementManager->getEngagementTime();
            }
            _pendingWeaponWork.clearChange();
        }
    }
    updateWeaponChamberState();
}

void Weapon::updateWeaponChamberState()
{
    if (_weaponState == WEAPON_RECHAMBERING && globalEngagementManager->getEngagementTime() > _timeUntilReady)
    {
        _weaponState = WEAPON_READY;
    }
}


void Weapon::updateRandomValues()
{
    while(_randomValues.size() < RANDOM_VALUE_COUNT)
    {
        double rand = globalRandom->randomDouble(0,1.0);
        _weaponNetworkUpdate.randomValuesToSend.push_back(rand);
        _randomValues.push_back(rand);
    }
}

void Weapon::setStored(bool stored)
{
    if(stored)
    {
        _weaponState = WEAPON_STORED;
    }
    else
    {
        _weaponState = WEAPON_UNSTORING;
        _timeUntilReady = (_definition->_readyRate * globalRandom->randomDouble(1.0, 2.0)) + globalEngagementManager->getEngagementTime();
    }
}

Vec2 Weapon::getWeaponWorkPosition()
{
    return _owningInfantry->getPosition();
}

void Weapon::addWorkToWeapon(double deltaTime)
{
    _pendingWeaponWork += deltaTime;
}


double Weapon::adjustFiringAngleForInaccuracy(double firingAngle)
{
    // Rotate the firing angle by a random value based on the weapon's MOA (minute of angle) imprecision
    // This simulates elevation error
    double random = _randomValues.front() * _imprecision;
    _randomValues.pop_front();
    double rotation = (_imprecision / 2) - random;
    firingAngle += rotation;
    return firingAngle;
}


Vec2 Weapon::adjustTargetPositionForInaccuracy(Vec2 targetPosition, Vec2 firePosition)
{
    // Rotate the goal position by a random value based on the weapon's MOA (minute of angle) imprecision
    // This simulates azimuth error
    double random = _randomValues.front() * 2 * _imprecision;
    _randomValues.pop_front();

    Vec2 vectorToGoal = targetPosition - firePosition;
    double rotation = _imprecision - random;
    vectorToGoal.rotate(Vec2(), rotation);
    targetPosition = firePosition + vectorToGoal;
    return targetPosition;
}

void Weapon::fire(double fireAngle, Vec2 position, Vec2 goalPosition, int height)
{
    goalPosition = adjustTargetPositionForInaccuracy(goalPosition, position);
    if(_definition->_type != DIRECT)
    {
        fireAngle = adjustFiringAngleForInaccuracy(fireAngle);
    }

    Projectile::create(_definition->projectile, position, goalPosition, fireAngle, height, _definition->_type, _maxRange, _owningInfantry);
    if(!_definition->_muzzleParticleName.empty())
    {
        globalParticleManager->requestParticle(_definition->_muzzleParticleName, position);
    }
    
    auto audio = SimpleAudioEngine::getInstance();
    //audio->playEffect("rifle.wav", false, globalRandom->randomDouble(0.8,1.2));
    
    _roundsInMagazine--;
    if(_roundsInMagazine <= 0)
    {
        _weaponState = WEAPON_NEEEDS_AMMO;
    }
    else
    {
        _timeUntilReady = _definition->_rechamberRate + globalEngagementManager->getEngagementTime();
        _weaponState = WEAPON_RECHAMBERING;
    }
}

void Weapon::meleeAttackWithTarget(Human * target, double experienceLevel)
{
    target->receiveDamage(_definition->_meleeDamage);
    _weaponState = WEAPON_RECHAMBERING;
    double nextTime = _definition->_rechamberRate;
    double timeDeviation = globalVariableStore->getVariable(ReloadTimeDeviationMultiplier );
    nextTime *= _definition->_reloadRate * globalRandom->randomDouble( 1.0 - timeDeviation, 1.0 + timeDeviation);
    nextTime *= 1.0 - (experienceLevel / globalVariableStore->getVariable(ExperienceDivisor)); // FIX this is stupid
    nextTime += globalEngagementManager->getEngagementTime();

    _timeUntilReady = nextTime;
    _weaponState = WEAPON_RECHAMBERING;
}

void Weapon::addAmmo(int ammo)
{
    _roundsInMagazine += ammo;
    if(_roundsInMagazine > 0)
    {
        double timeDeviation = globalVariableStore->getVariable(ReloadTimeDeviationMultiplier );
        _workUntilReady = _definition->_reloadRate * globalRandom->randomDouble( 1.0 - timeDeviation, 1.0 + timeDeviation); // FIX WTF is this
        _weaponState = WEAPON_RELOADING;
    }
}


float Weapon::getProjectileSpeed()
{
    float speed = 0;
    if(_projectile != nullptr) {
        speed = _projectile->_speed;
    }
    return speed;
}

float Weapon::getMinimumRange()
{
    return _minRange;
}

float Weapon::getMaximumRange()
{
    return _maxRange;
}

int Weapon::getAmmoCost()
{
    return _projectile->_ammoCost;
}

int Weapon::getMagazineSize()
{
    return _definition->_magazineSize;
}

int Weapon::getPreferenceOrder()
{
    return _definition->_preferenceOrder;

}

bool Weapon::isExplosiveProjectile()
{
    return _projectile != nullptr && _projectile->_explosive;
}

int Weapon::getCostToReload()
{
    return _definition->_magazineSize * _projectile->_ammoCost;
}

WEAPON_PROJECTILE_TYPE Weapon::getProjectileType()
{
    return _definition->_type;
}

float Weapon::getRecoil()
{
    return _definition->_recoilPerShot;
}

std::string Weapon::getName()
{
    return _definition != nullptr ? _definition->_entityName : "";
}

int Weapon::checkForNetworkUpdate()
{
    int updates = 0;
    if(_weaponNetworkUpdate.weaponState != _weaponState)
    {
        updates++;
    }
    if(_weaponNetworkUpdate.workUntilReady != _workUntilReady)
    {
        updates++;
    }
    if(_weaponNetworkUpdate.timeUntilReady != _timeUntilReady)
    {
        updates++;
    }
    if(_weaponNetworkUpdate.roundsInMagazine != _roundsInMagazine)
    {
        updates++;
    }
    if(_weaponNetworkUpdate.randomValuesToSend.size() > 0)
    {
        updates++;
    }
    return updates;
}

void Weapon::saveNetworkDate(cereal::BinaryOutputArchive & archive)
{
    unsigned short updates = 0;
    if(_weaponNetworkUpdate.weaponState != _weaponState)
    {
        updates += 1;
    }
    if(_weaponNetworkUpdate.workUntilReady != _workUntilReady)
    {
        updates += 2;
    }
    if(_weaponNetworkUpdate.timeUntilReady != _timeUntilReady)
    {
        updates += 4;
    }
    if(_weaponNetworkUpdate.roundsInMagazine != _roundsInMagazine)
    {
        updates += 8;
    }
    if(_weaponNetworkUpdate.randomValuesToSend.size() > 0)
    {
        updates += 16;
    }
    
    archive(updates);

    if(_weaponNetworkUpdate.weaponState != _weaponState)
    {
        _weaponNetworkUpdate.weaponState = _weaponState;
        archive(_weaponState);
    }
    if(_weaponNetworkUpdate.workUntilReady != _workUntilReady)
    {
        _weaponNetworkUpdate.workUntilReady = _workUntilReady;
        archive(_workUntilReady);
    }
    if(_weaponNetworkUpdate.timeUntilReady != _timeUntilReady)
    {
        _weaponNetworkUpdate.timeUntilReady = _timeUntilReady;
        archive(_timeUntilReady);
    }
    if(_weaponNetworkUpdate.roundsInMagazine != _roundsInMagazine)
    {
        _weaponNetworkUpdate.roundsInMagazine = _roundsInMagazine;
        archive(_roundsInMagazine);
    }
    if(_weaponNetworkUpdate.randomValuesToSend.size() > 0)
    {
        archive(_weaponNetworkUpdate.randomValuesToSend.size());
        for(auto & val : _weaponNetworkUpdate.randomValuesToSend)
        {
            archive(val);
        }
        _weaponNetworkUpdate.randomValuesToSend.clear();
    }
}

void Weapon::updateFromNetworkData(cereal::BinaryInputArchive & archive)
{
    unsigned short updates;
    archive(updates);
    
    if(updates & 1)
    {
        archive(_weaponState);
    }
    if(updates & 2)
    {
        archive(_workUntilReady);
    }
    if(updates & 4)
    {
        archive(_timeUntilReady);
    }
    if(updates & 8)
    {
        archive(_roundsInMagazine);
    }
    if(updates & 16)
    {
        size_t size;
        double randomVal;
        archive(size);
        for(int i = 0; i < size; i++)
        {
            archive(randomVal);
            _randomValues.push_back(randomVal);
        }
    }
}


void Weapon::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    ComparableRef::saveToArchive(archive);
    if(_owningInfantry != nullptr)
    {
        archive(_owningInfantry->uID());
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
    
    archive((int)_weaponState);
    archive(_definition->_hashedName);
    if(_projectile != nullptr)
    {
        archive(_projectile->_hashedName);
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
    
    archive(_maxRange);
    archive(_minRange);
    archive(_imprecision);
    archive(_workUntilReady);
    archive(_timeUntilReady);
    archive(_roundsInMagazine);
    archive(_pendingWeaponWork);

    
    archive(_randomValues.size());
    for(auto val : _randomValues)
    {
        archive(val);
    }
    
}

void Weapon::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    ComparableRef::loadFromArchive(archive);
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        _owningInfantry = dynamic_cast<Infantry *>(globalEntityManager->getEntity(thingId));
    }
    
    int state;
    archive(state);
    _weaponState = (WEAPONSTATE) state;
    
    archive(thingId);
    if(thingId != -1)
    {
        _definition = globalEntityFactory->getWeaponDefinitionWithHashedName(thingId);
    }
    
    archive(thingId);
    if(thingId != -1)
    {
        _projectile = (ProjectileDefinition *) globalEntityFactory->getPhysicalEntityDefinitionWithHashedName(thingId);
    }
    else
    {
        _projectile = nullptr;
    }
    
    archive(_maxRange);
    archive(_minRange);
    archive(_imprecision);
    archive(_workUntilReady);
    archive(_timeUntilReady);
    archive(_roundsInMagazine);
    archive(_pendingWeaponWork);

    _randomValues.clear();
    size_t size;
    double randomVal;
    archive(size);
    for(int i = 0; i < size; i++)
    {
        archive(randomVal);
        _randomValues.push_back(randomVal);
    }
}


void Weapon::populateDebugPanelForWeapon(GameMenu * menu, Vec2 location)
{
    std::ostringstream stream;
    stream << std::fixed;
    stream.precision(0);
    
    std::string test = getName();
    
    stream.str("");
    stream << "Weapon: " << test;
    
    if(_weaponState == WEAPON_READY)
    {
        stream << " (Ready)";
    }
    else if(_weaponState == WEAPON_RELOADING)
    {
        stream << " (Reloading: " <<  _workUntilReady << ")";
    }
    else if(_weaponState == WEAPON_RECHAMBERING)
    {
        stream << " (Rechamber: " <<  (_timeUntilReady - globalEngagementManager->getEngagementTime()) << ")";
    }
    else if(_weaponState == WEAPON_NEEEDS_AMMO)
    {
        stream << " (Need Ammo: " <<  _workUntilReady << ")";
    }
    
    menu->addText(stream.str());
    
    stream.str("");
    stream << "Ammo: " << getRoundsInMagazine();
    menu->addText(stream.str());
}
