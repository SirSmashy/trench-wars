//
//  ParticleSystemProxy.hpp
//  TrenchWars
//
//  Created by Paul Reed on 7/29/24.
//

#ifndef ParticleSystemProxy_h
#define ParticleSystemProxy_h

#include "cocos2d.h"
#include "DynamicParticleSystem.h"
#include "ChangeTrackingVariable.h"
#include "CocosProxy.h"
#include "VectorMath.h"


class ParticleSystemProxy : public CocosProxy
{
    PrimitiveChangeTrackingVariable<Vec2> _pendingPosition;
    SimpleChangeTrackingVariable<int> _pendingHeight;
    SimpleChangeTrackingVariable<float> _pendingAlpha;
    SimpleChangeTrackingVariable<float> _pendingRadius;
    SimpleChangeTrackingVariable<float> _pendingSize;
    SimpleChangeTrackingVariable<ParticleCloudProperties> _pendingCloudSetup;

    DynamicParticleSystem * _particleSystem;
    std::string _particleName;

    ParticleSystemProxy();
    void init(const std::string & particleName, Vec2 position);
    void setNeedsUpdate();
public:
    
    ~ParticleSystemProxy();
    static ParticleSystemProxy * create(const std::string & spriteName, Vec2 position);
    virtual ProxyType getProxyType() {return PARTICLE_SPRITE_PROXY;}
    
    virtual void createCocosObject();
    virtual bool updateCocosObject();
    virtual Sprite * getSprite() {return nullptr;}
    
    void setPosition(Vec2 position);
    void setAlpha(float alpha);
    void setSize(float size);
    void setRadius(float radius);
    void setupParticles(ParticleCloudProperties & properties);
    
    void remove();
};


#endif /* ParticleSystemProxy_h */
