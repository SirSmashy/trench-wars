//
//  UnitGroupEmblem.cpp
//  TrenchWars
//
//  Created by Paul Reed on 1/4/24.
//

#include "UnitGroupEmblem.h"
#include "MultithreadedAutoReleasePool.h"
#include "EmblemManager.h"
#include "MenuLayer.h"
#include "CircularMenuBuilder.h"
#include "EngagementManager.h"
#include "World.h"


#define UNITGROUPICONOFFSET -90


UnitGroupEmblem::~UnitGroupEmblem()
{
}


UnitGroupEmblem::UnitGroupEmblem() : Emblem(), _circleMenuCountdown(10)
{
    
}

bool UnitGroupEmblem::init(UnitGroup * group, EMBLEM_TYPE type, Vec2 position)
{
    if(Emblem::init(group,type, position))
    {
        _group = group;
        _showCircleMenuAfterCount = false;
        _objectiveDraggedOver = nullptr;
        globalEngagementManager->requestMainThreadFunction([this] () {
            if(_emblemType == POSITION_EMBLEM || _emblemType == ORDER_EMBLEM)
            {
                setAnimation(EMBLEM_STATUS_CASUALTY_75, false);
            }
        });
        return true;
    }
    return false;
}

UnitGroupEmblem * UnitGroupEmblem::create(UnitGroup * group, EMBLEM_TYPE type, Vec2 position)
{
    UnitGroupEmblem * newEmblem = new UnitGroupEmblem();
    if(newEmblem->init(group, type, position)) {
        globalAutoReleasePool->addObject(newEmblem);
        
        return newEmblem;
    }
    CC_SAFE_DELETE(newEmblem);
    return nullptr;
}

void UnitGroupEmblem::update(float deltaTime)
{
    Emblem::update(deltaTime);
    if(!_visible)
    {
        return;
    }
    
    if(_showCircleMenuAfterCount && _circleMenuCountdown.count())
    {
        globalEngagementManager->requestMainThreadFunction([this] () {
            CircularMenu * menu = globalMenuLayer->getPrimaryCircularMenu();
            CircularMenuBuilder::populateForEmblem(menu, this, getPosition());
        });
        _showCircleMenuAfterCount = false;
        _circleMenuCountdown.reset();
    }
    
    if(_emblemType == POSITION_EMBLEM)
    {
        Vec2 position = world->getAboveGroundPosition(_group->getPosition());
        position.x += UNITGROUPICONOFFSET;
        setPosition(position);
    }
}


void UnitGroupEmblem::setEmblemType(EMBLEM_TYPE type)
{
    _emblemType = type;
    if(_emblemType == ORDER_EMBLEM)
    {
        _background->setAnimation(EMBLEM_BACKGROUND_DIAMOND, false);
    }
    else
    {
        _background->setAnimation(EMBLEM_BACKGROUND_DIAMOND, false);
    }
}


void UnitGroupEmblem::emblemDragged(Vec2 position)
{
    if(!_canDrag)
    {
        return;
    }
    Emblem::emblemDragged(position);
    
    setFlashing(false);
    CircularMenu * menu = globalMenuLayer->getPrimaryCircularMenu();
    menu->clear(); //
    
    if(_emblemType == POSITION_EMBLEM)
    {
        setEmblemType(ORDER_EMBLEM);
        Emblem * emb = globalEmblemManager->createEmblemForUnitGroup(_group, POSITION_EMBLEM);
        removeAllStatusIcons();
        _icon->setAnimation(EMBLEM_ORDER, false);
        std::vector<Emblem *> emblems = globalEmblemManager->getEmblemsForCommandableObject(_group->uID());
        for(auto emblem : emblems)
        {
            if(emblem != this && (emblem->getEmblemType() == ORDER_EMBLEM))
            {
                globalEmblemManager->removeEmblemForCommandableObject(_ent->uID(), emblem);
            }
        }
    }
    
    // Highlight objectives and what not
}

void UnitGroupEmblem::emblemDragEnded(Vec2 position)
{
    Emblem::emblemDragEnded(position);
    
    CircularMenu * menu = globalMenuLayer->getPrimaryCircularMenu();
    CircularMenuBuilder::populateForEmblem(menu, this, position);
    setFlashing(true);
}

void UnitGroupEmblem::emblemHoverEnter(Vec2 position)
{
    if(_isLocalPlayersEmblem)
    {
        _showCircleMenuAfterCount = true;
        _circleMenuCountdown.reset();
    }
}

void UnitGroupEmblem::emblemHoverLeave(Vec2 position)
{
    _circleMenuCountdown.reset();
    _showCircleMenuAfterCount = false;
    
    CircularMenu * menu = globalMenuLayer->getPrimaryCircularMenu();
    menu->clear(); //
}
