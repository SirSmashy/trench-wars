//
//  AutoSizedListView.cpp
//  TrenchWars
//
//  Created by Paul Reed on 8/8/21.
//

#include "AutoSizedListView.h"

AutoSizedListView::AutoSizedListView() : ListView()
{
    
}

bool AutoSizedListView::init()
{
    ListView::init();
    
    return true;
}

AutoSizedListView * AutoSizedListView::create()
{
    AutoSizedListView * newMenu = new AutoSizedListView();
    if(newMenu->init())
    {
        newMenu->autorelease();
        return newMenu;
    }
    
    CC_SAFE_DELETE(newMenu);
    return nullptr;
}

void AutoSizedListView::addSeperater()
{
    Layout * layout = Layout::create();
    layout->setBackGroundColor(Color3B::WHITE);
    layout->setBackGroundColorType(BackGroundColorType::SOLID);
    layout->setTag(2);
    
    layout->setContentSize(Size(getContentSize().width,2));
//    _widgets.pushBack(layout);
    pushBackCustomItem(layout);
    doLayout();
}



void AutoSizedListView::doLayout()
{
    ListView::doLayout();
    updateContentSize();
}


void AutoSizedListView::updateContentSize()
{
    Rect contentBounds;
    Rect childBounds;
    
    Vector<Widget *> widgetsToSize;
    for(Node * child : getChildren())
    {
        childBounds = child->getBoundingBox();
        
        Widget * widget = dynamic_cast<Widget*>(child);
        if(widget)
        {
            LayoutParameter * params = widget->getLayoutParameter();
            Margin margin = params->getMargin();
            
            childBounds.origin.x -= margin.left;
            childBounds.origin.y -= margin.bottom;
            childBounds.size.width += margin.left + margin.right;
            childBounds.size.height += margin.bottom + margin.top;
            
            if(widget->getTag() == 2)
            {
                widgetsToSize.pushBack(widget);
            }
        }
        
        if(contentBounds.size.width == 0 && contentBounds.size.height == 0)
        {
            contentBounds = childBounds;
        }
        else
        {
            contentBounds.merge(childBounds);
        }
    }
    
    setContentSize(contentBounds.size);
    
    for(auto widget : widgetsToSize)
    {
        Size childSize = widget->getContentSize();
        childSize.width = contentBounds.size.width;
        LayoutParameter * params = widget->getLayoutParameter();
        if(params != nullptr)
        {
            Margin margin = params->getMargin();
            childSize.width -= margin.left + margin.right;
        }
        widget->setContentSize(childSize);
    }
    
}
