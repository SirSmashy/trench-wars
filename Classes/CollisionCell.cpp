//
//  CollisionCell.cpp
//  TrenchWars
//
//  Created by Paul Reed on 2/25/12.
//  Copyright (c) 2012 . All rights reserved.
//

#include "CollisionCell.h"
#include "VectorMath.h"
#include "StaticEntity.h"
#include "EngagementManager.h"
#include "NavigationGrid.h"
#include "CollisionGrid.h"
#include "PolygonLandscapeObject.h"
#include "MapSector.h"

USING_NS_CC;
using namespace std;

CollisionCell * theInvalidCell = nullptr;

/** CollisionCell
 *
 *
 *
 */
CollisionCell::CollisionCell(Rect origin, int xIndex, int yIndex)
{
    _cellOrigin = origin;
    _groundType = DIRT;
    _staticEntityInCell = nullptr;
    _landscapeObjectInCell = nullptr;
    _navigationPatch = nullptr;
    
    _cellIndexX = xIndex;
    _cellIndexY = yIndex;
    
    _concealmentInCell = 0;
    
    for(int i = 0; i < 10; i++)
    {
        _congestion[i] = 0;
    }
}

/** addEntity
 *
 *
 *
 */
void CollisionCell::addMovingPhysicalEntity(MovingPhysicalEntity * ent)
{
    Rect staticEntRect = ent->getCollisionBounds();
    Rect intersection = VectorMath::getRectIntersection(_cellOrigin, staticEntRect);
    float concealment = 50 * (intersection.size.width * intersection.size.height); //percent of cell covered by the ent
    
    _entitiesMutex.lock();
    auto it = _concealmentFromEntityMap.find(ent->uID());
    if(it != _concealmentFromEntityMap.end())
    {
        _concealmentInCell -= it->second;
        _concealmentFromEntityMap.erase(it);
    }

    _concealmentInCell += concealment;
    _concealmentFromEntityMap.insert_or_assign(ent->uID(), concealment);

    _movingEntsInCell.emplace(ent->uID(), ent);
    _entitiesMutex.unlock();
}

/** removeEntity
 *
 *
 *
 */
void CollisionCell::removeMovingPhysicalEntity(MovingPhysicalEntity * ent)
{
    _entitiesMutex.lock();
    _movingEntsInCell.erase(ent->uID());
    auto it = _concealmentFromEntityMap.find(ent->uID());
    if(it != _concealmentFromEntityMap.end())
    {
        _concealmentInCell -= it->second;
        _concealmentFromEntityMap.erase(it);
    }
    _entitiesMutex.unlock();

}

void CollisionCell::setStaticEntity(StaticEntity * object)
{
    if(_staticEntityInCell != nullptr)
    {
        _concealmentInCell -= _concealmentFromEntityMap.at(_staticEntityInCell->uID());
        _concealmentFromEntityMap.erase(_staticEntityInCell->uID());
    }
    _staticEntityInCell = object;
    if(_staticEntityInCell != nullptr)
    {
        float oldSecurity = _terrainCharacteristics.getTotalSecurity();
        _terrainCharacteristics = _staticEntityInCell->getTerrainCharacteristics();
        if(_mapSector != nullptr)
        {
            _mapSector->changeTotalSecurity(_terrainCharacteristics.getTotalSecurity() - oldSecurity);
        }
        
        Rect staticEntRect = _staticEntityInCell->getCollisionBounds();
        Rect intersection = VectorMath::getRectIntersection(_cellOrigin, staticEntRect);
        float concealment = _terrainCharacteristics.getConcealmentValue();
        concealment *= (intersection.size.width * intersection.size.height); //percent of cell covered by the ent
        
        _concealmentInCell += concealment;
        _concealmentFromEntityMap.insert_or_assign(_staticEntityInCell->uID(), concealment);
        
        if(_landscapeObjectInCell != nullptr)
        {
            _concealmentInCell -= _concealmentFromEntityMap.at(_landscapeObjectInCell->uID());
            _concealmentFromEntityMap.erase(_landscapeObjectInCell->uID());
        }
    }
    else if(_landscapeObjectInCell != nullptr)
    {
        float oldSecurity = _terrainCharacteristics.getTotalSecurity();
        _terrainCharacteristics = _landscapeObjectInCell->getTerrainCharacteristics();
        if(_mapSector != nullptr)
        {
            _mapSector->changeTotalSecurity(_terrainCharacteristics.getTotalSecurity() - oldSecurity);
        }
        
        unsigned short concealment = _terrainCharacteristics.getConcealmentValue();
        _concealmentInCell += concealment;
        _concealmentFromEntityMap.insert_or_assign(_landscapeObjectInCell->uID(), concealment);
    }
}

void CollisionCell::setLandscapeObject(PolygonLandscapeObject * entity)
{
    _landscapeObjectInCell = entity;
    if(_staticEntityInCell == nullptr && _landscapeObjectInCell != nullptr)
    {
        float oldSecurity = _terrainCharacteristics.getTotalSecurity();
        _terrainCharacteristics = _landscapeObjectInCell->getTerrainCharacteristics();
        if(_mapSector != nullptr)
        {
            _mapSector->changeTotalSecurity(_terrainCharacteristics.getTotalSecurity() - oldSecurity);
        }
        
        unsigned short concealment = _terrainCharacteristics.getConcealmentValue();
        _concealmentInCell += concealment;
        _concealmentFromEntityMap.insert_or_assign(_landscapeObjectInCell->uID(), concealment);
    }
}

bool CollisionCell::hasTrench()
{
    if(_staticEntityInCell != nullptr)
    {
        return _staticEntityInCell->getTerrainHeight() == -2;
    }
    return false;
}

bool CollisionCell::isTraversable()
{
    return !_terrainCharacteristics.getImpassable();
}

Vec2 CollisionCell::getVectorToOtherCell(CollisionCell * other)
{
    return Vec2(_cellIndexX - other->getXIndex(), _cellIndexY - other->getYIndex());
}


int CollisionCell::numberOfEntsInCellForTeam(int team, bool countForNotInTeam)
{
//    std::lock_guard<recursive_mutex> lock(_entitiesMutex);
    int count = 0;
    MovingPhysicalEntity * ent;
    for(auto iterator = _movingEntsInCell.begin(); iterator != _movingEntsInCell.end(); iterator++)
    {
        ent = iterator->second;
        
        if(ent->getEntityType() == INFANTRY  && ent->_primaryCollisionCell->_cellIndexX == _cellIndexX && ent->_primaryCollisionCell->_cellIndexY == _cellIndexY)
        {
            if(countForNotInTeam && ent->getTeamID() != team)
                count++;
            else if(!countForNotInTeam && ent->getTeamID() == team)
                count++;
        }
    }
    return count;
}


bool CollisionCell::hasCommandPostForTeam(int team)
{
//    std::lock_guard<recursive_mutex> lock(_entitiesMutex);
    MovingPhysicalEntity * ent;
    for(auto iterator = _movingEntsInCell.begin(); iterator != _movingEntsInCell.end(); iterator++)
    {
        ent = iterator->second;
        if(ent->getEntityType() == COMMANDPOST && ent->_primaryCollisionCell->_cellIndexX == _cellIndexX && ent->_primaryCollisionCell->_cellIndexY == _cellIndexY)
        {
            if(ent->getTeamID() == team)
            {
                return true;
            }
        }
    }
    return false;
}

bool CollisionCell::hasCommandPostNotInTeam(int team)
{
    MovingPhysicalEntity * ent;
    for(auto iterator = _movingEntsInCell.begin(); iterator != _movingEntsInCell.end(); iterator++)
    {
        ent = iterator->second;
        if(ent->getEntityType() == COMMANDPOST && ent->_primaryCollisionCell->_cellIndexX == _cellIndexX && ent->_primaryCollisionCell->_cellIndexY == _cellIndexY)
        {
            if(ent->getTeamID() != team)
            {
                return true;
            }
        }
    }
    return false;
}

void CollisionCell::damageEntitiesInCellWithPower(double damage)
{
//    std::lock_guard<recursive_mutex> lock(_entitiesMutex);
    damage -= (globalRandom->randomDouble(0.0, 1.0) * globalRandom->randomDouble(0.0, 1.0) * globalRandom->randomDouble(0.0, 1.0)) * damage;
    
    PhysicalEntity * ent;
    Vector<PhysicalEntity *> entsToDamage;
    
    for(auto iterator = _movingEntsInCell.begin(); iterator != _movingEntsInCell.end(); iterator++)
    {
        ent = iterator->second;
        //only apply damage to an entity if this is their primary collision cell
        if(ent->_primaryCollisionCell->_cellIndexX == _cellIndexX && ent->_primaryCollisionCell->_cellIndexY == _cellIndexY)
        {
            entsToDamage.pushBack(ent);
        }
    }
    
    for(PhysicalEntity * ent : entsToDamage)
    {
        ent->receiveDamage(damage);
    }
    
    if(_staticEntityInCell != nullptr)
    {
        _staticEntityInCell->receiveDamage(damage);
    }
}

/* Collision Detection Functions */

////////////////////////////////////////////////////



/** entityInCell
 *
 *
 *
 */
bool CollisionCell::entityInCell(PhysicalEntity * entity)
{
    if( entity->testCollisionWithBounds(_cellOrigin))
    {
        return true;
    }
    return false;
}

/** pointInCell
 *
 *
 *
 */
bool CollisionCell::pointInCell(Vec2 point)
{
    if(_cellOrigin.containsPoint(point))
        return true;
    return false;
}


/** circleInCell
 *
 *
 *
 */
bool CollisionCell::circleInCell(Vec2 origin, float radius)
{
    if(_cellOrigin.containsPoint(origin))
        return true;
    float xClosest = clamp(origin.x,getCellPosition().x,getCellPosition().x + _cellOrigin.size.width);
    float yClosest = clamp(origin.y,getCellPosition().y,getCellPosition().y + _cellOrigin.size.height);
    
    float distX = origin.x - xClosest;
    float distY = origin.y - yClosest;
    
    if( ((distX * distX) + (distY * distY)) < (radius * radius))
        return true;
    return false;
}


/** lineInCell
 *
 *
 *
 */
bool CollisionCell::lineInCell(Vec2 start, Vec2 end)
{
    if( _cellOrigin.containsPoint(start) || _cellOrigin.containsPoint(end))
        return true;
    
    ////bottom left to bottom right check
    Vec2 boundEdgeStart = getCellPosition();
    Vec2 boundEdgeEnd = getCellPosition();
    boundEdgeEnd.x += _cellOrigin.size.width;
    
    if(Vec2::isSegmentIntersect(start, end, boundEdgeStart, boundEdgeEnd))
        return true;
    
    //bottom right to top right
    boundEdgeStart = boundEdgeEnd;
    boundEdgeEnd.y += _cellOrigin.size.height;
    
    if(Vec2::isSegmentIntersect(start, end, boundEdgeStart, boundEdgeEnd))
        return true;
    
    //top right to top left
    boundEdgeStart = boundEdgeEnd;
    boundEdgeEnd.x = getCellPosition().x;
    
    if(Vec2::isSegmentIntersect(start, end, boundEdgeStart, boundEdgeEnd))
        return true;
    
    //top left to bottom left
    boundEdgeStart = boundEdgeEnd;
    boundEdgeEnd.y = getCellPosition().y;
    
    if(Vec2::isSegmentIntersect(start, end, boundEdgeStart, boundEdgeEnd))
        return true;
    
    return false;
}

/* Entity Collision Detection Functions */

//////////////////////////////////////////

/** findCollisionsWithEnt
 *
 *
 *
 */
void CollisionCell::findCollisionsWithEnt(PhysicalEntity * testEntity, int team, COLLISION_TYPES collisionTypes, std::list<PhysicalEntity *> * collisionsList)
{
    MovingPhysicalEntity * ent;
    for(auto iterator = _movingEntsInCell.begin(); iterator != _movingEntsInCell.end(); iterator++)
    {
        ent = iterator->second;
        //Don't collide with self or with crew weapons
        if(testEntity->uID() == ent->uID() || ent->getEntityType() == CREW_WEAPON)
        {
            continue;
        }
        if( (ent->getTeamID() != team && (collisionTypes & COLLIDE_OWN_TEAM_HUMAN))  ||
           ( ent->getTeamID() == team && (collisionTypes & COLLIDE_OTHER_TEAM_HUMAN) ))
        {
            continue;
        }
        
        if( testEntity->testCollisionWithBounds(ent->getCollisionBounds()))
        {
            collisionsList->push_back(ent);
        }
    }
}

void CollisionCell::setNavigationPatch(NavigationPatch * patch)
{
    if(this == theInvalidCell || patch == nullptr)
    {
        LOG_DEBUG_ERROR("NOOOO!");
    }
    _navigationPatch = patch;
}

NavigationPatch * CollisionCell::getNavigationPatch()
{
    return _navigationPatch;
}

void CollisionCell::addCongestion(int team)
{
    _congestionMutex.lock();
    _congestion[team]++;
    _congestionMutex.unlock();
}

void CollisionCell::removeCongestion(int team)
{
    _congestionMutex.lock();
    _congestion[team]--;
    _congestionMutex.unlock();
}

char CollisionCell::getCongestion(int team)
{
    return _congestion[team];
}

void CollisionCell::shutdown()
{
    _movingEntsInCell.clear();
}
