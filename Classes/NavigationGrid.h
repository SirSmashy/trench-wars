//
//  NavigationGrid.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/13/21.
//

#ifndef NavigationGrid_h
#define NavigationGrid_h

#include <shared_mutex>
#include "cocos2d.h"
#include "NavigationPatch.h"
#include "NavigationPatchBorder.h"
#include "List.h"

class CollisionGrid;

class NavigationGrid
{
    int nextNavigationPatchId;
    Map<ENTITY_ID, NavigationPatch *> _navigationPatches;
    std::unordered_set<CollisionCell *> _splitPoints;
    std::mutex splitPointsMutex;
    CollisionGrid * _collisionGrid;
    
    std::mutex splitsMutex;
    bool _shouldDrawNavigationPatches;
    PrimativeDrawer * _primitiveDrawer;
    
    std::unordered_map<int, std::unordered_map<ENTITY_ID, double>> _teamCongestionMap;
    
    void createInitialPatches(int gridSizeX, int gridSizeY);
    std::list<NavigationPatchRect> getRectsForSplitPoint(NavigationPatch * patchingBeingSplit, int splitX, int splitY, const TerrainCharacteristics & terrain);
    void mergePatches(std::list<NavigationPatch *> patches, std::list<NavigationPatch *> neighbors);
    bool shouldMergePatches(NavigationPatch * patch, NavigationPatch * other, std::list<NavigationPatchRect> & mergedPatches);
    
    NavigationPatchRect getMinMergeRemainder(NavigationPatch * patch, NavigationPatch * other, NavigationPatchRect & combination, DIRECTION border);
    NavigationPatchRect getMaxMergeRemainder(NavigationPatch * patch, NavigationPatch * other, NavigationPatchRect & combination, DIRECTION border);

public:
    NavigationGrid(Size  size, CollisionGrid *  collisionGrid, const std::vector<NavigationPatchRect> & navPatchRects);

    NavigationGrid(Size  size, CollisionGrid *  collisionGrid);
    bool patchContainsPoint(ENTITY_ID patchId, Vec2 position);
    bool patchIdValid(ENTITY_ID patchId);
    
    void markPatchForRemoval(NavigationPatch * patch, bool immediatelyDelete = false);
    
    void addSplitCell(CollisionCell * splitCell);
    void splitNavigationPatches();
    inline bool needsSplitting() {return _splitPoints.size() > 0;}
    void initNewPatches();
    void updatePatchNeighbors();
    void updatePathing();
    
    double getCongestionInNavigationPatch(int team, ENTITY_ID patchId);
    double addCongestionInNavigationPatch(int team, ENTITY_ID patchId);
    double removeCongestionInNavigationPatch(int team, ENTITY_ID patchId);


    void drawNavigationGrid(Vec2 drawNearLocation);
    void toggleShowNavigationPatches();
    
    CollisionGrid * getCollisionGrid() {return _collisionGrid;}    
    void shutdown();
};


#endif /* NavigationGrid_h */
