#ifndef __BACKGROUND_TASK_HANDLER_H__
#define __BACKGROUND_TASK_HANDLER_H__
//
//  BackgroundTaskHandler.h
//  TrenchWars
//
//  Created by Paul Reed on 6/29/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#include "cocos2d.h"
#include <thread>
#include <mutex>
#include "List.h"
#include "Refs.h"
#include "concurrentqueue.h"

USING_NS_CC;

class Entity;
class MapSectorAnalysis;
class AICommand;
class PatchPath;

enum BackgroundTaskType
{
    PATH_REQUEST,
    MAP_SECTOR_ANALYSIS_UPDATE,
    AI_GOAL_UPDATE,
    ENTITY_THINK,
    ENTITY_MOVE
};

class BackgroundTaskRequestor
{
public:
    virtual void backgroundTaskComplete() = 0;
};

class BackgroundTask
{
public:
    BackgroundTaskType _taskType;
    int _frameRequested;
    
CC_CONSTRUCTOR_ACCESS :
    virtual void init(BackgroundTaskType type);
    
    virtual ~BackgroundTask();
    
public:
    virtual bool readyToExecute();
    virtual void executeTask(int theadID, int testID) = 0;
};


////////////////////// Map Sector Analysis Update Request

class MapSectorAnalysisUpdateRequest: public BackgroundTask
{
    MapSectorAnalysis * _analysisToBeUpdated;
    
CC_CONSTRUCTOR_ACCESS :
    MapSectorAnalysisUpdateRequest();
    virtual void init(MapSectorAnalysis * analysis);
public:
    
    static MapSectorAnalysisUpdateRequest * create(MapSectorAnalysis * analysis);
    virtual void executeTask(int theadID, int testID);
    virtual bool readyToExecute();
    bool isEqual(BackgroundTask * otherTask);
};



////////////////////// AI Strategy Update Request

class StrategyUpdateRequest: public BackgroundTask
{
    int _goalType;
    AICommand * _command;
    
CC_CONSTRUCTOR_ACCESS :
    StrategyUpdateRequest();
    virtual void init(int goalType, AICommand * command);
    
public:
    
    static StrategyUpdateRequest * create(int goalType, AICommand * command);
    
    virtual bool readyToExecute();
    virtual void executeTask(int theadID, int testID);
    bool isEqual(BackgroundTask * otherTask);
};

#ifdef WIN32
typedef moodycamel::ConcurrentQueue<std::function<bool(void)>> ConcurrentQueue;
#else
typedef moodycamel::ConcurrentQueue<std::function<const bool(void)>> ConcurrentQueue;
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////BackgroundTaskHandler/////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
class BackgroundTaskHandler
{
private:
    int currentThreadNumber;
    std::vector<std::thread *> threads;
    
    
public:
    ConcurrentQueue backgroundTasks;
    ConcurrentQueue pathFindingTasks;

    
    bool _shouldRun;
    std::atomic_int _debugCounter;

    std::atomic_bool _canRunPathfindingTasks;
    std::atomic_int _pathfindingTasksActive;

    BackgroundTaskHandler(int threadCount);
    ~BackgroundTaskHandler();
    
    int getNextThreadId();
    int getThreadCount() {return threads.size();}
    void addBackgroundTask(const std::function<bool(void)>  & task);
    void addPathfindingTask(const std::function<bool(void)>  & task);
    
    bool runNextBackgroundTask();
    bool runNextPathfindingTask();
    
    size_t getBackgroundTasksSize();

    void shutDown();
    
    static void backgroundTaskLoop(void * taskHandler);
};

extern BackgroundTaskHandler * globalBackgroundTaskHandler;

#endif //__BACKGROUND_TASK_HANDLER_H__
