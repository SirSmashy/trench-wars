//
//  StaticEntity.cpp
//  TrenchWars
//
//  Created by Paul Reed on 4/7/12.
//  Copyright (c) 2012 . All rights reserved.
//
#include "StaticEntity.h"
#include "EntityFactory.h"
#include "EngagementManager.h"
#include "World.h"
#include "CollisionCell.h"
#include "GameStatistics.h"
#include "EntityManager.h"
#include "ParticleManager.h"
#include "MultithreadedAutoReleasePool.h"
#include "CollisionGrid.h"
#include "MapSector.h"
#include "NavigationGrid.h"
#include "SectorGrid.h"
#include "GameMenu.h"

StaticEntity::StaticEntity() : PhysicalEntity(),
_health(0)
{
    
}

bool StaticEntity::init(const std::string & definitionName, Vec2 position)
{
    StaticEntityDefinition * definition = (StaticEntityDefinition * ) globalEntityFactory->getPhysicalEntityDefinitionWithName(definitionName);
    if(definition != nullptr)
    {
        return init(definition, position);
    }
}

bool StaticEntity::init(StaticEntityDefinition * definition, Vec2 position)
{
    _definition = definition;
    if(PhysicalEntity::init(definition,position, false))
    {
        _updateCounter = -1;
        _objectLandscapeType = definition->_hashedName;
        _inTerrain.set(_definition->_height, _definition->_cover, _definition->_hardness,  _definition->_impassable);
        _health = _definition->_health;
        _maxHealth = _health;

        _sprite->setAnimation(_definition->_animationName, false);
        
        setPosition(_position); // todo FIX why do we need this
        getContainingCells();
        for(auto & particle : _definition->_spawnParticles)
        {
            if(particle.particlePerCell)
            {
                for(auto cell : _collisionCellMembership)
                {
                    Vec2 particleLocation = cell.second->getCellPosition();
                    particleLocation.x += cell.second->getCellSize().width / 2;
                    particleLocation.y += cell.second->getCellSize().height / 2;
                    globalParticleManager->requestParticle(particle.name, particleLocation + particle.offset, uID(), particle.height, particle.permanent);
                }
            }
            else
            {
                Vec2 position = _position + particle.offset;
                globalParticleManager->requestParticle(particle.name, position, uID(), particle.height, particle.permanent);
            }
        }
    }
    return true;
}

StaticEntity * StaticEntity::create(StaticEntityDefinition * definition, Vec2 position)
{
    StaticEntity * newLand = new StaticEntity();
    if(newLand && newLand->init(definition, position))
    {
        globalAutoReleasePool->addObject(newLand);
        return newLand;
    }
    CC_SAFE_DELETE(newLand);
    return nullptr;
}

StaticEntity * StaticEntity::create(const std::string & definitionName, Vec2 position)
{
    StaticEntity * newLand = new StaticEntity();
    if(newLand && newLand->init(definitionName, position))
    {
        globalAutoReleasePool->addObject(newLand);
        return newLand;
    }
    CC_SAFE_DELETE(newLand);
    return nullptr;
}

StaticEntity * StaticEntity::create()
{
    StaticEntity * newLand = new StaticEntity();
    if(newLand)
    {
        globalAutoReleasePool->addObject(newLand);
        return newLand;
    }
    CC_SAFE_DELETE(newLand);
    return nullptr;
}


void StaticEntity::postLoad()
{
    PhysicalEntity::postLoad();
    if(_inTerrain.getHeight() < 0)
    {
        _sprite->setUnderground(true);
    }
    std::unordered_set<MapSector *> sectorsToUpdate;

    for(auto pair : _collisionCellMembership)
    {
        CollisionCell * cell = pair.second;
        cell->setStaticEntity(this); //Can't do this while querying
        
        auto sector = cell->getMapSector();
        if(sector != nullptr)
        {
            sectorsToUpdate.insert(sector);
        }
    }
    
    for(auto sector : sectorsToUpdate)
    {
        sector->rebuildNeighborPaths();
    }
    setPosition(_position);
}

void StaticEntity::determineFacingWithCounter(int counter)
{
    if(_collisionCellMembership.size() <= 0 || _updateCounter >= counter)
        return;
    
    _updateCounter = counter;

    CollisionCell * containingCell = _primaryCollisionCell;
    
    CollisionCell * upCell = getCurrentCollisionGrid()->getCell(containingCell->getXIndex(), containingCell->getYIndex() +1);
    CollisionCell * downCell = getCurrentCollisionGrid()->getCell(containingCell->getXIndex(), containingCell->getYIndex() -1);
    CollisionCell * leftCell = getCurrentCollisionGrid()->getCell(containingCell->getXIndex() -1, containingCell->getYIndex());
    CollisionCell * rightCell = getCurrentCollisionGrid()->getCell(containingCell->getXIndex() +1, containingCell->getYIndex());
    
    StaticEntity * upObj = upCell != nullptr ? upCell->getStaticEntity(): nullptr;
    StaticEntity * downObj = downCell != nullptr ? downCell->getStaticEntity(): nullptr;
    StaticEntity * leftObj = leftCell != nullptr ? leftCell->getStaticEntity(): nullptr;
    StaticEntity * rightObj = rightCell != nullptr ? rightCell->getStaticEntity(): nullptr;
    
    bool up = upCell->getTerrainCharacteristics() == _inTerrain;
    bool down = downCell->getTerrainCharacteristics() == _inTerrain;
    bool left = leftCell->getTerrainCharacteristics() == _inTerrain;
    bool right = rightCell->getTerrainCharacteristics() == _inTerrain;
    
    bool updated = false;
    
    DIRECTION direction;
    // All four
    if(up && down && left && right) //16
    {
        direction = ALL;
        updated = true;
    }
    // Three sides
    else if(up && left && right) //15
    {
        direction = LEFT_UP_RIGHT;
        updated = true;
    }
    else if(up && down && left) //14
    {
        direction = DOWN_LEFT_UP;
        updated = true;
    }
    else if(down && left && right) //13
    {
        direction = RIGHT_DOWN_LEFT;
        updated = true;
    }
    else if(up && down && right) //12
    {
        direction = UP_RIGHT_DOWN;
        updated = true;
    }
    // Two Sides
    else if(up && down) //11
    {
        direction = UP_DOWN;
        updated = true;
    }
    else if(left && right) //10
    {
        direction = RIGHT_LEFT;
        updated = true;
    }
    else if(up && left) //9
    {
        direction = UP_LEFT;
        updated = true;
    }
    else if(down && left) //8
    {
        direction = DOWN_LEFT;
        updated = true;
    }
    else if(down && right) //7
    {
        direction = DOWN_RIGHT;
        updated = true;
    }
    else if(up && right) //6
    {
        direction = UP_RIGHT;
        updated = true;
    }
    // One side
    else if(up) //5
    {
        direction = UP;
        updated = true;
    }
    else if(left) //4
    {
        direction = LEFT;
        updated = true;
    }
    else if(down) //3
    {
        direction = DOWN;
        updated = true;
    }
    else if(right) //2
    {
        direction = RIGHT;
        updated = true;
    }
    // None
    else //1
    {
        direction = NONE;
        updated = true;
    }
    
    if(updated)
    {//let the cells around this object update their facing, but don't update the object that updated this object
        _sprite->setAnimation(DEFAULT, false, direction);

        if(right && rightObj->_updateCounter < counter)
        {
            rightObj->determineFacingWithCounter(counter);
        }
        if(down && downObj->_updateCounter < counter)
        {
            downObj->determineFacingWithCounter(counter);
        }
        if(left && leftObj->_updateCounter < counter)
        {
            leftObj->determineFacingWithCounter(counter);
        }
        if(up && upObj->_updateCounter < counter)
        {
            upObj->determineFacingWithCounter(counter);
        }

    }
}


///////// collision handling
bool StaticEntity::testCollisionWithLine(Vec2 start, Vec2 end)
{
    if(_inTerrain.getCoverValue() == 0 ) // No cover meaans no possible collision?
    {
        return PhysicalEntity::testCollisionWithLine(start, end);
    }
    return false;
}

void StaticEntity::receiveDamage(int damage)
{
    // Can't damage invincible static ents
    if(_health == -1)
    {
        return;
    }
    
    _health -= damage;
    if(_health <= 0)
    {
        globalParticleManager->requestParticleRemoval(uID());
        removeFromGame();
        if(!_definition->_spawnOnDeath.empty())
        {
            StaticEntity * newEnt = StaticEntity::create(_definition->_spawnOnDeath, _position);
            newEnt->receiveDamage(- _health); //over-damage
        }
    }
}

void StaticEntity::getContainingCells()
{
    _collisionCellMembership.clear();
    
    Rect bounds = getCollisionBounds();
    std::vector<CollisionCell *> cells;
    getCurrentCollisionGrid()->cellsInRect(bounds, &cells);
    
    double halfCell = 0.5;
    for (CollisionCell * cell : cells)
    {
        _primaryCollisionCell = cell;
        double xMax = std::min( bounds.getMaxX(),cell->getCellRect().getMaxX());
        double xMin = std::max( bounds.getMinX(),cell->getCellRect().getMinX());
        double yMax = std::min( bounds.getMaxY(),cell->getCellRect().getMaxY());
        double yMin = std::max( bounds.getMinY(),cell->getCellRect().getMinY());
        
        // If the cell only slightly overlaps with the landscape object then don't add it
        if((xMax - xMin) < halfCell || (yMax - yMin) < halfCell)
        {
            continue;
        }
        _collisionCellMembership.emplace(cell->uID(), cell);
        world->getNavGrid(cell->getCellPosition())->addSplitCell(cell);
    }
}

void StaticEntity::removeFromGame()
{
    for(auto pair : _collisionCellMembership)
    {
        CollisionCell * cell = pair.second;
        cell->setStaticEntity(nullptr);
    }
    PhysicalEntity::removeFromGame();
}


void StaticEntity::populateDebugPanel(GameMenu * menu, Vec2 location)
{
    PhysicalEntity::populateDebugPanel(menu,location);

    std::ostringstream outStream;
    outStream << std::fixed;
    outStream.precision(0);
    
    outStream.str("");
    outStream << "Health: " << _health;
    menu->addText(outStream.str());
    
    outStream.str("");
    outStream << "Cover: " << _inTerrain.getCoverValue();
    menu->addText(outStream.str());

    outStream.str("");
    outStream << " Hardness: " << _inTerrain.getHardness();
    menu->addText(outStream.str());
    
    outStream.str("");
    outStream << " Height: " << _inTerrain.getHeight();
    menu->addText(outStream.str());
    
    outStream.precision(5);

    outStream.str("");
    outStream << " Z Pos: " << _sprite->getSprite()->getPositionZ();
    menu->addText(outStream.str());
    
    outStream.str("");
    outStream << " Z Ind: " << _sprite->getSprite()->getZOrder();
    menu->addText(outStream.str());
    
}

void StaticEntity::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    PhysicalEntity::saveToArchive(archive);
    archive((int) _objectLandscapeType);
    archive(_updateCounter);
    archive(_health);
}

void StaticEntity::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    PhysicalEntity::loadFromArchive(archive);
    int type;
    archive( type );
    _objectLandscapeType = (LANDSCAPE_OBJECT_TYPE) type;
    
    archive(_updateCounter);
    archive(_health);
    
    _updateCounter = -1;
    _definition = (StaticEntityDefinition *) globalEntityFactory->getPhysicalEntityDefinitionWithHashedName(_hashedEntityName);
    _objectLandscapeType = _definition->_hashedName;
    _inTerrain.set(_definition->_height, _definition->_cover, _definition->_hardness,  _definition->_impassable);
    _maxHealth = _definition->_health;
    
    _sprite->setAnimation(_definition->_animationName, false);
    setPosition(_position);
    getContainingCells();
    
    for(auto & particle : _definition->_spawnParticles)
    {
        if(particle.particlePerCell)
        {
            for(auto cell : _collisionCellMembership)
            {
                Vec2 particleLocation = cell.second->getCellPosition();
                particleLocation.x += cell.second->getCellSize().width / 2;
                particleLocation.y += cell.second->getCellSize().height / 2;
                globalParticleManager->requestParticle(particle.name, particleLocation + particle.offset, uID(), particle.height, particle.permanent);
            }
        }
        else
        {
            globalParticleManager->requestParticle(particle.name, _position + particle.offset, uID(), particle.height, particle.permanent);
        }
    }
}



