//
//  UserInputParser.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/31/20.
//

#include "UserInputParser.h"
#include "RawInputHandler.h"
#include "StringUtils.h"
#include <stdexcept>

UserInputParser::UserInputParser(RawInputHandler * inputHandler)
{
    _inputHandler = inputHandler;
}

void UserInputParser::parseUserInputFile(const std::string & filename)
{
    std::string fileContents = FileUtils::getInstance()->getStringFromFile(filename);
    if(!fileContents.empty())
    {
        xml_document doc;
        pugi::xml_parse_result result = doc.load(fileContents.c_str());
        if(result.status == xml_parse_status::status_ok)
        {
            bool parsedSuccessfully = doc.traverse(*this);
            if(!parsedSuccessfully)
            {
                CCLOG("parse error!");
            }
        }
        else
        {
            CCLOG("parse error %s ",result.description());
        }
    }
    else
    {
        CCLOG("UserInputParser: Could not Open File %s",filename.c_str());
    }
    
}


bool UserInputParser::for_each(xml_node& node)
{
    ci_string name = node.name();
    std::string value = node.first_child().value();
    
    try
    {
        if(name.compare("Action") == 0)
        {
            currentAction = new InputAction();
            auto attribute = node.attribute("name");
            if(attribute != nullptr)
            {
                setAction(attribute.value());
                _inputHandler->addInputAction(currentAction);
            }
            else
            {
                CCLOG("Failure to Input File. Action element is missing name attribute");
            }
            
        }
        else if(name.compare("Input") == 0)
        {
            setInput(value);
        }
    }
    catch (const std::invalid_argument& ia)
    {
        CCLOG("Failure to Parse Input File. Invalid argument: %s for node %s with value %s ", ia.what(), name.c_str(), value.c_str());
        return false;
    }
    return true;
}

void UserInputParser::setAction(const std::string & string)
{
    if(string.compare("panLeft") == 0)
    {
        currentAction->action = PAN_LEFT;
    }
    else if(string.compare("panRight") == 0)
    {
        currentAction->action = PAN_RIGHT;
    }
    else if(string.compare("panUp") == 0)
    {
        currentAction->action = PAN_UP;
    }
    else if(string.compare("panDown") == 0)
    {
        currentAction->action = PAN_DOWN;
    }
    else if(string.compare("panMouse") == 0)
    {
        currentAction->action = PAN_MOUSE;
        currentAction->chordToActivate.mouseDrag = true;
    }
    else if(string.compare("zoomIn") == 0)
    {
        currentAction->action = ZOOM_IN;
    }
    else if(string.compare("zoomOut") == 0)
    {
        currentAction->action = ZOOM_OUT;
    }
    else if(string.compare("zoomMouse") == 0)
    {
        currentAction->action = ZOOM_MOUSE;
    }
    else if(string.compare("cameraReset") == 0)
    {
        currentAction->action = CAMERA_RESET;
    }
    else if(string.compare("selectObject") == 0)
    {
        currentAction->action = SELECT_OBJECT;
    }
    else if(string.compare("addToSelection") == 0)
    {
        currentAction->action = ADD_REMOVE_SELECTION;
    }
    else if(string.compare("dragObject") == 0)
    {
        currentAction->action = DRAG_OBJECT;
        currentAction->chordToActivate.mouseDrag = true;
    }
    else if(string.compare("showContextMenu") == 0)
    {
        currentAction->action = SHOW_CONTEXT_MENU;
    }
    else if(string.compare("drawObjective") == 0)
    {
        currentAction->action = DRAW_OBJECTIVE;
        currentAction->chordToActivate.mouseDrag = true;
    }
    else if(string.compare("marqueeSelect") == 0)
    {
        currentAction->action = MARQUEE_SELECT;
        currentAction->chordToActivate.mouseDrag = true;
    }
    else if(string.compare("debugMenu") == 0)
    {
        currentAction->action = DEBUG_MENU;
    }
    else if(string.compare("toggleInfoScreen") == 0)
    {
        currentAction->action = TOGGLE_INFO;
    }   
    else if(string.compare("togglePause") == 0)
    {
        currentAction->action = TOGGLE_PAUSE;
    }
    else if(string.compare("testClickAction") == 0)
    {
        currentAction->action = TEST_CLICK;
    }
}

void UserInputParser::setInput(const std::string & inputString)
{
    if(inputString.compare("mouseLeft") == 0)
    {
        currentAction->chordToActivate.mouseInput = MOUSE_LEFT;
    }
    else if(inputString.compare("mouseRight") == 0)
    {
        currentAction->chordToActivate.mouseInput = MOUSE_RIGHT;
    }
    else if(inputString.compare("mouseScroll") == 0)
    {
        currentAction->chordToActivate.mouseInput = MOUSE_SCROLL;
    }    
    else if(inputString.compare("TAB") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_TAB);
    }
    else if(inputString.compare("ESC") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_ESCAPE);
    }
    else if(inputString.compare("SHIFT") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_SHIFT);
    }
    else if(inputString.compare("CTRL") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_CTRL);
    }
    else if(inputString.compare("ALT") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_ALT);
    }
    else if(inputString.compare("SPACE") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_SPACE);
    }
    else if(inputString.compare("LEFT_ARROW") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_LEFT_ARROW);
    }
    else if(inputString.compare("RIGHT_ARROW") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_RIGHT_ARROW);
    }
    else if(inputString.compare("UP_ARROW") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_UP_ARROW);
    }
    else if(inputString.compare("DOWN_ARROW") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_DOWN_ARROW);
    }
    else if(inputString.compare("W") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_W);
    }
    else if(inputString.compare("S") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_S);
    }
    else if(inputString.compare("A") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_A);
    }
    else if(inputString.compare("D") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_D);
    }
    else if(inputString.compare("Z") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_Z);
    }
    else if(inputString.compare("X") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_X);
    }
    else if(inputString.compare("C") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_C);
    }
    else if(inputString.compare("V") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_V);
    }
    else if(inputString.compare("Q") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_Q);
    }
    else if(inputString.compare("I") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_I);
    }
    else if(inputString.compare("B") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_B);
    }
    else if(inputString.compare("N") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_N);
    }
    else if(inputString.compare("M") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_M);
    }
    else if(inputString.compare("F") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_F);
    }
    else if(inputString.compare("P") == 0)
    {
        currentAction->chordToActivate.keys.insert(EventKeyboard::KeyCode::KEY_P);
    }
}
