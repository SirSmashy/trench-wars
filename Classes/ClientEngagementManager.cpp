//
//  ClientEngagementManager.cpp
//  TrenchWars
//
//  Created by Paul Reed on 12/4/23.
//

#include "ClientEngagementManager.h"

#include "Timing.h"
#include "NavigationGrid.h"
#include "World.h"
#include "MultithreadedAutoReleasePool.h"
#include "GameStatistics.h"
#include "NavigationSystem.h"
#include "EntityManager.h"
#include "PathFinding.h"
#include "SectorGrid.h"
#include "TeamManager.h"
#include "Command.h"
#include "UIController.h"
#include "ClientEventController.h"
#include "BackgroundTaskHandler.h"

bool ClientEngagementManager::init(const unsigned char * mapData, size_t dataSize, Vector<TeamDefinition *> & teams)
{
    _engagementState = ENGAGEMENT_STATE_LOADING;
    ComparableRef::setNextId(LLONG_MIN + 1); //lol ??

    _state = CLIENT_LOADING_SCENARIO;
    prepareGame();
    _clientEventController = ClientEventController::create();
    
    globalBackgroundTaskHandler->addBackgroundTask([] () {
        globalNetworkInterface->update();
        return true;
    });
    _gameMode = NETWORK_MODE_CLIENT;
    
    Image * map = new Image();
    map->initWithImageData(mapData, dataSize);
    
    World::create(map, _engagementScene, false);
    new PathFinding();
    
    //Update the navigation grid, don't allow pathfinding to occur while the grid is being updated
    _updatingNavigation.store(false);
    bool navGridsChanged = globalNavigationSystem->updateNavigationGrids();
    //Wait for all background tasks to complete
    while(globalBackgroundTaskHandler->runNextBackgroundTask()) {}
    while(_updatingNavigation.load() == true) {}
    new SectorGrid( world->getWorldSize(), SECTOR_SIZE);
    
    new TeamManager(teams);
    UIController::create();

    globalTeamManager->postLoad();
    globalGameEventController->postLoad();
    updateManagerObjects(0);
    globalSectorGrid->generateSectorGridData();

    _waitingForServer = true;    
    _engagementState = ENGAGEMENT_STATE_RUNNING;
    

    return true;
}

ClientEngagementManager * ClientEngagementManager::create(const unsigned char * mapData, size_t dataSize, Vector<TeamDefinition *> & teams)
{
    ClientEngagementManager * game = new ClientEngagementManager();
    if(game->init(mapData,dataSize, teams))
    {
        game->autorelease();
        return game;
    }
    
    CC_SAFE_DELETE(game);
    return nullptr;
}


void ClientEngagementManager::update(float deltaTime)
{
    MicroSecondSpeedTimer timer( [=](long long int time) {
        globalGameStatistics->setTestValue("Frame: ", (double)time/1000.0);
    });
    
    if(_engagementState == ENGAGEMENT_STATE_PAUSED)
    {
        return;
    }
    
    bool isQueryFrame = _framesSinceQuerying >= QUERY_EVERY_X_FRAME;
    GameMessage * entityUpdateMessage = _frameNumberToEntityUpdateMessage.at(_frameCount);
    GameMessage * entityAdditionsMessage = _frameNumberToEntityAdditionsRemovalMessage.at(_frameCount);
    GameMessage * teamUpdateMessage = _frameNumberToTeamUpdateMessage.at(_frameCount);
    
    if( !isQueryFrame || (entityUpdateMessage != nullptr && teamUpdateMessage != nullptr && entityAdditionsMessage != nullptr))
    {
        _frameTime = CURRENT_TIME;
        _engagementTime += deltaTime;
        _queryDeltaTime += deltaTime;
        
        ////////////////  QUERY
        /////////////////////////////////////////////
        if(isQueryFrame)
        {
            MicroSecondSpeedTimer queryTimer( [=](long long int time) {
                globalGameStatistics->setTestValue("Query: ", (double)time/1000.0);
            });
            
            sendClientUpdate();
            
            // ////
            
            // Grab the appropriate Query update and update the entities
            _framesSinceQuerying = 1;
            
            parseEntityAdditionsRemovals(entityAdditionsMessage);
            parseTeamUpdate(teamUpdateMessage);
            parseEntityUpdates(entityUpdateMessage);
            _frameNumberToEntityUpdateMessage.erase(_frameCount);
            _frameNumberToTeamUpdateMessage.erase(_frameCount);
            
            while(globalBackgroundTaskHandler->runNextBackgroundTask()) {}
            while(_tasksRunning.load() > 0) {}
            
            queryTimer.stop();
            _queryDeltaTime = 0;
        }
        else
        {
            _framesSinceQuerying++;
        }
        act(deltaTime);
        _frameCount++;
    }

    CLIENT_STATE previousState = _state;
    globalNetworkInterface->drainReceivedQueue();
    updateManagerObjects(deltaTime);
}

void ClientEngagementManager::parseNetworkMessage(GameMessage * message)
{
    MESSAGE_TYPE messageType = (MESSAGE_TYPE) message->getHeader()->messageType;

    if(messageType == MESSAGE_FULL_GAME_STATE)
    {
        parseFullGameState(message->getArchive());
    }
    else if(messageType == MESSAGE_ENTITY_ADDITIONS_REMOVALS)
    {
        _frameNumberToEntityAdditionsRemovalMessage.insert(message->getHeader()->frameNumber, message);
    }
    else if(messageType == MESSAGE_ENTITY_QUERIES)
    {
        _frameNumberToEntityUpdateMessage.insert(message->getHeader()->frameNumber, message);
    }
    else if(messageType == MESSAGE_TEAM_STATE)
    {
        _frameNumberToTeamUpdateMessage.insert(message->getHeader()->frameNumber, message);
    }
}

void ClientEngagementManager::parseFullGameState(cereal::BinaryInputArchive & iArchive)
{
    LOG_DEBUG("Parsing full state! \n");
    int teamId;
    size_t commandCount;
    
    world->deserializeChanges(iArchive);
    iArchive(teamId);
    Team * team = globalTeamManager->getTeam(teamId);
    if(team == nullptr)
    {
        LOG_ERROR("Unknown team! \n");
    }
    team->loadBasicInfoFromArchive(iArchive);
    iArchive(commandCount);
    
    for(int i = 0; i < commandCount; i++)
    {
        int commandId;
        iArchive(commandId);
        Command * command = globalTeamManager->getCommand(commandId);
        command->loadBasicCommandInfoFromArchive(iArchive);
        command->loadOrdersFromArchive(iArchive);
    }
    globalEntityManager->deserializeEntities(iArchive);
    if(_waitingForServer)
    {
        _waitingForServer = false;
    }
    LOG_DEBUG("Done Parsing full state! \n");
    
    _state = CLIENT_READY;
}

void ClientEngagementManager::parseEntityAdditionsRemovals(GameMessage * message)
{
    auto & archive = message->getArchive();
    globalEntityManager->deserializeEntityAdditionsAndRemovals(archive);
}

void ClientEngagementManager::parseEntityUpdates(GameMessage * message)
{
    auto & archive = message->getArchive();
    globalEntityManager->deserializeEntityQueryChanges(archive);
}


void ClientEngagementManager::parseTeamUpdate(GameMessage * message)
{
    auto & archive = message->getArchive();
    world->deserializeChanges(archive);

    _clientEventController->parseServerUpdate(archive);
}

void ClientEngagementManager::sendClientUpdate()
{
    MessageBuffer * buffer = globalNetworkInterface->prepareNetworkPackage(MESSAGE_CLIENT_UPDATE);
    auto & message = buffer->getArchive();
    message(_frameCount);
    _clientEventController->buildClientUpdatePacket(message);
    globalNetworkInterface->sendPreparedDataToPlayer(buffer);
}


void ClientEngagementManager::playerJoinedGame(Player * player)
{
    
}

void ClientEngagementManager::playerLeftGame(int playerId)
{
    
}
