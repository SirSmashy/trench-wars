//
//  WorldLayer.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/19/24.
//

#include "WorldLayer.h"

WorldLayer::WorldLayer() : Node()
{
    
}


bool WorldLayer::init()
{
    if(Node::init())
    {
        return true;
    }
    return false;
}

WorldLayer * WorldLayer::create()
{
    WorldLayer * layer =  new WorldLayer();
    if(layer->init())
    {
        layer->autorelease();
        return layer;
    }
    layer->release();
    return nullptr;
}


void WorldLayer::visit(Renderer* renderer, const Mat4 &parentTransform, uint32_t parentFlags)
{
    auto director = Director::getInstance();
    auto mat = director->getProjectionMatrix(0);
    
    auto mvpMat = mat * _modelViewTransform;
    Vec3 pos1(0,0,0);
    Vec3 pos2(512,512,0);
    
    mvpMat.transformPoint(&pos1);
    mvpMat.transformPoint(&pos2);

    Node::visit(renderer, parentTransform, parentFlags);
}
