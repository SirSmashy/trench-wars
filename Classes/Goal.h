//
//  Goal.hpp
//  TrenchWars
//
//  Created by Paul Reed on 6/12/24.
//

#ifndef Goal_h
#define Goal_h

#include "cocos2d.h"
#include "Refs.h"
#include "CountTo.h"

enum GOAL_TYPE
{
    MOVE_GOAL,
    LIE_PRONE_GOAL,
    OBJECTIVE_WORK_GOAL,
    ENTITY_WORK_GOAL,
    ATTACK_POSITION_GOAL,
    ATTACK_TARGET_GOAL,
    RELOAD_WEAPON_GOAL,
    STOP_GOAL
};

class ObjectiveObjectCollection;
class PhysicalEntity;
class Objective;
class Human;
class Unit;
class Weapon;

class Goal : public ComparableRef
{
protected:
    
    ENTITY_ID _associatedOrderId;
    double _importance;
    
public:
    Goal();
    virtual ~Goal();
    virtual GOAL_TYPE getGoalType() = 0;
    
    double getImportance() {return _importance;}
    void setImportance(double importance) {_importance = importance;}
    
    void setAssociatedOrderId(ENTITY_ID orderId) {_associatedOrderId = orderId;}
    ENTITY_ID getAssociatedOrderId() {return _associatedOrderId;}
            
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
    virtual void removeFromGame();
    
    bool hasAssociatedOrder() {return _associatedOrderId != -1;}
    
    virtual std::string getGoalTypeAsString();
    static Goal * createGoalOfType(GOAL_TYPE type);
};


class MoveGoal : public Goal
{
protected:
    Vec2  _position;
    bool  _ignoreCover;
    bool _waitingForOptimize;
    bool _needsOptimization;
    
    MoveGoal();
    MoveGoal(Vec2 location, bool ignoreCover);
    ~MoveGoal();
    
public:
    static MoveGoal * create(Vec2 location, bool ignoreCover = false);
    static MoveGoal * create();
    
    virtual GOAL_TYPE getGoalType() {return MOVE_GOAL;}
    
    void setPosition(Vec2 position) {_position = position;}
    virtual Vec2 getPosition() {return _position;}
    
    bool shouldIgnoreCover() {return _ignoreCover;}
    
    bool getWaitingForOptimize() {return _waitingForOptimize;}
    void setWaitingForOptimize(bool waiting) {_waitingForOptimize= waiting;}
    
    bool getNeedsOptimization() {return _needsOptimization;}
    void setNeedsOptimization(bool needs) {_needsOptimization = needs;}

    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

class LieProneGoal : public Goal
{
private:
    LieProneGoal();
public:
    ~LieProneGoal();
    static LieProneGoal * create();
    virtual GOAL_TYPE getGoalType() {return LIE_PRONE_GOAL;}
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};


class WorkGoal : public Goal
{
protected:
    
    WorkGoal();
public:
    ~WorkGoal();
    
    virtual Vec2 getAverageWorkPosition() = 0;
    virtual bool containsEntity(PhysicalEntity * ent) = 0;
    virtual PhysicalEntity * getEntityNearestPosition(Vec2 position) = 0;
    virtual void workCompleteForEntity(PhysicalEntity * ent) = 0;
};

class EntityWorkGoal : public WorkGoal
{
    PhysicalEntity * _workObject;

    EntityWorkGoal(PhysicalEntity * workObject);

public:

    static EntityWorkGoal * create(PhysicalEntity * workObject = nullptr);
    virtual GOAL_TYPE getGoalType() {return ENTITY_WORK_GOAL;}
    Vec2 getAverageWorkPosition();
    virtual bool containsEntity(PhysicalEntity * ent);
    virtual PhysicalEntity * getEntityNearestPosition(Vec2 position);
    virtual void workCompleteForEntity(PhysicalEntity * ent);
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

class ObjectiveWorkGoal : public WorkGoal
{
    ObjectiveObjectCollection * _workObjects;
    ObjectiveWorkGoal(ObjectiveObjectCollection * workObjects);
    
public:
    
    static ObjectiveWorkGoal * create(ObjectiveObjectCollection * workObjects = nullptr);

    virtual GOAL_TYPE getGoalType() {return OBJECTIVE_WORK_GOAL;}

    Vec2 getAverageWorkPosition();
    virtual bool containsEntity(PhysicalEntity * ent);
    virtual PhysicalEntity * getEntityNearestPosition(Vec2 position);
    virtual void workCompleteForEntity(PhysicalEntity * ent);
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};


class ReloadWeaponGoal : public Goal
{
private:
    ReloadWeaponGoal(Weapon * weapon);
    Weapon * _weapon;
public:
    ~ReloadWeaponGoal();
    static ReloadWeaponGoal * create(Weapon * weapon);
    virtual GOAL_TYPE getGoalType() {return RELOAD_WEAPON_GOAL;}
    
    Weapon * getWeapon() {return _weapon;}
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;

};

class AttackPositionGoal : public Goal
{
private:
    Objective * _objective;
    AttackPositionGoal(Objective * objective);
    AttackPositionGoal();
    
public:
    ~AttackPositionGoal();
    static AttackPositionGoal * create(Objective * objective);
    static AttackPositionGoal * create();
    
    virtual GOAL_TYPE getGoalType() {return ATTACK_POSITION_GOAL;}
    
    Vec2 getRandomTargetPoint();
    Objective * getTargetObjective() {return _objective;}
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

class AttackTargetGoal : public Goal
{
private:
    Human * _target;
    
    float _targetAcquireTime;
    int _targetHeight;
    bool _shouldCharge;
    
    CountTo _selectWeaponCount;
    CountTo _checkTargetCount;
    AttackTargetGoal(Human * target);
    AttackTargetGoal();
    
public:
    ~AttackTargetGoal();
    static AttackTargetGoal * create(Human * target);
    static AttackTargetGoal * create();
    
    virtual GOAL_TYPE getGoalType() {return ATTACK_TARGET_GOAL;}    

    void setTarget(Human * target) {_target = target;}
    Human * getTarget() {return _target;}
    
    float getTargetAcquireTime() {return _targetAcquireTime;}
    void setTargetAcquireTime(float time) {_targetAcquireTime = time;}
    void decrementTargetAcquireTime(float timeDelta) 
    {
        _targetAcquireTime -= timeDelta;
        if(_targetAcquireTime < 0)
        {
            _targetAcquireTime = 0;
        }
    }
    
    bool shouldSelectWeapon();
    bool shouldCheckTarget();
    
    bool setCharging() {_shouldCharge = true;}
    bool shouldCharge() {return _shouldCharge;}
    
    int getTargetHeight() {return _targetHeight;}
    void setTargetHeight(int height) {_targetHeight = height;}
        
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};



class StopGoal : public Goal
{
private:
    StopGoal();
public:
    ~StopGoal();
    static StopGoal * create();
    virtual GOAL_TYPE getGoalType() {return STOP_GOAL;}
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

#endif /* Goal_h */
