//
//  GameVariableStore.cpp
//  TrenchWars
//
//  Created by Paul Reed on 7/23/20.
//

#include "GameVariableStore.h"
#include "ScenarioFactory.h"
#include "CollisionGrid.h"
#include "StringUtils.h"
#include <stdexcept>

GlobalVariableStore * globalVariableStore;

GlobalVariableStore::GlobalVariableStore() {
    globalVariableStore = this;
    testValue = false;
}

void GlobalVariableStore::parseGlobalVariableFile(const std::string & filename)
{
    std::string fileContents = FileUtils::getInstance()->getStringFromFile(filename);
    if(!fileContents.empty())
    {
        xml_document doc;
        pugi::xml_parse_result result = doc.load(fileContents.c_str());
        if(result.status == xml_parse_status::status_ok)
        {
            bool parsedSuccessfully = doc.traverse(*this);
            if(!parsedSuccessfully)
            {
                CCLOG("parse error!");
                return;
            }
            
            double exponent = globalVariables[CoverChangeExponent];
            for(int i = 0; i < 3000; i++)
            {
                double value = 20 / ( 1 + std::pow(M_E, -exponent * (i - 500)));
                coverDistanceMultiplier.push_back(value);
            }
            for(int i = 1; i < 3000; i++)
            {
                coverDistanceMultiplier[i] = coverDistanceMultiplier[i] - coverDistanceMultiplier[0] + 1;
            }
            coverDistanceMultiplier[0] = 1.0;

            globalVariables[CoverToConcealmentMaxValue] = pow(100,globalVariables[CoverToConcealmentExponent]);
            
            double coefficient = globalVariables[ProneChanceToHitCoefficient];
            for(int i = 0; i < 3000; i++)
            {
                proneChanceToHitMultiplier.push_back(1 / std::pow(coefficient, i));
            }
        }
        else
        {
            CCLOG("parse error %s ",result.description());
        }
    }
    else
    {
        CCLOG("GlobalVariableStore: Could not Open File %s",filename.c_str());
    }
}


bool GlobalVariableStore::for_each(xml_node& node)
{
    ci_string name = node.name();
    std::string value = node.first_child().value();

    try
    {
        if(name.compare("CommandPostCreationRadius") == 0)
        {
            globalVariables.emplace(CommandPostCreationRadius, std::stod(value));
        }
        else if(name.compare("CommandPostCaptureRadius") == 0)
        {
            globalVariables.emplace(CommandPostCaptureRadius, std::stod(value));
        }
        else if(name.compare("CommandPostDamagePerSecond") == 0)
        {
            globalVariables.emplace(CommandPostDamagePerSecond, std::stod(value));
        }
        else if(name.compare("CommandPostHealPerSecond") == 0)
        {
            globalVariables.emplace(CommandPostHealPerSecond, std::stod(value));
        }
        else if(name.compare("CommandPostDefaultRadius") == 0)
        {
            globalVariables.emplace(CommandPostDefaultRadius, std::stod(value));
        }
        else if(name.compare("CommandPostRecentAmmoDecay") == 0)
        {
            globalVariables.emplace(CommandPostRecentAmmoDecay, std::stod(value));
        }
        else if(name.compare("BunkerSizeX") == 0)
        {
            globalVariables.emplace(BunkerSizeX, std::stod(value));
        }
        else if(name.compare("BunkerSizeY") == 0)
        {
            globalVariables.emplace(BunkerSizeY, std::stod(value));
        }
        else if(name.compare("ExperienceDivisor") == 0)
        {
            globalVariables.emplace(ExperienceDivisor, std::stod(value));
        }
        else if(name.compare("HeightDifferenceCoverValue") == 0)
        {
            globalVariables.emplace(HeightDifferenceCoverValue,std::stof(value));
        }
        else if(name.compare("ProjectileGravity") == 0)
        {
            globalVariables.emplace(ProjectileGravity, std::stod(value));
        }
        else if(name.compare("ProjectileDamageToProjectileCountDivisor") == 0)
        {
            globalVariables.emplace(ProjectileDamageToProjectileCountDivisor, std::stod(value));
        }
        else if(name.compare("ProjectileDamageToCratorSizeDivisor") == 0)
        {
            globalVariables.emplace(ProjectileDamageToCratorSizeDivisor, std::stod(value));
        }
        else if(name.compare("ProjectileSpriteSize") == 0)
        {
            globalVariables.emplace(ProjectileSpriteSize, std::stod(value));
        }
        else if(name.compare("ProneChanceToHitCoefficient") == 0)
        {
            globalVariables.emplace(ProneChanceToHitCoefficient, std::stof(value));
        }
        
        else if(name.compare("BombardmentPlanMaxWait") == 0)
        {
            globalVariables.emplace(BombardmentPlanMaxWait, std::stod(value));
        }
        else if(name.compare("BombardmentPlanMaxWaitDistance") == 0)
        {
            globalVariables.emplace(BombardmentPlanMaxWaitDistance, std::stod(value));
        }
        else if(name.compare("ReloadTimeDeviationMultiplier") == 0)
        {
            globalVariables.emplace(ReloadTimeDeviationMultiplier, std::stod(value));
        }

        else if(name.compare("SecondsBeforeRemovingCorpse") == 0)
        {
            globalVariables.emplace(SecondsBeforeRemovingCorpse, std::stod(value));
        }
        else if(name.compare("ObjectiveVertexLength") == 0)
        {
            globalVariables.emplace(ObjectiveVertexLength, std::stod(value));
        }
        else if(name.compare("ObjectiveClosePolygonDistance") == 0)
        {
            globalVariables.emplace(ObjectiveClosePolygonDistance, std::stod(value));
        }
        else if(name.compare("MinCameraScale") == 0)
        {
            globalVariables.emplace(MinCameraScale, std::stod(value));
        }
        else if(name.compare("MaxCameraScale") == 0)
        {
            globalVariables.emplace(MaxCameraScale, std::stod(value));
        }
        else if(name.compare("MinimumMenuSize") == 0)
        {
            globalVariables.emplace(MinimumMenuSize, std::stod(value));
        }
        else if(name.compare("CoverChangeExponent") == 0)
        {
            globalVariables.emplace(CoverChangeExponent, std::stof(value));
        }
        else if(name.compare("CoverToConcealmentExponent") == 0)
        {
            globalVariables.emplace(CoverToConcealmentExponent, std::stof(value));
        }
        else if(name.compare("CourierAmmoCapacity") == 0)
        {
            globalVariables.emplace(CourierAmmoCapacity, std::stod(value));
        }
        else if(name.compare("CourierSpawnRate") == 0)
        {
            globalVariables.emplace(CourierSpawnRate, std::stod(value));
        }
        else if(name.compare("MaxCommandPostAmmoStockpile") == 0)
        {
            globalVariables.emplace(MaxCommandPostAmmoStockpile, std::stod(value));
        }
        else if(name.compare("BaseTakingFireStressLevel") == 0)
        {
            globalVariables.emplace(BaseTakingFireStressLevel, std::stod(value));
        }
        else if(name.compare("EnemyDistanceToStressExponent") == 0)
        {
            globalVariables.emplace(EnemyDistanceToStressExponent, std::stod(value));
        }
        else if(name.compare("SeekCoverStressLevel") == 0)
        {
            globalVariables.emplace(SeekCoverStressLevel, std::stod(value));
        }
        else if(name.compare("ImmediateCoverStressLevel") == 0)
        {
            globalVariables.emplace(ImmediateCoverStressLevel, std::stod(value));
        }
        else if(name.compare("PanicStressLevel") == 0)
        {
            globalVariables.emplace(PanicStressLevel, std::stod(value));
        }
        else if(name.compare("BaseStressReductionRate") == 0)
        {
            globalVariables.emplace(BaseStressReductionRate, std::stod(value));
        }
        else if(name.compare("PathingCongestionCostMultiplier") == 0)
        {
            globalVariables.emplace(PathingCongestionCostMultiplier, std::stod(value));
        }
        else if(name.compare("PathingHeuristicMultiplier") == 0)
        {
            globalVariables.emplace(PathingHeuristicMultiplier, std::stod(value));
        }
    }
    catch (const std::invalid_argument& ia)
    {
        CCLOG("Failure to Parse Scenario. Invalid argument: %s for node %s with value %s ", ia.what(), name.c_str(), value.c_str());
        return false;
    }
    
    return true;
}
