//
//  EntityDebugDrawer.cpp
//  TrenchWars
//
//  Created by Paul Reed on 7/8/22.
//

#include "EntityDebugDrawer.h"
#include "Unit.h"
#include "Infantry.h"
#include "UnitGroup.h"
#include "PlayerLayer.h"
#include "EmblemManager.h"
#include "EngagementManager.h"
#include "TeamManager.h"

EntityDebugDrawer::EntityDebugDrawer()  : DrawNode()
{
    
}

bool EntityDebugDrawer::init()
{
    if(DrawNode::init()) {
        globalPlayerLayer->addChild(this, 0); //1000
        Director * director = Director::getInstance();
        Scheduler * scheduler = director->getScheduler();
        scheduler->schedule([=] (float dt) { update(dt); }, this, 0.333333333, false, "debugDraw");
        
        _drawTargets = false;
        return true;
    }
    return false;
}

EntityDebugDrawer * EntityDebugDrawer::create()
{
    EntityDebugDrawer * debug = new EntityDebugDrawer();
    if(debug->init())
    {
        debug->autorelease();
        return  debug;
    }
    
    CC_SAFE_DELETE(debug);
    return nullptr;
}

void EntityDebugDrawer::drawTargettingInfo()
{
    Color4F lineColor = Color4F(0.0, 0.0, 1.0, 0.5);
    int width = 4;
    int i = 0;
    for(Team * team : globalTeamManager->getTeams())
    {
        for(Command * command : team->getCommands())
        {
            if(i % 2 == 1)
            {
                lineColor = Color4F(1.0, 0.0, 0.0, 0.5);
            }
            for(auto unit : command->getUnits())
            {
                for(auto ent : *unit)
                {
                    Infantry * infantry = (Infantry *) ent;
                    if(infantry->getActorState() == ALIVE)
                    {
                        Human * target = infantry->getCurrentTarget();
                        if(target != nullptr)
                        {
                            drawLine(infantry->getPosition(), target->getPosition(), lineColor);
                        }
                    }
                }
            }
            i++;
        }
    }
}

void EntityDebugDrawer::update(float deltaTime)
{
    clear();
    if(_drawTargets)
    {
        drawTargettingInfo();
    }
}
