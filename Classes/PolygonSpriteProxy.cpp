//
//  PolygonSpriteProxy.cpp
//  trench-wars
//
//  Created by Paul Reed on 10/3/23.
//

#include "PolygonSpriteProxy.h"
#include "poly2tri/poly2tri.h"
#include "CocosObjectManager.h"
#include "World.h"
#include "CollisionGrid.h"

PolygonSpriteProxy::PolygonSpriteProxy() :
_pendingPosition(Vec2()),
_pendingZIndex(0),
_pendingZPosition(0),
_pendingVisible(true),
_pendingTransform(AffineTransform())
{
}


void PolygonSpriteProxy::init(std::vector<Vec2> & edgeVertices,  const std::string & textureName, Vec2 textureSize, bool boundaryPolygon)
{
    _edgeVertices.reserve(edgeVertices.size());
    for(auto edgeVertex : edgeVertices)
    {
        _edgeVertices.push_back(edgeVertex * WORLD_TO_GRAPHICS_SIZE);
    }
    _textureName = textureName;
    _textureSize = textureSize;
    _boundaryPolygon = boundaryPolygon;
    _needsUpdate = false;
    globalCocosObjectManager->requestCocosObjectCreation(this);
}

PolygonSpriteProxy::~PolygonSpriteProxy()
{
    
}

PolygonSpriteProxy * PolygonSpriteProxy::create(std::vector<Vec2> & edgeVertices, const std::string & textureName, Vec2 textureSize, bool boundaryPolygon)
{
    PolygonSpriteProxy  * newProxy = new PolygonSpriteProxy();
    newProxy->init(edgeVertices, textureName, textureSize, boundaryPolygon);
    return newProxy;
}

void PolygonSpriteProxy::createCocosObject()
{
    Rect bounds;
    bounds.origin.x = 1000000;
    bounds.origin.y = 1000000;
    
    float maxX = -100000;
    float maxY = -100000;
    
    int i = 0;
    for(auto vec : _edgeVertices)
    {
        if(vec.x < bounds.origin.x)
        {
            bounds.origin.x = vec.x;
        }
        if(vec.y < bounds.origin.y)
        {
            bounds.origin.y = vec.y;
        }
        
        if(vec.x > maxX)
        {
            maxX = vec.x;
        }
        if(vec.y > maxY)
        {
            maxY = vec.y;
        }
        i++;
    }
    
    bounds.size.width = maxX - bounds.origin.x;
    bounds.size.height = maxY - bounds.origin.y;
    
    for(int i = 0; i < _edgeVertices.size(); i++)
    {
        _edgeVertices[i].x -= bounds.origin.x;
        _edgeVertices[i].y -= bounds.origin.y;
    }
    
    if(_boundaryPolygon)
    {
        createBoundaryVertices();
    }
    
    auto tri = generateTriangles(_edgeVertices);
    if(_boundaryPolygon)
    {
        calculatePolygonFollowingUV(tri.verts, tri.vertCount);
    }
    else
    {
        calculateStandardUV(bounds, tri.verts, tri.vertCount);
    }
    
    PolygonInfo polygon;
    polygon.setTriangles(tri);
    polygon.setFilename(_textureName);
    
    
    _sprite = Sprite::create(polygon);
    _sprite->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    _sprite->setPosition(bounds.origin);
    _sprite->setContentSize(bounds.size);
    
    Texture2D::TexParams params;
    params.magFilter = params.minFilter = GL_LINEAR;
    params.wrapS = GL_REPEAT;
    params.wrapT = GL_REPEAT;
    _sprite->getTexture()->setTexParameters(params);
    _sprite->retain();
    world->addChild(_sprite);
    _sprite->setPositionZ(-1);
}


TrianglesCommand::Triangles PolygonSpriteProxy::generateTriangles(const std::vector<Vec2>& points)
{
    // if there are less than 3 points, then we can't triangulate
    if(points.size()<3)
    {
        LOG_ERROR("AUTOPOLYGON: cannot triangulate with less than 3 points \n");
        return TrianglesCommand::Triangles();
    }
    std::vector<p2t::Point*> p2points;
    for(const auto& pt : points)
    {
        p2t::Point * p = new (std::nothrow) p2t::Point(pt.x, pt.y);
        p2points.push_back(p);
    }
    p2t::CDT cdt(p2points);
    cdt.Triangulate();
    std::vector<p2t::Triangle*> tris = cdt.GetTriangles();
    
    // we won't know the size of verts and indices until we process all of the triangles!
    std::vector<V3F_C4B_T2F> verts;
    std::vector<unsigned short> indices;
    
    unsigned short idx = 0;
    unsigned short vdx = 0;
    
    for(const auto& ite : tris)
    {
        for(int i = 0; i < 3; ++i)
        {
            auto p = ite->GetPoint(i);
            auto v3 = Vec3(p->x, p->y, 0);
            bool found = false;
            size_t j;
            size_t length = vdx;
            for(j = 0; j < length; j++)
            {
                if(verts[j].vertices == v3)
                {
                    found = true;
                    break;
                }
            }
            if(found)
            {
                //if we found the same vertex, don't add to verts, but use the same vertex with indices
                indices.push_back(j);
                idx++;
            }
            else
            {
                //vert does not exist yet, so we need to create a new one,
                auto c4b = Color4B::WHITE;
                auto t2f = Tex2F(0,0); // don't worry about tex coords now, we calculate that later
                V3F_C4B_T2F vert = {v3,c4b,t2f};
                verts.push_back(vert);
                indices.push_back(vdx);
                idx++;
                vdx++;
            }
        }
    }
    for(auto j : p2points)
    {
        delete j;
    };
    
    // now that we know the size of verts and indices we can create the buffers
    V3F_C4B_T2F* vertsBuf = new (std::nothrow) V3F_C4B_T2F[verts.size()];
    memcpy(vertsBuf, verts.data(), verts.size() * sizeof(V3F_C4B_T2F));
    
    unsigned short* indicesBuf = new (std::nothrow) unsigned short[indices.size()];
    memcpy(indicesBuf, indices.data(), indices.size() * sizeof(short));
    
    // Triangles should really use std::vector and not arrays for verts and indices.
    // Then the above memcpy would not be necessary
    TrianglesCommand::Triangles triangles = { vertsBuf, indicesBuf, static_cast< int>(verts.size()), static_cast< int>(indices.size()) };
    return triangles;
}

void PolygonSpriteProxy::calculateStandardUV(const Rect& rect, V3F_C4B_T2F* verts, ssize_t count)
{
    /*
     whole texture UV coordination
     0,0                  1,0
     +---------------------+
     |                     |0.1
     |                     |0.2
     |     +--------+      |0.3
     |     |texRect |      |0.4
     |     |        |      |0.5
     |     |        |      |0.6
     |     +--------+      |0.7
     |                     |0.8
     |                     |0.9
     +---------------------+
     0,1                  1,1
     */
    
    float texWidth  = _textureSize.x;
    float texHeight = _textureSize.y;
    
    auto end = &verts[count];
    for(auto i = verts; i != end; ++i)
    {
        // for every point, offset with the center point
        float u = (i->vertices.x - rect.origin.x) / texWidth;
        float v = (rect.origin.y + i->vertices.y) / texHeight;
        i->texCoords.u = u;
        i->texCoords.v = v;
    }
}

void PolygonSpriteProxy::calculatePolygonFollowingUV(V3F_C4B_T2F* verts, ssize_t count)
{
    auto end = &verts[count];
    
    for(auto i = verts; i != end; ++i)
    {
        Vec2 textureCoords;
        if(_textureCoords.find(Vec2(i->vertices.x, i->vertices.y)) != _textureCoords.end())
        {
            textureCoords = _textureCoords.at(Vec2(i->vertices.x, i->vertices.y));
        }
        i->texCoords.u = textureCoords.x;
        i->texCoords.v = textureCoords.y / _textureSize.y;
    }
}

void PolygonSpriteProxy::createBoundaryVertices()
{
    std::vector<Vec2> boundaryVertices(_edgeVertices.begin(), _edgeVertices.end());
    Vec2 startToEndConnection = (boundaryVertices[boundaryVertices.size() -1] - boundaryVertices[0]).getNormalized() * 6;
    startToEndConnection += boundaryVertices[0];
    boundaryVertices.push_back(startToEndConnection);
    
    std::vector<Vec2> interiorVertices(boundaryVertices.begin(), boundaryVertices.end());
    
    int lastIndex = boundaryVertices.size() - 1;
    int initialSize = boundaryVertices.size();
    
    Vec2 previousLine = (boundaryVertices[lastIndex] - boundaryVertices[lastIndex - 1]).getNormalized();
    double currentLength = 0;
    for(int i = lastIndex; i > 0; i--)
    {
        Vec2 nextLine = (boundaryVertices[i] - boundaryVertices[i - 1]);
        Vec2 line = (nextLine.getNormalized() + previousLine) / 2;
        line = line.getPerp().getNormalized();
        boundaryVertices.push_back(boundaryVertices[i] + (line));
        interiorVertices[i] -= (line * 2);
        _textureCoords.emplace(interiorVertices[i], Vec2(0, currentLength));
        _textureCoords.emplace(boundaryVertices[boundaryVertices.size() - 1], Vec2(1, currentLength));
        previousLine = nextLine.getNormalized();
        currentLength += nextLine.length();
    }
    
    Vec2 nextLine = (boundaryVertices[0] - boundaryVertices[lastIndex]).getNormalized();
    Vec2 line = nextLine.getNormalized();
    boundaryVertices.push_back(boundaryVertices[0] + (line));
    interiorVertices[0] -= (line * 2);
    _textureCoords.emplace(interiorVertices[0], Vec2(0, nextLine.length()));
    _textureCoords.emplace(boundaryVertices[boundaryVertices.size() - 1], Vec2(1, nextLine.length()));
    
    
    for(int i = 0; i < interiorVertices.size(); i++)
    {
        boundaryVertices[i] = interiorVertices[i];
    }
    
    _edgeVertices.clear();
    _edgeVertices.insert(_edgeVertices.begin(), boundaryVertices.begin(), boundaryVertices.end());
}

void PolygonSpriteProxy::setPosition(Vec2 position)
{
    _pendingPosition = position;
    _needsUpdate = true;
    globalCocosObjectManager->requestCocosObjectUpdate(this);
}

void PolygonSpriteProxy::setZIndex(int index)
{
    _pendingZIndex = index;
    _needsUpdate = true;
    globalCocosObjectManager->requestCocosObjectUpdate(this);
}

void PolygonSpriteProxy::setPositionZ(float position)
{
    _pendingZPosition = position;
    _needsUpdate = true;
    globalCocosObjectManager->requestCocosObjectUpdate(this);
}

void PolygonSpriteProxy::setVisible(bool visible)
{
    _pendingVisible = visible;
    _needsUpdate = true;
    globalCocosObjectManager->requestCocosObjectUpdate(this);
}

void PolygonSpriteProxy::setAdditionalTransform(AffineTransform & transform)
{
    _pendingTransform = transform;
}

void PolygonSpriteProxy::setPositionOffset(Vec2 offset)
{
    _positionOffset = offset;
    if(_sprite != nullptr)
    {
        setPosition(_sprite->getPosition()); //force update of position
    }
}

bool PolygonSpriteProxy::updateCocosObject()
{
    if(_pendingPosition.isChanged())
    {
        _sprite->setPosition(_pendingPosition + _positionOffset);
        _pendingPosition.clearChange();
    }
    if(_pendingZIndex.isChanged())
    {
        _sprite->setLocalZOrder(_pendingZIndex);
        _pendingZIndex.clearChange();
    }
    if(_pendingZPosition.isChanged())
    {
        _sprite->setPositionZ(_pendingZPosition);
        _pendingZPosition.clearChange();
    }
    if(_pendingVisible.isChanged())
    {
        _sprite->setVisible(_pendingVisible);
        _pendingVisible.clearChange();
    }
    if(_pendingTransform.isChanged())
    {
        _sprite->setAdditionalTransform((_pendingTransform));
    }
    _needsUpdate = false;
    return false;
}

void PolygonSpriteProxy::remove()
{
    globalCocosObjectManager->requestCocosObjectRemoval(this);
}
