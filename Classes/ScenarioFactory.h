//
//  Scenario.hpp
//  TrenchWars
//
//  Created by Paul Reed on 5/2/20.
//

#ifndef Scenario_h
#define Scenario_h

#include "cocos2d.h"
#include "pugixml/pugixml.hpp"

using namespace pugi;
USING_NS_CC;

enum DEBUG_SPAWN_MODE
{
    SPAWN_DO_NOTHING,
    SPAWN_DIG_IN,
    SPAWN_ATTACK_CP,
    SPAWN_GO_LEFT,
    SPAWN_GO_RIGHT
};

class CommandPointSpawn : public Ref
{
public:
    Vec2 location;
    std::string commandPostDefinitionName;
};

class FormationSpawn : public Ref
{
public:
    Vec2 location;
    std::string formationDefinitionName;
};

class CommandDefinition : public Ref
{
public:
    Vec2 spawnPoint;
    int supplyRate;
    int initialSupply;
    Vector<CommandPointSpawn *> commandPoints;
    Vector<FormationSpawn *> formations;
    DEBUG_SPAWN_MODE spawnAction = SPAWN_DO_NOTHING;
    unsigned int commandId;
    unsigned int teamId;
    unsigned int controllingPlayerId;
    bool aiCommand;
};

class TeamDefinition : public Ref
{
public:
    unsigned int teamId;
    Vector<CommandDefinition *> commands;
};

class ScenarioDefinition : public Ref
{
public:
    Vector<TeamDefinition *> teams;
    
    std::string terrainName;
    std::string scenarioName;
};


class ScenarioFactory : public Ref, xml_tree_walker
{
private:
    unsigned int nextCommandId;
    FormationSpawn * currentFormationSpawn;
    CommandPointSpawn * currentCommandSpawn;
    CommandDefinition * currentCommandDefinition;
    TeamDefinition * currentTeamDefinition;
    ScenarioDefinition * currentScenarioDefinition;
    
    Map<std::string,ScenarioDefinition *> scenarios;
public:
    
    ScenarioFactory();
    void parseScenarioFile(const std::string & filename);
    virtual bool for_each(xml_node& node);
    ScenarioDefinition * getScenario(const std::string & scenarioName);
    std::vector<std::string> getScenarioNames();
    
    DEBUG_SPAWN_MODE stringToSpawnAction(const std::string & spawnString);

};

extern ScenarioFactory * globalScenarioFactory;


#endif /* Scenario_h */
