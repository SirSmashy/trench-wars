//
//  LobbyScene.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/15/23.
//

#ifndef LobbyScene_h
#define LobbyScene_h

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "ScenarioFactory.h"
#include "AutoSizedListView.h"
#include "AutoSizedLayout.h"
#include "RawDataImageView.h"
#include "Modal.h"

using namespace cocos2d::ui;
USING_NS_CC;

class LobbyManager;
class Player;

class LobbyScene : public cocos2d::Scene, public cocos2d::ui::EditBoxDelegate
{
    AutoSizedListView * _scenarioMenu;
    AutoSizedListView * _commandsMenu;
    RawDataImageView * _scenarioImageView;

    LobbyManager * _lobbyManager;
    Button * _selectedScenarioButton;
    Button * _startButton;
    
    Player * _localPlayer;
    
    std::string _scenarioName;

//    ScrollView * chatScreen;
    bool init(LobbyManager * lobbyManager);
public:
    
    static LobbyScene* create(LobbyManager * lobbyManager);
    
    void commandSelected(int teamId, int commandId);
    void showScenariosButtonCallback(cocos2d::Ref* pSender);
    void showSavesButtonCallback(cocos2d::Ref* pSender);
    void showScenarioMenu();
    void updateCommandsMenu();

    void setScenarioImage(const unsigned char * data, size_t size);

    void showErrorMessage();
    void scenarioSelected(const std::string & scenarioName);
    
    virtual void editBoxReturn(EditBox* editBox);
};

#endif /* LobbyScene_h */
