//
//  ObjectiveDrawer.hpp
//  TrenchWars
//
//  Created by Paul Reed on 9/15/20.
//

#ifndef ObjectiveDrawer_h
#define ObjectiveDrawer_h

#include <stdio.h>
#include "LineDrawer.h"

class CollisionCell;
class Command;

class ObjectiveDrawer : public DrawNode
{
private:
    std::vector<Vec2> points;
    Vector<LineDrawer *> _lines;
    LineDrawer * _currentLine;
    Vec2 _currentPoint;
    Color3B _color;
   
    bool _currentSegmentValid;
    
    ObjectiveDrawer();
    ~ObjectiveDrawer();
    virtual bool init();
    
    bool nearStartPoint(Vec2 point);
    bool isCurrentSegmentValid(Vec2 segmentEnd);
            
public:
    static ObjectiveDrawer * create();

    virtual void dragStart(Vec2 point);
    virtual void dragMove(Vec2 point);
    virtual void dragEnd(Vec2 point);
    
    virtual void cancel();
};

#endif /* ObjectiveDrawer_h */
