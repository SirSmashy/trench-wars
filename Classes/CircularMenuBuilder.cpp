//
//  CircularMenuBuilder.cpp
//  TrenchWars
//
//  Created by Paul Reed on 12/10/22.
//

#include "GameEventController.h"
#include "CircularMenuBuilder.h"
#include "Command.h"
#include "PlayerLayer.h"
#include "World.h"
#include "EmblemManager.h"
#include "LandscapeObjectiveBuilder.h"
#include "Team.h"
#include "UIController.h"

void CircularMenuBuilder::populateForObjective(CircularMenu * menu, Objective * objective, Emblem * emblem, Vec2 location)
{
    if(!objective->isVisible()) {
        return;
    }
    
    InteractiveObject * object =  emblem->getCommandableObject();
    if(object == nullptr)
    {
        return;
    }
    
    menu->setPosition(location);
    // Create options for whatever entity is being dragged over this objective
    if(object->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
    {
        Unit * unit = dynamic_cast<Unit *>(object);
        if(unit->canBombard())
        {
            menu->addOption("Bombard",
            [=] (Vec2 point) {
                globalGameEventController->issueAttackPositionOrder(objective, unit, globalUIController->getCurrentPlan());
                emblem->setAnimation(EMBLEM_ATTACK, false);
                menu->clear();
            },
            !unit->positionValidForBombardment(location));
        }
        menu->addOption("Dig\nTrenches", [=] (Vec2 point) {
            globalGameEventController->issueDigTrenchesOrder(objective, unit, globalUIController->getCurrentPlan());
            emblem->setAnimation(EMBLEM_DIG, false);
            menu->clear();
        });
        
        if(objective->hasBunkers() && !objective->getBunkers()->isWorkComplete())
        {
            menu->addOption("Construct\nBunkers", [=] (Vec2 point) {
                globalGameEventController->issueDigBunkersOrder(objective, unit, globalUIController->getCurrentPlan());
                emblem->setAnimation(EMBLEM_DIG, false);
                menu->clear();
            });
        }
        
        menu->addOption("Occupy", [=] (Vec2 point) {
            globalGameEventController->issueOccupyOrder(objective, unit, globalUIController->getCurrentPlan());
            emblem->setAnimation(EMBLEM_MOVE, false);
            menu->clear();
        });
        
        if(!objective->hasCommandPost() && objective->getOwningTeam()->isPositionValidForNewCommandPost(location))
        {
            menu->addOption("Construct\nCommand Post", [=] (Vec2 point) {
                globalGameEventController->issueBuildCommandPostOrder(objective, unit, globalUIController->getCurrentPlan());
                emblem->setAnimation(EMBLEM_DIG, false);
                menu->clear();
            });
        }
    }
    else if(object->getInteractiveObjectType() == UNIT_GROUP_INTERACTIVE_OBJECT)
    {
        UnitGroup * group = dynamic_cast<UnitGroup *>(object);
        if(group->canBombard())
        {
            menu->addOption("Bombard",
                            [=] (Vec2 point) {
                group->issueAttackPositionOrder(objective);
                emblem->setAnimation(EMBLEM_ATTACK, false);
                menu->clear();
            },!group->positionValidForBombardment(location));
        }
        menu->addOption("Dig\nTrenches", [=] (Vec2 point) {
            group->issueDigTrenchesOrder(objective);
            emblem->setAnimation(EMBLEM_DIG, false);
            menu->clear();
        });
        
        if(objective->hasBunkers() && !objective->getBunkers()->isWorkComplete())
        {
            menu->addOption("Construct\nBunkers", [=] (Vec2 point) {
                group->issueDigBunkersOrder(objective);
                emblem->setAnimation(EMBLEM_DIG, false);
                menu->clear();
            });
        }
        
        menu->addOption("Occupy", [=] (Vec2 point) {
            group->issueOccupyOrder(objective);
            emblem->setAnimation(EMBLEM_MOVE, false);
            menu->clear();
        });
        if(objective->hasCommandPost() && !objective->getCommandPost()->isWorkComplete())
        {
            menu->addOption("Construct\nCommand Post", [=] (Vec2 point) {
                group->issueBuildCommandPostOrder(objective);
                emblem->setAnimation(EMBLEM_DIG, false);
                menu->clear();
            });
        }
    }
}


void CircularMenuBuilder::populateForLandscapeObjectiveBuilder(CircularMenu * menu, LandscapeObjectiveBuilder * builder, Emblem * emblem, Vec2 location)
{
    InteractiveObject * commandObject =  emblem->getCommandableObject();
    if(commandObject == nullptr)
    {
        return;
    }
    // Create options for whatever entity is being dragged over this objective
    if(commandObject->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
    {
        Unit * unit = dynamic_cast<Unit *>(commandObject);
        if(unit->canBombard())
        {
            menu->addOption("Bombard",
                [=] (Vec2 point) {
                    Objective * objective = builder->buildLandscapeObjective();
                    globalGameEventController->issueAttackPositionOrder(objective, unit, globalUIController->getCurrentPlan());
                    emblem->setAnimation(EMBLEM_ATTACK, false);
                    builder->unlock();
                    menu->clear();
                },unit->positionValidForBombardment(location));
        }
        menu->addOption("Occupy", [=] (Vec2 point) {
            Objective * objective = builder->buildLandscapeObjective();
            globalGameEventController->issueOccupyOrder(objective, unit, globalUIController->getCurrentPlan());
            emblem->setAnimation(EMBLEM_MOVE, false);
            builder->unlock();
            menu->clear();
        });
    }
}


void CircularMenuBuilder::populateForEmblem(CircularMenu * menu, Emblem * emblem, Vec2 location)
{
    menu->clear();
    InteractiveObject * commandObject =  emblem->getCommandableObject();
    if(commandObject == nullptr)
    {
        return;
    }
    Vec2 emblemCenter = emblem->getPosition();
    
    Vec2 screenLocation = globalPlayerLayer->convertToWorldSpace(world->getAboveGroundPosition(emblemCenter));
    menu->setGamePosition(emblemCenter);
    menu->setVisible(true);
    menu->setCircleExtent( -120 * D2R, 120 * D2R);
    
    float holeSize = std::min(emblem->getBoundingBox().size.width, emblem->getBoundingBox().size.height) * .49;
    holeSize *= globalPlayerLayer->getScale();
    menu->setHoleRadius(holeSize);
    Team * team = nullptr;
    
    if(commandObject->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
    {
        Unit * unit = dynamic_cast<Unit *>(commandObject);
        team = unit->getOwningTeam();
        if(emblem->getEmblemType() == POSITION_EMBLEM)
        {
            if(unit->hasOrder())
            {
                menu->addOption("Stop", [=] (Vec2 point) {
                    globalGameEventController->issueStopOrder(unit, globalUIController->getCurrentPlan());
                    menu->clear();
                });
            }
            else
            {
                menu->addOption("Dig In", [=] (Vec2 point) {
                    Rect bounds = unit->getBoundingRect();
                    std::vector<Vec2> points;
                    points.push_back( Vec2(bounds.getMinX(), bounds.getMinY()));
                    points.push_back( Vec2(bounds.getMaxX(), bounds.getMaxY()));
                    
                    globalGameEventController->createNewObjective(points, true, unit);
                    
                    emblem->setAnimation(EMBLEM_DEFAULT, false); //FIXME: make this a move emblem
                    menu->clear();
                });
            }
        }
        else
        {
            menu->addOption("Cancel", [=] (Vec2 point) {
                globalGameEventController->issueStopOrder(unit, globalUIController->getCurrentPlan());
                menu->clear();
            });
            if(emblem->canDrag())
            {
                menu->addOption("Move", [=] (Vec2 point) {
                    globalGameEventController->issueMoveOrder(location, unit, globalUIController->getCurrentPlan());
                    emblem->setAnimation(EMBLEM_MOVE, false);
                    menu->clear();
                });
            }
        }
    }
    else if(commandObject->getInteractiveObjectType() == UNIT_GROUP_INTERACTIVE_OBJECT)
    {
        UnitGroup * group = dynamic_cast<UnitGroup *>(commandObject);
        team = group->getOwningTeam();
        
        if(emblem->getEmblemType() == POSITION_EMBLEM)
        {
            if(group->hasOrder())
            {
                menu->addOption("Stop", [=] (Vec2 point) {
                    group->issueStopOrder();
                    menu->clear();
                });
            }
            else
            {
                menu->addOption("Dig In", [=] (Vec2 point) {
                    // TODO fix dig in stuff (make objecticve, add trench line, issue work order for trench line)
                    emblem->setAnimation(EMBLEM_DEFAULT, false); //FIXME: make this a move emblem
                    menu->clear();
                });
            }
        }
        else
        {
            menu->addOption("Cancel", [=] (Vec2 point) {
                group->issueStopOrder();
                menu->clear();
            });
            if(emblem->canDrag())
            {
                menu->addOption("Move", [=] (Vec2 point) {
                    group->issueMoveOrder(location);
                    emblem->setAnimation(EMBLEM_MOVE, false);
                    menu->clear();
                });
            }
        }
    }

    if(team != nullptr && emblem->canDrag())
    {
        Objective * obj = team->getObjectiveAtPoint(location);
        LandscapeObjectiveBuilder * builder = globalLandscapeObjectiveBuilder;
        
        if(obj != nullptr)
        {
            populateForObjective(menu, obj, emblem, location);
        }
        else if(builder != nullptr && builder->pointInObjective(location))
        {
            populateForLandscapeObjectiveBuilder(menu, builder, emblem, location);
        }
    }
   
}
