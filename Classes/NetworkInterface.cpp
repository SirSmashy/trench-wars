//
//  NetworkInterface.cpp
//  TrenchWars
//
//  Created by Paul Reed on 8/29/23.
//

#include "NetworkInterface.h"
#include "TrenchWarsManager.h"
#include "MultithreadedAutoReleasePool.h"
#include <chrono>
#include <thread>

#include "SerializationHelpers.h"
#include <cereal/types/string.hpp>


const uint32 DEFAULT_BUFFER_SIZE = 50000000;

NetworkInterface * globalNetworkInterface = nullptr;


struct Membuf : std::streambuf
{
    Membuf(char* begin, char* end) {
        this->setg(begin, begin, end);
    }
};


// /////////////////GameMessage //////////


GameMessage::GameMessage(char * bytes, size_t byteSize, int player, bool copyData) :
messageBytes(new char[byteSize])
{
    playerId = player;
    messageSize = byteSize;
    iArchive = nullptr;
    if(copyData)
    {
        memcpy(messageBytes, bytes, byteSize);
        createArchive();
    }
    headerUnion.byteValue = messageBytes;
}

GameMessage * GameMessage::create(char * bytes, size_t byteSize, int player, bool copyData)
{
    GameMessage * message = new GameMessage(bytes, byteSize, player, copyData);
    globalAutoReleasePool->addObject(message);
    return message;
}

void GameMessage::createArchive()
{
    buffer = new ArrayInputStreamBuf(messageBytes + MESSAGE_HEADER_SIZE,messageSize - MESSAGE_HEADER_SIZE);
    input = new std::istream(buffer);
    iArchive = new cereal::BinaryInputArchive(*input);
}


GameMessage::~GameMessage()
{
    if(iArchive != nullptr)
    {
        delete buffer;
        delete input;
        delete iArchive;
    }
    delete [] messageBytes;
}

// /////////////////ChunkedMessage //////////


ChunkedMessage::ChunkedMessage(size_t byteSize, int messageCount, int player) :
GameMessage(nullptr, byteSize, player, false)
{
    playerId = player;
    messageSize = byteSize;
    headerUnion.byteValue = messageBytes;
    _totalMessages = messageCount;
    _messagesReceived = 0;
}

ChunkedMessage * ChunkedMessage::create(size_t byteSize, int messageCount, int player)
{
    ChunkedMessage * message = new ChunkedMessage(byteSize, messageCount, player);
    globalAutoReleasePool->addObject(message);
    return message;
}



ChunkedMessage::~ChunkedMessage()
{
}


// /////////////////MessageBuffer //////////

MessageBuffer::MessageBuffer() :
_messageBuffer(new char[DEFAULT_BUFFER_SIZE]),
_streamBuf(DEFAULT_BUFFER_SIZE - MESSAGE_HEADER_SIZE, _messageBuffer + MESSAGE_HEADER_SIZE),
_outStream(&_streamBuf),
_archive(_outStream)
{
    
}

MessageBuffer::~MessageBuffer()
{
}

void MessageBuffer::reset()
{
    _streamBuf.clear();
    _outStream.clear(); // Hmmmmmmmm TODO Fix this might be a bug 
}

// /////////////////NetworkInterface //////////


NetworkInterface::NetworkInterface()
{
#ifdef STEAMNETWORKINGSOCKETS_OPENSOURCE
    SteamDatagramErrMsg errMsg;
    if ( !GameNetworkingSockets_Init( nullptr, errMsg ) )
        LOG_ERROR( "GameNetworkingSockets_Init failed.  %s", errMsg );
#else
    SteamDatagram_SetAppID( 570 ); // Just set something, doesn't matter what
    SteamDatagram_SetUniverse( false, k_EUniverseDev );
    
    SteamDatagramErrMsg errMsg;
    if ( !SteamDatagramClient_Init( errMsg ) )
        FatalError( "SteamDatagramClient_Init failed.  %s", errMsg );
    
    // Disable authentication when running with Steam, for this
    // example, since we're not a real app.
    //
    // Authentication is disabled automatically in the open-source
    // version since we don't have a trusted third party to issue
    // certs.
    SteamNetworkingUtils()->SetGlobalConfigValueInt32( k_ESteamNetworkingConfig_IP_AllowWithoutAuth, 1 );
#endif
    
    globalNetworkInterface = this;
    
    _networkInterface = SteamNetworkingSockets();
    _networkStatus = NETWORK_STATUS_NONE;
    _nextMessageId = 0;
    _nextBufferId = 0;
    
    SteamNetworkingUtils()->SetDebugOutputFunction( k_ESteamNetworkingSocketsDebugOutputType_Msg, NetworkInterface::DebugOutput );
    SteamNetworkingUtils()->SetGlobalConfigValueInt32( k_ESteamNetworkingConfig_IP_AllowWithoutAuth, 1 );
    SteamNetworkingUtils()->SetGlobalConfigValueInt32( k_ESteamNetworkingConfig_SendBufferSize, 3072000 );
}

void NetworkInterface::DebugOutput( ESteamNetworkingSocketsDebugOutputType eType, const char *pszMsg )
{
    SteamNetworkingMicroseconds time = SteamNetworkingUtils()->GetLocalTimestamp();
    LOG_DEBUG( "%10.6f %s\n", time*1e-6, pszMsg );
    fflush(stdout);
    if ( eType == k_ESteamNetworkingSocketsDebugOutputType_Bug )
    {
        fflush(stdout);
        fflush(stderr);
    }
}

void NetworkInterface::pollConnectionStateChanges()
{
    _networkInterface->RunCallbacks();
}

MessageBuffer * NetworkInterface::prepareNetworkPackage(MESSAGE_TYPE messageType, long long frameNumber)
{
    _bufferMutex.lock();
    if(_availableBuffers.size() == 0)
    {
        MessageBuffer * newBuffer = new MessageBuffer();
        newBuffer->_bufferId = _nextBufferId++;
        _availableBuffers.push_back(newBuffer);
    }
    
    MessageBuffer * buffer  = _availableBuffers.front();
    _inUseBuffers.emplace(buffer->_bufferId, buffer);

    _availableBuffers.pop_front();
    buffer->frameNumber = frameNumber;
    buffer->messageType = messageType;
    _bufferMutex.unlock();
    return buffer;
}

void NetworkInterface::SteamNetConnectionStatusChangedCallback( SteamNetConnectionStatusChangedCallback_t *pInfo )
{
    globalNetworkInterface->OnSteamNetConnectionStatusChanged( pInfo );
}

void NetworkInterface::update()
{
    if(_networkInterface != nullptr)
    {
        pollIncomingMessages();
        pollConnectionStateChanges();
        sendQueuedMessages();

    }
}

void NetworkInterface::parseChunkedMessage(char * data, int dataSize, int playerId)
{
    MessageHeaderUnion messageUnion;
    messageUnion.byteValue = data;
    

    if(_chunkedMessageMap.find(messageUnion.header->messageId) == _chunkedMessageMap.end())
    {
        ChunkedMessage * message = ChunkedMessage::create(messageUnion.header->messageDataSize + MESSAGE_HEADER_SIZE, messageUnion.header->messageCount, playerId);
        _chunkedMessageMap.insert(messageUnion.header->messageId, message);
        memcpy(_chunkedMessageMap.at(messageUnion.header->messageId)->getMessageBytes(), data, MESSAGE_HEADER_SIZE);
    }
    memcpy(_chunkedMessageMap.at(messageUnion.header->messageId)->getMessageBytes() + messageUnion.header->startByte + MESSAGE_HEADER_SIZE, data + MESSAGE_HEADER_SIZE, dataSize - MESSAGE_HEADER_SIZE);

    _chunkedMessageMap.at(messageUnion.header->messageId)->incrementMessagesReceived();
    if(_chunkedMessageMap.at(messageUnion.header->messageId)->getMessagesReceived() == _chunkedMessageMap.at(messageUnion.header->messageId)->getTotalMessage())
    {
        _chunkedMessageMap.at(messageUnion.header->messageId)->createArchive();
        _receivedMessagesMutex.lock();
        _receivedMessageQueue.pushBack(_chunkedMessageMap.at(messageUnion.header->messageId));
        _receivedMessagesMutex.unlock();
        _chunkedMessageMap.erase(messageUnion.header->messageId);
    }
}

void NetworkInterface::parseStandardMessage(char * data, int dataSize, int playerId)
{
    MessageHeaderUnion messageUnion;
    messageUnion.byteValue = data;
    _receivedMessagesMutex.lock();
    GameMessage * message = GameMessage::create(data, dataSize, playerId);
    
    _receivedMessageQueue.pushBack(message);
    _receivedMessagesMutex.unlock();
}

void NetworkInterface::sendChunkedMessage(MessageBuffer * message)
{
    int messageCount = ceil( (float) message->_streamBuf.size() / k_cbMaxSteamNetworkingSocketsMessageSizeSend);
    char * messageBytes = new char[k_cbMaxSteamNetworkingSocketsMessageSizeSend];
    size_t maxSize = (k_cbMaxSteamNetworkingSocketsMessageSizeSend - MESSAGE_HEADER_SIZE);
    size_t bytesLeft = message->_streamBuf.size();
    
    MessageHeaderUnion messageHeader;
    messageHeader.byteValue = messageBytes;
    messageHeader.header->messageCount = messageCount;
    messageHeader.header->messageId = _nextMessageId;
    messageHeader.header->messageDataSize = message->_streamBuf.size();
    messageHeader.header->frameNumber = message->frameNumber;
    messageHeader.header->messageType = message->messageType;
    size_t startByte = 0;
    
    for(unsigned char i = 0; bytesLeft > 0; i++)
    {
        size_t dataSize = bytesLeft > maxSize ? maxSize : bytesLeft;
        messageHeader.header->startByte = startByte;
        memcpy(messageBytes + MESSAGE_HEADER_SIZE, message->_streamBuf.getData() + startByte, dataSize);
        for(auto connection : message->connectionsToSendTo)
        {
            int retry = 0;

            auto result = _networkInterface->SendMessageToConnection(connection, messageBytes, dataSize + MESSAGE_HEADER_SIZE, k_nSteamNetworkingSend_Reliable, nullptr );
            while(result == k_EResultLimitExceeded && retry < 5)
            {
                LOG_WARNING("  Fail retry \n");
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                result = _networkInterface->SendMessageToConnection( connection, messageBytes, dataSize + MESSAGE_HEADER_SIZE, k_nSteamNetworkingSend_Reliable, nullptr );
                retry++;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        }
        bytesLeft -= dataSize;
        startByte += dataSize;
    }
}

void NetworkInterface::sendStandardMessage(MessageBuffer * message)
{
    MessageHeaderUnion messageHeader;
    messageHeader.byteValue = message->_messageBuffer;
    messageHeader.header->messageCount = 1;
    messageHeader.header->messageId = _nextMessageId;
    messageHeader.header->messageDataSize = message->_streamBuf.size();
    messageHeader.header->frameNumber = message->frameNumber;
    messageHeader.header->startByte = 0;
    messageHeader.header->messageType = message->messageType;
    
    for(auto connection : message->connectionsToSendTo)
    {
        _networkInterface->SendMessageToConnection( connection, message->_messageBuffer, message->_streamBuf.size() + MESSAGE_HEADER_SIZE, k_nSteamNetworkingSend_Reliable, nullptr );
    }
}


void NetworkInterface::drainReceivedQueue()
{
    _receivedMessagesMutex.lock();
    while(_receivedMessageQueue.size() > 0)
    {
        GameMessage * message = _receivedMessageQueue.front();
        _receivedMessagesMutex.unlock();
        
        globalTrenchWarsManager->receivedNetworkMessage(message);
        _receivedMessagesMutex.lock();
        _receivedMessageQueue.popFront();
    }
    _receivedMessagesMutex.unlock();
}

void NetworkInterface::sendQueuedMessages()
{
    _bufferMutex.lock();
    while(_buffersWaitingToBeSent.size() > 0)
    {
        MessageBuffer * buffer = _buffersWaitingToBeSent.front();
        _buffersWaitingToBeSent.pop_front();
        _bufferMutex.unlock();
        if(buffer->_streamBuf.size() > k_cbMaxSteamNetworkingSocketsMessageSizeSend)
        {
            sendChunkedMessage(buffer);
        }
        else
        {
            sendStandardMessage(buffer);
        }
        buffer->reset();
        _nextMessageId++;
        _bufferMutex.lock();
        _availableBuffers.push_back(buffer);
    }
    _bufferMutex.unlock();
}

void NetworkInterface::shutdown()
{
    for(auto buffer : _availableBuffers)
    {
        delete buffer;
    }
    _availableBuffers.clear();
    
    for(auto buffer : _buffersWaitingToBeSent)
    {
        delete buffer;
    }
    _buffersWaitingToBeSent.clear();
    for(auto buffer : _inUseBuffers)
    {
        delete buffer.second;
    }
    
    _chunkedMessageMap.clear();
#ifdef STEAMNETWORKINGSOCKETS_OPENSOURCE
    GameNetworkingSockets_Kill();
#else
    SteamDatagramClient_Kill();
#endif
}

void NetworkInterface::stupidTestSend(int playerId)
{
    LOG_DEBUG("Stupid test send! \n");
    MessageBuffer * buffer = prepareNetworkPackage(MESSAGE_TEST);
    auto & archive = buffer->getArchive();
    
    int size = 20;
    archive(size);
    for(int i = 0; i < 20; i++)
    {
        archive(i);
    }
    
    std::string test = "lolTest";
    archive(test);
    sendPreparedDataToPlayer(buffer, playerId);
}

void NetworkInterface::stupidTestReceive(GameMessage * message)
{
    LOG_DEBUG("Stupid test receive %d \n", message->getHeader()->messageType);
    for(int i = 0; i < message->getHeader()->messageDataSize + MESSAGE_HEADER_SIZE; i++)
    {
        LOG_DEBUG("  %d\n", message->getMessageBytes()[i]);
    }

    
    auto & inputArchive = message->getArchive();
    int size, value;
    
    inputArchive(size);
    for(int i = 0; i < size; i++)
    {
        inputArchive(value);
        LOG_DEBUG("%d \n", value);
    }
    
    std::string inputName;
    inputArchive(inputName);
    LOG_DEBUG("%s \n", inputName.c_str());
}
