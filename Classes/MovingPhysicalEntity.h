//
//  MovingPhysicalEntity.hpp
//  TrenchWars
//
//  Created by Paul Reed on 1/29/22.
//

#ifndef MovingPhysicalEntity_h
#define MovingPhysicalEntity_h

#include "PhysicalEntity.h"
#include "Timing.h"
#include "CountTo.h"
#include "List.h"


#define POSITIONS_SAME_DISTANCE 0.5
#define POSITIONS_SAME_DISTANCE_SQ 2



class MapSector;
class Path;

enum ACTIONTYPE
{
    ACTION_NONE,
    ACTION_ATTACK,
    ACTION_RELOADING,
    ACTION_DIG,
    ACTION_HELP
};

struct GoalPosition
{
    Vec2 position = Vec2();
    bool ignoreCover = false;
    
    GoalPosition()
    {
        position = Vec2();
        ignoreCover = false;
    }
    
    template<class A>
    void serialize(A & archive)
    {
        archive(position);
        archive(ignoreCover);
    }
};

struct MovingPhysicalEntityNetworkUpdate
{
    float   moveRate = 0;
    Vec2    nextMovePosition = Vec2::ZERO;
    Vec2    moveVector = Vec2::ZERO;
    bool        atGoalPosition = true;
    ACTIONTYPE  performingAction = ACTION_NONE;
    DIRECTION goalFacing = RIGHT;
    float   waitTime = 0;
    bool prone = false;
};

class MovingPhysicalEntity : public PhysicalEntity
{
protected:
    
    // Velocity
    Vec2 _velocity;
    Vec2 _averageVelocity;
    Vec2 _velocityAccumulator;
    std::list<Vec2> _velocityMovingWindow;
    bool _prone;
    
    // Movement related
    float   _moveRate; //Current move rate
    float   _maximumMoveRate; // Maximum move rate
    Vec2    _nextMovePosition; //Where to move to next
    Vec2    _moveVector; //Direction the ent is currently moving
    bool    _atGoalPosition;
    Path * _movePath; //Path to follow to goal position
    GoalPosition _goalPosition;
    DIRECTION _goalFacing; 

    MapSector * _currentMapSector;
    ACTIONTYPE  _performingAction;
    double  _timeSinceLastAction;
    
    Team * _owningTeam;

    // Location this entity has claimed as its own (used to handle positing of entities).
    // Other entities will not try to move to this location (though they may pass through the location)
    CollisionCell * _claimedPosition;
    SimpleChangeTrackingVariable<bool> _pathReady;
    SimpleChangeTrackingVariable<GoalPosition> _pendingGoalPosition;
    bool _waitingForPath;

    // Collision related
    CountTo _checkFriendCollisionCounter;
    float   _collisionsPerSecond = 0;
    bool    _avoidingCollision = false;
    float   _waitTime = 0;

    // Network
    MovingPhysicalEntityNetworkUpdate _networkVariables;
    
protected:

    
    void startNewMoveGoal(GoalPosition goal);

    MovingPhysicalEntity();
    bool init(MovingPhysicalEntityDefinition * definition, Vec2 position, Team * team);
    virtual void postLoad();
    void setGoalPosition(Vec2 position, bool ignoreCover = false);
    void getPath();
    void calculateAverageVelocity();
    
    void determineNextMovePosition();
    bool didCollideWithFriendly();
    void avoidCollisions();
    void setMovePosition(Vec2 position);
    bool checkIfMoveNeedsPath();

    
    virtual void startedMoving();
    virtual void goalPositionReached();
    void transitionToArea(Vec2 positionInArea);

    virtual CollisionCell * getOptimumCellAroundCell(CollisionCell * center);
    void changeAnimationState();
    
public:
    
    bool collisionTest;
    
    virtual bool shouldQuery() {return true;}
    virtual bool shouldAct() {return true;}
    
    virtual void query(float deltaTime);
    virtual void act(float deltaTime);
    
    Vec2 getGoalPosition();
    bool isMoving() {return !_velocity.isZero();}
    ACTIONTYPE getPerformingAction() {return _performingAction;}
    virtual void setPerformingAction(ACTIONTYPE action);

    virtual void setFacing(DIRECTION newFacing);
    
    void setClaimedPosition(CollisionCell * newPosition);
    CollisionCell * getClaimedPosition() {return _claimedPosition;}
    
    bool isProne() {return _prone;}
    virtual bool isPerformingVisibleAction() {return false;}

    
    virtual Vec2 getVelocity() {return _velocity;}
    virtual Vec2 getAverageVelocity() {return _averageVelocity;}
    
    void updateCollisionAlternative();
    void updateCollision();
    
    bool atGoalPosition() {return _atGoalPosition;}
    
    // Team related
    Team * getOwningTeam() {return _owningTeam;}
    int getTeamID();
    
    
    virtual int hasUpdateToSendOverNetwork();
    virtual void saveNetworkUpdate(cereal::BinaryOutputArchive & archive);
    virtual void updateFromNetwork(cereal::BinaryInputArchive & archive);
    
    virtual void setDebugMe();
    virtual void clearDebugMe();
    virtual void populateDebugPanel(GameMenu * menu, Vec2 location);

    void removeFromCollisionGrid();
    virtual void removeFromGame();
    
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

#endif /* MovingPhysicalEntity_h */
