//
//  EntityDefinitionXMLParser.cpp
//  TrenchWars
//
//  Created by Paul Reed on 9/4/12.
//  Copyright (c) 2012 . All rights reserved.
//

#include "EntityDefinitionXMLParser.h"
#include "CollisionGrid.h"
#include "StringUtils.h"
#include <stdexcept>


EntityDefinitionXMLParser::EntityDefinitionXMLParser(EntityFactory * factory)
{
    currentPhysicalEntityDefinition = nullptr;
    currentUnitDefinition = nullptr;
    currentUnitMemberDefinition = nullptr;
    _entityFactory = factory;
    currentParticleList = nullptr;
}

void EntityDefinitionXMLParser::parseFile(std::string const & filename)
{
    std::string fileContents = FileUtils::getInstance()->getStringFromFile(filename);
    if(!fileContents.empty())
    {
        xml_document doc;
        pugi::xml_parse_result result = doc.load(fileContents.c_str());
        if(result.status == xml_parse_status::status_ok)
        {
            bool parsedSuccessfully = doc.traverse(*this);
            if(!parsedSuccessfully)
            {
                CCLOG("parse error!");
            }
        }
        else
        {
            CCLOG("parse error %s ",result.description());
        }
    }
    else
    {
        CCLOG("EntityDefinitionXMLParser: Could not Open File %s",filename.c_str());
    }
}

bool EntityDefinitionXMLParser::for_each(xml_node& node)
{
    ci_string name = node.name();
    std::string value = node.first_child().value();
    
    try
    {
        if(name.compare("Infantry") == 0)
        {
            currentPhysicalEntityDefinition = new InfantryDefinition();
            auto attribute = node.attribute("id");
            if(attribute != nullptr)
            {
                currentPhysicalEntityDefinition->_entityName = attribute.value();
                currentPhysicalEntityDefinition->_hashedName = std::hash<std::string>{}(currentPhysicalEntityDefinition->_entityName);
                _entityFactory->addPhysicalEntityDefinition(currentPhysicalEntityDefinition);
            }
        }
        if(name.compare("Courier") == 0)
        {
            currentPhysicalEntityDefinition = new HumanDefinition();
            auto attribute = node.attribute("id");
            if(attribute != nullptr)
            {
                currentPhysicalEntityDefinition->_entityName = attribute.value();
                currentPhysicalEntityDefinition->_hashedName = std::hash<std::string>{}(currentPhysicalEntityDefinition->_entityName);
                _entityFactory->addPhysicalEntityDefinition(currentPhysicalEntityDefinition);
            }
        }
        else if(name.compare("CrewWeapon") == 0)
        {
            currentPhysicalEntityDefinition = new CrewWeaponDefinition();
            auto attribute = node.attribute("id");
            if(attribute != nullptr)
            {
                currentPhysicalEntityDefinition->_entityName = attribute.value();
                currentPhysicalEntityDefinition->_hashedName = std::hash<std::string>{}(currentPhysicalEntityDefinition->_entityName);
                _entityFactory->addPhysicalEntityDefinition(currentPhysicalEntityDefinition);
            }
        }
        else if(name.compare("Unit") == 0)
        {
            currentUnitDefinition = new UnitDefinition();
            currentUnitDefinition->_strength = 0;
            auto attribute = node.attribute("id");
            if(attribute != nullptr)
            {
                currentUnitDefinition->_entityName = attribute.value();
                currentUnitDefinition->_hashedName = std::hash<std::string>{}(currentUnitDefinition->_entityName);

                _entityFactory->addUnitDefinition(currentUnitDefinition);
            }
        }
        else if(name.compare("UnitMember") == 0)
        {
            currentUnitMemberDefinition = new UnitMemberDefinition();
            currentUnitDefinition->_members.pushBack(currentUnitMemberDefinition);
            currentUnitDefinition->_strength++;

        }
        else if(name.compare("StaticEntity") == 0)
        {
            currentPhysicalEntityDefinition = new StaticEntityDefinition();
            auto attribute = node.attribute("id");
            if(attribute != nullptr)
            {
                currentPhysicalEntityDefinition->_entityName = attribute.value();
                currentPhysicalEntityDefinition->_hashedName = std::hash<std::string>{}(currentPhysicalEntityDefinition->_entityName);
                _entityFactory->addPhysicalEntityDefinition(currentPhysicalEntityDefinition);
            }
        }       
        else if(name.compare("Cloud") == 0)
        {
            currentCloudDefinition = new CloudDefinition();
            auto attribute = node.attribute("id");
            if(attribute != nullptr)
            {
                currentCloudDefinition->_entityName = attribute.value();
                currentCloudDefinition->_hashedName = std::hash<std::string>{}(currentPhysicalEntityDefinition->_entityName);
                _entityFactory->addCloudDefinition(currentCloudDefinition);
            }
        }
        ///////// physical entity/
        else if(name.compare("type") == 0)
        {
            currentPhysicalEntityDefinition->_type = (ENTITY_TYPE)std::stoi(value);
        }
        else if(name.compare("spriteName") == 0)
        {
            currentPhysicalEntityDefinition->_spriteName = value;
        }
        else if(name.compare("workPosition") == 0)
        {
            currentPhysicalEntityDefinition->workPositions.push_back(parseVector(value));
        }
        else if(name.compare("CollisionSize") == 0)
        {
            currentPhysicalEntityDefinition->_collisionSize = parseVector(value);
        }
        else if(name.compare("SpriteSize") == 0)
        {
            currentPhysicalEntityDefinition->_spriteSize = parseVector(value);
        }
        else if(name.compare("WorkTime") == 0)
        {
            currentPhysicalEntityDefinition->_workTime =std::stoi(value);
        }
        
        //////////////////////////////// moving  physical entity
        else if(name.compare("speed") == 0)
        {
            MovingPhysicalEntityDefinition * actor = (MovingPhysicalEntityDefinition *) currentPhysicalEntityDefinition;
            actor->_speed =std::stof(value);
        }
        
        /////// human
        else if(name.compare("health") == 0)
        {
            HumanDefinition * actor = (HumanDefinition *) currentPhysicalEntityDefinition;
            actor->_health = std::stoi(value);
        }

        //////////// infantry
        else if(name.compare("targetAquireSpeed") == 0)
        {
            InfantryDefinition * infantry = (InfantryDefinition *) currentPhysicalEntityDefinition;
            infantry->_targetAquireSpeed =std::stof(value);
        }

        else if(name.compare("sightRadius") == 0)
        {
            HumanDefinition * human = (HumanDefinition *) currentPhysicalEntityDefinition;
            human->_sightRadius =std::stof(value);
        }
        else if(name.compare("chargeRadius") == 0)
        {
            InfantryDefinition * infantry = (InfantryDefinition *) currentPhysicalEntityDefinition;
            infantry->_chargeRadius =std::stof(value);
        }
        else if(name.compare("weapon") == 0)
        {
            InfantryDefinition * infantry = (InfantryDefinition *) currentPhysicalEntityDefinition;
            WeaponDefinition * weapon = _entityFactory->getWeaponDefinitionWithName(value);
            infantry->_weapons.pushBack(weapon);
        }
        else if(name.compare("ammoReserve") == 0)
        {
            InfantryDefinition * infantry = (InfantryDefinition *) currentPhysicalEntityDefinition;
            infantry->_ammoReserve =std::stoi(value);
        }
        else if(name.compare("muzzlePositionOffset") == 0)
        {
            InfantryDefinition * infantry = (InfantryDefinition *) currentPhysicalEntityDefinition;
            infantry->_muzzlePositionOffset = parseVector(value);
        }
        
        ////////// Crew Weapaon
        else if(name.compare("WeaponName") == 0)
        {
            CrewWeaponDefinition * crew = (CrewWeaponDefinition *) currentPhysicalEntityDefinition;
            WeaponDefinition * weapon = _entityFactory->getWeaponDefinitionWithName(value);
            crew->_weapon = weapon;
        }
        else if(name.compare("crewWeaponAmmo") == 0)
        {
            CrewWeaponDefinition * crew = (CrewWeaponDefinition *) currentPhysicalEntityDefinition;
            crew->_crewWeaponAmmo =std::stoi(value);
        }
        else if(name.compare("crewWeaponMuzzlePositionOffset") == 0)
        {
            CrewWeaponDefinition * crew = (CrewWeaponDefinition *) currentPhysicalEntityDefinition;
            crew->_muzzlePositionOffset = parseVector(value);
        }

        /////////////////////////////unit definition////////
        else if(name.compare("unitSprite") == 0)
        {
            currentUnitDefinition->_spriteName = value;
        }
        else if(name.compare("unitSupplyCost") == 0)
        {
            currentUnitDefinition->_supplyCost = std::stoi(value);
        }
        else if(name.compare("crewWeapoon") == 0)
        {
            currentUnitDefinition->_crewWeapaon = value;
        }
        else if(name.compare("crewWeapoonAmmoReserve") == 0)
        {
            currentUnitDefinition->_crewWeaponAmmoReserve = std::stoi(value);
        }
        

        //////////////////////////// unit member definition//////////////////////
        else if(name.compare("xOffset") == 0)
        {
            currentUnitMemberDefinition->_xOffset =std::stof(value);
        }
        else if(name.compare("yOffset") == 0)
        {
            currentUnitMemberDefinition->_yOffset =std::stof(value);
        }
        else if(name.compare("memberName") == 0)
        {
            currentUnitMemberDefinition->_memberName = value;
        }
        //////////////////////////// Static Object definition//////////////////////
        else if(name.compare("AnimationName") == 0)
        {
            StaticEntityDefinition * landscape = (StaticEntityDefinition *) currentPhysicalEntityDefinition;
            landscape->_animationName = _entityFactory->nameToAnimationType(value);
        }
        else if(name.compare("StaticEntityHealth") == 0)
        {
            StaticEntityDefinition * landscape = (StaticEntityDefinition *) currentPhysicalEntityDefinition;
            landscape->_health =std::stoi(value);
        }
        else if(name.compare("Cover") == 0)
        {
            StaticEntityDefinition * landscape = (StaticEntityDefinition *) currentPhysicalEntityDefinition;
            landscape->_cover =std::stod(value);
        }
        else if(name.compare("Hardness") == 0)
        {
            StaticEntityDefinition * landscape = (StaticEntityDefinition *) currentPhysicalEntityDefinition;
            landscape->_hardness =std::stod(value);
        }
        else if(name.compare("Height") == 0)
        {
            StaticEntityDefinition * landscape = (StaticEntityDefinition *) currentPhysicalEntityDefinition;
            landscape->_height =std::stoi(value);
        }
        else if(name.compare("Impassable") == 0)
        {
            StaticEntityDefinition * landscape = (StaticEntityDefinition *) currentPhysicalEntityDefinition;
            landscape->_impassable = std::stoi(value);
        }
        else if(name.compare("CanRotate") == 0)
        {
            StaticEntityDefinition * landscape = (StaticEntityDefinition *) currentPhysicalEntityDefinition;
            landscape->_allowRotation =std::stoi(value);
        }
        else if(name.compare("SpawnParticles") == 0)
        {
            StaticEntityDefinition * landscape = (StaticEntityDefinition *) currentPhysicalEntityDefinition;
            currentParticleList = &landscape->_spawnParticles;
        }
        else if(name.compare("SpawnOnDeath") == 0)
        {
            StaticEntityDefinition * landscape = (StaticEntityDefinition *) currentPhysicalEntityDefinition;
            landscape->_spawnOnDeath = value;
        }
        else if(name.compare("MapColor") == 0)
        {
            StaticEntityDefinition * landscape = (StaticEntityDefinition *) currentPhysicalEntityDefinition;
            landscape->_mapColor = parseColor4B(value);
        }
        else if(name.compare("Particle") == 0)
        {
            if(currentParticleList != nullptr)
            {
                parseParticle(currentParticleList, node);
            }
            else
            {
                LOG_ERROR("No particle list to add to! \n");
            }
        }
        //////////////////////////// Cloud definition//////////////////////
        else if(name.compare("ParticleName") == 0)
        {
            currentCloudDefinition->particleName = value;
        }
        else if(name.compare("BurstRadiusChangePerSecond") == 0)
        {
            currentCloudDefinition->burstRadiusChangePerSecond = stof(value);
        }
        else if(name.compare("BurstTime") == 0)
        {
            currentCloudDefinition->burstTime = stof(value);
        }
        else if(name.compare("LifeSpan") == 0)
        {
            currentCloudDefinition->lifeSpan = stof(value);
        }
        else if(name.compare("InitialDensity") == 0)
        {
            currentCloudDefinition->initialDensity = stof(value);
        }       
        else if(name.compare("DensityChangePerSecond") == 0)
        {
            currentCloudDefinition->densityChangePerSecond = stof(value);
        }        
        else if(name.compare("DensityToAlphaMultiple") == 0)
        {
            currentCloudDefinition->densityToAlphaMultiple = stof(value);
        }
        else if(name.compare("EnduringRadiusChangePerSecond") == 0)
        {
            currentCloudDefinition->enduringRadiusChangePerSecond = stof(value);
        }       
        else if(name.compare("ParticleCount") == 0)
        {
            currentCloudDefinition->particleCount = stof(value);
        }
        else if(name.compare("ParticleSizeToRadiusRatio") == 0)
        {
            currentCloudDefinition->particleSizeToRadiusRatio = stof(value);
        }
    }
    catch (const std::invalid_argument& ia)
    {
        CCLOG("Invalid argument: %s for node %s with value %s ", ia.what(), name.c_str(), value.c_str());
        return false;
    }
    
    return true;
}


void EntityDefinitionXMLParser::parseParticle(std::vector<ParticleDefinition> * particlesList, xml_node& node)
{
    particlesList->emplace_back();
    
    ParticleDefinition & definition = particlesList->back();
    auto attribute = node.attribute("name");
    if(attribute != nullptr)
    {
        definition.name = attribute.value();
    }
    
    attribute = node.attribute("offset");
    if(attribute != nullptr)
    {
        definition.offset = parseVector(attribute.value());
    }
    attribute = node.attribute("height");
    if(attribute != nullptr)
    {
        definition.height = std::stof(attribute.value());
    }
    attribute = node.attribute("particlePerCell");
    if(attribute != nullptr)
    {
        definition.particlePerCell = std::stoi(attribute.value());
    }
    attribute = node.attribute("permanent");
    if(attribute != nullptr)
    {
        definition.permanent = std::stoi(attribute.value());
    }
}
