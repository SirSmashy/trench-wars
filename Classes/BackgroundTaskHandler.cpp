//
//  BackgroundTaskHandler.m
//  TrenchWars
//
//  Created by Paul Reed on 6/29/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#include "BackgroundTaskHandler.h"
#include "CollisionGrid.h"
#include "EngagementManager.h"
#include "MapSectorAnalysis.h"
#include "AICommand.h"
#include <chrono>
#include <thread>

#define MAX_SLEEP 4194304 // roughly 4 ms

BackgroundTaskHandler * globalBackgroundTaskHandler;

void BackgroundTask::init(BackgroundTaskType type)
{
    _taskType = type;
    _frameRequested = globalEngagementManager->getFrameCount();
}

BackgroundTask::~BackgroundTask()
{
    
}

bool BackgroundTask::readyToExecute()
{
    return true;
}

//////////////////////////// Map Sector Analysis Update Request

MapSectorAnalysisUpdateRequest::MapSectorAnalysisUpdateRequest() {
    
}


void   MapSectorAnalysisUpdateRequest::init(MapSectorAnalysis * analysis)
{
    BackgroundTask::init(MAP_SECTOR_ANALYSIS_UPDATE);
    _analysisToBeUpdated = analysis;
}

MapSectorAnalysisUpdateRequest * MapSectorAnalysisUpdateRequest::create(MapSectorAnalysis * analysis)
{
    MapSectorAnalysisUpdateRequest * newRequest = new MapSectorAnalysisUpdateRequest();
    newRequest->init(analysis);
    return newRequest;
}



bool MapSectorAnalysisUpdateRequest::isEqual(BackgroundTask * otherTask)
{
    if(otherTask->_taskType == MAP_SECTOR_ANALYSIS_UPDATE)
    {
        MapSectorAnalysisUpdateRequest * otherRequest = (MapSectorAnalysisUpdateRequest * ) otherTask;
        if(_analysisToBeUpdated == otherRequest->_analysisToBeUpdated)
            return true;
    }
    return false;
}

bool MapSectorAnalysisUpdateRequest::readyToExecute()
{
    if(globalEngagementManager->getFrameCount() - _frameRequested > 10)
    {
        return true;
    }
    return false;
}

void MapSectorAnalysisUpdateRequest::executeTask(int theadID, int testID)
{
    // _analysisToBeUpdated->performAnalysis;
}

/////////// StrategyUpdateRequest ////////////////
StrategyUpdateRequest::StrategyUpdateRequest() {
    
}

void StrategyUpdateRequest::init(int goalType, AICommand * command) {
    BackgroundTask::init(AI_GOAL_UPDATE);
    _goalType = goalType;
    _command = command;
}

StrategyUpdateRequest * StrategyUpdateRequest::create(int goalType, AICommand * command)
{
    StrategyUpdateRequest * newRequest = new StrategyUpdateRequest();
    newRequest->init(goalType, command);
    return newRequest;
}


bool StrategyUpdateRequest::isEqual(BackgroundTask * otherTask)
{
    if(otherTask->_taskType == AI_GOAL_UPDATE)
    {
        StrategyUpdateRequest * otherRequest = (StrategyUpdateRequest * ) otherTask;
        if(_goalType == otherRequest->_goalType && _command == otherRequest->_command)
            return true;
    }
    return false;
}

bool StrategyUpdateRequest::readyToExecute()
{
    if(globalEngagementManager->getFrameCount() - _frameRequested > 30)
    {
        return true;
    }
    return false;
}

void StrategyUpdateRequest::executeTask(int theadID, int testID)
{
    _command->performAnalysisOfSectorsForGoalType((AI_GOAL_TYPE) _goalType);
}
////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////BackgroundTaskHandler/////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////

BackgroundTaskHandler::BackgroundTaskHandler(int threadCount)
{
    _shouldRun = true;
    currentThreadNumber = 1;
    _canRunPathfindingTasks = true;
    _pathfindingTasksActive = 0;
    if(threadCount < 1)
        threadCount = 1;
    
    for(int i = 0; i < threadCount; i++)
    {
        std::thread * newThread = new std::thread(&BackgroundTaskHandler::backgroundTaskLoop, this);
        threads.push_back(newThread);
    }
    globalBackgroundTaskHandler = this;
}

BackgroundTaskHandler::~BackgroundTaskHandler()
{
    
}

bool BackgroundTaskHandler::runNextPathfindingTask()
{
    std::function<const void(void)> task;
    if(pathFindingTasks.try_dequeue(task))
    {
        _pathfindingTasksActive++;
        task();
        _pathfindingTasksActive--;
        return true;
    }
    return false;

}

bool BackgroundTaskHandler::BackgroundTaskHandler::runNextBackgroundTask()
{
    std::function<const bool(void)> task;
    if(backgroundTasks.try_dequeue(task))
    {
        bool result = task();
        if(result)
        {
            backgroundTasks.enqueue(task);
        }
        return true;
    }
    return false;
}


int BackgroundTaskHandler::getNextThreadId()
{
    return currentThreadNumber++;
}

void BackgroundTaskHandler::addBackgroundTask(const std::function<bool(void) > & task)
{
    backgroundTasks.enqueue(task);
}

void BackgroundTaskHandler::addPathfindingTask(const std::function<bool(void)>  & task)
{
    pathFindingTasks.enqueue(task);
}

size_t BackgroundTaskHandler::getBackgroundTasksSize()
{
    return backgroundTasks.size_approx();
}

void BackgroundTaskHandler::shutDown()
{
    _shouldRun = false;
    
    for(auto thread : threads)
    {
        thread->join();
    }
}

void BackgroundTaskHandler::backgroundTaskLoop(void * taskHandler)
{
    std::function<const void(void)> task;
    BackgroundTaskHandler * handler = (BackgroundTaskHandler *) taskHandler;
    int threadID;
    unsigned int sleepTime = 1;
    bool ranTask;
    while(handler->_shouldRun)
    {
        if(!handler->runNextBackgroundTask())
        {
            if(handler->_canRunPathfindingTasks)
            {
                if(handler->runNextPathfindingTask())
                {
                    sleepTime = 1;
                }
                else
                {
                    sleepTime = sleepTime << 1;
                    if(sleepTime > MAX_SLEEP)
                    {
                        sleepTime = MAX_SLEEP;
                    }
                }
            }
        }
        else
        {
            sleepTime = 1;
        }
        std::this_thread::sleep_for(std::chrono::nanoseconds(sleepTime));
    }
}
