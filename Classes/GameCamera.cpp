//
//  GameCamera.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/4/20.
//

#include "GameCamera.h"
#include "GameVariableStore.h"
#include "World.h"
#include "PlayerLayer.h"
#include "EngagementManager.h"
#include "CollisionGrid.h"
#include "TestBed.h"

GameCamera * globalCamera;

GameCamera::GameCamera()
{
    
}

bool GameCamera::init()
{
    _currentScale = 1.0;
    globalCamera = this;
    cameraBounds = world->getCollisionGrid()->getWorldBounds();
    return true;
}
    
GameCamera * GameCamera::create()
{
    GameCamera * newCamera = new GameCamera();
    if(newCamera->init())
    {
        newCamera->autorelease();
        
        return newCamera;
    }
    
    CC_SAFE_DELETE(newCamera);
    return nullptr;
}

// Point conversion routines
Vec2 GameCamera::convertPointToNodeSpace(Vec2 point, Node * node)
{
    return world->getWorldCocosNode()->convertToNodeSpace(node->convertToWorldSpace(point));
}
Vec2 GameCamera::convertPointWorldSpace(Vec2 touchLocation, Node * node)
{
    // do the inverse of the routine above
    // Where touchLocation is the result of what is called from the UIGestureRecognizer
    auto director = cocos2d::Director::getInstance();
    
    Vec2 newPos = director->convertToGL(touchLocation);
    newPos = node->convertToNodeSpace(newPos);
    return newPos;
}
   
void GameCamera::setCameraPosition(Vec2 newCameraPosition)
{
    Size winSize = Director::getInstance()->getVisibleSize();
    Vec2 max;
    max.x = -(cameraBounds.size.width - (winSize.width / _currentScale));
    max.y = -(cameraBounds.size.height - (winSize.height / _currentScale));
    max *=  _currentScale;


//    if(newCameraPosition.x > 0)
//    {
//        newCameraPosition.x = 0;
//    }
//    else if(newCameraPosition.x < max.x)
//    {
//        newCameraPosition.x = max.x;
//    }
//    if(newCameraPosition.y > 0)
//    {
//        newCameraPosition.y = 0;
//    }
//    else if(newCameraPosition.y < max.y)
//    {
//        newCameraPosition.y = max.y;
//    }
    
    _currentCammeraPosition = newCameraPosition;
    globalPlayerLayer->setPosition(_currentCammeraPosition);
    world->getWorldCocosNode()->setPosition(_currentCammeraPosition);
    globalTestBed->updateGrids = true;
}

void GameCamera::zoom(float zoomScale, Vec2 mouseScreen)
{
    if ((_currentScale * zoomScale) <= globalVariableStore->getVariable(MinCameraScale))
    {
        zoomScale = globalVariableStore->getVariable(MinCameraScale) / _currentScale;
    }
    if ((_currentScale * zoomScale) >= globalVariableStore->getVariable(MaxCameraScale))
    {
        zoomScale = globalVariableStore->getVariable(MaxCameraScale) / _currentScale;
    }
    
    _currentScale *= zoomScale;    
    
    double minScale;
    Size winSize = Director::getInstance()->getVisibleSize();
    if(winSize.width > winSize.height)
    {
        minScale = winSize.width / cameraBounds.size.width;
    }
    else
    {
        minScale = winSize.height / cameraBounds.size.height;
    }
        
//    if(_currentScale < minScale)
//    {
//        _currentScale = minScale;
//    }
    
    Vec2 mousePositionNode = world->getWorldCocosNode()->convertToNodeSpace(mouseScreen);
    Vec2 scaledWorld = world->getWorldCocosNode()->convertToWorldSpace(mousePositionNode * zoomScale);
    Vec2 position = _currentCammeraPosition - (scaledWorld - mouseScreen);
    
    globalPlayerLayer->setScale(_currentScale);
    world->getWorldCocosNode()->setScale(_currentScale);
    
    setCameraPosition(position);
}

void GameCamera::panScreen(Vec2 delta)
{
    Vec2 position = world->getWorldCocosNode()->getPosition() - delta;

    setCameraPosition(position);
}

void GameCamera::setScreenPosition(Vec2 position)
{
    position *= WORLD_TO_GRAPHICS_SIZE;
    Size winSize = Director::getInstance()->getVisibleSize();
    position.x -= (winSize.width / (2 * _currentScale));
    position.y -= (winSize.height / (2 * _currentScale));
    setCameraPosition(-position);
}

Vec2 GameCamera::getCameraPositionWorld()
{
    Size winSize = Director::getInstance()->getVisibleSize();
    Vec2 cameraPosition = _currentCammeraPosition;
    cameraPosition.x -= (winSize.width / 2);
    cameraPosition.y -= (winSize.height / 2);
    cameraPosition = cameraPosition / _currentScale;
    cameraPosition *= ONE_OVER_WORLD_TO_GRAPHICS_SIZE;
    return cameraPosition;
}

void GameCamera::reset()
{
    _currentScale = 1.0;
    globalPlayerLayer->setScale(_currentScale);
    world->getWorldCocosNode()->setScale(_currentScale);
    setCameraPosition(Vec2());
}

void GameCamera::saveToArchive(cereal::BinaryOutputArchive & archive)
{
    archive(_currentScale);
    archive(_currentCammeraPosition);
    archive(cameraBounds);
}

void GameCamera::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    archive(_currentScale);
    archive(_currentCammeraPosition);
    archive(cameraBounds);
    
    globalPlayerLayer->setScale(_currentScale);
    world->getWorldCocosNode()->setScale(_currentScale);
    
    globalPlayerLayer->setPosition(_currentCammeraPosition);
    world->getWorldCocosNode()->setPosition(_currentCammeraPosition);
}
