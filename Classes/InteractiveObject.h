//
//  InteractiveObject.h
//  TrenchWars
//
//  Created by Paul Reed on 7/14/23.
//

#ifndef InteractiveObject_h
#define InteractiveObject_h

#include "Refs.h"

enum INTERACTIVE_OBJECT_TYPE
{
    ENTITY_INTERACTIVE_OBJECT,
    UNIT_GROUP_INTERACTIVE_OBJECT,
    POLYGON_LANDSCAPE_INTERACTIVE_OBJECT,
    EMBLEM_INTERACTIVE_OBJECT,
    TESTER_INTERACTIVE_OBJECT,
    OTHER_INTERACTIVE_OBJECT
};

class InteractiveObject : virtual public ComparableRef
{
public:
    virtual INTERACTIVE_OBJECT_TYPE getInteractiveObjectType() {return OTHER_INTERACTIVE_OBJECT;}
};

#endif /* InteractiveObject_h */
