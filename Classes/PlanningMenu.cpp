//
//  PlanningMenu.cpp
//  TrenchWars
//
//  Created by Paul Reed on 8/2/21.
//

#include "PlanningMenu.h"
#include "MenuLayer.h"
#include "UIController.h"
#include "TeamManager.h"
#include "GameEventController.h"
#include "TrenchWarsManager.h"

PlanningMenu::PlanningMenu() : AutoSizedListView()
{
    ListView::init();
    globalMenuLayer->addChild(this);
    
    setDirection(Direction::VERTICAL);
    setPosition(Vec2(240,10));
    
    setBackGroundColor(Color3B::GRAY);
    setBackGroundColorType(BackGroundColorType::SOLID);
    setBackGroundColorOpacity(125);
    setPadding(5, 5, 5, 5);
    _activePlanId = -1;
    
    Button * addPlanButton = createButton("\uE072", [=] (Ref* sender) {
        createNewPlanMenu();
       globalGameEventController->createNewPlan(globalTrenchWarsManager->getLocalPlayerId());
    }, true);
    
    pushBackCustomItem(addPlanButton);
    
    Button * noPlan = createButton("No Plan", [=](Ref* sender) {
        globalUIController->setCurrentPlan(nullptr);
        setActivePlan(nullptr);
    });
    noPlan->setName("No Plan");
    noPlan->setColor(Color3B::GREEN);
    pushBackCustomItem(noPlan);
    
    auto plans = globalTeamManager->getLocalPlayersTeam()->getPlans();
    for(auto plan : plans)
    {
        addPlan(plan);
    }
    
    setActivePlan(globalUIController->getCurrentPlan());
}

PlanningMenu * PlanningMenu::create()
{
    PlanningMenu * menu = new PlanningMenu();
    menu->autorelease();
    return menu;
}


Button * PlanningMenu::createButton(std::string name, const AbstractCheckButton::ccWidgetClickCallback & callback, bool icon)
{
    Button * button = Button::create();
    button->setTitleText(name);
    button->setTitleAlignment(TextHAlignment::LEFT, TextVAlignment::CENTER);
    if(icon)
    {
        button->setTitleFontName("icons.ttf");
    }
    else
    {
        button->setTitleFontName("arial.ttf");
    }
    button->setTitleFontSize(20);
    button->addClickEventListener(callback);
    return button;
}

void PlanningMenu::createNewPlanMenu()
{
    AutoSizedListView * listView = AutoSizedListView::create();
    listView->setLayoutType(Layout::Type::HORIZONTAL);
    listView->setRightPadding(5);
    listView->setItemsMargin(3);
    
    int emptyPlanId = 0;
    while(_planMenus.find(emptyPlanId) != _planMenus.end())
    {
        emptyPlanId++;
    }
    
    Button * planName = createButton("New Plan", [=](Ref* sender) {
    });
    planName->setName("Blank");
    listView->pushBackCustomItem(planName);
    
    _beingCreatedPlanMenus.pushBack(listView);
    pushBackCustomItem(listView);
}

void PlanningMenu::addPlan(Plan * plan)
{
    AutoSizedListView * listView;
    if(_beingCreatedPlanMenus.size() > 0)
    {
        listView = *_beingCreatedPlanMenus.begin();
        listView->removeAllItems();
        _beingCreatedPlanMenus.popFront();
    }
    else
    {
        listView = AutoSizedListView::create();
        listView->setLayoutType(Layout::Type::HORIZONTAL);
        listView->setRightPadding(5);
        listView->setItemsMargin(3);
        pushBackCustomItem(listView);
    }


    Button * planName = createButton(plan->getName(), [=](Ref* sender) {
        globalUIController->setCurrentPlan(plan);
        setActivePlan(plan);
    });
    planName->setName("Select");
    listView->pushBackCustomItem(planName);
    
    std::string executeState = plan->isExecuting() ? "\uE03F" : "\uE03D";
    
    // //// Create a button to execute or stop the plan
    Button * execute = createButton(executeState, [=](Ref* sender) {
        Button * button = (Button *) sender;
        if(plan->isExecuting())
        {
            button->setTitleText("\uE03D");
            globalGameEventController->stopPlan(plan);
        }
        else
        {
            button->setTitleText("\uE03F");
            globalGameEventController->executePlan(plan);
        }
    }, true);
    execute->setName("Execute");
    listView->pushBackCustomItem(execute);
    
    // //// Create a button to remove the plan
    Button * remove = createButton("\uE08D", [=](Ref* sender) {
        removePlan(plan);
        globalGameEventController->removePlan(plan);
    }, true);
    listView->pushBackCustomItem(remove);
    
    
    // //// Create a button to rename the plan
    Button * rename = createButton("\uE0FC", [=](Ref* sender) {
        listView->removeItem(0); //remove the old name label
        TextField * nameField = TextField::create("", "arial.ttf", 20);
        nameField->addEventListener( [=] (Ref * sender, TextField::EventType eventType)
        {
            auto test = nameField->getString();
            if(eventType == TextField::EventType::DETACH_WITH_IME)
            {
                listView->removeChild(nameField);
                globalGameEventController->renamePlan(plan, nameField->getString());
                planRenamed(plan, nameField->getString());
            }
        });
        nameField->setEnabled(true);
        nameField->setTextAreaSize(Size(50, 20));
        nameField->setContentSize(Size(50,20));
        nameField->setCursorEnabled(true);
        nameField->setAnchorPoint(Vec2::ANCHOR_MIDDLE);
        nameField->setTextColor(Color4B::WHITE);
        nameField->setPlaceHolderColor(Color4B::WHITE);
        nameField->attachWithIME();
        listView->insertCustomItem(nameField, 0);
    }, true);
    
    listView->pushBackCustomItem(rename);
    listView->forceDoLayout();
    _planMenus.insert(plan->uID(), listView);
}

void PlanningMenu::planExecuting(Plan * plan)
{
    AutoSizedListView * listView = _planMenus.at(plan->uID());
    if(listView != nullptr)
    {
        Button * button = (Button *) listView->getChildByName("Execute");
        if(button != nullptr)
        {
            button->setTitleText("\uE03F");
        }
    }
}

void PlanningMenu::planStopped(Plan * plan)
{
    AutoSizedListView * listView = _planMenus.at(plan->uID());
    if(listView != nullptr)
    {
        Button * button = (Button *) listView->getChildByName("Execute");
        if(button != nullptr)
        {
            button->setTitleText("\uE03D");
        }
    }
}

void PlanningMenu::planRenamed(Plan * plan, const std::string & newName)
{
    AutoSizedListView * listView = _planMenus.at(plan->uID());
    if(listView != nullptr)
    {
        int i = 0;
        for(auto item : listView->getItems())
        {
            if(item->getName() == "Select")
            {
                listView->removeItem(i );
                break;
            }
            i++;
        }
        Button * planName = createButton(newName, [=](Ref* sender) {
            globalUIController->setCurrentPlan(plan);
            setActivePlan(plan);
        });
        planName->setName("Select");
        if(_activePlanId == plan->uID())
        {
            planName->setColor(Color3B::GREEN);
        }
        listView->insertCustomItem(planName, 0);
    }
}

void PlanningMenu::removePlan(Plan * plan)
{
    if(_activePlanId == plan->uID())
    {
        Button * button = (Button *) getChildByName("No Plan");
        button->setColor(Color3B::GREEN);
        _activePlanId = -1;
    }
    AutoSizedListView * view = _planMenus.at(plan->uID());
    removeChild(view);
}

void PlanningMenu::setActivePlan(Plan * plan)
{
    if(_activePlanId != -1)
    {
        AutoSizedListView * view = _planMenus.at(_activePlanId);
        Button * button = (Button *) view->getChildByName("Select");
        button->setColor(Color3B::WHITE);
    }
    else
    {
        Button * button = (Button *) getChildByName("No Plan");
        button->setColor(Color3B::WHITE);
    }
    
    if(plan != nullptr)
    {
        AutoSizedListView * view = _planMenus.at(plan->uID());
        Button * button = (Button *) view->getChildByName("Select");
        button->setColor(Color3B::GREEN);
        _activePlanId = plan->uID();
    }
    else
    {
        Button * button = (Button *) getChildByName("No Plan");
        button->setColor(Color3B::GREEN);
        _activePlanId = -1;
    }
}

void PlanningMenu::clear()
{
    removeAllChildren();
    _activePlanId = -1;
}

void PlanningMenu::remove()
{
    
}
