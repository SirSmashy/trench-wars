//
//  TeamManager.hpp
//  TrenchWars
//
//  Created by Paul Reed on 6/25/23.
//

#ifndef TeamManager_h
#define TeamManager_h

#include "Command.h"
#include "Team.h"
#include "ScenarioFactory.h"
#include <cereal/archives/binary.hpp>

USING_NS_CC;

class TeamManager
{
    Vector<Team *> _teams;
    int _localTeamId;
public:
    TeamManager(Vector<TeamDefinition *> & teams);
    TeamManager(cereal::BinaryInputArchive & inputArchive);
    
    void postLoad();
    void query(float deltaTime);
    Team * getTeam(int teamId);
    Team * getLocalPlayersTeam();
    
    Team * getTeamForPlayer(int playerId);
    
    const Vector<Team *> & getTeams() {return _teams;}
    Command * getCommand(int commandId);
    void shutdown();
        
    void saveToArchive(cereal::BinaryOutputArchive & archive);
};

extern TeamManager * globalTeamManager;

#endif /* TeamManager_h */
