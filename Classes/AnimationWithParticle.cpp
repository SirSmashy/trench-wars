//
//  AnimationWithParticle.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/18/20.
//

#include "AnimationWithParticle.h"
#include "ParticleManager.h"



AnimationWithParticle::AnimationWithParticle()
{
    
}

bool AnimationWithParticle::init(EntityAnimation *animation, Size size)
{
    if(AutoSizeAnimation::init(animation, size))
    {
        _entityAnimation = animation;
        return true;
    }
    return false;
}
    
AnimationWithParticle* AnimationWithParticle::create(EntityAnimation *animation, Size size)
{
    AnimationWithParticle * newAnimation = new AnimationWithParticle();
    if(newAnimation->init(animation, size))
    {
        newAnimation->autorelease();
        return newAnimation;
    }
    CC_SAFE_DELETE(newAnimation);
    return nullptr;
}

void AnimationWithParticle::update(float t)
{
    AutoSizeAnimation::update(t);
    if(_currFrameIndex == _entityAnimation->_particleCreationFrame)
    {
        Node * target = getTarget();
        Vec2 position = getTarget()->getPosition();
        position += _entityAnimation->_particleOffset;
        globalParticleManager->requestParticle(_entityAnimation->_particleName, position, -1, _entityAnimation->_particleHeight);
    }
}
