//
//  PlayerLayer.hpp
//  TrenchWars
//
//  Created by Paul Reed on 10/4/20.
//

#ifndef PlayerLayer_h
#define PlayerLayer_h

#include "cocos2d.h"

USING_NS_CC;

class PlayerLayer: public Node
{
private:
    PlayerLayer();
    virtual bool init();
    
public:
    static PlayerLayer * create();
};

extern PlayerLayer * globalPlayerLayer;

#endif /* PlayerLayer_h */
