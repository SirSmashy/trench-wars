//
//  GameEventController.h
//  TrenchWars
//
//  Created by Paul Reed on 1/17/16.
//  Copyright © 2016 Apportable. All rights reserved.
//
#ifndef __EVENT_CONTROLLER_H__
#define __EVENT_CONTROLLER_H__

#include "cocos2d.h"
#include "Refs.h"
#include "SerializationHelpers.h"
#include "Plan.h"

USING_NS_CC;


class MapSector;
class CommandPost;
class Human;
class Unit;
class UnitGroup;
class Plan;
class Objective;
class Unit;
class Command;
class Order;

enum OBJECTIVE_ORDER_ACTION
{
    OBJECTIVE_ORDER_OCCUPY,
    OBJECTIVE_ORDER_DIG_TRENCHES,
    OBJECTIVE_ORDER_DIG_BUNKERS,
    OBJECTIVE_ORDER_BUILD_COMMAND_POST,
    OBJECTIVE_ORDER_CLEAR_AREA,
    OBJECTIVE_ORDER_ATTACK,
};

enum OBJECTIVE_ACTION
{
    OBJECTIVE_ACTION_CREATE,
    OBJECTIVE_ACTION_REMOVE,
    OBJECTIVE_ACTION_CREATE_COMMAND_POST,
    OBJECTIVE_ACTION_CREATE_TRENCH,
    OBJECTIVE_ACTION_CREATE_BUNKERS,
    OBJECTIVE_ACTION_REMOVE_TRENCH,
    OBJECTIVE_ACTION_REMOVE_BUNKERS
};

enum PLAN_ACTION
{
    PLAN_ACTION_CREATE,
    PLAN_ACTION_REMOVE,
    PLAN_ACTION_EXECUTE,
    PLAN_ACTION_STOP,
    PLAN_ACTION_RENAME
};


/* GameEventController
 * The GameEventController singleton acts as the interface between the UI and the game model
 * ClientGameEventController will issue network requests for player actions (e.g., create an objective) and tell the UI about new objects received from the server
 */
class GameEventController : public Ref
{
protected:

public:
    virtual void init();

    virtual void postLoad() = 0;
    virtual void query(float deltaTime) = 0;

    // ////////  New Event Requests ////////
    // Orders
    virtual void issueMoveOrder(Vec2 position, Unit * commandable, Plan * addToPlan) = 0;
    virtual void issueOccupyOrder(Objective * objective, Unit * commandable, Plan * addToPlan) = 0;

    virtual void issueDigTrenchesOrder(Objective * objective, Unit * commandable, Plan * addToPlan) = 0;
    virtual void issueDigBunkersOrder(Objective * objective, Unit * commandable, Plan * addToPlan) = 0;
    virtual void issueBuildCommandPostOrder(Objective * objective, Unit * commandable, Plan * addToPlan) = 0;
    virtual void issueClearAreaOrder(Objective * objective, Unit * commandable, Plan * addToPlan) = 0;
    virtual void issueAttackPositionOrder(Objective * objective, Unit * commandable, Plan * addToPlan) = 0;
    virtual void issueStopOrder(Unit * commandable, Plan * addToPlan) = 0;
    
    // Objectives
    virtual void createNewObjective(std::vector<Vec2> & points, bool enclosed, Unit * unitCreating) = 0;
    virtual void removeObjective(Objective * objective) = 0;
    
    // Command Posts
    virtual void createCommandPostInObjective(Objective * objective, Vec2 location) = 0;
    
    // Trench Lines
    virtual void createTrenchLineInObjective(Objective * objective) = 0;
    virtual void removeTrenchLineInObjective(Objective * objective) = 0;
    
    // Bunkers
    virtual void createBunkersInObjective(Objective * objective) = 0;
    virtual void removeBunkersInObjective(Objective * objective) = 0;
    
    // Plans
    virtual void createNewPlan(int playerId) = 0;
    virtual void removePlan(Plan * plan) = 0;
    virtual void executePlan(Plan * plan) = 0;
    virtual void stopPlan(Plan * plan) = 0;
    virtual void renamePlan(Plan * plan, const std::string & newName) = 0;
    
    // Team status
    virtual void updateTeamVisibility(Team * team, 
                                      std::unordered_map<ENTITY_ID, double> & sectorVisibleTime,
                                      std::unordered_set<ENTITY_ID> & sectorsNewlyHidden,
                                      std::unordered_set<ENTITY_ID> & sectorsNewlyVisible) = 0;

    
    // ///////// Handling of newly created events ///////
    // Orders
    virtual void handleEventOrderIssued(Order * order, Unit * unit, Plan * addToPlan) = 0;
    virtual void handleEventOrderComplete(Unit * entCompleting, Order * order, bool canceled = false) = 0;
    virtual void handleEventOrderCancelled(Unit * commandable) = 0;


    // Objectives
    virtual void handleEventObjectiveCreated(Objective * objective) = 0;
    virtual void handleEventObjectiveRemoved(Objective * objective) = 0;
    
    //visible
    virtual void handleEventSectorVisible(MapSector * sector) = 0;
    virtual void handleEventTeamVisibilityUpdate(Team * team, std::unordered_map<ENTITY_ID, double> & sectorVisibleTime) = 0;

    //command post related
    virtual void handleEventNewCommandPost(CommandPost * commandPost) = 0;
    virtual void handleEventCommandPostDestroyed(CommandPost * commandPost) = 0;
        
    
    //Unit related
    virtual void handleEventNewUnit(Unit * unit) = 0;
    virtual void handleEventUnitDestroyed(Unit * unit) = 0;

    //Plan related
    virtual void handleEventNewPlan(Plan * plan) = 0;
    virtual void handleEventPlanRemoved(Plan * plan) = 0;
    virtual void handleEventPlanExecuting(Plan * plan) = 0;
    virtual void handleEventPlanStopped(Plan * plan) = 0;
    virtual void handleEventPlanRenamed(Plan * plan, const std::string & newName) = 0;
    virtual void handleEventOrderRemovedFromPlan(Unit * unit, Plan * plan) =0;

    //friendly unit presence
    virtual void handleEventUnitInSector(MapSector * sector, Unit * unit) = 0;
    
    //enemy unit presence
    virtual void handleEventEnemyUnitSpotted(Unit * enemy) = 0;
    virtual void handleEventEnemyCommandPostSpotted(CommandPost *enemy) = 0;
    virtual void handleEventEnemyCanEngageSector(MapSector * sector, Human * enemy) = 0;
    virtual void handleEventEnemyArtilleryInSector(MapSector * sector) = 0;
    //////////// end low level event report functions
    ///
};

extern GameEventController * globalGameEventController;

#endif //__EVENT_CONTROLLER_H__
