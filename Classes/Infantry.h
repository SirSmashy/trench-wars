#ifndef __INFANTRY_H__
#define __INFANTRY_H__
//
//  Infantry.h
//  TrenchWars
//
//  Created by Paul Reed on 4/17/12.
//  Copyright (c) 2012 . All rights reserved.
//
#include "cocos2d.h"
#include "Human.h"
#include "List.h"
#include "EntityDefinitions.h"
#include "CrewWeapon.h"


class AttackPositionOrder;
//class CrewWeapon;

static double experienceSuppresionReductionRate [5] = {1.0, 1.5, 2.0 , 2.5 , 3.0};

struct InfantryNetworkUpdate
{
    bool firing = false;
    ENTITY_ID currentWeapon = 0;
    ENTITY_ID crewWeapon = 0;
    
    Vec2 targetPosition = Vec2::ZERO;
    float targetFireAngle = 0;
    int targetHeight = 0;
    Vec2 firePosition = Vec2::ZERO;
    int currentAmmoReserve = 0;
};

//
class Infantry : public Human
{
private:
    // Weapon target related
    Vec2 _targetPosition;
    float _targetFireAngle;
    int _targetHeight;
    Vec2 _firePosition;
    
    // Attack state
    bool _firing;
    float _currentRecoil;
    Vec2 _fireOffsets[8];

    
    // Ammo
    int   _maxAmmoReserve;
    int   _currentAmmoReserve;
    
    // Weapons
    Weapon * _currentWeapon;  //weapon in the actors hand
    Vector<Weapon *> _weaponList;
    CrewWeapon * _crewWeapon;
    RefChangeTrackingVariable<CrewWeapon> _newCrewWeapon;
    
    // Targetting
    CountTo checkChargeCounter;
    CountTo lookForTargetCounter;
    float   _targetAquireSpeed;
    float _acquireTargetTimeModifier;
    
    double  _chargeRadius;
    
    Unit * _owningUnit; //The unit this infantry belongs to
    
    InfantryNetworkUpdate _infantryNetworkUpdate;
            
private:
    void fireWeapon();
    
    ///////
    virtual void updateReloadGoal(ReloadWeaponGoal * goal, float deltaTime);
    virtual void updateAttackPositionGoal(AttackPositionGoal * goal, float deltaTime);
    virtual void updateAttackTargetGoal(AttackTargetGoal * goal, float deltaTime);
    
    // Target related
    void chooseWeaponForTarget(Vec2 targetLocataion);
    void lookForTarget();
    void clearTarget();    
    
    // Weapon related
    void updateCurrentWeapon();
    Vec2 getWeaponFirePosition();
    Vec2 getPredictedTargetPosition(Vec2 initialTargetPosition, Vec2 targetVelocity);
    double getFiringAngle(Vec2 targetPosition);
    void fireCurrentWeaponAtTarget(Human * target, int height);
    bool isFriendNearLocation(Vec2 targetLocation);
    void addAmmoToWeapon();
    
    // Goal related
    virtual void handleGoalComplete(Goal * goal);

    
CC_CONSTRUCTOR_ACCESS :
    Infantry();
    ~Infantry();
    virtual bool init(InfantryDefinition * definition, Vec2 position, Command * command, Unit * unit);
        
public:
    
    static Infantry * create(InfantryDefinition * definition, Vec2 position, Command * command, Unit * unit);
    static Infantry * create(const std::string & definitionName, Vec2 position, Command * command, Unit * unit);
    static Infantry * create();

    virtual ENTITY_TYPE getEntityType() const {return INFANTRY;}


    virtual void query(float deltaTime);
    virtual void act(float deltaTime);
    void aimAtTarget(AttackTargetGoal * targetGoal);

    float getMinWeaponRange();
    float getMaxWeaponRange();
    
    int getCurrentAmmoReserve() {return _currentAmmoReserve;}
    int getMaxAmmoReserve() {return _maxAmmoReserve;}
    
    Human * getCurrentTarget();
    void addCrewWeapon(CrewWeapon * weapon);
    
    void startFiringCurrentWeapon(double angle, Vec2 position, int height);
    void stopFiring() {_firing = false;}
    
    virtual void startedMoving();
    virtual void goalPositionReached();
    
    
    void enemySpotted(DIRECTION direction, float distance);
    
    Unit * getOwningUnit() {return _owningUnit;}
    virtual Vec2 getUnitPosition();

    virtual CollisionCell * getOptimumCellAroundCell(CollisionCell * center);
    
    virtual int hasUpdateToSendOverNetwork();
    virtual void saveNetworkUpdate(cereal::BinaryOutputArchive & archive);
    virtual void updateFromNetwork(cereal::BinaryInputArchive & archive);

    virtual void die();
    virtual void removeFromGame();
        
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
    
    virtual void populateDebugPanel(GameMenu * menu, Vec2 location);

};

#endif //__INFANTRY_H__
