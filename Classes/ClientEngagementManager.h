//
//  ClientEngagementManager.hpp
//  TrenchWars
//
//  Created by Paul Reed on 12/4/23.
//

#ifndef ClientEngagementManager_h
#define ClientEngagementManager_h

#include "EngagementManager.h"
 

class ClientEventController;
class ClientEngagementManager : public EngagementManager
{
    int _lastQueryUpdateReceived;
    bool _waitingForServer;

    void parseFullGameState(cereal::BinaryInputArchive & iArchive);
    void parseEntityAdditionsRemovals(GameMessage * message);
    void parseEntityUpdates(GameMessage * message);
    void parseTeamUpdate(GameMessage * message);
    
    void sendClientUpdate();
    
    Map<long long, GameMessage *> _frameNumberToEntityUpdateMessage;
    Map<long long, GameMessage *> _frameNumberToTeamUpdateMessage;
    Map<long long, GameMessage *> _frameNumberToEntityAdditionsRemovalMessage;

    
    CLIENT_STATE _state;
    
    ClientEventController * _clientEventController;


public:
    
    virtual bool init(const unsigned char * mapData, size_t mapDataSize, Vector<TeamDefinition *> & teams);
    static ClientEngagementManager * create(const unsigned char * mapData, size_t mapDataSize, Vector<TeamDefinition *> & teams);
    
    virtual void update(float deltaTime);
    virtual void parseNetworkMessage(GameMessage * message);
    
    void playerJoinedGame(Player * player);
    void playerLeftGame(int playerId);
};

#endif /* ClientEngagementManager_h */
