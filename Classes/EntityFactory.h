#ifndef __ENTITY_FACTORY_H__
#define __ENTITY_FACTORY_H__

#include "EntityDefinitions.h"
#include "PhysicalEntity.h"
#include "Infantry.h"
#include "Projectile.h"
#include "CrewWeapon.h"
#include "CrewWeaponUnit.h"
#include "Bunker.h"
#include "CommandPost.h"
#include "UnitGroup.h"
#include "Courier.h"
#include "PolygonLandscapeObject.h"
#include "SpriteBatchGroup.h"

//
//  EntityManager.h
//  TrenchWars
//
//  Created by Paul Reed on 5/2/12.
//  Copyright (c) 2012 . All rights reserved.
//

class MovingEntitiesLayer;
class EntityDefinitionXMLParser;
class SpriteDefinitionXMLParser;
class WeaponDefinitionXMLParser;
class ProjectileDefinitionXMLParser;
class PolygonLandscapeObjectXMLParser;
class FormationDefinitionXMLParser;

class EntityFactory : public Ref
{
    EntityDefinitionXMLParser * entityDefinitionParser;
    WeaponDefinitionXMLParser * weaponDefinitionParser;
    ProjectileDefinitionXMLParser * projectileDefinitionParser;
    SpriteDefinitionXMLParser * spriteDefinitionParser;
    PolygonLandscapeObjectXMLParser * PolygonLandscapeObjectParser;
    FormationDefinitionXMLParser * formationDefinitionParser;
    
    Map<size_t,EntityAnimationSet *>  entityBatches;
    Map<size_t,PolygonLandscapeObjectDefiniton *>  landscapeObjectTypes;
    Map<size_t,PhysicalEntityDefinition *>  physicalEntityTypes;
    Map<size_t,FormationDefinition *> formationTypes;
    Map<size_t,UnitDefinition *> unitTypes;
    Map<size_t,CloudDefinition *> cloudTypes;
    std::vector<std::string> unitNames;
    std::vector<std::string> formationNames;
    std::vector<std::string> physicalEntityNames;
    std::vector<std::string> cloudNames;

    Map<size_t,WeaponDefinition *> weaponTypes;
    
public:
    EntityFactory();
        
    EntityAnimationSet * createEntityAnimationSet(const std::string & name, const std::string & plistFile);
    EntityAnimationSet * getEntityAnimationSet(const std::string & name);
    void createSpriteBatches();

    void addPolygonLandscapeDefinition(PolygonLandscapeObjectDefiniton * def);
    void addPhysicalEntityDefinition(PhysicalEntityDefinition * entDef);
    void addUnitDefinition(UnitDefinition * unitDef);
    void addWeaponDefinition(WeaponDefinition * weaponDef);
    void addFormationDefinition(FormationDefinition * formationDef);
    void addCloudDefinition(CloudDefinition * cloudDef);
    
    PhysicalEntityDefinition * getPhysicalEntityDefinitionWithName(const std::string & name);
    UnitDefinition * getUnitDefinitionWithName(const std::string & name);
    WeaponDefinition * getWeaponDefinitionWithName(const std::string & name);
    PolygonLandscapeObjectDefiniton * getPolygonLandscapeObjectDefinitionWithName(const std::string & name);
    
    PhysicalEntityDefinition * getPhysicalEntityDefinitionWithHashedName(size_t hashedName);
    UnitDefinition * getUnitDefinitionWithHashedName(size_t hashedName);
    WeaponDefinition * getWeaponDefinitionWithHashedName(size_t hashedName);
    PolygonLandscapeObjectDefiniton * getPolygonLandscapeObjectDefinitionWithHashedName(size_t hashedName);
    
    FormationDefinition * getFormationDefinition(const std::string & name);
    CloudDefinition * getCloudDefinition(const std::string & name);
    ANIMATION_TYPE nameToAnimationType(const std::string & name);
    DIRECTION nameToDirection(const std::string & name);

//    const Map<std::string,EntityAnimationSet *> & getAllEntityBatches() {return entityBatches;}
    
    std::vector<std::string> getAllUnitNames() const;
    std::vector<std::string> getAllPhysicalEntityNames() const;
    std::vector<std::string> getAllFormationNames() const;
    std::vector<std::string> getAllCloudNames() const;
};

extern EntityFactory * globalEntityFactory;

#endif //__ENTITY_FACTORY_H__
