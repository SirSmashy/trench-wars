//
//  ProposedEntity.cpp
//  TrenchWars
//
//  Created by Paul Reed on 2/19/23.
//

#include "ProposedEntity.h"
#include "GameStatistics.h"
#include "MultithreadedAutoReleasePool.h"
#include "EntityManager.h"
#include "EntityFactory.h"
#include "Objective.h"
#include "Command.h"
#include "EngagementManager.h"
#include "TeamManager.h"
#include <cereal/types/string.hpp>

ProposedEntity::~ProposedEntity()
{
}

ProposedEntity::ProposedEntity() : PhysicalEntity()
{
    
}

void ProposedEntity::postLoad()
{
    _sprite->setUnderground(true);
}

bool ProposedEntity::isWorkComplete()
{
    if(_workUntilCreated <= 0)
    {
        return true;
    }
    return false;
}

void ProposedEntity::addWork(float deltaTime)
{
    _workMutex.lock();
    _workUntilCreated -= deltaTime;
    if(_workUntilCreated <= 0)
    {
        workComplete();
    }
    _workMutex.unlock();
}

void ProposedEntity::completeAllWork()
{
    addWork(10000000000000000);
}

void ProposedEntity::populateDebugPanel(GameMenu * menu, Vec2 location)
{
    PhysicalEntity::populateDebugPanel(menu,location);

    std::ostringstream outStream;
    outStream << std::fixed;
    outStream.precision(0);
    
    outStream.str("");
    outStream << "Work: " << _workUntilCreated;
    menu->addText(outStream.str());
    
    outStream.str("");
    outStream << "Cover: " << _inTerrain.getCoverValue();
    menu->addText(outStream.str());
    
    outStream.str("");
    outStream << " Hardness: " << _inTerrain.getHardness();
    menu->addText(outStream.str());
    
    outStream.str("");
    outStream << " Height: " << _inTerrain.getHeight();
    menu->addText(outStream.str());
    
    outStream.precision(5);
    
    outStream.str("");
    outStream << " Z Pos: " << _sprite->getSprite()->getPositionZ();
    menu->addText(outStream.str());
    
    outStream.str("");
    outStream << " Z Ind: " << _sprite->getSprite()->getZOrder();
    menu->addText(outStream.str());
    
}

////////////// TRENCH ///////////
bool ProposedTrench::init(Vec2 position, PhysicalEntityDefinition * definition, ObjectiveObjectCollection * onCompleteCollection)
{
    if(PhysicalEntity::init(definition,position, false))
    {
        _onCompleteCollection = onCompleteCollection;
        _workUntilCreated = definition->_workTime * globalRandom->randomDouble(.66,1.33); // FIX magic number... this is stupid
        
        if(_workUntilCreated <= 0)
        {
            workComplete();
        }
        else
        {
            // FIX, this doesn't belong here
            _sprite->setColor(Color3B(200, 200, 200));
            _sprite->setOpacity(125);
        }
        
    }
    return true;
}

ProposedTrench * ProposedTrench::create(Vec2 position, ObjectiveObjectCollection * onCompleteCollection)
{
    PhysicalEntityDefinition * definition = globalEntityFactory->getPhysicalEntityDefinitionWithName("trench");
    ProposedTrench * newLand = new ProposedTrench();
    if(newLand && newLand->init(position, definition, onCompleteCollection))
    {
        globalAutoReleasePool->addObject(newLand);
        return newLand;
    }
    CC_SAFE_DELETE(newLand);
    return nullptr;
}

ProposedTrench * ProposedTrench::create(Vec2 position, PhysicalEntityDefinition * definition, ObjectiveObjectCollection * onCompleteCollection)
{
    ProposedTrench * newLand = new ProposedTrench();
    if(newLand && newLand->init(position, definition, onCompleteCollection))
    {
        globalAutoReleasePool->addObject(newLand);
        return newLand;
    }
    CC_SAFE_DELETE(newLand);
    return nullptr;
}

ProposedTrench * ProposedTrench::create()
{
    ProposedTrench * newLand = new ProposedTrench();
    globalAutoReleasePool->addObject(newLand);
    return newLand;
}

void ProposedTrench::workComplete()
{
    _onCompleteCollection->removeEntity(this);
    StaticEntity * trench = StaticEntity::create("trench", _position);
    _onCompleteCollection->addEntity(trench);
    removeFromGame();
}

void ProposedTrench::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    PhysicalEntity::loadFromArchive(archive);
    archive(_workUntilCreated);
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        _onCompleteCollection = dynamic_cast<ObjectiveObjectCollection *>(globalEntityManager->getEntity(thingId));
    }
    if(_workUntilCreated <= 0)
    {
        workComplete();
    }
    else
    {
        // FIX, this doesn't belong here
        _sprite->setColor(Color3B(200, 200, 200));
        _sprite->setOpacity(125);
    }
}



void ProposedTrench::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    PhysicalEntity::saveToArchive(archive);
    archive(_workUntilCreated);
    archive(_onCompleteCollection->uID());
}


////////////// BUNKER ///////////
bool ProposedBunker::init(Vec2 position, PhysicalEntityDefinition * definition, Size bunkerSize, DIRECTION bunkerFacing,  ObjectiveObjectCollection * onCompleteCollection)
{
    if(PhysicalEntity::init(definition,position, false))
    {
        _bunkerSize = bunkerSize;
        _bunkerFacing = bunkerFacing;
        _onCompleteCollection = onCompleteCollection;
        _workUntilCreated = definition->_workTime * globalRandom->randomDouble(.66,1.33); // FIX magic number... this is stupid
        
        if(_workUntilCreated <= 0)
        {
            workComplete();
        }
        else
        {
            // FIX, this doesn't belong here
            _sprite->setColor(Color3B(200, 200, 200));
            _sprite->setOpacity(125);
        }
        
    }
    return true;
}

ProposedBunker * ProposedBunker::create(Vec2 position, const std::string & definitionName, Size bunkerSize, DIRECTION bunkerFacing,  ObjectiveObjectCollection * onCompleteCollection)
{
    PhysicalEntityDefinition * definition = globalEntityFactory->getPhysicalEntityDefinitionWithName("definitionName");
    ProposedBunker * newLand = new ProposedBunker();
    if(newLand && newLand->init(position, definition, bunkerSize,bunkerFacing, onCompleteCollection))
    {
        globalAutoReleasePool->addObject(newLand);
        return newLand;
    }
    CC_SAFE_DELETE(newLand);
    return nullptr;
}

ProposedBunker * ProposedBunker::create(Vec2 position, PhysicalEntityDefinition * definition, Size bunkerSize, DIRECTION bunkerFacing,  ObjectiveObjectCollection * onCompleteCollection)
{
    ProposedBunker * newLand = new ProposedBunker();
    if(newLand && newLand->init(position, definition, bunkerSize,bunkerFacing, onCompleteCollection))
    {
        globalAutoReleasePool->addObject(newLand);
        return newLand;
    }
    CC_SAFE_DELETE(newLand);
    return nullptr;
}

ProposedBunker * ProposedBunker::create()
{
    ProposedBunker * newLand = new ProposedBunker();
    globalAutoReleasePool->addObject(newLand);
    return newLand;
}

void ProposedBunker::workComplete()
{
    _onCompleteCollection->removeEntity(this);
    Bunker * bunker = Bunker::create(_position, _bunkerSize, _facing);
    _onCompleteCollection->addEntity(bunker);
    removeFromGame();
}

void ProposedBunker::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    PhysicalEntity::loadFromArchive(archive);
    archive(_workUntilCreated);
    archive(_bunkerSize);
    int facing;
    archive(facing);
    _bunkerFacing = (DIRECTION) facing;
    
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        _onCompleteCollection = dynamic_cast<ObjectiveObjectCollection *>(globalEntityManager->getEntity(thingId));
    }
    
    if(_workUntilCreated <= 0)
    {
        workComplete();
    }
    else
    {
        // FIX, this doesn't belong here
        _sprite->setColor(Color3B(200, 200, 200));
        _sprite->setOpacity(125);
    }
}

void ProposedBunker::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    PhysicalEntity::saveToArchive(archive);
    archive(_workUntilCreated);
    archive(_bunkerSize);
    archive((int) _bunkerFacing);
    archive(_onCompleteCollection->uID());
}




////////////// COMMAND POST ///////////
bool ProposedCommandPost::init(Vec2 position, PhysicalEntityDefinition * definition, Command * command, Objective * objective)
{
    if(definition == nullptr)
    {
        return false;
    }
    if(PhysicalEntity::init(definition,position, false))
    {
        _command = command;
        _objective = objective;
        _workUntilCreated = definition->_workTime * globalRandom->randomDouble(.66,1.33); // FIX magic number... this is stupid
        
        if(_workUntilCreated <= 0)
        {
            workComplete();
        }
        else
        {
            // FIX, this doesn't belong here
            _sprite->setColor(Color3B(200, 200, 200));
            _sprite->setOpacity(125);
        }
        
    }
    return true;
}

ProposedCommandPost * ProposedCommandPost::create(Vec2 position, const std::string & definitionName, Command * command, Objective * objective)
{
    PhysicalEntityDefinition * definition = globalEntityFactory->getPhysicalEntityDefinitionWithName(definitionName);

    ProposedCommandPost * newLand = new ProposedCommandPost();
    if(newLand && newLand->init(position, definition, command, objective))
    {
        globalAutoReleasePool->addObject(newLand);
        return newLand;
    }
    CC_SAFE_DELETE(newLand);
    return nullptr;
}

ProposedCommandPost * ProposedCommandPost::create(Vec2 position, PhysicalEntityDefinition * definition, Command * command, Objective * objective)
{
    ProposedCommandPost * newLand = new ProposedCommandPost();
    if(newLand && newLand->init(position, definition, command, objective))
    {
        globalAutoReleasePool->addObject(newLand);
        return newLand;
    }
    CC_SAFE_DELETE(newLand);
    return nullptr;
}

ProposedCommandPost * ProposedCommandPost::create()
{
    ProposedCommandPost * newLand = new ProposedCommandPost();
    globalAutoReleasePool->addObject(newLand);
    return newLand;
}

void ProposedCommandPost::workComplete()
{
    CommandPost * post = CommandPost::create(_position, _command);
    _objective->setCommandPost(post);
    removeFromGame();
}

void ProposedCommandPost::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    PhysicalEntity::loadFromArchive(archive);
    archive(_workUntilCreated);
    int command;
    archive(command);
    _command = globalTeamManager->getCommand(command);
    
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        _objective = dynamic_cast<Objective *>(globalEntityManager->getEntity(thingId));
    }
    else
    {
        _objective = nullptr;
    }
    
    if(_workUntilCreated <= 0)
    {
        workComplete();
    }
    else
    {
        // FIX, this doesn't belong here
        _sprite->setColor(Color3B(200, 200, 200));
        _sprite->setOpacity(125);
    }
}

void ProposedCommandPost::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    PhysicalEntity::saveToArchive(archive);
    archive(_workUntilCreated);
    archive(_command->getCommandID());
    archive(_objective->uID());
}
