//
//  CocosObjectManager.cpp
//  TrenchWars
//
//  Created by Paul Reed on 6/19/23.
//

#include "CocosObjectManager.h"

CocosObjectManager * globalCocosObjectManager;

CocosObjectManager::CocosObjectManager()
{
    globalCocosObjectManager = this;
}


void CocosObjectManager::requestCocosObjectCreation(CocosProxy * proxy)
{
    _proxyMutex.lock();
    _proxiesToCreate.pushBack(proxy);
    _proxyMutex.unlock();

}

void CocosObjectManager::requestCocosObjectUpdate(CocosProxy * proxy)
{
    _proxyMutex.lock();
    _proxiesToUpdate.insert(proxy);
    _proxyMutex.unlock();

}

void CocosObjectManager::requestCocosObjectRemoval(CocosProxy * proxy)
{
    _proxyMutex.lock();
    _proxiesToRemove.pushBack(proxy);
    _proxyMutex.unlock();

}


void CocosObjectManager::updateProxies()
{
    AnimatedSprite * sprite;
    for(auto proxy : _proxiesToCreate)
    {
        proxy->createCocosObject();
        _cocosObjectProxies.insert(proxy->uID(), proxy);
    }
    _proxiesToCreate.clear();
    
    for(auto proxy : _proxiesToRemove)
    {
        _cocosObjectProxies.erase(proxy->uID());
        _proxiesToUpdate.erase(proxy);
    }
    _proxiesToRemove.clear();
    
    std::vector<CocosProxy *> stopUpdating;
    stopUpdating.reserve(_proxiesToUpdate.size());
    
    for(auto sprite : _proxiesToUpdate)
    {
        if(!sprite->updateCocosObject())
        {
            stopUpdating.push_back(sprite);
        }
    }
    
    for(auto sprite : stopUpdating)
    {
        _proxiesToUpdate.erase(sprite);
    }
}
