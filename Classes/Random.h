//
//  RandomNumbers.h
//  TrenchWars
//
//  Created by Paul Reed on 8/1/22.
//
// Simple random int generator based on: https://en.wikipedia.org/wiki/Permuted_congruential_generator#Example_code

#ifndef RandomNumbers_h
#define RandomNumbers_h

#include "Refs.h"

class RandomNumbers : public Ref
{
    uint64_t mcg_state = 0xcafef00dd15ea5e5u;    // Must be odd
    uint64_t const multiplier = 6364136223846793005u;
    
    RandomNumbers();
    
public:
    
    static RandomNumbers * create();
    
    uint32_t randomNumber(void);
    int randomInt(int min, int  max); //inclusive of min, exclusive of max
    double randomDouble(double min, double max); //inclusive of min, exclusive of max
};

extern RandomNumbers * globalRandom;
#endif /* RandomNumbers_h */
