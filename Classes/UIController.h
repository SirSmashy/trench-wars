//
//  UIController.hpp
//  TrenchWars
//
//  Created by Paul Reed on 12/27/23.
//

#ifndef UIController_h
#define UIController_h

#include "cocos2d.h"
#include "Refs.h"

#include "CircularMenu.h"
#include "OverlayDrawer.h"
#include "EmblemManager.h"
#include "UnitGroupDrawer.h"
#include "Emblem.h"
#include "ObjectiveDrawer.h"
#include "GameMenu.h"
#include "InteractiveObjectHighlighter.h"
#include "CommandPostCreationDrawer.h"
#include "ObjectiveInfoDisplay.h"
#include "GameCamera.h"
#include "Plan.h"
#include "OrderDrawer.h"
#include "Order.h"
#include "Command.h"
#include "HistoricalViewSprite.h"


class PrimativeDrawer;
class InputActionHandler;
class PlanningMenu;
class PathFindingTester;
class VisibilityTester;

class UIController : public Ref
{
protected:
    bool _planningMode;
    int testValue;
    Plan * _currentPlan;
    Map<ENTITY_ID, UnitGroup *> _unitGroupsMap;
    std::unordered_map<ENTITY_ID, ENTITY_ID> _unitToActiveOrderMap;
    
    GameMenu * _entityMenu;
    ComparableRef * _lastDropTarget;
    CommandPostCreationDrawer * _commandPostCreationDrawer;
    LandscapeObjectiveBuilder * _landscapeObjectiveBuilder;
    GameCamera * _camera;
    InputActionHandler * _inputActionHandler;
    PlanningMenu * _planningMenu;
    OrderDrawer * _orderDrawer;
    PrimativeDrawer * _primativeDrawer;
    HistoricalViewSprite * _teamVisibilitySprite;
    std::unordered_map<Vec2, unsigned char, Vec2Hash, Vec2Compare> _sectorVisibilityMap;

    Map<ENTITY_ID, ObjectiveInfoDisplay *> _objectiveInfos;
    
    Vector<Tester *> _testers;
    
    void init();
    void drawPositioningBoundsForUnit(Unit * unit, DIRECTION orientation);
    
    bool dumbTest;

public:
    
    static UIController * create();
    
    void postLoad();
    void update(float deltaTime);
    
    // FIX make this private later
    /////// UI Variables
    Vec2 positioningBounds[4];
    
    bool togglePlanningMode();
    void setTestValue(int value) {testValue = value;}

    // Modification and creation of plans
    void setCurrentPlan(Plan * plan);
    void addPlan(Plan * plan);
    void removePlan(Plan * plan);
    Plan * getCurrentPlan() {return _currentPlan;}


    ///////////////////////// low level sector report functions  //////////////////
    //
    //visible
    void reportSectorVisible(MapSector * sector);
    void reportTeamVisibilityUpdate(std::unordered_map<ENTITY_ID, double> & sectorVisibleTime);
    //command post related
    void reportNewCommandPost(CommandPost * commandPost);
    void reportCommandPostDestroyed(CommandPost * commandPost);
    
    //Objective related
    void reportNewObjective(Objective * objective);
    void reportObjectiveRemoved(Objective * objective);
    void setObjectiveName(const std::string & name);

    //Unit related
    void reportNewUnit(Unit * unit);
    void reportUnitDestroyed(Unit * unit);
    void reportUnitIssuedOrder(Order * order, Unit * unit);
    void reportOrderComplete(Unit * entCompleting, Order * order, bool failed = false);
    void reportOrderCancelled(Unit * unit);
    
    //Plan related
    void reportNewPlan(Plan * plan);
    void reportPlanExecuting(Plan * plan);
    void reportPlanStopped(Plan * plan);
    void reportOrderAddedToPlan(Order * order, Unit * unit, Plan * plan);
    void reportOrderRemovedFromPlan(Unit * unit, Plan * plan);
    void reportPlanRenamed(Plan * plan, const std::string & newName);
    void reportPlanRemoved(Plan * plan);
    
    // Unit Groups
    void reportNewUnitGroup(UnitGroup * newGroup);
    UnitGroup * getUnitGroup(ENTITY_ID groupId);
    const Map<ENTITY_ID, UnitGroup *> & getUnitGroups() {return _unitGroupsMap;}
    void reportUnitGroupRemoved(UnitGroup * groupRemoved);

    //friendly unit presence
    void reportUnitInSector(MapSector * sector, Unit * unit);
    void reportCourierTransportingOrder(Order * order, Unit * destination);
    
    //enemy unit presence
    void reportEnemyUnitSpotted(Unit * enemy);
    void reportEnemyCommandPostSpotted(CommandPost *enemy);
    void reportEnemyCanEngageSector(MapSector * sector, Human * enemy);
    void reportEnemyArtilleryInSector(MapSector * sector);
    //////////// end low level sector report functions
    
    void populateContextMenu(Emblem * emblem, Vec2 location);
    void showTestMenu(Vec2 location);
        
    // Testers
    void createVisibilityTester(Vec2 position);
    void createPathFindingTester(Vec2 position);

    Tester * getTesterAtPoint(Vec2 point);
    void removeTester(Tester * tester);
    
    
    void toggleHistoricalView();
    void captureWorldAtPoint(Vec2 point);

    
    GameCamera * getCamera() {return _camera;}
    
    ObjectiveInfoDisplay * objectiveInfoForObjective(Objective * objective);
    
    void clearDrawer();
    void clearEntityMenu();
};


extern  UIController * globalUIController;
#endif /* UIController_h */
