//
//  NetworkClient.hpp
//  TrenchWars
//
//  Created by Paul Reed on 3/11/24.
//

#ifndef NetworkClient_h
#define NetworkClient_h

#include "NetworkInterface.h"

class NetworkClient : public NetworkInterface
{
    HSteamNetConnection _connectionToServer;
    
public:
    NetworkClient();
    virtual void connectToServer(const std::string & serverIp);
    virtual void OnSteamNetConnectionStatusChanged( SteamNetConnectionStatusChangedCallback_t *connectionInfo );
    virtual void sendPreparedDataToPlayer(MessageBuffer * buffer, int playerId = 0);
    virtual void pollIncomingMessages();
};

#endif /* NetworkClient_h */
