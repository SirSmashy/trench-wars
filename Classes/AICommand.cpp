//
//  AICommand.m
//  TrenchWars
//
//  Created by Paul Reed on 1/17/16.
//  Copyright © 2016 Apportable. All rights reserved.
//

#include "AICommand.h"
#include "EngagementManager.h"
#include "MapSector.h"
#include "MapSectorAnalysis.h"
#include "VectorMath.h"
#include "PrimativeDrawer.h"
#include "PlanExecution.h"
#include "BackgroundTaskHandler.h"
#include "AIStrategy.h"
#include "Order.h"
#include "GameVariableStore.h"
#include "PlayerLayer.h"
#include "Objective.h"
#include "EntityFactory.h"
#include "SectorGrid.h"
#include "TeamManager.h"

#define resetEnemySetsFrames 100
#define THINKABOUTPLANSFRAMES 15

StrategyTypeUnit::StrategyTypeUnit(AI_GOAL_TYPE type)
{
    _typeOfStrategy = type;
}

AICommand::AICommand()
{
    
}

void AICommand::init(Team * owningTeam, CommandDefinition * definition)
{
    Command::init(owningTeam, definition);
    resetEnemySetsCounter = resetEnemySetsFrames;
    queryAboutGoalsCounter = -THINKABOUTPLANSFRAMES;
    
    strategyTypeUnits.insert(BOMBARD, new StrategyTypeUnit(BOMBARD));
    strategyTypeUnits.insert(BUILD_COMMAND_POST, new StrategyTypeUnit(BUILD_COMMAND_POST));
    strategyTypeUnits.insert(OCCUPY, new StrategyTypeUnit(OCCUPY));
    strategyTypeUnits.insert(ATTACK, new StrategyTypeUnit(ATTACK));
    strategyTypeUnits.insert(RETREAT_FROM, new StrategyTypeUnit(RETREAT_FROM));
    strategyTypeUnits.insert(FORITY, new StrategyTypeUnit(FORITY));
    
    sectorsToAnalyzeTypeUnits.emplace(BOMBARD, new Vector<MapSectorAnalysis *>());
    sectorsToAnalyzeTypeUnits.emplace(BUILD_COMMAND_POST, new Vector<MapSectorAnalysis *>());
    sectorsToAnalyzeTypeUnits.emplace(OCCUPY, new Vector<MapSectorAnalysis *>());
    sectorsToAnalyzeTypeUnits.emplace(ATTACK, new Vector<MapSectorAnalysis *>());
    sectorsToAnalyzeTypeUnits.emplace(RETREAT_FROM, new Vector<MapSectorAnalysis *>());
    sectorsToAnalyzeTypeUnits.emplace(FORITY, new Vector<MapSectorAnalysis *>());
    
    strategyIDCounter = 0;
    
    executionStateNames.push_back("not");
    executionStateNames.push_back("exe");
    executionStateNames.push_back("com");
    executionStateNames.push_back("hol");
    executionStateNames.push_back("fail");
    executionStateNames.push_back("nil");
    
    goalTypeNames.push_back("BUILD");
    goalTypeNames.push_back("OCC");
    goalTypeNames.push_back("ATK");
    goalTypeNames.push_back("BOMB");
    goalTypeNames.push_back("RETREAT");
    goalTypeNames.push_back("FORT");
    
    aiFilePointer = fopen("Users/paulreed/Desktop/aiDebug.txt", "w+");
}

AICommand * AICommand::create(Team * owningTeam, CommandDefinition * definition)
{
    AICommand * newController = new AICommand();
    newController->init(owningTeam,definition);
    newController->autorelease();
    return newController;
}


void AICommand::query(float deltaTime)
{
    queryAboutGoalsCounter++;
    if(queryAboutGoalsCounter == THINKABOUTPLANSFRAMES)
    {
        queryAboutGoalsCounter = 0;
        // queryAboutGoals();
    }
}

float AICommand::getStrategyDeleteThreshold(StrategyTypeUnit * typeUnit)
{
    //obviously these values will be different later
    switch(typeUnit->_typeOfStrategy)
    {
        case BUILD_COMMAND_POST:
            return typeUnit->averageValue;
        case RETREAT_FROM:
            return typeUnit->averageValue;
        case BOMBARD:
            return typeUnit->averageValue;
        case OCCUPY:
            return typeUnit->averageValue;
        case ATTACK:
            return typeUnit->averageValue;
        case FORITY:
            return typeUnit->averageValue;
            
    }
    return 0;
}


Unit *  AICommand::selectBestUnitForStrategy(AIStrategy * strategy)
{   // lol, this is probably supposed to do a bit more
    Unit * unit = unitsNotInStrategy.front();
    unitsNotInStrategy.erase(0);
    return unit;
}

// dammit I should have commented WTF this thing is doing
void AICommand::addUnitsToExistingExecutions()
{
    int totalImportance = 0;
    
    // order the strategies by their importance
    std::sort (strategiesThatWantUnits.begin(), strategiesThatWantUnits.end(), [] (AIStrategy * first, AIStrategy * second) -> bool
               {
                  return first->importance > second->importance;
               });
    
    //strategies that are important enough that units should start being allocated to them
    Vector<AIStrategy *> possibleStratigies(strategiesThatWantUnits);
    // There might be more strategies than units available
    // remove the lower importance strategies
    int strategiesToRemove = strategiesThatWantUnits.size() - unitsNotInStrategy.size();
    
    if(possibleStratigies.size() < strategiesToRemove)
    {
        return;
    }
    
    while(strategiesToRemove > 0)
    {
        possibleStratigies.erase(possibleStratigies.end());
        strategiesToRemove--;
    }
    
    for(AIStrategy * strategy : possibleStratigies)
    {
        totalImportance += strategy->importance;
    }
    
    totalImportance /= possibleStratigies.size();
    
    for(AIStrategy * strategy : possibleStratigies)
    {
        if(unitsNotInStrategy.size() == 0)
            return;
        //get the goal
        int unitsToDistribute = ((strategy->importance) / totalImportance) * unitsNotInStrategy.size();
        if(unitsToDistribute < 1)
        {
            unitsToDistribute = 1;
        }
        else if(unitsToDistribute > strategy->execution->numberOfUnitsNeeded)
        {
             unitsToDistribute = strategy->execution->numberOfUnitsNeeded;
        }
        for(int i = 0; i < unitsToDistribute; i++)
        {
            if(unitsNotInStrategy.size() == 0)
                break;
            Unit * selectedUnit = selectBestUnitForStrategy(strategy);
            strategy->execution->addUnit(selectedUnit);
            strategyForUnitMap.insert(selectedUnit->uID(), strategy);
        }
    }
}

void AICommand::deleteStrategy(AIStrategy * strategy)
{
    
    StrategyTypeUnit * typeUnit = strategyTypeUnits.at(strategy->_goalType);
    
    _strategyLockMutex.lock();
    strategies.eraseObject(strategy);
    strategiesToUpdate.eraseObject(strategy);
    typeUnit->strategies.eraseObject(strategy);
    _strategyLockMutex.unlock();

        
    if(strategy->execution != nullptr)
    {
        strategy->execution->deleteExecution();
        strategy->execution = nullptr;
    }
    strategy->deleteStrategy();
}

void AICommand::drawGoalInfo(AIStrategy * strategy)
{
    float red;
    float green;
    float blue;
    int radius;
    int riskRadius;
    int index = 0;
    Vec2 centerSector;
    MapSectorAnalysis * sector;
    
    for(int i = 0; i < strategy->sectorsInStrategy.size();i++)
    {
        _strategyLockMutex.lock();
        sector = strategy->sectorsInStrategy.at(i);
        _strategyLockMutex.unlock();

        red = 0;
        blue = 0;
        green = 0;
        
        Color4F color1;
        
        Vec2 position = sector->_sector->getSectorOrigin();
        Vec2 dotPosition = sector->_sector->getSectorOrigin();
        dotPosition.x += sector->_sector->getSectorBounds().size.width/2;
        dotPosition.y += sector->_sector->getSectorBounds().size.height/2;
        
        if(strategy->execution != nullptr)
        {
            if(strategy->execution->numberOfUnitsNeeded != strategy->requiredUnitStrenth)
            {
                red = 1.0;
                green = 1.0;
                blue = 1.0;
            }
            else
            {
                red = 1.0;
            }
        }
        color1 = Color4F(red, green, blue, .9);
        if(index != 0)
        {
            radius = 10;
            _primativeDrawer->drawSegment(dotPosition, centerSector, 3, color1);
            _primativeDrawer->drawDot(dotPosition, radius, color1);
        }
        else
        {
            centerSector = position;
            if(strategy->testText == nullptr)
            {
                strategy->testText = Label::createWithTTF("", "arial.ttf", 36);
                strategy->testText->setColor(Color3B(255,255,255));
                globalPlayerLayer->addChild(strategy->testText, 100000);
            }
            
            
            centerSector.x += sector->_sector->getSectorBounds().size.width/2;
            centerSector.y += sector->_sector->getSectorBounds().size.height/2;
            strategy->testText->setPosition(centerSector);
            std::ostringstream testString;

            testString << strategy->_strategyID << " " << goalTypeNames[strategy->_goalType] << " I: " << strategy->importance;
            if(strategy->execution == nullptr)
            {
                testString << " " << executionStateNames[5];
            }
            else
            {
                  testString << " " << executionStateNames[strategy->execution->executionState];
            }
            strategy->testText->setString(testString.str());

            radius = 16;
         //   [_primativeDrawer drawDot:riskPosition radius:radius color:[CCColor redColor]];
            _primativeDrawer->drawDot(dotPosition, radius, color1);

        }
        index++;
        //[_primativeDrawer drawBounds:sector.sector.sectorBounds color:color1 lineRadius:3];
    }
}

void AICommand::updateTypeUnitAverages()
{
    if(strategies.size() == 0)
        return;
    unitsNeededForMinimum = 0;
    totalValue = 0;
    
    //update the strategy type units' average value and risk
    StrategyTypeUnit * strategyTypeUnit;
    for(int key : strategyTypeUnits.keys())
    {
        _strategyLockMutex.lock();
        strategyTypeUnit = strategyTypeUnits.at(key);
        _strategyLockMutex.unlock();

        strategyTypeUnit->averageValue = 0;
        
        if(strategyTypeUnit->strategies.size() == 0)
            continue;
        
        int number = 0;
        for(AIStrategy * strategy : strategyTypeUnit->strategies)
        {
            if(strategy->importance > 1)
            {
                strategyTypeUnit->averageValue += strategy->importance;
                if(strategy->execution != nullptr && strategy->execution->numberOfUnitsNeeded > 0)
                {
                    unitsNeededForMinimum += strategy->execution->numberOfUnitsNeeded;
                }
                number++;
            }
        }
        strategyTypeUnit->averageValue = strategyTypeUnit->averageValue / number;
    }
    
    // This doesn't appear to do anything
    totalValue /= strategies.size();
}



void AICommand::queryAboutGoals()
{
    CCLOG("query!");
    //debug drawing stuff
    _primativeDrawer->clear();
    
    
    _strategyLockMutex.lock();
    //FIXME: this seems terribly slow
    for(AIStrategy * strategyToThinkAbout : strategies)
    {
        if(strategyToThinkAbout->execution != nullptr)
            drawGoalInfo(strategyToThinkAbout);
    }
    _strategyLockMutex.unlock();

    
    if(strategiesToUpdate.size() == 0 && unitsNotInStrategy.size() == 0)
        return;
    
    //update the type unit averages
    updateTypeUnitAverages();

    StrategyTypeUnit * strategyTypeUnit;
    bool updateAllNextTick = false;
    Vector<AIStrategy *> strategiesToRemove;
    Vector<AIStrategy *> strategyExecutionsToRemove;

    
    //if one or more units need to be assigned then add all the strategies that want units to the strategies to update list
    if(unitsNotInStrategy.size() > 0)
    {
        strategiesToUpdate.pushBack(strategiesThatWantUnits);
        strategiesThatWantUnits.clear();
    }
    
    //first walk through the strategy and determine which of them should have an execution created, which of them should have their execution removed, which of them should be
    // executed, and which of them should be deleted
    for(AIStrategy * strategyToThinkAbout : strategiesToUpdate)
    {
        _strategyLockMutex.lock();
        strategyTypeUnit = strategyTypeUnits.at(strategyToThinkAbout->_goalType);
        _strategyLockMutex.unlock();

        MapSectorAnalysis * sector;
        strategyToThinkAbout->sectorsArrayMutex.lock();
        sector =  strategyToThinkAbout->sectorsInStrategy.at(0);
        strategyToThinkAbout->sectorsArrayMutex.unlock();
        
        //decide if this strategy is important enough to create an execution for it
        if(strategyToThinkAbout->execution == nullptr && strategyToThinkAbout->importance >= (strategyTypeUnit->averageValue))
        {
            strategyToThinkAbout->execution = PlanExecution::create(strategyToThinkAbout);
        }
        else if(strategyToThinkAbout->importance < getStrategyDeleteThreshold(strategyTypeUnit))
        {
            //perhaps this strategy should have its execution deleted
            strategyExecutionsToRemove.pushBack(strategyToThinkAbout);
        }
        
        if(strategyToThinkAbout->execution != nullptr)
        {
            if(strategyToThinkAbout->execution->executionState == COMPLETED_PLAN)
            {
                updateAllNextTick = true;
                strategiesToRemove.pushBack(strategyToThinkAbout);
            }
            else if(strategyToThinkAbout->execution->numberOfUnitsNeeded > 0) //does this strategy need units?
            {
                strategiesThatWantUnits.pushBack(strategyToThinkAbout);
            }
        }
    }
    
    //delete the strategies
    for(AIStrategy * strategyToRemove : strategiesToRemove)
    {
        deleteStrategy(strategyToRemove);
    }
    for(AIStrategy * strategyExecutionToRemove : strategyExecutionsToRemove)
    {
        if(strategyExecutionToRemove->execution != nullptr)
        {
            _strategyLockMutex.lock();//ug.....
            strategyTypeUnit = strategyTypeUnits.at(strategyExecutionToRemove->_goalType);
            _strategyLockMutex.unlock();

            CCLOG("delete execution! %f %s ", getStrategyDeleteThreshold(strategyTypeUnit), AIStrategy::stringForGoalType(strategyExecutionToRemove->_goalType).c_str());
            strategyExecutionToRemove->execution->deleteExecution();
            strategyExecutionToRemove->execution = nullptr;
        }
    }
    
    strategiesToUpdate.clear();
    if(updateAllNextTick)
    {
        strategiesToUpdate.pushBack(strategies);
    }
    
    //step 2: add units to strategies
    addUnitsToExistingExecutions();
}

//command post related
void AICommand::addCommandPost(CommandPost * commandPost)
{
    Command::addCommandPost(commandPost);
    //mark each sector as within the new command post's command radius
    MapSector * commandPostSector  = globalSectorGrid->sectorForPosition(commandPost->getPosition());
    std::vector<MapSector *> sectors;
    globalSectorGrid->sectorsInCircleWithCenter(commandPost->getPosition(), commandPost->getControlRadius(), &sectors);

    for(MapSector * sector : sectors)
    {
        MapSectorAnalysis * analysis = getMapSectorAnalysisForSector(sector);
        
        analysis->_inCommandPostBuildRadius = true;
        //is this sector in the build command post zone?
        if(analysis->_inControlRadiusStatus != -1 && (VectorMath::rectOnCircumfrenceWithCenter(commandPost->getPosition(), commandPost->getControlRadius(), sector->getSectorBounds()) ||
            VectorMath::rectOnCircumfrenceWithCenter(commandPost->getPosition(), commandPost->getCreationRadius(), sector->getSectorBounds())))
        {
            analysis->_inControlRadiusStatus = 0;
            analysis->_inCommandPostBuildRadius = true;
        }
        else
        {
            analysis->_inControlRadiusStatus = -1;
        }
        if(sector == commandPostSector)
        {
            analysis->_friendlyCommandPostPresent = true;
        }
        
        globalBackgroundTaskHandler->addBackgroundTask([=] {
            //analysis->performAnalysis();
            return false;
        });
        
    }
    createStrategyUpdateRequests();
}

void AICommand::addUnit(Unit * unit)
{
    Command::addUnit(unit);
    unitsNotInStrategy.pushBack(unit);
    strategiesToUpdate.clear();
    strategiesToUpdate.pushBack(strategies);
}

void AICommand::removeUnit(Unit * unit)
{
    Command::removeUnit(unit);
    AIStrategy * strategy = strategyForUnitMap.at(unit->uID());
    if(strategy != nullptr)
    {
        if(strategy->execution != nullptr)
        {
            strategy->execution->unitFailedTask(unit);
        }
        strategiesToUpdate.pushBack(strategy);
        strategy->updateStrategyScore();
    }
}

void AICommand::unitCompletedOrder(Unit * entCompleting, Order * order, bool failed)
{
    AIStrategy * strategy = strategyForUnitMap.at(entCompleting->uID());
    
    if(strategy != nullptr)
    {
        Unit * unit = (Unit *) entCompleting;
        if(strategy->execution != nullptr)
        {
            strategy->execution->unitFinishedTask(unit);
            if(strategy->execution->executionState == COMPLETED_PLAN || strategy->_goalType == OCCUPY)
            {
                strategiesToUpdate.pushBack(strategy);
            }
        }
        strategy->updateStrategyScore();
    }
}

//enemy unit presence
void AICommand::enemyUnitSpotted(Unit * enemy)
{
//    MapSector * contactSector = globalSectorGrid->sectorForPosition(enemy->_unitPosition);
//    UnitLocationReport * reportForEnemy = enemyUnitsLocationReportMap.at(enemy->uID());
//    
//    //Get the map sector analysis for the sector the enemy is in
//    MapSectorAnalysis * contactSectorAnalysis = getMapSectorAnalysisForSector(contactSector);
//    
//    //if there is no contact report for this unit yet then create one
//    if(reportForEnemy == nullptr)
//    {
//        CCLOG("New Contact for enemy %d ",enemy->uID());
//
//        reportForEnemy = new UnitLocationReport();
//        reportForEnemy->analysis = nullptr;
//    }
//    
//    reportForEnemy->frameWhenReported = globalEngagementManager->getFrameCount();
//    
//    //If the unit's location changed
//    if(reportForEnemy->analysis != contactSectorAnalysis)
//    {
//        //remove the unit from the previous sector analysis
//        if(reportForEnemy->analysis != nullptr)
//        {
//            CCLOG("Change Contact for enemy %d ",enemy->uID());
//            reportForEnemy->analysis->_enemyStrength -= 1;
//            //update the analysis of the old sector
//            MapSectorAnalysisUpdateRequest * updateRequest = MapSectorAnalysisUpdateRequest::create(reportForEnemy->analysis);
//            globalBackgroundTaskHandler->addBackgroundTask(updateRequest);
//            
//            //and update the analysis for all the places the enemy could project strength
//            for(MapSectorAnalysis * analysis : reportForEnemy->enemyStrengthProjection)
//            {
//                analysis->_enemyProjectedStrength -= 1;
//                MapSectorAnalysisUpdateRequest * updateRequest = MapSectorAnalysisUpdateRequest::create(analysis);
//                globalBackgroundTaskHandler->addBackgroundTask(updateRequest);
//            }
//            reportForEnemy->enemyStrengthProjection.clear();
//        }
//        
//        CCLOG("Update Contact for enemy %d ",enemy->uID());
//
//        //now update the sector analysis
//        Vector<MapSector *> sightArray;
//        
//        //add enemy strength projection to all the sector's in the enemy's view
//        //this should probably be based on a assumed weapon strength bound
//        float radius = enemy->_maxWeaponRange * 1.1; //fudge this a bit
//        
//        globalSectorGrid->sectorsInCircleWithCenter(enemy->_averageUnitPosition, radius, &sightArray);
//        
//        for(MapSector * sectortInView : sightArray)
//        {
//            MapSectorAnalysis * analysisForSectorInViews = getMapSectorAnalysisForSector(sectortInView);
//            reportForEnemy->enemyStrengthProjection.pushBack(analysisForSectorInViews);
//            analysisForSectorInViews->_enemyFirePower += 1;
//            
//            MapSectorAnalysisUpdateRequest * updateRequest = MapSectorAnalysisUpdateRequest::create(analysisForSectorInViews);
//            globalBackgroundTaskHandler->addBackgroundTask(updateRequest);
//        }
//        
//        //change the unit's report map
//        enemyUnitsLocationReportMap.insert(enemy->uID(), reportForEnemy);
//        
//        //add the unit to the new sector analysis
//        contactSectorAnalysis->_enemyStrength += 1;
//        reportForEnemy->analysis = contactSectorAnalysis;
//
//        MapSectorAnalysisUpdateRequest * updateRequest = MapSectorAnalysisUpdateRequest::create(contactSectorAnalysis);
//        globalBackgroundTaskHandler->addBackgroundTask(updateRequest);
//    }
//    createStrategyUpdateRequests();
}

void AICommand::enemyCommandPostSpotted(CommandPost *enemy)
{
//    MapSector * sector = globalSectorGrid->sectorForPosition(enemy->getPosition());
//    
//    //Get the map sector analysis for the sector the enemy is in
//    MapSectorAnalysis * analysis = getMapSectorAnalysisForSector(sector);
//    UnitLocationReport * reportForEnemy = enemyUnitsLocationReportMap.at(enemy->uID());
//    
//    //if there is no contact report for this unit yet then create one
//    if(reportForEnemy == nullptr)
//    {
//        CCLOG("New Contact for Command Post %d ",enemy->uID());
//        reportForEnemy = new UnitLocationReport();
//        reportForEnemy->analysis = nullptr;
//
//        enemyUnitsLocationReportMap.insert(enemy->uID(), reportForEnemy);
//        analysis->_enemyCommandPostPresent = true;
//        
//        MapSectorAnalysisUpdateRequest * updateRequest = MapSectorAnalysisUpdateRequest::create(analysis);
//        globalBackgroundTaskHandler->addBackgroundTask(updateRequest);
//    }
//    reportForEnemy->frameWhenReported = globalEngagementManager->getFrameCount();
//    createStrategyUpdateRequests();
}


void AICommand::enemyCanEngageSector(MapSector * sector, Human * enemy)
{
    
}


void AICommand::enemyArtilleryInSector(MapSector * sector)
{
    
}


/////////// end low level report functions ///////

void AICommand::removeUnitFromStrategy(Unit * unit)
{
    strategyForUnitMap.erase(unit->uID());
    unitsNotInStrategy.pushBack(unit);
}


/* A sector has a new value high enough to warrant creating a new strategy, consider doing so
 *    If the strategy type is BUILD_COMMAND_POST then set all BUILD_COMMAND_POST strategies in the command post radius of the new sector to a negative value (ensuring their deletion) and create a new BUILD_COMMAND_POST strategy at this location
 *  If the strategy type is NOT BUILD_COMMAND_POST and stratsToConsider is not empty, then add this sector to the highest importance strategy in strats to consider
 *      Otherwise create a new strategy at this location
 */
void AICommand::considerCreationOfNewStrategyAtMapSector(MapSectorAnalysis * sector, AI_GOAL_TYPE strategyType, float value, Vector<AIStrategy *> stratsToConsider)
{
    
    StrategyTypeUnit * strategyUnit = strategyTypeUnits.at(strategyType);
    //If the type of strategy to be updated is build command post, then stamp all strategies within the updated radius to nill
    if(strategyType == BUILD_COMMAND_POST)
    {
        for(AIStrategy * strategy : stratsToConsider)
        {
            strategy->importance = -1000;
            strategiesToUpdate.pushBack(strategy);
        }
    }
    else
    {
        AIStrategy * highestStrategy = nullptr;
        double lowestValue;
        for(AIStrategy * strategy : stratsToConsider)
        {
            double distance = strategy->distanceFromPrincipleSectorInStrategy(sector->_sector);
            if(highestStrategy == nullptr || distance < lowestValue)
            {
                highestStrategy = strategy;
                lowestValue = distance;
            }
        }
        
        if(highestStrategy != nullptr)
        {
            highestStrategy->addSector(sector);
            strategiesToUpdate.pushBack(highestStrategy);
            return;
        }
    }
    
    //Not updating an existing goal, so create a new one
    if(value >= getStrategyDeleteThreshold(strategyUnit) / 10)
    {
        AIStrategy * newStrategy = AIStrategy::create(strategyType, sector, this, strategyIDCounter);
        strategyIDCounter++;
        _strategyLockMutex.lock();
        strategyUnit->strategies.pushBack(newStrategy);
        strategies.pushBack(newStrategy);
        strategiesToUpdate.pushBack(newStrategy);
        return;
    }
    return;
}

/**
 *
 *
 */
void AICommand::evaluateSectorAnalysis(MapSectorAnalysis * sector, AI_GOAL_TYPE goalType, float value)
{
    Vector<AIStrategy *> possibleStrategies;
    
    /////two steps:
    //1. Update existing strategies containing this sector
    //2. Consider creating a new goal cenetered on this sector
    StrategyTypeUnit * strategyUnit = strategyTypeUnits.at(goalType);
    AIStrategy * strategy;

    for(int i = 0; i < strategyUnit->strategies.size();i++)
    {
        _strategyLockMutex.lock();
        strategy = strategyUnit->strategies.at(i);
        _strategyLockMutex.unlock();

        SECTOR_ACTION recommendedAction = strategy->considerSector(sector);
        //Possibly add or a new strategy centered on this sector, or replace a strategy with one cenetered on this sector
        if(recommendedAction == ADD || recommendedAction == REPLACE)
        {
            possibleStrategies.pushBack(strategy);
        }
        else if(recommendedAction == REMOVE) //remove a strategy centered on this sector
        {
            strategy->removeSector(sector);
        }
        else if(recommendedAction == DO_NOT_CREATE_NEW) //
        {
            return;
        }
    }
    
    considerCreationOfNewStrategyAtMapSector(sector, goalType, value, possibleStrategies);
}

void AICommand::createStrategyUpdateRequests()
{
    globalBackgroundTaskHandler->addBackgroundTask([=] {
        //performAnalysisOfSectorsForGoalType(OCCUPY);
        return false;
    });
    
    globalBackgroundTaskHandler->addBackgroundTask([=] {
        //performAnalysisOfSectorsForGoalType(FORITY);
        return false;
    });
    
    globalBackgroundTaskHandler->addBackgroundTask([=] {
        //performAnalysisOfSectorsForGoalType(BOMBARD);
        return false;
    });
    
    globalBackgroundTaskHandler->addBackgroundTask([=] {
        //performAnalysisOfSectorsForGoalType(BUILD_COMMAND_POST);
        return false;
    });
    
    globalBackgroundTaskHandler->addBackgroundTask([=] {
        //performAnalysisOfSectorsForGoalType(RETREAT_FROM);
        return false;
    });
}

void AICommand::changeToSectorAnalysis(MapSectorAnalysis * sector, AI_GOAL_TYPE goalType)
{
    Vector<MapSectorAnalysis *> * typeUnit = sectorsToAnalyzeTypeUnits.at(goalType);
    _strategyLockMutex.lock();
    typeUnit->pushBack(sector);
    _strategyLockMutex.unlock();
}

AIStrategy * AICommand::strategyForUnit(Unit * unit)
{
    return strategyForUnitMap.at(unit->uID());
}

MapSectorAnalysis * AICommand::getMapSectorAnalysisForSector(MapSector * sector)
{
    MapSectorAnalysis * analysis = sectorAnalyses.at(sector->uID());
    if(analysis == nullptr)
    {
        analysis = MapSectorAnalysis::create(sector, this);
        sectorAnalyses.insert(sector->uID(), analysis);
    }
    return analysis;
}

void AICommand::performAnalysisOfSectorsForGoalType(AI_GOAL_TYPE goalType)
{
    Vector<MapSectorAnalysis *> * typeUnit = sectorsToAnalyzeTypeUnits.at(goalType);

    sort(typeUnit->begin(), typeUnit->end(),
         [=]( MapSectorAnalysis *  a,  MapSectorAnalysis * b) -> bool {
             float strategy1Value = AIStrategy::qualitativeVariableForGoal(goalType, a);
             float strategy2Value = AIStrategy::qualitativeVariableForGoal(goalType, b);
             if(strategy1Value > strategy2Value)
                 return true;
             return false;
         });
    
    for (MapSectorAnalysis * sector : (*typeUnit)) {
        evaluateSectorAnalysis(sector, goalType, AIStrategy::qualitativeVariableForGoal(goalType, sector));
    }
    
    typeUnit->clear();
}
