//
//  InputActionHandler.hpp
//  TrenchWars
//
//  Created by Paul Reed on 7/23/21.
//

#ifndef InputActionHandler_h
#define InputActionHandler_h

#include "cocos2d.h"
#include "GameEventController.h"
#include "OverlayDrawer.h"
#include "LandscapeObjectiveBuilder.h"
#include "EmblemManager.h"
#include "UnitGroupDrawer.h"
#include "Emblem.h"
#include "ObjectiveDrawer.h"
#include "DebugObjectMenu.h"
#include "InteractiveObjectHighlighter.h"
#include "CommandPostCreationDrawer.h"

USING_NS_CC;
using namespace cocos2d::ui;

#define DRAG_THRESHOLD 10

class Command;
class Entity;
class RawInputHandler;

class InputActionHandler : public Ref
{
    ObjectiveDrawer * _newObjective;
    UnitGroupDrawer * _activeUnitGroupDrawer;
    UnitGroup * _newUnitGroup;
    
    InteractiveObjectHighlighter * _selectedHighlighter;
    DebugObjectMenu * _debugInfoMenu;
    
    OverlayDrawer * _overlay;
    
    RawInputHandler * inputHandler;
    Emblem * _dragEmblem;
    Emblem * _selectedEmblem;
    
    void dragEmblem(Vec2 location, bool dragEnd);
    
    InputActionHandler();

    
public:
    
    static InputActionHandler * create();
    void dragObject(InteractiveObject * objDragged, Vec2 location, bool dragEnd);
    void marqueeSelect(Vec2 location, bool dragEnd);
    void drawObjective(Vec2 location, bool dragEnd);
    void selectObject(InteractiveObject * objClicked, Vec2 location);
    void modifySelection(InteractiveObject * objClicked);
    void orderObject(Vec2 location);
    void showTestMenu(Vec2 location);
    void toggleInfoScreen();
    void toggleGameMenu();
    void togglePause();
    
    void hoverEnter(InteractiveObject * objectHovered, Vec2 location);
    void hoverLeave(InteractiveObject * objectHovered, Vec2 location);
    
    void remove();
};



#endif /* InputActionHandler_h */
