#ifndef __MAP_SECTOR_ANALYSIS_H__
#define __MAP_SECTOR_ANALYSIS_H__
//
//  MapSectorAnalysis.h
//  Trench Wars
//
//  Created by Paul Reed on 6/2/14.
//  Copyright (c) 2014 Paul Reed. All rights reserved.
//


#include "cocos2d.h"

class MapSector;
class AICommand;
class PrimativeDrawer;
class AIStrategy;

USING_NS_CC;


class MapSectorAnalysis : public Ref
{
public:
    //FIXME: make these private later
    AICommand * _owningCommand;
    MapSector * _sector;
    
    //////// high lvl analysis variables
    float _buildCommandPostGoalPotential;
    float _defendGoalPotential;
    float _bombardGoalPotential;
    float _fortifyGoalPotential;
    float _retreatFromGoalPotential;
    float _attackGoalPotential;
    float _danger;
    
    //////// low lvl analysis variables, each ranges from 0 to 100
    int _friendlyStrength;
    int _friendlyFirePower;
    int _friendlyArtilleryPower;
    int _friendlyProjectedStrength; //units that are moving through or to this sector
    bool _friendlyCommandPostPresent;
    
    int _enemyStrength;
    int _enemyFirePower;
    int _enemyArtilleryPower;
    int _enemyProjectedStrength; //units that are moving through or to this sector
    bool _enemyCommandPostPresent;
    
    int _inControlRadiusStatus; //1 = outside control radius, 0 = in control radius but not inside build command radius, -1 = in control radius and inside build command
    bool _inCommandPostBuildRadius;
    bool _inView;
    
    double distanceFromPrimaryCommandPost;
    double distanceFromNearestCommandPost;
    double distanceFromNearestEnemyCommandPost;
    double distanceFromEnemyTerritory; //territory held by the enemy
    
    PrimativeDrawer * _primativeDrawer;
    
    Vector<AIStrategy *> plans; //T
    
    /*
    int _leftTraversability;
    int _rightTraversability;
    int _upTraversability;
    int _downTraversability;
    
    int _leftCoverStrength;
    int _rightCoverStrength;
    int _upCoverStrength;
    int _downCoverStrength;
     */
private:
    void determineTraversability();
public:
    MapSectorAnalysis();
    virtual bool init(MapSector * sector, AICommand * command);
    static MapSectorAnalysis * create(MapSector * sector, AICommand * command);
    
    void setEnemyCommandPostPresent(bool commandPostPresent);
    void setEnemyStrength(int enemyStrength);
    void setEnemyFirePower(int enemyFirePower);
    void setInControlRadiusStatus(int inControlRadiusStatus);
    void setInCommandPostBuildRadius(bool inCommandPostBuildRadius);

    void performAnalysis();
};

#endif //__MAP_SECTOR_ANALYSIS_H__
