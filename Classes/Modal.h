//
//  Modal.hpp
//  TrenchWars
//
//  Created by Paul Reed on 11/10/23.
//

#ifndef Modal_h
#define Modal_h

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "AutoSizedListView.h"

class Modal : public Layout
{
protected:
    ListView * _contentWidget;
    virtual bool init(Layout::Type layoutDirection);
    Modal();

public:
    static Modal * create(Layout::Type layoutDirection);
    void addContent(Widget * content);
    void clearContent();
};

#endif /* Modal_h */
