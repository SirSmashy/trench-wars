//
//  EmblemManager.hpp
//  TrenchWars
//
//  Created by Paul Reed on 7/13/21.

#ifndef EmblemManager_h
#define EmblemManager_h

#include "cocos2d.h"
#include "Emblem.h"
#include "TesterEmblem.h"
#include "UnitEmblem.h"
#include "UnitGroupEmblem.h"
#include "Entity.h"
#include "Command.h"
#include "Order.h"
#include "UnitGroup.h"

USING_NS_CC;

class EmblemManager
{
    std::unordered_map<ENTITY_ID, std::unordered_set<ENTITY_ID>> _entityToEmblemsMap;
    Map<ENTITY_ID, Emblem *> _emblemsMap;
public:
    
    EmblemManager();
    
    void update(float deltaTime);
    void moveEmblemToOrderPosition(Order * order, Emblem * emblem);
    
    Emblem * createEmblemForUnitGroup(UnitGroup * group, EMBLEM_TYPE type);
    Emblem * createEmblemForUnit(Unit * entity, EMBLEM_TYPE type);
    TesterEmblem * createEmblemForTester(Tester * tester, bool updateOnDragStart);

    void removeAllEmblemsForCommandableObject(ENTITY_ID objectId);
    void removeEmblemForCommandableObject(ENTITY_ID objectId, Emblem * emblem);
    void removeAllEmblemsWithType(EMBLEM_TYPE type);

    Emblem * getEmblem(ENTITY_ID emblemId);
    Emblem * getEmblemForCommandableObjectWithType(ENTITY_ID objectId, EMBLEM_TYPE type);

    std::vector<Emblem *> getEmblemsForCommandableObject(ENTITY_ID objectId);
    Emblem * getEmblemAtPoint(Vec2 point);
};

extern EmblemManager * globalEmblemManager;

#endif /* EmblemManager_h */
