//
//  AutoSizeAnimation.hpp
//  TrenchWars
//
//  Created by Paul Reed on 12/29/22.
//

#ifndef AutoSizeAnimation_h
#define AutoSizeAnimation_h

#include "cocos2d.h"
#include "EntityFactory.h"

class AutoSizeAnimation : public Animate
{
protected:
    std::string _projectileName;
    EntityAnimation * _entityAnimation;
    Vec2 _spriteScale;
    
    bool init(EntityAnimation *animation, Size size);
    
public:
    static AutoSizeAnimation* create(EntityAnimation *animation, Size size);
    virtual void startWithTarget(Node *target) override;
};

#endif /* AutoSizeAnimation_h */
