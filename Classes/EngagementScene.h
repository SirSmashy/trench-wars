//
//  HelloWorldScene.h
//  Trench Wars
//
//  Created by Paul Reed on 1/22/14.
//  Copyright Paul Reed 2014. All rights reserved.
//
// -----------------------------------------------------------------------

// Importing cocos2d.h and cocos2d-ui.h, will import anything you need to start using Cocos2D v3
#include "cocos2d.h"

using namespace cocos2d::ui;
USING_NS_CC;

// -----------------------------------------------------------------------

/**
 *  The main scene
 */
class EngagementScene  : public cocos2d::Scene
{
public:
    
    virtual bool init();
    static EngagementScene * create();


    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    
    void finish();
};
