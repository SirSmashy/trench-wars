//
//  ObjectiveDrawer.cpp
//  TrenchWars
//
//  Created by Paul Reed on 9/15/20.
//

#include "Objective.h"
#include "ObjectiveDrawer.h"
#include "EngagementManager.h"
#include "VectorMath.h"
#include "GameVariableStore.h"
#include "PlayerVariableStore.h"
#include "UIController.h"
#include "GameEventController.h"
#include "CollisionGrid.h"
/**
 *
*/
ObjectiveDrawer::ObjectiveDrawer() : DrawNode()
{
    _currentSegmentValid = true;
}

/**
 *
*/
ObjectiveDrawer::~ObjectiveDrawer()
{
}

/**
 *
*/
bool ObjectiveDrawer::init()
{
    if(DrawNode::init()) {
        globalPlayerLayer->addChild(this, 0); //1000
        _color.r = globalPlayerVariableStore->getVariable(ObjectiveColorR);
        _color.g = globalPlayerVariableStore->getVariable(ObjectiveColorG);
        _color.b = globalPlayerVariableStore->getVariable(ObjectiveColorB);

        return true;
    }
    return false;
}
           
/**
 *
*/
ObjectiveDrawer * ObjectiveDrawer::create()
{
    ObjectiveDrawer * plan = new ObjectiveDrawer();
    if(plan->init()) {
        plan->autorelease();
        return plan;
    }
    CC_SAFE_DELETE(plan);
    return nullptr;
    
    return nullptr;
}

/**
 *
*/
void ObjectiveDrawer::dragStart(Vec2 point)
{
    _currentLine = LineDrawer::create(point * WORLD_TO_GRAPHICS_SIZE, point * WORLD_TO_GRAPHICS_SIZE,2);
    _currentLine->setScaleWidthWithParentScale(true);
    _currentLine->setColor(_color);
    points.push_back(point);
    return;
}

/**
 *
*/
void ObjectiveDrawer::dragMove(Vec2 point)
{
    clear();
    _currentPoint = point;
    _currentSegmentValid = isCurrentSegmentValid(point);
    if(_currentSegmentValid)
    {
        double dist = points[points.size() -1].distance(point);
        _currentLine->setLinePosition(points.back() * WORLD_TO_GRAPHICS_SIZE, _currentPoint * WORLD_TO_GRAPHICS_SIZE);
        if(dist >= (globalVariableStore->getVariable(ObjectiveVertexLength) / globalPlayerLayer->getScale()))
        {
            points.push_back(point);
            _lines.pushBack(_currentLine);

            _currentLine = LineDrawer::create(_currentPoint * WORLD_TO_GRAPHICS_SIZE, _currentPoint * WORLD_TO_GRAPHICS_SIZE, 2);
            _currentLine->setScaleWidthWithParentScale(true);
            _currentLine->setColor(_color);
        }
        if(points.size() > 2) {
            if(nearStartPoint(point))
            {
                Color4F alphaColor(_color);
                drawCircle(points[0] * WORLD_TO_GRAPHICS_SIZE, (globalVariableStore->getVariable(ObjectiveClosePolygonDistance) * WORLD_TO_GRAPHICS_SIZE) / globalPlayerLayer->getScale(), 0, 30, false, alphaColor);
            }
        }
    }
    return;
}

/**
 *
*/
void ObjectiveDrawer::dragEnd(Vec2 point)
{
    clear();
    bool isEnclosed = false;
    _currentSegmentValid = isCurrentSegmentValid(point);
    if(_currentSegmentValid)
    {
        points.push_back(point);
    }
    _currentSegmentValid = true;
    if(points.size() > 2 && nearStartPoint(point)) {
        isEnclosed = true;
        //close the poly
        points.push_back(points[0]);
    }
    
    globalPlayerLayer->removeChild(this);
    for(auto line : _lines)
    {
        line->remove();
    }
    _lines.clear();
    _currentLine->remove();
    globalGameEventController->createNewObjective(points, isEnclosed, nullptr);
    return;
}

/**
 *
*/
bool ObjectiveDrawer::nearStartPoint(Vec2 point)
{
    double dist = points[0].distance(point);
    if(dist < (globalVariableStore->getVariable(ObjectiveClosePolygonDistance) / globalPlayerLayer->getScale())) {
        return true;
    }
    return false;
}

/**
 *
*/
bool ObjectiveDrawer::isCurrentSegmentValid(Vec2 segmentEnd)
{
    if(points.size() < 3) {
        return true;
    }
    for(int i = 1; i < points.size() - 2; i++) {
        if(points[i].isSegmentIntersect(points[i-1], points[i], points[points.size() - 1], segmentEnd)) {
            return false;
        }
    }
    return true;
}

void ObjectiveDrawer::cancel()
{
    globalPlayerLayer->removeChild(this);
}
