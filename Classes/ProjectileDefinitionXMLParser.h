#ifndef ProjectileDefinitionXMLParser_h
#define ProjectileDefinitionXMLParser_h
//
//  ProjectileDefinitionXMLParser.hpp
//  TrenchWars
//
//  Created by Paul Reed on 4/24/20.
//

#include "pugixml/pugixml.hpp"
#include "EntityFactory.h"
#include "cocos2d.h"

using namespace pugi;
USING_NS_CC;


class ProjectileDefinitionXMLParser :public xml_tree_walker
{
    ProjectileDefinition * _currentProjectileDefinition;
    EntityFactory *  _entityFactory;
    
public:
    ProjectileDefinitionXMLParser(EntityFactory * factory);
    
    void parseFile(std::string const & filename);
    virtual bool for_each(xml_node& node);
};

#endif /* ProjectileDefinitionXMLParser_hpp */
