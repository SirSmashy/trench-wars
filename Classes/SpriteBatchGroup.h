//
//  SpriteBatchGroup.hpp
//  trench-wars
//
//  Created by Paul Reed on 10/3/23.
//

#ifndef SpriteBatchGroup_h
#define SpriteBatchGroup_h


#include "cocos2d.h"
#include "Refs.h"
#include "AnimatedSprite.h"

USING_NS_CC;

class EntityAnimationSet;

class SpriteBatchGroup : public Ref
{
    Vector<SpriteBatchNode *> _batches;
    int _currentBatchIndex;
    Texture2D * _texture;
    EntityAnimationSet * _animationSet;
    
    void init(EntityAnimationSet * animationSet);
public:
    
    static SpriteBatchGroup * create(EntityAnimationSet * animationSet);
    
    void update(float deltaTime);
    SpriteBatchNode * addSprite(AnimatedSprite * sprite, int zOrder);
    void setVisible(bool visible);
    void setPositionOffset(Vec2 position);
    void removeParticleForOwner(ENTITY_ID owner);
};
#endif /* SpriteBatchGroup_hpp */
