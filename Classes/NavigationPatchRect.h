//
//  NavigationRect.h
//  TrenchWars
//
//  Created by Paul Reed on 2/8/20.
//

#ifndef NavigationPatchRect_h
#define NavigationPatchRect_h

#include "cocos2d.h"
#include "CollisionCell.h"
#include "VectorMath.h"
#include "NavigableRect.h"

#define MAX_PATCH_SIZE 500
#define MAX_PATCH_AREA MAX_PATCH_SIZE * MAX_PATCH_SIZE
#define SIZE_TO_ASPECT_RATIO_CONSTANT 25 //(5^2)


class NavigationPatchRect : public NavigableRect
{
    TerrainCharacteristics _terrain;
    
public:
    
    NavigationPatchRect();
    NavigationPatchRect(int xMin, int xMax, int yMin, int yMax);
    NavigationPatchRect(int xMin, int xMax, int yMin, int yMax, const TerrainCharacteristics & terrain);
    

    void set(int xMin, int xMax, int yMin, int yMax, const TerrainCharacteristics & terrain);
    
    inline const TerrainCharacteristics & getTerrain() {return _terrain;}
    inline const void setTerrain(TerrainCharacteristics & terrain) {_terrain = terrain;}


    bool hasSameTerrain(NavigationPatchRect & other) const;
    
    bool operator<(const NavigationPatchRect & other) const;
    bool operator==(const NavigationPatchRect & other) const;
    bool operator!=(const NavigationPatchRect & other) const;
};

#endif /* NavigationPatchRect_h */
