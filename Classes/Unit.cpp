//
//  Unit.cpp
//  TrenchWars
//
//  Created by Paul Reed on 2/5/12.
//  Copyright (c) 2012. All rights reserved.
//

#include "Unit.h"
#include "Infantry.h"
#include "VectorMath.h"
#include "EntityManager.h"
#include "EntityFactory.h"
#include "StaticEntity.h"
#include "AICommand.h"
#include "Order.h"
#include "ObjectiveObjectCollection.h"
#include "GameMenu.h"
#include "MultithreadedAutoReleasePool.h"
#include "MenuLayer.h"
#include "EngagementManager.h"
#include "CollisionGrid.h"
#include "SectorGrid.h"
#include "TeamManager.h"
#include "GameEventController.h"
#include "InteractiveObjectHighlighter.h"
#include "PlanExecution.h"

#define ENEMIES_TO_CHECK 10

Unit::Unit() :
checkSightCounter(SIGHTCOUNTERUNITMAX, true),
updateBoundsCounter(UPDATEBOUNDSCOUNTERMAX, true)
{
    
}

Unit * Unit::create(UnitDefinition * definition, Vec2 position, Command * command)
{
    Unit * newUnit = new Unit();
    if(newUnit && newUnit->init(definition, position, command))
    {
        globalAutoReleasePool->addObject(newUnit);
        return newUnit;
    }
    CC_SAFE_DELETE(newUnit);
    return nullptr;
}

Unit * Unit::create(const std::string & definitionName, Vec2 position, Command * command)
{
    UnitDefinition * definition = globalEntityFactory->getUnitDefinitionWithName(definitionName);
    if(definition != nullptr)
    {
        if(definition->_crewWeapaon.compare("") != 0)
        {
            return CrewWeaponUnit::create(definition, position, command);
        }
        else
        {
            return Unit::create(definition, position, command);
        }
    }
    return nullptr;
}


Unit * Unit::create()
{
    Unit * newUnit = new Unit();
    globalAutoReleasePool->addObject(newUnit);
    return newUnit;
}

bool Unit::init(UnitDefinition * definition, Vec2 position, Command * command)
{
    
    _unitDefinition = definition;
    _hashedEntityName = _unitDefinition->_hashedName;
    
    _currentStrength = 0;
    _maxStrength = definition->_strength;
    _owningCommand = command;
    if(_owningCommand !=  nullptr)
    {
        _owningTeam = _owningCommand->getOwningTeam();
    }
    else
    {
        _owningTeam = nullptr;
    }
    
    PlanningObject::init(_owningTeam);

    _unitPosition = position;
    _averageUnitPosition = position; //this will be updated post load
    _averageUnitVelocity.setZero();
    _currentOrder = nullptr;
    _membersWorkingOnOrder = 0;
    _nearestEnemyDirection = NONE;
    _averageUnitStress = 0;
    _averageUnitAmmoPercent = 0;
    createActors();
    return true;
}


void Unit::createActors()
{
    Infantry * newEnt;
    Vec2 spawnPoint;
    for(UnitMemberDefinition * member : _unitDefinition->_members)
    {
        spawnPoint.x = _unitPosition.x + member->_xOffset;
        spawnPoint.y = _unitPosition.y + member->_yOffset;
        
        spawnPoint = world->getCollisionGrid(spawnPoint)->getValidMoveCellAroundPosition(spawnPoint, 20);
        
        InfantryDefinition * actorDef = (InfantryDefinition *) globalEntityFactory->getPhysicalEntityDefinitionWithName(member->_memberName);
        
        if(actorDef->_type == INFANTRY)
        {
            newEnt = Infantry::create(member->_memberName, spawnPoint, _owningCommand, this);
        }
        else
        {
            continue;
        }
        
        newEnt->setUnitOffset(Vec2(member->_xOffset,member->_yOffset));
        _tryRandomEnemiesForInfantry[newEnt] = true;
        addEntity(newEnt);
    }
}

void Unit::postLoad()
{
    float xMax,xMin,yMax,yMin;

    xMax = 0;
    xMin = 100000;
    yMax = 0;
    yMin = 100000;
    
    for(Infantry * member : _unitMembers)
    {
        if((member->getPosition().x + member->getCollisionBounds().size.width) > xMax)
            xMax = member->getPosition().x + member->getCollisionBounds().size.width;
        if((member->getPosition().x ) < xMin)
            xMin = member->getPosition().x;
        if((member->getPosition().y + member->getCollisionBounds().size.height) > yMax)
            yMax = member->getPosition().y + member->getCollisionBounds().size.height;
        if((member->getPosition().y) < yMin)
            yMin = member->getPosition().y;
    }
    
    _defaultUnitSize.width = xMax - xMin;
    _defaultUnitSize.height = yMax - yMin;
     updateBounds();
    
    _owningCommand->addUnit(this);
    globalGameEventController->handleEventNewUnit(this);
    
    if(_currentOrder != nullptr)
    {
        globalGameEventController->handleEventOrderIssued(_currentOrder, this, nullptr); //FIXME: TODO fix do we need this???
    }
}

void Unit::query(float deltaTime)
{
    if(_unitMembersToRemove.isChanged())
    {
        auto removals = _unitMembersToRemove.get();
        _unitMembersToRemove.clearChange();
        for(auto remove : removals)
        {
            handleMemberRemoval(remove);
        }
        
        if(_currentStrength <= 0)
        {
            removeFromGame();
            return;
        }
    }
    if(checkSightCounter.count())
    {
        updateVision();
    }
    if(_pendingOrder.isChanged())
    {
        startOrder();
    }
    
    
    _sectorsVisible.clear();
    for(auto member : _unitMembers)
    {
        member->getLastVisibleSectors(_sectorsVisible);
    }
    _owningTeam->markSectorsVisible(_sectorsVisible);
}

void Unit::act(float deltaTime)
{
    updateBounds();
}

void Unit::addEntity(PhysicalEntity * ent)
{
    if(_currentStrength < _maxStrength && ent->getEntityType() == INFANTRY)
    {
        Infantry * inf = (Infantry *) ent;
        _unitMembers.push_back(inf);
        _currentStrength++;
    }
}

bool Unit::containsEntity(PhysicalEntity * ent)
{
    for(auto member : _unitMembers)
    {
        if(member->uID() == ent->uID())
        {
            return true;
        }
    }
    return false;
}


void Unit::removeEntity(PhysicalEntity * ent)
{
    Infantry * inf = (Infantry *) ent;
    _unitMembersToRemove.addValue(inf);
}

void Unit::handleMemberRemoval(Infantry * member)
{
    _currentStrength--;
    auto iter = std::find(_unitMembers.begin(), _unitMembers.end(), member);
    if (iter != _unitMembers.end())
    {
        _unitMembers.erase(iter);
    }
}

void Unit::updateBounds()
{
    float xMax = 0;
    float xMin = 100000;
    float yMax = 0;
    float yMin = 100000;
    
    float averageStress = 0;
    float averageAmmoPercent = 0;
    
    Vec2 averagePosition;
    _averageUnitVelocity.setZero();
    for(Infantry * member : _unitMembers)
    {
        if(member->getActorState() == ALIVE)
        {
            if((member->getPosition().x + member->getSightRadius()) > xMax)
                xMax = member->getPosition().x + member->getSightRadius();
            if((member->getPosition().x - member->getSightRadius()) < xMin)
                xMin = member->getPosition().x - member->getSightRadius();
            if((member->getPosition().y + member->getSightRadius()) > yMax)
                yMax = member->getPosition().y + member->getSightRadius();
            if((member->getPosition().y - member->getSightRadius()) < yMin)
                yMin = member->getPosition().y - member->getSightRadius();
            
            averagePosition += member->getPosition();
            _averageUnitVelocity += member->getAverageVelocity();
        }
        
        averageStress += member->getStress();
        averageAmmoPercent += member->getCurrentAmmoReserve() / ( (float) member->getMaxAmmoReserve());
    }
    
    _averageUnitPosition = averagePosition / _unitMembers.size();
    
    _averageUnitVelocity.x /= _unitMembers.size();
    _averageUnitVelocity.y /= _unitMembers.size();
    
    _averageUnitStress = averageStress / _unitMembers.size();
    _averageUnitAmmoPercent = averageAmmoPercent / _unitMembers.size();

    
    _sightBounds.origin.x = xMin;
    _sightBounds.origin.y = yMin;
    _sightBounds.size.width = xMax - xMin;
    _sightBounds.size.height = yMax - yMin;
    
    globalGameEventController->handleEventUnitInSector(globalSectorGrid->sectorForPosition(_averageUnitPosition),this);
    xMax = 0;
    xMin = 100000;
    yMax = 0;
    yMin = 100000;
    
    for(Infantry * member : _unitMembers)
    {
        if(member->getActorState() == ALIVE)
        {
            if((member->getPosition().x + member->getCollisionBounds().size.width) > xMax)
                xMax = member->getPosition().x + member->getCollisionBounds().size.width;
            if(member->getPosition().x < xMin)
                xMin = member->getPosition().x;
            if((member->getPosition().y + member->getCollisionBounds().size.height) > yMax)
                yMax = member->getPosition().y + member->getCollisionBounds().size.height;
            if(member->getPosition().y < yMin)
                yMin = member->getPosition().y;
        }
    }
    _boundingRect.origin.x = xMin;
    _boundingRect.origin.y = yMin;
    _boundingRect.size.width = xMax - xMin;
    _boundingRect.size.height = yMax - yMin;
}


void Unit::updateVision()
{    
    _enemiesVisibleMutex.lock();
    _enemies.clear();
    std::vector<float> enemeyDistanceForDirection = {-1,-1,-1,-1,-1,-1,-1,-1}; //nearest enemy in each direction
    CollisionGrid * grid = world->getCollisionGrid(_averageUnitPosition);
    
    for(auto team : globalTeamManager->getTeams())
    {
        if(team == _owningTeam)
            continue;
        for(auto command : team->getCommands())
        {
            for(Unit * unit : command->getUnits())
            {
                if(_sightBounds.intersectsRect(unit->_boundingRect))
                {
                    Vec2 positionDifference = unit->getAveragePosition() - getAveragePosition();
                    DIRECTION direction = VectorMath::facingFromVector(positionDifference);
                    float distanceSq = positionDifference.lengthSquared();
                    if(enemeyDistanceForDirection[direction] == -1 || distanceSq < enemeyDistanceForDirection[direction])
                    {
                        enemeyDistanceForDirection[direction] = distanceSq;
                    }
                    globalGameEventController->handleEventEnemyUnitSpotted(unit);
                    for(Infantry * member : _unitMembers)
                    {
                        if(member->getActorState() != DEAD && member->getEntityType() == INFANTRY) {
                            _enemies.pushBack(member);
                        }
                    }
                }
            }
            
            for(CommandPost * post : command->getCommandPosts())
            {
                for(Courier * courier : post->getCouriers())
                {
                    if(courier->getActorState() != DEAD && courier->getCurrentCollisionGrid() == grid && _sightBounds.intersectsRect(courier->getCollisionBounds()))
                    {
                        _enemies.pushBack(courier);
                    }
                }
                if(_sightBounds.intersectsRect(post->getCollisionBounds()))
                {
                    globalGameEventController->handleEventEnemyCommandPostSpotted(post);
                }
            }
        }
    }
    
    // Find the direction with the nearest enemy
    DIRECTION previous = _nearestEnemyDirection;
    _nearestEnemyDirection = NONE; 
    float nearest = 9999999999;
    bool enemyFound = false;
    for(int i = 0; i < enemeyDistanceForDirection.size(); i++)
    {
        if(enemeyDistanceForDirection[i] != -1 && enemeyDistanceForDirection[i] < nearest)
        {
            nearest = enemeyDistanceForDirection[i];
            _nearestEnemyDirection = (DIRECTION) i;
            enemyFound = true;
        }
    }
    
    // Didn't find a nearest direction, just set it to a vector pointing away from this unit's primary command post (i.e., in the general direction of the enemy)
    if(_nearestEnemyDirection == NONE)
    {
        if(_owningCommand->getPrimaryCommandPost() != nullptr)
        {
            Vec2 diff = getAveragePosition() - _owningCommand->getPrimaryCommandPost()->getPosition();
            _nearestEnemyDirection = VectorMath::facingFromVector(diff);
        }
    }
    
//    std::unordered_map<ENTITY_ID, float> enemyDistanceMap;
//    for(auto enemy : _enemies)
//    {
//        enemyDistanceMap.emplace(enemy->uID(), _averageUnitPosition.distanceSquared(enemy->getPosition()));
//    }
    
    sort(_enemies.begin(), _enemies.end(), [=](Human * a, Human * b) -> bool
     {
        return _averageUnitPosition.distanceSquared(a->getPosition()) < _averageUnitPosition.distanceSquared(b->getPosition());
    });
    
    nearest = VectorMath::fastSqrt(nearest);
    _currentOrderMutex.lock();
    // See if enemies have appeared in a new direction... TODO fix this is a dumb way to handle things
    // If enemeies are closing from multiple directions this could bounce back and forth between two directions and cause more stress than necessary
    // Clearly the unit should keep track of previously seen enemy units (or maybe humans?) and if a new one appears add stress appropriately
//    if(enemyFound && _nearestEnemyDirection != previous)
//    {   //New enemy position, take cover!
//        for(Infantry * member : _unitMembers)
//        {
//            member->enemySpotted(_nearestEnemyDirection, nearest);
//        }
//    }
    _currentOrderMutex.unlock();
    _enemiesVisibleMutex.unlock();

}


void Unit::startOrder()
{
    clearOrder();
    Order * order  = _pendingOrder.get();
    if(order == nullptr)
    {
        _pendingOrder.clearChange();
        return;
    }
    _pendingOrder.clearChange();
    
    _currentOrderMutex.lock();
    _currentOrder = order;
    _currentOrderMutex.unlock();

    if(order->getOrderType() == MOVE_ORDER)
    {
        MoveOrder * moveOrder = (MoveOrder *) order;
        doMoveOrder(moveOrder);
    }
    else if(order->getOrderType() == OCCUPY_ORDER)
    {
        OccupyOrder * occupy = (OccupyOrder *) order;
        doOccupyOrder(occupy);
    }
    else if(order->getOrderType() == ENTITY_WORK_ORDER || order->getOrderType() == OBJECTIVE_WORK_ORDER)
    {
        WorkOrder * work = (WorkOrder *) order;
        doWorkOrder(work);
    }
    else if(order->getOrderType() == ATTACK_POSITION_ORDER)
    {
        AttackPositionOrder * attack = (AttackPositionOrder *) order;
        doAttackPositionOrder(attack);
    }
    else if(order->getOrderType() == STOP_ORDER)
    {
        clearOrder();
    }
}

void Unit::doWorkOrder(WorkOrder * work)
{
    for(Infantry * member : _unitMembers)
    {
        _membersWorkingOnOrder++;
        member->addGoalFromOrder(work);
    }
}

void Unit::doMoveOrder(MoveOrder * move)
{
    for(Infantry * member : _unitMembers)
    {
        _membersWorkingOnOrder++;
        member->addGoalFromOrder(move);
    }
}

void Unit::doOccupyOrder(OccupyOrder * occupy)
{
    Objective * objective = occupy->getObjective();
    int searchSize = std::max(getDefaultUnitSize().width, getDefaultUnitSize().height);
    Vec2 best = objective->findAreaInObjectiveWithBestCoverValue(searchSize);
    occupy->setPosition(best);
    for(Infantry * member : _unitMembers)
    {
        _membersWorkingOnOrder++;
        member->addGoalFromOrder(occupy);
    }
}

void Unit::doAttackPositionOrder(AttackPositionOrder * attack)
{
    for(Infantry * member : _unitMembers)
    {
        _membersWorkingOnOrder++;
        member->addGoalFromOrder(attack);
    }
}

void Unit::setOrder(Order * order)
{
    _pendingOrder = order;
}

void Unit::memberCompletedGoal(ENTITY_ID associatedOrderId, bool failed)
{
    _currentOrderMutex.lock();
    if(_currentOrder != nullptr && _currentOrder->uID() == associatedOrderId)
    {
        _membersWorkingOnOrder--;
        if(_membersWorkingOnOrder <= 0)
        {
            if(!failed)
            {
                if(_currentOrder->getOrderType() == ENTITY_WORK_ORDER)
                {
                    WorkOrder * workOrder = (WorkOrder *) _currentOrder;
                    // TODO fix if the work happened as part of an objective thing, then find a good cover position in the objective, I think
                    MoveOrder * moveOrder = MoveOrder::create(workOrder->getAverageWorkPosition());
                    _owningCommand->addOrder(moveOrder);
                    setOrder(moveOrder);
                    
                }
                else if(_currentOrder->getOrderType() == OBJECTIVE_WORK_ORDER)
                {
                    ObjectiveWorkOrder * work = (ObjectiveWorkOrder *) _currentOrder;
                    Vec2 position = work->getWorkObjects()->getOwningObjective()->findAreaInObjectiveWithBestCoverValue(12);
                    MoveOrder * moveOrder = MoveOrder::create(position);
                    _owningCommand->addOrder(moveOrder);
                    setOrder(moveOrder);
                }
            }

            _owningCommand->unitCompletedOrder(this, _currentOrder, false);
            _currentOrder = nullptr;
        }
    }
    _currentOrderMutex.unlock();
}

bool Unit::testCollisionWithPoint(Vec2 point)
{
    if(point.getDistance(getAveragePosition()) < 10) { //FIX todo magic number
        return true;
    }
    return false;
}


Human * Unit::findTargetForActor(Infantry * actor)
{
    _enemiesVisibleMutex.lock();
    if(_enemies.size() == 0)
    {
        _enemiesVisibleMutex.unlock();
        return nullptr;
    }
    
    if(actor->isDebugMe())
    {
        globalEngagementManager->requestMainThreadFunction([] () {
            globalDebugHighlighter->clearObjects();
        });
    }
    Vec2 actorPosition = actor->getPosition();
    // Get targets to consider
    // grab up to 20 targets that are within: (1.5 * square distance to minimum target)
    Vector<Human *> possibleTargets;
    Human * target;
    int i = 0;

    // If this actor should try to acquire a random target then pick a random starting index that is greater than ENEMIES_TO_CHECK
    if(_tryRandomEnemiesForInfantry[actor] == true && _enemies.size() > ENEMIES_TO_CHECK)
    {
        i = globalRandom->randomInt(0, _enemies.size() - ENEMIES_TO_CHECK) + ENEMIES_TO_CHECK;
    }

    // Grab ENEMIES_TO_CHECK random targets and toss them in a list of possible targets
    while(possibleTargets.size() < ENEMIES_TO_CHECK && i < _enemies.size())
    {
        target = _enemies.at(i);
        if(target->getActorState() == ALIVE)
        {
            possibleTargets.pushBack(target);
        }
        i++;
    }
          
    // Sort the possible targets by distance
    sort(possibleTargets.begin(), possibleTargets.end(),
         [=](Human * a, Human * b) -> bool
    {
        return actorPosition.distanceSquared(a->getPosition()) < actorPosition.distanceSquared(b->getPosition());
    });
    

    // See if the target is visible to the actor, if so return that target
    for(Human * target : possibleTargets)
    {
        int height;
        if(actor->canSeeOtherHuman(target, height))
        {
            _tryRandomEnemiesForInfantry[actor] = false;
            _enemiesVisibleMutex.unlock();
            return target;
        }
    }
    
    if(actor->isDebugMe())
    {
        globalEngagementManager->requestMainThreadFunction([possibleTargets] () {
            globalDebugHighlighter->clearObjects();
            for(Human * target : possibleTargets)
            {
                globalDebugHighlighter->addObject(target);
            }
        });
    }
    if(actor->isDebugMe())
    {
        LOG_DEBUG_ERROR("No valid target for %lld at %lld \n", actor->uID(), globalEngagementManager->getFrameCount());
    }
    _tryRandomEnemiesForInfantry[actor] = !_tryRandomEnemiesForInfantry[actor];
    
//    LOG_DEBUG_ERROR("No target for %lld \n");
    _enemiesVisibleMutex.unlock();
    return nullptr;
}

bool Unit::isMoving()
{
    return _averageUnitVelocity.x > 0 || _averageUnitVelocity.y > 0;
}

bool Unit::hasOrder()
{
    std::lock_guard<std::mutex> lock(_currentOrderMutex);
    return _currentOrder != nullptr;
}

void Unit::clearOrder()
{
    _currentOrderMutex.lock();
    if(_currentOrder != nullptr)
    {
        _owningCommand->unitCompletedOrder(this, _currentOrder, true);
        _currentOrder = nullptr;
        _membersWorkingOnOrder = 0;
        for(Infantry * member : _unitMembers)
        {
            member->clearUnitGoal();
        }
    }
    _currentOrderMutex.unlock();
}

void Unit::updateUnitStatusDisplay()
{        
    AICommand * ai = (AICommand *) _owningCommand; // TODO FIX 
    
    AIStrategy * strat = ai->strategyForUnit(this);
    if(strat != nullptr && strat->execution != nullptr)
    {
        globalMenuLayer->setUnitStatusVariableForUnit(_id, "Order ID", std::to_string(strat->_strategyID));
        
        globalMenuLayer->setUnitStatusVariableForUnit(_id, "Order Val", std::to_string(strat->importance));
        
        globalMenuLayer->setUnitStatusVariableForUnit(_id, "Order Type", std::to_string((int)strat->_goalType));

        globalMenuLayer->setUnitStatusVariableForUnit(_id, "Order State", strat->execution->stringForExecutionState());
    }
    else
    {
        if(!ai->unitsNotInStrategy.contains(this))
        {
            globalMenuLayer->setUnitStatusVariableForUnit(_id, "Order ID", "SHIT!");
        }
        else
        {
            globalMenuLayer->setUnitStatusVariableForUnit(_id, "Order ID", "None");
        }
        globalMenuLayer->setUnitStatusVariableForUnit(_id, "Order Val", "None");
        
        globalMenuLayer->setUnitStatusVariableForUnit(_id, "Order Type", "None");
        
        globalMenuLayer->setUnitStatusVariableForUnit(_id, "Order State", "None");
    }
    
}

void Unit::removeFromGame()
{
    clearOrder();
    _unitMembers.clear();
    globalGameEventController->handleEventUnitDestroyed(this);
    _owningCommand->removeUnit(this);
    
    _enemiesVisibleMutex.lock();
    _enemies.clear();
    _enemiesVisibleMutex.unlock();
    PlanningObject::removeFromGame();
}


void Unit::populateDebugPanel(GameMenu * menu, Vec2 location)
{
    std::ostringstream stream;
    stream << std::fixed;
    stream.precision(0);
    
    stream.str("");
    stream << "ID: " << uID();
    menu->addText(stream.str());
    
    stream.str("");
    stream << "Position: " << _averageUnitPosition.x << "," << _averageUnitPosition.y;
    menu->addText(stream.str());
    
    stream.str("");
    stream << "Stregth: " << _currentStrength << "/" << _maxStrength;
    menu->addText(stream.str());
    
    stream.str("");
    stream << "Enemy Count: " << _enemies.size();
    menu->addText(stream.str());
    
    stream.str("");
    if(_currentOrder == nullptr)
    {
        stream << "No Order";
    }
    else
    {
        stream << "Order: ";
        switch(_currentOrder->getOrderType())
        {
            case MOVE_ORDER:
            {
                stream << "Move";
                break;
            }
            case OCCUPY_ORDER:
            {
                stream << "Occupy";
                break;
            }
            case OBJECTIVE_WORK_ORDER:
            case ENTITY_WORK_ORDER: {
                stream << "Work";
                break;
            }
            case ATTACK_POSITION_ORDER: {
                stream << "Attack Position";
                break;
            }
            case STOP_ORDER: {
                stream << "Stop";
                break;
            }
        }
    }
    menu->addText(stream.str());
}


void Unit::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    PlanningObject::saveToArchive(archive);
    archive(_hashedEntityName);
    archive(_unitMembers.size());
    for(auto member : _unitMembers)
    {
        archive(member->uID());
    }
    archive(checkSightCounter);
    archive(updateBoundsCounter);
    archive(_enemies.size());
    for(auto enemy : _enemies)
    {
        archive(enemy->uID());
    }
    archive(_tryRandomEnemiesForInfantry.size());
    for(auto pair : _tryRandomEnemiesForInfantry)
    {
        archive(pair.first->uID());
        archive(pair.second);
    }
    
    archive((int) _nearestEnemyDirection);
    if(_unitMembersToRemove.isChanged())
    {
        std::vector<Infantry *> removes = _unitMembersToRemove.get();
        archive(removes.size());
        for(auto inf : removes)
        {
            archive(inf->uID());
        }
    }
    else
    {
        archive((size_t)0);
    }
    
    archive(_sightBounds);
    archive(_boundingRect);
    archive(_defaultUnitSize);
    archive(_unitPosition);
    archive(_averageUnitPosition);
    archive(_averageUnitVelocity);
    archive(_averageUnitStress);
    archive(_averageUnitAmmoPercent);
    archive(_owningCommand->getCommandID());
    archive(_currentStrength);

    if(_pendingOrder.isChanged())
    {
        Order * order = _pendingOrder.get();
        if(order != nullptr)
        {
            archive(order->uID());
        }
        else
        {
            archive((ENTITY_ID) -1);
        }
    }
    else
    {
        archive((ENTITY_ID) -1);
    }

    if(_currentOrder != nullptr)
    {
        archive(_currentOrder->uID());
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
    archive(_membersWorkingOnOrder);
}

void Unit::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    PlanningObject::loadFromArchive(archive);
    archive(_hashedEntityName);
    _unitDefinition = globalEntityFactory->getUnitDefinitionWithHashedName(_hashedEntityName);

    ENTITY_ID thingId;
    size_t size;
    archive(size);
    for(int i = 0; i < size; i++)
    {
        archive(thingId);
        Infantry * inf = dynamic_cast<Infantry *>(globalEntityManager->getEntity(thingId));
        _unitMembers.push_back(inf);
    }
    
    archive(checkSightCounter);
    archive(updateBoundsCounter);
    archive(size);
    for(int i = 0; i < size; i++)
    {
        archive(thingId);
        Human * enemy = dynamic_cast<Human *>(globalEntityManager->getEntity(thingId));
        _enemies.pushBack(enemy);
    }
    archive(size);
    for(int i = 0; i < size; i++)
    {
        bool random;
        archive(thingId);
        archive(random);
        Infantry * inf = dynamic_cast<Infantry *>(globalEntityManager->getEntity(thingId));
        _tryRandomEnemiesForInfantry.emplace(inf, random);
    }
    
    int direction;
    archive(direction);
    _nearestEnemyDirection = (DIRECTION) direction;
    
    archive(size);
    for(int i = 0; i < size; i++)
    {
        archive(thingId);
        Infantry * inf = dynamic_cast<Infantry *>(globalEntityManager->getEntity(thingId));
        _unitMembersToRemove.addValue(inf);
    }
    
    archive(_sightBounds);
    archive(_boundingRect);
    archive(_defaultUnitSize);
    archive(_unitPosition);
    archive(_averageUnitPosition);
    archive(_averageUnitVelocity);
    archive(_averageUnitStress);
    archive(_averageUnitAmmoPercent);
    

    int commandId;
    archive(commandId);
    _owningCommand = globalTeamManager->getCommand(commandId);
    archive(_currentStrength);
    
    archive(thingId);
    if(thingId != -1)
    {
        _pendingOrder = _owningCommand->getOrderWithId(thingId);
    }
    archive(thingId);
    if(thingId != -1)
    {
        _currentOrder = _owningCommand->getOrderWithId(thingId);
    }
    else
    {
        _currentOrder = nullptr;
    }
    archive(_membersWorkingOnOrder);
}


