//
//  TWCommandPost.cpp
//  TrenchWars
//
//  Created by Paul Reed on 2/15/12.
//  Copyright (c) 2012  All rights reserved.
//

#include "CommandPost.h"
#include "Unit.h"
#include "GameVariableStore.h"
#include "EngagementManager.h"
#include "EntityManager.h"
#include "PrimativeDrawer.h"
#include "List.h"
#include "GameStatistics.h"
#include "Random.h"
#include "EntityFactory.h"
#include "MultithreadedAutoReleasePool.h"
#include "CollisionGrid.h"
#include "Command.h"
#include "GameMenu.h"
#include "GameEventController.h"
#include "Team.h"

CommandPost::CommandPost() : Bunker(), requestAmmoCounter(REQUEST_AMMO_CHECK_FREQUENCY)
{
    
}

bool CommandPost::init(Vec2 position, Command * command)
{
    StaticEntityDefinition * definition = (StaticEntityDefinition * ) globalEntityFactory->getPhysicalEntityDefinitionWithName("CommandPost");
    if(definition == nullptr)
    {
        return false;
    }
    Size bunkerSize(globalVariableStore->getVariable(BunkerSizeX),globalVariableStore->getVariable(BunkerSizeY));

    if(Bunker::init(definition, position,bunkerSize, RIGHT ))
    {
        _owningCommand = command;
        _controlRadius = globalVariableStore->getVariable(CommandPostDefaultRadius);
        _primary = false;
        _shocked = false;
        _ammoStockpile = 0;
        _ammoStockpileMax = globalVariableStore->getVariable(MaxCommandPostAmmoStockpile);
        _maxCouriers = 5;
        _lastCourierSpawn = globalEngagementManager->getEngagementTime();
        _recentAmmoUsage = 0;
        _ammoBeingDelivered = 0;
        _ammoDebt = 0;
        _postToSendAmmoTo = nullptr;
        
        _beingCaptured = false;
        _currentCheckTime = globalEngagementManager->getEngagementTime();
        _currentCaptureTime = _currentCheckTime;
        _owningCommand->addCommandPost(this);
        return true;
    }
    return false;
}

CommandPost * CommandPost::create(Vec2 position, Command * command)
{
    CommandPost * newCommandPost = new CommandPost();
    if(newCommandPost && newCommandPost->init(position, command))
    {
        globalAutoReleasePool->addObject(newCommandPost);
        return newCommandPost;
    }
    CC_SAFE_DELETE(newCommandPost);
    return nullptr;
}

CommandPost * CommandPost::create()
{
    CommandPost * newCommandPost = new CommandPost();
    if(newCommandPost)
    {
        globalAutoReleasePool->addObject(newCommandPost);
        return newCommandPost;
    }
    CC_SAFE_DELETE(newCommandPost);
    return nullptr;
}

void CommandPost::postLoad()
{
    StaticEntity::postLoad();
    globalGameEventController->handleEventNewCommandPost(this);
}

void CommandPost::query(float deltaTime)
{
    // determine if the CP is being captured
    std::list<PhysicalEntity *> enemies;
    std::vector<CollisionCell *> cells;
    bool enemyPresent = false;
    
    world->getCollisionGrid()->cellsInCircle(getPosition(), globalVariableStore->getVariable(CommandPostCaptureRadius), &cells);
    for(auto cell : cells)
    {
        for(auto ent : cell->getMovingEntsInCell())
        {
            if(ent.second->getTeamID() != _owningCommand->getTeamId())
            {
                enemyPresent =  true;
                break;
            }
        }
        if(enemyPresent)
        {
            break;
        }
    }
    
    if(enemyPresent)
    {
        //        receiveDamage(globalVariableStore->getVariable(CommandPostDamagePerSecond) * deltaTime);
        _beingCaptured = true;
        if(_health <= 0)
        {
            removeFromGame();
        }
    }
    else
    {
        _health += globalVariableStore->getVariable(CommandPostHealPerSecond) * deltaTime;
        if(_health >= _maxHealth)
        {
            _shocked = false;
            _health = _maxHealth;
        }
        _beingCaptured = false;
    }
}
void CommandPost::act(float deltaTime)
{

    if(_couriers.size() < _maxCouriers)
    {
        if((globalEngagementManager->getEngagementTime() - _lastCourierSpawn) > globalVariableStore->getVariable(CourierSpawnRate))
        {
            Courier * courier = Courier::create("courier", _position, _owningCommand, this); //TODO fix need to determine the correct position for the spawning courier
            _couriers.pushBack(courier);
            _lastCourierSpawn = globalEngagementManager->getEngagementTime();
        }
    }
    
    if(_postToSendAmmoTo != nullptr)
    {
        sendAmmoToPost();
    }
    sendPackagesInQueue();
    
    if(!_primary && requestAmmoCounter.count())
    {
        requestAmmoFromNeighbors();
    }
    
    if(_recentAmmoUsage > 0)
    {
        _recentAmmoUsage -= globalVariableStore->getVariable(CommandPostRecentAmmoDecay) * deltaTime;
    }
    
    
}

void CommandPost::setControlRadius(int radius)
{
    _controlRadius = radius;
}

float CommandPost::getCreationRadius()
{
    return _controlRadius * globalVariableStore->getVariable(CommandPostCreationRadius);
}

void CommandPost::setIsPrimary(bool primary)
{
    _primary = primary;
    if(_primary)
    {
        _ammoStockpileMax *= 3;
    }
}


void CommandPost::updateLinking()
{
    _linkedCommandPosts.clear();
    _linkStepsToPrimaryCommandPost = -1; //no link
    
    if(_shocked)
    {
        return;
    }

    for(CommandPost * otherPost : _owningCommand->getCommandPosts())
    {
        if(otherPost == this)
        {
            continue;
        }
        float distance = getPosition().distance(otherPost->getPosition());
        if(distance < _controlRadius * 2)
        {
            _linkedCommandPosts.pushBack(otherPost);
        }
    }
}

void CommandPost::setLinkStepsToPrimaryCommandPost(int linkLevel)
{
    if(_shocked)
    {
        return;
    }
    if(linkLevel < _linkStepsToPrimaryCommandPost || _linkStepsToPrimaryCommandPost == -1)
    {
        _linkStepsToPrimaryCommandPost = linkLevel;
        for(CommandPost * linkedPost : _linkedCommandPosts)
        {
            linkedPost->setLinkStepsToPrimaryCommandPost(_linkStepsToPrimaryCommandPost + 1);
        }
    }
}

void CommandPost::depositAmmo(int ammo)
{
    _ammoStockpile += ammo;
    if(_ammoStockpile > _ammoStockpileMax)
    {
        _ammoStockpile = _ammoStockpileMax;
    }
    _ammoBeingDelivered -= ammo;
    if(_ammoBeingDelivered < 0)
    {
        _ammoBeingDelivered = 0;
    }
}

int CommandPost::withdrawAmmo(int requestedAmount, bool forTransfer)
{
    int returnedAmmo = 0;
    if(_ammoStockpile.load() < requestedAmount)
    {
        returnedAmmo = _ammoStockpile.load();
        _ammoStockpile = 0;
    }
    else
    {
        _ammoStockpile -= requestedAmount;
        returnedAmmo = requestedAmount;
    }
    
    if(forTransfer)
    {
        _ammoDebt -= requestedAmount;
        if(_ammoDebt < 0)
        {
            _ammoDebt = 0;
        }
    }
    else
    {
        _recentAmmoUsage += returnedAmmo;
    }
    
    return returnedAmmo;
}


void CommandPost::requestAmmoFromNeighbors()
{
    int ammoNeeded = getAmmoNeeded();
    if(ammoNeeded <= 0)
    {
        return;
    }
    
    // Find couries available
    std::list<Courier *> availableCouriers;
    std::copy_if(_couriers.begin(), _couriers.end(),
                 std::back_inserter(availableCouriers),
                 [=](Courier * courier) {
        return courier->getCourierStatus() == COURIER_WAITING;
    });
    
    if(availableCouriers.size() == 0)
    {
        // No available couriers, give up
        return;
    }
    
    std::vector<CommandPost *> possiblePosts;
    int supply = getAmmoSupplyWeighting();
    
    // Create a list of CPs that can possible send this CP ammo
    // Can't request ammo from CPs with no supply OR CPs who are using more ammo than this CP
    std::copy_if(_linkedCommandPosts.begin(), _linkedCommandPosts.end(),
     std::back_inserter(possiblePosts),
     [=](CommandPost * post) {
        return (post->getAmmoSupplyWeighting() /2) > supply;
    });
    
    if(possiblePosts.size() == 0)
    {
        // No neighboring posts that can send us ammo
        return;
    }
    
    // Now generate weights for each CP based on their available  ammo, current ammo usage, and how close to the primary CP they are
    float weightPerCourier = 0;
    for(CommandPost * possiblePost : possiblePosts)
    {
        weightPerCourier += possiblePost->getAmmoSupplyWeighting();
    }
    
    weightPerCourier /= availableCouriers.size();
    
    
    // Sort the possible CPs by weight
    std::sort(possiblePosts.begin(), possiblePosts.end(), [=] (CommandPost * a, CommandPost * b) {
        return a->getAmmoSupplyWeighting() > b->getAmmoSupplyWeighting();
    });
    
    int currentPost = 0;

    int ammoPerCourier = globalVariableStore->getVariable(CourierAmmoCapacity);
    while(ammoNeeded > 0 && currentPost < possiblePosts.size())
    {
        float weight = possiblePosts[currentPost]->getAmmoSupplyWeighting();
        int currentPostAmmo = possiblePosts[currentPost]->getAmmoStockpile();
        while(availableCouriers.size() > 0 && weight >= weightPerCourier && currentPostAmmo > 0 && ammoNeeded > 0)
        {
            possiblePosts[currentPost]->promiseAmmoWithdrawal(ammoPerCourier);

            AmmoPackage * ammo = AmmoPackage::create( possiblePosts[currentPost], this, ammoPerCourier, true, _owningCommand);
            availableCouriers.front()->transportPackage(ammo);
            availableCouriers.pop_front();
            ammoNeeded -= ammoPerCourier;
            currentPostAmmo -= ammoPerCourier;
            weight -= weightPerCourier;
            _ammoBeingDelivered += ammoPerCourier;
        }
        currentPost++;
    }
    
//    possiblePosts
}

void CommandPost::promiseAmmoDeposit(int amount)
{
    _ammoBeingDelivered += amount;
    if(_ammoBeingDelivered < 0)
    {
        _ammoBeingDelivered = 0;
    }
}

void CommandPost::promiseAmmoWithdrawal(int amount)
{
    _ammoDebt += amount;
}

float CommandPost::getAmmoNeeded()
{
    return _ammoStockpileMax - _ammoStockpile - _ammoBeingDelivered;
}


float CommandPost::getAmmoSupplyWeighting()
{
    float availableAmmo = _ammoStockpile - _ammoDebt;
    // Lower weighting if this post is currently using a lot of ammo (don't send ammo away if its being used here)
    availableAmmo -=  _recentAmmoUsage * 5; // FIX magic number
    // Lower weighting for posts a long ways from the primary (posts at the front shouldn't often transfer directly ammo to each other, instead they should draw upon ammo further up the command post chain)
    availableAmmo /= pow(_linkStepsToPrimaryCommandPost, 1.5); //FIXME: magic number
    
    //
    availableAmmo = availableAmmo < 0 ? 0 : availableAmmo;
    return availableAmmo;
}

void CommandPost::sendPackagesInQueue()
{
    _packagesMutex.lock();
    if(_packagesToSend.size() > 0)
    {
        for(Courier * courier : _couriers)
        {
            if(courier->getCourierStatus() == COURIER_WAITING)
            {
                courier->transportPackage(_packagesToSend.front());
                _packagesToSend.popFront();
                break;
            }
        }
    }
    _packagesMutex.unlock();

}

void CommandPost::sendOrderToUnit(Order * order, Unit * unit)
{
    _packagesMutex.lock();
    OrderPackage * orders = OrderPackage::create(this, unit, order, _owningCommand);
    _packagesToSend.pushBack(orders);
    _packagesMutex.unlock();
}

void CommandPost::requestAmmoForPost(CommandPost * post)
{
    _postToSendAmmoTo = post;
}

void CommandPost::sendAmmoToPost()
{
    CommandPost * post = _postToSendAmmoTo;
    _postToSendAmmoTo = nullptr;
    if(_ammoStockpile.load() <= 0 || post->getAmmoNeeded() <= 0)
    {
        //Don't have any ammo to send or the destination post doesn't need any
        return;
    }
    std::list<Courier *> availableCouriers;
    std::copy_if(_couriers.begin(), _couriers.end(),
                 std::back_inserter(availableCouriers),
                 [=](Courier * courier) {
        return courier->getCourierStatus() == COURIER_WAITING;
    });
    
    if(availableCouriers.size() == 0)
    {
        // No couriers to run the ammo over
        return;
    }
    
    // Create a new ammo package and give it to the first available courier
    int ammo = globalVariableStore->getVariable(CourierAmmoCapacity);
    ammo = ammo <= _ammoStockpile.load() ? ammo : _ammoStockpile.load();
    AmmoPackage * ammoPackage = AmmoPackage::create(this, post,ammo, false, _owningCommand);
    _ammoStockpile -= ammo;
    post->promiseAmmoDeposit(ammo);
    availableCouriers.front()->transportPackage(ammoPackage);
}

void CommandPost::courierDied(Courier * courier)
{
    _couriers.eraseObject(courier);
    Package * package = courier->getPackage();
    if(package != nullptr)
    {
        if(package->getPackageType() == ORDER_PACKAGE)
        {
            OrderPackage * order = (OrderPackage *) package;
            sendOrderToUnit(order->getOrder(), order->getUnit());
        }
        else if(package->getPackageType() == AMMO_PACKAGE)
        {
            AmmoPackage * ammo = (AmmoPackage *) package;
            ammo->getAmmoDestination()->promiseAmmoDeposit(-ammo->getAmmoAmount()); // remove the promised amount
            if(!ammo->wasAmmoPickedUp())
            {
                ammo->getOrigin()->promiseAmmoWithdrawal(-ammo->getAmmoAmount()); // No longer going to withdrawal
            }
        }
    }
}

void CommandPost::populateDebugPanel(GameMenu * menu, Vec2 location)
{
    std::ostringstream stream;
    stream << std::fixed;
    stream.precision(0);
    
    stream.str("");
    stream << "Stockpile: " << _ammoStockpile.load();
    menu->addText(stream.str());
    
    stream.str("");
    stream << "Debt: " << _ammoDebt.load();
    menu->addText(stream.str());
    
    stream.str("");
    stream << "Promised: " << _ammoBeingDelivered.load();
    menu->addText(stream.str());
}

void CommandPost::removeFromGame()
{
    _owningCommand->removeCommandPost(this);
    globalGameEventController->handleEventCommandPostDestroyed(this);
    Bunker::removeFromGame();
}

void CommandPost::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Bunker::saveToArchive(archive);
    archive( _controlRadius );
    archive( _primary );
    archive( workUntilCreated);
    archive( _currentCaptureTime);
    archive( _currentCheckTime);
    archive( _beingCaptured);
    archive( _shocked);
    archive( _ammoStockpile.load());
    archive( _ammoBeingDelivered.load());
    archive( _ammoDebt.load());
    if(_postToSendAmmoTo != nullptr)
    {
        archive( _postToSendAmmoTo->uID());
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
    
    archive( _ammoStockpileMax);
    archive( _recentAmmoUsage);
    
    archive( _maxCouriers);
    archive( _lastCourierSpawn);
    archive( _linkStepsToPrimaryCommandPost);
    
    
    archive( _linkedCommandPosts.size());
    for(auto post : _linkedCommandPosts)
    {
        archive(post->uID());
    }
    
    archive( _packagesToSend.size());
    for(auto package : _packagesToSend)
    {
        archive((int) package->getPackageType());
        package->saveToArchive(archive);
    }
    
    archive( _couriers.size());
    for(auto courier : _couriers)
    {
        archive(courier->uID());
    }
}

void CommandPost::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Bunker::loadFromArchive(archive);
    
    archive( _controlRadius );
    archive( _primary );
    archive( workUntilCreated);
    archive( _currentCaptureTime);
    archive( _currentCheckTime);
    archive( _beingCaptured);
    archive( _shocked);
    archive( _ammoStockpile.load());
    archive( _ammoBeingDelivered.load());
    archive( _ammoDebt.load());
    
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        _postToSendAmmoTo = dynamic_cast<CommandPost *>(globalEntityManager->getEntity(thingId));
    }
    else
    {
        _postToSendAmmoTo = nullptr;
    }
    
    archive( _ammoStockpileMax);
    archive( _recentAmmoUsage);
    
    archive( _maxCouriers);
    archive( _lastCourierSpawn);
    archive( _linkStepsToPrimaryCommandPost);

    size_t size;
    archive( size);
    for(int i = 0; i < size; i++)
    {
        archive(thingId);
        CommandPost * post = dynamic_cast<CommandPost *>(globalEntityManager->getEntity(thingId));
        _linkedCommandPosts.pushBack(post);
    }
    
    int packageType;
    archive( size);
    for(int i = 0; i < size; i++)
    {
        archive(packageType);
        Package * package;
        if(packageType == AMMO_PACKAGE)
        {
            package = AmmoPackage::create();
            package->loadFromArchive(archive);
        }
        else if(packageType == ORDER_PACKAGE)
        {
            package = OrderPackage::create();
            package->loadFromArchive(archive);
        }
        _packagesToSend.pushBack(package);
    }
    
    archive( size);
    for(int i = 0; i < size; i++)
    {
        archive(thingId);
        Courier * courier = dynamic_cast<Courier *>(globalEntityManager->getEntity(thingId));
        _couriers.pushBack(courier);
        
    }
    _owningCommand->addCommandPost(this);
}

