//
//  OverlayDrawer.cpp
//  TrenchWars
//
//  Created by Paul Reed on 6/18/21.
//

#include "OverlayDrawer.h"
#include "PlayerLayer.h"


OverlayDrawer::OverlayDrawer()
{
    
}

OverlayDrawer::~OverlayDrawer()
{
    
}

bool OverlayDrawer::init(Color4F backgroundColor)
{
    _visible = true;
    _holeDrawer = DrawNode::create();
    _holeDrawer->retain();
    _backgroundDrawer = DrawNode::create();
    _backgroundDrawer->retain();
    _backgroundColor = backgroundColor;
    
    _clippingNode = ClippingNode::create();
    _clippingNode->addChild(_backgroundDrawer);
    _clippingNode->setInverted(true);
    _clippingNode->setStencil(_holeDrawer);
    _clippingNode->retain();
    
    _holeColor.r = 1;
    _holeColor.b = 1;
    _holeColor.g = 1;
    _holeColor.a = .1;
    globalPlayerLayer->addChild(_clippingNode, 0); //1000
    return true;
}

OverlayDrawer * OverlayDrawer::create(Color4F backgroundColor)
{
    OverlayDrawer * newDrawer = new OverlayDrawer();
    if(newDrawer->init(backgroundColor))
    {
        newDrawer->autorelease();
        return newDrawer;
    }
    CC_SAFE_DELETE(newDrawer);
    return nullptr;
}

void OverlayDrawer::drawOverlay()
{
    _backgroundDrawer->clear();
    _holeDrawer->clear();
    
    if(!_visible)
    {
        return;
    }

    _backgroundDrawer->drawSolidRect(Vec2(-100000,-100000), Vec2(100000,100000), _backgroundColor);

    for(auto hole : _holes)
    {
        _holeDrawer->drawSolidPoly(&hole[0], hole.size(), _holeColor);
    }
}

void OverlayDrawer::addHole(std::vector<Vec2> & hole)
{
    _holes.push_back(hole);
    drawOverlay();
}

void OverlayDrawer::setVisible(bool visible)
{
    _visible = visible;
    drawOverlay();
}
