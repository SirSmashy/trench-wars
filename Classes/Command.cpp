//
//  Command.m
//  TrenchWars
//
//  Created by Paul Reed on 1/29/12.
//  Copyright (c) 2012. All rights reserved.
//

#include "Command.h"
#include "CommandPost.h"
#include "Unit.h"
#include "EngagementManager.h"
#include "EntityManager.h"
#include "GameEventController.h"
#include "GameVariableStore.h"
#include "Objective.h"
#include "CollisionGrid.h"
#include "CollisionCell.h"
#include "EntityFactory.h"
#include "MultithreadedAutoReleasePool.h"
#include "Order.h"
#include "MapSector.h"
#include "Team.h"
#include "Objective.h"
#include "TeamManager.h"

Command::Command()
{

}

void Command::init(Team * owningTeam, CommandDefinition * definition)
{
    if(definition != nullptr)
    {
        _commandID = definition->commandId;
        _spawnPosition = definition->spawnPoint;
        _supplyAccumulationRate = definition->supplyRate;
    }
    _owningTeam = owningTeam;
    _primaryCommandPost = nullptr;
    _primativeDrawer = PrimativeDrawer::create();
    _pendingSupply = 0;
    _debugSpawnMode = SPAWN_DO_NOTHING;
}

Command * Command::create(Team * owningTeam, CommandDefinition * definition)
{
    Command * command = new Command();
    command->init(owningTeam, definition);
    globalAutoReleasePool->addObject(command);
    return command;
}

int Command::getTeamId()
{
    return _owningTeam->getTeamId();

}
 
void Command::spawnInitialUnits(CommandDefinition * commandDefinition)
{
    for(CommandPointSpawn * commandSpawn : commandDefinition->commandPoints)
    {
        CommandPost * newPost = CommandPost::create(commandSpawn->location, this);
        if(_primaryCommandPost == nullptr)
        {
            Objective::create(_owningTeam, newPost); //Each Post also gets an objective
            newPost->setIsPrimary(true);
            newPost->depositAmmo(commandDefinition->initialSupply);
            _primaryCommandPost = newPost;
        }
    }
    _owningTeam->updateCommandPostLinking();
    for(FormationSpawn * formation : commandDefinition->formations)
    {
        spawnFormation(formation->formationDefinitionName, formation->location);
    }
}

void Command::query(float deltaTime)
{
    _pendingSupply += _supplyAccumulationRate * deltaTime;
    if(_primaryCommandPost != nullptr && _pendingSupply > 1)
    {
        _primaryCommandPost->depositAmmo(_pendingSupply);
        _pendingSupply = 0;
    }
}

void Command::addCommandPost(CommandPost * post)
{
    std::lock_guard<std::mutex> lock(_entitiesMutex);
    _commandPosts.pushBack(post);
    _owningTeam->updateCommandPostLinking();
}

void Command::removeCommandPost(CommandPost * post)
{
    std::lock_guard<std::mutex> lock(_entitiesMutex);
    if(_primaryCommandPost == post)
    {
        _primaryCommandPost = nullptr;
    }
    else
    {
        _commandPosts.eraseObject(post);
    }
    _owningTeam->updateCommandPostLinking();
}

void Command::addUnit(Unit * unit)
{
    std::lock_guard<std::mutex> lock(_entitiesMutex);
    _units.pushBack(unit);
    globalEngagementManager->requestMainThreadFunction( [this, unit] () {
        doSpawnOrderForUnit(unit);
    });
}


void Command::removeUnit(Unit * unit)
{
    std::lock_guard<std::mutex> lock(_entitiesMutex);
    _units.eraseObject(unit);
}

void Command::addOrder(Order * order)
{
    std::lock_guard<std::mutex> lock(_ordersMutex);
    _orders.insert(order->uID(), order);
}

Order * Command::getOrderWithId(ENTITY_ID orderId)
{
    return _orders.at(orderId);
}

void Command::unitCompletedOrder(Unit * unit, Order * order, bool failed)
{
    globalEngagementManager->requestMainThreadFunction([this, unit,order,failed] {
        globalGameEventController->handleEventOrderComplete(unit, order, failed);
        removeOrder(order);
    });
}

void Command::removeOrder(Order * order)
{
    std::lock_guard<std::mutex> lock(_ordersMutex);
    _orders.erase(order->uID());
}

void Command::spawnFormation(const std::string & formationName, Vec2 location)
{
    FormationDefinition * def = globalEntityFactory->getFormationDefinition(formationName);
    if(def != nullptr)
    {
        for(auto unitDef : def->units)
        {
            Vec2 position = location + unitDef->location;
            position = world->getCollisionGrid()->getValidMoveCellAroundPosition(position);
            Unit * unit = Unit::create(unitDef->unitDefinitionName, position, this);
        }
    }
}

void Command::unitsInRect(Rect rect, Vector<Unit *> & units)
{
    for(Unit * unit : _units)
    {
        if(rect.containsPoint( unit->getAveragePosition() ))
        {
            units.pushBack(unit);
        }
    }
}


CommandPost * Command::getCommandPostAtPoint(Vec2 point)
{
    for(CommandPost * post: _commandPosts)
    {
        if(post->getCollisionBounds().containsPoint(point))
        {
            return post;
        }
    }
    return nullptr;
}

Vec2 Command::getValidCommandPostPositionTowardPosition(Vec2 position)
{
    CommandPost * nearest = getNearestCommandPost(position);
    double distance = nearest->getPosition().distance(position);
    Vec2 orderPostion = nearest->getPosition().lerp(position, 500 / distance);
    return orderPostion;
}

CommandPost * Command::getNearestCommandPost(Vec2 position, bool onlyLinked)
{
    CommandPost * nearest = nullptr;
    double distance;
    for(CommandPost * post : _commandPosts)
    {
        if(onlyLinked && !post->isLinkedToPrimaryCommandPost())
        {
            continue;
        }
        if(nearest == nullptr)
        {
            nearest = post;
            distance = post->getPosition().distance(position);
            continue;
        }
        else if(post->getPosition().distance(position) < distance)
        {
            nearest = post;
            distance = post->getPosition().distance(position);
        }
    }
    return nearest;
}


bool Command::isPositionInCommandArea(Vec2 position)
{
    position = world->getAboveGroundPosition(position);
    for(CommandPost * post : _commandPosts)
    {
        if(!post->isLinkedToPrimaryCommandPost())
        {
            continue;
        }
        if(position.distance(post->getPosition()) < post->getControlRadius())
            return true;
    }
    return false;
}

bool Command::isPositionValidForNewCommandPost(Vec2 position)
{
    bool withinCreationZone = false;
    for(CommandPost * post : _commandPosts)
    {
        float distance = position.distance(post->getPosition());
        if(distance < post->getCreationRadius())
        {
            return false;
        }
        if(distance < (post->getControlRadius() * 2)) // multiply by 2 because the new command post's control radius must overlap with this command post's control radius
        {
            withinCreationZone = true;
        }
    }
    
    return withinCreationZone;
}

void Command::updateCommandPostLinking()
{
    for(CommandPost * post : _commandPosts)
    {
        post->updateLinking();
    }
    if(_primaryCommandPost != nullptr)
    {
        _primaryCommandPost->setLinkStepsToPrimaryCommandPost(0);
    }
}

int Command::withdrawAmmo(PhysicalEntity * requestingEntity, int requestedAmount)
{
    CommandPost * commandPost = getNearestCommandPost(requestingEntity->getPosition());
    if(commandPost == nullptr)
    {
        return 0;
    }
    return commandPost->withdrawAmmo(requestedAmount);
}

bool Command::isLocalPlayer()
{
    return _commandID == globalTrenchWarsManager->getLocalPlayerId();
}

int Command::getTotalAmmo()
{
    int totalAmmo = 0;
    for(CommandPost * post : _commandPosts)
    {
        totalAmmo += post->getAmmoStockpile();
        
        for(Courier * courier : post->getCouriers())
        {
            if(courier->getPackage() != nullptr && courier->getPackage()->getPackageType() == AMMO_PACKAGE)
            {
                AmmoPackage * ammo = (AmmoPackage *) courier->getPackage();
                if(ammo->wasAmmoPickedUp())
                {
                    totalAmmo += ammo->getAmmoAmount();
                }
            }
        }
    }
    return totalAmmo;
}


void Command::doSpawnOrderForUnit(Unit * unit)
{
    switch(_debugSpawnMode)
    {
        case SPAWN_DO_NOTHING:
        {
            break;
        }
        case SPAWN_DIG_IN: {
            // TODO fix do a thing here
            break;
        }
        case SPAWN_ATTACK_CP:
        {
            Command * enemy = nullptr;
            // Lol, so dumb
            if(_commandID == 1)
            {
                enemy = globalTeamManager->getCommand(0);
            }
            else
            {
                enemy = globalTeamManager->getCommand(1);
            }
            
            Vec2 position = enemy->getPrimaryCommandPost()->getPosition();
            position += Vec2(globalRandom->randomInt(-400, 400), globalRandom->randomInt(-400, 400));
            position = world->getCollisionGrid()->getValidMoveCellAroundPosition(position);
            
            globalGameEventController->issueMoveOrder(position, unit, nullptr);
            break;
        }
        case SPAWN_GO_LEFT: {
            Vec2 position = unit->getAveragePosition();
            position.x -= 5000;
            if(position.x < 0)
            {
                position.x = 1;
            }
            globalGameEventController->issueMoveOrder(position, unit, nullptr);
            break;
        }
        case SPAWN_GO_RIGHT: {
            Vec2 position = unit->getAveragePosition();
            position.x += 5000;
            if(position.x >= world->getWorldSize().width)
            {
                position.x = world->getWorldSize().width -1;
            }
            
            globalGameEventController->issueMoveOrder(position, unit, nullptr);
            break;
        }
    }
}


void Command::shutdown()
{
    _primaryCommandPost = nullptr;
    _commandPosts.clear();
    _units.clear();
}

void Command::saveBasicCommandInfoToArchive(cereal::BinaryOutputArchive & archive)
{
    archive(_commandID);
    archive(_supplyAccumulationRate);
    archive(_pendingSupply);
    archive(_spawnPosition);
    if(_primaryCommandPost != nullptr)
    {
        archive(_primaryCommandPost->uID());
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
}

void Command::saveOrdersToArchive(cereal::BinaryOutputArchive & archive)
{
    archive(_orders.size());
    for(auto order : _orders)
    {
        archive(order.second->getOrderType());
        archive(order.first);
    }
    for(auto order : _orders)
    {
        order.second->saveToArchive(archive);
    }
}

void Command::saveToArchive(cereal::BinaryOutputArchive & archive)
{
    saveBasicCommandInfoToArchive(archive);
    saveOrdersToArchive(archive);
}


void Command::loadFromArchive(cereal::BinaryInputArchive & inputArchive)
{
    loadBasicCommandInfoFromArchive(inputArchive);
    loadOrdersFromArchive(inputArchive);
}


void Command::loadBasicCommandInfoFromArchive(cereal::BinaryInputArchive & archive)
{
    archive(_commandID);
    archive(_supplyAccumulationRate);
    archive(_pendingSupply);
    archive(_spawnPosition);
    ENTITY_ID commandPostId;
    archive(commandPostId);
    
    // This shoooould work, hopefully
    if(commandPostId != -1)
    {
        globalEngagementManager->requestMainThreadFunction([this, commandPostId] () {
            _primaryCommandPost = (CommandPost *) globalEntityManager->getEntity(commandPostId);
            _owningTeam->updateCommandPostLinking();
        });
    }

    
}

void Command::loadOrdersFromArchive(cereal::BinaryInputArchive & archive)
{
    _primaryCommandPost = nullptr;
    _primativeDrawer = PrimativeDrawer::create();
    
    size_t size;
    archive(size);
    
    ORDER_TYPE type;
    ENTITY_ID orderId;
    std::vector<Order *> ordersToAdd;
    
    for(int i = 0; i < size; i++)
    {
        archive(type);
        archive(orderId);
        
        Order * order = Order::createOrderOfType(type);
        ordersToAdd.push_back(order);
        _orders.insert(orderId, order);
    }
    
    for(auto order : ordersToAdd)
    {
        order->loadFromArchive(archive);
    }
}
