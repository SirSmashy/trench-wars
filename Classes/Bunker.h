//
//  Bunker.hpp
//  TrenchWars
//
//  Created by Paul Reed on 9/5/21.
//

#ifndef Bunker_h
#define Bunker_h

#include "StaticEntity.h"


class CollisionGrid;
class NavigationPatch;
class WorldSection;

class Bunker: public StaticEntity
{
protected:
    Size _bunkerSize;
    AnimatedSprite * _bunkerExitSprite;
    WorldSection * _undergroundArea;

    Bunker();
    virtual bool init(StaticEntityDefinition * definition, Vec2 position, Size bunkerSize, DIRECTION entranceFacing);
    virtual bool init(Vec2 position, Size bunkerSize, DIRECTION entranceFacing);

public:
    
    static Bunker * create(Vec2 position, Size bunkerSize, DIRECTION entranceFacing);
    static Bunker * create();

    
    virtual ENTITY_TYPE getEntityType() const {return BUNKER;}
    void setUndergroundArea(WorldSection * area) {_undergroundArea = area;}
    WorldSection * getUndergroundArea() {return _undergroundArea;}

    virtual void removeFromGame();
    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
};

#endif /* Bunker_h */
