//
//  Transporter.hpp
//  TrenchWars
//
//  Created by Paul Reed on 1/13/22.
//

#ifndef Courier_h
#define Courier_h

#include "cocos2d.h"
#include "Human.h"
#include "EntityDefinitions.h"
#include "Package.h"


class CommandPost;


enum COURIER_STATUS
{
    COURIER_WAITING,
    COURIER_TRAVELLING,
    COURIER_RETURNING
};



//
class Courier : public Human
{
    CC_CONSTRUCTOR_ACCESS :
    Courier();
    virtual bool init(HumanDefinition * definition, Vec2 position, Command * command, CommandPost * owningPost);
    
private:
    Package * _packageToDeliver;
    CommandPost * _owningPost;
    COURIER_STATUS _status;
    
    Vec2 getCourierWaitPosition();

    
public:
    static Courier * create(HumanDefinition * definition, Vec2 position, Command * command, CommandPost * owningPost);
    static Courier * create(const std::string & definitionName, Vec2 position, Command * command, CommandPost * owningPost);
    static Courier * create();

    virtual ENTITY_TYPE getEntityType() const {return COURIER;}

    virtual void query(float deltaTime);

    Package * getPackage() {return _packageToDeliver;}
    CommandPost * getOwningPost() {return _owningPost;}

    
    COURIER_STATUS getCourierStatus() {return _status;}
    void transportPackage(Package * package);
    
    virtual void die();
        
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;

};

#endif /* Courier_h */
