//
//  PermanentParticleSystem.cpp
//  TrenchWars
//
//  Created by Paul Reed on 7/20/22.
//

#include "PermanentParticleSystem.h"

PermanentParticleSystem::~PermanentParticleSystem()
{
}

PermanentParticleSystem::PermanentParticleSystem() : cocos2d::ParticleSystemQuad()
{
    _created = false;
}

PermanentParticleSystem * PermanentParticleSystem::createWithTotalParticles(int numberOfParticles)
{
    PermanentParticleSystem * system = new PermanentParticleSystem();
    
    if(system->initWithTotalParticles(numberOfParticles))
    {
        system->autorelease();
        return system;
    }
    
    CC_SAFE_DELETE(system);
    return nullptr;
}


PermanentParticleSystem * PermanentParticleSystem::create(const std::string& filename)
{
    PermanentParticleSystem * system =  new PermanentParticleSystem();
    if (system && system->initWithFile(filename))
    {
        system->autorelease();
        return system;
    }
    CC_SAFE_DELETE(system);
    return system;
}

void PermanentParticleSystem::update(float deltaTime)
{
    if(!_created)
    {
        ParticleSystemQuad::update(1000);
        _created = true;
        unscheduleUpdate();
    }
}

void PermanentParticleSystem::stop()
{
    this->unscheduleUpdate();
    _parent->removeChild(this, true);
}
