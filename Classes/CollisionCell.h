#ifndef __COLLISION_CELL_H__
#define __COLLISION_CELL_H__
//
//  CollisionCell.h
//  TrenchWars
//
//  Created by Paul Reed on 2/25/12.
//  Copyright (c) 2012 . All rights reserved.
//

#include "cocos2d.h"
#include "Refs.h"
#include "MovingPhysicalEntity.h"
#include "TerrainCharacteristics.h"
#include <unordered_set>

USING_NS_CC;

enum COLLISION_TYPES
{
    COLLIDE_OWN_TEAM_HUMAN = 1,
    COLLIDE_OTHER_TEAM_HUMAN = 2,
    COLLIDE_GROUND = 4,
    COLLIDE_STATIC_OBJECT = 8,
    COLLIDE_LANDSCAPE_OBJECT = 16,
    COLLIDE_NOT_TRAVERSABLE = 32
};

#define COLLIDE_ALL 63
#define COLLIDE_ALL_BUT_OWN_TEAM 62
#define COLLIDE_LANDSCAPE 12
#define COLLIDE_OTHER_TEAM_AND_STATIC_OBJECTS 10
#define COLLIDE_OWN_TEAM_AND_STATIC_OBJECTS  9
#define COLLIDE_ALL_HUMANS 3

#define COLLIDE_FOR_TARGETING COLLIDE_OWN_TEAM_HUMAN | COLLIDE_STATIC_OBJECT | COLLIDE_LANDSCAPE_OBJECT
#define COLLIDE_FOR_PROJECTILE_IMPACT COLLIDE_ALL_HUMANS | COLLIDE_STATIC_OBJECT | COLLIDE_LANDSCAPE_OBJECT


class NavigationPatch;
class MapSector;
class StaticEntity;
class PolygonLandscapeObject;
class CollisionGrid;

typedef std::function<bool (COLLISION_TYPES, Entity *)> LINE_COLLISION_CALLBACK;


class CollisionCell : public ComparableRef
{
private:
    std::unordered_map<ENTITY_ID, MovingPhysicalEntity *> _movingEntsInCell;
    NavigationPatch * _navigationPatch;

    GROUNDTYPE _groundType;
    Rect _cellOrigin;
    unsigned short _cellIndexX;
    unsigned short _cellIndexY;
    
    unsigned short _concealmentInCell;
    
    std::unordered_map<ENTITY_ID, unsigned short> _concealmentFromEntityMap;
        
    char _congestion [10];
    std::mutex _congestionMutex;
    
    std::recursive_mutex _entitiesMutex;
    StaticEntity * _staticEntityInCell;
    PolygonLandscapeObject * _landscapeObjectInCell;
    
    TerrainCharacteristics _terrainCharacteristics;
    MapSector * _mapSector;

public:
    CollisionCell(Rect origin, int xIndex, int yIndex);
    void addMovingPhysicalEntity(MovingPhysicalEntity * ent);
    void removeMovingPhysicalEntity(MovingPhysicalEntity * ent);
    
    void setStaticEntity(StaticEntity * entity);
    void setLandscapeObject(PolygonLandscapeObject * entity);
    bool isTraversable();
    
    TerrainCharacteristics getTerrainCharacteristics() {return _terrainCharacteristics;}
    bool hasTrench();

    // Cell characteristics
    StaticEntity * getStaticEntity() {return _staticEntityInCell;}
    PolygonLandscapeObject * getLandscapeObject() {return _landscapeObjectInCell;}

    inline Vec2 getCellPosition() {return _cellOrigin.origin;}
    inline Vec2 getMiddleOfCellPosition() {return _cellOrigin.origin + Vec2(.5,.5);}
    inline Vec2 getTopRightCellPosition() {return _cellOrigin.origin + Vec2(0.98,0.98);}
    inline Vec2 getTopLeftCellPosition() {return _cellOrigin.origin + Vec2(0.0 ,0.98);}
    inline Vec2 getBottomRightCellPosition() {return _cellOrigin.origin + Vec2(0.98, 0);}
    inline Size getCellSize() {return _cellOrigin.size;}
    inline Rect getCellRect() {return _cellOrigin;}
    inline unsigned short getXIndex() {return _cellIndexX;}
    inline unsigned short getYIndex() {return _cellIndexY;}
    
    inline unsigned short getConcealmentInCell() {return _concealmentInCell;}
    
    Vec2 getVectorToOtherCell(CollisionCell * other);
    const std::unordered_map<ENTITY_ID, MovingPhysicalEntity *> & getMovingEntsInCell() {return _movingEntsInCell;}

    void addCongestion(int team);
    void removeCongestion(int team);
    char getCongestion(int team);

    int  numberOfEntsInCellForTeam(int team, bool countForNotInTeam);
    bool hasCommandPostForTeam(int team);
    bool hasCommandPostNotInTeam(int team);
    void damageEntitiesInCellWithPower(double damage);
        
    /////////////// Cell collision detection
    bool entityInCell(PhysicalEntity * entity);
    bool pointInCell(Vec2 point);
    bool circleInCell(Vec2 origin, float radius);
    bool lineInCell(Vec2 start, Vec2 end);
    
    ///////////////// PhysicalEntity collision Detection
    void findCollisionsWithEnt(PhysicalEntity * testEntity, int team, COLLISION_TYPES collisionTypes, std::list<PhysicalEntity *> * collisionsList);
        
    void setNavigationPatch(NavigationPatch * patch);
    NavigationPatch * getNavigationPatch();
    
    void setMapSector(MapSector * sector) {_mapSector = sector;}
    MapSector * getMapSector() {return _mapSector;}
    
    void shutdown();
};

extern CollisionCell * theInvalidCell;

#endif //__COLLISION_CELL_H__
