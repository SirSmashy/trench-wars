//
//  TestingScene.cpp
//  TrenchWars
//
//  Created by Paul Reed on 9/19/23.
//

#include "TestingScene.h"

#include "MainMenuScene.h"
#include "Timing.h"
#include "VectorMath.h"
#include "BackgroundTaskHandler.h"
#include <algorithm>
#include <execution>
#include "Random.h"
#include <cereal.hpp>
#include <cereal/archives/binary.hpp>
#include <sstream>
#include "FixedStreamBuf.h"
#include "ScenarioFactory.h"
#include "MainMenuScene.h"
#include "NetworkServer.h"
#include "CollisionGrid.h"

#include "SerializationHelpers.h"
#include <cereal/types/string.hpp>



USING_NS_CC;


class Test : public ComparableRef
{
    bool _value;
    float _result;
    float _result2;
    
public:
    Test(bool val)
    {
        _value = val;
    }
    
    void act(float number)
    {
        _result = (uID() * number);
    }
    
    void query(float number)
    {
        _result2 = (uID() / number);
    }
    
    ~Test()
    {
        
    }
    
    bool someFunction() {return _value;}
};

Scene* TestingScene::createScene()
{
    return TestingScene::create();
}

// -----------------------------------------------------------------------
bool TestingScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    // Create a colored background (Dark Grey)
    LayerColor * backgroundColor = LayerColor::create(Color4B::GRAY, visibleSize.width, visibleSize.height);
    this->addChild(backgroundColor);
    
    
    // Game Label
    auto label = Label::createWithTTF("Tests", "Marker Felt.ttf", 36);
    label->setPositionNormalized(Vec2(0.5,0.55));
    label->setColor(Color3B::RED);
    this->addChild(label);
        
    // Test Button
    auto testButton = MenuItemLabel::create(
                                            Label::createWithTTF("Test","Marker Felt.ttf", 18),
                                            CC_CALLBACK_1(TestingScene::testCallback,this));
    testButton->setPositionNormalized(Vec2(.5,.35));
    
    
    // Back Button
    auto backButton = MenuItemLabel::create(
                                             Label::createWithTTF("Back","Marker Felt.ttf", 18),
                                             CC_CALLBACK_1(TestingScene::menuBackCallback,this));
    
    backButton->setPositionNormalized(Vec2(.5,.15));
    
    mainMenu = Menu::create(testButton,backButton, NULL);
    mainMenu->setPosition(Vec2::ZERO);
    this->addChild(mainMenu);
    return true;
}


void TestingScene::backButtonCallback(cocos2d::Ref* pSender)
{
    Director::getInstance()->replaceScene(TransitionFade::create(0.1, MainMenuScene::create(), Color3B(0,255,255)));
}


static std::atomic_int testCount = 0;
static std::atomic_int testOutput = 0;


void stupidTest()
{
    testOutput.store(testOutput.load() + 1) ;
    LOG_DEBUG("set three %d \n", testOutput.load() );
}

void TestingScene::testCallback(Ref* pSender)
{    
    CollisionGrid * grid = new CollisionGrid(Size(1000,1000),0);
    
    Vec2 start = Vec2::ZERO;
    Vec2 end(500,500);
    
    int count = 10000;
    
    std::vector<Vec2> starts;
    std::vector<Vec2> ends;
    
    int cellsHit = 0;
    int thing = 0;
    for(int i = 0; i < count; i++)
    {
        starts.emplace_back(Vec2(globalRandom->randomInt(0, 1000), globalRandom->randomInt(0, 1000)));
        ends.emplace_back(Vec2(globalRandom->randomInt(0, 1000), globalRandom->randomInt(0, 1000)));
    }
    
    MicroSecondSpeedTimer itTime2( [=](long long int time) {
        LOG_DEBUG("Iterate 2 Time: %f \n", (double)time/1000.0);
    });
    
    for(int i = 0; i < count; i++)
    {
        grid->iterateCellsInLine(starts[i], ends[i], [&cellsHit, &thing] (CollisionCell * cell, float distanceTraveled, bool minorCell) {
            cellsHit++;
            //        thing += cell->getXIndex();
            return true;
        });
    }
    
    itTime2.stop();

}


void TestingScene::test(Vec2 vec)
{
    
    
    
}

void TestingScene::test2(Vec2 vec)
{
    
    
}


void TestingScene::menuBackCallback(Ref* pSender)
{
    Director::getInstance()->replaceScene(TransitionFade::create(0.5, MainMenuScene::create(), Color3B(0,255,255)));

    
}


