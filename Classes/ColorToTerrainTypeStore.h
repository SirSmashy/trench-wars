//
//  ColorToTerrainType.h
//  TrenchWars
//
//  Created by Paul Reed on 9/16/22.
//

#ifndef ColorToTerrainTypeStore_h
#define ColorToTerrainTypeStore_h

#include <unordered_map>
#include <string>
#include "cocos2d.h"
#include "pugixml/pugixml.hpp"

using namespace pugi;
USING_NS_CC;


#define COLOR4B_TO_INT(color) ((color.r << 24) + (color.g << 16) + (color.b << 8) + (color.a))

struct Color4BHash
{
public:
    std::size_t operator() (Color4B const & color) const {
        return COLOR4B_TO_INT(color);
    }
};

struct Color4BCompare
{
public:
    bool operator() (Color4B const &colorA, Color4B const &colorB) const {
        return COLOR4B_TO_INT(colorA) < COLOR4B_TO_INT(colorB);
    }
};

struct TerrainType
{
    Color4B color;
    bool staticEntity;
    std::string name;
    size_t hashedName;
};

class ColorToTerrainTypeStore : public Ref
{
    std::unordered_map<size_t, TerrainType> _terrainTypes;
    std::unordered_map<Color4B, size_t, Color4BHash> _colorToTerrainIdMap;
public:
    
    ColorToTerrainTypeStore();
    void addTerrainType(std::string name, Color4B color, size_t hashedName, bool staticEntity);
    size_t colorToTerrainType(unsigned char r, unsigned char g, unsigned char b, unsigned char a);
    bool isTerrainTypeStaticEnt(size_t terrainType);
    std::string terrainTypeToName(size_t terrainType);
};

extern ColorToTerrainTypeStore * globalColorToTerrainTypeStore;

#endif /* ColorToTerrainTypeStore_h */
