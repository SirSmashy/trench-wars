//
//  FixedStreamBuf.hpp
//  TrenchWars
//
//  Created by Paul Reed on 9/5/23.
//

#ifndef FixedStreamBuf_h
#define FixedStreamBuf_h

#include <streambuf>

class FixedOutputStreamBuf : public std::streambuf
{
    char * _internalBuffer;
    bool bufferIsExternallyController;
    size_t _size;
    
public:
    FixedOutputStreamBuf(size_t bufferSize, char * externalBuffer = nullptr )
    {
        if(externalBuffer != nullptr)
        {
            _internalBuffer = externalBuffer;
            bufferIsExternallyController = true;
        }
        else
        {
            bufferIsExternallyController = false;
        }
        _size = bufferSize;
        setp(_internalBuffer, _internalBuffer + _size);
    }
    
    ~FixedOutputStreamBuf()
    {
        if(!bufferIsExternallyController)
        {
            delete [] _internalBuffer;
        }
    }
    
    char * getData()
    {
        return _internalBuffer;
    }
    
    void clear()
    {
        setp(_internalBuffer, _internalBuffer + _size);
    }
    
    size_t size()
    {
        return pptr() - pbase();
    }
    
protected:
    std::streamsize xsputn(const char* s, std::streamsize n) override
    {
        std::memcpy(pptr(), s, n);
        pbump(n);
        return n;
    };
    
    int_type overflow(int_type ch) override
    {
        // Fixed stream buffer doesn't handle overflow because we don't want to
        throw std::runtime_error("FixedOutputStreamBuf overflow!");
    }
    
    pos_type seekoff(off_type off, std::ios_base::seekdir dir, std::ios_base::openmode which) override
    {
        // Only output stream is supported
        if (which != std::ios_base::out)
            throw std::runtime_error("Not supported");
        
        if (dir == std::ios_base::cur)
        {
            pbump(off);
        }
        else if (dir == std::ios_base::end)
        {
            setp(pbase(), epptr());
            pbump( _size - off );
        }
            
        else if (dir == std::ios_base::beg)
        {
            setp(pbase(), epptr());
            pbump(off);
        }
        return pptr() - pbase();
    }
};

class ArrayInputStreamBuf : public std::streambuf
{
public:
    
    ArrayInputStreamBuf(const char * data, size_t len) :     // ptr + size
    begin_(data), end_(data + len), current_(data)
    {}
    
    ArrayInputStreamBuf(const char * beg, const char * end) : // begin + end
    begin_(beg), end_(end), current_(beg)
    {}
    
protected:
    int_type underflow() override
    {
        return (current_ == end_ ? traits_type::eof() : traits_type::to_int_type(*current_));
    }
    
    int_type uflow() override
    {
        return (current_ == end_ ? traits_type::eof() : traits_type::to_int_type(*current_++));
    }
    
    int_type pbackfail(int_type ch) override
    {
        if (current_ == begin_ || (ch != traits_type::eof() && ch != current_[-1]))
            return traits_type::eof();
        
        return traits_type::to_int_type(*--current_);
    }
    
    std::streamsize showmanyc() override
    {
        return end_ - current_;
    }
    
    const char * const begin_;
    const char * const end_;
    const char * current_;
};

#endif /* FixedStreamBuf_h */
