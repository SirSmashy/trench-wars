//
//  Projectile.cpp
//  TrenchWars
//
//  Created by Paul Reed on 3/17/12.
//  Copyright (c) 2012 . All rights reserved.
//

#include "Projectile.h"
#include "EngagementManager.h"
#include "EntityManager.h"
#include "VectorMath.h"
#include "Human.h"
#include "Timing.h"
#include "GameVariableStore.h"
#include "GameStatistics.h"
#include "Random.h"
#include "ParticleManager.h"
#include "CollisionGrid.h"
#include "MultithreadedAutoReleasePool.h"
#include "MapSector.h"
#include "Infantry.h"
#include "EntityFactory.h"
#include "SectorGrid.h"
#include "TeamManager.h"
#include "ClientProjectile.h"
#include "TrenchWarsManager.h"

Projectile::Projectile() : PhysicalEntity()
{
}

Projectile::~Projectile()
{
}

void initFromDefinition(ProjectileDefinition * definition)
{

}

bool Projectile::init(ProjectileDefinition * definition, Vec2 position, Vec2 goalPosition, double fireAngle, int height, WEAPON_PROJECTILE_TYPE type, double fizzleDistance, Human * owner, float speedOverride, int damageOverride)
{
    if(PhysicalEntity::init(definition,position, true))
    {
        _owner = owner;
        _projectileDefinition = definition;

        _speed =  _projectileDefinition->_speed;
        if(speedOverride != -1)
        {
            _speed = speedOverride;
        }

        _damageChance = 1;
        _damageChanceChange = .75 / (fizzleDistance / _speed); // FIX make this more reasonable, less linear
        _attackDamage = definition->_damage;
        if(damageOverride != -1)
        {
            _attackDamage = damageOverride;
        }
        
        _penetrationPower = _attackDamage * globalRandom->randomDouble(.67, 1.33); //FIXME: magic numbers
        _maxDistance = fizzleDistance;
        _projectileStart = position;
        _collisionCheckStart = _projectileStart;
        _projectileType = type;

        _startTime = globalEngagementManager->getEngagementTime();
        _collided = false;
        if(_projectileType != DIRECT && _projectileType != SHRAPNEL)
        {
            _projectileHeight = 0;
            _verticalVelocity = _speed * sin(fireAngle);
        }
        
        _sprite->setPositionZOffset(Z_POSITION_UNDERGROUND + .1); // Fix, is this the correct place for this?
        _projectileVelocity = goalPosition - position;
        _projectileVelocity.normalize();
        _projectileVelocity *= _speed * cos(fireAngle);
        Vec2 fizzleLocation = position + (_projectileVelocity * fizzleDistance);

        _startHeight = height;
        int size = globalVariableStore->getVariable(ProjectileSpriteSize);
        _sprite->setScale(Vec2(size,size));
        
        if(_projectileType == DIRECT || _projectileType == SHRAPNEL)
        {
            // This seems overly expensive
            getCurrentCollisionGrid()->iterateCellsInLine(position, fizzleLocation, [this, &position] (CollisionCell * cell, float distanceTraveled, bool minorCell) {
                if(cell->getTerrainCharacteristics().getHeight() > _startHeight)
                {
                    _maxDistance = (cell->getCellPosition() - position).length();
                    return false;
                }
                return true;
            });
        }

        return true;
    }
    return false;
}


Projectile * Projectile::create(ProjectileDefinition * definition, Vec2 position, Vec2 goalPosition, double fireAngle, int height, WEAPON_PROJECTILE_TYPE type, double fizzleDistance, Human * owner, float speedOverride, int damageOverride)
{
    Projectile * newProj;
    if(globalTrenchWarsManager->getNetworkMode() == NETWORK_MODE_CLIENT)
    {
        newProj = new ClientProjectile();
    }
    else
    {
        newProj = new Projectile();
    }
    if(newProj->init(definition,
                     position,
                     goalPosition,
                     fireAngle,
                     height,
                     type,
                     fizzleDistance,
                     owner,
                     speedOverride,
                     damageOverride))
    {
        globalAutoReleasePool->addObject(newProj);
        return newProj;
    }
    CC_SAFE_DELETE(newProj);
    return nullptr;
}

Projectile * Projectile::create()
{
    Projectile * newProj;
    if(globalTrenchWarsManager->getNetworkMode() == NETWORK_MODE_CLIENT)
    {
        newProj = new ClientProjectile();
    }
    else
    {
        newProj = new Projectile();
    }
    globalAutoReleasePool->addObject(newProj);
    return newProj;
    CC_SAFE_DELETE(newProj);
    return nullptr;
}


void Projectile::query(float deltaTime)
{
    if(_projectileType == DIRECT || _projectileType == SHRAPNEL)
    {
        checkForCollisions();
        _damageChance -= _damageChanceChange * deltaTime;
        MapSector * currentSector = globalSectorGrid->sectorForPosition(getPosition());
        if(currentSector != nullptr && sectorsEncounted.find(currentSector->_ID) == sectorsEncounted.end())
        {
            sectorsEncounted.emplace(currentSector->_ID, currentSector);
            for(auto team : globalTeamManager->getTeams())
            {
                if(team->getTeamId() == _owner->getTeamID())
                {
                    continue;
                }
                
                // Getting shot at adds a bit of experience
                for(MovingPhysicalEntity * enemy : currentSector->getEntitiesForTeam(team->getTeamId()))
                {
                    if(enemy->getEntityType() == INFANTRY)
                    {
                        Infantry * inf = (Infantry *) enemy;
                        inf->addStress( globalVariableStore->getVariable(BaseTakingFireStressLevel) ); //FIXME: make this a real number
                    }
                }
            }
        }
    }
    if(_collided)
    {
        if(_projectileType != DIRECT && _projectileType != SHRAPNEL)
        {
            createExplosion();
        }
        removeFromGame();
        return;
    }
}

void Projectile::act(float deltaTime)
{
    if(_collided)
    {
        return;
    }
    if(_projectileType == DIRECT || _projectileType == SHRAPNEL)
    {
        moveStraight(deltaTime);
    }
    else
    {
        moveArc(deltaTime);
    }
}


void Projectile::checkForCollisions()
{
    std::unordered_set<ENTITY_ID> entsEncountered;
    if(_owner != nullptr)
    {
        // NOTE: This means sub-projectiles created by this projectile (e.g., grenade shrapnel) can never harm the owner (e.g., grendate thrower)
        // todo fix make sure we are okay with this
        entsEncountered.emplace(_owner->uID());
    }
    getCurrentCollisionGrid()->iterateCellsInLine(_collisionCheckStart, getPosition(), [this, &entsEncountered] (CollisionCell * cell, float distanceTraveled, bool minorCell)
    {
        // See if the projectile impacts a static entity in the cell, or the landscape
        if(cell->getTerrainCharacteristics().getCoverValue() > 0 && cell->getTerrainCharacteristics().getHardness() > 0)
        {
            bool checkImpact = true;
            if(cell->getStaticEntity() != nullptr)
            {
                if(!entsEncountered.contains(cell->getStaticEntity()->uID()))
                {
                    entsEncountered.insert(cell->getStaticEntity()->uID());
                }
                else
                {
                    checkImpact = false; //already encountered this ent, skip impact check
                }
            }
            if(checkImpact)
            {
                if(globalRandom->randomInt(0, 100) < cell->getTerrainCharacteristics().getCoverValue())
                {
                    //We hit the static ent, now damage it and decrease penetration power
                    _penetrationPower -= cell->getTerrainCharacteristics().getHardness();
                    if(cell->getStaticEntity() != nullptr)
                    {
                        cell->getStaticEntity()->receiveDamage(_attackDamage);
                    }
                }
            }
            if(_penetrationPower <= 0)
            {
                _collided = true;
                return false;
            }
        }
        
        // Now see if any moving ents in the cell are hit
        float chanceToHit = 100 - cell->getTerrainCharacteristics().getHeightChanceToHit(_startHeight);
        chanceToHit *= _damageChance;

        for(auto ent : cell->getMovingEntsInCell())
        {
            if(ent.second->getEntityType() == CREW_WEAPON ||
               entsEncountered.contains(ent.second->uID()) || // Already tried to hit this ent, skip
               (ent.second->getTeamID() == _owner->getTeamID() && _projectileType == DIRECT)) // can't shoot our own teammates unless this is shrapnel
            {
                continue;
            }
            if(ent.second->testCollisionWithLine(_collisionCheckStart, getPosition()))
            {
                entsEncountered.insert(ent.second->uID());
                // See if we hit the ent, odds are based upon cover gained from height
                // Any non-height based cover was checked above
                
                float thisTargetChance = chanceToHit;
                if(ent.second->getEntityType() == INFANTRY || ent.second->getEntityType() == COURIER)
                {
                    Human * human = (Human *) ent.second;
                    human->addStress( chanceToHit); // FIX todo make sure this is reasonable
                    if(human->isProne())
                    {
                        thisTargetChance *= globalVariableStore->getProneChanceToHitMultiplier(_distanceTravelled);
                    }
                }

                if(globalRandom->randomInt(0, 100) < chanceToHit)
                {
                    ent.second->receiveDamage(_attackDamage);
                    _penetrationPower -= 50; //FIXME: magic number
                    if(_penetrationPower <= 0)
                    {
                        _collided = true;
                        return false;
                    }
                }
            }
        }
        return true;
    });

    
    _collisionCheckStart = getPosition();
}

void Projectile::moveStraight(float deltaTime)
{
    Vec2 moveDistance = _projectileVelocity * deltaTime;
    Vec2 lineEnd = _position + moveDistance;
    _distanceTravelled += _speed * deltaTime;
        
    if(_distanceTravelled > _maxDistance)
    {
        _collided = true;
    }
    else
    {
        setPosition(lineEnd);
    }
}

void Projectile::moveArc(float deltaTime)
{
    _projectileHeight += (_verticalVelocity * deltaTime);
    _verticalVelocity -= (globalVariableStore->getVariable(ProjectileGravity) * deltaTime);
        
    if(_projectileHeight <= 0)
    {
        _collided = true;
    }
    else
    {
        setPosition(getPosition() + (_projectileVelocity * deltaTime));
    }
    
    _sprite->setPositionZ(_projectileHeight * WORLD_TO_GRAPHICS_SIZE);

}

void Projectile::setPosition(Vec2 position)
{
    _position = position;
    _sprite->setPosition(position);
}

/**
 *
 *
 */
void Projectile::createExplosion()
{    
    Vec2 location = getPosition();
    CollisionCell * center = getCurrentCollisionGrid()->getCellForCoordinates(location);
    if(center == nullptr)
    {
        return;
    }
    
    double damage;
    double speed;
    double projectileCount = _attackDamage / globalVariableStore->getVariable(ProjectileDamageToProjectileCountDivisor);
    double halfPower = _attackDamage / 2;
    Vec2 goalPosition;
    double projectileDirection = VectorMath::signedAngleBetweenVecs(Vec2(1,0), _projectileVelocity);
    
    ProjectileDefinition * shrapnal = (ProjectileDefinition *) globalEntityFactory->getPhysicalEntityDefinitionWithName("shrapnel");
    
    float radsPerProjectile = _projectileDefinition->_burstAngle * D2R / projectileCount;
    float projectileAngle = projectileDirection - ((_projectileDefinition->_burstAngle * D2R) /2);
    
    goalPosition = Vec2(1000,0);
    goalPosition.rotate(Vec2(), projectileAngle);
    goalPosition += location;
    
    for(int i = 0; i < projectileCount; i++)
    {
        damage = globalRandom->randomInt(halfPower, _attackDamage);
        speed = damage * 2;
        Projectile::create(shrapnal, location, goalPosition, 0, center->getTerrainCharacteristics().getHeight(), SHRAPNEL, damage, _owner, speed, damage);

        goalPosition.rotate(location, radsPerProjectile);
    }
    
    if(!_projectileDefinition->_airBurst)
    {
        float cratorDiameter = (( (float) _attackDamage / globalVariableStore->getVariable(ProjectileDamageToCratorSizeDivisor)) - 1);
        createCratorAtLocation(location, cratorDiameter);
    }
    
    if(!_projectileDefinition->_detonateParticleName.empty())
    {
        globalParticleManager->requestParticle(_projectileDefinition->_detonateParticleName, getPosition());
    }
}

void Projectile::createCratorAtLocation(Vec2 location, float diameter)
{
    Vec2 center = location;
    location.x -= diameter / 2;
    location.y -= diameter / 2;
    double damage = diameter * 20;
    
    //FIXME: this should happen in THINK
    CollisionCell * centerCell = getCurrentCollisionGrid()->getCellForCoordinates(center);
    centerCell->damageEntitiesInCellWithPower(damage); //damage whatever is in the center cell
    
    // If the center cell still has a landscape object in it, then don't create the crator
    StaticEntity * objectInCell = centerCell->getStaticEntity();
    if(objectInCell != nullptr)
    {
        return;
    }
    
    double partialDiameter = diameter * .9;
    std::string fileName = "DirtBloom.plist";
    std::vector<CollisionCell *> cellsToDestroy;
    getCurrentCollisionGrid()->cellsInCircle(center, partialDiameter, &cellsToDestroy);
    
    for(CollisionCell * cell :cellsToDestroy)
    {
        cell->damageEntitiesInCellWithPower(damage); //destroy whatever is in the cell
    }
    
    StaticEntity * crator = StaticEntity::create("crator", location); // TODO fix turn this into a crater object
}

void Projectile::removeFromGame()
{
    PhysicalEntity::removeFromGame();
}

void Projectile::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    PhysicalEntity::loadFromArchive(archive);
    _projectileDefinition = (ProjectileDefinition *) globalEntityFactory->getPhysicalEntityDefinitionWithHashedName(_hashedEntityName);

    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        _owner = dynamic_cast<Human *>(globalEntityManager->getEntity(thingId));
    }
    else
    {
        _owner = nullptr;
    }
    archive(_penetrationPower);
    archive(_maxDistance);
    archive(_projectileStart);
    archive(_collisionCheckStart);
    int type;
    archive(type);
    _projectileType = (WEAPON_PROJECTILE_TYPE) type;
    archive(_startTime);
    archive(_startHeight);
    archive(_collided);
    archive(_projectileHeight);
    archive(_verticalVelocity);
    archive(_projectileVelocity);
    archive(_speed);
    archive(_damageChance);
    archive(_damageChanceChange);
    archive(_attackDamage);
    
    _sprite->setPositionZOffset(Z_POSITION_UNDERGROUND + .1); // Fix, is this the correct place for this?
    int size = globalVariableStore->getVariable(ProjectileSpriteSize);
    _sprite->setScale(Vec2(size,size));
}



void Projectile::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    PhysicalEntity::saveToArchive(archive);
    archive(_owner->uID());
    archive(_penetrationPower);
    archive(_maxDistance);
    archive(_projectileStart);
    archive(_collisionCheckStart);
    archive((int)_projectileType);
    archive(_startTime);
    archive(_startHeight);
    archive(_collided);
    archive(_projectileHeight);
    archive(_verticalVelocity);
    archive(_projectileVelocity);
    archive(_speed);
    archive(_damageChance);
    archive(_damageChanceChange);
    archive(_attackDamage);
}
