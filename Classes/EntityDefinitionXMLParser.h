#ifndef __ENTITY_DEFINITION_XML_PARSER_H__
#define __ENTITY_DEFINITION_XML_PARSER_H__

//
//  EntityDefinitionXMLParser.h
//  TrenchWars
//
//  Created by Paul Reed on 9/4/12.
//  Copyright (c) 2012 . All rights reserved.
//

#include "pugixml/pugixml.hpp"
#include "EntityFactory.h"
#include "cocos2d.h"

using namespace pugi;
USING_NS_CC;

class EntityDefinitionXMLParser :public xml_tree_walker
{
    PhysicalEntityDefinition * currentPhysicalEntityDefinition;
    UnitDefinition * currentUnitDefinition;
    UnitMemberDefinition * currentUnitMemberDefinition;
    CloudDefinition * currentCloudDefinition;
    
    EntityFactory * _entityFactory;
    
    void checkFinishedDefinition();
    void parseParticle(std::vector<ParticleDefinition> * particlesList, xml_node& node);
    
    std::vector<ParticleDefinition> * currentParticleList;
    
    
public:
    EntityDefinitionXMLParser(EntityFactory * factory);
    
    void parseFile(std::string const & filename);
    virtual bool for_each(xml_node& node);
};

#endif
