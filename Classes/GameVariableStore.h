//
//  GameVariableStore.hpp
//  TrenchWars
//
//  Created by Paul Reed on 7/23/20.
//

#ifndef GameVariableStore_h
#define GameVariableStore_h

#include "cocos2d.h"
#include "pugixml/pugixml.hpp"
#include <stdio.h>

using namespace pugi;
USING_NS_CC;
// These are variables that are used
enum GameVariables
{
    CommandPostCreationRadius,
    CommandPostCaptureRadius,
    CommandPostDamagePerSecond,
    CommandPostHealPerSecond,
    CommandPostDefaultRadius,
    CommandPostRecentAmmoDecay,
    
    BunkerSizeX,
    BunkerSizeY,

    ExperienceDivisor,
    
    HeightDifferenceCoverValue,
    ProjectileGravity,
    ProjectileDamageToProjectileCountDivisor,
    ProjectileDamageToCratorSizeDivisor,
    ProjectileSpriteSize,
    ProneChanceToHitCoefficient,
    
    BombardmentPlanMaxWait,
    BombardmentPlanMaxWaitDistance,
    
    ReloadTimeDeviationMultiplier,
    SecondsBeforeRemovingCorpse,
    
    ObjectiveVertexLength,
    ObjectiveClosePolygonDistance,
    MinCameraScale,
    MaxCameraScale,
    MinimumMenuSize,
    
    CoverChangeExponent,
    CoverToConcealmentExponent,
    CoverToConcealmentMaxValue,
    
    CourierAmmoCapacity,
    CourierSpawnRate,
    MaxCommandPostAmmoStockpile,
    
    BaseTakingFireStressLevel,
    EnemyDistanceToStressExponent,
    SeekCoverStressLevel,
    ImmediateCoverStressLevel,
    PanicStressLevel,
    BaseStressReductionRate,
    
    PathingCongestionCostMultiplier,
    PathingHeuristicMultiplier
    
} ;

class GlobalVariableStore : public Ref, xml_tree_walker
{
private:
    std::unordered_map<GameVariables,double> globalVariables;
    
    std::vector<double> coverDistanceMultiplier;
    
    std::vector<double> proneChanceToHitMultiplier;

    
public:
    GlobalVariableStore();
    
    void parseGlobalVariableFile(const std::string & filename);
    virtual bool for_each(xml_node& node);
    inline double  getVariable(GameVariables variable) {
        return globalVariables[variable];
    };
    
    inline void setVariable(GameVariables variable, double value)
    {
        globalVariables[variable] = value;
    }
    
    inline double getCoverMultiplierForDistance(int distance)
    {
        return coverDistanceMultiplier[distance];
    }    
    
    inline double getProneChanceToHitMultiplier(int distance)
    {
        return proneChanceToHitMultiplier[distance];
    }
    
    
    bool testValue;
};

extern GlobalVariableStore * globalVariableStore;

#endif /* GameVariableStore_hpp */
