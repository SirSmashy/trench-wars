//
//  HelloWorldScene.m
//  Trench Wars
//
//  Created by Paul Reed on 1/22/14.
//  Copyright Paul Reed 2014. All rights reserved.
//
// -----------------------------------------------------------------------

#include "EngagementScene.h"
#include "MainMenuScene.h"


// -----------------------------------------------------------------------

USING_NS_CC;

// -----------------------------------------------------------------------

bool EngagementScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }
    return true;
}


EngagementScene * EngagementScene::create()
{
    EngagementScene * scene = new EngagementScene();
    if (scene && scene->init())
    {
        scene->autorelease();
        return scene;
    }
    else
    {
        delete scene;
        return nullptr;
    }
}



// -----------------------------------------------------------------------

void EngagementScene::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    removeAllChildren();
    Director::getInstance()->end();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
    
    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/
    
    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
}


void EngagementScene::finish()
{
//    Director::getInstance()->replaceScene(TransitionFade::create(0.5, MainMenuScene::createScene(), Color3B(0,255,255)));
}
