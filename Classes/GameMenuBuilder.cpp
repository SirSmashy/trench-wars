//
//  GameMenuBuilder.cpp
//  TrenchWars
//
//  Created by Paul Reed on 9/7/20.
//

#include "GameMenuBuilder.h"
#include "PlayerLayer.h"
#include "Order.h"
#include "Command.h"
#include "EngagementManager.h"
#include "CollisionGrid.h"
#include "EntityFactory.h"
#include "GameVariableStore.h"
#include "NavigationGrid.h"
#include "ObjectiveInfoDisplay.h"
#include "AICommand.h"
#include "TestBed.h"
#include "ParticleManager.h"
#include "PathFinding.h"
#include "SectorGrid.h"
#include "TeamManager.h"
#include "UIController.h"
#include "GameEventController.h"
#include "BackgroundTaskHandler.h"
#include "Cloud.h"

void GameMenuBuilder::populateFromUnit(GameMenu * menu, Unit * unit, Vec2 location)
{
    if(!world->isPositionUnderground(location))
    {
        menu->addButton("Dig In", [=](Ref* sender) {
            // TODO fix dig in stuff (make objecticve, add trench line, issue work order for trench line)
        });
    }
    menu->addButton("Stop", [=](Ref* sender) {
        globalGameEventController->issueStopOrder(unit, globalUIController->getCurrentPlan());
    });
    menu->addButton("Move", [=](Ref* sender) {
        globalGameEventController->issueMoveOrder(location, unit, globalUIController->getCurrentPlan());
    });
}

void GameMenuBuilder::populateFromUnitGroup(GameMenu * menu, UnitGroup * group, Vec2 location)
{
    if(!world->isPositionUnderground(location))
    {
        menu->addButton("Dig In", [=](Ref* sender) {
            // TODO fix dig in stuff (make objecticve, add trench line, issue work order for trench line)
        });
    }
    menu->addButton("Stop", [=](Ref* sender) {
        group->issueStopOrder();
    });
    menu->addButton("Move", [=](Ref* sender) {
        group->issueMoveOrder(location);
    });
    
    menu->addButton("Delete", [=](Ref* sender)
    {
        group->removeFromGame();
    });
    
}
    
void GameMenuBuilder::populateFromObjective(GameMenu * menu, Objective * objective, Vec2 location)
{
    if(!objective->hasTrenchLine() && objective->getOwningTeam()->isPositionValidForNewCommandPost(location))
    {
        menu->addButton("Create Command Post", [=](Ref* sender)
        {
            globalGameEventController->createCommandPostInObjective(objective, location);
        });
    }
    
    if(!objective->hasTrenchLine())
    {
        menu->addButton("Create Trench Line", [=](Ref* sender)
        {
            globalGameEventController->createTrenchLineInObjective(objective);
        });
    }
    else
    {
        menu->addButton("Delete Trench Line", [=](Ref* sender)
        {
            globalGameEventController->removeTrenchLineInObjective(objective);
        });
        
        if(!objective->hasBunkers())
        {
            menu->addButton("Create Bunkers", [=](Ref* sender)
            {
                globalGameEventController->createBunkersInObjective(objective);
            });
        }
    }
    
    if(objective->hasBunkers())
    {
        menu->addButton("Delete Bunkers", [=](Ref* sender)
        {
            globalGameEventController->removeBunkersInObjective(objective);
        });
    }
    
    if(objective->hasCommandPost())
    {
        /// TEST ONLY
//        CommandPost * post = objective->getCommandPost();
//        menu->addButton("Use Ammo", [=] (Ref * sender) {
//            post->withdrawAmmo(300);
//        });
//        menu->addButton("Add Ammo", [=] (Ref * sender) {
//            post->depositAmmo(300);
//        });
    }
    
    menu->addButton("Set Name", [=](Ref* sender)
    {
        ObjectiveInfoDisplay * info = globalUIController->objectiveInfoForObjective(objective);
        if(info != nullptr)
        {
            info->openNameTextField();
        }
    });
    
    menu->addButton("Delete", [=](Ref* sender)
    {
        globalGameEventController->removeObjective(objective);
    });
}

    
void GameMenuBuilder::populateFromTester(GameMenu * menu, Tester * tester)
{
    menu->addButton("Remove", [=](Ref* sender)
    {
        tester->removeFromGame();
    });
}

void GameMenuBuilder::populateForObject(GameMenu * menu, InteractiveObject * object, Vec2 location)
{
    menu->clear();
    if(object == nullptr)
    {
        return;
    }
    
    if(object->getInteractiveObjectType() == EMBLEM_INTERACTIVE_OBJECT)
    {
        Emblem * emblem = dynamic_cast<Emblem *>(object);
        object = emblem->getCommandableObject();
    }
    if(object->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
    {
        Entity * entity = dynamic_cast<Entity *>(object);
        if(entity->getEntityType() == UNIT || entity->getEntityType() == CREW_WEAPON_UNIT)
        {
            GameMenuBuilder::populateFromUnit(menu, (Unit *) object, location);
        }
        else if(entity->getEntityType() == OBJECTIVE_ENTITY)
        {
            GameMenuBuilder::populateFromObjective(menu,(Objective *) object, location);
        }

    }
   
    if(object->getInteractiveObjectType() == TESTER_INTERACTIVE_OBJECT)
    {
        Tester * test = dynamic_cast<Tester *>(object);
        GameMenuBuilder::populateFromTester(menu, test);
    }
    if(object->getInteractiveObjectType() == UNIT_GROUP_INTERACTIVE_OBJECT)
    {
        UnitGroup * group = dynamic_cast<UnitGroup *>(object);
        GameMenuBuilder::populateFromUnitGroup(menu,group, location);
    }
}

void GameMenuBuilder::populateTestMenu(GameMenu * menu, Vec2 location)
{
    menu->addChildMenu("Spawn", [location] (GameMenu * child) {
        populateSpawnMenu(child, location);
    });
    menu->addSeperater();

    menu->addChildMenu("Click Action", [location] (GameMenu * child) {
        populateClickActionMenu(child, location);
    });
    menu->addSeperater();
    
    
    // /////////////////////////////////////////////////////////////////
    // ENT BEHAVIOR RELATED
    menu->addChildMenu("Ent Behaviors", [location] (GameMenu * child)
                       {
        std::string name;
        bool beingSkipped;
        
        beingSkipped = globalTestBed->behaviorsToSkip & AVOID_COLLISION_BEHAVIOR;
        name = "Avoid Collision";
        if(beingSkipped)
            name += "-";
        child->addButton(name, [=](Ref* sender)
                         {
            globalTestBed->setMovingEntBehavior(AVOID_COLLISION_BEHAVIOR, !beingSkipped);
        });
        
        beingSkipped = globalTestBed->behaviorsToSkip & GET_NEXT_MOVE_BEHAVIOR;
        name = "Get Next Move";
        if(beingSkipped)
            name += "-";
        child->addButton(name, [=](Ref* sender)
                         {
            globalTestBed->setMovingEntBehavior(GET_NEXT_MOVE_BEHAVIOR, !beingSkipped);
        });
        
        beingSkipped = globalTestBed->behaviorsToSkip & UPDATE_STRESS_BEHAVIOR;
        name = "Update Stress";
        if(beingSkipped)
            name += "-";
        child->addButton(name, [=](Ref* sender)
                         {
            globalTestBed->setMovingEntBehavior(UPDATE_STRESS_BEHAVIOR, !beingSkipped);
        });
        
        beingSkipped = globalTestBed->behaviorsToSkip & UPDATE_GOALS_BEHAVIOR;
        name = "Update Goals";
        if(beingSkipped)
            name += "-";
        child->addButton(name, [=](Ref* sender)
                         {
            globalTestBed->setMovingEntBehavior(UPDATE_GOALS_BEHAVIOR, !beingSkipped);
        });
        
        beingSkipped = globalTestBed->behaviorsToSkip & ATTACK_TARGET_GOAL_BEHAVIOR;
        name = "Attack Target";
        if(beingSkipped)
            name += "-";
        child->addButton(name, [=](Ref* sender)
                         {
            globalTestBed->setMovingEntBehavior(ATTACK_TARGET_GOAL_BEHAVIOR, !beingSkipped);
        });
        
        beingSkipped = globalTestBed->behaviorsToSkip & MOVE_GOAL_BEHAVIOR;
        name = "Do Move Goal";
        if(beingSkipped)
            name += "-";
        child->addButton(name, [=](Ref* sender)
                         {
            globalTestBed->setMovingEntBehavior(MOVE_GOAL_BEHAVIOR, !beingSkipped);
        });
        
        beingSkipped = globalTestBed->behaviorsToSkip & WORK_GOAL_BEHAVIOR;
        name = "Do Work Goal";
        if(beingSkipped)
            name += "-";
        child->addButton(name, [=](Ref* sender)
                         {
            globalTestBed->setMovingEntBehavior(WORK_GOAL_BEHAVIOR, !beingSkipped);
        });
        
        beingSkipped = globalTestBed->behaviorsToSkip & LOOK_FOR_TARGET_BEHAVIOR;
        name = "Look for targets";
        if(beingSkipped)
            name += "-";
        child->addButton(name, [=](Ref* sender)
                         {
            globalTestBed->setMovingEntBehavior(LOOK_FOR_TARGET_BEHAVIOR, !beingSkipped);
        });
        
        beingSkipped = globalTestBed->behaviorsToSkip & LOOK_AROUND_BEHAVIOR;
        name = "Look around";
        if(beingSkipped)
            name += "-";
        child->addButton(name, [=](Ref* sender)
                         {
            globalTestBed->setMovingEntBehavior(LOOK_AROUND_BEHAVIOR, !beingSkipped);
        });
        
        beingSkipped = globalTestBed->behaviorsToSkip & UPDATE_WEAPON_BEHAVIOR;
        name = "Update Weapon";
        if(beingSkipped)
            name += "-";
        child->addButton(name, [=](Ref* sender)
                         {
            globalTestBed->setMovingEntBehavior(UPDATE_WEAPON_BEHAVIOR, !beingSkipped);
        });
    });

    // /////////////////////////////////////////////////////////////////
    // PATHING RELATED
    menu->addSeperater();
    std::string path = "Path Heuristic: ";
    path.append(std::to_string(globalVariableStore->getVariable(PathingHeuristicMultiplier)));
    menu->addButton(path, [=](Ref* sender)
    {
        double value = globalVariableStore->getVariable(PathingHeuristicMultiplier) + .1;
        if(value > 1.3)
        {
            value = .7;
        }
        globalVariableStore->setVariable(PathingHeuristicMultiplier, value);
       
    });
    
    path = "Congestion Mult: ";
    path.append(std::to_string(globalVariableStore->getVariable(PathingCongestionCostMultiplier)));
    menu->addButton(path, [=](Ref* sender)
    {
        double value = globalVariableStore->getVariable(PathingCongestionCostMultiplier) + 2.0;
        if(value > 10.0)
        {
            value = 0.0;
        }
        globalVariableStore->setVariable(PathingCongestionCostMultiplier, value);
    });
    
    menu->addButton("Toggle Patches", [=](Ref* sender)
    {
        world->getNavGrid()->toggleShowNavigationPatches();
        globalTestBed->updateGrids = true;
    });
    
    menu->addButton("Toggle Path Debug", [=](Ref* sender)
    {
        globalTestBed->pathTesting = !globalTestBed->pathTesting;
        if(!globalTestBed->pathTesting)
        {
            globalPathFinding->pathDebug.clear();
        }
    });
    
    
    // /////////////////////////////////////////////////////////////////
    // Menus
    menu->addSeperater();
    menu->addButton("Toggle Timings", [=](Ref* sender)
    {
        globalMenuLayer->toggleTimingMenu();
    });
    
    menu->addButton("Toggle Test Value", [=](Ref* sender)
    {
        globalVariableStore->testValue = !globalVariableStore->testValue;
    });

    // /////////////////////////////////////////////////////////////////
    // Debug Drawing
    menu->addSeperater();
    menu->addButton("Toggle Debug Targetting", [=](Ref* sender)
    {
        globalTestBed->toggleDebugDrawing();
    });
    
    menu->addButton("Toggle Sectors", [=](Ref* sender)
    {
        globalSectorGrid->toggleDrawSectors();
    });
    
    menu->addButton("Toggle Cells", [=](Ref* sender)
    {
        globalTestBed->toggleDrawCells();
    });
    
    menu->addChildMenu("Cell Highlight", [location] (GameMenu * child) 
    {
        child->addButton("None", [=](Ref* sender)
        {
            globalTestBed->setCellHighlight(CELL_HIGHLIGHT_NONE);
        });
        child->addButton("Concealment", [=](Ref* sender)
        {
            globalTestBed->setCellHighlight(CELL_HIGHLIGHT_CONCEALMENT);
        });
        child->addButton("Security", [=](Ref* sender)
        {
            globalTestBed->setCellHighlight(CELL_HIGHLIGHT_TOTAL_SECURITY);
        });
        child->addButton("Security w/o Height", [=](Ref* sender)
        {
            globalTestBed->setCellHighlight(CELL_HIGHLIGHT_SECURITY_NO_HEIGHT);
        });
        child->addButton("Height", [=](Ref* sender)
        {
            globalTestBed->setCellHighlight(CELL_HIGHLIGHT_HEIGHT);
        });       
        child->addButton("Impassable", [=](Ref* sender)
        {
            globalTestBed->setCellHighlight(CELL_HIGHLIGHT_IMPASSABLE);
        });
    });
    
    if(globalParticleManager->arePermanentParticlesVisible())
    {
        menu->addButton("Hide Particles", [=](Ref* sender)
        {
            globalParticleManager->setPermanentParticleVisibility(false);
        });
    }
    else
    {
        menu->addButton("Show Particles", [=](Ref* sender)
        {
            globalParticleManager->setPermanentParticleVisibility(true);
        });
    }
    
    switch(globalTestBed->testCollisionType)
    {
        case NEW_COLLISION:
        {
            menu->addButton("Use Old Collision", [=](Ref* sender)
            {
                globalTestBed->testCollisionType = OLD_COLLISION;
            });
            break;
        }
        case OLD_COLLISION:
        {
            menu->addButton("Use No Collision", [=](Ref* sender)
            {
                globalTestBed->testCollisionType = NO_COLLISION;
            });
            break;
        }
        case NO_COLLISION:
        {
            menu->addButton("Use New Collision", [=](Ref* sender)
            {
                globalTestBed->testCollisionType = NEW_COLLISION;
            });
            break;
        }
    }
    
    menu->addChildMenu("Change Looks", [location] (GameMenu * child) {
        if(globalTestBed->looksPerFrame == 0)
        {
            globalTestBed->looksPerFrame++;
        }
        else if(globalTestBed->looksPerFrame > 20)
        {
            globalTestBed->looksPerFrame = 0;
        }
        else
        {
            globalTestBed->looksPerFrame *= 2;
        }
        LOG_DEBUG("Checks: %d \n", globalTestBed->looksPerFrame);
    });
    
    menu->addChildMenu("Change Vision Type", [location] (GameMenu * child) {
        globalTestBed->visionType++;

        if(globalTestBed->visionType > 3)
        {
            globalTestBed->visionType = 0;
        }
        LOG_DEBUG("Vision: %d \n", globalTestBed->visionType);
    });
    
    menu->addButton("Historival View", [=] (Ref* sender) {
        globalUIController->toggleHistoricalView();
    });
    
    menu->addButton("Historival Capture", [=] (Ref* sender) {
        MapSector * sector = globalSectorGrid->sectorForPosition(location);
        globalUIController->captureWorldAtPoint(sector->getSectorOrigin());
    });
    
    menu->addSeperater();

}

void GameMenuBuilder::populateSpawnMenu(GameMenu * menu, Vec2 location)
{
    int i = 0;
    for(auto team : globalTeamManager->getTeams())
    {
        for(auto command : team->getCommands())
        {
            std::string name = "Command: " + std::to_string(i);
            i++;
            GameMenu * playerMenu = menu->addChildMenu(name, [command, location] (GameMenu * playerMenu) {
                
                GameMenu * formationMenu = playerMenu->addChildMenu("Formations:" , [command, location]  (GameMenu * formationMenu) {
                    auto formations = globalEntityFactory->getAllFormationNames();
                    for(auto formation : formations)
                    {
                        formationMenu->addButton(formation, [formation, command, location] (Ref * sender)
                                                 {
                            command->spawnFormation(formation, location);
                        });
                    }
                });
                
                
                GameMenu * unitMenu = playerMenu->addChildMenu("Units:", [command, location]  (GameMenu * unitMenu) {
                    auto unitNames = globalEntityFactory->getAllUnitNames();
                    for(auto unitName : unitNames)
                    {
                        unitMenu->addButton(unitName, [unitName, command, location] (Ref * sender)
                        {
                            Vec2 validLocation = world->getCollisionGrid()->getValidMoveCellAroundPosition(location);
                            Unit * unit = Unit::create(unitName, validLocation, command);
                        });
                    }
                });
                
                
                GameMenu * statics = playerMenu->addChildMenu("Positions:", [command, location]  (GameMenu * statics) {
                    auto staticEntNames = globalEntityFactory->getAllPhysicalEntityNames();
                    for(auto entName : staticEntNames)
                    {
                        auto definition = globalEntityFactory->getPhysicalEntityDefinitionWithName(entName);
                        if(definition->_type == BUNKER)
                        {
                            statics->addButton(entName, [entName, location] (Ref * sender) {
                                Bunker * bunker = Bunker::create(location, Size(5,5), RIGHT);
                            });
                        }
                        else if(definition->_type == COMMANDPOST)
                        {
                            statics->addButton(entName, [entName, location, command] (Ref * sender) {
                                CommandPost * post = CommandPost::create(location, command);
                                post->addWork(1000000);
                            });
                        }
                    }
                });
                
                
                GameMenu * spawn = playerMenu->addChildMenu("Spawn Action:", [command, location]  (GameMenu * spawn) {
                    std::string name;
                    if(command->_debugSpawnMode == SPAWN_DO_NOTHING)
                    {
                        name = "+";
                    }
                    name += "Do Nothing";
                    spawn->addButton(name, [command] (Ref * senddr) {
                        command->_debugSpawnMode = SPAWN_DO_NOTHING;
                    },MENU_BUTTON_CLOSE_SUB_MENU);
                    name.clear();
                    
                    if(command->_debugSpawnMode == SPAWN_DIG_IN)
                    {
                        name = "+";
                    }
                    name += "Dig In";
                    spawn->addButton(name, [command] (Ref * senddr) {
                        command->_debugSpawnMode = SPAWN_DIG_IN;
                    },MENU_BUTTON_CLOSE_SUB_MENU);
                    name.clear();
                    
                    if(command->_debugSpawnMode == SPAWN_ATTACK_CP)
                    {
                        name = "+";
                    }
                    name += "Attack CP";
                    spawn->addButton(name, [command] (Ref * senddr) {
                        command->_debugSpawnMode = SPAWN_ATTACK_CP;
                    },MENU_BUTTON_CLOSE_SUB_MENU);
                    name.clear();
                    
                    if(command->_debugSpawnMode == SPAWN_GO_LEFT)
                    {
                        name = "+";
                    }
                    name += "Move Left";
                    spawn->addButton(name, [command] (Ref * senddr) {
                        command->_debugSpawnMode = SPAWN_GO_LEFT;
                    },MENU_BUTTON_CLOSE_SUB_MENU);
                    name.clear();
                    
                    if(command->_debugSpawnMode == SPAWN_GO_RIGHT)
                    {
                        name = "+";
                    }
                    name += "Move Right";
                    spawn->addButton(name, [command] (Ref * senddr) {
                        command->_debugSpawnMode = SPAWN_GO_RIGHT;
                    },MENU_BUTTON_CLOSE_SUB_MENU);
                });
            });
        }
    }
    
    GameMenu * statics = menu->addChildMenu("Static Entities:", [location]  (GameMenu * statics) {        
        auto staticEntNames = globalEntityFactory->getAllPhysicalEntityNames();
        for(auto entName : staticEntNames)
        {
            auto definition = globalEntityFactory->getPhysicalEntityDefinitionWithName(entName);
            if(definition->_type == STATIC_ENTITY)
            {
                statics->addButton(entName, [entName, location] (Ref * sender) {
                    StaticEntity::create(entName, location);
                });
            }
        }
    });
    
    
    GameMenu * other = menu->addChildMenu("Other:", [location]  (GameMenu * other) {
        other->addButton("Create Path Test", [=](Ref* sender)
        {
            globalUIController->createPathFindingTester(location);
        });        
        other->addButton("Smoke Test", [=](Ref* sender)
        {
            globalTestBed->createTestCloud(location);
        });
        other->addButton("Create Visibility Test", [=](Ref* sender)
        {
            globalUIController->createVisibilityTester(location);
        });
        other->addButton("Create Combat Wave", [=](Ref* sender)
         {
            globalTestBed->createTestCombatWave(location);
        });
        
        other->addButton("Create Random Wave", [=](Ref* sender)
         {
            globalTestBed->createTestRandomUnits(location);
        });
        
        other->addButton("Create Attack Wave", [=](Ref * sencder)
         {
            globalTestBed->createAttackWave(Vec2());
        });
    });
}

void GameMenuBuilder::populateClickActionMenu(GameMenu * menu, Vec2 location)
{
    CLICK_TEST_ACTION action = globalTestBed->getClickAction();
    for(int i = 0; i < CLICK_ACTION_END; i++)
    {
        CLICK_TEST_ACTION ithAction = (CLICK_TEST_ACTION) i;
        std::string name;
        if(ithAction == action)
        {
            name += "+";
        }
        name += globalTestBed->clickActionToName(ithAction);
        menu->addButton(name, [=](Ref* sender)
        {
            globalTestBed->setClickAction(ithAction);
        });
    }
}
