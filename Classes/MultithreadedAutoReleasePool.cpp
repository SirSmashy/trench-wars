//
//  MultithreadedAutoReleasePool.cpp
//  TrenchWars
//
//  Created by Paul Reed on 6/28/22.
//

#include "MultithreadedAutoReleasePool.h"

MultithreadedAutoReleasePool * globalAutoReleasePool;

MultithreadedAutoReleasePool::MultithreadedAutoReleasePool()
{
    globalAutoReleasePool = this;
}

void MultithreadedAutoReleasePool::addObject(Ref * object)
{
    _objectsPool.enqueue(object);
}

void MultithreadedAutoReleasePool::clearPool()
{
    Ref * object;
    // Seems a little dangerous to stay in a queue that is approximate
    while(_objectsPool.size_approx() > 0)
    {
        if(_objectsPool.try_dequeue(object))
        {
            object->release();
        }
    }
}
