//
//  TesterEmblem.cpp
//  TrenchWars
//
//  Created by Paul Reed on 12/10/22.
//

#include "TesterEmblem.h"
#include "MultithreadedAutoReleasePool.h"



bool TesterEmblem::init(Tester * tester, Vec2 position, bool updateOnDragStart)
{
    if(Emblem::init(tester, TEST_EMBLEM, position))
    {
        _updateOnDragStart = updateOnDragStart;
        _tester = tester;
        return true;
    }
    return false;
}

TesterEmblem * TesterEmblem::create(Tester * tester, Vec2 position, bool updateOnDragStart)
{
    TesterEmblem * newEmblem = new TesterEmblem();
    if(newEmblem->init(tester, position, updateOnDragStart)) {
        globalAutoReleasePool->addObject(newEmblem);
        return newEmblem;
    }
    CC_SAFE_DELETE(newEmblem);
    return nullptr;
}


void TesterEmblem::emblemDragged(Vec2 position)
{
    Emblem::emblemDragged(position);
    if(_updateOnDragStart)
    {
        _tester->update();
    }
}

void TesterEmblem::emblemDragEnded(Vec2 position)
{
    Emblem::emblemDragEnded(position);
    if(!_updateOnDragStart)
    {
        _tester->update();
    }
}

void TesterEmblem::emblemHoverEnter(Vec2 position)
{
    
}

void TesterEmblem::emblemHoverLeave(Vec2 position)
{
    
}
