//
//  CircleMenu.cpp
//  TrenchWars
//
//  Created by Paul Reed on 10/18/20.
//

#include "DropMenu.h"
#include "PlayerLayer.h"


DropMenu::DropMenu() : Layout()
{
    
}

bool DropMenu::init()
{
    Layout::init();
    return true;
}

DropMenu * DropMenu::create()
{
    DropMenu * newMenu = new DropMenu();
    if(newMenu->init())
    {
        newMenu->autorelease();
        return newMenu;
    }
       
    CC_SAFE_DELETE(newMenu);
    return nullptr;
}

void DropMenu::addButton(string name, const Widget::ccWidgetTouchCallback & callback)
{
    Button * button = Button::create();
    button->setPressedActionEnabled(true);
    button->setTitleText(name);
    button->setTitleFontName("arial.ttf");
    double fontSize = 20 / globalPlayerLayer->getScale();
    if(fontSize < 12 )
    {
        fontSize = 12;
    }
    button->setTitleFontSize(fontSize);
    button->addTouchEventListener(callback);
    addChild(button);
    doLayout();
}

void DropMenu::clear()
{
    removeAllChildren();
}

void DropMenu::updateContentSize()
{
    Rect contentBounds;
    Rect childBounds;
    
    for(Node * child : getChildren())
    {
        childBounds = child->getBoundingBox();
        Button * button = dynamic_cast<Button*>(child);
        if(button)
        {
            LayoutParameter * params = button->getLayoutParameter();
            Margin margin = params->getMargin();
            
            contentBounds.origin.x -= margin.left;
            contentBounds.origin.y -= margin.bottom;
            contentBounds.size.width += margin.left + margin.right;
            contentBounds.size.height += margin.bottom + margin.top;
        }
        
        if(contentBounds.size.width == 0 && contentBounds.size.height == 0)
        {
            contentBounds = childBounds;
        }
        else
        {
            contentBounds.merge(childBounds);
        }
    }
    setContentSize(contentBounds.size);
}

void DropMenu::doLayout()
{
    double width = 0;
    for(Node * child : getChildren())
    {
        Button * button = dynamic_cast<Button*>(child);
        if(button)
        {
            if(getBoundingBox().size.width > width)
            {
                width = getBoundingBox().size.width;
            }
        }
    }
    width =
}
