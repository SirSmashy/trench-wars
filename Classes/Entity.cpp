//
//  Entity.cpp
//  TrenchWars
//
//  Created by Paul Reed on 9/19/20.
//

#include "Entity.h"
#include "EntityManager.h"
#include "GameStatistics.h"

Entity::~Entity()
{
}

Entity::Entity()
{
    globalEntityManager->registryEntity(this);
}

void Entity::init()
{
    globalGameStatistics->incrementEntityTypeCount(getEntityType());
}

void Entity::removeFromGame()
{
    globalGameStatistics->decrementEntityTypeCount(getEntityType());
    globalEntityManager->removeEntity(this);
}
