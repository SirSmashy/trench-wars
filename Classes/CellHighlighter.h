//
//  CellHighlighter.hpp
//  TrenchWars
//
//  Created by Paul Reed on 6/27/21.
//

#ifndef CellHighlighter_h
#define CellHighlighter_h

#include <stdio.h>
#include <stdio.h>
#include "cocos2d.h"
USING_NS_CC;

class CollisionCell;

enum CELL_HIGHLIGHT_DATA_TYPE
{
    CELL_HIGHLIGHT_NONE,
    CELL_HIGHLIGHT_CONCEALMENT,
    CELL_HIGHLIGHT_TOTAL_SECURITY,
    CELL_HIGHLIGHT_SECURITY_NO_HEIGHT,
    CELL_HIGHLIGHT_HEIGHT,
    CELL_HIGHLIGHT_IMPASSABLE
};

class CellHighlighter : public DrawNode
{
    CellHighlighter();
    ~CellHighlighter();
    bool init();
    
    CELL_HIGHLIGHT_DATA_TYPE _highlightType;
    Color4F _highlightColor;
    
    
public:
    static CellHighlighter * create();
    
    void highlightCell(CollisionCell * cell);
    void setHightlightType(CELL_HIGHLIGHT_DATA_TYPE type);
    void setHighlightColor(Color4F highlightColor);
    void hightlightCellsNearPoint(Vec2 location, float radius);
    void highlightCells(const std::vector<CollisionCell *> & cells);
    void clearCells();
    
    void remove();
};

#endif /* CellHighlighter_h */
