//
//  ClientEventController.cpp
//  TrenchWars
//
//  Created by Paul Reed on 12/12/23.
//

#include "ClientEventController.h"
#include "UIController.h"
#include "TrenchWarsManager.h"
#include "EntityManager.h"
#include "TeamManager.h"

ClientEventController::ClientEventController()
{
    
}

void ClientEventController::init()
{
    GameEventController::init();
}

ClientEventController * ClientEventController::create()
{
    ClientEventController * controller = new ClientEventController();
    controller->init();
    return controller;
}


void ClientEventController::postLoad()
{
    globalUIController->postLoad(); 
    Player * localPlayer = globalTrenchWarsManager->getLocalPlayer();
    Command * command = globalTeamManager->getCommand(localPlayer->commandId);
    globalUIController->getCamera()->setScreenPosition(command->getSpawnPosition());
}

void ClientEventController::query(float deltaTime)
{
    
}

void ClientEventController::parseServerUpdate(cereal::BinaryInputArchive & archive)
{
    int teamId;
    size_t size;
    ENTITY_ID entId, entId2;
    PLAN_ACTION planAction;
    archive(teamId);
    Team * team = globalTeamManager->getTeam(teamId);

    archive(size);
    for(int i = 0; i < size; i++)
    {
        archive(entId);
        archive(planAction);
        
        switch (planAction) {
            case PLAN_ACTION_CREATE:
            {
                Plan * newPlan = Plan::create();
                newPlan->loadFromArchive(archive);
                handleEventNewPlan(newPlan);
                break;
            }
            case PLAN_ACTION_EXECUTE:
            {
                Plan * plan = team->getPlanWithId(entId);
                handleEventPlanExecuting(plan);
                break;
            }
            case PLAN_ACTION_STOP:
            {
                Plan * plan = team->getPlanWithId(entId);
                handleEventPlanStopped(plan);
                break;
            }
            case PLAN_ACTION_RENAME:
            {
                std::string newName;
                archive(newName);
                Plan * plan = team->getPlanWithId(entId);
                handleEventPlanRenamed(plan, newName);
                break;
            }
            case PLAN_ACTION_REMOVE:
            {
                Plan * plan = team->getPlanWithId(entId);
                handleEventPlanRemoved(plan);
                break;
            }
        }
    }
    
    archive(size);
    for(int i = 0; i < size; i++)
    {
        ORDER_TYPE orderType;
        archive(orderType);
        Order * newOrder = Order::createOrderOfType(orderType);
        newOrder->loadFromArchive(archive);
        archive(entId);
        archive(entId2);
        Unit * unit = (Unit *) globalEntityManager->getEntity(entId);
        Plan * plan = entId2 != -1 ? team->getPlanWithId(entId2) : nullptr;
        LOG_DEBUG("Received new order %d for %lld plan: %lld \n", orderType, entId, entId2);
        handleEventOrderIssued(newOrder, unit, plan);
    }
    
    archive(size);
    for(int i = 0; i < size; i++)
    {
        bool failed;
        Order * order = nullptr;
        archive(entId);
        archive(entId2);
        archive(failed);
        
        for(auto command : team->getCommands())
        {
            order = command->getOrderWithId(entId);
            if(order != nullptr)
            {
                break;
            }
        }
        Unit * unit = (Unit *) globalEntityManager->getEntity(entId2);
        handleEventOrderComplete(unit, order, failed);
    }
}

void ClientEventController::buildClientUpdatePacket(cereal::BinaryOutputArchive & archive)
{
    std::lock_guard<std::mutex> lock(_pendingMutex);
    archive(_pendingOrders.size());
    for(auto pending : _pendingOrders)
    {
        pending->saveToArchive(archive);
    }
    _pendingOrders.clear();
    
    archive(_pendingObjectiveActions.size());
    for(auto pending : _pendingObjectiveActions)
    {
        LOG_DEBUG("Sending %d \n", pending->action);
        pending->saveToArchive(archive);
    }
    _pendingObjectiveActions.clear();
    
    archive(_pendingPlanActions.size());
    for(auto pending : _pendingPlanActions)
    {
        pending->saveToArchive(archive);
    }
    _pendingPlanActions.clear();
}

// ///////// GENERATE EVENTS //////

void ClientEventController::issueMoveOrder(Vec2 position, Unit * commandable, Plan * addToPlan)
{
    PendingMoveOrder * pending = new PendingMoveOrder();
    pending->unit = commandable->uID();
    pending->type = MOVE_ORDER;
    pending->plan = addToPlan == nullptr ? -1 : addToPlan->uID();
    
    LOG_DEBUG("Issue move %.1f %.1f for %lld plan: %lld \n", position.x, position.y, pending->unit, pending->plan);

    
    pending->position = position;
    std::lock_guard<std::mutex> lock(_pendingMutex);

    _pendingOrders.pushBack(pending);
    pending->release();

}

void ClientEventController::issueOccupyOrder(Objective * objective, Unit * commandable, Plan * addToPlan)
{
    PendingObjectiveOrder * pending = new PendingObjectiveOrder();
    pending->unit = commandable->uID();
    pending->type = OCCUPY_ORDER;
    pending->plan = addToPlan == nullptr ? -1 : addToPlan->uID();
    
    pending->objectiveId = objective->uID();
    pending->objectiveAction = OBJECTIVE_ORDER_OCCUPY;
    std::lock_guard<std::mutex> lock(_pendingMutex);

    _pendingOrders.pushBack(pending);
    pending->release();
}

void ClientEventController::issueDigTrenchesOrder(Objective * objective, Unit * commandable, Plan * addToPlan)
{
    PendingObjectiveOrder * pending = new PendingObjectiveOrder();
    pending->unit = commandable->uID();
    pending->type = OBJECTIVE_WORK_ORDER;
    pending->plan = addToPlan == nullptr ? -1 : addToPlan->uID();
    
    pending->objectiveId = objective->uID();
    pending->objectiveAction = OBJECTIVE_ORDER_DIG_TRENCHES;
    std::lock_guard<std::mutex> lock(_pendingMutex);

    _pendingOrders.pushBack(pending);
    pending->release();
}

void ClientEventController::issueDigBunkersOrder(Objective * objective, Unit * commandable, Plan * addToPlan)
{
    PendingObjectiveOrder * pending = new PendingObjectiveOrder();
    pending->unit = commandable->uID();
    pending->type = OBJECTIVE_WORK_ORDER;
    pending->plan = addToPlan == nullptr ? -1 : addToPlan->uID();
    
    pending->objectiveId = objective->uID();
    pending->objectiveAction = OBJECTIVE_ORDER_DIG_BUNKERS;
    std::lock_guard<std::mutex> lock(_pendingMutex);

    _pendingOrders.pushBack(pending);
    pending->release();
}

void ClientEventController::issueBuildCommandPostOrder(Objective * objective, Unit * commandable, Plan * addToPlan)
{
    PendingObjectiveOrder * pending = new PendingObjectiveOrder();
    pending->unit = commandable->uID();
    pending->type = OBJECTIVE_WORK_ORDER;
    pending->plan = addToPlan == nullptr ? -1 : addToPlan->uID();
    
    pending->objectiveId = objective->uID();
    pending->objectiveAction = OBJECTIVE_ORDER_BUILD_COMMAND_POST;
    std::lock_guard<std::mutex> lock(_pendingMutex);

    _pendingOrders.pushBack(pending);
    pending->release();
}

void ClientEventController::issueClearAreaOrder(Objective * objective, Unit * commandable, Plan * addToPlan)
{
    PendingObjectiveOrder * pending = new PendingObjectiveOrder();
    pending->unit = commandable->uID();
    pending->type = OBJECTIVE_WORK_ORDER;
    pending->plan = addToPlan == nullptr ? -1 : addToPlan->uID();
    
    pending->objectiveId = objective->uID();
    pending->objectiveAction = OBJECTIVE_ORDER_CLEAR_AREA;
    std::lock_guard<std::mutex> lock(_pendingMutex);

    _pendingOrders.pushBack(pending);
    pending->release();
}

void ClientEventController::issueAttackPositionOrder(Objective * objective, Unit * commandable, Plan * addToPlan)
{
    PendingObjectiveOrder * pending = new PendingObjectiveOrder();
    pending->unit = commandable->uID();
    pending->type = ATTACK_POSITION_ORDER;
    pending->plan = addToPlan == nullptr ? -1 : addToPlan->uID();
    
    pending->objectiveId = objective->uID();
    pending->objectiveAction = OBJECTIVE_ORDER_ATTACK;
    std::lock_guard<std::mutex> lock(_pendingMutex);

    _pendingOrders.pushBack(pending);
    pending->release();
}

void ClientEventController::issueStopOrder(Unit * commandable, Plan * addToPlan)
{
    PendingOrder * pending = new PendingOrder();
    pending->unit = commandable->uID();
    pending->type = STOP_ORDER;
    pending->plan = addToPlan == nullptr ? -1 : addToPlan->uID();
    std::lock_guard<std::mutex> lock(_pendingMutex);

    _pendingOrders.pushBack(pending);
    pending->release();
    
    handleEventOrderCancelled(commandable);
}

void ClientEventController::createNewObjective(std::vector<Vec2> & points, bool enclosed, Unit * unitCreating)
{
    PendingObjectiveCreate * pending = new PendingObjectiveCreate();
    pending->action = OBJECTIVE_ACTION_CREATE;
    pending->objectiveId = -1;
    
    pending->points = points;
    pending->enclosed = enclosed;
    pending->unitCreatingId = unitCreating != nullptr ? unitCreating->uID() : -1;
    std::lock_guard<std::mutex> lock(_pendingMutex);

    _pendingObjectiveActions.pushBack(pending);
    pending->release();
}

void ClientEventController::removeObjective(Objective * objective)
{
    PendingObjectiveAction * pending = new PendingObjectiveAction();
    pending->action = OBJECTIVE_ACTION_REMOVE;
    pending->objectiveId = objective->uID();
    std::lock_guard<std::mutex> lock(_pendingMutex);
    _pendingObjectiveActions.pushBack(pending);
    pending->release();
}


// Command Pots
void ClientEventController::createCommandPostInObjective(Objective * objective, Vec2 location)
{
    PendingObjectiveCreateCommandPost * pending = new PendingObjectiveCreateCommandPost();
    pending->action = OBJECTIVE_ACTION_CREATE_COMMAND_POST;
    pending->objectiveId = objective->uID();
    pending->position = location;
    std::lock_guard<std::mutex> lock(_pendingMutex);
    _pendingObjectiveActions.pushBack(pending);
    pending->release();
}

// Trenches
void ClientEventController::createTrenchLineInObjective(Objective * objective)
{
    PendingObjectiveAction * pending = new PendingObjectiveAction();
    pending->action = OBJECTIVE_ACTION_CREATE_TRENCH;
    pending->objectiveId = objective->uID();
    std::lock_guard<std::mutex> lock(_pendingMutex);
    _pendingObjectiveActions.pushBack(pending);
    pending->release();
}

void ClientEventController::removeTrenchLineInObjective(Objective * objective)
{
    PendingObjectiveAction * pending = new PendingObjectiveAction();
    pending->action = OBJECTIVE_ACTION_REMOVE_TRENCH;
    pending->objectiveId = objective->uID();
    std::lock_guard<std::mutex> lock(_pendingMutex);
    _pendingObjectiveActions.pushBack(pending);
    pending->release();
}

// Bunkers
void ClientEventController::createBunkersInObjective(Objective * objective)
{
    PendingObjectiveAction * pending = new PendingObjectiveAction();
    pending->action = OBJECTIVE_ACTION_CREATE_BUNKERS;
    pending->objectiveId = objective->uID();
    std::lock_guard<std::mutex> lock(_pendingMutex);
    _pendingObjectiveActions.pushBack(pending);
    pending->release();
}

void ClientEventController::removeBunkersInObjective(Objective * objective)
{
    PendingObjectiveAction * pending = new PendingObjectiveAction();
    pending->action = OBJECTIVE_ACTION_REMOVE_BUNKERS;
    pending->objectiveId = objective->uID();
    std::lock_guard<std::mutex> lock(_pendingMutex);
    _pendingObjectiveActions.pushBack(pending);
    pending->release();
}

// Plans
void ClientEventController::createNewPlan(int playerId)
{
    PendingNewPlan * pending = new PendingNewPlan();
    pending->action = PLAN_ACTION_CREATE;
    pending->planId = -1;
    pending->playerId = playerId;
    std::lock_guard<std::mutex> lock(_pendingMutex);
    _pendingPlanActions.pushBack(pending);
    pending->release();
}

void ClientEventController::removePlan(Plan * plan)
{
    PendingPlanAction * pending = new PendingPlanAction();
    pending->action = PLAN_ACTION_REMOVE;
    pending->planId = plan->uID();
    std::lock_guard<std::mutex> lock(_pendingMutex);
    _pendingPlanActions.pushBack(pending);
    pending->release();
}

void ClientEventController::executePlan(Plan * plan)
{
    PendingPlanAction * pending = new PendingPlanAction();
    pending->action = PLAN_ACTION_EXECUTE;
    pending->planId = plan->uID();
    std::lock_guard<std::mutex> lock(_pendingMutex);
    _pendingPlanActions.pushBack(pending);
    pending->release();
}

void ClientEventController::stopPlan(Plan * plan)
{
    PendingPlanAction * pending = new PendingPlanAction();
    pending->action = PLAN_ACTION_STOP;
    pending->planId = plan->uID();
    std::lock_guard<std::mutex> lock(_pendingMutex);
    _pendingPlanActions.pushBack(pending);
    pending->release();
}

void ClientEventController::renamePlan(Plan * plan, const std::string & newName)
{
    PendingPlanRename * pending = new PendingPlanRename();
    pending->action = PLAN_ACTION_RENAME;
    pending->planId = plan->uID();
    pending->name = newName;
    std::lock_guard<std::mutex> lock(_pendingMutex);
    _pendingPlanActions.pushBack(pending);
    pending->release();
}

// Team status
void ClientEventController::updateTeamVisibility(Team * team, 
                                                 std::unordered_map<ENTITY_ID, double> & sectorVisibleTime,
                                                 std::unordered_set<ENTITY_ID> & sectorsNewlyHidden,
                                                 std::unordered_set<ENTITY_ID> & sectorsNewlyVisible)
{
    
}


// //////////// Handle events //////////
void ClientEventController::handleEventOrderIssued(Order * order, Unit * unit, Plan * addToPlan)
{
    if(addToPlan == nullptr)
    {
        if(unit->getOwningTeam()->isPositionInCommandArea(unit->getAveragePosition()))
        {
            globalUIController->reportUnitIssuedOrder(order, unit);
        }
        else
        {
            globalUIController->reportCourierTransportingOrder(order, unit);
        }
    }
    else
    {
        LOG_DEBUG("Handle order %.1f %.1f \n", order->getPosition().x, order->getPosition().y);
        addToPlan->addOrder(unit, order);
        globalUIController->reportOrderAddedToPlan(order, unit, addToPlan);
    }
}

void ClientEventController::handleEventOrderComplete(Unit * entCompleting, Order * order, bool failed)
{
    Team * team = nullptr;
    ENTITY_ID completingId = -1;
    if(entCompleting != nullptr)
    {
        team = entCompleting->getOwningCommand()->getOwningTeam();
    }

    auto plan = team->getPlanWithId(order->getOwningPlanId());
    if(plan != nullptr)
    {
        plan->orderComplete(entCompleting);
    }
    globalUIController->reportOrderComplete(entCompleting, order, failed);
}

void ClientEventController::handleEventOrderCancelled(Unit * commandable)
{
    globalUIController->reportOrderCancelled(commandable);
}


void ClientEventController::handleEventObjectiveCreated(Objective * objective)
{
    globalUIController->reportNewObjective(objective);
}

void ClientEventController::handleEventObjectiveRemoved(Objective * objective)
{
    globalUIController->reportObjectiveRemoved(objective);
}


void ClientEventController::handleEventSectorVisible(MapSector * sector)
{
    globalUIController->reportSectorVisible(sector);
}

void ClientEventController::handleEventTeamVisibilityUpdate(Team * team, std::unordered_map<ENTITY_ID, double> & sectorVisibleTime)
{
    if(globalUIController != nullptr && globalTeamManager->getLocalPlayersTeam() == team)
    {
        
        globalUIController->reportTeamVisibilityUpdate(sectorVisibleTime);
    }
}

//command post related
void ClientEventController::handleEventNewCommandPost(CommandPost * commandPost)
{
    globalUIController->reportNewCommandPost(commandPost);
}

void ClientEventController::handleEventCommandPostDestroyed(CommandPost * commandPost)
{
    // do nothing
}

//Unit related
void ClientEventController::handleEventNewUnit(Unit * unit)
{
    if(unit->getOwningTeam() == globalTeamManager->getLocalPlayersTeam())
    {
        globalUIController->reportNewUnit(unit);
    }
}


void ClientEventController::handleEventUnitDestroyed(Unit * unit)
{
    globalUIController->reportUnitDestroyed(unit);
}

// PLAN related
void ClientEventController::handleEventNewPlan(Plan * plan)
{
    Team * team = globalTeamManager->getLocalPlayersTeam();
    team->addPlan(plan);
    globalUIController->reportNewPlan(plan);
}

void ClientEventController::handleEventPlanExecuting(Plan * plan)
{
    plan->setExecuting(true);
    globalUIController->reportPlanExecuting(plan);
}

void ClientEventController::handleEventPlanStopped(Plan * plan)
{
    plan->setExecuting(false);
    globalUIController->reportPlanStopped(plan);
}

void ClientEventController::handleEventPlanRenamed(Plan * plan, const std::string & newName)
{
    plan->setName(newName);
    globalUIController->reportPlanRenamed(plan, newName);
}

void ClientEventController::handleEventPlanRemoved(Plan * plan)
{
    globalUIController->reportPlanRemoved(plan);
}

void ClientEventController::handleEventOrderRemovedFromPlan(Unit * unit, Plan * plan)
{
    globalUIController->reportOrderRemovedFromPlan(unit, plan);
}


//friendly unit presence
void ClientEventController::handleEventUnitInSector(MapSector * sector, Unit * unit)
{
    
}

//enemy unit presence
void ClientEventController::handleEventEnemyUnitSpotted(Unit * enemy)
{
    
}

void ClientEventController::handleEventEnemyCommandPostSpotted(CommandPost *enemy)
{
    
}

void ClientEventController::handleEventEnemyCanEngageSector(MapSector * sector, Human * enemy)
{
    
}

void ClientEventController::handleEventEnemyArtilleryInSector(MapSector * sector)
{
    
}
