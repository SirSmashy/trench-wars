//
//  HistoricalViewSprite.h
//  TrenchWars
//
//  Created by Paul Reed on 10/25/24.
//

#ifndef HistoricalViewSprite_h
#define HistoricalViewSprite_h

#include "cocos2d.h"
#include "VectorMath.h"
#include <tuple>

USING_NS_CC;

class HistoricalViewSprite : public Sprite
{
protected:
    
    //renderer caches and callbacks
    void onBegin();
    void onEnd();
                
    void setupPolygon(int captureAreasWidth, int captureAreasHeight);
    
    Mat4 _oldTransMatrix, _oldProjMatrix;
    Mat4 _transformMatrix, _projectionMatrix;
    Node * _nodeToCapture;
    
    GroupCommand  _groupCommand;
    CustomCommand _beginCommand;
    CustomCommand _endCommand;
    
    Texture2D * _paperTexture;
        
    
    Size         _worldSize; // Size of the game world, in world coordinates
    Size         _sectorSize; // Size of one sector in the game world, in world coordinates
    Size         _textureSize; //Size of the texture. This texture will be laid over the entire world, but may not have the same resolution as the world. e.g., The world may be 16k by 16k, but the texture might be 4k by 4k
    double       _textureToWorldScale; // The scale factor to go from world coordinates to texture coordinates
    Size         _scaledSectorSize; // The size of a single sector in texture coordinates.
    
    GLuint       _captureFBO;
    GLuint       _captureDepthRenderBuffer;
    Vec2         _captureStartPosition;
    
    GLint        _oldFBO;
    
    std::vector<int> _captureAreasPerVertex;
    std::queue<Vec2> _captureQueue;
    std::unordered_map<Vec2, std::tuple<int, int, int, int>, Vec2Hash, Vec2Compare> _captureAreaToVertexIndicesMap;
    
    /** Initializes a RenderTexture object with width and height in Points and a pixel format( only RGB and RGBA formats are valid ) and depthStencil format.
     *
     * @param w The RenderTexture object width.
     * @param h The RenderTexture object height.
     * @param format In Points and a pixel format( only RGB and RGBA formats are valid ).
     * @param depthStencilFormat The depthStencil format.
     * @return If succeed, it will return true.
     */
    bool init(Size worldSize, Size captureSize, int xCaptures, int yCaptures, Node * nodeToCapture);
    
    bool _updating;
    bool _capturingEntireWorld;

    void doCaptureWorldAtPoint(Vec2 point);

    
public:
    
    /** Initializes a RenderTexture object with width and height in Points and a pixel format( only RGB and RGBA formats are valid ) and depthStencil format.
     *
     * @param w The RenderTexture object width.
     * @param h The RenderTexture object height.
     * @param format In Points and a pixel format( only RGB and RGBA formats are valid ).
     * @param depthStencilFormat The depthStencil format.
     */
    static HistoricalViewSprite * create(Size worldSize, Size captureSize, int xCaptures, int yCaptures, Node * nodeToCapture);
    
    void captureWorldAtPoint(Vec2 point);
    void captureEntireWorld();
    void updateVisibility(std::unordered_map<Vec2, unsigned char, Vec2Hash, Vec2Compare> & captureAreaVisibilityMap);

    virtual void begin();
    virtual void end();
    
    void update();
    
    virtual void visit(Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags) override;
};
#endif /* HistoricalViewSprite_h */
