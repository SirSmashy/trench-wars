//
//  InputActionHandler.cpp
//  TrenchWars
//
//  Created by Paul Reed on 7/23/21.
//

#include "InputActionHandler.h"
#include "Command.h"
#include "RawInputHandler.h"
#include "EntityFactory.h"
#include "PlayerLayer.h"
#include "World.h"
#include "EngagementManager.h"
#include "TestBed.h"
#include "UIController.h"
#include "TeamManager.h"
#include "CollisionGrid.h"

InputActionHandler::InputActionHandler()
{
    _selectedEmblem = nullptr;
    _newObjective = nullptr;
    _activeUnitGroupDrawer = nullptr;
    _newUnitGroup = nullptr;
    _debugInfoMenu = DebugObjectMenu::create();

    _selectedHighlighter = InteractiveObjectHighlighter::create();
    _selectedHighlighter->retain();    
    _dragEmblem = nullptr;
    inputHandler = new RawInputHandler(this, globalUIController->getCamera());
}

InputActionHandler * InputActionHandler::create()
{
    InputActionHandler * handler = new InputActionHandler();
    handler->autorelease();
    return handler;
}


void InputActionHandler::dragEmblem(Vec2 location, bool dragEnd)
{
        // Drag is ending, determine the correct action to take
        if(dragEnd)
        {
            _dragEmblem->emblemDragEnded(location);
            globalUIController->clearDrawer();
            _dragEmblem = nullptr;
        }
        else
        {
            _dragEmblem->emblemDragged(location);
    }
}

void InputActionHandler::dragObject(InteractiveObject * objDragged, Vec2 location, bool dragEnd)
{
    // Are we currently dragging an entity?
    if(_dragEmblem == nullptr && objDragged != nullptr && objDragged->getInteractiveObjectType() == EMBLEM_INTERACTIVE_OBJECT)
    {
        _dragEmblem = (Emblem *) objDragged;
    }
    if(_dragEmblem != nullptr)
    {
        dragEmblem(location, dragEnd);
    }
    
    //If a unit group was just created it is considered impermanet until interacted with, remove impermanet unit-groups if they aren't immediatly interacted with
    // FIX WHHHHHYYYYY is this implemented here???
    if(_newUnitGroup != nullptr)
    {
        bool removeNewUnitGroup = true;
        if(objDragged->getInteractiveObjectType() == EMBLEM_INTERACTIVE_OBJECT)
        {
            Emblem * emblem = (Emblem *) objDragged;
            if(emblem->getCommandableObject()->uID() == _newUnitGroup->uID())
            {
                removeNewUnitGroup = false;
            }
        }
        if(removeNewUnitGroup)
        {
            _newUnitGroup->removeFromGame();
            _newUnitGroup = nullptr;
        }
        else
        {
            _newUnitGroup = nullptr;
        }
    }
    
    if(_newObjective != nullptr)
    {
        _newObjective->dragEnd(location);
        _newObjective->release();
        _newObjective = nullptr;
    }
}

void InputActionHandler::marqueeSelect(Vec2 location, bool dragEnd)
{
    if(dragEnd)
    {
        _activeUnitGroupDrawer->dragEnd(location);
        _newUnitGroup = _activeUnitGroupDrawer->getUnitGroup();
        _activeUnitGroupDrawer->release();
        _activeUnitGroupDrawer = nullptr;
    }
    else if(_activeUnitGroupDrawer == nullptr)
    {
        _activeUnitGroupDrawer = UnitGroupDrawer::create(); // FIX UNIT GROUPS
        _activeUnitGroupDrawer->retain();
        _activeUnitGroupDrawer->dragStart(location);
        
        //If a unit group was just created it is considered impermanet until interacted with, remove impermanet unit-groups if they aren't immediatly interacted with
        if(_newUnitGroup != nullptr)
        {
            _newUnitGroup->removeFromGame();
            _newUnitGroup = nullptr;
        }
    }
    else
    {
        _activeUnitGroupDrawer->dragMove(location);
    }
}

void InputActionHandler::drawObjective(Vec2 location, bool dragEnd)
{
    if(dragEnd)
    {
        _newObjective->dragEnd(location);
        _newObjective->release();
        _newObjective = nullptr;
    }
    else if(_newObjective == nullptr)
    {
        _newObjective = ObjectiveDrawer::create();
        _newObjective->retain();
        _newObjective->dragStart(location);
    }
    else
    {
        _newObjective->dragMove(location);
    }
    
    //If a unit group was just created it is considered impermanet until interacted with, remove impermanet unit-groups if they aren't immediatly interacted with
    if(_newUnitGroup != nullptr)
    {
        _newUnitGroup->removeFromGame();
        _newUnitGroup = nullptr;
    }
}

void InputActionHandler::selectObject(InteractiveObject * objClicked, Vec2 location)
{
    _debugInfoMenu->setObject(objClicked, true);

    if(objClicked != nullptr)
    {
        if(objClicked->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
        {
            Entity * entClicked = dynamic_cast<Entity *>(objClicked);
            if(entClicked->getEntityType() == INFANTRY)
            {
                Infantry * inf = dynamic_cast<Infantry *>(entClicked);
                objClicked = globalEmblemManager->getEmblemForCommandableObjectWithType(inf->getOwningUnit()->uID(),POSITION_EMBLEM);
            }
            else if(entClicked->getEntityType() == CREW_WEAPON)
            {
                CrewWeapon * crew = dynamic_cast<CrewWeapon*>(entClicked);
                objClicked = globalEmblemManager->getEmblemForCommandableObjectWithType(crew->getOwningUnit()->uID(),POSITION_EMBLEM);
            }
            else if(entClicked->getEntityType() == CLOUD_ENTITY)
            {
                Cloud * cloud = dynamic_cast<Cloud*>(entClicked);
                cloud->debugClick();
            }
        }
        else if(objClicked->getInteractiveObjectType() != EMBLEM_INTERACTIVE_OBJECT)
        {
            objClicked = nullptr;
        }
    }
    
    Emblem * clickedEmblem = dynamic_cast<Emblem *>(objClicked);
    
    if(_selectedEmblem != nullptr)
    {
        _selectedEmblem->release();
    }
    
    _selectedEmblem = clickedEmblem;
    _selectedHighlighter->setObject(_selectedEmblem);
    
    if(_selectedEmblem != nullptr)
    {
        _selectedEmblem->retain();
    }
    globalUIController->clearEntityMenu();
    
    //If a unit group was just created it is considered impermanet until interacted with, remove impermanet unit-groups if they aren't immediatly interacted with
    if(_newUnitGroup != nullptr)
    {
        Emblem * emblem = globalEmblemManager->getEmblemForCommandableObjectWithType(_newUnitGroup->uID(),POSITION_EMBLEM);
        
        if(emblem != objClicked)
        {
            _newUnitGroup->removeFromGame();
            _newUnitGroup = nullptr;
        }
        else
        {
            _newUnitGroup = nullptr;
        }
    }
}

void InputActionHandler::modifySelection(InteractiveObject * objClicked)
{
    if(objClicked == nullptr)
    {
        //No modification
        return;
    }
    if(objClicked->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
    {
        Entity * entClicked = dynamic_cast<Entity *>(objClicked);
        if(entClicked->getEntityType() == INFANTRY || entClicked->getEntityType() == CREW_WEAPON)
        {
            Infantry * actor = dynamic_cast<Infantry *>(entClicked);
            objClicked = globalEmblemManager->getEmblemForCommandableObjectWithType(actor->getOwningUnit()->uID(),POSITION_EMBLEM);
        }
    }
    
    if(objClicked->getInteractiveObjectType() != EMBLEM_INTERACTIVE_OBJECT)
    {
        return;
    }

    
    Emblem * clickedEmblem = dynamic_cast<Emblem *>(objClicked);
    
    if(_selectedEmblem == nullptr)
    {
        // just select
        _selectedEmblem = clickedEmblem;
        _selectedHighlighter->setObject(_selectedEmblem);
        _selectedEmblem->retain();
        
    }
    else if(_selectedEmblem == clickedEmblem)
    {   //just unselect
        _selectedEmblem->release();
        _selectedEmblem = nullptr;
        _selectedHighlighter->setObject(_selectedEmblem);
    }
    else
    {
        if(_selectedEmblem->getCommandableObject()->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
        {
            if(clickedEmblem->getCommandableObject()->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
            {
                // Create a unit group
                Vector<Unit *> units;
                units.pushBack((Unit *) _selectedEmblem->getCommandableObject());
                units.pushBack((Unit *) clickedEmblem->getCommandableObject());
                _newUnitGroup = UnitGroup::create(units, globalTeamManager->getLocalPlayersTeam()); //FIXME: UNIT GROUPS?
                
                _selectedEmblem = globalEmblemManager->getEmblemForCommandableObjectWithType(_newUnitGroup->uID(),POSITION_EMBLEM);
                _selectedHighlighter->setObject(_selectedEmblem);
                _selectedEmblem->retain();
            }
            else if(clickedEmblem->getCommandableObject()->getInteractiveObjectType() == UNIT_GROUP_INTERACTIVE_OBJECT)
            {
                // Add to the unit group
                UnitGroup * group = (UnitGroup *) clickedEmblem->getCommandableObject();
                Unit * unit = (Unit *) _selectedEmblem->getCommandableObject();
                group->addUnit(unit);
            }
        }
        else if(_selectedEmblem->getCommandableObject()->getInteractiveObjectType() == UNIT_GROUP_INTERACTIVE_OBJECT)
        {
            UnitGroup * group = (UnitGroup *) _selectedEmblem->getCommandableObject();
            if(clickedEmblem->getCommandableObject()->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
            {
                Unit * unit = (Unit *) clickedEmblem->getCommandableObject();
                if(group->containsUnit(unit))
                {
                    group->removeUnit(unit);
                }
                else
                {
                    //Add to the unit group
                    group->addUnit(unit);
                }
            }
            if(group->getUnits().size() == 0)
            {
                _selectedEmblem = nullptr;
            }
            else
            {
                _selectedHighlighter->setObject(_selectedEmblem);
            }
        }
    }
}

void InputActionHandler::orderObject(Vec2 location)
{
    if(_selectedEmblem == nullptr || _selectedEmblem->isLocalPlayersEmblem())
    {
        globalUIController->populateContextMenu(_selectedEmblem, location);
    }
}


void InputActionHandler::showTestMenu(Vec2 location)
{
    globalUIController->showTestMenu(location);
}


void InputActionHandler::hoverEnter(InteractiveObject * objectHovered, Vec2 location)
{
    Vec2 screenLocation = globalPlayerLayer->convertToWorldSpace(world->getAboveGroundPosition(location) * WORLD_TO_GRAPHICS_SIZE);
    _debugInfoMenu->setObject(objectHovered, false);
    if(!_debugInfoMenu->isFollowingObject())
    {
        _debugInfoMenu->setPosition(screenLocation);
    }
}

void InputActionHandler::hoverLeave(InteractiveObject * objectHovered, Vec2 location)
{
    if(objectHovered == nullptr)
    {
        return;
    }
    
    if(!_debugInfoMenu->isFollowingObject())
    {
        _debugInfoMenu->clearObject();
    }
    if(objectHovered->getInteractiveObjectType() == ENTITY_INTERACTIVE_OBJECT)
    {
        Entity * entHovered = dynamic_cast<Entity *>(objectHovered);
        if(entHovered->getEntityType() == INFANTRY)
        {
            Infantry * inf = dynamic_cast<Infantry *>(entHovered);
            objectHovered = inf->getOwningUnit();
            inf->collisionTest = false;
        }
        else if(entHovered->getEntityType() == CREW_WEAPON)
        {
            CrewWeapon * crew = dynamic_cast<CrewWeapon *> (entHovered);
            objectHovered = crew->getOwningUnit();
            crew->collisionTest = false;
            
        }
    }
    
    if(objectHovered == nullptr)
    {
        return;
    }
    if(objectHovered->getInteractiveObjectType() == EMBLEM_INTERACTIVE_OBJECT)
    {
        Emblem * emblem = dynamic_cast<Emblem *>(objectHovered);
        emblem->emblemHoverLeave(location);
    }
 
}

void InputActionHandler::toggleInfoScreen()
{
    bool newVisibility = !_debugInfoMenu->isVisible();
    
    _debugInfoMenu->setVisible(newVisibility);
    if(!newVisibility)
    {
        _debugInfoMenu->clearObject();
    }
}

void InputActionHandler::toggleGameMenu()
{
    globalMenuLayer->toggleGameMenu();
    globalEngagementManager->togglePause();
}

void InputActionHandler::togglePause()
{
    globalEngagementManager->togglePause();
}


void InputActionHandler::remove()
{
    if(_newObjective != nullptr)
    {
        _newObjective->release();
    }
    if(_activeUnitGroupDrawer != nullptr)
    {
        _activeUnitGroupDrawer->release();
    }
    _selectedHighlighter->release();
    _debugInfoMenu->release();
    inputHandler->release();
    if(_newObjective != nullptr)
    {
        _newObjective->release();
    }
}
