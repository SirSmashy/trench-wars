//
//  ChangeTrackingVariable.hpp
//  TrenchWars
//
//  Created by Paul Reed on 6/22/22.
//

#ifndef ChangeTrackingVariable_h
#define ChangeTrackingVariable_h

#include "Refs.h"
#include "List.h"
#include <mutex>
#include <type_traits>

class ChangeTrackingVariable
{
protected:
    bool _changed;
public:
    virtual bool isChanged() const {return _changed;}
    virtual void clearChange() = 0;
};


template <typename T,typename = std::enable_if_t<std::is_base_of_v<Ref, T>>>
class RefChangeTrackingVariable : public ChangeTrackingVariable
{
protected:
    T * _ref;
    
public:
    RefChangeTrackingVariable<T>()
    {
        _ref = nullptr;
        _changed = false;
    }
    
    virtual RefChangeTrackingVariable<T> & operator=(T * ref)
    {
        if(_ref != nullptr)
        {
            _ref->release();
        }
        _ref = ref;
        if(_ref != nullptr)
        {
            _ref->retain();
        }
        _changed = true;
        return *this;
    }
    
    virtual bool operator==(T * other)
    {
        return _ref == other;
    }
    
    virtual bool operator!=(const T * other)
    {
        return _ref != other;
    }
    

    
    
    virtual T * get() const {return _ref;}
    
    operator T * () const
    {
        return _ref;
    }
    
    virtual void clearChange()
    {
        _changed = false;
        if(_ref != nullptr)
        {
            _ref->release();
        }
        _ref = nullptr;
    }
};


template<class T>
class SimpleChangeTrackingVariable : public ChangeTrackingVariable
{
protected:
    T _variable;
    T _defaultValue;
public:
    
    SimpleChangeTrackingVariable(const T & defaultValue) {
        _defaultValue = defaultValue;
        _changed = false;
    }
    
    virtual SimpleChangeTrackingVariable<T>& operator=(const T& other)
    {
        _variable = other;
        _changed = true;
        return *this;
    }
    
    inline operator T & ()
    {
        return _variable;
    }
    
    inline operator const T & () const
    {
        return _variable;
    }
    
    virtual inline T & get() {return _variable;}

    virtual inline const T & getConst() const {return _variable;}
    
    virtual void setChanged()
    {
        _changed = true;
    }
    
    virtual void clearChange()
    {
        _changed = false;
        _variable = _defaultValue;
    }
    
    template<class A>
    void serialize(A & archive)
    {
        archive(_changed);
        archive(_variable);
        archive(_defaultValue);
    }
};

template<class T>
class PrimitiveChangeTrackingVariable : public SimpleChangeTrackingVariable<T>
{
public:
    
    PrimitiveChangeTrackingVariable(const T & defaultValue) : SimpleChangeTrackingVariable<T>(defaultValue)
    {
    }
    
    virtual bool operator==(const T& other)
    {
        return this->_variable == other;
    }
    
    virtual bool operator!=(const T& other)
    {
        return this->_variable != other;
    }
    
    virtual PrimitiveChangeTrackingVariable<T>& operator=(const T& other)
    {
        this->_variable = other;
        this->_changed = true;
        return *this;
    }
    
    virtual PrimitiveChangeTrackingVariable<T>& operator+=(const T& other)
    {
        this->_variable += other;
        this->_changed = true;
        return *this;
    }
    
    virtual PrimitiveChangeTrackingVariable<T>& operator-=(const T& other)
    {
        this->_variable -= other;
        this->_changed = true;
        return *this;
    }
    
    virtual T operator+(const T& other)
    {
        return this->_variable + other;
    }
    
    virtual T operator-(const T& other)
    {
        return this->_variable - other;
    }
    
    template<class A>
    void serialize(A & archive)
    {
        archive(this->_changed);
        archive(this->_variable);
        archive(this->_defaultValue);
    }
};

template<class T>
class AtomicChangeTrackingVariable  : public ChangeTrackingVariable
{
protected:
    std::atomic<T> _variable;
    T _defaultValue;
    
public:
    
    AtomicChangeTrackingVariable(const T & defaultValue) {
        _defaultValue = defaultValue;
        _variable = _defaultValue;
        _changed = false;
    }
    
    virtual AtomicChangeTrackingVariable<T>& operator=(const T& other)
    {
        _variable = other;
        _changed = true;
        return *this;
    }
    
    virtual AtomicChangeTrackingVariable<T>& operator+=(const T& other)
    {
        _variable.store( _variable.load() + other);
        _changed = true;
        return *this;
    }
    
    virtual AtomicChangeTrackingVariable<T>& operator-=(const T& other)
    {
        _variable.store( _variable.load() - other);
        _changed = true;
        return *this;
    }
    
    virtual T get() const {return _variable.load();}
    
    operator T() const
    {
        return _variable.load();
    }
    
    virtual void clearChange()
    {
        _changed = false;
        _variable.store(_defaultValue);
    }
    virtual bool isChanged() const {return _changed;}
    
    template<class A>
    void save(A & archive) const
    {
        archive(_changed);
        archive(_variable.load());
        archive(_defaultValue);
    }
    
    template<class A>
    void load(A & archive)
    {
        archive(_changed);
        T object;
        archive(object);
        _variable.store(object);
        archive(_defaultValue);
    }
};


template <typename T,typename = std::enable_if_t<std::is_base_of_v<Ref, T>>>
class BlockingRefChangeTrackingVariable : public ChangeTrackingVariable
{
protected:
    T * _ref;
    mutable std::mutex block;

public:
    BlockingRefChangeTrackingVariable<T>()
    {
        _ref = nullptr;
        _changed = false;
    }
    
    virtual BlockingRefChangeTrackingVariable<T> & operator=(T * ref)
    {
        block.lock();
        if(_ref != nullptr)
        {
            _ref->release();
        }
        _ref = ref;
        if(ref != nullptr)
        {
            ref->retain();
        }
        _changed = true;
        block.unlock();
        return *this;
    }
    
    
    virtual T * get() const
    {
        block.lock();
        return _ref;
    }
    
    virtual void clearChange()
    {
        _changed = false;
        if(_ref != nullptr)
        {
            _ref->release();
        }
        _ref = nullptr;
        block.unlock();
    }
    
    virtual void unlock() const
    {
        block.unlock();
    }
    
    template<class A>
    void serialize(A & archive)
    {
        archive(_changed);
        archive(*_ref);
    }
};


template <typename T>
class VectorChangeTrackingVariable : public ChangeTrackingVariable
{
protected:
    std::vector<T> _vector;
    mutable std::mutex block;
    
public:
    VectorChangeTrackingVariable<T>()
    {
        _changed = false;
    }
    
    virtual void addValue(const T& other)
    {
        block.lock();
        _vector.push_back(other);
        _changed = true;
        block.unlock();
    }
    
    
    virtual std::vector<T> get() const
    {
        std::lock_guard<std::mutex> lock(block);
        return _vector;
    }

    
    virtual void clearChange()
    {
        block.lock();
        _vector.clear();
        _changed = false;
        block.unlock();
    }
};


template <typename T>
class ListChangeTrackingVariable : public ChangeTrackingVariable
{
protected:
    std::list<T> _list;
    mutable std::mutex block;
    
public:
    ListChangeTrackingVariable<T>()
    {
        _changed = false;
    }
    
    virtual void pushBack(const T& other)
    {
        block.lock();
        _list.push_back(other);
        _changed = true;
        block.unlock();
    }
    
    virtual std::list<T> get() const
    {
        std::lock_guard<std::mutex> lock(block);
        return _list;
    }
    
    virtual T popFront()
    {
        block.lock();
        T val = _list.front();
        _list.pop_front();
        block.unlock();
        return val;
    }
    
    virtual size_t size() const
    {
        std::lock_guard<std::mutex> lock(block);
        return _list.size();
    }
    
    virtual void clearChange()
    {
        block.lock();
        _list.clear();
        _changed = false;
        block.unlock();
    }
};


template <typename T,typename = std::enable_if_t<std::is_base_of_v<Ref, T>>>
class RefListChangeTrackingVariable : public ChangeTrackingVariable
{
protected:
    List<T *> _list;
    mutable std::mutex block;
    
public:
    RefListChangeTrackingVariable<T>()
    {
        _changed = false;
    }
    
    virtual void pushBack(T * obj)
    {
        block.lock();
        _list.pushBack(obj);
        _changed = true;
        block.unlock();
    }
    
    virtual List<T*> get() const
    {
        std::lock_guard<std::mutex> lock(block);
        return _list;
    }
    
    virtual T * popFront()
    {
        block.lock();
        T * val = _list.front();
        _list.popFront();
        block.unlock();
        return val;
    }
    
    virtual size_t size() const
    {
        std::lock_guard<std::mutex> lock(block);
        return _list.size();
    }
    
    virtual void clearChange()
    {
        block.lock();
        _list.clear();
        _changed = false;
        block.unlock();
    }
};



template <typename K, typename T, typename = std::enable_if_t<std::is_base_of_v<Ref, T>>>
class RefMapChangeTrackingVariable : public ChangeTrackingVariable
{
protected:
    Map<K, T *> _map;
    mutable std::mutex block;
    
public:
    RefMapChangeTrackingVariable<T>()
    {
        _changed = false;
    }
    
    virtual void insert(K key, T * obj)
    {
        block.lock();
        _map.insert(key, obj);
        _changed = true;
        block.unlock();
    }
    
    virtual Map<K, T *> getAndClear()
    {
        std::lock_guard<std::mutex> lock(block);
        Map<K, T *> result = _map;
        _map.clear();
        _changed = false;
        return result;
    }
    
    virtual Map<K, T *> get() const
    {
        std::lock_guard<std::mutex> lock(block);
        Map<K, T *> result = _map;
        return result;
    }
    
    virtual size_t size() const
    {
        std::lock_guard<std::mutex> lock(block);
        return _map.size();
    }
    
    virtual void clearChange()
    {
        block.lock();
        _map.clear();
        _changed = false;
        block.unlock();
    }
};


#endif /* ChangeTrackingVariable_h */
