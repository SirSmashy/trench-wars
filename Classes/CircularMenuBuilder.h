//
//  CircularMenuBuilder.hpp
//  TrenchWars
//
//  Created by Paul Reed on 12/10/22.
//

#ifndef CircularMenuBuilder_h
#define CircularMenuBuilder_h


#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "CircularMenu.h"
#include "Unit.h"
#include "UnitGroup.h"
#include "Objective.h"


USING_NS_CC;
using namespace cocos2d::ui;

class LandscapeObjectiveBuilder;

class CircularMenuBuilder
{
public:
    static void populateForLandscapeObjectiveBuilder(CircularMenu * menu, LandscapeObjectiveBuilder * builder, Emblem * emblem, Vec2 location);
    static void populateForObjective(CircularMenu * menu, Objective * objective, Emblem * emblem, Vec2 location);
    static void populateForEmblem(CircularMenu * menu, Emblem * emblem, Vec2 location);
};

#endif /* CircularMenuBuilder_h */
