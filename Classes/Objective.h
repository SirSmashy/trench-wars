//
//  Objective.h
//  TrenchWars
//
//  Created by Paul Reed on 6/26/21.
//

#ifndef Objective_h
#define Objective_h

#include "cocos2d.h"
#include "PlanningObject.h"
#include "Emblem.h"
#include "Bunker.h"
#include "LineDrawer.h"
#include "ProposedEntity.h"

USING_NS_CC;


class CollisionCell;
class Command;
class Team;
class CommandPost;
class MapSector;
class ObjectiveObjectCollection;
/*
 * Objectives are lines or polygonal areas designated by the a player.
 */
class Objective : public PlanningObject
{
protected:
    Rect _bounds;
    std::vector<Vec2> _points;
    std::unordered_map<ENTITY_ID, CollisionCell *> _cellsMap;
    std::vector<CollisionCell *> _cellsInObjective;
    std::vector<CollisionCell *> _cellsAlongObjectiveBorder;

    std::unordered_map<ENTITY_ID, MapSector *> _sectorsMap;
    std::vector<MapSector *> _sectorsInObjective;

    Team * _team;
    ObjectiveObjectCollection * _trenchLine;
    ObjectiveObjectCollection * _bunkers;
    
    ProposedEntity * _proposedCommandPost;
    
    CommandPost * _commandPost;    
    Vector<LineDrawer *> _lines;
    
    std::unordered_map<int, double> test;

    bool _visible;
    bool _isEnclosedArea;
    std::string _name;
    
    Objective();
    ~Objective();
    virtual bool init(Team * team, std::vector<Vec2> & points, bool enclosed);
    virtual bool init(Team * team, CommandPost * post);

    virtual void drawObjective();
    virtual void calculateCellsInObjective();
    
    DIRECTION getBunkerEntrance(PhysicalEntity * trench);
    
public:
    static Objective * create(Team * team, std::vector<Vec2> & points, bool enclosed);
    static Objective * create(Team * team, CommandPost * post);
    static Objective * create();
        
    virtual ENTITY_TYPE getEntityType() const {return OBJECTIVE_ENTITY;}    
    virtual void postLoad();
    
    double getArea();
    double getPerimeterLength();
    bool isEnclosedArea();
    Rect getBounds() {return _bounds;}
    
    virtual Team * getOwningTeam() {return _team;}
    virtual void setVisible(bool visible);
    virtual bool isVisible() {return _visible;}
    
    virtual void createTrenchLine();
    virtual void removeTrenchLine();
    virtual bool hasTrenchLine() {return _trenchLine != nullptr;}
    virtual ObjectiveObjectCollection * getTrenchLine() {return _trenchLine;}
    
    virtual bool hasBunkers() {return _bunkers != nullptr;}
    virtual ObjectiveObjectCollection * getBunkers() {return _bunkers;}
    virtual void createBunkers();
    virtual void removeBunkers();

    virtual bool hasCommandPost();
    virtual CommandPost * getCommandPost() {return _commandPost;}
    virtual void createCommandPost(Vec2 location);
    virtual void setCommandPost(CommandPost * post);
    
    virtual bool pointInObjective(Vec2 point);
    virtual void setName(const std::string & name);
    const std::string & getName() {return _name;}
    
    virtual void setHighlighted(bool highlighted);

    // Collision
    virtual Vec2 getRandomPointInObjective();
    virtual Vec2 findAreaInObjectiveWithBestCoverValue(int searchSize);
    virtual Vec2 getCenterPoint();
    
    virtual const std::vector<CollisionCell *> & cellsInObjective() {return _cellsInObjective;}
    virtual const std::vector<CollisionCell *> & cellsAlongBorder() {return _cellsAlongObjectiveBorder;}
    virtual const std::vector<MapSector *> & sectorsInObjective() {return _sectorsInObjective;}

    
    virtual void loadFromArchive(cereal::BinaryInputArchive & archive);
    virtual void saveToArchive(cereal::BinaryOutputArchive & archive) const;
    
    virtual void removeFromGame();

};
#endif /* Objective_h */
