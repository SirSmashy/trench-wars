//
//  Emblem.hpp
//  TrenchWars
//
//  Created by Paul Reed on 9/19/20.
//

#ifndef Emblem_h
#define Emblem_h

#include "cocos2d.h"
#include "AnimatedSprite.h"
#include "Order.h"
#include "Refs.h"
#include "AutoSizedLayout.h"
#include "AnimatedSpriteProxy.h"
#include "ChangeTrackingVariable.h"
#include "InteractiveObject.h"

USING_NS_CC;

class Unit;

enum EMBLEM_TYPE
{
    POSITION_EMBLEM,
    ORDER_EMBLEM,
    TEST_EMBLEM
};


struct StatusIcon : public Ref
{
    ANIMATION_TYPE statusType;
    AnimatedSpriteProxy * _icon;
    AnimatedSpriteProxy * _background;
};

/*
 * An entity that displays an emblem on the player's screen. Emblems represent a unit or group of units.
 */
class Emblem : public InteractiveObject
{
protected:
    InteractiveObject * _ent;
    
    AnimatedSpriteProxy * _icon;
    AnimatedSpriteProxy * _background;
    Map<ANIMATION_TYPE, StatusIcon *> _statusIcons;
    bool _disabled;
    bool _canDrag;
    float _scale;
    bool _visible;
    Vec2 _spriteToDefaultSizeScale;
    EMBLEM_TYPE _emblemType;
    Vec2 _position;
    bool _isLocalPlayersEmblem;
    
    VectorChangeTrackingVariable<ANIMATION_TYPE> _pendingStatusAdditions;
    VectorChangeTrackingVariable<ANIMATION_TYPE> _pendingStatusRemovals;
    SimpleChangeTrackingVariable<ANIMATION_TYPE> _pendingAnimation;
    SimpleChangeTrackingVariable<bool> _pendingFlashing;

    Emblem();
    ~Emblem();
    
    void updateScale();
    virtual bool init(InteractiveObject * object, EMBLEM_TYPE type, Vec2 position);
    double getBackgroundLuminance(Color3B backgroundColor);
    
    void createStatusIcon(ANIMATION_TYPE statusType);
    void removeStatusIconInternal(ANIMATION_TYPE statusType);
    
public:
    static Emblem * create(InteractiveObject * object, EMBLEM_TYPE type, Vec2 position);
        
    virtual void update(float deltaTime);
    
    virtual INTERACTIVE_OBJECT_TYPE getInteractiveObjectType() {return EMBLEM_INTERACTIVE_OBJECT;}

    void setAnimation(ANIMATION_TYPE animation, bool repeat);
    void setAnimationFromOrderType(ORDER_TYPE order);
        
    virtual void setPosition(Vec2 position);
    virtual Vec2 getPosition();
    
    void addStatusIcon(ANIMATION_TYPE statusType);
    void removeStatusIcon(ANIMATION_TYPE statusType);
    void removeAllStatusIcons();
    
    void setDisabled(bool disabled);
    bool isDisabled();
    
    void setCancel(bool cancel);
    bool isCanceled();
    
    bool canDrag();
    void setCanDrag(bool canDrag);
    
    
    void setFlashing(bool flashing);
    void setVisible(bool visable);
    void setColor(const Color3B & color);
    void setScale(float scale);

    virtual void emblemDragged(Vec2 position);
    virtual void emblemDragEnded(Vec2 position);
    virtual void emblemHoverEnter(Vec2 position);
    virtual void emblemHoverLeave(Vec2 position);
    
    EMBLEM_TYPE getEmblemType() {return _emblemType;}
    virtual void setEmblemType(EMBLEM_TYPE type);
    InteractiveObject * getCommandableObject() {return _ent;}
    Rect getBoundingBox();
    
    bool isLocalPlayersEmblem() {return _isLocalPlayersEmblem;}
    
    virtual bool testCollisionWithCircle(Vec2 center, float radius);
    virtual void removeFromGame();
};
#endif /* Emblem_h */
