//
//  PathFinding.hpp
//  TrenchWars
//
//  Created by Paul Reed on 12/26/19.
//

#ifndef PathFinding_h
#define PathFinding_h

#include <shared_mutex>
#include "cocos2d.h"
#include "CollisionGrid.h"
#include "NavigableRect.h"
#include "NavigationPatch.h"
#include "NavigationPatchBorder.h"
#include "List.h"
#include "SectorGrid.h"
#include "PathBuildingNode.h"
#include "Path.h"


struct PATH_DEBUG
{
    int cellX;
    int cellY;
    
    int parentX;
    int parentY;
    double costSoFar;
    double remainingCostHeuristic;
    double totalCostEstimate;
    int type;
    bool touch;
    bool validToUpdate;
    float terrainMovementCost;
    ENTITY_ID patchId;
    int updateNumber;
    
    double congestionMultiplier;
    struct PATH_DEBUG * next;

};


class PathFinding
{
private:    
    PATH_DEBUG * pathDebugFront;
    PATH_DEBUG * pathDebugEnd;
    FILE * pathDebugFilePointer;

    Map<ENTITY_ID, Label *> patchEntryPointMap;

    void createPathDebugRecord(PathBuildingNode * pathingInfo, bool touch , bool valid, MOVEMENT_PREFERENCE movePreference);
    void printPathDebug(CollisionCell * start, CollisionCell * goal);
    void clearPathDebug();
        
    
    //// Pathfinding functions
    void smoothPath(PatchPathBuildingNode * goalInfo, CollisionCell * goalCell);
    void buildPathNodeList(CollisionCell * goalCell, PatchPathBuildingNode * goalInfo, List<PathNode *> * pathArray, int team);

    double buildPath(Vec2 start, Vec2 end, List<PathNode *> * pathArray, MOVEMENT_PREFERENCE movePreference, int team, bool debug);

    MapSector * getMapSectorPathNodeNearCell(CollisionCell * cell);
    
public:
    PathFinding();
    double getPath(Vec2 start, Vec2 end, List<PathNode *> * pathArray, MOVEMENT_PREFERENCE movePreference, int team, bool debug);

    double getRoute(Vec2 start, Vec2 end, List<RouteNode *> * routeArray, int team, bool debug = false);
    double getPathCostWithinInBounds(CollisionCell * startCell, CollisionCell * endCell, const NavigableRect & bounds, MOVEMENT_PREFERENCE movePreference);

    Map<ENTITY_ID, PathBuildingNode *> pathDebug;
};

extern PathFinding * globalPathFinding;
#endif
