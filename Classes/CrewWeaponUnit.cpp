//
//  CrewWeaponUnit.cpp
//  TrenchWars
//
//  Created by Paul Reed on 1/22/22.
//

#include "CrewWeaponUnit.h"
#include "CrewWeapon.h"
#include "Objective.h"
#include "EntityFactory.h"
#include "EntityManager.h"
#include "MultithreadedAutoReleasePool.h"
#include "CollisionGrid.h"
#include "Command.h"
#include "GameEventController.h"

CrewWeaponUnit::CrewWeaponUnit() : Unit()
{
    
}


bool CrewWeaponUnit::init(UnitDefinition * definition, Vec2 position, Command * command)
{
     if(Unit::init(definition, position, command))
     {
         _crewWeapon = CrewWeapon::create(definition->_crewWeapaon, position, command, this);
         _weaponOwner = _unitMembers.at(0);
         _weaponOwner->addCrewWeapon(_crewWeapon);
         _crewWeapon->setOperator(_weaponOwner);
         _maxUnitAmmo = _currentUnitAmmo = definition->_crewWeaponAmmoReserve;
    
         return true;
     }
    return false;
}

CrewWeaponUnit * CrewWeaponUnit::create(UnitDefinition * definition, Vec2 position, Command * command)
{
    CrewWeaponUnit * newUnit = new CrewWeaponUnit();
    if(newUnit->init(definition, position, command))
    {
        globalAutoReleasePool->addObject(newUnit);
        return newUnit;
    }
    
    CC_SAFE_DELETE(newUnit);
    return nullptr;
}

CrewWeaponUnit * CrewWeaponUnit::create()
{
    CrewWeaponUnit * newUnit = new CrewWeaponUnit();
    globalAutoReleasePool->addObject(newUnit);
    return newUnit;
}


void CrewWeaponUnit::postLoad()
{
    float xMax,xMin,yMax,yMin;
    
    xMax = 0;
    xMin = 100000;
    yMax = 0;
    yMin = 100000;
    
    for(Infantry * member : _unitMembers)
    {
        if((member->getPosition().x + member->getCollisionBounds().size.width) > xMax)
            xMax = member->getPosition().x + member->getCollisionBounds().size.width;
        if((member->getPosition().x ) < xMin)
            xMin = member->getPosition().x;
        if((member->getPosition().y + member->getCollisionBounds().size.height) > yMax)
            yMax = member->getPosition().y + member->getCollisionBounds().size.height;
        if((member->getPosition().y) < yMin)
            yMin = member->getPosition().y;
    }
    
    if((_crewWeapon->getPosition().x + _crewWeapon->getCollisionBounds().size.width) > xMax)
        xMax = _crewWeapon->getPosition().x + _crewWeapon->getCollisionBounds().size.width;
    if((_crewWeapon->getPosition().x ) < xMin)
        xMin = _crewWeapon->getPosition().x;
    if((_crewWeapon->getPosition().y + _crewWeapon->getCollisionBounds().size.height) > yMax)
        yMax = _crewWeapon->getPosition().y + _crewWeapon->getCollisionBounds().size.height;
    if((_crewWeapon->getPosition().y) < yMin)
        yMin = _crewWeapon->getPosition().y;
    
    
    _defaultUnitSize.width = xMax - xMin;
    _defaultUnitSize.height = yMax - yMin;
    updateBounds();
    
    _owningCommand->addUnit(this);
    globalGameEventController->handleEventNewUnit(this);
    
    if(_currentOrder != nullptr)
    {
        globalGameEventController->handleEventOrderIssued(_currentOrder, this, nullptr); // TODO fix do we need this???
    }
}

void CrewWeaponUnit::crewWeaponNeedsWork(bool helpReloading)
{
    if(helpReloading)
    {
        for(Infantry * member : _unitMembers)
        {
            ReloadWeaponGoal * reload = ReloadWeaponGoal::create(_crewWeapon);
            // If this weapon can directly fire on the enemy, then give it a higher importance for the weapon owner (indicating that they should priorizte working on this weapon)
            if(member == _weaponOwner && _crewWeapon->getProjectileType() == DIRECT)
            {
                reload->setImportance(10);
            }
            else
            {
                reload->setImportance(4);
            }
            member->addHelpGoal(reload, helpReloading);
        }

    }
    else
    {
        for(Infantry * member : _unitMembers)
        {
            EntityWorkGoal * work = EntityWorkGoal::create(_crewWeapon);
            work->setImportance(1);
            member->addHelpGoal(work, helpReloading);
        }
    }
}

void CrewWeaponUnit::doWorkOrder(WorkOrder * work)
{
    // If the crew weapon is not at the average work position, move it there before performing the work
    if(work->getAverageWorkPosition() != _crewWeapon->getPosition())
    {
        MoveGoal * weaponMove = MoveGoal::create(work->getAverageWorkPosition());
        _crewWeapon->setGoal(weaponMove);
    }
    for(Infantry * member : _unitMembers)
    {
        _membersWorkingOnOrder++;
        member->addGoalFromOrder(work);
    }
}

void CrewWeaponUnit::doMoveOrder(MoveOrder * move)
{
    MoveGoal * weaponMove = MoveGoal::create(move->getPosition(), move->shouldIgnoreCover());
    _crewWeapon->setGoal(weaponMove);
}

void CrewWeaponUnit::doAttackPositionOrder(AttackPositionOrder * attack)
{
    _weaponOwner->addGoalFromOrder(attack);
}

void CrewWeaponUnit::clearOrder()
{
    _currentOrderMutex.lock();
    if(_currentOrder != nullptr)
    {
        _owningCommand->unitCompletedOrder(this, _currentOrder, true);
        _currentOrder = nullptr;
        _crewWeapon->setGoal(nullptr);
        _membersWorkingOnOrder = 0;
        for(Infantry * member : _unitMembers)
        {
            member->clearUnitGoal();
        }
    }
    _currentOrderMutex.unlock();
}

bool CrewWeaponUnit::isIndirectFireWeapon()
{
    return _crewWeapon->getProjectileType() != DIRECT;
}

bool CrewWeaponUnit::positionValidForBombardment(Vec2 position)
{
    float distance = _crewWeapon->getPosition().distance(position);
    if(distance > _crewWeapon->getMinimumRange() && distance < _crewWeapon->getMaximumRange())
    {
        return true;
    }
    return false;
}

int CrewWeaponUnit::getUnitAmmo(int requestedAmount)
{
    if(_currentUnitAmmo < requestedAmount)
    {
        // Try to get ammo
        _currentUnitAmmo += getOwningCommand()->withdrawAmmo(_crewWeapon, _maxUnitAmmo - _currentUnitAmmo);
    }
    requestedAmount = _currentUnitAmmo < requestedAmount ? 0 : requestedAmount;
    _currentUnitAmmo -= requestedAmount;
    return requestedAmount;
}

void CrewWeaponUnit::handleMemberRemoval(Infantry * member)
{
    Unit::handleMemberRemoval(member);
    
    if(_currentStrength > 0)
    {
        _maxUnitAmmo -= _maxUnitAmmo / ((double) _maxStrength);
        if(_currentUnitAmmo > _maxUnitAmmo)
        {
            _currentUnitAmmo = _maxUnitAmmo;
        }
        _weaponOwner = _unitMembers.at(0);
        _weaponOwner->addCrewWeapon(_crewWeapon);
        _crewWeapon->setOperator(_weaponOwner);
        if(_currentOrder != nullptr && _currentOrder->getOrderType() == ATTACK_POSITION_ORDER)
        {
            _weaponOwner->addGoalFromOrder(_currentOrder);
        }
    }
}

void CrewWeaponUnit::removeFromGame()
{
    _crewWeapon->markForRemoval();
    Unit::removeFromGame();
}

void CrewWeaponUnit::loadFromArchive(cereal::BinaryInputArchive & archive)
{
    Unit::loadFromArchive(archive);
    ENTITY_ID thingId;
    archive(thingId);
    if(thingId != -1)
    {
        _crewWeapon = dynamic_cast<CrewWeapon *>(globalEntityManager->getEntity(thingId));
    }
    else
    {
        _crewWeapon = nullptr;
    }
    
    archive(thingId);
    if(thingId != -1)
    {
        _weaponOwner = dynamic_cast<Infantry *>(globalEntityManager->getEntity(thingId));
    }
    else
    {
        _weaponOwner = nullptr;
    }
    
    archive(_maxUnitAmmo);
    archive(_currentUnitAmmo);
}

void CrewWeaponUnit::saveToArchive(cereal::BinaryOutputArchive & archive) const
{
    Unit::saveToArchive(archive);
    if(_crewWeapon != nullptr)
    {
        archive(_crewWeapon->uID());
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
    if(_weaponOwner != nullptr)
    {
        archive(_weaponOwner->uID());
    }
    else
    {
        archive((ENTITY_ID) -1);
    }
    
    archive(_maxUnitAmmo);
    archive(_currentUnitAmmo);
}
